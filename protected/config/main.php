<?php

return array(
	'name'=>'Food Store',
	'defaultController'=>'store',
	'import'=>array(
		'application.models.*',
		'application.models.admin.*',
		'application.components.*',
		'application.vendor.*',
		'application.modules.mobileapp.components.*',
		'application.modules.merchantapp.components.*',
		'application.modules.driver.components.*',

	),
	
	'language'=>'default',		
			
	// only enabled the addon if you have them
	
	'modules'=>array(		
		'mobileapp'=>array(
		  'require_login'=>true
		),
		'merchantapp'=>array(
		  'require_login'=>true
		),
		'driver'=>array(
		  'require_login'=>true
		)
	),
		
	'components'=>array(
		/*'urlManager'=>array(
			'urlFormat'=>'path',			
		),*/
	    'urlManager'=>array(
		    'urlFormat'=>'path',
		    'rules'=>array(
		        '<controller:\w+>/<id:\d+>'=>'<controller>/view',
		        '<controller:\w+>'=>'<controller>/index',
		        '<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
		        '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',		        
		    ),
		    'showScriptName'=>false,
		),
				
		'db'=>array(	        
		    'class'            => 'CDbConnection' ,
			'connectionString' => 'mysql:host=localhost;dbname=foodcepot',
			'emulatePrepare'   => true,
			'username'         => 'root',
			'password'         => '',
			'charset'          => 'utf8',
			'tablePrefix'      => 'mt_',
	    ),
	    
	    'functions'=> array(
	       'class'=>'Functions'
	    ),
	    'validator'=>array(
	       'class'=>'Validator'
	    ),
	    'widgets'=> array(
	       'class'=>'Widgets'
	    ),
	    	    
	    'Smtpmail'=>array(
	        'class'=>'application.extension.smtpmail.PHPMailer',
	        'Host'=>"",
            'Username'=>'',
            'Password'=>'',
            'Mailer'=>'smtp',
            'Port'=>587,
            'SMTPAuth'=>true,   
            'ContentType'=>'UTF-8'
	    ), 
	    
	    'GoogleApis' => array(
	         'class' => 'application.extension.GoogleApis.GoogleApis',
	         'clientId' => 'food-1350', 
	         'clientSecret' => '794764338074',
	         'redirectUri' => '',
	         'developerKey' => 'AIzaSyARECdMiluWbZ4OBF09a-QZl9AhURRztpw',
	    ),
        // 'cache'=>array(
            // 'class'=>'system.caching.CMemCache',
            // 'servers'=>array(
                // array('host'=>'server1', 'port'=>11211, 'weight'=>60),
                // // array('host'=>'server2', 'port'=>11211, 'weight'=>40),
            // ),
        // ),
	),
);

function statusList()
{
	return array(
	 'publish'=>Yii::t("default",'Publish'),
	 'pending'=>Yii::t("default",'Pending for review'),
	 'draft'=>Yii::t("default",'Draft')
	);
}
function statusList2()
{
	return array(
	 'publish'=>Yii::t("default",'Publish'),
	 'draft'=>Yii::t("default",'Draft')
	);
}
function statusList3()
{
	return array(
	 'pending'=>Yii::t("default",'Pending'),
	 'approved'=>Yii::t("default",'Approved'),
	 'declined'=>Yii::t("default",'Declined'),
	);
}
function conditionList()
{
	return array(
	 'new'=>Yii::t("default",'New'),
	 'second'=>Yii::t("default",'Second')
	);
}
function clientStatus()
{
	return array(
	  'pending'=>Yii::t("default",'pending for approval'),
	 'active'=>Yii::t("default",'active'),	 
	 'suspended'=>Yii::t("default",'suspended'),
	 'blocked'=>Yii::t("default",'blocked'),
	 'expired'=>Yii::t("default",'expired')
	);
}
function orderStatusMerchant()
{
	return array(
		'pending'=>Yii::t("default",'Pending'),
		'accepted_merchant'=>Yii::t("default",'Accepted'),
		'declined_merchant'=>Yii::t("default",'Declined'),
	);
}
function paymentStatus()
{
	return array(
	 'pending'=>Yii::t("default",'pending'),
	 'paid'=>Yii::t("default",'paid'),
	 'draft'=>Yii::t("default",'Draft')
	);
}

function dump($data=''){
	// file_put_contents('dump.txt',file_get_contents('dump.txt').'\n /n'.var_export($data,true));

    echo '<pre>';print_r($data);echo '</pre>';
	
}

	function getOne($table,$column,$id)
	{
		$DbExt=new DbExt;
		$stmt="SELECT * FROM
		{{".$table."}}
		WHERE
		$column='$id'
		LIMIT 0,1
		";
		// dump($stmt);
		if ( $res=$DbExt->rst($stmt)){
			return $res[0];
		}
		return false;
	}
	/**
	Get One Object From query
	**/
	function getOneObj($table,$column,$id)
	{
		$connection=Yii::app()->db;
		$stmt="SELECT * FROM
		{{".$table."}}
		WHERE
		$column='$id'
		LIMIT 0,1
		";
		$rows=$connection->createCommand($stmt)->setFetchMode(PDO::FETCH_OBJ)->queryAll();
		if (is_array($rows) && count($rows)>=1){
			return $rows[0];
		} else return false;
	}
	function getWhere($table,$column,$id)
	{
		$DbExt=new DbExt;
		$stmt="SELECT * FROM
		{{".$table."}}
		WHERE
		$column='$id'
		";
		if ( $res=$DbExt->rst($stmt)){
			return $res;
		}
		return false;
	}
	function query($stmt,$argument = "")
	{
		if (!empty($stmt)){
			$query=Yii::app()->db->createCommand($stmt);
			if(!empty($argument)) {
				$query->params = $argument;
			}
			$rows = $query->queryAll();;
		    if (is_array($rows) && count($rows)>=1){
		    	return $rows;
		    } else return false;
		} else return false;
		
		// $query = Yii::app()->db->createCommand($sql);
		// $query->params = $newArgs;
		// $DbExt=new DbExt;
		// if ( $res=$DbExt->rst($stmt)){
		    // if (is_array($res) && count($res)>=1){
		    	// return $res;
		    // } else return false;
		// }
		// return false;
	}
	function queryNoFetch($stmt,$argument = "")
	{
		if (!empty($stmt)){
			$query=Yii::app()->db->createCommand($stmt);
			if(!empty($argument)) {
				$query->params = $argument;
			}
			$rows = $query->query();
			return true;
		} else return false;
	}
	function querySQL($stmt)
	{
		if (!empty($stmt)){
			$connection=Yii::app()->db;
		    $command=$connection->createCommand($stmt);
			// $rowCount=$command->execute(); // execute the non-query SQL
			$dataReader=$command->query(); // execute a query SQL
			return true;
		} else return false;
	}
	
	function queryO($sql='')
	{	
		if (!empty($sql)){
			$connection=Yii::app()->db;
		    $rows=$connection->createCommand($sql)->setFetchMode(PDO::FETCH_OBJ)->queryAll();
		    if (is_array($rows) && count($rows)>=1){
		    	return $rows;
		    } else return false;
		} else return false;
	}
	
	function trans($message) {
		$message = str_replace(' ','%20',$message);
		$curl = curl_init();
		// $header[] = "Host: translate.yandex.net";
		// $header[] = "Content-Length: 17";
		// $header[] = "Accept: */*";
		// $header[] = "Content-Length: 17";
        $header[] = "Content-Type: application/x-www-form-urlencoded";
		// $header[] = "text=Hello World!";

// POST /api/v1.5/tr.json/translate?lang=en-ru&key=API-KEY HTTP/1.1

        // $header[] = "key: $this->api_key";
        // $query = http_build_query($params);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
		//FOR ACCOUNT PRO
        curl_setopt($curl, CURLOPT_URL, 
		'https://translate.yandex.net/api/v1.5/tr.json/translate?lang=en-id&key=trnsl.1.1.20160907T105730Z.168b3db63a0e344b.d8ea639f98ff766ed2988aa05510b873126eefe8&text='.$message
		);
		//FOR STARTER
        // curl_setopt($curl, CURLOPT_URL, $this->api_url . "" . $this->account_type . "/" . $this->endpoint);
        curl_setopt($curl, CURLOPT_POST, TRUE);
        // curl_setopt($curl, CURLOPT_POSTFIELDS, $query);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($curl, CURLOPT_TIMEOUT, 10); //timeout in seconds
        $request = curl_exec($curl);
        // $return = ($request === FALSE) ? curl_error($curl) : $request;
        // curl_close($curl);
        // return $return;
		// $curl_errno = curl_errno($curl);
        // $curl_error = curl_error($curl);
        // curl_close($curl);
        // if ($curl_errno > 0) {
             // return $curl_error;
        // } else {
        var_dump($request);
		$r = json_decode($request);
		if(isset($r->code) && $r->code == 200) {
			if(!empty($r->text[0])){
				return $r->text[0];
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	function vdump($var) {
		file_put_contents('output.txt',var_export($var,true), FILE_APPEND);
	}
	function gdump($var) {
		file_put_contents('geocodelog.txt',var_export($var,true), FILE_APPEND);
	}