<?php
if (!isset($_SESSION)) { session_start(); }

class AjaxController extends CController
{
	public $layout='_store';	
	public $code=2;
	public $msg;
	public $details;
	public $data;
	
	public function __construct()
	{
		$this->data=$_POST;	
		if (isset($_GET['post_type'])){
			if ($_GET['post_type']=="get"){
				$this->data=$_GET;	
			}
		}
	}
	
	private function jsonResponse()
	{
		$resp=array('code'=>$this->code,'msg'=>$this->msg,'details'=>$this->details);
		echo CJSON::encode($resp);
		Yii::app()->end();
	}
	
	private function otableNodata()
	{
		if (isset($_GET['sEcho'])){
			$feed_data['sEcho']=$_GET['sEcho'];
		} else $feed_data['sEcho']=1;	   
		     
        $feed_data['iTotalRecords']=0;
        $feed_data['iTotalDisplayRecords']=0;
        $feed_data['aaData']=array();		
        echo json_encode($feed_data);
    	die();
	}

	private function otableOutput($feed_data='')
	{
	  echo json_encode($feed_data);
	  die();
    }    
    
    public function actionLoadAllRestoMap()
    {
    	$data='';
    	$stmt=$_SESSION['kmrs_search_stmt'];
    	if (!empty($stmt)){
    		$pos=strpos($stmt,'LIMIT');    		
    		$stmt=substr($stmt,0,$pos-1);       		
    		$DbExt=new DbExt();
    		$DbExt->qry("SET SQL_BIG_SELECTS=1");
    		if ( $res=$DbExt->rst($stmt)){
    			foreach ($res as $val) {    
    				if (!empty($val['latitude']) && !empty($val['longitude'])){
	    				$data[]=array(
	    				  'merchant_name'=>stripslashes($val['merchant_name']),
	    				  'merchant_slug'=>$val['merchant_slug'],
	    				  'merchant_address'=>$val['merchant_address'],
	    				  'latitude'=>$val['latitude'],
	    				  'longitude'=>$val['longitude'],
	    				  'logo'=>FunctionsV3::getMerchantLogo($val['merchant_id']),
	    				  'link'=>Yii::app()->createUrl('store/menu/merchant/'.$val['merchant_slug'])
	    				);
    				}
    			}    			
    			$this->code=1;
    			$this->msg="OK";
    			$this->details=$data;
    		} else $this->msg=t("no records");
    	} else $this->msg=t("invalid statement query");
    	$this->jsonResponse();
    }
    
    public function actionloadAllMerchantMap()
    {    	
    	$datas='';
    	if ( $data=Yii::app()->functions->getAllMerchant(true)){
    		foreach ($data['list'] as $val) {
    			if (!empty($val['latitude']) && !empty($val['longitude'])){
    				$datas[]=array(
    				  'merchant_name'=>$val['merchant_name'],
    				  'merchant_slug'=>$val['merchant_slug'],
    				  'merchant_address'=>$val['merchant_address'],
    				  'latitude'=>$val['latitude'],
    				  'longitude'=>$val['longitude'],
    				  'logo'=>FunctionsV3::getMerchantLogo($val['merchant_id']),
    				  'link'=>Yii::app()->createUrl('store/menu/merchant/'.$val['merchant_slug'])
    				);
				}
    		}
    		$this->code=1;
			$this->msg="OK";
			$this->details=$datas;
    	} else $this->msg=t("no records");
    	$this->jsonResponse();
    }
    
    public function actionsaveAvatar()
    {    	
    	$DbExt=new DbExt;
    	if (!empty($this->data['filename'])){
    		$params=array(
    		  'avatar'=>$this->data['filename'],
    		  'date_modified'=>date(''),
    		  'ip_address'=>$_SERVER['REMOTE_ADDR']
    		);
    		$client_id=Yii::app()->functions->getClientId();    		
    		if (is_numeric($client_id)){
    			$DbExt->updateData("{{client}}",$params,'client_id',$client_id);
    			$this->msg=t("You have succesfully change your profile picture");
    			$this->code=1;
    		} else $this->msg=t("ERROR: Your session has expired.");
    	} else $this->msg=t("Filename is empty");
    	$this->jsonResponse();
    }
    
    public function actionViewReceipt()
    {
    	/** Register all scripts here*/
    	ScriptManager::registerAllCSSFiles();
		$this->render('/store/receipt-front',array(
		  'data'=>Yii::app()->functions->getOrder2( isset($this->data['order_id'])?$this->data['order_id']:'' )
		));
    }
    
    public function actionResendEmailCode()
    {
    	$client_id=isset($this->data['client_id'])?$this->data['client_id']:'';
    	if( $res=Yii::app()->functions->getClientInfo( $client_id )){	
    		FunctionsV3::sendEmailVerificationCode($res['email_address'],$res['email_verification_code'],$res);
    		$this->code=1;
    		$this->msg=t("We have sent verification code to your email address");
    	} else $this->msg=t("Sorry but we cannot find your information.");
    	$this->jsonResponse();
    }
    	
} /*end class*/    