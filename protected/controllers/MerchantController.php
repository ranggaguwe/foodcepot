<?php
/**
 * MerchantController Controller
 *
 */
if (!isset($_SESSION)) { session_start(); }

class MerchantController extends CController
{
	public $layout='merchant_tpl';	
	public $crumbsTitle='';
	
	public function accessRules()
	{		
		
	}
	
	public function beforeAction($action)
    {    	
    	$action_name= $action->id ;
    	$accept_controller=array('login','ajax','autologin');
	    //if(!Yii::app()->functions->isMerchantLogin() )
	    if(!Yii::app()->functions->validateMerchantSession() )
	    {
	    	if (!in_array($action_name,$accept_controller)){	 	           
	           if ( Yii::app()->functions->has_session){
	    	   	    $message_out=t("You were logout because someone login with your account");
	    	   	    $this->redirect(array('merchant/login/?message='.urlencode($message_out)));
	    	   } else $this->redirect(array('merchant/login'));	           
	    	}
	    }		    
	    
	    if ( $action_name=="autologin"){
	    	return true;
	    }
	    /*echo $this->uniqueid;
	    echo '<br/>';
	    echo $action_name;*/
	    if ( $this->uniqueid=="merchant"){
	    	if ( !Yii::app()->functions->hasMerchantAccess($action_name)){
	    		if ( $action_name!="login"){
	    			if ( $action_name!="index"){
	    				$this->crumbsTitle=Yii::t("default","No Access");		
	    		        $this->render('noaccess');
	    		        return ;
	    			}    
	    		}
	    	}
	    }
	    return true;	    
    }	
        	
	public function init()
	{		
		 $name=Yii::app()->functions->getOptionAdmin('website_title');
		 if (!empty($name)){		 	
		 	 Yii::app()->name = $name;
		 }		 
		 
		 
		 $mtid=Yii::app()->functions->getMerchantID();		 
		 // set website timezone
		 $website_timezone=Yii::app()->functions->getOptionAdmin("website_timezone");		 
		 if (!empty($website_timezone)){		 	
		 	Yii::app()->timeZone=$website_timezone;
		 }		 		 
		 $mt_timezone=Yii::app()->functions->getOption("merchant_timezone",$mtid);	   	   	    	
    	 if (!empty($mt_timezone)){    	 	
    		Yii::app()->timeZone=$mt_timezone;
    	 }		     	 
	}
				  
	public function actionIndex()
	{					
		if ( !Yii::app()->functions->isMerchantLogin()){						
			$this->layout='login_tpl';
			$this->render('login');
		} else {											
			$this->crumbsTitle=Yii::t("default","Dashboard");		
			$this->render('dashboard');			
		}		
	}	
	
	
	public function actionDashBoard()
	{					
		$this->crumbsTitle=Yii::t("default","Dashboard");
		
		if ( !Yii::app()->functions->isMerchantLogin()){						
			$this->layout='login_tpl';
			$this->render('login');
		} else {									
			$this->crumbsTitle=Yii::t("default","Dashboard");
			$this->render('dashboard');			
		}		
	}	
	
	
	public function actionLogin()
	{		
		if (isset($_GET['logout'])){
			//Yii::app()->request->cookies['kr_merchant_user'] = new CHttpCookie('kr_merchant_user', ""); 			
			unset($_SESSION['kr_merchant_user']);
		}		
		$this->layout='login_tpl';
	    $this->render('login');
	}
	
	public function actionAjax()
	{			
		if (isset($_REQUEST['tbl'])){
		   $data=$_REQUEST;	
		} else $data=$_POST;
				
		if (isset($data['debug'])){
			dump($data);
		}
		$class=new AjaxAdmin;
	    $class->data=$data;
	    $class->$data['action']();	    
	    echo $class->output();
	    yii::app()->end();
	}	
	
	public function actionCategoryList()
	{	    
		$this->crumbsTitle=Yii::t("default","Category");
		
	    if (isset($_GET['Do'])){
			if ( $_GET['Do']=="Add"){
				$this->render('category_add');
			} elseif ( $_GET['Do'] =="Sort" ){	
			   $this->render('category_sort');
			} else $this->render('category_list');
		} else $this->render('category_list');
	}

	
	public function actionFoodItem()
	{
		$this->crumbsTitle=Yii::t("default","Food Item");
		
		if (isset($_GET['Do'])){
			if ( $_GET['Do']=="Add"){
				$this->render('food-item-add');
			} elseif ( $_GET['Do'] =="Sort" ){	
			   $this->render('food_item_sort');	
			} else $this->render('food-item-list');		
		} else $this->render('food-item-list');
	}
	
	public function actionMerchant()
	{
		$this->crumbsTitle=Yii::t("default","Merchant");
		$this->render('merchant-info');
	}
	
	public function actionSettings()
	{
		$this->crumbsTitle="Settings";
		$this->render('settings');
	}
	
	public function actionSocialSettings()
	{
		$this->crumbsTitle=t("Social Settings");
		$this->render('social-settings');
	}
	
	public function actionAlertSettings()
	{
		$this->crumbsTitle=Yii::t("default","Alert Settngs");
		$this->render('alert-settings');
	}
	
    
    public function actionSalesReport()
    {
    	$this->crumbsTitle=Yii::t("default","Sales Report");
    	$this->render('sales-report');
    }

    
    
	public function actionSetlanguage()
	{		
		if (isset($_GET['Id'])){			
			Yii::app()->request->cookies['kr_merchant_lang_id'] = new CHttpCookie('kr_merchant_lang_id', $_GET['Id']);						
			$id=Yii::app()->functions->getMerchantID();			
			Yii::app()->functions->updateMerchantLanguage($id,$_GET['Id']);
			
			if (!empty($_SERVER['HTTP_REFERER'])){
					header('Location: '.$_SERVER['HTTP_REFERER']);
					die();
		    } else {
		    	header('Location: '.Yii::app()->request->baseUrl);
		    	die();
		    }
		}
		header('Location: '.Yii::app()->request->baseUrl);
	}	    
	

	
	public function actionUser()
	{
		$this->crumbsTitle=Yii::t("default","User List");
		if (isset($_GET['Do'])){
			$this->crumbsTitle=Yii::t("default","User Add/Update");
			$this->render('user-add');		
		} else $this->render('user-list');		
	}
	

	
	public function actionReview()
	{
		$this->crumbsTitle=Yii::t("default","Customer reviews");		
		if (isset($_GET['Do'])){
			if ( Yii::app()->functions->getOptionAdmin('merchant_can_edit_reviews')=="yes"){			
				$this->render('error',array(
				 'message'=>t("Sorry but you don't have access this page.")
				));
			} else {				
				$this->crumbsTitle=Yii::t("default","Customer reviews Update");
				$this->render('review-add');
			}
		} else $this->render('review-list');				
	}
	
	
	
	public function actionAutoLogin()
	{
		$DbExt=new DbExt;
		$data=$_GET;		
		$stmt="SELECT * FROM
		       {{merchant}}
		       WHERE
		       merchant_id=".Yii::app()->db->quoteValue($data['id'])."
		       AND
		       password=".Yii::app()->db->quoteValue($data['token'])."
		       LIMIT 0,1
		";							
		if ( $res=$DbExt->rst($stmt)){										
			$_SESSION['kr_merchant_user']=json_encode($res);
			
			$session_token=Yii::app()->functions->generateRandomKey().md5($_SERVER['REMOTE_ADDR']);				
			 $params=array(
			  'session_token'=>$session_token,
			  //'last_login'=>date('c')
			 );
			 $DbExt->updateData("{{merchant}}",$params,'merchant_id',$res[0]['merchant_id']);
			 
			 $_SESSION['kr_merchant_user_session']=$session_token;
			 $_SESSION['kr_merchant_user_type']='admin';
			
			$this->redirect(baseUrl()."/merchant",true);			
		} else $msg=t("Login Failed. Either username or password is incorrect");
		echo $msg;
	}
		
	public function actionGallerySettings()
	{
		$this->crumbsTitle=Yii::t("default","gallery settings");		
		$this->render('gallery-settings');
	}
	


	
	public function actionEarnings()
	{
		$this->crumbsTitle=Yii::t("default","Earnings");		
		$this->render('earnings');
	}
	
	
	public function actionWithdrawals()
	{
		$stats=yii::app()->functions->getOptionAdmin('wd_payout_disabled');
		if ($stats==2){
			$this->crumbsTitle=Yii::t("default","Withdrawals");
			$this->render('error',array('message'=>t("Sorry but widthrawal is disabled by the site owner")));
		} else {
			$this->crumbsTitle=Yii::t("default","Withdrawals");		
			$this->render('withdrawals');
		}
	}
	
	public function actionWithdrawalStep2()
	{
		$this->crumbsTitle=Yii::t("default","Withdrawals Complete");		
		$this->render('withdrawals-step2');
	}
	
	public function actionWithdrawalsHistory()
	{
		$this->crumbsTitle=Yii::t("default","Withdrawal History");		
		$this->render('withdrawals-history');
	}
	

	public function actionProfile()
	{
		$merchant_info=Yii::app()->functions->getMerchantInfo();
		$user_id=$merchant_info[0]->merchant_user_id;
		$data=Yii::app()->functions->getMerchantUserInfo($user_id);
		if (is_array($data) && count($data)>=1){
		    $this->crumbsTitle=Yii::t("default","Profile");		
			$this->render('profile',array('data'=>$data));
		} else {
			$this->crumbsTitle=Yii::t("default","Error");		
			$this->render('error',array('message'=>t("Error session has expired")));
		}
	}
	
}
/*END CONTROLLER*/
