<?php
if (!isset($_SESSION)) { session_start(); }

class StoreController extends CController
{
	public $layout='store_tpl';	
	public $crumbsTitle='';
	public $theme_compression='';
	
	public function beforeAction($action)
	{
		//$cs->registerCssFile($baseUrl.'/css/yourcss.css'); 		
		if( parent::beforeAction($action) ) {			
			
			/** Register all scripts here*/
			if ($this->theme_compression==2){
				ScriptManagerCompress::RegisterAllJSFile();
			    ScriptManagerCompress::registerAllCSSFiles();
			   
				$compress_css = require_once 'assets/css/css.php';
			    $cs = Yii::app()->getClientScript();
			    Yii::app()->clientScript->registerCss('compress-css',$compress_css);
			} else {
			   ScriptManager::RegisterAllJSFile();
			   ScriptManager::registerAllCSSFiles();
			}
			return true;
		}
		return false;
	}
	
	public function accessRules()
	{		
		
	}
	
    public function filters()
    {
    	$this->theme_compression = getOptionA('theme_compression');
		if ($this->theme_compression==2){
	        $filters = array(  
	            array(
	                'application.filters.HtmlCompressorFilter',
	            ),  
	        );
	        return $filters;
		}
    }	
		
	public function init()
	{		
		 $name=Yii::app()->functions->getOptionAdmin('website_title');
		 if (!empty($name)){		 	
		 	 Yii::app()->name = $name;
		 }
		 		 
		 // set website timezone
		 $website_timezone=Yii::app()->functions->getOptionAdmin("website_timezone");		 
		 if (!empty($website_timezone)){		 	
		 	Yii::app()->timeZone=$website_timezone;
		 }		 		 
	}
	
	public function actionHome()
	{
		
		unset($_SESSION['voucher_code']);
        unset($_SESSION['less_voucher']);
        unset($_SESSION['google_http_refferer']); 
        
		if (isset($_GET['token'])){
			if (!empty($_GET['token'])){
			    
			}
		}
				
		$seo_title=Yii::app()->functions->getOptionAdmin('seo_home');
		$seo_meta=Yii::app()->functions->getOptionAdmin('seo_home_meta');
		$seo_key=Yii::app()->functions->getOptionAdmin('seo_home_keywords');
					
		if (!empty($seo_title)){
		   $seo_title=smarty('website_title',getWebsiteName(),$seo_title);
		   $this->pageTitle=$seo_title;
		   Yii::app()->functions->setSEO($seo_title,$seo_meta,$seo_key);
		}

		
		$this->render('index',array(
		   'home_search_mode'=>getOptionA('home_search_mode'),
		   'enabled_advance_search'=> getOptionA('enabled_advance_search'),
		   'theme_hide_how_works'=>getOptionA('theme_hide_how_works'),
		   'theme_hide_cuisine'=>getOptionA('theme_hide_cuisine'),
		   'disabled_featured_merchant'=>getOptionA('disabled_featured_merchant'),
		   'disabled_subscription'=>getOptionA('disabled_subscription'),
		   'theme_show_app'=>getOptionA('theme_show_app'),
		   'theme_app_android'=>FunctionsV3::prettyUrl(getOptionA('theme_app_android')),
		   'theme_app_ios'=>FunctionsV3::prettyUrl(getOptionA('theme_app_ios')),
		   'theme_app_windows'=>FunctionsV3::prettyUrl(getOptionA('theme_app_windows')),
		));
	}
				  
	public function actionIndex()
	{							
		$this->redirect(Yii::app()->request->baseUrl."/store/home");
		Yii::app()->end();		
	}	
	
	public function actionSignup()
	{
		$cs = Yii::app()->getClientScript();
		$baseUrl = Yii::app()->baseUrl; 
		$cs->registerScriptFile($baseUrl."/assets/js/fblogin.js?ver=1"); 
		    
		if (Yii::app()->functions->isClientLogin()){
			$this->redirect(Yii::app()->createUrl('/store')); 
			die();
		}
		
		$act_menu=FunctionsV3::getTopMenuActivated();
		if (!in_array('signup',(array)$act_menu)){
			$this->render('404-page',array('header'=>true));
			return ;
		}	
		
		$fb=1;
		$fb_app_id=getOptionA('fb_app_id');
		$fb_flag=getOptionA('fb_flag');
		
		if ( $fb_flag=="" && $fb_app_id<>""){
			$fb=2;
		}
		
		$this->render('signup',array(
		   'terms_customer'=>getOptionA('website_terms_customer'),
		   'terms_customer_url'=>Yii::app()->functions->prettyLink(getOptionA('website_terms_customer_url')),
		   'fb_flag'=>$fb,
		   'google_login_enabled'=>getOptionA('google_login_enabled'),
		   'captcha_customer_login'=>getOptionA('captcha_customer_login'),
		   'captcha_customer_signup'=>getOptionA('captcha_customer_signup')
		));
	}
	
	public function actionSignin()
	{
		$this->render('index');
	}
	
	public function actionMerchantSignup()
	{		
		
		$act_menu=FunctionsV3::getTopMenuActivated();
		if (!in_array('resto_signup',(array)$act_menu)){
			$this->render('404-page',array('header'=>true));
			return ;
		}
		
		$seo_title=Yii::app()->functions->getOptionAdmin('seo_merchantsignup');
		$seo_meta=Yii::app()->functions->getOptionAdmin('seo_merchantsignup_meta');
		$seo_key=Yii::app()->functions->getOptionAdmin('seo_merchantsignup_keywords');
		
		if (!empty($seo_title)){
		   $seo_title=smarty('website_title',getWebsiteName(),$seo_title);
		   $this->pageTitle=$seo_title;
		   Yii::app()->functions->setSEO($seo_title,$seo_meta,$seo_key);
		}
		
		if(isset($_GET['package_id'])){
			$_GET['id']=$_GET['package_id'];
		}	
		
		if (isset($_GET['Do'])){
			$_GET['do']=$_GET['Do'];
		}	
		
		if (isset($_GET['do'])){
			switch ($_GET['do']) {
				case 'step2':
					$this->render('merchant-signup-step2',array(
					  // 'data'=>Yii::app()->functions->getPackagesById($_GET['package_id']),
					  'data'=>Yii::app()->functions->getPackagesById(1),
					  'limit_post'=>Yii::app()->functions->ListlimitedPost(),
					  'terms_merchant'=>getOptionA('website_terms_merchant'),
					  'terms_merchant_url'=>getOptionA('website_terms_merchant_url'),
					  'package_list'=>Yii::app()->functions->getPackagesList(),
					  'kapcha_enabled'=>getOptionA('captcha_merchant_signup')
					));		
					break;
				case "step3":
					 $renew=false;
					 $package_id=isset($_GET['package_id'])?$_GET['package_id']:'';  
					 if (isset($_GET['action'])){	 
						 $renew=true;
					 } 
					 $this->render('merchant-signup-step3',array(
					    'merchant'=>Yii::app()->functions->getMerchantByToken($_GET['token']),
					    'package_id'=>$package_id,
					    'renew'=>$renew					    
					 ));		
					break;
				case "step3a":
					 $this->render('merchant-signup-step3a');		
					break;	
				case "step3b":					    
					if (isset($_GET['gateway'])){
						if ($_GET['gateway']=="mcd"){
							$this->render('mercado-init');
						} elseif ( $_GET['gateway']=="pyl" ){
							$this->render('payline-init2');
						} elseif ( $_GET['gateway']=="ide" ){
							$this->render('sow-init');
						} elseif ( $_GET['gateway']=="payu" ){							
							$this->render('pau-init');	
						} elseif ( $_GET['gateway']=="pys" ){							
							$this->render('paysera-init');	
						} else {
							$this->render($_GET['gateway'].'-init');	
						}
					} else $this->render('merchant-signup-step3b');
					break;		
					
				case "step4":					     
				     $disabled_verification=Yii::app()->functions->getOptionAdmin('merchant_email_verification');
				     if ( $disabled_verification=="yes"){
				     	$this->render('merchant-signup-thankyou2',array(
				     	  'data'=>Yii::app()->functions->getMerchantByToken($_GET['token'])
				     	));		
				     } else {			
				     					     	
				     	 $continue=true;
						 if ($merchant=Yii::app()->functions->getMerchantByToken($_GET['token'])){	
							if ( $merchant['package_price']>=1){
								if ($merchant['payment_steps']!=3){
									$continue=false;
								}
							}
						 } else $continue=false;
						 						 						
				     	 /*check if payment was offline*/
				     	 $is_offline_paid=false;
			      	 	 if ( $package_info=FunctionsV3::getMerchantPaymentMembership($merchant['merchant_id'],
			      	 	 $merchant['package_id'])){						      	 	 	  	 	
			      	 		$offline_payment=FunctionsV3::getOfflinePaymentList();			      	 		
			      	 		if ( array_key_exists($package_info['payment_type'],(array)$offline_payment)){
			      	 			$is_offline_paid=true;
			      	 		}
			      	 	 }			

			      	 	 if ($is_offline_paid){
			      	 	 	$this->render('merchant-signup-thankyou2',array(
				     	       'data'=>$merchant
				     	    ));		
			      	 	 } else {				   			      	 	   		     						 
					     	 $this->render('merchant-signup-step4',array(
					            'continue'=>$continue
					         ));						
			      	 	 }	 
				     }
					break;	
					
				case "thankyou1":
					 $this->render('merchant-signup-thankyou1',array(
					   'data'=>Yii::app()->functions->getMerchantByToken($_GET['token'])
					 ));		
					break;		
				case "thankyou2":
					$this->render('merchant-signup-thankyou2',array(
					  'data'=>Yii::app()->functions->getMerchantByToken($_GET['token'])
					));		
					break;		
				case "thankyou3":
					$this->render('merchant-signup-thankyou3',array(
					  'data'=>Yii::app()->functions->getMerchantByToken($_GET['token'])
					));		
					break;			
				default:
					//mod
					$this->render('merchant-signup-step2',array(
					  // 'data'=>Yii::app()->functions->getPackagesById($_GET['package_id']),
					  'data'=>Yii::app()->functions->getPackagesById(1),
					  'limit_post'=>Yii::app()->functions->ListlimitedPost(),
					  'terms_merchant'=>getOptionA('website_terms_merchant'),
					  'terms_merchant_url'=>getOptionA('website_terms_merchant_url'),
					  'package_list'=>Yii::app()->functions->getPackagesList(),
					  'kapcha_enabled'=>getOptionA('captcha_merchant_signup')
					));
					// $this->render('merchant-signup',array(
					    // 'list'=>Yii::app()->functions->getPackagesList(),
		               // 'limited_post'=>Yii::app()->functions->ListlimitedPost()
					// ));		
					break;
			}
		} else {
			$disabled_membership_signup=getOptionA('admin_disabled_membership_signup');
			if($disabled_membership_signup==1){				
				$this->render('404-page',array('header'=>true));
			} else {
				//mod
				$this->render('merchant-signup-step2',array(
				  // 'data'=>Yii::app()->functions->getPackagesById($_GET['package_id']),
				  'data'=>Yii::app()->functions->getPackagesById(1),
				  'limit_post'=>Yii::app()->functions->ListlimitedPost(),
				  'terms_merchant'=>getOptionA('website_terms_merchant'),
				  'terms_merchant_url'=>getOptionA('website_terms_merchant_url'),
				  'package_list'=>Yii::app()->functions->getPackagesList(),
				  'kapcha_enabled'=>getOptionA('captcha_merchant_signup')
				));		
				// $this->render('merchant-signup',array(
			      // 'list'=>Yii::app()->functions->getPackagesList(),
			      // 'limited_post'=>Yii::app()->functions->ListlimitedPost()
			    // ));						
			}
		}
	}
	
	public function actionAbout()
	{
		$this->render('index');
	}
	
	public function actionContact()
	{
		
		$act_menu=FunctionsV3::getTopMenuActivated();
		if (!in_array('contact',(array)$act_menu)){
			$this->render('404-page',array('header'=>true));
			return ;
		}	
		
		$seo_title=Yii::app()->functions->getOptionAdmin('seo_contact');
		$seo_meta=Yii::app()->functions->getOptionAdmin('seo_contact_meta');
		$seo_key=Yii::app()->functions->getOptionAdmin('seo_contact_keywords');
		
		if (!empty($seo_title)){
			$seo_title=smarty('website_title',getWebsiteName(),$seo_title);
		    $this->pageTitle=$seo_title;
		    Yii::app()->functions->setSEO($seo_title,$seo_meta,$seo_key);
		}
		
		$website_map_location=array(
		  'map_latitude'=>getOptionA('map_latitude'),
		  'map_longitude'=>getOptionA('map_longitude'),
		);
				
		$address=getOptionA('website_address');		
		
		if (empty($website_map_location['map_latitude'])){		
			if ($lat_res=Yii::app()->functions->geodecodeAddress($address)){				
				$website_map_location['map_latitude']=$lat_res['lat'];
				$website_map_location['map_longitude']=$lat_res['long'];
	    	} 
		}
						
		$cs = Yii::app()->getClientScript();
		$cs->registerScript(
		  'website_location',
		  'var website_location = '.json_encode($website_map_location).'
		  ',
		  CClientScript::POS_HEAD
		);
			
		$this->render('contact',array(
		  'address'=>$address,
		  'website_title'=>getOptionA('website_title'),
		  'contact_phone'=>getOptionA('website_contact_phone'),
		  'contact_email'=>getOptionA('website_contact_email'),
		  'contact_content'=>getOptionA('contact_content'),
		  'country'=>Yii::app()->functions->adminCountry()		  
		));
	}
	
	public function actionMenu()
	{		
				
		$data=$_GET;		
		$current_merchant='';
		if (isset($_SESSION['kr_merchant_id'])){
			$current_merchant=$_SESSION['kr_merchant_id'];
		}

		$res=FunctionsV3::getMerchantBySlug($data['merchant']);
		
		if (is_array($res) && count($res)>=1){
			// if ( $current_merchant !=$res['merchant_id']){							 
				 // unset($_SESSION['kr_item']);
			// }		
			
			if ( $res['status']=="active"){
								
				/*SEO*/
				$seo_title=Yii::app()->functions->getOptionAdmin('seo_menu');
				$seo_meta=Yii::app()->functions->getOptionAdmin('seo_menu_meta');
				$seo_key=Yii::app()->functions->getOptionAdmin('seo_menu_keywords');
				
				if (!empty($seo_title)){
					$seo_title=smarty('website_title',getWebsiteName(),$seo_title);
					$seo_title=smarty('merchant_name',ucwords($res['merchant_name']),$seo_title);		    
				    $this->pageTitle=$seo_title;
				    
				    $seo_meta=smarty('merchant_name',ucwords($res['merchant_name']),$seo_meta);
				    $seo_key=smarty('merchant_name',ucwords($res['merchant_name']),$seo_key);		    
				    
				    Yii::app()->functions->setSEO($seo_title,$seo_meta,$seo_key);
				}
				/*END SEO*/
				
				unset($_SESSION['guest_client_id']);
				
				$merchant_id=$res['merchant_id'];				
				
				/*SET TIME*/
				$mt_timezone=Yii::app()->functions->getOption("merchant_timezone",$merchant_id);				
		    	if (!empty($mt_timezone)){       	 	
		    		Yii::app()->timeZone=$mt_timezone;
		    	}
		    	
		    	
		    	$distance_type='';
		    	$distance='';
		    	$merchant_delivery_distance='';
		    	$delivery_fee=0;
		    			    			    	
		    	/*double check if session has value else use cookie*/		    	
		    	FunctionsV3::cookieLocation();
					    		    	
		    	if (isset($_SESSION['client_location'])){
		    		
		    		/*get the distance from client address to merchant Address*/             
	                 $distance_type=FunctionsV3::getMerchantDistanceType($merchant_id); 
	                 $distance_type_orig=$distance_type;
	                 
		             $distance=FunctionsV3::getDistanceBetweenPlot(
		                $_SESSION['client_location']['lat'],
		                $_SESSION['client_location']['long'],
		                $res['latitude'],$res['longitude'],$distance_type
		             );           
		             		            		 
		             $distance_type_raw = $distance_type=="M"?"miles":"kilometers";            		            
		             $distance_type=$distance_type=="M"?t("miles"):t("kilometers");
		             $distance_type_orig = $distance_type;
		             
		              if(!empty(FunctionsV3::$distance_type_result)){
		             	$distance_type_raw=FunctionsV3::$distance_type_result;
		             	$distance_type=t(FunctionsV3::$distance_type_result);
		             }
		             
		             $merchant_delivery_distance=getOption($merchant_id,'merchant_delivery_miles');             
		             		             
		             $delivery_fee=FunctionsV3::getMerchantDeliveryFee(
		                          $merchant_id,
		                          $res['delivery_charges'],
		                          $distance,
		                          $distance_type_raw);
		    		
		    	}			
		    			    		
		    	
		    	/*SESSION REF*/
		    	$_SESSION['kr_merchant_id']=$merchant_id;
                $_SESSION['kr_merchant_slug']=$data['merchant'];
		    	$_SESSION['shipping_fee']=$delivery_fee;		
		    			    	

		    	
		    	$photo_enabled=getOption($merchant_id,'gallery_disabled')=="yes"?false:true;
		    	if ( getOptionA('theme_photos_tab')==2){
		    		$photo_enabled=false;
		    	}
						    
				$this->render('menu' ,array(
				   'data'=>$res,
				   'merchant_id'=>$merchant_id,
				   'distance_type'=>t("kilometers"),//$distance_type,
				   'distance_type_orig'=>$distance_type_orig,
				   'distance'=>$distance,
				   'merchant_delivery_distance'=>$merchant_delivery_distance,
				   'delivery_fee'=>$delivery_fee,
				   'disabled_addcart'=>getOption($merchant_id,'merchant_disabled_ordering'),
				   'merchant_website'=>getOption($merchant_id,'merchant_extenal'),
				   'photo_enabled'=>$photo_enabled,
				   'tc'=>getOptionA('theme_menu_colapse'),
				   'theme_promo_tab'=>getOptionA('theme_promo_tab'),
				   'theme_hours_tab'=>getOptionA('theme_hours_tab'),
				   'theme_reviews_tab'=>getOptionA('theme_reviews_tab'),
				   'theme_map_tab'=>getOptionA('theme_map_tab'),
				   'theme_info_tab'=>getOptionA('theme_info_tab'),
				   'theme_photos_tab'=>getOptionA('theme_photos_tab')
				));	
								
			}  else  $this->render('error',array(
		       'message'=>t("Sorry but this merchant is no longer available")
		    ));
			
		} else $this->render('error',array(
		  'message'=>t("merchant is not available")
		));
	}
	
	
	public function actionCart()
	{
				
		$data=$_GET;		
		$current_merchant='';
		if (isset($_SESSION['kr_merchant_id'])){
			$current_merchant=$_SESSION['kr_merchant_id'];
		}

		$res=FunctionsV3::getMerchantBySlug($data['merchant']);
		
		if (is_array($res) && count($res)>=1){
			// if ( $current_merchant !=$res['merchant_id']){							 
				 // unset($_SESSION['kr_item']);
			// }		
			
			if ( $res['status']=="active"){
								
				/*SEO*/
				$seo_title=Yii::app()->functions->getOptionAdmin('seo_menu');
				$seo_meta=Yii::app()->functions->getOptionAdmin('seo_menu_meta');
				$seo_key=Yii::app()->functions->getOptionAdmin('seo_menu_keywords');
				
				if (!empty($seo_title)){
					$seo_title=smarty('website_title',getWebsiteName(),$seo_title);
					$seo_title=smarty('merchant_name',ucwords($res['merchant_name']),$seo_title);		    
				    $this->pageTitle=$seo_title;
				    
				    $seo_meta=smarty('merchant_name',ucwords($res['merchant_name']),$seo_meta);
				    $seo_key=smarty('merchant_name',ucwords($res['merchant_name']),$seo_key);		    
				    
				    Yii::app()->functions->setSEO($seo_title,$seo_meta,$seo_key);
				}
				/*END SEO*/
				
				unset($_SESSION['guest_client_id']);
				
				$merchant_id=$res['merchant_id'];				
				
				/*SET TIME*/
				$mt_timezone=Yii::app()->functions->getOption("merchant_timezone",$merchant_id);				
		    	if (!empty($mt_timezone)){       	 	
		    		Yii::app()->timeZone=$mt_timezone;
		    	}
		    	
		    	
		    	$distance_type='';
		    	$distance='';
		    	$merchant_delivery_distance='';
		    	$delivery_fee=0;
		    			    			    	
		    	/*double check if session has value else use cookie*/		    	
		    	FunctionsV3::cookieLocation();
					    		    	
		    	if (isset($_SESSION['client_location'])){
		    		
		    		/*get the distance from client address to merchant Address*/             
	                 $distance_type=FunctionsV3::getMerchantDistanceType($merchant_id); 
	                 $distance_type_orig=$distance_type;
	                 
		             $distance=FunctionsV3::getDistanceBetweenPlot(
		                $_SESSION['client_location']['lat'],
		                $_SESSION['client_location']['long'],
		                $res['latitude'],$res['longitude'],$distance_type
		             );           
		             		            		 
		             $distance_type_raw = $distance_type=="M"?"miles":"kilometers";            		            
		             $distance_type=$distance_type=="M"?t("miles"):t("kilometers");
		             $distance_type_orig = $distance_type;
		             
		              if(!empty(FunctionsV3::$distance_type_result)){
		             	$distance_type_raw=FunctionsV3::$distance_type_result;
		             	$distance_type=t(FunctionsV3::$distance_type_result);
		             }
		             
		             $merchant_delivery_distance=getOption($merchant_id,'merchant_delivery_miles');             
		             		             
		             $delivery_fee=FunctionsV3::getMerchantDeliveryFee(
		                          $merchant_id,
		                          $res['delivery_charges'],
		                          $distance,
		                          $distance_type_raw);
		    		
		    	}			
		    			    		
		    	
		    	/*SESSION REF*/
		    	$_SESSION['kr_merchant_id']=$merchant_id;
                $_SESSION['kr_merchant_slug']=$data['merchant'];
		    	$_SESSION['shipping_fee']=$delivery_fee;		
		    			    	

		    	
		    	$photo_enabled=getOption($merchant_id,'gallery_disabled')=="yes"?false:true;
		    	if ( getOptionA('theme_photos_tab')==2){
		    		$photo_enabled=false;
		    	}
						    
				$this->render('cart' ,array(
				   'data'=>$res,
				   'merchant_id'=>$merchant_id,
				   'distance_type'=>$distance_type,
				   'distance_type_orig'=>$distance_type_orig,
				   'distance'=>$distance,
				   'merchant_delivery_distance'=>$merchant_delivery_distance,
				   'delivery_fee'=>$delivery_fee,
				   'disabled_addcart'=>getOption($merchant_id,'merchant_disabled_ordering'),
				   'merchant_website'=>getOption($merchant_id,'merchant_extenal'),
				   'photo_enabled'=>$photo_enabled,
				   'tc'=>getOptionA('theme_menu_colapse'),
				   'theme_promo_tab'=>getOptionA('theme_promo_tab'),
				   'theme_hours_tab'=>getOptionA('theme_hours_tab'),
				   'theme_reviews_tab'=>getOptionA('theme_reviews_tab'),
				   'theme_map_tab'=>getOptionA('theme_map_tab'),
				   'theme_info_tab'=>getOptionA('theme_info_tab'),
				   'theme_photos_tab'=>getOptionA('theme_photos_tab')
				));	
								
			}  else  $this->render('error',array(
		       'message'=>t("Sorry but this merchant is no longer available")
		    ));
			
		} else $this->render('error',array(
		  'message'=>t("merchant is not available")
		));
	}
	public function actionCheckout()
	{
		if ( Yii::app()->functions->isClientLogin()){	       
 	       $this->redirect(Yii::app()->createUrl('/store/PaymentOption')); 
 	       die();
        }
        
        $cs = Yii::app()->getClientScript();
		$baseUrl = Yii::app()->baseUrl; 
		$cs->registerScriptFile($baseUrl."/assets/js/fblogin.js?ver=1"); 
		    
		if (Yii::app()->functions->isClientLogin()){
			$this->redirect(Yii::app()->createUrl('/store')); 
			die();
		}

		$_SESSION['google_http_refferer']=websiteUrl()."/store/paymentOption";
		
		$seo_title=Yii::app()->functions->getOptionAdmin('seo_checkout');
		$seo_meta=Yii::app()->functions->getOptionAdmin('seo_checkout_meta');
		$seo_key=Yii::app()->functions->getOptionAdmin('seo_checkout_keywords');
		
		$current_merchant='';
		if (isset($_SESSION['kr_merchant_id'])){
			$current_merchant=$_SESSION['kr_merchant_id'];
		}
											               		
		if (!empty($seo_title)){
		   $seo_title=smarty('website_title',getWebsiteName(),$seo_title);
		   if ( $info=Yii::app()->functions->getMerchant($current_merchant)){        	
		   	   $seo_title=smarty('merchant_name',ucwords($info['merchant_name']),$seo_title);
           }		   
		   $this->pageTitle=$seo_title;
		   Yii::app()->functions->setSEO($seo_title,$seo_meta,$seo_key);
		}
		
		$fb=1;
		$fb_app_id=getOptionA('fb_app_id');
		$fb_flag=getOptionA('fb_flag');
		
		if ( $fb_flag=="" && $fb_app_id<>""){
			$fb=2;
		}
		
		$this->render('checkout',array(
		   'terms_customer'=>getOptionA('website_terms_customer'),
		   'terms_customer_url'=>Yii::app()->functions->prettyLink(getOptionA('website_terms_customer_url')),
		   'disabled_guest_checkout'=>getOptionA('website_disabled_guest_checkout'),
		   'enabled_mobile_verification'=>getOptionA('website_enabled_mobile_verification'),
		   'fb_flag'=>$fb,
		   'google_login_enabled'=>getOptionA('google_login_enabled'),
		   'captcha_customer_login'=>getOptionA('captcha_customer_login'),
		   'captcha_customer_signup'=>getOptionA('captcha_customer_signup')
		));
	}
	
	public function actionPaymentOption()
	{	
		
		
 	    $seo_title=Yii::app()->functions->getOptionAdmin('seo_checkout');
		$seo_meta=Yii::app()->functions->getOptionAdmin('seo_checkout_meta');
		$seo_key=Yii::app()->functions->getOptionAdmin('seo_checkout_keywords');
		
		$current_merchant='';
		if (isset($_SESSION['kr_merchant_id'])){
			$current_merchant=$_SESSION['kr_merchant_id'];
		}
		
		if (!empty($seo_title)){
		   $seo_title=smarty('website_title',getWebsiteName(),$seo_title);
		   if ( $info=Yii::app()->functions->getMerchant($current_merchant)){        	
		   	   $seo_title=smarty('merchant_name',ucwords($info['merchant_name']),$seo_title);
           }		   
		   $this->pageTitle=$seo_title;		   
		   Yii::app()->functions->setSEO($seo_title,$seo_meta,$seo_key);
		}
		$this->render('payment-option',array(
		  'website_enabled_map_address'=>getOptionA('website_enabled_map_address'),
		  'address_book'=>Yii::app()->functions->showAddressBook()
		));
	}
	
	public function actionReceipt()
	{
		$this->render('receipt');
	}
	
	public function actionLogout()
	{
		unset($_SESSION['kr_client']);
		$http_referer=$_SERVER['HTTP_REFERER'];				
		if (preg_match("/receipt/i", $http_referer)) {
			$http_referer=websiteUrl()."/store";
		}		
		if (preg_match("/orderHistory/i", $http_referer)) {
			$http_referer=websiteUrl()."/store";
		}		
		if (preg_match("/Profile/i", $http_referer)) {
			$http_referer=websiteUrl()."/store";
		}		
		if (preg_match("/Cards/i", $http_referer)) {
			$http_referer=websiteUrl()."/store";
		}		
		if (preg_match("/PaymentOption/i", $http_referer)) {
			$http_referer=websiteUrl()."/store";
		}		
		if (preg_match("/verification/i", $http_referer)) {
			$http_referer=websiteUrl()."/store";
		}		
		if ( !empty($http_referer)){
			header("Location: ".$http_referer);
		} else header("Location: ".Yii::app()->request->baseUrl);		
	}
	
	public function actionOrderHistory()
	{
		$this->render('order-history');
	}
	
	public function actionProfile()
	{
		if (Yii::app()->functions->isClientLogin()){		   
		   $this->render('profile',array(
		     'tabs'=>isset($_GET['tab'])?$_GET['tab']:'',
		     'disabled_cc'=>getOptionA('disabled_cc_management'),
		     'info'=>Yii::app()->functions->getClientInfo( Yii::app()->functions->getClientId()),
		     'avatar'=>FunctionsV3::getAvatar( Yii::app()->functions->getClientId() )
		   ));
		} else $this->render('404-page',array(
		   'header'=>true
		));
	}
	
	
	
	public function actionforgotPassword()
	{
		if ($res=Yii::app()->functions->getLostPassToken($_GET['token']) ){
			$this->render('forgot-pass');
		} else $this->render('error',array('message'=>t("ERROR: Invalid token.")));
	}
	
	public function actionPage()
	{
		$_GET=array_flip($_GET);   
        $slug=$_GET[''];
        if ($data=yii::app()->functions->getCustomPageBySlug($slug)){
        	
            /*SET SEO META*/
			if (!empty($data['seo_title'])){
			     $this->pageTitle=ucwords($data['seo_title']);
			     Yii::app()->clientScript->registerMetaTag($data['seo_title'], 'title'); 
			}
			if (!empty($data['meta_description'])){   
			     Yii::app()->clientScript->registerMetaTag($data['meta_description'], 'description'); 
			}
			if (!empty($data['meta_keywords'])){   
			     Yii::app()->clientScript->registerMetaTag($data['meta_keywords'], 'keywords'); 
			}
        	
        	$this->render('custom-page',array(
        	  'data'=>$data
        	));
        } else {
        	$this->render('404-page',array('header'=>true));
        }
	}
	
	public function actionSetlanguage()
	{		
		if (isset($_GET['Id'])){			
			Yii::app()->request->cookies['kr_lang_id'] = new CHttpCookie('kr_lang_id', $_GET['Id']);			
			//$_COOKIE['kr_lang_id']=$_GET['Id'];
			/*dump($_COOKIE);
			die();*/
			if (!empty($_SERVER['HTTP_REFERER'])){
					header('Location: '.$_SERVER['HTTP_REFERER']);
					die();
		    } else {
		    	header('Location: '.Yii::app()->request->baseUrl);
		    	die();
		    }
		}
		header('Location: '.Yii::app()->request->baseUrl);
	}
	

	public function actionBrowse()
	{		
		$act_menu=FunctionsV3::getTopMenuActivated();
		if (!in_array('browse',(array)$act_menu)){
			$this->render('404-page',array('header'=>true));
			return ;
		}
		
        /*update merchant if expired and sponsored*/
		Yii::app()->functions->updateMerchantSponsored();
		
		
		if (!isset($_GET['tab'])){
			$_GET['tab']='';
		}
		$tabs=1;
		$list=Yii::app()->functions->getAllMerchant();		

		$country_list=Yii::app()->functions->CountryList();
		$country=getOptionA('merchant_default_country');  
		if (array_key_exists($country,(array)$country_list)){
			$country_name = $country_list[$country];
		} else $country_name="United states";
						
    	// if ($lat_res=Yii::app()->functions->geodecodeAddress($country_name)){    		
    		// $lat_res=array(
    		  // 'lat'=>$lat_res['lat'],
    		  // 'lng'=>$lat_res['long'],
    		// );
    	// } else {
    		// $lat_res=array();
    	// } 
    	
    	$cs = Yii::app()->getClientScript();
    	$cs->registerScript(
		  'country_coordinates',
		  'var country_coordinates = '.json_encode($lat_res).'
		  ',
		  CClientScript::POS_HEAD
		);

		$this->render('browse-resto',array(
		  'list'=>$list,
		  'tabs'=>$tabs
		));

	}
	

	public function actionConfirmorder()
	{
		$data=$_GET;		
		if (isset($data['id']) && isset($data['token'])){
			$db_ext=new DbExt;
			$stmt="SELECT a.*,
				(
				select activation_token
				from
				{{merchant}}
				where
				merchant_id=a.merchant_id
				) as activation_token
			 FROM
			{{order}} a
			WHERE
			order_id=".Yii::app()->functions->q($data['id'])."
			";
			if ($res=$db_ext->rst($stmt)){
				if ( $res[0]['activation_token']==$data['token']){
					$params=array(
					 'status'=>"received",
					 'date_modified'=>date('c'),
					 'ip_address'=>$_SERVER['REMOTE_ADDR'],
					 'viewed'=>2
					);				
					if ($res[0]['status']=="paid"){
						unset($params['status']);
					}	
					if ( $db_ext->updateData("{{order}}",$params,'order_id',$data['id'])){
						$msg=t("Order Status has been change to received, Thank you!");
						
						if (FunctionsV3::hasModuleAddon("mobileapp")){
							/** Mobile save logs for push notification */
							$new_data['order_id']=$data['id'];
							$new_data['status']='received';
							
					    	Yii::app()->setImport(array(			
							  'application.modules.mobileapp.components.*',
						    ));
					    	AddonMobileApp::savedOrderPushNotification($new_data);	
						}
				    	
				    	/*Now we insert the order history*/	    		
	    				$params_history=array(
	    				  'order_id'=>$data['id'],
	    				  'status'=>'received',
	    				  'remarks'=>'',
	    				  'date_created'=>date('c'),
	    				  'ip_address'=>$_SERVER['REMOTE_ADDR']
	    				);	    				
	    				$db_ext->insertData("{{order_history}}",$params_history);						
						
					} else $msg= t("Failed cannot update order");
				} else $msg= t("Token is invalid or not belong to the merchant");
			}
		} else $msg= t("Missing parameters");
		$this->render('confirm-order',array('data'=>$msg));
	}
	
	public function actionApicheckout()
	{
		$data=$_GET;		
		if (isset($data['token'])){
			$ApiFunctions=new ApiFunctions;		
			if ( $res=$ApiFunctions->getCart($data['token'])){				
				$order='';
				$merchant_id=$res[0]['merchant_id'];
				foreach ($res as $val) {															
					$temp=json_decode($val['raw_order'],true);				
					$temp_1='';
					if(is_array($temp) && count($temp)>=1){						
						$temp_1['row']=$val['id'];
						$temp_1['row_api_id']=$val['id'];
						$temp_1['merchant_id']=$val['merchant_id'];
						$temp_1['currentController']="store";
						foreach ($temp as $key=>$value) {							
							$temp_1[$key]=$value;
						}						
						$order[]=$temp_1;
					}
				}							
				//unset($_SESSION);
				$_SESSION['api_token']=$data['token'];
				$_SESSION['currentController']="store";
				$_SESSION['kr_merchant_id']=$merchant_id;
				$_SESSION['kr_delivery_options']['delivery_type']=$data['delivery_type'];
				$_SESSION['kr_delivery_options']['delivery_date']=$data['delivery_date'];
				$_SESSION['kr_delivery_options']['delivery_time']=$data['delivery_time'];				
				$_SESSION['kr_item']=$order;								
				$redirect=Yii::app()->getBaseUrl(true)."/store/checkout";
				header("Location: ".$redirect);
				$this->render('error',array('message'=>t("Please wait while we redirect you")));
			} else $this->render('error',array('message'=>t("Token not found")));
		} else $this->render('error',array('message'=>t("Token is missing")));
	}
	
	
	public function actionMerchantSignupSelection()
	{
		
		$act_menu=FunctionsV3::getTopMenuActivated();
		if (!in_array('resto_signup',(array)$act_menu)){
			$this->render('404-page',array('header'=>true));
			return ;
		}	

		if ( Yii::app()->functions->getOptionAdmin("merchant_disabled_registration")=="yes"){
			//$this->render('error',array('message'=>t("Sorry but merchant registration is disabled by admin")));
			$this->render('404-page',array('header'=>true));
		} else $this->render('merchant-signup-selection',array(
		  'percent'=>getOptionA('admin_commision_percent'),
		  'commision_type'=>getOptionA('admin_commision_type'),
		  'currency'=>adminCurrencySymbol(),
		  'disabled_membership_signup'=>getOptionA('admin_disabled_membership_signup')
		));		
	}
	
	public function actionMerchantSignupinfo()
	{
		$this->render('merchant-signup-info',array(
		  'terms_merchant'=>getOptionA('website_terms_merchant'),
		  'terms_merchant_url'=>getOptionA('website_terms_merchant_url'),
		  'kapcha_enabled'=>getOptionA('captcha_merchant_signup')
		));
	}
	
	public function actionCancelWithdrawal()
	{
		$this->render('withdrawal-cancel');
	}
	
	
	public function actionVerification()
	{
		$continue=true;
		$msg='';
		$id=Yii::app()->functions->getClientId();
		if (!empty($id)){
			$continue=false;
			$msg=t("Sorry but we cannot find what you are looking for.");
		}
		if ( $continue==true){
			if( $res=Yii::app()->functions->getClientInfo($_GET['id'])){								
				if ( $res['status']=="active"){
					$continue=false;
					$msg=t("Your account is already verified");
				}
			} else {
				$continue=false;
				$msg=t("Sorry but we cannot find what you are looking for.");
			}
		}		
		
		if ( $continue==true){
		   $this->render('mobile-verification');
		} else $this->render('error',array('message'=>$msg));
	}


	
	public function missingAction($action)
	{
		/** Register all scripts here*/
		ScriptManager::RegisterAllJSFile();
		ScriptManager::registerAllCSSFiles();
		$this->render('404-page',array(
		  'header'=>true
		));
	}
	
	public function actionGoogleLogin()
	{
		if (isset($_GET['error'])){
			$this->redirect(Yii::app()->createUrl('/store')); 
		}
			
		$plus = Yii::app()->GoogleApis->serviceFactory('Oauth2');
		$client = Yii::app()->GoogleApis->client;
		Try {
			 if(!isset(Yii::app()->session['auth_token']) 
			  || is_null(Yii::app()->session['auth_token']))
			    // You want to use a persistence layer like the DB for storing this along
			    // with the current user
			    Yii::app()->session['auth_token'] = $client->authenticate();
			  else
			    			  			  			    
			    if (isset($_SESSION['auth_token'])) {			    	 
				    $client->setAccessToken($_SESSION['auth_token']);
				}		    
				
				if (isset($_REQUEST['logout'])) {				   
				   unset($_SESSION['auth_token']);
				   $client->revokeToken();
				}
																								
			    if ( $token=$client->getAccessToken()){			    	
			    	$t=$plus->userinfo->get();			    			    	
			    	if (is_array($t) && count($t)>=1){
				        $func=new FunctionsK();
				        if ( $resp_t=$func->googleRegister($t) ){						        	
				            Yii::app()->functions->clientAutoLogin($t['email'],
				            $resp_t['password'],$resp_t['password']);
				        	unset($_SESSION['auth_token']);
				            $client->revokeToken();		
				            if (isset($_SESSION['google_http_refferer'])){
				                $this->redirect($_SESSION['google_http_refferer']);   	
				            } else $this->redirect(Yii::app()->createUrl('/store')); 
				            
				        	die();
				        	
				        } else echo t("ERROR: Something went wrong");
			    	} else echo t("ERROR: Something went wrong");
			    }  else {
			    	$authUrl = $client->createAuthUrl();			    	
			    }			    
			    if(isset($authUrl)) {
				    print "<a class='login' href='$authUrl'>Connect Me!</a>";
				} else {
				   print "<a class='logout' href='?logout'>Logout</a>";
				}
		} catch(Exception $e) {
			Yii::app()->session['auth_token'] = null;
            throw $e;
		}
	}
		
	public function actionAddressBook()
	{
		 if ( Yii::app()->functions->isClientLogin()){
		 	if (isset($_GET['do'])){		
		 	   $data='';
		 	   if ( isset($_GET['id'])){
		 	   	    $data=Yii::app()->functions->getAddressBookByID($_GET['id']);
		 	   }		 
		       $this->render('address-book-add',array(
		         'data'=>$data
		       ));
		 	} else $this->render('address-book');
		 } else $this->render('error',array('message'=>t("Sorry but we cannot find what you are looking for.")));
	}
	
	public function actionAutoPostAddress()
	{
		$datas='';
		$str=isset($_POST['search'])?$_POST['search']:'';
		$db_ext=new DbExt;
		$stmt="
		SELECT * FROM
		{{zipcode}}
		WHERE
		stree_name LIKE '$str%'		
		AND
		status IN ('publish','published')
		ORDER BY stree_name ASC
		LIMIT 0,16
		";				
		if ( $res=$db_ext->rst($stmt)){
			foreach ($res as $val) {								
				$full=$val['stree_name']."," .$val['area'] .",".$val['city'].",".$val['zipcode'];
				$datas[]=array(				  
				  'value'=>$full,
				  'title'=>$full,
				  'text'=>$full,
				);
			}			
			echo json_encode($datas);
		}
	}
		
	public function actionItem()
	{
		$data=Yii::app()->functions->getItemById($_GET['item_id']);
		$this->layout='mobile_tpl';
		$this->render('item',array(
		   'title'=>"test title",
		   'data'=>$data,
		   'this_data'=>isset($_GET)?$_GET:''
		));
	}
	
	public function actionTy()
	{
		$this->render('ty',array(
		  'verify'=>isset($_GET['verify'])?true:false
		));
	}	
	
	public function actionEmailVerification()
	{
		
		if ( Yii::app()->functions->isClientLogin()){
			$this->redirect(Yii::app()->request->baseUrl."/store/home");
		    Yii::app()->end();
		}
		
		$continue=true; $msg='';
		
		if(!isset($_GET['id'])){
			$_GET['id']='';
		}
		if( $res=Yii::app()->functions->getClientInfo($_GET['id'])){	
			if ( $res['status']=="active"){
				$continue=false;
				$msg=t("Your account is already verified");
			}
		} else {
			$continue=false;
			$msg=t("Sorry but we cannot find what you are looking for.");
		}
		
		if ($continue){
		   $this->render('email-verification',array(
		     'data'=>$res
		   ));
		} else $this->render('error',array('message'=>$msg));
	}
	
		
} /*END CLASS*/
	