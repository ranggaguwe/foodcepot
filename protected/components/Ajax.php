<?php
class Ajax extends AjaxAdmin 
{
	

	public function merchantSignUp2()
	{
					
        /** check if admin has enabled the google captcha*/    	    	
    	if ( getOptionA('captcha_merchant_signup')==2){
    		if ( GoogleCaptcha::checkCredentials()){
    			if ( !GoogleCaptcha::validateCaptcha()){
    				$this->msg=GoogleCaptcha::$message;
    				return false;
    			}	    		
    		}	    	
    	} 
	    	
		$status=Yii::app()->functions->getOptionAdmin('merchant_sigup_status');			
		$token=md5($this->data['merchant_name'].date('c'));
		
		$percent=Yii::app()->functions->getOptionAdmin('admin_commision_percent');
		
	    $params=array(
	      'merchant_name'=>$this->data['merchant_name'],
	      'merchant_phone'=>$this->data['merchant_phone'],
	      'contact_name'=>$this->data['contact_name'],
	      'contact_phone'=>$this->data['contact_phone'],
	      'contact_email'=>$this->data['contact_email'],
	      'street'=>$this->data['street'],
	      'city'=>$this->data['city'],
	      'post_code'=>$this->data['post_code'],
	      // 'cuisine'=>json_encode($this->data['cuisine']),
	      'username'=>$this->data['username'],
	      'password'=>md5($this->data['password']),
	      'status'=>$status,
	      'date_created'=>date('c'),
	      'ip_address'=>$_SERVER['REMOTE_ADDR'],
	      'activation_token'=>$token,
	      'activation_key'=>Yii::app()->functions->generateRandomKey(5),
	      'merchant_slug'=>Yii::app()->functions->createSlug($this->data['merchant_name']),	      
	      'payment_steps'=>3,	      
	      'country_code'=>"ID",//$this->data['country_code'],
	      'state'=>$this->data['state'],
	      'is_commission'=>2,
	      'percent_commision'=>$percent,	      
	      'abn'=>isset($this->data['abn'])?$this->data['abn']:''
	    );			
	    
	    if ( !Yii::app()->functions->validateUsername($this->data['username']) ){
	    	
	    	if ($respck=Yii::app()->functions->validateMerchantUserFromMerchantUser($params['username'],
	    	    $params['contact_email'])){
	    		$this->msg=$respck;
	    		return ;		    		
	    	}		    
	    		    		    	
		    if ($this->insertData("{{merchant}}",$params)){
		    	$this->code=1;
		    	$this->msg=Yii::t("default","Successful");
		    	$this->details=$token;
		    	
		    	// send email activation key
		    	$tpl=EmailTPL::merchantActivationCode($params);
	            $sender=Yii::app()->functions->getOptionAdmin('website_contact_email');
	            $to=$this->data['contact_email'];		    		  
	            if (!sendEmail($to,$sender,"Merchant Registration",$tpl)){		    	
	            	//$this->details="failed";
	            } //else $this->details="ok mail";
	            			    				    				    	
		    } else $this->msg=Yii::t("default","Sorry but we cannot add your information. Please try again later");
	    } else $this->msg=Yii::t("default","Sorry but your username is alread been taken.");
	}
	
	public function getMerchantBalance()
	{
		$mtid=Yii::app()->functions->getMerchantID();	
		
		$balance=displayPrice(adminCurrencySymbol(),
		normalPrettyPrice(Yii::app()->functions->getMerchantBalance($mtid)));
			
		$this->details=$balance;
		$this->code=1;
		$this->msg="ok";
	}
	
	public function removeNotice()
	{
		$mtid=Yii::app()->functions->getMerchantID();		
		Yii::app()->functions->updateOption("merchant_read_notice","1",$mtid);
		$this->code=1;
	}

	public function withdrawalSettings()
	{
		/*Yii::app()->functions->updateOptionAdmin("wd_minimum_amount",
	    isset($this->data['wd_minimum_amount'])?$this->data['wd_minimum_amount']:'');*/
		
		Yii::app()->functions->updateOptionAdmin("wd_paypal_minimum",
	    isset($this->data['wd_paypal_minimum'])?$this->data['wd_paypal_minimum']:'');
	    
	    Yii::app()->functions->updateOptionAdmin("wd_bank_minimum",
	    isset($this->data['wd_bank_minimum'])?$this->data['wd_bank_minimum']:'');
	    
	    Yii::app()->functions->updateOptionAdmin("wd_days_process",
	    isset($this->data['wd_days_process'])?$this->data['wd_days_process']:'');
	    
	    Yii::app()->functions->updateOptionAdmin("wd_paypal",
	    isset($this->data['wd_paypal'])?$this->data['wd_paypal']:'');
	    
	    Yii::app()->functions->updateOptionAdmin("wd_paypal_mode",
	    isset($this->data['wd_paypal_mode'])?$this->data['wd_paypal_mode']:'');
	    
	    Yii::app()->functions->updateOptionAdmin("wd_paypal_mode_user",
	    isset($this->data['wd_paypal_mode_user'])?$this->data['wd_paypal_mode_user']:'');
	    
	    Yii::app()->functions->updateOptionAdmin("wd_paypal_mode_pass",
	    isset($this->data['wd_paypal_mode_pass'])?$this->data['wd_paypal_mode_pass']:'');
	    
	    Yii::app()->functions->updateOptionAdmin("wd_paypal_mode_signature",
	    isset($this->data['wd_paypal_mode_signature'])?$this->data['wd_paypal_mode_signature']:'');
	    
	    
	    Yii::app()->functions->updateOptionAdmin("wd_bank_deposit",
	    isset($this->data['wd_bank_deposit'])?$this->data['wd_bank_deposit']:'');
	    

	    Yii::app()->functions->updateOptionAdmin("wd_enabled_paypal",
	    isset($this->data['wd_enabled_paypal'])?$this->data['wd_enabled_paypal']:'');
	    
	    Yii::app()->functions->updateOptionAdmin("wd_payout_disabled",
	    isset($this->data['wd_payout_disabled'])?$this->data['wd_payout_disabled']:'');
	    	    
	    Yii::app()->functions->updateOptionAdmin("wd_payout_notification",
	    isset($this->data['wd_payout_notification'])?$this->data['wd_payout_notification']:'');
	    

	    Yii::app()->functions->updateOptionAdmin("wd_bank_fields",
	    isset($this->data['wd_bank_fields'])?$this->data['wd_bank_fields']:'');
	    
	    $this->code=1;
	    $this->msg=t("Successful");
	}
	
	public function requestPayout()
	{
		
		$mtid=Yii::app()->functions->getMerchantID();
		
 		$wd_paypal_minimum=yii::app()->functions->getOptionAdmin('wd_paypal_minimum');
        $wd_bank_minimum=yii::app()->functions->getOptionAdmin('wd_bank_minimum');
        
        $wd_paypal_minimum=standardPrettyFormat($wd_paypal_minimum);
        $wd_bank_minimum=standardPrettyFormat($wd_bank_minimum);
        
        $current_balance=Yii::app()->functions->getMerchantBalance($mtid);
        $this->data['current_balance']=$current_balance;

		$validator=new Validator;
		
		$req=array(
		  'payment_type'=>t("Payment type is required"),
		  'payment_method'=>t("Payment Method is required"),
		);								
		

		if ( $this->data['payment_type']=="single"){
			if ( $this->data['minimum_amount']>$this->data['amount']){
			   $validator->msg[]=t("Sorry but minimum amount is")." ".displayPrice(baseCurrency(),$this->data['minimum_amount']);
			}				
			if ( $current_balance<$this->data['amount']){		    	
		       $validator->msg[]=t("Amount is greater than your balance");
		    }
		} elseif ( $this->data['payment_type']=="all-earnings"){						
			$this->data['amount']=$current_balance;
			if ( $this->data['minimum_amount']>$this->data['amount']){
			   $validator->msg[]=t("Sorry but minimum amount is")." ".displayPrice(baseCurrency(),$this->data['minimum_amount']);
			}				
			if ( $this->data['minimum_amount']>$current_balance){
			   $validator->msg[]=t("Sorry but minimum amount is")." ".displayPrice(baseCurrency(),$this->data['minimum_amount']);
			}		
		} else {
			
		}
												
		$validator->required($req,$this->data);
											
		if ( $validator->validate()){
			if ( $this->data['payment_type']=="single"){															
				/*if  ( $wd_paypal_minimum>$this->data['amount']){
					$this->msg=t("Sorry but minimum amount is")." ".displayPrice(baseCurrency(),$wd_paypal_minimum);
				} else {*/
					$resp=Yii::app()->functions->payoutRequest($this->data['payment_method'],$this->data);
					if ($resp){
						$this->details=$resp['id'];
						$this->code=1;
						$this->msg=t("Successful");
																	
					} else $this->msg=t("ERROR: Something went wrong");
				//}			
			} else {
				//echo 'all earning';
				$resp=Yii::app()->functions->payoutRequest($this->data['payment_method'],$this->data);
				if ($resp){
					$this->details=$resp['id'];
					$this->code=1;
					$this->msg=t("Successful");											
			   } else $this->msg=t("ERROR: Something went wrong");
			}
		} else $this->msg=$validator->getErrorAsHTML();
				
		if ( $this->code==1){
            
            if ( isset($this->data['default_account_bank'])){
            	if ( $this->data['default_account_bank']==2){
            		$bank_info=array(
            		  'swift_code'=>isset($this->data['swift_code'])?$this->data['swift_code']:'',
            		  'bank_account_number'=>isset($this->data['bank_account_number'])?$this->data['bank_account_number']:'' ,
            		  'account_name'=>isset($this->data['account_name'])?$this->data['account_name']:'',
            		  'bank_account_number'=>isset($this->data['bank_account_number'])?$this->data['bank_account_number']:'',
            		  'swift_code'=>isset($this->data['swift_code'])?$this->data['swift_code']:'',
            		  'bank_name'=>isset($this->data['bank_name'])?$this->data['bank_name']:'',
            		  'bank_branch'=>isset($this->data['bank_branch'])?$this->data['bank_branch']:''
            		);
            		Yii::app()->functions->updateOption("merchant_payout_bank_account",
            		json_encode($bank_info),$mtid);
            	}            
            }		
            
            // send email
            $merchant_email='';
			$tpl='';
			
			$wd_days_process=Yii::app()->functions->getOptionAdmin("wd_days_process");		
			if (empty($wd_days_process)){
    		    $wd_days_process=5;
    	    }
			$cancel_date=$wd_days_process-2;
	        $cancel_date=date("F d Y", strtotime (" +$cancel_date days"));
	        $process_date=date("F d Y", strtotime (" +$wd_days_process days"));
	        
			if ( $merchant_info=Yii::app()->functions->getMerchant($mtid)){			
				$merchant_email=$merchant_info['contact_email'];
				$cancel_link=websiteUrl()."/store/cancelwithdrawal/id/".$resp['token'];
				$tpl=yii::app()->functions->getOptionAdmin('wd_template_payout');
			    $tpl=smarty("merchant-name",$merchant_email['merchant_name'],$tpl);
			    $tpl=smarty("payout-amount",standardPrettyFormat($this->data['amount']),$tpl);
			    $tpl=smarty("payment-method",$this->data['payment_method'],$tpl);
			    $tpl=smarty("account",$this->data['account'],$tpl);
			    $tpl=smarty("cancel-date",$cancel_date,$tpl);
			    $tpl=smarty("cancel-link",$cancel_link,$tpl);
			    $tpl=smarty("process-date",$process_date,$tpl);
			}		
										
			if (!empty($tpl)){
				$wd_template_payout_subject=yii::app()->functions->getOptionAdmin('wd_template_payout_subject');
                if (empty($wd_template_payout_subject)){
	                $wd_template_payout_subject=t("Your Request for Withdrawal was Received");
                }                
				sendEmail($merchant_email,'',$wd_template_payout_subject,$tpl);
			}            
		} 
	}	

	public function incomingWithdrawals()
	{		
		$show_action=true;
		$and="WHERE status IN ('pending')";		
		if (isset($this->data['w-list'])){
			switch ($this->data['w-list']) {
				case "cancel":
					$and="WHERE status IN ('cancel')";
					$show_action=false;
					break;
				case "paid":
					$and="WHERE status IN ('paid')";
					$show_action=true;
					break;
				case "denied":					
					$and="WHERE status IN ('denied')";
					$show_action=false;
					break;

				case "approved":    
				    $and="WHERE status IN ('approved')";
					$show_action=true;
					break;
				case "all":    
				    $and="";
				    $show_action=false;
				    break;
				default:
					break;
			}
		}
		
		if (isset($this->data['start_date']) && isset($this->data['end_date'])){
			if (!empty($this->data['start_date']) && !empty($this->data['end_date'])){
				if (!empty($and)){				
					$and.=" AND date_created BETWEEN  '".$this->data['start_date']." 00:00:00' AND 
	    		        '".$this->data['end_date']." 23:59:00'
	    		    ";
				} else {
					$and.=" WHERE date_created BETWEEN  '".$this->data['start_date']." 00:00:00' AND 
	    		        '".$this->data['end_date']." 23:59:00'
	    		    ";
				}
			}
		}	
		
		if (isset($this->data['merchant_id'])){
			if (!empty($this->data['merchant_id'])){
				if (!empty($and)){
					$and.=" AND merchant_id='".$this->data['merchant_id']."'";
				} else {
					$and=" WHERE merchant_id='".$this->data['merchant_id']."'";
				}
			}
		}	
		
		$slug=$this->data['slug'];
        $stmt="
		SELECT a.*,
		(
		select merchant_name 
		from
		{{merchant}}
		where
		merchant_id=a.merchant_id
		) as merchant_name
		 FROM
		{{withdrawal}} a		
		$and
		ORDER BY withdrawal_id DESC
		";
        
        if (isset($_GET['debug'])){
           dump($this->data);
           dump($stmt);
        }
        
        $_SESSION['kr_export_stmt']=$stmt;
        
		$connection=Yii::app()->db;
	    $rows=$connection->createCommand($stmt)->queryAll();     	    
	    if (is_array($rows) && count($rows)>=1){
	    	foreach ($rows as $val) {    	 
	    		$method=t("Paypal to")." ".$val['account'];
	    		if ( $val['payment_method']=="bank"){
	    			$method=t("Bank to")." ".$val['bank_account_number'];
	    		}	    	
	    		
	    		if ( $this->data['w-list']=="paid" || $this->data['w-list']=="denied"){
	    		} else {
	    		$action="<a href=\"javascript:;\" class=\"payout_action\" data-id=\"$val[withdrawal_id]\" data-status=\"approved\">".t("approved")."</a><br/>";
	    		$action.="<a href=\"javascript:;\" class=\"payout_action\" data-id=\"$val[withdrawal_id]\" data-status=\"denied\">".t("denied")."</a><br/>";
				$action.="<a href=\"javascript:;\" class=\"payout_action\" data-id=\"$val[withdrawal_id]\" data-status=\"paid\">".t("paid")."</a>";
	    		}
	    		
	    		$chk="<input type=\"checkbox\" name=\"row[]\" value=\"$val[cook_id]\" class=\"chk_child\" >";   		
	    		$option="<div class=\"options\">
	    		<a href=\"$slug/id/$val[cook_id]\" >".Yii::t("default","Edit")."</a>
	    		<a href=\"javascript:;\" class=\"row_del\" rev=\"$val[cook_id]\" >".Yii::t("default","Delete")."</a>
	    		</div>";
	    		/*$date=date('M d,Y G:i:s',strtotime($val['date_created']));  
	    		$date=Yii::app()->functions->translateDate($date);*/
	    		$date=FormatDateTime($val['date_created']);
	    		
	    		/*$date_created=displayDate($val['date_created']);
	    		$date_to_process=displayDate($val['date_to_process']);*/
	    		$date_created=FormatDateTime($val['date_created'],false);
	    		$date_to_process=FormatDateTime($val['date_to_process'],false);
	    		
	    		$bank_info='';
	    		if ( $val['payment_method']=="bank"){
	    			$bank_info="<br/><a href=\"javascript:;\" data-id=\"$val[withdrawal_id]\" class=\"view-bank-info\">".t("View bank info")."</a>";
	    		}
	    		
	    		$feed_data['aaData'][]=array(
	    		  $val['withdrawal_id'],
	    		  $val['merchant_name'],
	    		  $method.$bank_info,
	    		  displayPrice(adminCurrencySymbol(),normalPrettyPrice($val['amount'])),
	    		  displayPrice(adminCurrencySymbol(),normalPrettyPrice($val['current_balance'])),
	    		  "<span class=\"uk-badge withdrawal-status\">".t($val['status'])."</span>",
	    		  $date_created,
	    		  $date_to_process,	    		  
	    		  $show_action==true?$action:''
	    		);
	    	}
	    	$this->otableOutput($feed_data);
	    }     	    
	    $this->otableNodata();	
	}
	
	public function payoutChangeStatus()
	{	    
	    $validator=new Validator;
	    $req=array(
	     'withdrawal_id'=>t("withdrawal id is required"),
	     'status'=>t("Status is required"),
	    );

	    $validator->required($req,$this->data);
	    if ( $validator->validate()){
			$r = query("SELECT status FROM {{withdrawal}} WHERE withdrawal_id=?",array($this->data['withdrawal_id']));
			if($r[0]['status']=='pending' || $r[0]['status']=='approved') {
				$params=array(
				  'status'=>$this->data['status'],
				  'viewed'=>2
				);
				$DbExt=new DbExt;
				if ( $DbExt->updateData("{{withdrawal}}",$params,'withdrawal_id',$this->data['withdrawal_id'])){
					$this->code=1;
					$this->msg=t("Successful");
				} else $this->msg=t("Failed cannot update records");
			} else $this->msg=t("You cannot change status that is paid or denied");
	    } else $this->msg=$validator->getErrorAsHTML();
	}
	
	public function wdPayoutNotification()
	{	
		$DbExt=new DbExt;
		$stmt="SELECT count(*) as total
		 FROM
		{{withdrawal}}
		WHERE
		status ='pending'
		AND
		viewed='1'
		";
		if ( $res=$DbExt->rst($stmt)){			
			if ( $res[0]['total']>=1){
				$this->code=1;
				$msg=t("There are")." (".$res[0]['total'].") ".t("withdrawals waiting for your approval");
				$this->msg=$msg."<br/><a class=\"white-link\" href=\"".websiteUrl()."/admin/incomingwithdrawal\">".t("Click here to view")."</a>";
				$this->details=$res[0]['total'];
			} else $this->msg="no results";
		} else $this->msg="no results";
	}
	
	public function cancelWithdrawal()
	{
		if ( $res=Yii::app()->functions->getWithdrawalInformationByToken($this->data['id'])){			
			if ($res['status']=="cancel"){				
				$this->msg=t("This withdrawal request has been already cancel");
				return ;
			}		
		}
		
		$DbExt=new DbExt;
		if (isset($this->data['id'])){
			$params=array(
			  'status'=>'cancel',
			  'date_process'=>date('c'),
			  'ip_address'=>date('c')			
			);
			if ($DbExt->updateData("{{withdrawal}}",$params,'withdrawal_token',$this->data['id'])){
				$this->code=1;
				$this->msg=t("Your withdrawal has been cancel");
			} else $this->msg=t("Error cannot cancel withdrawal please contact site admin");
		} else $this->msg=t("Missing id");
	}
	
	public function rptMerchantSalesSummaryReport()
	{
		if(isset($_GET['debug'])){
			dump($this->data);
		}		
		
		unset($_SESSION['rpt_date_range']);
	
		$and='';  
    	if (isset($this->data['start_date']) && isset($this->data['end_date']))	{
    		if (!empty($this->data['start_date']) && !empty($this->data['end_date'])){
    		   $and=" AND date_created BETWEEN  '".$this->data['start_date']." 00:00:00' AND 
    		        '".$this->data['end_date']." 23:59:00'
    		   ";
    		   $_SESSION['rpt_date_range']=array(
    		     'start_date'=>$this->data['start_date'],
    		     'end_date'=>$this->data['end_date']
    		   );
    		}
    	}
    	

    	
    	$where='';
    	if (isset($this->data['merchant_id'])){
    		if (!empty($this->data['merchant_id'])){
    			$where=" WHERE a.merchant_id=".Yii::app()->functions->q($this->data['merchant_id'])." ";
    		}    	
    	}	
    	
        $stmt="
		SELECT a.merchant_id,a.merchant_name,(total_earning-total_withdrawal) as saldo,
		IFNULL(j.total_earning, 0) total_earning,IFNULL(k.total_withdrawal, 0) total_withdrawal
		FROM 
		{{merchant}} a 
		LEFT JOIN 
		(
		select d.merchant_id,sum(d.earning) as total_earning
		from 
		{{order_merchant}} d
		JOIN {{order}} z
		ON z.order_id=d.order_id
		WHERE z.status='successful' 
		$and 
		GROUP BY merchant_id 
		) as j ON j.merchant_id=a.merchant_id 
		
		LEFT JOIN (
		select merchant_id,sum(amount) as total_withdrawal
		from 
		{{withdrawal}} w 
		WHERE status='paid' 
		$and 
		GROUP BY merchant_id 
		) as k ON k.merchant_id=a.merchant_id 
		
		$where
		ORDER BY merchant_name ASC
		";
        if(isset($_GET['debug'])){
        	dump($stmt);
        }
        
       /* (
		select sum(number_guest)
		from
		{{bookingtable}}
		where
		merchant_id=a.merchant_id
		and status='approved'		
		) as total_guest*/
		
        
        $_SESSION['kr_export_stmt']=$stmt;
        
		$connection=Yii::app()->db;
	    $rows=$connection->createCommand($stmt)->queryAll();     	    
	    if (is_array($rows) && count($rows)>=1){
	    	foreach ($rows as $val) {    	     	    			    		
	    		if(isset($_GET['debug'])){
	    		   dump($val);
	    		}
	    		$feed_data['aaData'][]=array(	    		  
				   $val['merchant_id'],
	    		   $val['merchant_name'],
	    		   displayPrice(adminCurrencySymbol(),normalPrettyPrice($val['total_earning']-$val['total_withdrawal'])),
	    		   displayPrice(adminCurrencySymbol(),normalPrettyPrice($val['total_earning'])),
	    		   displayPrice(adminCurrencySymbol(),normalPrettyPrice($val['total_withdrawal'])),
	    		);
	    	}
	    	$this->otableOutput($feed_data);
	    }     	    
	    $this->otableNodata();	
	}
	
	public function testEmail()
	{
		require_once 'test-email.php';
		die();
	}
	
	public function sendTestEmail()
	{		
		if (isset($this->data['email'])){
			$content="This is a test email";
			if ( Yii::app()->functions->sendEmail($this->data['email'],'',$content,$content)){
				$this->code=1;
				$this->msg=t("Successful");
			} else $this->msg=t("Sending of email has failed");		
		}  else $this->msg=t("Email is required");
	}
	




	public function initpaymentprovider()
	{
		$params="?method=".$this->data['payment_opt'];
		$params.="&purchase=".$this->data['purchase'];		
		$params.="&return_url=".$this->data['return_url'];
				
		$FunctionsK=new FunctionsK;		
		$merchantinfo=Yii::app()->functions->getMerchantInfo();
				
		switch ($this->data['purchase']) {
			case "fax_package":	
					
			    $resp=$FunctionsK->getFaxPackagesById($this->data['fax_package_id']);			    
			    if (!$resp){
			    	$this->msg=t("Package information not found");
			    	return ;
			    }		
			    
			    if ($resp['promo_price']>=1){
					$package_price.=$resp['promo_price'];
			    } else $package_price=$resp['price'];
			   
			    $credit=$resp['fax_limit'];
			    
			    if ($this->data['payment_opt']=="pyp" || $this->data['payment_opt']=="stp"){
					$params.="&package_id=".$this->data['fax_package_id'];				
					//if ( $resp=$FunctionsK->getFaxPackagesById($this->data['fax_package_id'])){					
					if ($resp){
						$params2='';
						if ($resp['promo_price']>=1){
							$params2.="&price=".$resp['promo_price'];
						} else $params2.="&price=".$resp['price'];
							
						$params2.="&description=".urlencode($resp['title']);		
						$params.="&raw=".base64_encode($params2);
												
						$this->code=1;
						$this->msg=t("Please wait while we redirect you");
						$this->details=websiteUrl()."/merchant/pay/$params";
					} else $this->msg=t("Package information not found");
					
			    } elseif ( $this->data['payment_opt']=="obd"){
			    	
			    	//$merchantinfo=Yii::app()->functions->getMerchantInfo();			    	
			    	if ( is_array($merchantinfo) && count($merchantinfo)>=1){
			    		$merchant_email=$merchantinfo[0]->contact_email;
			    		$ref="FAX_".Yii::app()->functions->generateRandomKey(8);
			 			    		
			    		$params_insert=array(
			    		 'merchant_id'=>Yii::app()->functions->getMerchantID(),
			    		 'fax_package_id'=>$this->data['fax_package_id'],
			    		 'payment_type'=>Yii::app()->functions->paymentCode("bankdeposit"),
			    		 'package_price'=>$package_price,
			    		 'fax_limit'=>$credit,
			    		 'payment_reference'=>$ref,
			    		 'date_created'=>date('c'),
			    		 'ip_address'=>$_SERVER['REMOTE_ADDR']
			    		);		
			    		$bank_info=Yii::app()->functions->getBankDepositInstruction();			    		
			    		$tpl=$bank_info['content'];
			    		$tpl=smarty('amount',displayPrice(baseCurrency(),standardPrettyFormat($package_price)),$tpl);
			    		$tpl=smarty('verify-payment-link',
			    		websiteUrl()."/merchant/faxbankdepositverification/?ref=$ref",$tpl);
			    		
			    		if (sendEmail($merchant_email,$bank_info['sender'],$bank_info['subject'],$tpl)){
			    			if ( $this->insertData("{{fax_package_trans}}",$params_insert)){
			    				$this->details=websiteUrl()."/merchant/faxreceipt/id/".Yii::app()->db->getLastInsertID();
			    				$this->code=1;
$this->msg=t("We have sent bank information instruction to your email")." :$merchant_email";


                                //$FunctionsK=new FunctionsK();
                                $FunctionsK->faxSendNotification((array)$merchantinfo[0],
                                                   $this->data['fax_package_id'],
                                                   Yii::app()->functions->paymentCode("bankdeposit"),
                                                   $package_price);
                                
			    			} else $this->msg=t("Error cannot saved information");
			    		} else $this->msg=t("Failed cannot send bank payment instructions");
			    	} else $this->msg=t("Something went wrong merchant information is empty");
			    } else {
			    	if ($package_price==0){
			    		// Free package
			    		$params_insert=array(
			    		 'merchant_id'=>Yii::app()->functions->getMerchantID(),
			    		 'fax_package_id'=>$this->data['fax_package_id'],
			    		 'payment_type'=>'Free',
			    		 'package_price'=>$package_price,
			    		 'fax_limit'=>$credit,
			    		 'payment_reference'=>'',
			    		 'date_created'=>date('c'),
			    		 'ip_address'=>$_SERVER['REMOTE_ADDR'],
			    		 'status'=>"paid"
			    		);					    		
			    		if ( $this->insertData("{{fax_package_trans}}",$params_insert)){
			    			$this->details=websiteUrl()."/merchant/faxreceipt/id/".Yii::app()->db->getLastInsertID();
			    			$this->code=1;
                            $this->msg=t("Successful");                            
                            $FunctionsK->faxSendNotification((array)$merchantinfo[0],
                                                   $this->data['fax_package_id'],
                                                   "Free",
                                                   $package_price);
			    		} else $this->msg=t("Error cannot saved information");
			    	} else $this->msg=t("No payment options has been selected");
			    }
				break;
		
			default:
				$this->msg=t("No found instructions");
				break;
		}
	}
	
	public function updateMerchantUserProfile()
	{		
		$params=array(
		  'first_name'=>$this->data['first_name'],
		  'last_name'=>$this->data['last_name'],
		  'contact_email'=>$this->data['contact_email'],
		  'username'=>$this->data['username'],
		  'date_modified'=>date('c')
		);
		if (!empty($this->data['password'])){
			$params['password']=md5($this->data['password']);
		}
		
		if ($this->updateData("{{merchant_user}}",$params,'merchant_user_id',$this->data['merchant_user_id'])){
			$this->code=1;
			$this->msg=t("Profile successfully updated");
		} else $this->msg=t("ERROR: cannot update");
	}
	


	public function viewBankInfo()
	{
		require_once 'bank-info.php';
		die();
	}
	
	public function merchantList()
	{
		
		$slug=$this->data['slug'];
		
		$aColumns = array(
		  'merchant_id','merchant_name','street','city','country_code','contact_phone',
		  'activation_token','status'
		);
		
		$sWhere=''; $sOrder=''; $sLimit='';
		
		$sTable = "{{merchant}}";
		
		$functionk=new FunctionsK();
		$t=$functionk->ajaxDataTables($aColumns);
		if (is_array($t) && count($t)>=1){
			$sWhere=$t['sWhere'];
			$sOrder=$t['sOrder'];
			$sLimit=$t['sLimit'];
		}	
		$stmt = "
			SELECT SQL_CALC_FOUND_ROWS 
			a.*,
			(
			select title
			from
			{{packages}}
			where
			package_id = a.package_id
			limit 0,1
			) as package_name
			
			FROM $sTable a
			$sWhere
			$sOrder
			$sLimit
		";
		$class="";
		if (isset($_GET['debug'])){
		   dump($stmt);
		}
		if ( $res=$this->rst($stmt)){		
			
			$iTotalRecords=0;
			$stmt2="SELECT FOUND_ROWS()";
			if ( $res2=$this->rst($stmt2)){
				//dump($res2);
				$iTotalRecords=$res2[0]['FOUND_ROWS()'];
			}	
						
			$feed_data['sEcho']=intval($_GET['sEcho']);
			$feed_data['iTotalRecords']=$iTotalRecords;
			$feed_data['iTotalDisplayRecords']=$iTotalRecords;
			
			foreach ($res as $val) {	
					/*$date=date('M d,Y G:i:s',strtotime($val['date_created']));
					$date=Yii::app()->functions->translateDate($date);*/
					$date=FormatDateTime($val['date_created']);
					
					$action="<div class=\"options\">
    	    		<a href=\"$slug/id/$val[merchant_id]\" >".Yii::t("default","Edit")."</a>
    	    		<a href=\"javascript:;\" class=\"row_del\" rev=\"$val[merchant_id]\" >".Yii::t("default","Delete")."</a>
    	    		</div>";
					
					$val['package_name']=isset($val['package_name'])?$val['package_name']:'';
					
					if ($val['status']=="expired"){
					   $class='uk-badge-danger';
					} elseif ( $val['status']=="pending"){
						$class='';
					} elseif ($val['status']=="active"){
						$class='uk-badge-success';
					}				
					$membershipdate=FormatDateTime($val['membership_expired'],false);
					$membershipdate=Yii::app()->functions->translateDate($membershipdate);					
					
					$url_login=baseUrl()."/merchant/autologin/id/".$val['merchant_id']."/token/".$val['password'];
					$link_login='<br/><br/>
					<a target="_blank" href="'.$url_login.'"><div class="uk-badge">'.t("AutoLogin").'</div></a>
					';
					
					$aa_access=Yii::app()->functions->AAccess();
					if (!in_array('autologin',(array)$aa_access)){
						$link_login='';						
					}
					
					if (getOptionA('home_search_mode')!="postcode"){
						$feed_data['aaData'][]=array(
						  $val['merchant_id'],stripslashes($val['merchant_name']).$action,
						  $val['street'],
						  $val['city'],
						  $val['country_code'],
						  $val['merchant_phone']." / ".$val['contact_phone'],
						  $val['activation_key'],
						  $date."<br/><div class=\"uk-badge $class\">".strtoupper(Yii::t("default",$val['status']))."</div>".$link_login
						);
					} else {
						$feed_data['aaData'][]=array(
						  $val['merchant_id'],stripslashes($val['merchant_name']).$action,
						  $val['street'],						  
						  $val['post_code'],
						  $val['merchant_phone']." / ".$val['contact_phone'],
						  $val['activation_key'],
						  $date."<br/><div class=\"uk-badge $class\">".strtoupper(Yii::t("default",$val['status']))."</div>".$link_login
					);
					}
		
				}										
						
			$this->otableOutput($feed_data);	
		}
	    $this->otableNodata();
	}
	
	
	
	public function verifyMobileCode()
	{		
		if( $res=Yii::app()->functions->getClientInfo($this->data['client_id'])){
			if ( $this->data['code']==$res['mobile_verification_code']){
				$this->code=1;
				$this->msg=t("Successful");
				
				$params=array( 
				  'status'=>"active",
				  'mobile_verification_date'=>date('c')
				);
				$this->updateData("{{client}}",$params,'client_id',$res['client_id']);
				
				Yii::app()->functions->clientAutoLogin($res['email_address'],$res['password'],$res['password']);
				
			} else $this->msg=t("Verification code is invalid");		
		} else $this->msg=t("Sorry but we cannot find your records");
	}
	
	public function resendMobileCode()
	{
		$date_now=date('Y-m-d g:i:s a');				
		if ( isset($_SESSION['resend_code'])){			
			$date_diff=Yii::app()->functions->dateDifference($_SESSION['resend_code'],$date_now);			
			if (is_array($date_diff) && count($date_diff)>=1){
				if ( $date_diff['minutes']<5){
					$remaining=5-$date_diff['minutes'];
					$this->msg=t("Please wait for a minute to receive your code");					
					$this->msg.=" (".$remaining ." "."minutes".")";
					return ;
				}			
			}		
		}	
				
		if ( isset($this->data['id'])){
			if( $res=Yii::app()->functions->getClientInfo($this->data['id'])){				
				$code=$res['mobile_verification_code'];
				$_SESSION['resend_code']=$date_now;
				Yii::app()->functions->sendVerificationCode($res['contact_phone'],$code);
				$this->code=1;
				$this->msg=t("Your verification code has been sent to")." ".$res['contact_phone'];
			} else $this->msg=t("Sorry but we cannot find your records");
		} else $this->msg=t("Missing id");
	}
		
	public function getAllMerchantCoordinates()
	{		
		$admin_country_set=Yii::app()->functions->getOptionAdmin('admin_country_set');
		
		$this->qry("SET SQL_BIG_SELECTS=1");
		
		$stmt="
		SELECT merchant_id,
		merchant_slug,merchant_name,latitude,longitude,
		concat(street,' ',city,' ',state,' ',post_code) as address
		FROM
		{{view_merchant}}
		WHERE
		status in ('active')
		AND latitude <>''
		AND longitude <>''		
		ORDER BY latitude ASC
		";
		if ( $res=$this->rst($stmt)){
			$list='';
			$x=0;
			foreach ($res as $val) {					
				$photo=Yii::app()->functions->getOption("merchant_photo",$val['merchant_id']);
				if (empty($photo)){
					$photo='thumbnail-medium.png';
				}
				
				$logo='<a href="'.websiteUrl()."/store/menu/merchant/".$val['merchant_slug'].'">';
                $logo.='<img title="" alt="" src="'.uploadURL()."/$photo".'" class="uk-thumbnail uk-thumbnail-mini">';
				$logo.='</a>';
				
							
				$list[]=array(
				  $val['merchant_name'],
				  $val['latitude'],
				  $val['longitude'],
				  $x,
				  $val['address'],
				  $val['merchant_slug'],
				  $logo,
				);
				$x++;
			}			
			
			$lng='';
			$lng='';
			$country=Yii::app()->functions->getAdminCountrySet();			
			if( $lat_res=Yii::app()->functions->geodecodeAddress($country)){				
				$lat=$lat_res['lat'];
				$lng=$lat_res['long'];
			}		
			
			$this->code=1;
			$this->msg=array(
			  'lat'=>$lat,
			  'lng'=>$lng,
			);
			$this->details=$list;
		} else $this->msg=t("0 restaurant found");
	}
	
	public function findGeo()
	{
		$home_search_unit_type=Yii::app()->functions->getOptionAdmin('home_search_unit_type');
		$home_search_radius=Yii::app()->functions->getOptionAdmin('home_search_radius');
		
		if(!is_numeric($home_search_radius)){
			$home_search_radius=15;
		}	
				
		$distance_exp=3959;
		if ($home_search_unit_type=="km"){
			$distance_exp=6371;
		}		
		
		$lat=isset($this->data['lat'])?$this->data['lat']:0;
		$long=isset($this->data['lng'])?$this->data['lng']:0;
						
		if ($lat_res=Yii::app()->functions->geodecodeAddress($this->data['geo_address'])){			
			$lat=$lat_res['lat'];
			$long=$lat_res['long'];
		}		
						
		if (isset($this->data['geo_address'])){
			$stmt="
			SELECT 
			SQL_CALC_FOUND_ROWS a.*, ( $distance_exp * acos( cos( radians($lat) ) * cos( radians( latitude ) ) 
			* cos( radians( longitude ) - radians($long) ) 
			+ sin( radians($lat) ) * sin( radians( latitude ) ) ) ) 
			AS distance								
			
			FROM {{view_merchant}} a 
			HAVING distance < $home_search_radius	
			AND status='active' AND is_ready='2' 		
			";		
			//dump($stmt);
			if ( $res=$this->rst($stmt)){
				$list='';
			    $x=0;
				foreach ($res as $val) {
					$address=$val['street']." ".$val['city']." ".$val['state']." ".$val['post_code'];
					
					
					$photo=Yii::app()->functions->getOption("merchant_photo",$val['merchant_id']);
				    if (empty($photo)){
					   $photo='thumbnail-medium.png';
				    }
				
				    $logo='<a href="'.websiteUrl()."/store/menu/merchant/".$val['merchant_slug'].'">';
                    $logo.='<img title="" alt="" src="'.uploadURL()."/$photo".'" class="uk-thumbnail uk-thumbnail-mini">';
				    $logo.='</a>';
					
					$list[]=array(
					  $val['merchant_name'],
					  $val['latitude'],
					  $val['longitude'],
					  $x,
					  $address,
					  $val['merchant_slug'],
					  $logo,
					);
				    $x++;
				}				
				$this->code=1;
			    $this->msg=array(
			      'lat'=>$lat,
			      'lng'=>$long
			    );
			    $this->details=$list;
			} else $this->msg=t("No results");
		} else $this->msg=t("Missing parameters");
	}
	
     

     
     public function addressBook()
     {     	
 	    $slug=createUrl("store/profile/?tab=2");
		$stmt="SELECT id,location_name,country_code,as_default,
		concat(street,' ',city,' ',state,' ',zipcode) as address		
		FROM
		{{address_book}}		
		WHERE
		client_id ='".Yii::app()->functions->getClientId()."'	
		ORDER BY id DESC
		";						
		if ($res=$this->rst($stmt)){
		   foreach ($res as $val) {				   	    			   	    
				$action="<div class=\"options\">
	    		<a href=\"$slug&do=add&id=$val[id]\" ><i class=\"ion-ios-compose-outline\"></i></a>
	    		<a href=\"javascript:;\" class=\"row_del\" rev=\"$val[id]\" ><i class=\"ion-ios-trash\"></i></a>
	    		</div>";		   	   
		   	   $feed_data['aaData'][]=array(
		   	      $val['address'].$action,
		   	      $val['location_name'],
		   	      $val['as_default']==2?'<i class="fa fa-check"></i>':'<i class="fa fa-times"></i>'
		   	   );			       
		   }
		   $this->otableOutput($feed_data);
		}
		$this->otableNodata();			
     }
     
     public function addAddressBook()
     {     	
     	$params=array(
     	  'client_id'=>Yii::app()->functions->getClientId(),
     	  'street'=>$this->data['street'],
     	  'city'=>$this->data['city'],
     	  'state'=>$this->data['state'],
     	  'zipcode'=>$this->data['zipcode'],
     	  'location_name'=>isset($this->data['location_name'])?$this->data['location_name']:'',
     	  'as_default'=>isset($this->data['as_default'])?$this->data['as_default']:1,
     	  'date_created'=>date('c'),
     	  'ip_address'=>$_SERVER['REMOTE_ADDR'],
     	  'country_code'=>$this->data['country_code']
     	);     	
     	
     	if (!isset($this->data['as_default'])){
     		$this->data['as_default']='';
     	}
     	     	
     	if ( $this->data['as_default']==2){
     		$sql_up="UPDATE {{address_book}}
     		SET as_default='1' 	     		
     		WHERE
     		client_id='".Yii::app()->functions->getClientId()."'
     		";
     		$this->qry($sql_up);
     	}     
     	
     	if ( isset($this->data['id'])){
     		unset($params['date_created']);
     		$params['date_modified']=date('c');
     		if ( $this->updateData("{{address_book}}",$params,'id',$this->data['id'])){
     			$this->code=1;
     			$this->msg=Yii::t("default","Successful");		 
     		} else $this->msg=t("ERROR: Something went wrong");	
     	} else {
     	    if ( $this->insertData('{{address_book}}',$params)){
	        	$this->details=Yii::app()->db->getLastInsertID();
	    		$this->code=1;
	    		$this->msg=Yii::t("default","Successful");		 
	        } else $this->msg=t("ERROR: Something went wrong");		
     	}
     }
     
	public function adminCustomerReviews()
	{		
		$slug=$this->data['slug'];
		$stmt="SELECT a.*,
		(
		select concat(first_name,' ',last_name)
		from {{client}}
		where
		client_id=a.client_id
		) client_name,
		
		(
		select merchant_name
		from
		{{merchant}}
		where
		merchant_id=a.merchant_id
		) as merchant_name
		
		 FROM
		{{review}} a			
		ORDER BY id DESC
		";					
		$class= "";
		if ($res=$this->rst($stmt)){
		   foreach ($res as $val) {				   	    			   	    
				$action="<div class=\"options\">
	    		<a href=\"$slug/Do/Add/?id=$val[id]\" >".Yii::t("default","Edit")."</a>
	    		<a href=\"javascript:;\" class=\"row_del\" rev=\"$val[id]\" >".Yii::t("default","Delete")."</a>
	    		</div>";		   	  
				
				if ( $this->data['currentController']=="admin"){						
				} else {					
					if ( Yii::app()->functions->getOptionAdmin('merchant_can_edit_reviews')=="yes"){
						$action='';
					}
				}
				
			   /*$date=Yii::app()->functions->prettyDate($val['date_created']);	
			   $date=Yii::app()->functions->translateDate($date); */
			   $date=FormatDateTime($val['date_created']);
			   
		   	   $feed_data['aaData'][]=array(
		   	     $val['id'],
		   	      $val['merchant_name'].$action,
		   	      $val['client_name'],
		   	      $val['review'],
		   	      /*$val['order_id'],*/
		   	      $val['rating'],
		   	      $date."<br/><div class=\"uk-badge $class\">".strtoupper(Yii::t("default",$val['status']))."</div>"
		   	   );			       
		   }
		   $this->otableOutput($feed_data);
		}
		$this->otableNodata();	
	}     
	
	public function AdminUpdateCustomerReviews()
	{
		$db_ext=new DbExt;			
		if (isset($this->data['id'])){
			$params=array(
			  'review'=>$this->data['review'],
			  'status'=>$this->data['status'],
			  'rating'=>$this->data['rating'],
			  'ip_address'=>$_SERVER['REMOTE_ADDR']
			);
			if ($db_ext->updateData("{{review}}",$params,'id',$this->data['id'])){
				$this->code=1;
				$this->msg=Yii::t("default","Successful");
			} else $this->msg=Yii::t("default","ERROR: cannot update");
		} else $this->msg="";		
	}	
	
	public function clearCart()
	{
		unset($_SESSION['kr_item']);
		$this->code=1;
		$this->msg="OK";
	}
	
	public function UpdateItemAvailable()
	{		
		if (isset($this->data['item_id'])){
			$params=array('not_available'=>$this->data['checked']==1?2:1);
			$db_ext=new DbExt;
			if ( $db_ext->updateData("{{item}}",$params,'item_id',$this->data['item_id'])){
				$this->code=1;
				$this->msg=t("Successful");
			} else $this->msg=t("ERROR: cannot update records.");
		} else $this->msg=t("Missing parameters");
	}
	
	
	public function getCommissionTotal()
	{
		$total_com=displayPrice(adminCurrencySymbol(),
		normalPrettyPrice(Yii::app()->functions->getTotalCommission()));
		
		$total_today=displayPrice(adminCurrencySymbol(),
		normalPrettyPrice(Yii::app()->functions->getTotalCommissionToday()));
				
		$total_last=displayPrice(adminCurrencySymbol(),
		normalPrettyPrice(Yii::app()->functions->getTotalCommissionLast()));
		
		$commission=array(
		  'total_com'=>$total_com,
		  'total_today'=>$total_today,
		  'total_last'=>$total_last
		);
		$this->code=1;
		$this->msg="Ok";
		$this->details=$commission;
	}
	
	public function switchMerchantAccount()
	{		
		if (!isset($this->data['iagree'])){
			$this->msg=t("You must agree to switch your account to commission");
			return ;
		}	
		
		$params=array(
		  'is_commission'=>2,
		  'percent_commision'=>getOptionA('admin_commision_percent'),
		  'commision_type'=>getOptionA('admin_commision_type')
		);		
		$merchant_id=Yii::app()->functions->getMerchantID();		
		$db_ext=new DbExt;
		if ( $db_ext->updateData("{{merchant}}",$params,'merchant_id',$merchant_id)){
			$this->code=1; 
			$this->msg=t("You have successfully switch your account to commission");
			$this->msg.="<br/>";
			$this->msg.=t("Please not you might have to relogin again to see the balance");
			$this->details=websiteUrl()."/merchant/dashboard";
		} else $this->msg=t("ERROR: cannot update");
	}

	public function sendUpdateOrderEmail()
	{
		if (empty($this->data['email_order_change_msg'])){
			$this->msg=t("Email content is required");
			return false;
		}	
		if (empty($this->data['subject'])){
			$this->msg=t("Subject is required");
			return false;
		}	
		if ($res=Yii::app()->functions->getOrder($this->data['order_id'])){
			$client_email=$res['email_address'];			
			$content=$this->data['email_order_change_msg'];
			$subject=$this->data['subject'];
			if (Yii::app()->functions->sendEmail($client_email,'',$subject,$content)){
				$this->code=1;
				$this->msg=t("Email sent");
			} else $this->msg=t('ERROR: Cannot sent email.');
		} else $this->msg=t("Sory but we cannot find the order information");
	}
	
	public function viewOrderHistory()
	{		
		?>
		<div class="view-receipt-pop">
	      <h3><?php echo Yii::t("default",'History')?></h3>
	    
	      <?php if ( $resh=FunctionsK::orderHistory($this->data['id'])):?>                    
               <table class="uk-table uk-table-hover">
                 <thead>
                   <tr>
                    <th class="uk-text-muted"><?php echo t("Date/Time")?></th>
                    <th class="uk-text-muted"><?php echo t("Status")?></th>
                    <th class="uk-text-muted"><?php echo t("Remarks")?></th>
                   </tr>
                 </thead>
                 <tbody>
                   <?php foreach ($resh as $valh):?>
                   <tr style="font-size:12px;">
                     <td><?php                       
                      echo FormatDateTime($valh['date_created'],true);
                      ?></td>
                     <td><?php echo t($valh['status'])?></td>
                     <td><?php echo $valh['remarks']?></td>
                   </tr>
                   <?php endforeach;?>
                 </tbody>
               </table> 
          <?php else :?>                
            <p class="uk-text-danger order-order-history show-history-<?php echo $val['order_id']?>">
              <?php echo t("No history found")?>
            </p>
          <?php endif;?>	 
	    	 
	    </div>
		<?php
		Yii::app()->end();
	}
	
	public function getArea()
	{		
		if (isset($this->data['city'])){
		   $stmt="
		   SELECT DISTINCT area
		   FROM
		   {{zipcode}}
		   WHERE
		   city =".q($this->data['city'])."
		   ORDER BY area ASC
		   ";
		   if ( $res=$this->rst($stmt)){
		   	   $this->code=1;
		   	   $this->msg="OK";
		   	   $this->details=$res;
		   } else $this->msg=t("No results");
		} else $this->msg=t("missing city parameters");
	}
	
	public function verifyEmailCode()
	{
		$client_id=isset($this->data['client_id'])?$this->data['client_id']:'';
		if( $res=Yii::app()->functions->getClientInfo( $client_id )){	
			
		    if ($res['email_verification_code']==trim($this->data['code'])){
		    	$this->code=1;
		    	$this->msg=t("Successful");
		    	
		    	$params=array( 
				  'status'=>"active",
				  'last_login'=>date('c')
				);
				$this->updateData("{{client}}",$params,'client_id',$res['client_id']);
				
				Yii::app()->functions->clientAutoLogin($res['email_address'],$res['password'],$res['password']);
				
		    } else $this->msg=t("Verification code is invalid");
		} else $this->msg=t("Sorry but we cannot find your information.");
    }
    
   
			
} /*END CLASS*/