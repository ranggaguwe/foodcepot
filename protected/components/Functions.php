<?php
class Functions extends CApplicationComponent
{	
	public $data;
	
	public $code=2;
	public $msg;
	public $details;
	public $db_ext;
	public $has_session=false;
	public $sms_msg;

	public $search_result_total=0;
	
	public function __construct()
	{
		$this->db_ext=new DbExt; 		
	}
	
	public function isAdminLogin()
	{						
		$is_login=FALSE;				
		/*if (!empty($_COOKIE['kr_user'])){
			$user=json_decode($_COOKIE['kr_user']);									
			if (is_numeric($user[0]->admin_id)){
				$is_login=TRUE;
			}
		}*/				
		if ($this->validateAdminSession()){
			return true;
		}			
		return false;
	}
	
	public function getAdminInfo()
	{
		/*if (!empty($_COOKIE['kr_user'])){
			$user=json_decode($_COOKIE['kr_user']);									
			if (is_numeric($user[0]->admin_id)){
				return $user[0];
			}
		}*/		
		if (!empty($_SESSION['kr_user'])){
			$user=json_decode($_SESSION['kr_user']);									
			if (is_numeric($user[0]->admin_id)){
				return $user[0];
			}
		}
		return false;
	}
	
	public function getAdminId()
	{
		if (!empty($_SESSION['kr_user'])){
			$user=json_decode($_SESSION['kr_user']);									
			if (is_numeric($user[0]->admin_id)){
				return $user[0]->admin_id;
			}
		}
		return false;
	}	
	
	public function isMerchantLogin()
	{						
		$is_login=FALSE;						
		if (!empty($_SESSION['kr_merchant_user'])){
			$user=json_decode($_SESSION['kr_merchant_user']);									
			if (is_numeric($user[0]->merchant_id)){
				$is_login=TRUE;
			}
		}
		if ($is_login){
			return true;
		}
		return false;
	}
	
	public function getMerchantID()
	{
		if (!empty($_SESSION['kr_merchant_user'])){
			$user=json_decode($_SESSION['kr_merchant_user']);			
			if (is_array($user) && count($user)>=1){
				return $user[0]->merchant_id;
			}
		}
		return false;
	}		
	
	public function getMerchantUserName()
	{
		if (!empty($_SESSION['kr_merchant_user'])){
			$user=json_decode($_SESSION['kr_merchant_user'],true);			
			//dump($user);
			if (is_array($user) && count($user)>=1){
				//return ucwords($user[0]->contact_name);
				if (isset($user[0]['first_name'])){
					return $user[0]['first_name'];
				} else return $user[0]['contact_name'];
			}
		}
		return false;
	}
	
	public function getMerchantInfo()
	{
		if (!empty($_SESSION['kr_merchant_user'])){
			$user=json_decode($_SESSION['kr_merchant_user']);			
			if (is_array($user) && count($user)>=1){
				return $user;
			}
		}
		return false;
	}
	
	public function CountryList()
	{
		$cuntry_list=require 'CountryCode.php';  
		return $cuntry_list;
	}
	
	public function Services()
	{
		return array(
		  1=>Yii::t("default","Delivery & Pickup"),
		  2=>Yii::t("default","Delivery Only"),
		  3=>yii::t("default","Pickup Only")
		);
	}
	
	public function DeliveryOptions($merchant_id='')
	{		
		if ( $res=$this->getMerchant($merchant_id)){
			switch ($res['service']) {
				case 2:
					return array(
			           'delivery'=>Yii::t("default","Delivery"),
			        );
					break;
				case 3:
					return array(
			            'pickup'=>Yii::t("default","Pickup")          
			        );
					break;
				default:
					return array(
			           'delivery'=>Yii::t("default","Delivery"),
			           'pickup'=>Yii::t("default","Pickup") 
			        );
					break;
			}
		} else {
			return array(
			  'delivery'=>Yii::t("default","Delivery"),
			  'pickup'=>Yii::t("default","Pickup") 
			);
		}
	}
	
	public function isMerchantExist($contact_email='')
	{
		$DbExt=new DbExt;
		$stmt="SELECT * FROM
		{{merchant}}
		WHERE
		contact_email='".$contact_email."'
		LIMIT 0,1
		";
		if ( $res=$DbExt->rst($stmt)){
			return $res;
		}
		return false;
	}
	
	public function getMerchant($merchant_id='')
	{
		$DbExt=new DbExt;
		$stmt="SELECT a.*,
		(
		select title
		from
		{{packages}}
		where
		package_id=a.package_id
		) as package_name
		 FROM
		{{merchant}} a
		WHERE
		merchant_id='".$merchant_id."'
		LIMIT 0,1
		";
		if ( $res=$DbExt->rst($stmt)){
			return $res[0];
		}
		return false;
	}	
		
	public function getMerchantBySlug($slug_id='')
	{
		$DbExt=new DbExt;
		$stmt="SELECT * FROM
		{{merchant}}
		WHERE
		merchant_slug=".q($slug_id)."
		LIMIT 0,1
		";
		if ( $res=$DbExt->rst($stmt)){
			return $res[0];
		}
		return false;
	}

	public function getProduct($item_id='')
	{
		$DbExt=new DbExt;
		$stmt="SELECT * FROM
		{{item}}
		WHERE
		item_id=".q($item_id)."
		LIMIT 0,1
		";
		if ( $res=$DbExt->rst($stmt)){
			return $res[0];
		}
		return false;
	}
	public function getOne($table,$column,$id)
	{
		$DbExt=new DbExt;
		$stmt="SELECT * FROM
		{{$column}}
		WHERE
		$column='$id'
		LIMIT 0,1
		";
		if ( $res=$DbExt->rst($stmt)){
			return $res[0];
		}
		return false;
	}
	public function getWhere($table,$column,$id)
	{
		$DbExt=new DbExt;
		$stmt="SELECT * FROM
		{{".$table."}}
		WHERE
		$column='$id'
		";
		if ( $res=$DbExt->rst($stmt)){
			return $res;
		}
		return false;
	}
	public function getMerchantByToken($token='')
	{
		if (empty($token)){
			return false;
		}	
		$DbExt=new DbExt;
		$stmt="SELECT a.*,
		(
		select title from
		{{packages}}
		where
		package_id = a.package_id
		) as package_name
		
		FROM
		{{merchant}} a
		WHERE
		activation_token='".$token."'
		LIMIT 0,1
		";
		if ( $res=$DbExt->rst($stmt)){
			return $res[0];
		}
		return false;
	}		
		
	
	public function createSlug($merchant_name='')
	{
		//$slug_id=str_replace(" ","-",$merchant_name);	
		//$slug_id=$this->seo_friendly_url($merchant_name);
		$merchant_name=str_replace("'",'',$merchant_name);
		
		$DbExt=new DbExt;
		$stmt="SELECT count(*) as total FROM
		{{merchant}}
		WHERE
		merchant_name LIKE '%".addslashes($merchant_name)."%'
		LIMIT 0,1
		";				
		if ( $res=$DbExt->rst($stmt)){			
			if ($res[0]['total']==0){
				return $this->seo_friendly_url($merchant_name);
			} else {
				return $this->createSlug($merchant_name.$res[0]['total']);
			}		
		}
		return $this->seo_friendly_url($merchant_name);
	}
		
    public function jsLanguageAdmin()
    {
    	
    	$link="<a href=\"".Yii::app()->request->baseUrl."/merchant/MerchantStatus/"."\">".Yii::t("default","click here to renew membership")."</a>";
    	return array(
    	  "deleteWarning"=>Yii::t("default","You are about to permanently delete the selected items.\n'Cancel' to stop, 'OK' to delete.?"),
    	  "checkRowDelete"=>Yii::t("default","Please check on of the row to delete."),
    	  "removeFeatureImage"=>Yii::t("default","Remove image"),
    	  "removeFiles"=>Yii::t("default","Remove Files"),
    	  "lastTotalSales"=>Yii::t("default","Last 30 days Total Sales"),
    	  "lastItemSales"=>Yii::t("default","Last 30 days Total Sales By Item"),
    	  "NewOrderStatsMsg"=>Yii::t("default","New Order has been placed."),
    	  
    	  'Hour'=>Yii::t("default","Hour"),
    	  'Minute'=>Yii::t("default","Minute"),
    	  'processing'=>Yii::t("default","processing."),
    	  'merchantStats'=>Yii::t("default","Your merchant membership is expired. Please renew your membership.").$link,
    	  "Status"=>Yii::t("default","Status"),
    	  
    	  "tablet_1"=>Yii::t("default","No data available in table"),
    	  "tablet_2"=>Yii::t("default","Showing _START_ to _END_ of _TOTAL_ entries"),
    	  "tablet_3"=>Yii::t("default","Showing 0 to 0 of 0 entries"),
    	  "tablet_4"=>Yii::t("default","(filtered from _MAX_ total entries)"),
    	  "tablet_5"=>Yii::t("default","Show _MENU_ entries"),
    	  "tablet_6"=>Yii::t("default","Loading..."),
    	  "tablet_7"=>Yii::t("default","Processing..."),
    	  "tablet_8"=>Yii::t("default","Search:"),
    	  "tablet_9"=>Yii::t("default","No matching records found"),
    	  "tablet_10"=>Yii::t("default","First"),
    	  "tablet_11"=>Yii::t("default","Last"),
    	  "tablet_12"=>Yii::t("default","Next"),
    	  "tablet_13"=>Yii::t("default","Previous"),
    	  "tablet_14"=>Yii::t("default",": activate to sort column ascending"),
    	  "tablet_15"=>Yii::t("default",": activate to sort column descending"),
    	      	  
    	  "trans_1"=>Yii::t("default","Please rate the restaurant before submitting your review!"),
    	  "trans_2"=>Yii::t("default","Sorry but you can select only"),
    	  "trans_3"=>Yii::t("default","addon"),
    	  "trans_4"=>Yii::t("default","Are you sure?"),
    	  "trans_5"=>Yii::t("default","Sorry but Minimum order is"),
    	  "trans_6"=>Yii::t("default","Please select payment method"),
    	  "trans_7"=>Yii::t("default","Mobile number is required"),
    	  "trans_8"=>Yii::t("default","Please select your credit card"),
    	  "trans_9"=>Yii::t("default","Map not available"),
    	  "trans_10"=>Yii::t("default","Are you sure you want to delete this review? This action cannot be undone."),
    	  "trans_11"=>Yii::t("default","Add your recent order to cart?"),
    	  "trans_12"=>Yii::t("default","Got a total of _TOTAL_ Merchant to show (_START_ to _END_)"),
    	  "trans_13"=>Yii::t("default","Got a total of _TOTAL_ Records to show (_START_ to _END_)"),
    	  "trans_14"=>Yii::t("default","ERROR:"),
    	  "trans_15"=>Yii::t("default","Sorry but this merchant delivers only with in "),
    	  "trans_16"=>Yii::t("default","miles"),
    	  "trans_17"=>Yii::t("default","Notice: Your merchant will not show on search result. Click on Publish your merchant."),
    	  "trans_18"=>Yii::t("default","Continue?"),
    	  "trans_19"=>Yii::t("default","You are about to send SMS to"),
    	  "trans_20"=>Yii::t("default","customer"),
    	  "trans_21"=>Yii::t("default","Browse"),
    	  "trans_22"=>Yii::t("default","Invalid Voucher code"),
    	  "trans_23"=>Yii::t("default","Remove Voucher"),
    	  "trans_24"=>Yii::t("default","Use Voucher"),
    	  "trans_25"=>Yii::t("default","Please enter your origin"),
    	  "trans_26"=>Yii::t("default","Error: Something went wrong"),
    	  "trans_27"=>Yii::t("default","No results found"),
    	  "trans_28"=>Yii::t("default","Geocoder failed due to:"),
    	  "trans_29"=>Yii::t("default","Please select price"),
    	  "trans_30"=>Yii::t("default","Sorry this merchant is closed."),
    	  'Prev'=>Yii::t("default","Prev"),
    	  'Next'=>Yii::t("default","Next"),
    	  'Today'=>Yii::t("default","Today"),
    	  'January'=>Yii::t("default","January"),
    	  'February'=>Yii::t("default","February"),
    	  'March'=>Yii::t("default","March"),
    	  'April'=>Yii::t("default","April"),
    	  'May'=>Yii::t("default","May"),
    	  'June'=>Yii::t("default","June"),
    	  'July'=>Yii::t("default","July"),
    	  'August'=>Yii::t("default","August"),
    	  'September'=>Yii::t("default","September"),
    	  'October'=>Yii::t("default","October"),
    	  'November'=>Yii::t("default","November"),
    	  'December'=>Yii::t("default","December"),
    	  'Jan'=>Yii::t("default","Jan"),
    	  'Feb'=>Yii::t("default","Feb"),
    	  'Mar'=>Yii::t("default","Mar"),
    	  'Apr'=>Yii::t("default","Apr"),
    	  'May'=>Yii::t("default","May"),
    	  'Jun'=>Yii::t("default","Jun"),
    	  'Jul'=>Yii::t("default","Jul"),
    	  'Aug'=>Yii::t("default","Aug"),
    	  'Sep'=>Yii::t("default","Sep"),
    	  'Oct'=>Yii::t("default","Oct"),
    	  'Nov'=>Yii::t("default","Nov"),
    	  'Dec'=>Yii::t("default","Dec"),
    	  'Sun'=>Yii::t("default","Sun"),
    	  'Mon'=>Yii::t("default","Mon"),
    	  'Tue'=>Yii::t("default","Tue"),
    	  'Wed'=>Yii::t("default","Wed"),
    	  'Thu'=>Yii::t("default","Thu"),
    	  'Fri'=>Yii::t("default","Fri"),
    	  'Sat'=>Yii::t("default","Sat"), 	  
    	  'Su'=>Yii::t("default","Su"),
    	  'Mo'=>Yii::t("default","Mo"),
    	  'Tu'=>Yii::t("default","Tu"),
    	  'We'=>Yii::t("default","We"),
    	  'Th'=>Yii::t("default","Th"),
    	  'Fr'=>Yii::t("default","Fr"),
    	  'Sa'=>Yii::t("default","Sa"),
    	  'Hour'=>Yii::t("default","Hour"),
    	  'Minute'=>Yii::t("default","Minute"),
    	  'AM'=>Yii::t("default","AM"),
    	  'PM'=>Yii::t("default","PM"),
    	  "trans_31"=>Yii::t("default","Sorry but Maximum order is"),
    	  "trans_32"=>Yii::t("default","Select Some Options"),
    	  "trans_33"=>Yii::t("default","No results match"),
    	  "trans_34"=>Yii::t("default","New Booking Table"),
    	  "trans_35"=>t("Restaurant name"),
    	  "trans_36"=>t("Address"),
    	  "trans_37"=>t("Order Now"),
    	  "trans_38"=>t("Pickup Time"),
    	  "trans_39"=>t("Delivery Time"),
    	  "trans_40"=>t("Please select payment provider"),
    	  "trans_41"=>t("Pickup Time is required"),
    	  "trans_42"=>t("Pickup Date is required"),
    	  "trans_43"=>t("Delivery Date is required"),
    	  "trans_44"=>t("Delivery Time is required"),
    	  "trans_45"=>t("Tip"),
    	  "trans_46"=>t("You must select price for left and right flavor"),
    	  'trans_47'=>t("You must select at least one addon"),
    	  'trans_48'=>t("Please drag the marker to select your address"),
    	  'trans_49'=>t("You can drag the map marker"),
    	  'trans_50'=>t("Is this address correct"),
    	  'trans_51'=>t("Sorry but this item is not available"),
    	  'trans_52'=>t("Please validate Captcha"),
    	  'trans_53'=>t("SMS code is required"),
    	  'trans_54'=>t("You have to choose driver"),
    	  
    	  'find_restaurant_by_name'=>t("Find restaurant by name"),
    	  'find_restaurant_by_streetname'=>t("Find by street name"),
    	  'find_restaurant_by_cuisine'=>t("Find restaurant by cuisine"),
    	  'find_restaurant_by_food'=>t("Find restaurant by food"),
    	  'read_more'=>t("Read more"),
    	  'close'=>t("Close"),
    	  'close_fullscreen'=>t("Close fullscreen"),
    	  'view_fullscreen'=>t("View in fullscreen"),
    	  'not_authorize'=>t("You are not authorize with this app"),
    	  'not_login_fb'=>t("Sorry but you are not login with facebook"),
    	  'login_succesful'=>t("Login Successful"),
    	  'you_cannot_edit_order'=>t("You cannot edit this order since you have redeem points")
    	);
    }   	
    
    public function jsLanguageValidator()
    {
    	$js_lang=array(
		  'requiredFields'=>Yii::t("default","You have not answered all required fields"),
		  'groupCheckedTooFewStart'=>Yii::t("default","Please choose at least"),
		  'badEmail'=>Yii::t("default","You have not given a correct e-mail address"),
		);
		return $js_lang;
    }
    
    public function getCategory($cat_id='')
    {
    	$DbExt=new DbExt;
	    $stmt="SELECT `cat_id`, `category_name`, `category_description`, `photo`, `status`, `sequence`, `date_created`, `date_modified` FROM
			{{category}}
			WHERE
			cat_id='".$cat_id."'
			ORDER BY cat_id DESC			
		";		
		if ( $res=$DbExt->rst($stmt)){
			return $res[0];
		}
		return false;
    }		
    
    public function getCategory2($cat_id='')
    {
    	$DbExt=new DbExt;
	    $stmt="SELECT `cat_id`, `category_name`, `category_description`, `photo`, `status`, `sequence`, `date_created`, `date_modified` FROM
			{{category}}
			WHERE
			cat_id='".$cat_id."'
			ORDER BY cat_id DESC			
		";		
		if ( $res=$DbExt->rst($stmt)){
			return $res[0];
		}
		return false;
    }		    
    
    public function getCategoryList()
	{
		$data_feed='';
		$stmt="
		SELECT * FROM
		{{category}}
		ORDER BY sequence ASC
		";					
		$connection=Yii::app()->db;
		$rows=$connection->createCommand($stmt)->queryAll(); 				
		if (is_array($rows) && count($rows)>=1){
			if ($this->data=="list"){
				foreach ($rows as $val) {									   
				   $data_feed[$val['cat_id']]=$val['category_name'];
				}
				return $data_feed;
			} else return $rows;
		}
		return FALSE;
	}    
	
	public function getAllCategory()
	{
		$data_feed='';
		$stmt="
		SELECT * FROM
		{{category}}
		ORDER BY sequence ASC
		";		
		$connection=Yii::app()->db;
		$rows=$connection->createCommand($stmt)->queryAll(); 				
		if (is_array($rows) && count($rows)>=1){
			if ($this->data=="list"){
				foreach ($rows as $val) {									   
				   $data_feed[$val['cat_id']]=$val['category_name'];
				}
				return $data_feed;
			} else return $rows;
		}
		return FALSE;
	}
    public function getSize($id='')
    {
    	$DbExt=new DbExt;
	    $stmt="SELECT * FROM
			{{size}}
			WHERE
			size_id='".$id."'
			LIMIT 0,1			
		";		
		if ( $res=$DbExt->rst($stmt)){
			return $res[0];
		}
		return false;
    }		
    
    public function getSizeList($merchant_id)
    {    	
    	$data_feed[]='';
    	$DbExt=new DbExt;
	    $stmt="SELECT * FROM
			{{size}}
			WHERE
			merchant_id='".$merchant_id."'
			ORDER BY sequence ASC			
		";			    
		if ( $res=$DbExt->rst($stmt)){			
			if ($this->data=="list"){
				foreach ($res as $val) {									   
				   $data_feed[$val['size_id']]=$val['size_name'];
				}
				return $data_feed;
			} else return $res;
		}
		return false;
    }		
    
    public function getSizeListAll()
    {    	
    	$data_feed[]='';
    	$DbExt=new DbExt;
	    $stmt="SELECT * FROM
			{{size}}		
			ORDER BY sequence ASC			
		";			    
		if ( $res=$DbExt->rst($stmt)){			
			if ($this->data=="list"){
				foreach ($res as $val) {									   
				   $data_feed[$val['size_id']]=$val['size_name'];
				}
				return $data_feed;
			} else return $res;
		}
		return false;
    }		    
    

    public function getWholeSalePrice($item_id)
    {    	
    	$data_feed[]='';
    	$DbExt=new DbExt;
	    $stmt="SELECT * FROM
			{{wholesale_price}}		
			WHERE item_id='".$item_id."'
			ORDER BY id ASC
		";
		if ( $res=$DbExt->rst($stmt)){
			return $res;
		}
		return false;
    }

	public function merchantMenu()
	{

		return array(  
		    'activeCssClass'=>'active', 
		    'encodeLabel'=>false,
		    'items'=>array(
                array('visible'=>$this->hasMerchantAccess("DashBoard"),'tag'=>"DashBoard",'label'=>'<i class="fa fa-home"></i>'.Yii::t("default","Dashboard"),
                'url'=>array('/merchant/DashBoard')),
                
                array('visible'=>$this->hasMerchantAccess("Merchant"),'tag'=>"Merchant",'label'=>'<i class="fa fa-cutlery"></i>'.Yii::t("default","Merchant Info"),
                'url'=>array('/merchant/Merchant')),
                
                array('visible'=>$this->hasMerchantAccess("Settings"),'tag'=>"Settings",'label'=>'<i class="fa fa-cog"></i>'.Yii::t("default","Settings"),
                'url'=>array('/merchant/Settings')),

                array('visible'=>$this->hasMerchantAccess("FoodItem"),'tag'=>"FoodItem",'label'=>'<i class="fa fa-list-alt"></i>'.Yii::t("default","Food Item"),
                'url'=>array('/merchant/FoodItem')),             

                array('visible'=>$this->hasMerchantAccess("gallerysettings"),'tag'=>"gallerysettings",'label'=>'<i class="fa fa-list-alt"></i>'.Yii::t("default","Gallery Settings"),
                'url'=>array('/merchant/gallerysettings')),

			   array('visible'=>$this->hasMerchantAccess("earnings"),'tag'=>'earnings', 'label'=>'<i class="fa fa-paypal"></i>'.t("Earnings"),
			   'url'=>array('merchant/earnings')),     
			   
			   array('visible'=>$this->hasMerchantAccess("withdrawal"),'tag'=>'withdrawals', 'label'=>'<i class="fa fa-paypal"></i>'.t("Withdrawals"),
			   'url'=>array('merchant/withdrawals')),

			   array('visible'=>$this->hasMerchantAccess("salesReport"),'tag'=>'salesReport','label'=>'<i class="fa fa-paypal"></i>'.Yii::t("default","Sales Report"), 
			   'url'=>array('merchant/salesReport')),
			   
                array('visible'=>$this->hasMerchantAccess("review"),'tag'=>"review",'label'=>'<i class="fa fa-list-alt"></i>'.Yii::t("default","Customer reviews"),
                'url'=>array('/merchant/review')),               
                
                array('visible'=>$this->hasMerchantAccess("SocialSettings"),'tag'=>"SocialSettings",'label'=>'<i class="fa fa-facebook-square"></i>'.Yii::t("default","Social Settings"),
                'url'=>array('/merchant/SocialSettings')),               
                
                array('visible'=>$this->hasMerchantAccess("AlertSettings"),'tag'=>"AlertSettings",'label'=>'<i class="fa fa-bell"></i>'.Yii::t("default","Alert Notification"),
                'url'=>array('/merchant/AlertSettings')),               
                                
                array('visible'=>$this->hasMerchantAccess("user"),'tag'=>"user",'label'=>'<i class="fa fa-users"></i>'.Yii::t("default","User"),
                'url'=>array('/merchant/user')),
                                
                array('tag'=>"logout",'label'=>'<i class="fa fa-sign-out"></i>'.Yii::t("default","Logout"),
                'url'=>array('/merchant/login/logout/true')),
            )
		);
	}	    
	
	public function hasMerchantAccess($tag='')
	{
		/*LIST OF ACCEPTED CONTROLLER NAME IN MERCHANT*/
		$accepted_tag=array(
			'MerchantStatus',
			'purchaseSMScredit',
			'paypalInit',
			'stripeInit',
			'mercadopagoInit',
			'sisowinit',
			'payuinit',
			'obdinit',
			'pysinit',
			'creditCardInit',
			'smsReceipt',
			'Setlanguage',
			'pay',
			'paymentconfirm',
			'faxreceipt',
			'profile'
		);
		if (in_array($tag,$accepted_tag)){
			return true;
		}	
		
		$tag_paymentgateway=array(
		  'cash','cod','credit',
		);
				
		if ( $tag=="obdreceive"){
			$tag='obd';
		}	
		
		if (in_array($tag,$tag_paymentgateway)){			
			$list_payment=$this->getMerchantListOfPaymentGateway();		
		/*	dump($list_payment);
			die();*/				
			if (!in_array($tag,(array)$list_payment)){
				return false;
			}
		}
		
		
		
		
		
		$info=$this->getMerchantInfo();		
		if ( is_array($info) && count($info)>=1){
			$info=(array)$info[0];			
			if (isset($info['merchant_user_id'])){
				$access=json_decode($info['user_access']);																
				if (in_array($tag,(array)$access)){
					return true;
				}			
			} else return true;							    
		}
		return false;		
	}

	public function adminMenu()
	{
		return array(  
		    'activeCssClass'=>'active', 
		    'encodeLabel'=>false,
		    'items'=>array(
                array('visible'=>$this->AA('dashboard'),
                'tag'=>"dashboard",'label'=>'<i class="fa fa-home"></i>'.Yii::t("default","Dashboard"),
                'url'=>array('/admin/dashboard')),                              

                array('visible'=>$this->AA('merchant'),
                'tag'=>"merchant",'label'=>'<i class="fa fa-cutlery"></i>'.Yii::t("default","Merchant List"),
                'url'=>array('/admin/merchant')),
                
				array('visible'=>$this->AA('sponsoredMerchantList'),
                'tag'=>"sponsoredMerchantList",
                'label'=>'<i class="fa fa-list-alt"></i>'.Yii::t("default","Sponsored Listing"),                
                'url'=>array('/admin/sponsoredMerchantList')),
				
                array('visible'=>$this->AA('categoryList'),
                'tag'=>"categoryList",'label'=>'<i class="fa fa-cutlery"></i>'.Yii::t("default","Category"),
                'url'=>array('/admin/categorylist')),

                
                array('visible'=>$this->AA('customerlist'),'tag'=>"customerlist",
                'label'=>'<i class="fa fa-list-alt"></i>'.Yii::t("default","Customer List"),
                'url'=>array('/admin/customerlist')),    
				
                array('visible'=>$this->AA('topuplist'),'tag'=>"topuplist",
                'label'=>'<i class="fa fa-list-alt"></i>'.Yii::t("default","Topup List"),
                'url'=>array('/admin/topuplist')),
				                              
                
            

                
                // array('visible'=>$this->AA('reports'),
                // 'tag'=>'reports','label'=>'<i class="fa fa-list-alt"></i>'.Yii::t("default",'Reports'),
                   // 'itemOptions'=>array('class'=>'has-children'), 'items'=>array(
                   
                   // array('visible'=>$this->AA('rptMerchantReg'),'tag'=>'rptMerchantReg',
                   // 'label'=>'<i class="fa fa-paypal"></i>'.Yii::t("default","Merchant Registration"), 
                   // 'url'=>array('admin/rptMerchantReg')), 
                   
                   
                   // array('visible'=>$this->AA('rptMerchanteSales'),'tag'=>'rptMerchanteSales',
                   // 'label'=>'<i class="fa fa-paypal"></i>'.Yii::t("default","Merchant Sales Report"), 
                   // 'url'=>array('admin/rptMerchanteSales')), 
                   
                   // array('visible'=>$this->AA('rptmerchantsalesummary'),'tag'=>'rptmerchantsalesummary',
                   // 'label'=>'<i class="fa fa-paypal"></i>'.Yii::t("default","Merchant Summary Report"), 
                   // 'url'=>array('admin/rptmerchantsalesummary')), 
                   

                 // )),

                   array('visible'=>$this->AA('rptMerchantReg'),'tag'=>'rptMerchantReg',
                   'label'=>'<i class="fa fa-paypal"></i>'.Yii::t("default","Merchant Registration"), 
                   'url'=>array('admin/rptMerchantReg')), 
                   
                   
                   array('visible'=>$this->AA('rptMerchanteSales'),'tag'=>'rptMerchanteSales',
                   'label'=>'<i class="fa fa-paypal"></i>'.Yii::t("default","Merchant Sales Report"), 
                   'url'=>array('admin/rptMerchanteSales')), 
                   
                   array('visible'=>$this->AA('rptmerchantsalesummary'),'tag'=>'rptmerchantsalesummary',
                   'label'=>'<i class="fa fa-paypal"></i>'.Yii::t("default","Merchant Summary Report"), 
                   'url'=>array('admin/rptmerchantsalesummary')), 
				   
				   
				   
                array('visible'=>$this->AA('settings'),'tag'=>'sitesettings',
                'label'=>'<i class="fa fa-plus-circle"></i>'.Yii::t("default",'Site Settings'),
                   'itemOptions'=>array('class'=>'has-children'), 'items'=>array(

					array('visible'=>$this->AA('settings'),
					'tag'=>"settings",'label'=>'<i class="fa fa-list-alt"></i>'.Yii::t("default","General Settings"),
					'url'=>array('/admin/settings')),       

					array('visible'=>$this->AA('Ratings'),
					'tag'=>"Ratings",'label'=>'<i class="fa fa-star-o"></i>'.Yii::t("default","Ratings"),
					'url'=>array('/admin/Ratings')),                
					
					
					array('visible'=>$this->AA('ManageCurrency'),'tag'=>"ManageCurrency",
					'label'=>'<i class="fa fa-usd"></i>'.Yii::t("default","Manage Currency"),
					'url'=>array('/admin/ManageCurrency')),                
					
					array('visible'=>$this->AA('ManageLanguage'),'tag'=>"ManageLanguage",
					'label'=>'<i class="fa fa-flag-o"></i>'.Yii::t("default","Manage Language"),
					'url'=>array('/admin/ManageLanguage')),

					array('visible'=>$this->AA('SocialSettings'),'tag'=>"SocialSettings",
					'label'=>'<i class="fa fa-list-alt"></i>'.Yii::t("default","Social Settings"),
					'url'=>array('/admin/SocialSettings')),
					
					array('visible'=>$this->AA('emailsettings'),'tag'=>"emailsettings",
					'label'=>'<i class="fa fa-list-alt"></i>'.Yii::t("default","Mail & SMTP Settings"),
					'url'=>array('/admin/emailsettings')),

					
                 )),  				

                array('visible'=>$this->AA('reviews'),'tag'=>"reviews",
                'label'=>'<i class="fa fa-list-alt"></i>'.Yii::t("default","Reviews"),
                'url'=>array('admin/reviews')),                   
				

                array('visible'=>$this->AA('withdrawal'),'tag'=>'withdrawal',
                   'label'=>'<i class="fa fa-university"></i>'.Yii::t("default",'Withdrawal'),
                   'itemOptions'=>array('class'=>'has-children'), 
                   'items'=>array(        
                     array('visible'=>$this->AA('incomingwithdrawal'),'tag'=>'incomingwithdrawal',
                     'label'=>'<i class="fa fa-paypal"></i>'.Yii::t("default","Withdrawal List"), 
                     'url'=>array('admin/incomingwithdrawal')),                                
                     
                      array('visible'=>$this->AA('withdrawalsettings'),'tag'=>'withdrawalsettings',
                      'label'=>'<i class="fa fa-paypal"></i>'.Yii::t("default","Settings"), 
                     'url'=>array('admin/withdrawalsettings')),
                 )),          
				

                
                array('visible'=>$this->AA('customPage'),
                'tag'=>"customPage",'label'=>'<i class="fa fa-list-alt"></i>'.Yii::t("default","Custom Page"),
                'url'=>array('/admin/customPage')),                

                
                /**add ons */
                array('visible'=>$this->AA('addons'),'tag'=>'addons',
                'label'=>'<i class="fa fa-plus-circle"></i>'.Yii::t("default",'Add-ons Apps'),
                   'itemOptions'=>array('class'=>'has-children'), 'items'=>array(
                   
                   array('visible'=>$this->AA('mobileapp'),
                   'tag'=>'mobileapp','label'=>'<i class="fa fa-paypal"></i>'.Yii::t("default","MobileApp"), 
                   'url'=>Yii::app()->getBaseUrl(true)."/mobileapp"),    
				   
                   array('visible'=>$this->AA('merchantapp'),
                   'tag'=>'merchantapp','label'=>'<i class="fa"></i>'.Yii::t("default","MerchantApp"), 
                   'url'=>Yii::app()->getBaseUrl(true)."/merchantapp"),          
              
                 )),  
				array('visible'=>$this->AA('ContactSettings'),'tag'=>"ContactSettings",
				'label'=>'<i class="fa fa-list-alt"></i>'.Yii::t("default","Contact Settings"),
				'url'=>array('/admin/ContactSettings')),                
				

                 array('visible'=>$this->AA('userList'),
                 'tag'=>"userList",'label'=>'<i class="fa fa-users"></i>'.Yii::t("default","User List"),
                'url'=>array('/admin/userList')),                

                array('tag'=>"logout",'label'=>'<i class="fa fa-sign-out"></i>'.Yii::t("default","Logout"),
                'url'=>array('/admin/login/logout/true')),
            )
		);
	}

	public function topMenu()
	{
		$hide=true;
		if ( $this->isClientLogin()){
			$hide=false;
		}
		
		$merchant_disabled_registration=$this->getOptionAdmin('merchant_disabled_registration');		
		$enabled_reg=$merchant_disabled_registration=="yes"?false:true;
		
		$enabled_commission=Yii::app()->functions->getOptionAdmin('admin_commission_enabled');		
		$signup_link="/store/merchantsignup";
		
		$website_disabled_login_popup=Yii::app()->functions->getOptionAdmin('website_disabled_login_popup');
		$link_sigup='javascript:;';
		$link_sigup_class='top_signup';
		if ( $website_disabled_login_popup=="yes"){
			$link_sigup=array('/store/signup');
			$link_sigup_class='';
		}	
		
		$view_map=true;
		if ( getOptionA('view_map_disabled')==2){
			$view_map=false;
		}	
				
		return array(  
		    'activeCssClass'=>'active', 		    
		    'encodeLabel'=>false,		    
		    'items'=>array(
                array('visible'=>$hide,'tag'=>"signup",'label'=>'<i class="fa fa-user"></i>'.Yii::t("default","Login & Signup"),
                'url'=>$link_sigup,'itemOptions'=>array('class'=>$link_sigup_class)),            
                array('visible'=>$enabled_reg,'tag'=>"home",'label'=>'<i class="fa fa-cutlery"></i>'.Yii::t("default","Restaurant Signup"),
                'url'=>array($signup_link)),
                                
                array('tag'=>"home",'label'=>'<i class="fa fa-search"></i>'.Yii::t("default","Browse Restaurant"),
                'url'=>array('/store/browse')),                                
                
                array('visible'=>$view_map, 'tag'=>"home",'label'=>'<i class="fa fa-map-marker"></i>'.Yii::t("default","View Restaurant by map"),
                'url'=>array('/store/map')),                                
             )   
          );
	}
	
    public function topLeftMenu()
	{
		$top_menu[]=array('tag'=>"signup",'label'=>'<i class="fa fa-home"></i>'.Yii::t("default","Home"),
                'url'=>array('/store/home'));

        $top_menu[]=array('tag'=>"home",'label'=>'<i class="fa fa-envelope-o"></i> '.Yii::t("default","Contact"),
                'url'=>array('/store/contact'));
                             
		if ($data=Yii::app()->functions->customPagePosition()){
			foreach ($data as $val) {
				if ($val['is_custom_link']==2){
					if (!preg_match("/http/i", $val['content'])) {
						$val['content']="http://".$val['content'];
					} 
					if ( $val['open_new_tab']==2){
						$top_menu[]=array('tag'=>"home",'label'=>'<i class="'.$val['icons'].'"></i> '.
						Yii::t("default",$val['page_name']),
		                'url'=>$val['content'],
		                'linkOptions'=>array('target'=>"_blank")
		                );
					} else {
						$top_menu[]=array('tag'=>"home",'label'=>'<i class="'.$val['icons'].'"></i> '.
					    Yii::t("default",$val['page_name']),
	                   'url'=>$val['content']);
					}		
				} else {		
					if ( $val['open_new_tab']==2){
						$top_menu[]=array('tag'=>"home",'label'=>'<i class="'.$val['icons'].'"></i>'.
					    Yii::t("default",$val['page_name']),
	                   'url'=>array('/store/page/'.$val['slug_name']),
	                   'linkOptions'=>array('target'=>"_blank"));
					} else {
						$top_menu[]=array('tag'=>"home",'label'=>'<i class="'.$val['icons'].'"></i>'.
						Yii::t("default",$val['page_name']),
		                'url'=>array('/store/page/'.$val['slug_name']));
					}
				}
			}
		}	
		return array(  		    
		    'id'=>"top-menu",
		    'activeCssClass'=>'active', 
		    'encodeLabel'=>false,
		    'items'=>$top_menu                      
          );
	}
	
	public function bottomMenu($position='bottom')
	{
		
	   $menu=array();
       if ($data=Yii::app()->functions->customPagePosition($position)){
			foreach ($data as $val) {
				if ($val['is_custom_link']==2){
					if (!preg_match("/http/i", $val['content'])) {
						$val['content']="http://".$val['content'];
					} 
					if ( $val['open_new_tab']==2){
						$menu[]=array('tag'=>"home",'label'=>'<i class="'.$val['icons'].'"></i> '.
						Yii::t("default",$val['page_name']),
		                'url'=>$val['content'],
		                'linkOptions'=>array('target'=>"_blank")
		                );
					} else {
						$menu[]=array('tag'=>"home",'label'=>'<i class="'.$val['icons'].'"></i> '.
					    Yii::t("default",$val['page_name']),
	                   'url'=>$val['content']);
					}		
				} else {								
					if ( $val['open_new_tab']==2){						
						$menu[]=array('tag'=>"home",'label'=>'<i class="'.$val['icons'].'"></i> '.
						Yii::t("default",$val['page_name']),
		                'url'=>array('/store/page/'.$val['slug_name']),
		                'linkOptions'=>array('target'=>"_blank")
		                );
					} else {												
						$menu[]=array('tag'=>"home",'label'=>'<i class="'.$val['icons'].'"></i> '.
					    Yii::t("default",$val['page_name']),
	                   'url'=>array('/store/page/'.$val['slug_name']));
					}		
				}	
			}
		}
                  		
		return array(  		    
		    'id'=>"bottom-menu",
		    'activeCssClass'=>'active', 
		    'encodeLabel'=>false,
		    'items'=>$menu
          );
	}
	
	public function navMenu()
	{
		return array(  		    
		    'id'=>"nav-menu",
		    'activeCssClass'=>'active', 
		    'encodeLabel'=>false,
		    'items'=>array(
                array('tag'=>"signup",'label'=>Yii::t("default","Home"),
                'url'=>array('/store')),
                
                array('tag'=>"home",'label'=>Yii::t("default","How it works"),
                'url'=>array('/store/about')),
                
                array('tag'=>"home",'label'=>Yii::t("default","Contact"),
                'url'=>array('/store/contact')),
                
             )   
          );
	}
	
	public function socialMenu()
	{
		$social_flag=yii::app()->functions->getOptionAdmin('social_flag');
		$admin_fb_page=yii::app()->functions->getOptionAdmin('admin_fb_page');
		$admin_twitter_page=yii::app()->functions->getOptionAdmin('admin_twitter_page');
		$admin_google_page=yii::app()->functions->getOptionAdmin('admin_google_page');
				
		if ( $social_flag==1){
			return array(  		    		    
		    'activeCssClass'=>'active', 
		    'encodeLabel'=>false
		    );
		}
		
		$fb=true;
		$twiter=true;
		$google=true;
		if (empty($admin_fb_page)){
			$fb=false;
		} else {
			if (!preg_match("/http/i",$admin_fb_page )) {
				$admin_fb_page="http://$admin_fb_page";
			}
		}
	
		if (empty($admin_twitter_page)){
			$twiter=false;
		} else {
			if (!preg_match("/http/i",$admin_twitter_page )) {
				$admin_twitter_page="http://$admin_twitter_page";
			}
		}
			
		if (empty($admin_google_page)){
			$google=false;
		} else {
			if (!preg_match("/http/i",$admin_google_page )) {
				$admin_google_page="http://$admin_google_page";
			}
		}	
						
		return array(  		    		    
		    'activeCssClass'=>'active', 
		    'encodeLabel'=>false,
		    'items'=>array(
                array('visible'=>$fb, 'tag'=>"signup",'label'=>'<i class="fa fa-facebook"></i>&nbsp;',
                'url'=>$admin_fb_page,'linkOptions'=>array('target'=>"_blank")),

                array('visible'=>$twiter,'tag'=>"signup",'label'=>'<i class="fa fa-twitter"></i>&nbsp;',
                'url'=>$admin_twitter_page,'linkOptions'=>array('target'=>"_blank")),
                
                array('visible'=>$google,'tag'=>"signup",'label'=>'<i class="fa fa-google-plus"></i>&nbsp;',
                'url'=>$admin_google_page,'linkOptions'=>array('target'=>"_blank")),
                
             )   
         );
	}
		
	public function getCurrencyCode()
	{								
		return $this->adminCurrencySymbol();
		// currency code define on admin
		/*$DbExt=new DbExt;
		if (is_numeric($merchant_id)){
			$currency_symbol=$this->getOption('merchant_currency',$merchant_id);
			if ( !empty($currency_symbol)){
				$stmt="SELECT * FROM 
				{{currency}}
				WHERE
				currency_code='$currency_symbol'
				LIMIT 0,1
				";				
				if ( $res=$DbExt->rst($stmt)){
					return $res[0]['currency_symbol'];
				}			
			}
		}	*/
		//return "$";
	}
			
	public function getCurrencyDetails($currency_code='')
	{
		$DbExt=new DbExt;
		$stmt="SELECT * FROM
		{{currency}}
		WHERE
		currency_code='$currency_code'
		LIMIT 0,1
		";
		if ($res=$DbExt->rst($stmt)){
			return $res[0];
		}
		return false;	
	}
		
	public function multiOptions()
	{
		return array(
		  'one'=>Yii::t("default","Can Select Only One"),
		  'multiple'=>Yii::t("default","Can Select Multiple"),
		  'custom'=>Yii::t("default","Custom")
		);
	}
	
    public function limitText($text='',$limit=100)
    {
    	if ( !empty($text)){
    		return substr($text,0,$limit)."...";
    	}    
    	return ;    	
    }
    
    public function getFoodItem($item_id='')
    {
    	$DbExt=new DbExt;
	    $stmt="SELECT * FROM
			{{item}}
			
			WHERE
			item_id='".$item_id."'
			LIMIT 0,1
		";		
		if ( $res=$DbExt->rst($stmt)){
			return $res[0];
		}
		return false;
    }
    public function getFoodItem2($item_id='')
    {
    	$merchant_id=$this->getMerchantID();
    	$DbExt=new DbExt;
	    $stmt="SELECT * FROM
			{{item}}
			WHERE
			item_id='".$item_id."'
			AND
			merchant_id ='$merchant_id'
			LIMIT 0,1
		";		
		if ( $res=$DbExt->rst($stmt)){
			return $res[0];
		}
		return false;
    }		           
    
    public function getFoodItemList($merchant_id='')
	{
		$data_feed='';
		$stmt="
		SELECT * FROM
		{{item}}
		WHERE 
		merchant_id='".$merchant_id."'
		ORDER BY sequence ASC
		";				
		$connection=Yii::app()->db;
		$rows=$connection->createCommand($stmt)->queryAll(); 				
		if (is_array($rows) && count($rows)>=1){
			if ($this->data=="list"){
				foreach ($rows as $val) {									   
				   $data_feed[$val['item_id']]=$val['item_name'];
				}
				return $data_feed;
			} else return $rows;
		}
		return FALSE;
	}    
	
    public function getFoodItemLists($merchant_id='')
	{
		$where='';
		if (is_numeric($merchant_id)){
			$where=" WHERE merchant_id=".$this->q($merchant_id)."";
		}			
		
		$data_feed='';
		$stmt="
		SELECT * FROM
		{{item}}	
		$where	
		ORDER BY sequence ASC
		";				
				
		$connection=Yii::app()->db;
		$rows=$connection->createCommand($stmt)->queryAll(); 				
		if (is_array($rows) && count($rows)>=1){
			if ($this->data=="list"){
				foreach ($rows as $val) {									   
				   $data_feed[$val['item_id']]=$val['item_name'];
				}
				return $data_feed;
			} else return $rows;
		}
		return FALSE;
	}    	
    public function getFoodItemLists2($item_ids)
	{
		// $where='';
		// if (is_numeric($merchant_id)){
			$where=" ";
		// }
		
		$data_feed='';
		$stmt="
		SELECT * FROM
		{{item}} 
		WHERE item_id IN(".$item_ids.")	
		ORDER BY sequence ASC
		";

		$connection=Yii::app()->db;
		$rows=$connection->createCommand($stmt)->queryAll(); 				
		if (is_array($rows) && count($rows)>=1){
			if ($this->data=="list"){
				foreach ($rows as $val) {									   
				   $data_feed[$val['item_id']]=$val['item_name'];
				}
				return $data_feed;
			} else return $rows;
		}
		return FALSE;
	}    	
   
    public function updateOption($option_name='',$option_value='',$merchant_id='')
	{
		$and='';
		if ( !empty($merchant_id)){
			$and=" AND merchant_id='".$merchant_id."' ";
		}
		$stmt="SELECT * FROM
		{{option}}
		WHERE
		option_name='".addslashes($option_name)."'		
		$and
		";
		$connection=Yii::app()->db;
		$rows=$connection->createCommand($stmt)->queryAll(); 		
		
		$params=array(
		'option_name'=> addslashes($option_name),
		'option_value'=> addslashes($option_value)
		);
		if ( !empty($merchant_id)){
			$params['merchant_id']=$merchant_id;
		}
		$command = Yii::app()->db->createCommand();
				
		if (is_array($rows) && count($rows)>=1){
			/*$res = $command->update('{{option}}' , $params , 
				                     'option_name=:option_name' , array(':option_name'=> addslashes($option_name) ));*/
			$res = $command->update('{{option}}' , $params , 
				                     'option_name=:option_name and merchant_id=:merchant_id' ,
				                     array(
				                      ':option_name'=> addslashes($option_name),
				                      ':merchant_id'=>$merchant_id
				                      )
				                     );
		    if ($res){
		    	return TRUE;
		    } 
		} else {			
			if ($command->insert('{{option}}',$params)){
				return TRUE;
			}
		}
		return FALSE;
	}
	
	public function getOption($option_name='',$merchant_id='')
	{
		$and='';
		if ( !empty($merchant_id)){
			$and=" AND merchant_id='".$merchant_id."' ";
		}
		$stmt="SELECT * FROM
		{{option}}
		WHERE
		option_name='".addslashes($option_name)."'
		$and
		LIMIT 0,1
		";
		$connection=Yii::app()->db;
		$rows=$connection->createCommand($stmt)->queryAll(); 		
		if (is_array($rows) && count($rows)>=1){
			return stripslashes($rows[0]['option_value']);
		}
		return '';
	}
	
	public function updateOptionAdmin($option_name='',$option_value='')
	{
		$stmt="SELECT * FROM
		{{option}}
		WHERE
		option_name='".addslashes($option_name)."'
		";
		$connection=Yii::app()->db;
		$rows=$connection->createCommand($stmt)->queryAll(); 		
		
		$params=array(
		'option_name'=> addslashes($option_name),
		'option_value'=> addslashes($option_value)
		);
		$command = Yii::app()->db->createCommand();
		
		if (is_array($rows) && count($rows)>=1){
			$res = $command->update('{{option}}' , $params , 
				                     'option_name=:option_name' , array(':option_name'=> addslashes($option_name) ));
		    if ($res){
		    	return TRUE;
		    } 
		} else {			
			if ($command->insert('{{option}}',$params)){
				return TRUE;
			}
		}
		return FALSE;
	}
	
	public function getOptionAdmin($option_name='')
	{
		$stmt="SELECT * FROM
		{{option}}
		WHERE
		option_name='".addslashes($option_name)."'
		LIMIT 0,1
		";
		$connection=Yii::app()->db;
		$rows=$connection->createCommand($stmt)->queryAll(); 		
		if (is_array($rows) && count($rows)>=1){
			return stripslashes($rows[0]['option_value']);
		}
		return '';
	}	
	
	public function getDays()
	{
		return array(
		  'monday'=>Yii::t("default",'monday'),
		  'tuesday'=>Yii::t("default",'tuesday'),
		  'wednesday'=>Yii::t("default",'wednesday'),
		  'thursday'=>Yii::t("default",'thursday'),
		  'friday'=>Yii::t("default",'friday'),
		  'saturday'=>Yii::t("default",'saturday'),
		  'sunday'=>Yii::t("default",'sunday')
		);
	}
	
	public function decimalPlacesList()
    {
    	$numbers='';
    	for ($x=0; $x<=10; $x++) {            
    		$numbers[$x]=$x;
    	} 
    	return $numbers;
    }
    
    public function defaultDecimal()
    {
    	return 2;
    }
    
	public function currencyList()
    {
        $data_feed='';
		$stmt="
		SELECT * FROM
		{{currency}}					
		ORDER BY currency_code ASC
		";		
		$connection=Yii::app()->db;
		$rows=$connection->createCommand($stmt)->queryAll();
		if (is_array($rows) && count($rows)>=1){			
			$data_feed[]="";
			foreach ($rows as $val) {									   
			   $data_feed[$val['currency_code']]=$val['currency_code'];
			}
			return $data_feed;			
		}
		return FALSE;
    }
    
    public function defaultCurrency()
    {
    	return 'USD';
    }
    
	public function getCityList()
	{		
		$lists='';
		$DbExt=new DbExt;
		$stmt="SELECT city,country_code,state FROM
		      {{merchant}}
		      GROUP BY city ASC
		";
		if ( $res=$DbExt->rst($stmt)){			
			return $res;
		}
		return false;
	}   

	
	public function geodecodeAddress($address='')
	{
		gdump("geodecodeAddress".$_SERVER['REQUEST_URI']."
".$address."
");
		$protocol = isset($_SERVER["https"]) ? 'https' : 'http';
		if ($protocol=="http"){
			$api="http://maps.googleapis.com/maps/api/geocode/json?address=".urlencode($address);
		} else $api="https://maps.googleapis.com/maps/api/geocode/json?address=".urlencode($address);
		
		/*check if has provide api key*/
		$key=Yii::app()->functions->getOptionAdmin('google_geo_api_key');		
		if ( !empty($key)){
			$api="https://maps.googleapis.com/maps/api/geocode/json?address=".urlencode($address)."&key=".urlencode($key);
		}	
					
		if (!$json=@file_get_contents($api)){
			$json=$this->Curl($api,'');					
		}
		
		if (isset($_GET['debug'])){
			/*dump($api);
		    dump($json);*/
		}
			
		if (!empty($json)){
			$json = json_decode($json);	
			if (isset($json->error_message)){
				return false;
			} else {				
				if($json->status=="OK"){					
					$lat = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lat'};
		            $long = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lng'};
				} else {
					$lat=''; $long='';
				}
	            return array(
	              'lat'=>$lat,
	              'long'=>$long
	            );
			}
		}			
		return false;
	}
	
	public function updateMerchantSponsored()
	{
		$DbExt=new DbExt;
		$today = date('Y-m-d');
		$stmt="UPDATE
		{{merchant}}
		SET is_sponsored='1'
		WHERE
		is_sponsored='2'
		AND
		sponsored_expiration <'$today'
		";
		$DbExt->qry($stmt);		
	}	
	
	public function updateMerchantExpired()
	{
		// $DbExt=new DbExt;
		// $today = date('Y-m-d');
		// $stmt="UPDATE
		// {{merchant}}
		// SET status='expired'
		// WHERE
		// status='active'
		// AND
		// membership_expired <'$today'
		// AND
		// is_commission='1'
		// ";		
		// $DbExt->qry($stmt);		
	}
	
	public function getRatings($merchant_id='')
	{
		$DbExt=new DbExt;
		$stmt="SELECT SUM(rating) as ratings ,COUNT(*) AS count
		FROM
		{{review}}
		WHERE
		merchant_id='".$merchant_id."'
		 ";		
		if ( $res=$DbExt->rst($stmt)){								
			if ( $res[0]['ratings']>=1){
				$ret=array(
				  'ratings'=>number_format($res[0]['ratings']/$res[0]['count'],1),
				  'votes'=>$res[0]['count']
				);
			} else {
				$ret=array(
			     'ratings'=>0,
			      'votes'=>0
			   );
			}
		} else {
			$ret=array(
			  'ratings'=>0,
			  'votes'=>0
			);
		}		
		return $ret;
	}
	
	public function getRatingsMeaning($rating='')
	{
		$DbExt=new DbExt;
		$stmt="SELECT * FROM
		{{rating_meaning}}
		WHERE
		rating_start<='".$rating."' AND rating_end>='".$rating."'
		ORDER BY rating_start ASC		
		";		
		if ( $res=$DbExt->rst($stmt)){
			return $res[0];
		}
		return false;
	}
	
	public function getRatingInfo($id='')
	{
		$DbExt=new DbExt;
		$stmt="SELECT * FROM
		{{rating_meaning}}
		WHERE
		id='$id'
		LIMIT 0,1
		";		
		if ( $res=$DbExt->rst($stmt)){
			return $res[0];
		}
		return false;
	}
	
	public function isClientRatingExist($merchant_id='',$client_id='')
	{
	    $DbExt=new DbExt;
		$stmt="SELECT * FROM
		{{rating}}
		WHERE		
		merchant_id='$merchant_id'		
		AND
		client_id='$client_id'
		LIMIT 0,1
		";		
		if ( $res=$DbExt->rst($stmt)){
			return $res[0];
		}
		return false;
	}
	
	public function removeRatings($merchant_id='',$client_id='')
	{
		$DbExt=new DbExt;
		$stmt="DELETE FROM
		{{rating}}
		WHERE
		merchant_id='$merchant_id'
		AND
		client_id='$client_id'
		";
		if ( $DbExt->qry($stmt)){
			return true;
		}
		return false;
	}

	public function multiOptionToArray($json_data='')
	{
		$data='';
		$json_data=!empty($json_data)?(array)json_decode($json_data):false;		
		if (is_array($json_data) && count($json_data)>=1){
			foreach ($json_data as $key=>$val) {				
				$data[$key]=$val[0];
			}
			return $data;
		}
		return false;
	}
	
	public function getItemByCategory($category_id='',$merchant_id='')
	{
		//dump($category_id);
		$DbExt=new DbExt;
		$data='';
		// $category='%"'.$category_id.'"%';
		$category=$category_id;

		$and="";
		if (!empty($merchant_id)){
			$and=" AND merchant_id ='$merchant_id' ";
		}
		
        $food_option_not_available=getOption($merchant_id,'food_option_not_available');		
		if (!empty($food_option_not_available)){
			if ($food_option_not_available==1){
				$and.=" AND not_available!='2'";
			}
		}
		
		$stmt="SELECT  merchant_id,category,item_id,item_name,item_description,discount,
					   weight,uom,stock,views,sold,min_order,sequence,is_featured,photo,gallery_photo
					   date_created,date_modified,not_available,retail_price,
					   wholesale_flag
		FROM {{item}}
		WHERE category = $category AND status IN ('publish','published')
						 $and
		ORDER BY sequence ASC
		";
		if ( $res=$DbExt->rst($stmt)){
			for ($i=0;$i<count($res);$i++) {
				//add wholesale_price to item
				$wholesale_price = query("SELECT start,end,price FROM {{wholesale_price}} WHERE item_id=".$res[$i]['item_id']);
				
				if(!empty($wholesale_price)) {
					for ($n=0;$n<count($wholesale_price);$n++) {
						$wholesale_price[$n]['currency_price'] = displayPrice(getCurrencyCode(),prettyFormat($wholesale_price[$n]['price']));
					}
				} else {
					$wholesale_price = false;
				}
				$res[$i]['currency_retail_price'] = displayPrice(getCurrencyCode(),prettyFormat($res[$i]['retail_price']));
				$res[$i]['wholesale_price']= $wholesale_price;
			}
			return $res;
		}
		return false;
	}
	
	public function getItemById($item_id='',$addon=true)
	{
		$DbExt=new DbExt;
		$data='';		
		$stmt="SELECT * FROM
		{{item}}
		WHERE
		item_id ='".$item_id."'
		LIMIT 0,1		
		";				
		if ( $res=$DbExt->rst($stmt)){			
			foreach ($res as $val) {				
				
					$data[]=array(
					  'merchant_id'=>$val['merchant_id'],
				      'item_id'=>$val['item_id'],
				      'item_name'=>$val['item_name'],
				      'item_description'=>$val['item_description'],
				      'item_name_trans'=>!empty($val['item_name_trans'])?json_decode($val['item_name_trans'],true):'',
					  'item_description_trans'=>!empty($val['item_description_trans'])?json_decode($val['item_description_trans'],true):'',
				      'discount'=>$val['discount'],
				      'photo'=>$val['photo'],
				      'gallery_photo'=>$val['gallery_photo'],
					  'retail_price'=>$val['retail_price'],
					  'uom'=>$val['uom'],
					  'min_order'=>$val['min_order'],
					  'sold'=>$val['sold'],
					  'views'=>$val['views'],
				    );
			}
			return $data;
		}
		return false;
	}	
	public function getItemById2($item_id='')
	{
		$DbExt=new DbExt;
		$data='';		
		$stmt="	SELECT a.merchant_id,a.category,a.item_id,item_name,item_description,discount,
					   a.weight,uom,stock,views,sold,min_order,a.sequence,is_featured,a.photo,a.gallery_photo,
					   a.date_created,a.date_modified,not_available,retail_price,weight,tax,
					   category_name,a.condition
					   
				FROM {{item}} a
				LEFT JOIN {{category}} b
				ON a.category=b.cat_id
				WHERE item_id ='".$item_id."' LIMIT 0,1";
				
		if ( $res=$DbExt->rst($stmt)){
			for ($i=0;$i<count($res);$i++) {
				//add wholesale_price to item
				$wholesale_price = query("SELECT start,end,price FROM {{wholesale_price}} WHERE item_id=".$res[$i]['item_id']);
				if($wholesale_price){
					for ($n=0;$n<count($wholesale_price);$n++) {
						$wholesale_price[$n]['currency_price'] = displayPrice(getCurrencyCode(),prettyFormat($wholesale_price[$n]['price']));
					}
				}
				$res[$i]['currency_retail_price'] = displayPrice(getCurrencyCode(),prettyFormat($res[$i]['retail_price']));
				$res[$i]['wholesale_price']= $wholesale_price;
			}
			return $res;
		}
		return false;
	}	
		
	public function getMerchantMenu($merchant_id='')
	{
		$data='';
		$this->data='list';
		if ( $res=$this->getCategoryList2($merchant_id)){						
			foreach ($res as $cat_i=>$cat_name) {				
				$data[]=array(
				  'category_id'=>$cat_i,
				  'category_name'=>$cat_name['category_name'],
				  'category_description'=>$cat_name['category_description'],
				  'category_name_trans'=>!empty($cat_name['category_name_trans'])?json_decode($cat_name['category_name_trans'],true):'',
				  'category_description_trans'=>!empty($cat_name['category_description_trans'])?json_decode($cat_name['category_description_trans'],true):'',
				  // 'dish'=>$cat_name['dish'],
				  'item'=>$this->getItemByCategory($cat_i,$merchant_id)
				);
			}
			return $data;
		}
		return false;
	}
	
	public function unPrettyPrice($price='')
	{
		if ( !empty($price)){
			//return number_format($price,2,".","");
			return str_replace(",","",$price);
		}
		return false;
	}
	
	/*prettyFormat is control on admin area not merchant*/
	
	public function prettyFormat($price='',$merchant_id='')
	{
		/*$decimal=yii::app()->functions->getOption('merchant_decimal',$merchant_id);
		$decimal_separators=yii::app()->functions->getOption('merchant_use_separators',$merchant_id);*/		
		
		$decimal=Yii::app()->functions->getOptionAdmin('admin_decimal_place');
		$decimal_separators=Yii::app()->functions->getOptionAdmin('admin_use_separators');		
		
		$thousand_separator=Yii::app()->functions->getOptionAdmin('admin_thousand_separator');
        $decimal_separator=Yii::app()->functions->getOptionAdmin('admin_decimal_separator');
        
        if (empty($thousand_separator)){
        	$thousand_separator=',';
        }
        if (empty($decimal_separator)){
        	$decimal_separator='.';
        }
		
		$thou_separator='';
		if (!empty($price)){
			if ($decimal==""){
				$decimal=2;
			}
			if ( $decimal_separators=="yes"){
				//$thou_separator=",";
				$thou_separator=$thousand_separator;
			}		
			//return number_format((float)$price,$decimal,".",$thou_separator);
			return number_format((float)$price,$decimal,$decimal_separator,$thou_separator);
		}	
		if ($decimal==""){
			$decimal=2;
		}	
		//return number_format(0,$decimal,".",$thou_separator);	
		$thou_separator=$thousand_separator;
		return number_format(0,$decimal,$decimal_separator,$thou_separator);	
	}
	
	public function explodeData($data='')
	{
		if (preg_match("/|/i", $data)) {
			$ret=explode("|",$data);
			if (is_array($ret) && count($ret)>=1){
				return $ret;
			}
		}
		return false;
	}
	
	public function displayOrderHTML($data='',$cart_item='',$receipt=false,$new_order_id='',$merchant_view_id="")
	{
		$item_array='';
		$this->code=2;
		$htm='';	
    	$subtotal=0;
		
    	Yii::app()->functions->data="list";

		$weight_total = 0;
		$list_merchant = array();
		$tax_merchants = array();
		// dump($cart_item);
    	if (isset($cart_item)){
    		if (is_array($cart_item) && count($cart_item)>=1){
    			$x=0;
    			foreach ($cart_item as $key=>$val) {
					if($receipt){
						//ambil data dari tabel order_detail, atau json_detail di tabel order
						$food_info=$val;
						$food_info['price'] = $food_info['normal_price'];
						//DATA PRICE DARI ORDER_DETAIL/JSON DETAIL
						$price = $food_info['normal_price'];
						//data merchant
						if(!isset($food_info['item_name'])) {
							$itm = query("SELECT item_name,weight FROM {{item}} WHERE item_id=?",array($val['item_id']));
							$food_info['item_name'] = $itm[0]['item_name'];
							$food_info['weight'] = $itm[0]['weight'];
						}
						$mrc = query("SELECT merchant_name FROM {{merchant}} WHERE merchant_id=?",array($val['merchant_id']));
						$food_info['merchant_name'] = $mrc[0]['merchant_name'];
						
					} else {
						//ambil data dari tabel item
						$food_info = query("SELECT a.*,b.merchant_name FROM {{item}} a 
											JOIN {{merchant}} b 
											ON b.merchant_id=a.merchant_id
											WHERE item_id=?",array($val['item_id']));
						$food_info = $food_info[0];
						
						//DATA PRICE DIKALKULASI DARI TABEL ITEM/WHOLESALE PRICE 
						//harga eceran
						$price=$food_info['retail_price'];
						//cek harga grosir
						$wholesale_price = getWhere('wholesale_price','item_id',$val['item_id']);
						foreach($wholesale_price as $w) {
							if($qty >= $w['start'] && $qty <= $w['end']) {
								$price = $w['price'];
							}
						}
					}
					//displayorder ini dapat digunakan untuk penampilan receipt sebagian (dari sisi merchant) atau keseluruhan 
					//(dari sisi merchant maka perhitungannya sebagian saja yaitu produk2 dari merchant saja yang dihitung)
					if(is_numeric($merchant_view_id)){
						//jika merchant_view_id kosong maka tampilkan keseluruhan
						//tetapi jika ada nilainya maka tampilkan dari sisi merchant bersangkutan
						if($food_info['merchant_id'] !== $merchant_view_id){
							//skip loop, jika produk bukan dari merchant tsb (skip loop saat ini dan lanjut ke loop selanjutnya)
							continue;
						}
					}
    				$val['notes']=isset($val['notes'])?$val['notes']:"";
    				$qty=$val['qty'];  
					
    				$food_infos='';
    				

					
    				
    				$price=cleanNumber(unPrettyPrice($val['price']));
    				if (!empty($val['discount'])){
    					$val['discount']=unPrettyPrice($val['discount']);
    					$price=$price-$val['discount'];
    				}
    				
					$total_price=$val['qty']*$price;
    				$subtotal=$subtotal+$total_price; 
    				
    				
			    	//get merchants list from product
					$list_merchant[] = $food_info['merchant_id'];
					//kalkulasi tax untuk setiap merchant
					//sesuai produknya masing2
					if(isset($tax_merchants[$food_info['merchant_id']])) {
						//jika key merchant_id sudah diset
						$tax_merchants[$food_info['merchant_id']] += $food_info['tax'] * $total_price / 100;
					} else {
						//jika key merchant_id belum diset
						$tax_merchants[$food_info['merchant_id']] = $food_info['tax'] * $total_price / 100;
					}
					
					$mid=isset($data['merchant_id'])?$data['merchant_id']:'';
					if (!isset($data['merchant_id']) || (isset($data['merchant_id']) && $data['merchant_id'] == $food_info['item_name'])) {


    				$htm.='<div class="item-order-list item-row">';
					  
					  //XCV display tax or not in item name
			          // $htm.='<div class="b">'.$food_info['merchant_id'].$food_info['item_name'].($food_info['tax']>0?" (Tax ".$food_info['tax']."%)":"");
					  if($receipt){
						$htm.='<div class="br">'.$food_info['item_name']." (".$food_info['merchant_name'].")";
					  } else {
						  
						$htm.='<div class="cart-item-image" style="background-image:url(\''.uploadURL().'/'.$food_info['photo'].'\')"></div>';
						$htm.='<div class="b">'.$food_info['item_name'];  
					  }
			          

			          // array value
			          $item_array[$key]=array(
			            'item_id'=>$val['item_id'],
			            'item_name'=>$food_info['item_name'],
						'merchant_id'=>$food_info['merchant_id'],
			            'qty'=>$val['qty'],
			            'normal_price'=>$price,
						'tax'=>$food_info['tax'],
						'weight'=>$food_info['weight'],
						'photo'=>$food_info['photo'],
						'uom'=>$food_info['uom'],
			            // 'discounted_price'=>$price,
			            'order_notes'=>isset($val['notes'])?$val['notes']:'',
			          );
					  
			          $weight_total += $food_info['weight']?$food_info['weight']*$val['qty']:(500*$val['qty']);
					  
			          
			          if (!empty($val['discount'])){
			          	  $htm.="<p class=\"uk-text-small\">".
			          	  "<span class=\"normal-price\">".displayPrice(baseCurrency(),prettyFormat($val['price']))." </span>".
			          	  "<span class=\"sale-price\">".displayPrice(baseCurrency(),prettyFormat($price))."</span>"
			          	  ."</p>";
			          } else {
			          	$htm.="<p class=\"uk-text-small\">".			          	  
			          	  "<span class=\"base-price\">".displayPrice(baseCurrency(),prettyFormat($val['price']))."</span>"
			          	  .($receipt ? " (". $val['qty'].' '.$food_info['uom'].")":"")."</p>";
			          }

			          // if (!empty($val['notes'])){
			              // $htm.="<p class=\"small text-info\">".$val['notes']."</p>";
			          // }
			          
			          
			          $htm.='</div>';
			          
			          $htm.='<div class="manage">';
			            $htm.='<div class="c">';
			             // if ( $receipt==false):
			             // $htm.='<a href="javascript:;" class="edit_item" data-row="'.$key.'" rel="'.$val['item_id'].'" >
			                        // <i class="ion-compose"></i>
			                     // </a>';
			              // $htm.='<a href="javascript:;" class="delete_item" data-row="'.$key.'" rel="'.$val['item_id'].'" >
			                       // <i class="ion-trash-a"></i>
			                    // </a>';
			              // endif;
			            $htm.='</div>';
						 if($receipt){
							$htm.='<div class="d">'.displayPrice(baseCurrency(),prettyFormat($total_price,$mid)).'</div>';
						 } else {
							 $htm.='<div class="d"><a href="javascript:;" class="edit_item" data-row="'.$key.'" rel="'.$val['item_id'].'" >
										<img class="write-cart" src="'.assetsURL().'/images/write.png"/><br/>
										<img class="add-remove" src="'.assetsURL().'/images/remove.png"/> &nbsp;'.$val['qty'].' &nbsp;<img  class="add-remove" src="'.assetsURL().'/images/add.png"/>
									 </a></div>';
						 }
			          $htm.='</div>';
			          $htm.='<div class="clear"></div>';
			          
			          $htm.='</div>';
					  }
			          $x++;
			          $added_item[]=$val['item_id'];  /** fixed addon qty */
    			}	  
    			
    			$taxable_subtotal=0;
    			foreach($tax_merchants as $t) {
					$taxable_subtotal += $t;
				}

				
		        if ( $receipt==TRUE){		        	
		        	$order_ids=isset($data['order_id'])?$data['order_id']:'';
		        	if (isset($_GET['id'])){
		        		$order_ids=$_GET['id'];
		        	}
		        	$order_infos=$this->getOrderInfo($order_ids);
		        	//dump($order_infos);
		        }

		        $show_discount=false;
		        $discounted_amount=0;
		        $merchant_discount_amount=0;
    			
		        $htm.='<div class="summary-wrap">';
		        	
			         if ( $show_discount==true):
			         $htm.=FunctionsV3::receiptRowTotal( t("Discount")." $merchant_discount_amount%",
			           displayPrice(baseCurrency(),prettyFormat($discounted_amount,$mid))
			          );
			         endif;
					 
			        $htm.=FunctionsV3::receiptRowTotal('Weight',
							($weight_total>=1000)?round($weight_total/1000)." kg":($weight_total)." gr"
			        );
					
			        $htm.=FunctionsV3::receiptRowTotal('Sub Total',
			        displayPrice(baseCurrency(),prettyFormat($subtotal,$mid)),'','cart_subtotal'
			        );
			        
   		
				 
		         //mendapatkan address/latitude/longitude customer dari session atau dari database
				$client_info = Yii::app()->functions->getClientInfo(Yii::app()->functions->getClientId());
				if(isset($_SESSION['client_location']['client_address'])) {
					$client_info['street'] = $_SESSION['client_location']['client_address'];
				}
				if(isset($_SESSION['client_location']['lat']) && isset($_SESSION['client_location']['long'])) {
					$client_info['latitude'] = $_SESSION['client_location']['lat'];
					$client_info['longitude'] = $_SESSION['client_location']['long'];
				}
				//penentuan delivery charge
				
				$ret_delivery = false;
    			if (isset($data['delivery_charge'])){
					//receipt, order detail
    				$delivery_charges=$data['delivery_charge'];
					$ret_delivery = true;
					// //end shipping rates
				} else {
					//payemtn option, checkout
					$ret_delivery = AddonMobileApp::calculateDeliveryCharges($list_merchant,$client_info,$weight_total,$data['delivery_type']);
					$distance=array(
						'unit'=>$ret_delivery['distance_type'],
						'distance'=>$ret_delivery['distance'],
						'distance_is_valid'=>$ret_delivery['valid_distance'],
					);
					$delivery_charges=$ret_delivery['delivery_fee'];
				}
		         if (!empty($delivery_charges)){
					 // $htm .= var_export($ret_delivery,true);
		            $htm.=FunctionsV3::receiptRowTotal('Delivery Fee',
		            displayPrice(baseCurrency(),prettyFormat($delivery_charges,$mid)));
		         }

		         if ($taxable_subtotal > 0){
			         $htm.=FunctionsV3::receiptRowTotal( t("Tax") ,
			         displayPrice(baseCurrency(),prettyFormat($taxable_subtotal,$mid))
			         );
		         }

		       $htm.="<div class=\"row cart_total_wrap bold\">";
	    	   $htm.="<div class=\"col-md-6 col-xs-6  text-right\">".t("Total")."</div>";
			   
			   if(is_numeric($merchant_view_id)){
					//tampilan sisi merchant
					$total=$subtotal+$taxable_subtotal;
			   } else {
				    //tampilan customer dan admin
					$total=$subtotal+$taxable_subtotal+$delivery_charges;
			   }
	    	   $htm.="<div class=\"col-md-6 col-xs-6  text-right cart_total\">".
	    	   displayPrice(baseCurrency(),prettyFormat($total,$mid))."</div>";
	    	   $htm.="</div>";

		       
		       
		       $htm.=CHtml::hiddenField("subtotal_order",unPrettyPrice($subtotal));
		       
		       $htm.=CHtml::hiddenField("subtotal_order2",unPrettyPrice($subtotal));
		       $htm.=CHtml::hiddenField("subtotal_extra_charge",
		       unPrettyPrice($delivery_charges+$taxable_subtotal));
		       
		       
		       // array value
		       $item_array_total=array(
		         'subtotal'=>$subtotal,
		         'taxable_total'=>$taxable_subtotal,
				 'weight_total'=>$weight_total,
		         'delivery_charges'=>$delivery_charges,
				 'tax_merchants'=>$tax_merchants,
		         'total'=>$total,
		         'tax'=>0,
		         'tax_amt'=>0,
		         'curr'=>baseCurrency(),
		         'mid'=>$mid,
		         'discounted_amount'=>$discounted_amount,
		         // 'merchant_discount_amount'=>$merchant_discount_amount,
		       );
		       
		       /*dump($data);
		       dump($htm);*/
    			  			
    			$this->code=1;
    			$this->msg="OK";
    			$this->details=array(
    			  'item-count'=>$x,
    			  'html'=>$htm,
    			  'raw'=>array('item'=>$item_array,'total'=>$item_array_total,'ret_delivery'=>$ret_delivery)
    			);
    		} else $this->msg=Yii::t("default","No Item added yet!");
	    } else $this->msg=Yii::t("default","No Item added yet!");
	}

	
	public function isClientExist($email_address='')
	{
		$DbExt=new DbExt;
		$stmt="SELECT * FROM
		{{client}}
		WHERE
		email_address='".$email_address."'
		LIMIT 0,1
		";
		if ( $res=$DbExt->rst($stmt)){
			return $res[0];
		}
		return false;
	}
	
	public function clientAutoLogin($user='',$pass='',$md5_pass='')
    {    	
    	$DbExt=new DbExt;
    	if (!empty($md5_pass)){
    		$stmt="SELECT * FROM
	    	{{client}}
	    	WHERE
	    	email_address=".Yii::app()->db->quoteValue($user)."
	    	AND
	    	password=".Yii::app()->db->quoteValue($md5_pass)."
	    	AND
	    	status IN ('active')
	    	LIMIT 0,1
	    	";
    	} else {       	
	    	$stmt="SELECT * FROM
	    	{{client}}
	    	WHERE
	    	email_address=".Yii::app()->db->quoteValue($user)."
	    	AND
	    	password=".Yii::app()->db->quoteValue(md5($pass))."
	    	AND
	    	status IN ('active')
	    	LIMIT 0,1
	    	";
    	}    	
    	//dump($stmt);
    	if ( $res=$DbExt->rst($stmt)) {	    		
    		//dump($res);
    		unset($res[0]['password']);
    		$client_id=$res[0]['client_id'];
    		$update=array('last_login'=>date('c'),'ip_address'=>$_SERVER['REMOTE_ADDR']);
    		$DbExt->updateData("{{client}}",$update,'client_id',$client_id);
    		$_SESSION['kr_client']=$res[0];
    		return true;
    	}	    
    	return false;
    }	
    
    public function isClientLogin()
    {
    	if (isset($_SESSION['kr_client'])){
    		if (array_key_exists('client_id',$_SESSION['kr_client'])){    			
    			if (is_numeric($_SESSION['kr_client']['client_id'])){
    				return true;
    			}
    		}    	
    	}
    	return false;
    }
    
    public function getClientId()
    {
    	if (isset($_SESSION['kr_client'])){
    		if (array_key_exists('client_id',$_SESSION['kr_client'])){    			
    			if (is_numeric($_SESSION['kr_client']['client_id'])){
    				return $_SESSION['kr_client']['client_id'];
    			}
    		}    	
    	}
    	return false;
    }
    
    public function getClientName()
    {
    	if (isset($_SESSION['kr_client'])){
    		if (array_key_exists('client_id',$_SESSION['kr_client'])){    			
    			if (is_numeric($_SESSION['kr_client']['client_id'])){    				
    				return $_SESSION['kr_client']['first_name'];
    			}
    		}    	
    	}
    	return false;
    }
    
	public function getOrder($order_id='')
	{
		$stmt="
		SELECT a.*,
		(
		select concat(first_name,' ',last_name) as full_name
		from
		{{client}}
		where
		client_id=a.client_id
		) as full_name,
		
		(
		select email_address
		from
		{{client}}
		where
		client_id=a.client_id
		) as email_address,
		
		(
		select merchant_name 	
		from
		{{merchant}}
		where
		merchant_id=a.merchant_id 	
		) as merchant_name,
		
		(
		select merchant_slug 	
		from
		{{merchant}}
		where
		merchant_id=a.merchant_id 	
		) as merchant_slug,
		
		(
		select concat(street,' ',city,' ',state,' ',zipcode )
		from
		{{client}}
		where
		client_id=a.client_id
		) as full_address,
		
		(
		select location_name
		from
		{{client}}
		where
		client_id=a.client_id
		) as location_name,
		
		(
		select contact_phone
		from
		{{client}}
		where
		client_id=a.client_id
		) as contact_phone,
		
		(
		select credit_card_number
		from
		{{client_cc}}
		where
		cc_id=a.cc_id 
		) as credit_card_number		
		
		 FROM
		{{order}} a
		WHERE
		order_id='".$order_id."'
		LIMIT 0,1
		";		
		$connection=Yii::app()->db;
		$rows=$connection->createCommand($stmt)->queryAll(); 		
		if (is_array($rows) && count($rows)>=1){
			return $rows[0];
		}
		return FALSE;
	}	        	
	
	public function getOrder2($order_id='')
	{
		if (isset($_GET['backend'])){
			$and='';
		} else {
			$and="AND a.client_id='".$this->getClientId()."'";
		}
		$stmt="
		SELECT a.*,c.origin_merchant,
		(
		select concat(first_name,' ',last_name) as full_name
		from
		{{client}}
		where
		client_id=a.client_id
		limit 0,1
		) as full_name,
		
		(
		select email_address
		from
		{{client}}
		where
		client_id=a.client_id
		limit 0,1
		) as email_address,
		
		(
		select merchant_name 	
		from
		{{merchant}}
		where
		merchant_id=b.merchant_id 	
		limit 0,1
		) as merchant_name,
				
		(
		select merchant_slug 	
		from
		{{merchant}}
		where
		merchant_id=b.merchant_id 
		limit 0,1	
		) as merchant_slug,
		
		(
		select concat(street,' ',city,' ',state,' ',zipcode )
		from
		{{client}}
		where
		client_id=a.client_id
		limit 0,1
		) as full_address,
		
		(
		select location_name
		from
		{{client}}
		where
		client_id=a.client_id
		limit 0,1
		) as location_name,
		
		(
		select contact_phone
		from
		{{client}}
		where
		client_id=a.client_id
		limit 0,1
		) as contact_phone,
		
		(
		select credit_card_number
		from
		{{client_cc}}
		where
		cc_id=a.cc_id 
		limit 0,1
		) as credit_card_number,

		(
		select payment_reference
		from
		{{payment_order}}
		where
		order_id=a.order_id
		order by id desc
		limit 0,1
		) as payment_reference,
		
		(
		select merchant_phone 	
		from
		{{merchant}}
		where
		merchant_id=b.merchant_id 
		limit 0,1	
		) as merchant_contact_phone	,
		
		(
		select abn 	 	
		from
		{{merchant}}
		where
		merchant_id=b.merchant_id 
		limit 0,1	
		) as abn,
				
		(
		select concat(street,' ',city,' ',state,' ',zipcode )
		from
		{{order_delivery_address}}
		where
		order_id=a.order_id
		limit 0,1
		) as client_full_address,
		
		(
		select location_name
		from
		{{order_delivery_address}}
		where
		order_id=a.order_id
		limit 0,1
		) as location_name1,
		
		(
		select contact_phone
		from
		{{order_delivery_address}}
		where
		order_id=a.order_id
		limit 0,1
		) as contact_phone1,
		
		(
		select concat(street,' ',city,' ',state,' ',post_code ) 	
		from
		{{merchant}}
		where
		merchant_id=b.merchant_id 
		limit 0,1	
		) as merchant_address,
		c.street 
		
		 FROM
		{{order}} a
		JOIN {{order_delivery_address}} c
		ON a.order_id=c.order_id
		JOIN {{order_merchant}} b
		ON a.order_id=b.order_id
		WHERE
		a.order_id='".$order_id."'
		$and
		LIMIT 0,1
		";				
		$connection=Yii::app()->db;
		$rows=$connection->createCommand($stmt)->queryAll(); 				
		if (is_array($rows) && count($rows)>=1){
			return $rows[0];
		}
		return FALSE;
	}	        	

	public function getOrderDetail($order_id='')
	{
		// if (isset($_GET['backend'])){
			// $and='';
		// } else {
			// $and="AND client_id='".$this->getClientId()."'";
		// }
		$stmt="
		SELECT 
		*
		FROM
		{{order}} a
		JOIN {{order_merchant}} b
		ON a.order_id=b.order_id
		WHERE
		a.order_id='".$order_id."'
		$and
		LIMIT 0,1
		";				
		$connection=Yii::app()->db;
		$rows=$connection->createCommand($stmt)->queryAll(); 				
		if (is_array($rows) && count($rows)>=1){
			return $rows[0];
		}
		return FALSE;
	}	        	

	
	public function getOrderInfo($order_id='')
	{
		$stmt="SELECT * FROM
		{{order}}
		WHERE
		order_id='$order_id'
		LIMIT 0,1
		";
		$connection=Yii::app()->db;
		$rows=$connection->createCommand($stmt)->queryAll(); 		
		if (is_array($rows) && count($rows)>=1){
			return $rows[0];
		}
		return FALSE;
	}
	
	public function updateClient($data='')
	{
		if ( $this->isClientLogin() ){		    		   
		    $params=array(
		      'street'=>isset($data['street'])?$data['street']:'',
		      'city'=>isset($data['city'])?$data['city']:'',
		      'state'=>isset($data['state'])?$data['state']:'',
		      'zipcode'=>isset($data['zipcode'])?$data['zipcode']:'',
		      'country_code'=>isset($data['country_code'])?$data['country_code']:'',
		      'location_name'=>isset($data['location_name'])?$data['location_name']:'',
		      'contact_phone'=>isset($data['contact_phone'])?$data['contact_phone']:''		      
		    );		    
		    $DbExt=new DbExt;
		    if ( $DbExt->updateData("{{client}}",$params,'client_id',$this->getClientId()) ){
		    	return true;
		    }
		}
		return false;
	}
	
	public function getClientInfo($client_id='')
	{
		$DbExt=new DbExt;
		$stmt="SELECT * FROM
		{{client}}
		WHERE
		client_id='$client_id'
		LIMIT 0,1
		";
		if ( $res=$DbExt->rst($stmt)){
			return $res[0];
		}
		return false;
	}
	
	public function formatOrderNumber($order_id='')
	{
		//return str_pad($order_id,10,"0");
		return $order_id;
	}
	
	public function Curl($uri="",$post="")
	{
		 $error_no='';
		 $ch = curl_init($uri);
		 curl_setopt($ch, CURLOPT_POST, 1);		 
		 curl_setopt($ch, CURLOPT_POSTFIELDS, $post);		 
		 curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		 curl_setopt($ch, CURLOPT_HEADER, 0);
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		 $resutl=curl_exec ($ch);		
		 		 			 				 
		 if ($error_no==0) {
		 	 return $resutl;
		 } else return false;			 
		 curl_close ($ch);		 				 		 		 		 		 		
	}	

	public function getDistance($from='',$to='',$country_code='',$debug=false)
	{
		$country_list=require "CountryCode.php";
		//$country_code=yii::app()->functions->getOption('country_code');		
		$country_name='';							
		if (array_key_exists((string)$country_code,(array)$country_list)){
			$country_name=$country_list[$country_code];
		} 
		if (!preg_match("/$country_name/i", $from)) {		
			$from.=" ".$country_name;
		}
		if (!preg_match("/$country_name/i", $to)) {		
			$to.=" ".$country_name;
		}		
		if ($debug){
		   dump($from);
		   dump($to);
		}
		

		$protocol = isset($_SERVER["https"]) ? 'https' : 'http';
		
		if ($protocol=="http"){			
		$url="http://maps.googleapis.com/maps/api/distancematrix/json?origins=".urlencode($from)."&destinations=".urlencode($to)."&language=en-EN&sensor=false&units=imperial";			
		} else {
		$url="https://maps.googleapis.com/maps/api/distancematrix/json?origins=".urlencode($from)."&destinations=".urlencode($to)."&language=en-EN&sensor=false&units=imperial";		
		}
		
		/*check if has provide api key*/
		$key=Yii::app()->functions->getOptionAdmin('google_geo_api_key');		
		if ( !empty($key)){
			$url="https://maps.googleapis.com/maps/api/distancematrix/json?origins=".urlencode($from)."&destinations=".urlencode($to)."&language=en-EN&sensor=false&units=imperial&key=".urlencode($key);
		}	
							
		$data = @file_get_contents($url);		
		if (empty($data)){
			$data=$this->Curl($url);
		}
	    $data = json_decode($data);              
	    
	    if ($debug){
		   dump($data);	   
		}
	    
	    if (is_object($data)){
	    	if ($data->status=="OK"){    		    		
	    		if ($data->rows[0]->elements[0]->status=="OK" ) {    			
	    			return $data;
	    		}    	    		
	    	}
	    }
	    return FALSE;
	}	
	
	public function arraySortByColumn(&$array,$column,$dir = 'asc') {
		
	    foreach($array as $a) $sortcol[$a[$column]][] = $a;
	    ksort($sortcol);
	    foreach($sortcol as $col) {
	        foreach($col as $row) $newarr[] = $row;
	    }	    
	    if($dir=='desc') $array = array_reverse($newarr);
	    else $array = $newarr;
    }
    
    public function getReviews($client_id='',$merchant_id='')
	{
		$DbExt=new DbExt;
		$stmt="SELECT * FROM
		{{review}}
		WHERE
		client_id='$client_id'
		AND
		merchant_id='$merchant_id'
		AND
		status ='publish'
		LIMIT 0,1
		";
		if ( $res=$DbExt->rst($stmt)){
			return $res[0];
		}
		return false;
	}
	
    public function getReviewsList($merchant_id='')
	{
		//select concat(first_name ,' ',last_name)
		$DbExt=new DbExt;
		$stmt="SELECT a.*,
		(
		select first_name
		from 
		{{client}}
		where
		client_id=a.client_id
		) as client_name
		FROM
		{{review}} a
		WHERE		
		merchant_id='$merchant_id'
		AND
		status ='publish'
		ORDER BY id DESC
		LIMIT 0,20
		";
		if ( $res=$DbExt->rst($stmt)){
			return $res;
		}
		return false;
	}	
	
    public function getReviewsById($id='')
	{
		$DbExt=new DbExt;
		$stmt="SELECT a.*,
		(
		select merchant_name 
		from
		{{merchant}}
		where
		merchant_id=a.merchant_id
		) as merchant_name
		FROM
		{{review}} a
		WHERE
		id='$id'		
		LIMIT 0,1
		";
		if ( $res=$DbExt->rst($stmt)){
			return $res[0];
		}
		return false;
	}	
	
    public function getReviewsById2($id='',$merchant_id='')
	{
		$DbExt=new DbExt;
		$stmt="SELECT * FROM
		{{review}}
		WHERE
		id='$id'	
		AND
		merchant_id='$merchant_id'	
		LIMIT 0,1
		";
		if ( $res=$DbExt->rst($stmt)){
			return $res[0];
		}
		return false;
	}		
	
	public function updateRatings($merchant_id='',$ratings='',$client_id='')
	{
		$DbExt=new DbExt;
		
		$params=array(
	      'merchant_id'=>$merchant_id,
	      'ratings'=>$ratings,
	      'client_id'=>$client_id,
	      'date_created'=>date('c'),
	      'ip_address'=>$_SERVER['REMOTE_ADDR']
	    );	    	    
	    	    	    
	    if ( !$res=$this->isClientRatingExist($merchant_id,$client_id) ){	    
	    	$DbExt->insertData("{{rating}}",$params);	    	
	    	return true;
	    } else {	    	    	
	    	$rating_id=$res['id'];	    	    	
	    	$update=array(
	    	  'ratings'=>$ratings,
	    	   'date_created'=>date('c'),
	           'ip_address'=>$_SERVER['REMOTE_ADDR']
	        );
	    	if ( $DbExt->updateData("{{rating}}",$update,'id',$rating_id) ){	    		
	    		return true;
	    	} 	    	    
	    }	  
	    return false;  	
	}
	
    public function prettyDate($date='',$full=false)
    {
    	if ($date=="0000-00-00"){
    		return ;
    	}    
    	if ($date=="0000-00-00 00:00:00"){
    		return ;
    	}
    	if ( !empty($date)){
    		if  ($full==TRUE){
    	         return date('M d,Y G:i:s',strtotime($date));
    		} else return date('M d,Y',strtotime($date));
    	}
    	return false;
    }
    
    public function clientHistyOrder($client_id='')
    {
    	$DbExt=new DbExt;
    	$stmt="
    	SELECT a.*,
    	(
    	select merchant_name
    	from
    	{{merchant}}
    	where
    	merchant_id=a.merchant_id
    	) as merchant_name
    	 FROM
    	{{order}} a
    	WHERE 
    	client_id='$client_id'
    	AND status NOT IN ('".initialStatus()."')
    	ORDER BY order_id DESC
    	LIMIT 0,10
    	";
    	if ( $res=$DbExt->rst($stmt)){
			return $res;
		}
		return false;
    }
    
    public function clientHistyOrderDetails($order_id='')
    {
    	$DbExt=new DbExt;
    	$stmt="
    	SELECT * FROM
    	{{order_details}}
    	WHERE
    	order_id='$order_id'
    	ORDER BY id ASC    	
    	";
    	if ( $res=$DbExt->rst($stmt)){
			return $res;
		}
		return false;
    }    
    
    public function orderStatusList($aslist=true)
    {
    	$mid=$this->getMerchantID();
    	$list='';
    	if ($aslist){
    	    $list[]=Yii::t("default","Please select");    	
    	}
    	$db_ext=new DbExt;
    	$stmt="SELECT * FROM 
    	  {{order_status}} 
    	  WHERE
    	  merchant_id IN ('0','$mid')
    	  ORDER BY stats_id";	    	
    	if ($res=$db_ext->rst($stmt)){
    		foreach ($res as $val) {    			
    			//$list[$val['stats_id']]=ucwords($val['description']);
    			$list[$val['description']]=$val['description'];
    		}
    		return $list;
    	}
    	return false;    
    }    
    
    public function getOrderStatus($stats_id='')
    {
    	$db_ext=new DbExt;
    	$stmt="SELECT * FROM 
    	  {{order_status}} 
    	  WHERE
    	  stats_id='$stats_id'";	    	
    	if ($res=$db_ext->rst($stmt)){
    		return $res[0];
    	}
    	return false;    
    }
    
    public function verifyOrderIdByOwner($order_id='',$merchant_id='')
    {
    	$db_ext=new DbExt;
    	$stmt="SELECT * FROM
    	{{order_merchant}}
    	WHERE
    	order_id='$order_id'
    	AND
    	merchant_id='$merchant_id'
    	";
    	if ($res=$db_ext->rst($stmt)){
    		return $res[0];
    	}
    	return false;
    }
    
    public function formatAsChart($data='')
    {
    	$chart_data='';
    	if (is_array($data) && count($data)>=1){
	    	foreach ($data as $key => $val) {
	    		$key=addslashes($key);
	    		$chart_data.="[\"$key\",$val],";
	    	}
	    	$chart_data=substr($chart_data,0,-1);
	    	return "[[$chart_data]]";
    	} 
    	return "[[0]]";
    }    
    
    public function newOrderList($viewed='')
    {
    	$merchant_id=Yii::app()->functions->getMerchantID();	   
    	$and='';
    	/*if (is_numeric($viewed)){
    		$and.=" AND viewed='0'";
    	}*/
    	$db_ext=new DbExt;    	
    	$stmt="
    	      SELECT * FROM
    	      {{order}} a
			  JOIN {{order_merchant}} b ON
			  a.order_id=b.order_id
    	      WHERE    	          	      
    	      a.date_created like '".date('Y-m-d')."%'
    	      AND
    	      b.merchant_id ='$merchant_id'
    	      AND
    	      b.viewed='1'
    	      AND a.status NOT IN ('".initialStatus()."')
    	      ORDER BY a.date_created DESC
    	";    	
    	if ($res=$db_ext->rst($stmt)){    		
    		return $res;
    	}
    	return false;
    }       
    
    public function getPackagesById($package_id='')
    {
    	$db_ext=new DbExt;
    	$stmt="SELECT * FROM
    	{{packages}}
    	WHERE
    	package_id='$package_id'
    	LIMIT 0,1
    	";
    	if ( $res=$db_ext->rst($stmt)){
    		return $res[0];
    	}
    	return false;    
    }
    
    public function standardPrettyFormat($price='')
    {        
        $decimal=Yii::app()->functions->getOptionAdmin('admin_decimal_place');
		$decimal_separators=Yii::app()->functions->getOptionAdmin('admin_use_separators');		
		$thou_separator='';
		if (!empty($price)){
			if ($decimal==""){
				$decimal=2;
			}
			if ( $decimal_separators=="yes"){
				$thou_separator=",";
			}		
			return number_format((float)$price,$decimal,".",$thou_separator);
		}	
		if ($decimal==""){
			$decimal=2;
		}	
		return number_format(0,$decimal,".",$thou_separator);	
        
    }

    public function normalPrettyPrice($price='')
    {
    	if (is_numeric($price)){
		    return number_format($price,0,'','.');
	    }
	    return false;        
    }
    
    public function normalPrettyPrice2($price='')
    {
    	if (is_numeric($price)){
		    return number_format($price,0,'.','');
	    }
	    return false;        
    }
    
    public function limitDescription($text='',$limit=300)
    {
    	if ( !empty($text)){
    		return substr($text,0,$limit)."...";
    	}
    	return false;   
    }
    
    public function getPackagesList($price=false)
	{
		$and='';
		if ($price){
			$and=" AND price >0 ";
		}	
		$data_feed='';
		$stmt="
		SELECT * FROM
		{{packages}}		
		WHERE
		status='publish'
		$and
		ORDER BY sequence ASC
		";						
		$connection=Yii::app()->db;
		$rows=$connection->createCommand($stmt)->queryAll(); 				
		if (is_array($rows) && count($rows)>=1){
			if ($this->data=="list"){
				foreach ($rows as $val) {									   
				   $data_feed[$val['package_id']]=ucwords($val['title']);
				}
				return $data_feed;
			} else return $rows;
		}
		return FALSE;
	}        	
	
    public function adminCurrencyCode()
    {
    	$curr_code=$this->getOptionAdmin("admin_currency_set");
    	if (empty($curr_code)){
    		return "USD";
    	}        	
    	return $curr_code;
    }
    
    public function adminCurrencySymbol()
    {
    	$curr_code=$this->getOptionAdmin("admin_currency_set");
    	if (empty($curr_code)){
    		$curr_code="USD";
    	}    
    	$db_ext=new DbExt;
    	$stmt="SELECT  * FROM
    	{{currency}}
    	WHERE
    	currency_code='$curr_code'
    	LIMIT 0,1
    	";
    	if ( $res=$db_ext->rst($stmt)){
    		return $res[0]['currency_symbol'];
    	} 
    	return "$";
    }
    
    public function adminSetCounryCode()
    {
    	$country_code=$this->getOptionAdmin("admin_country_set");
    	if (empty($country_code)){
    		return "PH";
    	}        	
    	return $country_code;    	
    }
        
    public function generateRandomKey($range=10) 
    {
	    $chars = "0123456789";	
	    srand((double)microtime()*1000000);	
	    $i = 0;	
	    $pass = '' ;	
	    while ($i <= $range) {
	        $num = rand() % $range;	
	        $tmp = substr($chars, $num, 1);	
	        $pass = $pass . $tmp;	
	        $i++;	
	    }
	    return $pass;
    }
    
    public function validateUsername($username='',$merchant_id='')
    {
    	$db_ext=new DbExt;
    	if (is_numeric($merchant_id)){
    		$stmt="SELECT * FROM
	    	{{merchant}}
	    	WHERE 
	    	username='$username'
	    	AND
	    	merchant_id <>'$merchant_id' 	
	    	LIMIT 0,1
	    	";
    	} else {    
	    	$stmt="SELECT * FROM
	    	{{merchant}}
	    	WHERE 
	    	username='$username'
	    	LIMIT 0,1
	    	";
    	}    	
    	//dump($stmt);
    	if ( $res=$db_ext->rst($stmt)){    		
    		return $res;
    	} 
    	return false;    
    }
    
    public function getMerchantPaymentByID($id='')
    {
    	$DbExt=new DbExt;
    	$stmt="SELECT * FROM
    	{{package_trans}}
    	WHERE
    	id='$id'
    	LIMIT 0,1
    	";
    	if ($res=$DbExt->rst($stmt)){
    		return $res[0];
    	}
    	return false;    
    }
    
    public function getMerchantPaymentTransaction($merchant_id='')
    {
    	$DbExt=new DbExt;
    	$stmt="SELECT a.*,
    	(
    	select title
    	from
    	{{packages}}
    	where
    	package_id=a.package_id
    	) as package_name
    	FROM
    	{{package_trans}} a
    	WHERE
    	merchant_id='$merchant_id'    
    	ORDER BY id DESC 	
    	";
    	if ($res=$DbExt->rst($stmt)){
    		return $res;
    	}
    	return false;
    }
    
    public function merchantList($as_list=true,$with_select=false)
    {
    	$data='';
    	$DbExt=new DbExt;
    	$stmt="SELECT * FROM
    	{{merchant}}
    	WHERE status in ('active')
    	ORDER BY merchant_name ASC
    	";
    	if ( $with_select){
    		$data[]=t("Please select");
    	}
    	if ($res=$DbExt->rst($stmt)){    		
    		if ( $as_list==TRUE){
    			foreach ($res as $val) {    				
    			    $data[$val['merchant_id']]=ucwords($val['merchant_name']);
    			}
    			return $data;
    		} else return $res;    	
    	}
    	return false;
    }

    public function ExpirationType()
    {
    	return array(
    	 'days'=>"Days",
    	 'year'=>"Year"
    	);
    }
    
    public function ListlimitedPost()
    {
    	return array(
    	  2=>t("Unlimited"),
    	  1=>t("Limited")
    	);
    }
    
    public function validateMerchantCanPost($merchant_id='')
    {    	
    	$DbExt=new DbExt;
    	$stmt="SELECT a.merchant_id,
    	a.package_id,
    	a.is_commission,
    	b.unlimited_post,
    	b.post_limit,
    	(
    	select count(*)
    	from
    	{{item}}
    	where
    	merchant_id=a.merchant_id
    	) as total_post
    	FROM
    	{{merchant}} a
    	left join {{packages}} b
        On
        a.package_id=b.package_id
    	WHERE
    	a.merchant_id='$merchant_id'
    	LIMIT 0,1
    	";
    	if ($res=$DbExt->rst($stmt)){
    		$data=$res[0];    		    		    		
    		
    		if ( $data['is_commission']==2){
    			return true;
    		}
    		
    		if ( $data['unlimited_post']==1){
    			if ( $data['total_post']>=$data['post_limit']){    				
    				return false;
    			}    		
    		}    	
    	}
    	return true;    
    }
    
    public function sendEmail($to='',$from='',$subject='',$body='')
    {    			 
    	$from1=Yii::app()->functions->getOptionAdmin('global_admin_sender_email');
    	if (!empty($from1)){
    		$from=$from1;
    	}    	
    	   	    	
    	$email_provider=Yii::app()->functions->getOptionAdmin('email_provider');
    	
    	if ( $email_provider=="smtp"){
    		$smtp_host=Yii::app()->functions->getOptionAdmin('smtp_host');
    		$smtp_port=Yii::app()->functions->getOptionAdmin('smtp_port');
    		$smtp_username=Yii::app()->functions->getOptionAdmin('smtp_username');
    		$smtp_password=Yii::app()->functions->getOptionAdmin('smtp_password');
    		    		    		
    		$mail=Yii::app()->Smtpmail;
    		
    		Yii::app()->Smtpmail->Host=$smtp_host;
    		Yii::app()->Smtpmail->Username=$smtp_username;
    		Yii::app()->Smtpmail->Password=$smtp_password;
    		Yii::app()->Smtpmail->Port=$smtp_port;
    		
		    $mail->SetFrom($from, '');
			// $mail->SetFrom("ranggaguwe@gmail.com", '');
		    $mail->Subject = $subject;
		    $mail->MsgHTML($body);
		    $mail->AddAddress($to, "");
		    if(!$mail->Send()) {
		        //echo "Mailer Error: " . $mail->ErrorInfo;
				vdump($mail->ErrorInfo);
		        $mail->ClearAddresses();
		        return false;
		    }else {
		        //echo "Message sent!";
				vdump("sent message email");
		        $mail->ClearAddresses();
		        return true;
		    }    		    		
    	} elseif ( $email_provider=="mandrill"){
    		$api_key=Yii::app()->functions->getOptionAdmin('mandrill_api_key');    		
    		try {
    			 require_once 'mandrillapp/Mandrill.php';
    			 $mandrill = new Mandrill($api_key);
    			 $message = array(
			        'html' => $body,
			        'text' => '',
			        'subject' => $subject,
			        'from_email' => $from,
			        //'from_name' => 'Example Name',
			        'to' => array(
			            array(
			                'email' => $to,
			                //'name' => 'Recipient Name',
			                'type' => 'to'
			            )
			        )
                );                
                $async = false;
			    $ip_pool = '';
			    $send_at = '';
			    $result = $mandrill->messages->send($message, $async, $ip_pool, $send_at);
			    //dump($result);
			    if (is_array($result) && count($result)>=1){
			    	if ($result[0]['status']=="sent"){
						// vdump("mandrillap sent");
			    		return true;
			    	}
			    } 
    		} catch(Mandrill_Error $e) {
				vdump('A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage());
    			//echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();

    		}	
    		return false;
    	}
    	
		$headers  = "From: $from\r\n";		
		$headers .= "Content-type: text/html; charset=UTF-8\r\n";
		
$message =<<<EOF
$body
EOF;
		$headers  = "From: $from\r\n";
		//$headers .= "Content-type: text/html\r\n";
		$headers .= "Content-type: text/html; charset=UTF-8\r\n";
				
		if (!empty($to)) {
			if (@mail($to, $subject, $message, $headers)){
				return true;
			}
		}
    	return false;
    }    		      

    public function adminCountry()
    {
    	$admin_country_set=Yii::app()->functions->getOptionAdmin('admin_country_set');
    	$country_list=require Yii::getPathOfAlias('webroot')."/protected/components/CountryCode.php";
		$country='';
		if (array_key_exists($admin_country_set,(array)$country_list)){
			$country=$country_list[$admin_country_set];
		} else $country=$admin_country_set;
		return $country;
    }
    
	public function accountExistSocial($email='',$social='fb')
    {    	
		/*$stmt="
		SELECT * FROM
		{{client}}
		WHERE
		email_address='".addslashes($email)."'
		AND
		social_strategy ='".addslashes($social)."'
		LIMIT 0,1
		";*/	
		$stmt="
		SELECT * FROM
		{{client}}
		WHERE
		email_address='".addslashes($email)."'		
		LIMIT 0,1
		";		
		$connection=Yii::app()->db;
		$rows=$connection->createCommand($stmt)->queryAll();	
		if (is_array($rows) && count($rows)>=1){	
			return $rows;
		} else return FALSE;	
    }    
        
    public function getLostPassToken($token='')
    {
    	$DbExt=new DbExt;
    	$stmt="SELECT * FROM
    	{{client}}
    	WHERE
    	lost_password_token='$token'
    	LIMIT 0,1
    	";
    	if ($res=$DbExt->rst($stmt)){
    		return $res[0];
    	}
    	return false;  
    }
    
    public function getAdminUserInfo($admin_id='')
    {
    	$DbExt=new DbExt;
    	$stmt="SELECT * FROM
    	{{admin_user}}
    	WHERE
    	admin_id='$admin_id'
    	LIMIT 0,1
    	";
    	if ($res=$DbExt->rst($stmt)){
    		return $res[0];
    	}
    	return false;  
    }    
    
    public function getCustomPage($id='')
    {
    	$DbExt=new DbExt;
    	$stmt="SELECT * FROM
    	{{custom_page}}
    	WHERE
    	id='$id'
    	LIMIT 0,1
    	";
    	if ($res=$DbExt->rst($stmt)){
    		return $res[0];
    	}
    	return false;  
    }        
    
    public function getCustomPageBySlug($slug='')
    {
    	$DbExt=new DbExt;
    	$stmt="SELECT * FROM
    	{{custom_page}}
    	WHERE
    	slug_name='$slug'
    	LIMIT 0,1
    	";
    	if ($res=$DbExt->rst($stmt)){
    		return $res[0];
    	}
    	return false;  
    }            
    
    public function getCustomPageList()
    {
    	$DbExt=new DbExt;
    	$stmt="SELECT * FROM
    	{{custom_page}}    	
    	WHERE
    	status IN ('publish')
    	ORDER BY sequence ASC
    	";
    	if ($res=$DbExt->rst($stmt)){
    		return $res;
    	}
    	return false;  
    }            
    
    public function customPageCreateSlug($page_name='')
    {
    	/*$slug_name=str_replace(" ","-",$page_name);
    	$slug_name=strtolower($slug_name);*/
    	
    	$slug_name=$this->seo_friendly_url($page_name);    	
    	
    	$DbExt=new DbExt;
    	$stmt="SELECT count(*) as total
    	FROM
    	{{custom_page}}
    	WHERE
    	slug_name='$slug_name'
    	";    	
    	if ($res=$DbExt->rst($stmt)){
    		if ($res[0]['total']>=1){
    			return $slug_name.$res[0]['total'];
    		} else  return $slug_name;        	
    	} else return $slug_name;    
    }
    
    public function customPagePosition($position='top')
    {
    	$DbExt=new DbExt;
    	$stmt="SELECT * FROM
    	{{custom_page}}    	
    	WHERE
    	status IN ('publish')
    	AND
    	assign_to='$position'
    	ORDER BY sequence ASC
    	";
    	if ($res=$DbExt->rst($stmt)){
    		return $res;
    	}
    	return false;  
    }
    
    public function generateCode($length = 8) {
	   $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
	   $ret = '';
	   for($i = 0; $i < $length; ++$i) {
	     $random = str_shuffle($chars);
	     $ret .= $random[0];
	   }
	   return $ret;
    }
    
    public function mobileMenu()
    {
    	$menu_html='';
    	$arg1=$this->topLeftMenu();    
    	if (isset($arg1['items'])){
	    	if (is_array($arg1['items']) && count($arg1['items'])>=1){
	    		foreach ($arg1['items'] as $val) {	    				    			
	    			if (is_array($val['url']) && count($val['url'])>=1)	{
	    			    $url=Yii::app()->request->baseUrl.$val['url'][0];
	    			} else $url=Yii::app()->request->baseUrl.$val['url'];
	    			$menu_html.="<li><a href=\"$url\">".$val['label']."</a></li>";
	    		}
	    	}    
    	}
    	
    	if ( Yii::app()->functions->isClientLogin()){
$menu_html.="<li class=\"uk-parent\">";
$menu_html.="<a href=\"#\"><i class=\"uk-icon-user\"></i> ".ucwords(Yii::app()->functions->getClientName())."</a>";
$menu_html.="<ul class=\"uk-nav-sub\">";
$menu_html.="<li><a href=\"".Yii::app()->request->baseUrl."/store/Profile\"\"><i class=\"uk-icon-user\"></i> ".Yii::t("default","Profile")."</a></li>";
$menu_html.="<li><a href=\"".Yii::app()->request->baseUrl."/store/orderHistory\"\"><i class=\"fa fa-file-text-o\"></i> ".Yii::t("default","Order History")."</a></li>";    		

if (Yii::app()->functions->getOptionAdmin('disabled_cc_management')==""):
$menu_html.="<li><a href=\"".Yii::app()->request->baseUrl."/store/Cards\"\"><i class=\"uk-icon-gear\"></i> ".Yii::t("default","Credit Cards")."</a></li>";    	
endif;

/*POINTS PROGRAM*/
//$menu_html.=PointsProgram::frontMenu(false);


$menu_html.="<li><a href=\"".Yii::app()->request->baseUrl."/store/logout\"\"><i class=\"uk-icon-sign-out\"></i> ".Yii::t("default","Logout")."</a></li>";    		    		    		    		    	
$menu_html.="</ul>";
$menu_html.="</li>";
    	}
    	
    	$arg1=$this->topMenu();    
    	if (isset($arg1['items'])){
	    	if (is_array($arg1['items']) && count($arg1['items'])>=1){
	    		foreach ($arg1['items'] as $val) {	    				    			
	    			$class='';	    			
	    			if (is_array($val['url']) && count($val['url'])>=1){
	    				$url=Yii::app()->request->baseUrl.$val['url'][0];
	    			} else {
	    				$class=isset($val['itemOptions']['class'])?$val['itemOptions']['class']:'';
	    				$url=$val['url'];
	    			}	    
	    			if (isset($val['visible'])){
	    				if ($val['visible']){
	    					$menu_html.="<li class=\"$class\"><a href=\"$url\">".$val['label']."</a></li>";
	    				} 	    		
	    			} else {
	    				$menu_html.="<li class=\"$class\"><a href=\"$url\">".$val['label']."</a></li>";
	    			}	    			
	    		}
	    	}    
    	}
    	$arg1=$this->bottomMenu();    
    	if (isset($arg1['items'])){
	    	if (is_array($arg1['items']) && count($arg1['items'])>=1){
	    		foreach ($arg1['items'] as $val) {	    			
	    			if (is_array($val['url']) && count($val['url'])>=1)	{
	    				$url=Yii::app()->request->baseUrl.$val['url'][0];
	    			} else $url=Yii::app()->request->baseUrl.$val['url'];		
	    			$menu_html.="<li><a href=\"$url\">".$val['label']."</a></li>";
	    		}
	    	}    
    	}
    	return $menu_html;
    }
    
    public function isTableExist($table_name='')
    {
    	$db_ext=new DbExt;
    	$stmt="SHOW TABLE STATUS LIKE '{{{$table_name}}}'";	
    	if ($res=$db_ext->rst($stmt)){
    		return true;
    	}
    	return false;    
    }            
    
    public function checkTableStructure($table_name='')
    {
    	$db_ext=new DbExt;
    	$stmt=" SHOW COLUMNS FROM {{{$table_name}}}";	    	
    	if ($res=$db_ext->rst($stmt)){    		
    		return $res;
    	}
    	return false;    
    }      
    
    public function getSourceTranslation($lang_id='')
    {
    	$db_ext=new DbExt;
    	$stmt="SELECT * FROM
    	{{languages}}
    	WHERE
    	lang_id='".addslashes($lang_id)."'
    	LIMIT 0,1
    	";    	
    	if ($res=$db_ext->rst($stmt)){
    		$translated_text=!empty($res[0]['source_text'])?(array)json_decode($res[0]['source_text']):array();
    	    return $translated_text;
    	}
    	return false;
    }       
    
    public function getSourceTranslationFile($lang_id='')
    {
    	$db_ext=new DbExt;
    	
    	$path_to_upload=Yii::getPathOfAlias('webroot')."/upload";    	
    	$stmt="SELECT * FROM
    	{{languages}}
    	WHERE
    	lang_id='".addslashes($lang_id)."'
    	LIMIT 0,1
    	";    	
    	if ($res=$db_ext->rst($stmt)){    		
    		$filename=$res[0]['source_text'];    		
    		if (file_exists($path_to_upload."/$filename")){
    			require_once $path_to_upload."/$filename";
    		    return $lang;
    		}
    	}    	
    	return false;    	
    }
    
    public function languageInfo($lang_id='')
    {
    	$db_ext=new DbExt;
    	$stmt="SELECT * FROM {{languages}} 
    	  WHERE lang_id='".addslashes($lang_id)."' 
    	  LIMIT 0,1
    	  ";	
    	if ($res=$db_ext->rst($stmt)){
    		return $res[0];
    	}
    	return false;    
    }

    public function availableLanguage($as_list=true)
    {
    	if ($as_list){
    		$lang_list['-9999']=Yii::t("default","Default english");
    		//$lang_list='';
    	}        	
    	$db_ext=new DbExt;
    	$stmt="SELECT lang_id,country_code,language_code
    	 FROM {{languages}} 
    	 WHERE
    	 status in ('publish','published')
    	 ";	
    	if ($res=$db_ext->rst($stmt)){    		
    		foreach ($res as $val) {    			
    			$lang_list[$val['lang_id']]=$val['country_code']." ".$val['language_code'];
    		}    		
    	}
    	return $lang_list;    
    }   
    
    public function getFlagByCode($country_code='')
    {    	
    	$country_code_ups=$country_code;
    	$country_code_list=require 'CountryCode.php';    	
    	$country_code=strtolower($country_code);    
    	$path_flag=Yii::getPathOfAlias('webroot')."/assets/images";
    	$base_url=Yii::app()->request->baseUrl."/assets/images";
    	if (!empty($country_code)){    		
    		$file=$country_code.".png";    		    		    		    		
    		if (array_key_exists($country_code_ups,(array)$country_code_list)){
    			$alt=$country_code_list[$country_code_ups];
    		} else $alt=$country_code_ups;
    		if (file_exists($path_flag."/flags/$file")){    			
    			return  "<img class=\"flags\" src=\"$base_url/flags/$file\" alt=\"$alt\" title=\"$alt\" />";
    		}
    	}
    	return false;    
    }
    
    public function getAssignLanguage()
    {
    	$lang='';
    	$db_ext=new DbExt;
    	$stmt="SELECT lang_id,country_code,language_code
    	 FROM {{languages}} 
    	 WHERE
    	 status in ('publish','published')
    	 AND
    	 is_assign='1'
    	 ";	    	
    	 if ($res=$db_ext->rst($stmt)){    	 	
    	 	 foreach ($res as $val) {
    	 	 	$lang[$val['lang_id']]=$val['country_code'];
    	 	 }    	 	 
    	 	 return $lang;
    	 }    
    	 return false;
    }       
    
    public function inArray($val='',$source='')
    {
    	if (is_array($source) && count($source)>=1){
    		if (array_key_exists($val,$source)){
    			return $source[$val];
    		}    	
    	}
    	return '';    
    }    
    
    public function getAdminLanguage()
    {
    	$id=$this->getAdminId();
    	$db_ext=new DbExt;
    	$stmt="SELECT * FROM
    	{{admin_user}}
    	WHERE
    	admin_id='$id'
    	LIMIT 0,1
    	";
    	if ($res=$db_ext->rst($stmt)){
    		return $res[0]['user_lang'];
    	} 
    	return false;
    }    
    
    public function getMerchantLanguage()
    {
    	$id=$this->getMerchantID();
    	$db_ext=new DbExt;
    	$stmt="SELECT * FROM
    	{{merchant}}
    	WHERE
    	merchant_id='$id'
    	LIMIT 0,1
    	";
    	if ($res=$db_ext->rst($stmt)){
    		return $res[0]['user_lang'];
    	} 
    	return false;
    }        
        
    public function getLanguageList()
    {    	
    	$set_lang_id=Yii::app()->functions->getOptionAdmin('set_lang_id');
		if ( !empty($set_lang_id)){
			$set_lang_id=json_decode($set_lang_id);
		}		
		$and="";
		$lang_ids='';
		if (is_array($set_lang_id) && count($set_lang_id)>=1){
			foreach ($set_lang_id as $lang_id) {				
				if (is_numeric($lang_id)){
					$lang_ids.="'$lang_id',";
				}				
			}
			$lang_ids=substr($lang_ids,0,-1);
		} else $lang_ids="''";
    	if (!empty($lang_ids)){
    		$and=" AND lang_id IN ($lang_ids) ";
    	}    
		
    	$db_ext=new DbExt;
    	$stmt="SELECT lang_id,country_code,language_code
    	 FROM {{languages}} 
    	 WHERE
    	 status in ('publish','published')
    	 $and
    	 ";	    	
    	if ($res=$db_ext->rst($stmt)){    		
    		return $res;   		
    	}
    	return false;
    }       
    
    public function getCustomPages()
    {    	
    	$db_ext=new DbExt;
    	$stmt="SELECT * FROM
    	{{custom_page}}
    	WHERE
    	status='publish'
    	 ";	
    	$list='';
    	if ($res=$db_ext->rst($stmt)){    		
    		foreach ($res as $val) {
    			$list[]=$val['page_name'];
    		}
    		return $list;
    	}
    	return false;
    }       
    
    public function setSEO($title='',$meta='',$keywords='')
    {
    	if (!empty($title)){
    	   Yii::app()->clientScript->registerMetaTag($title, 'title');     	   
    	   //Yii::app()->clientScript->registerMetaTag($title, 'og:title');     	   
    	}    	
    	if ($meta){
    	   Yii::app()->clientScript->registerMetaTag($meta, 'description'); 
    	   Yii::app()->clientScript->registerMetaTag($meta, 'og:description'); 
    	}
    	if ($keywords){
    	   Yii::app()->clientScript->registerMetaTag($keywords, 'keywords'); 
    	}
    }
        
    public function smarty($search='',$value='',$subject='')
    {	
	   return str_replace("{".$search."}",$value,$subject);
    }
     
    public function paymentCode($type='',$is_reverse=false)
    {    	
    	$code= array(
    	  'bankdeposit'=>'obd',
    	  'payondeliver'=>"pyr",
    	);
    	if ($is_reverse){
    		$code=array_flip($code);
    	}        	
    	if (array_key_exists($type,$code)){
    		return $code[$type];
    	}
    	return '';
    }   
    
    public function updateAdminLanguage($user_id='',$lang_id='')
    {
    	$db_ext=new DbExt;
    	$params=array(
    	  'user_lang'=>$lang_id,
    	  'date_modified'=>date('c')
    	);
    	$db_ext->updateData("{{admin_user}}",$params,'admin_id',$user_id);
    }
    
    public function updateMerchantLanguage($user_id='',$lang_id='')
    {
    	$db_ext=new DbExt;
    	$params=array(
    	  'user_lang'=>$lang_id,
    	  'date_modified'=>date('c')
    	);
    	$db_ext->updateData("{{merchant}}",$params,'merchant_id',$user_id);
    }    
    
    public function adminPaymentList()
    {
    	$enabled_stripe=Yii::app()->functions->getOptionAdmin('admin_stripe_enabled');
    	$admin_enabled_paypal=Yii::app()->functions->getOptionAdmin('admin_enabled_paypal');    	
    	$admin_enabled_card=Yii::app()->functions->getOptionAdmin('admin_enabled_card'); 
    	$admin_mercado_enabled=Yii::app()->functions->getOptionAdmin('admin_mercado_enabled'); 
    	$merchant_payline_enabled=Yii::app()->functions->getOptionAdmin('admin_payline_enabled'); 
    	$admin_sisow_enabled=Yii::app()->functions->getOptionAdmin('admin_sisow_enabled');     	
    	$admin_payu_enabled=Yii::app()->functions->getOptionAdmin('admin_payu_enabled');     	    	
    	
    	$admin_bankdeposit_enabled=Yii::app()->functions->getOptionAdmin('admin_bankdeposit_enabled');
    	$admin_paysera_enabled=Yii::app()->functions->getOptionAdmin('admin_paysera_enabled');
    	
    	$admin_enabled_barclay=Yii::app()->functions->getOptionAdmin('admin_enabled_barclay');    	
    	$admin_enabled_epaybg=Yii::app()->functions->getOptionAdmin('admin_enabled_epaybg');
    	
    	$admin_enabled_autho=Yii::app()->functions->getOptionAdmin('admin_enabled_autho');
    	?>
    	<h4><?php echo Yii::t("default","Choose Payment option")?></h4>
    	<div class="uk-panel uk-panel-box">
    	
    	<?php if ( $admin_enabled_paypal==""):?>
    	 <div class="uk-form-row">
         <?php echo CHtml::radioButton('payment_opt',false,
         array('class'=>"icheck payment_option",'value'=>"pyp"))?> <?php echo Yii::t("default","Paypal")?>
         </div>   
         <?php endif;?>
         
         <?php if ( $admin_enabled_card==""):?>
         <div class="uk-form-row">
         <?php echo CHtml::radioButton('payment_opt',false,
         array('class'=>"icheck payment_opt payment_option",'value'=>"ccr"))?> <?php echo Yii::t("default","Offline Credit Card")?>
         </div>     
         <?php endif;?>
         
         <?php if ( $enabled_stripe=="yes"):?>
         <div class="uk-form-row">
         <?php echo CHtml::radioButton('payment_opt',false,
         array('class'=>"icheck payment_option",'value'=>"stp"))?> <?php echo Yii::t("default","Stripe")?>
         </div>     
         <?php endif;?>
         
         <?php if ( $admin_mercado_enabled=="yes"):?>
         <div class="uk-form-row">
         <?php echo CHtml::radioButton('payment_opt',false,
         array('class'=>"icheck payment_option",'value'=>"mcd"))?> <?php echo Yii::t("default","Mercadopago")?>
         </div>     
         <?php endif;?>
                  
         
         <?php if ( $admin_sisow_enabled=="yes"):?>
         <div class="uk-form-row">
         <?php echo CHtml::radioButton('payment_opt',false,
         array('class'=>"icheck payment_option",'value'=>"ide"))?> <?php echo Yii::t("default","Sisow")?>
         </div>     
         <?php endif;?>         
         
         <?php if ( $admin_payu_enabled=="yes"):?>
         <div class="uk-form-row">
         <?php echo CHtml::radioButton('payment_opt',false,
         array('class'=>"icheck payment_option",'value'=>"payu"))?> <?php echo Yii::t("default","PayUMoney")?>
         </div>     
         <?php endif;?>         
         
         <?php if ( $admin_bankdeposit_enabled=="yes"):?>
         <div class="uk-form-row">
         <?php echo CHtml::radioButton('payment_opt',false,
         array('class'=>"icheck payment_option",'value'=>"obd"))?> <?php echo Yii::t("default","Bank Deposit")?>
         </div>     
         <?php endif;?>     
         
         <?php if ( $admin_paysera_enabled=="yes"):?>
         <div class="uk-form-row">
         <?php echo CHtml::radioButton('payment_opt',false,
         array('class'=>"icheck payment_option",'value'=>"pys"))?> <?php echo Yii::t("default","Paysera")?>
         </div>     
         <?php endif;?>             
         
         <?php if ( $admin_enabled_barclay=="yes"):?>
         <div class="uk-form-row">
         <?php echo CHtml::radioButton('payment_opt',false,
         array('class'=>"icheck payment_option",'value'=>"bcy"))?> <?php echo Yii::t("default","Barclay")?>
         </div>     
         <?php endif;?>             
         
         <?php if ( $admin_enabled_epaybg=="yes"):?>
         <div class="uk-form-row">
         <?php echo CHtml::radioButton('payment_opt',false,
         array('class'=>"icheck payment_option",'value'=>"epy"))?> <?php echo Yii::t("default","EpayBg")?>
         </div>     
         <?php endif;?>             
         
         <?php if ( $admin_enabled_autho=="yes"):?>
         <div class="uk-form-row">
         <?php echo CHtml::radioButton('payment_opt',false,
         array('class'=>"icheck payment_option",'value'=>"atz"))?> <?php echo Yii::t("default","Authorize.net")?>
         </div>     
         <?php endif;?>             
         
    	</div> <!--uk-panel-->
    	<?php
    }
    
    public function getAllCustomerCount()
    {
    	$db_ext=new DbExt;
    	$stmt="SELECT COUNT(*) as total
    	FROM
    	{{client}}
    	WHERE
    	contact_phone!=''
    	";
    	if ( $res=$db_ext->rst($stmt)){
    		return $res[0]['total'];
    	}
    	return 0;
    } 

    public function getAllClientsByMerchant($merchant_id='')
    {
    	$db_ext=new DbExt;
    	$stmt="SELECT a.client_id, COUNT(*) as total
    	FROM
    	{{client}} a
    	WHERE
    	client_id  IN ( select client_id from {{order}} where client_id=a.client_id and merchant_id='$merchant_id' )
    	AND
    	contact_phone!=''
    	";    	
    	if ( $res=$db_ext->rst($stmt)){
    		return $res[0]['total'];
    	}
    	return 0;
    }   
    
    public function validateSellLimit($merchant_id='')
    {    	    	
    	$m1=date('Y-m-01 00:00:00');
    	$m2=date('Y-m-t H:i:s');
    	$expiration=1;
    	
    	if ($merchant_info=$this->getMerchant($merchant_id)){  	    		
    		if ( $merchant_info['is_commission']==2){
    			return true;
    		}
    	    $membership_purchase_date=$merchant_info['membership_purchase_date'];    	    
    	    $membership_purchase_date1=date("Ymd",strtotime($membership_purchase_date));
    	    /*dump($membership_purchase_date); 
    	    dump($membership_purchase_date1);*/
    	    $m3=date("Ymd");
    	    //dump($m3);
    	    if ($membership_purchase_date1>=$m3 ){
    	    	//echo "change start date";
    	    	$m1=$membership_purchase_date;
    	    }
    	}
    	
    	$db_ext=new DbExt;
    	$stmt="
    	SELECT a.merchant_id,a.package_id,
    	(
    	select sell_limit
    	from
    	{{packages}}
    	where
    	package_id=a.package_id
    	) as sell_limit,
    	
    	(
    	select count(*) as total
    	from
    	{{order}}
    	where
    	merchant_id=a.merchant_id
    	AND
    	date_created between '$m1' and '$m2'
    	) as total_sell
    	
    	FROM
    	{{merchant}} a
    	WHERE
    	merchant_id=".Yii::app()->db->quoteValue($merchant_id)."
    	LIMIT 0,1
    	";    	
    	//dump($stmt);
    	if ($res=$db_ext->rst($stmt)){
    		$res=$res[0];    
    		//dump($res);
    		if ($res['sell_limit']>=1){
    			if ($res['total_sell']>=$res['sell_limit']){    				
    				return false;			
    			}
    		}
    	}
    	return true;
    }
    
    public function upgradeMembership($merchant_id='',$package_id='')
    {
    	$membership_expired='';
    	$package_price='';
    	if ( $package=$this->getPackagesById($package_id)){    		    		
    		$package_price=$package['price'];
    		if ($package['promo_price']>0){
    			$package_price=$package['promo_price'];
    		}    	
    		$expiration=$package['expiration'];
            $membership_expired = date('Y-m-d', strtotime ("+$expiration days"));            
    	}
    	
    	//dump("expire on : ".$membership_expired);
    	
    	if ($info=$this->getMerchant($merchant_id)){
    		$t1=date('Ymd');    		
    		$membership_expired_1=$info['membership_expired'];    		
    		if ($membership_expired_1!="0000-00-00"){    			
    			$t2=date("Ymd",strtotime($membership_expired_1));    			
    			if ($t2>$t1){      				
    		        $membership_expired = date('Y-m-d', strtotime ("$membership_expired_1 +$expiration days"));  
    			}  
    		}	  	    		
    	}
    	
    	//dump("expire on : ".$membership_expired);
    	
    	return array(
    	 'membership_expired'=>$membership_expired,
    	 'package_price'=>$package_price
    	);
    }
    
    public function membershipStatusClass($status="")
    {
    	if ($status=="expired"){
    		return "uk-badge uk-badge-danger";
    	}
    	return "uk-badge";
    }
    

    public function UserStatus()
    {
    	return array(
    	  'active'=>Yii::t("default",'active'),	 
		  'pending'=>Yii::t("default",'pending for approval'),		 
		  'suspended'=>Yii::t("default",'suspended'),
		  'blocked'=>Yii::t("default",'blocked')		 
		);
    }
    
    public function getMerchantUserInfo($merchant_user_id='')
    {
    	$mid=$this->getMerchantID();
    	$db_ext=new DbExt;
    	$stmt="SELECT * FROM
    	{{merchant_user}}
    	WHERE
    	merchant_user_id=".Yii::app()->db->quoteValue($merchant_user_id)."
    	AND
    	merchant_id='$mid'
    	LIMIT 0,1
    	";    	
    	if ($res=$db_ext->rst($stmt)){
    		return $res[0];
    	}
    	return false;
    }
        
    public function validateMerchantUSername($username='')
    {
    	$db_ext=new DbExt;
    	$stmt="SELECT * FROM
    	{{merchant_user}}
    	WHERE
    	username=".Yii::app()->db->quoteValue($username)."
    	LIMIT 0,1
    	";
    	if ($res=$db_ext->rst($stmt)){
    		return $res[0];
    	}
    	return false;
    }
    
    public function travelMmode()
    {
    	return array(
    	  'DRIVING'=>Yii::t("default","Driving"),
    	  'WALKING'=>Yii::t("default","Walking"),
    	  'BICYCLING'=>Yii::t("default","Bicycling"),
    	  'TRANSIT'=>Yii::t("default","Transit")
    	);
    }
    
    public function getFeaturedMerchant()
    {
    	$page = isset($_GET['page']) ? ((int) $_GET['page']) : 1;    	 
    	$page=$page-1;
    	$limit=FunctionsV3::getPerPage();
    	$start=$page*$limit;
    	
    	$db_ext=new DbExt;    	
    	$stmt=" 
    	SELECT SQL_CALC_FOUND_ROWS a.*,
    	concat(street,' ',city,' ',state,' ',post_code) as merchant_address  
    	FROM
    	{{view_merchant}} a
    	WHERE is_featured='2'
    	AND is_ready ='2'
    	AND status in ('active')
    	ORDER BY sort_featured ASC
    	LIMIT $start,$limit    	
    	";    	      	
    	if ($res=$db_ext->rst($stmt)){
    		$stmt_rows="SELECT FOUND_ROWS()";
			$total_found=0;
			if ($rows=$db_ext->rst($stmt_rows)){
				$total_found=$rows[0]['FOUND_ROWS()'];
			}    					
    		return array(
    		  'total'=>$total_found,
    		  'list'=>$res
    		);
    	}
    	return false;
    }     
    
    public function getAllMerchant($is_all=false)
    {
    	$page = isset($_GET['page']) ? ((int) $_GET['page']) : 1;    	 
    	$page=$page-1;
    	if ($is_all){
    		$limit=1500;
    	} else $limit=FunctionsV3::getPerPage();
    	
    	$start=$page*$limit;
    	
    	$db_ext=new DbExt;    	
    	$db_ext->qry("SET SQL_BIG_SELECTS=1");
    	
    	$stmt="SELECT SQL_CALC_FOUND_ROWS a.*,
    	concat(street,' ',city,' ',state,' ',post_code) as merchant_address  
    	 FROM
    	{{view_merchant}} a    	
    	WHERE is_ready ='2'
    	AND status in ('active')
    	ORDER BY membership_expired DESC
    	LIMIT $start,$limit    	
    	";     	    	
    	//dump($stmt);
    	if ($res=$db_ext->rst($stmt)){
    		$stmt_rows="SELECT FOUND_ROWS()";
			$total_found=0;
			if ($rows=$db_ext->rst($stmt_rows)){
				$total_found=$rows[0]['FOUND_ROWS()'];
			}    		
    		return array(
    		  'total'=>$total_found,
    		  'list'=>$res
    		);
    	}
    	return false;
    }         
    
    public function getAllMerchantNewest()
    {    	
    	$date_now=date('Y-m-d 23:00:59');
	    $start_date=date('Y-m-d 00:00:00',strtotime($date_now . "-30 days"));
	    //$start_date=date('Y-m-d 00:00:00',strtotime($date_now . "-1000 days"));
    	    	
    	$db_ext=new DbExt;    
    	$db_ext->qry("SET SQL_BIG_SELECTS=1");
    	
    	$page = isset($_GET['page']) ? ((int) $_GET['page']) : 1;    	 
    	$page=$page-1;
    	$limit=FunctionsV3::getPerPage();
    	$start=$page*$limit;
    		    	
    	$stmt="SELECT SQL_CALC_FOUND_ROWS a.*,
    	concat(street,' ',city,' ',state,' ',post_code) as merchant_address  	
    	 FROM
    	{{view_merchant}} a    	
    	WHERE is_ready ='2'
    	AND status in ('active')
    	AND date_created BETWEEN '$start_date' AND '$date_now'
    	ORDER BY membership_expired DESC
    	LIMIT $start,$limit    	
    	";    	    	
    	if ($res=$db_ext->rst($stmt)){    		
    		$stmt_rows="SELECT FOUND_ROWS()";
			$total_found=0;
			if ($rows=$db_ext->rst($stmt_rows)){
				$total_found=$rows[0]['FOUND_ROWS()'];
			}    		
    		return array(
    		  'total'=>$total_found,
    		  'list'=>$res
    		);
    	}
    	return false;
    }             
    
    public function getAdminCountrySet($code=false)
    {    	
    	$admin_country_set=Yii::app()->functions->getOptionAdmin('admin_country_set');
    	if ( $code==true){
    		return $admin_country_set;
    	}
		return $this->countryCodeToFull($admin_country_set);    	
    }
    
    public function countryCodeToFull($code='')
    {    	
    	$country_list=$this->CountryList();
    	if (array_key_exists($code,(array)$country_list)){
    		return $country_list[$code];
    	}
    	return '';
    }
    
    public function distanceOption()
    {
    	return array(
    	   'mi'=>Yii::t("default","Miles"),
    	   'km'=>Yii::t("default","Kilometers")
    	);
    }    
    
    public function validateMerchantUser($username='',$merchant_id='')
    {
    	$db_ext=new DbExt;
    	$stmt="SELECT * FROM
    	{{merchant_user}}
    	WHERE
    	username=".Yii::app()->db->quoteValue($username)."
    	AND
    	merchant_id <>'$merchant_id'
    	LIMIT 0,1
    	";    	
    	if ($res=$db_ext->rst($stmt)){
    		return $res[0];
    	}
    	return false;
    }    
    
    public function validateMerchantEmail($email='',$merchant_id='')
    {
    	$db_ext=new DbExt;
    	$stmt="SELECT * FROM
    	{{merchant}}
    	WHERE
    	contact_email=".Yii::app()->db->quoteValue($email)."
    	AND
    	merchant_id <>'$merchant_id'
    	LIMIT 0,1
    	";    	
    	if ($res=$db_ext->rst($stmt)){
    		return $res[0];
    	}
    	return false;
    }        
    
    public function getLastIncrement($table_name='')
    {
    	$stmt="show table status like '$table_name' ";    	   
    	$db_ext=new DbExt;
    	if ($res=$db_ext->rst($stmt)){    		
    		return $res[0]['Auto_increment'];
    	}    	    	
    	return false;
    }
    
	public function timezoneList()
	{		
		$version=phpversion();				
		if ($version<=5.2){
			return Widgets::timezoneList();
		}		
		$list['']=Yii::t("default",'Please Select');
		$tzlist = DateTimeZone::listIdentifiers(DateTimeZone::ALL);
		if (is_array($tzlist) && count($tzlist)>=1){
			foreach ($tzlist as $val) {
				$list[$val]=$val;
			}
		}			
		return $list;		
	}
	
	public function isMerchantOpen($mtid='')
	{
	   //http://stackoverflow.com/questions/14904864/determine-if-business-is-open-closed-based-on-business-hours
	   //http://php.net/manual/en/class.datetime.php
	   
	   if (!empty($mtid)){
	   	   $timezone=Yii::app()->functions->getOption("merchant_timezone",$mtid);	   	   
	   	   if (!empty($timezone)){	   	   
	   	       date_default_timezone_set($timezone);	   	   
	   	   }	   	   
	   	   
	   	   $times=$this->getBusinnesHours($mtid);
	   	   if (empty($times)){
	   	   	  return true;
	   	   	  
	   	   }
	   	   /*dump(Yii::app()->timeZone);
	   	   echo date('c');*/
	   	   //dump($times);
							   	   
			$now = strtotime(date("h:ia"));
			$open = $this->isOpen($now, $times);
	
			if ($open == 0) {
				//echo "is close";
			    return false;
			} else {
			    //echo "Is open. Will close in ".ceil($open/60)." minutes";
			    return true;
			}	   	   
	   }
	   return false;	   	
	} 
		
	function compileHours($times, $timestamp) {
	    //$times = $times[strtolower(date('D',$timestamp))];
	    $times = isset($times[strtolower(date('D',$timestamp))])?$times[strtolower(date('D',$timestamp))]:'';	
	    if(!strpos($times, '-')) return array();
	    $hours = explode(",", $times);
	    $hours = array_map('explode', array_pad(array(),count($hours),'-'), $hours);
	    $hours = array_map('array_map', array_pad(array(),count($hours),'strtotime'), $hours, array_pad(array(),count($hours),array_pad(array(),2,$timestamp)));
	    end($hours);
	    if ($hours[key($hours)][0] > $hours[key($hours)][1]) $hours[key($hours)][1] = strtotime('+1 day', $hours[key($hours)][1]);
	    return $hours;
	}
	
	function isOpen($now, $times) {
	    $open = 0;
	    $hours = array_merge($this->compileHours($times, strtotime('yesterday',$now)),$this->compileHours($times, $now)); 		
	    foreach ($hours as $h) {
	        if ($now >= $h[0] and $now < $h[1]) {
	            $open = $h[1] - $now;
	            return $open;
	        } 
	    }
	    return $open;
	}	

	public function getBusinnesHours($merchant_id='')
	{
		$stores_open_day=Yii::app()->functions->getOption("stores_open_day",$merchant_id);
		$stores_open_day=Yii::app()->functions->getOption("stores_open_day",$merchant_id);
		$stores_open_starts=Yii::app()->functions->getOption("stores_open_starts",$merchant_id);
		$stores_open_ends=Yii::app()->functions->getOption("stores_open_ends",$merchant_id);
		$stores_open_custom_text=Yii::app()->functions->getOption("stores_open_custom_text",$merchant_id);
		
		$stores_open_day=!empty($stores_open_day)?(array)json_decode($stores_open_day):false;
		$stores_open_starts=!empty($stores_open_starts)?(array)json_decode($stores_open_starts):false;
		$stores_open_ends=!empty($stores_open_ends)?(array)json_decode($stores_open_ends):false;
				
		
		$stores_open_pm_start=Yii::app()->functions->getOption("stores_open_pm_start",$merchant_id);
		$stores_open_pm_start=!empty($stores_open_pm_start)?(array)json_decode($stores_open_pm_start):false;
		
		$stores_open_pm_ends=Yii::app()->functions->getOption("stores_open_pm_ends",$merchant_id);
		$stores_open_pm_ends=!empty($stores_open_pm_ends)?(array)json_decode($stores_open_pm_ends):false;		
		
		$business_hours='';
		if (is_array($stores_open_day) && count($stores_open_day)>=1){
		   foreach ($stores_open_day as $days) {		   	  
		   	  $days1=substr($days,0,3);
		   	  $start=''; $end='';
		   	  if (array_key_exists($days,$stores_open_starts)){		   	 
		   	  	 if (!empty($stores_open_starts[$days])){
		   	  	     $start=date("h:i A",strtotime($stores_open_starts[$days]));
		   	  	 }
		   	  }
		   	  if (array_key_exists($days,(array)$stores_open_ends)){		 
		   	  	 if (!empty($stores_open_ends[$days]))  	  {
		   	  	     $end=date("h:i A",strtotime($stores_open_ends[$days]));
		   	  	 }
		   	  }
		   	  
		   	  $pm_starts=''; $pm_ends='';
		   	  if (array_key_exists($days,(array)$stores_open_pm_start)){		   	  
		   	  	if (!empty($stores_open_pm_start[$days])){
		   	  	    $pm_starts=date("h:i A",strtotime($stores_open_pm_start[$days]));
		   	  	}
		   	  }
		   	  if (array_key_exists($days,(array)$stores_open_pm_ends)){		   	  
		   	  	 if (!empty($stores_open_pm_ends[$days])){
		   	  	    $pm_ends=date("h:i A",strtotime($stores_open_pm_ends[$days]));
		   	  	 }
		   	  }
		   	  			   	  	   	 
		   	  if (!empty($start) && !empty($end)){		   	  	
		   	  	  $business_hours[$days1]="$start - $end";
		   	  }
		   	  if (!empty($pm_starts) && !empty($pm_ends)){
		   	  	  $business_hours[$days1].=",$pm_starts - $pm_ends";
		   	  }
		   }
		}							
				
		if (is_array($business_hours) && count($business_hours)>=1){
			return $business_hours;
		} else return false;
	}
	
    public function dateTranslation()
    {
    	return array(
    	  'January'=>Yii::t("default","January"),
    	  'February'=>Yii::t("default","February"),
    	  'March'=>Yii::t("default","March"),
    	  'April'=>Yii::t("default","April"),
    	  'May'=>Yii::t("default","May"),
    	  'June'=>Yii::t("default","June"),
    	  'July'=>Yii::t("default","July"),
    	  'August'=>Yii::t("default","August"),
    	  'September'=>Yii::t("default","September"),
    	  'October'=>Yii::t("default","October"),
    	  'November'=>Yii::t("default","November"),
    	  'December'=>Yii::t("default","December"),
    	  'Jan'=>Yii::t("default","Jan"),
    	  'Feb'=>Yii::t("default","Feb"),
    	  'Mar'=>Yii::t("default","Mar"),
    	  'Apr'=>Yii::t("default","Apr"),
    	  'May'=>Yii::t("default","May"),
    	  'Jun'=>Yii::t("default","Jun"),
    	  'Jul'=>Yii::t("default","Jul"),
    	  'Aug'=>Yii::t("default","Aug"),
    	  'Sep'=>Yii::t("default","Sep"),
    	  'Oct'=>Yii::t("default","Oct"),
    	  'Nov'=>Yii::t("default","Nov"),
    	  'Dec'=>Yii::t("default","Dec"),
    	  'Sunday'=>t("Sunday"),
    	  'Monday'=>t("Monday"),
    	  'Tuesday'=>t("Tuesday"),
    	  'Wednesday'=>t("Wednesday"),
    	  'Thursday'=>t("Thursday"),
    	  'Friday'=>t("Friday"),
    	  'Saturday'=>t("Saturday"),
    	  'Sun'=>Yii::t("default","Sun"),
    	  'Mon'=>Yii::t("default","Mon"),
    	  'Tue'=>Yii::t("default","Tue"),
    	  'Wed'=>Yii::t("default","Wed"),
    	  'Thu'=>Yii::t("default","Thu"),
    	  'Fri'=>Yii::t("default","Fri"),
    	  'Sat'=>Yii::t("default","Sat"),
    	  'Su'=>Yii::t("default","Su"),
    	  'Mo'=>Yii::t("default","Mo"),
    	  'Tu'=>Yii::t("default","Tu"),
    	  'We'=>Yii::t("default","We"),
    	  'Th'=>Yii::t("default","Th"),
    	  'Fr'=>Yii::t("default","Fr"),
    	  'Sa'=>Yii::t("default","Sa"),
    	  
    	  'day'=>Yii::t("default","day"),
    	  'days'=>Yii::t("default","days"),
    	  'week'=>Yii::t("default","week"),
    	  'weeks'=>Yii::t("default","weeks"),
    	  'month'=>Yii::t("default","month"),
    	  'months'=>Yii::t("default","months"),
    	  'ago'=>Yii::t("default","ago"),
    	  'In'=>Yii::t("default","In"),
    	  'minute'=>Yii::t("default","minute"),
    	  'hour'=>Yii::t("default","hour"),
    	);
    }
    
    public function translateDate($date='')
    {    	    	
    	$translate=$this->dateTranslation();    	
    	foreach ($translate as $key=>$val) {    		
    		$date=str_replace($key,$val,$date);
    	}
    	return $date;
    }
    
    public function orderStatusList2($aslist=true)
    {
    	$mid=$this->getMerchantID();
    	$list='';
    	if ($aslist){
    	    $list[]=Yii::t("default","Please select");    	
    	}
    	$db_ext=new DbExt;
    	$stmt="SELECT * FROM 
    	  {{order_status}}     	      	 
    	  ORDER BY stats_id";	    	
    	if ($res=$db_ext->rst($stmt)){
    		foreach ($res as $val) {    			    			
    			$list[$val['description']]=ucwords($val['description']);
    		}
    		return $list;
    	}
    	return false;    
    }        
    
    public function offlineBankDeposit($merchant='',$data='')
    {    
    	    	
    	if (isset($_REQUEST['renew'])){ 
	    	$package_price=0;
	    	$membership_expired='';
	    	$membership_info=Yii::app()->functions->upgradeMembership($merchant['merchant_id'],$data['package_id']);
	    	$merchant_email=$merchant['contact_email'];
	    	$package_id=$data['package_id'];
	    	
	    	if (is_array($membership_info) && count($membership_info)>=1){
    		   $package_price=$membership_info['package_price'];
    		   $membership_expired=$membership_info['membership_expired'];
    	    }    	
	    	
    	} else {
    		$merchant_email=$merchant['contact_email'];
    		$package_id=$merchant['package_id'];
    		$package_price=$merchant['package_price'];
    		$membership_expired=$merchant['membership_expired'];
    	}

    	   	
    	$subject=Yii::app()->functions->getOptionAdmin('admin_deposit_subject');
    	$from=Yii::app()->functions->getOptionAdmin('admin_deposit_sender');
    	
    	if (empty($from)){
    	    $from='no-reply@'.$_SERVER['HTTP_HOST'];
    	}
    	if (empty($subject)){
    	    $subject=Yii::t("default","Bank Deposit instructions");
    	}
    	    	
    	$to=$merchant_email; 
    	
    	$link=Yii::app()->getBaseUrl(true)."/store/bankdepositverify/?ref=".$merchant['activation_token'];
    	$links="<a href=\"$link\" target=\"_blank\" >".Yii::t("default","Click on this link")."</a>";
    	$tpl=Yii::app()->functions->getOptionAdmin('admin_deposit_instructions');
    	if (!empty($tpl)){    	   
    	   $tpl=$this->smarty('amount',
    	   $this->adminCurrencySymbol().$this->standardPrettyFormat($package_price),$tpl);
    	   $tpl=$this->smarty('verify-payment-link',$links,$tpl);
    	   if ($this->sendEmail($to,$from,$subject,$tpl)){
    	   	   $params=array('payment_steps'=>3);
    	   	   $db_ext=new DbExt;
    	   	   $db_ext->updateData("{{merchant}}",$params,'merchant_id',$merchant['merchant_id']);
    	   	   
    	   	   $params2=array(
    	   	     'package_id'=>$package_id,
    	   	     'merchant_id'=>$merchant['merchant_id'],
    	   	     'price'=>$package_price,
    	   	     'payment_type'=>'obd',
    	   	     'membership_expired'=>$membership_expired,
    	   	     'date_created'=>date('c'),
    	   	     'ip_address'=>$_SERVER['REMOTE_ADDR']
    	   	   );
    	   	   $db_ext->insertData("{{package_trans}}",$params2);
    	   	   return true;
    	   }	    	   	
    	}
    	return false;
    }
    
    public function getMerchantListOfPaymentGateway()
    {
    	$db_ext=new DbExt;
    	$paymentgateway=$this->getOptionAdmin('paymentgateway');
		if (!empty($paymentgateway)){
			$paymentgateway=json_decode($paymentgateway,true);
		} else {
			$paymentgateway=array();
		}
		return $paymentgateway;
    }

    public function currencyPosition()
    {
    	return array(
    	   'left'=>t("Left"),
    	   'right'=>t("Right"),
    	);
    }  
    
    public function displayPrice($currency='',$amount='')
    {    	
    	$pos=Yii::app()->functions->getOptionAdmin('admin_currency_position');    	
    	if ( $pos=="right"){
    		return $amount." ".$currency;
    	} else {    		
    		return $currency." ".$amount;
    	}
    }
    
    
	public function CountryListMerchant()
	{
		  $country_list=$this->CountryList();
		  $merchant_default_country=Yii::app()->functions->getOptionAdmin('merchant_default_country');  
		  $merchant_specific_country=Yii::app()->functions->getOptionAdmin('merchant_specific_country');
		  if (!empty($merchant_specific_country)){
			$merchant_specific_country=json_decode($merchant_specific_country);
		  }    
		  if (is_array($merchant_specific_country) && count($merchant_specific_country)>=1){  	  
		  	 $country_list_tem=$country_list;
		  	  $country_list='';  	
		  	  foreach ($country_list_tem as $c_key=>$c_value) {  	  	   	  	 
		  	  	 if ( in_array($c_key,$merchant_specific_country)){
		  	  	 	$country_list[$c_key]=$c_value;
		  	  	 }
		  	  }
		  }  
		  return $country_list;
	}
	
	public function getWebsiteName()
	{
		return $this->getOptionAdmin('website_title');
	}
	
	public function getMerchantHoliday($merchant_id='')
	{
		$merchant_holiday=Yii::app()->functions->getOption("merchant_holiday",$merchant_id);
		if (!empty($merchant_holiday)){
			$merchant_holiday=json_decode($merchant_holiday,true);
			if (is_array($merchant_holiday) && count($merchant_holiday)>=1){
			   return $merchant_holiday;	
			}			
		}		
		return false;
	}
	
	public function isAdminExist($contact_email='')
	{
		$DbExt=new DbExt;
		$stmt="SELECT * FROM
		{{admin_user}}
		WHERE
		email_address='".$contact_email."'
		LIMIT 0,1
		";
		if ( $res=$DbExt->rst($stmt)){
			return $res;
		}
		return false;
	}	
	
	public function updateMerchantToken($merchant_id='')
	{
		$token=$this->generateRandomKey();
		$token=md5($token);
		$DbExt=new DbExt;
		$params=array('activation_token'=>$token);
		if ( $DbExt->updateData("{{merchant}}",$params,'merchant_id',$merchant_id)){
			return $token;
		}
		return false;
			
	}
	
	public function getPaymentProvider($id='')
	{
		$DbExt=new DbExt;
		$stmt="SELECT * FROM
		{{payment_provider}}
		WHERE
		id='$id'
		LIMIT 0,1
		";		
		if ($res=$DbExt->rst($stmt)){			
			return $res[0];
		}
		return false;	
	}	
	
	public function getPaymentProviderList()
	{
		$DbExt=new DbExt;
		$stmt="SELECT * FROM
		{{payment_provider}}
		ORDER BY sequence ASC	
		";		
		if ($res=$DbExt->rst($stmt)){			
			return $res;
		}
		return false;	
	}		
	
	public function getPaymentProviderListActive()
	{
		$DbExt=new DbExt;
		$stmt="SELECT * FROM
		{{payment_provider}}
		WHERE
		status IN ('publish','published')
		ORDER BY sequence ASC	
		";		
		if ($res=$DbExt->rst($stmt)){			
			return $res;
		}
		return false;	
	}	

	public function getPaymentProviderMerchant($merchant_id='')
	{
		$provider='';
		$payondeliver_list=Yii::app()->functions->getOption('payment_provider',$merchant_id);
        if (!empty($payondeliver_list)){
        	$payondeliver_list=json_decode($payondeliver_list);
        	if (is_array($payondeliver_list) && count($payondeliver_list)>=1){
        		foreach ($payondeliver_list as $val) {
        			if ( $res=$this->getPaymentProvider($val)){        			    
        			    $provider[]=array(
        			      'id'=>$res['id'],
        			      'payment_name'=>$res['payment_name'],        			      
        			      'payment_logo'=>$res['payment_logo'],
        			    );
        			}
        		}
        	}	   
        	
        	if (is_array($provider) && count($provider)>=1) {
        		return $provider;
        	}        		
        }	
        return false;        
	}
	
	public function q($data='')
	{
		return Yii::app()->db->quoteValue($data);
	}	
	
	public function getMerchantReceiptTemplate($merchant_id='')
	{
		$tpl2=Yii::app()->functions->getOption("merchant_receipt_content",$merchant_id);
		if (empty($tpl2)){	
			$tpl2=EmailTPL::receiptMerchantTPL();
		}
		return $tpl2;
	}
    
	public function getMerchantActivationToken($merchant_id='')
	{
		$DbExt=new DbExt;
		$stmt="SELECT activation_token FROM
		{{merchant}}
		WHERE
		merchant_id=".$this->q($merchant_id)."
		LIMIT 0,1
		";
		if ( $res=$DbExt->rst($stmt)){			
			if ( empty($res[0]['activation_token'])){
				$token=$this->updateMerchantToken($merchant_id);
			} else $token=$res[0]['activation_token'];
			return $token;
		}
		return false;			
	}
	
    public function getFeatureMerchant2()
    {
    	$db_ext=new DbExt; 
    	$stmt="SELECT a.*,  
    	    (
	    	select option_value
	    	from 
	    	{{option}}
	    	WHERE
	    	merchant_id=a.merchant_id
	    	and
	    	option_name='merchant_photo'
	    	limit 0,1
	    	) as merchant_logo
	    	    	
    	    FROM
			{{merchant}} a
			WHERE
			status in ('active')
			AND
			is_featured='2'
			ORDER BY merchant_name ASC
			LIMIT 0,20
		";    	
    	//is_sponsored='2'
    	if ( $res=$db_ext->rst($stmt)){    		
    		return $res;
    	}
    	return false;    	
    }
    
	function createLogs($response='',$filename=''){    
		$path_to_upload=Yii::getPathOfAlias('webroot')."/upload/logs";
	    if(!file_exists($path_to_upload)) {	
           if (!@mkdir($path_to_upload,0777)){           	    
           	    return ;
           }		    
	    }	   
	    $myFile=$path_to_upload;
	    $myFile.= "/$filename-". date("Y-m-d") . '.txt';            
	    $fh = @fopen($myFile, 'a');
	    $stringData .= 'URL=>'.$_SERVER['REQUEST_URI'] . "\n";    
	    $stringData .= 'IP ADDRESS=>'.$_SERVER['REMOTE_ADDR'] . "\n";     
	    $stringData .= 'DATE =>'.date("Y-m-d g:h i") . "\n";     
	    $stringData .= 'POST VAR=>'. json_encode($_POST) . "\n";  
	    $stringData .= 'GET VAR=>'. json_encode($_GET) . "\n";  
	    $stringData .= 'RESPONSE =>'. json_encode($response) . "\n";  
	    $stringData .=  "\n"; 
	    fwrite($fh, $stringData);                         
	    fclose($fh); 
	}
	
	public function getMerchantMembershipType()
	{
		if (!empty($_SESSION['kr_merchant_user'])){
			$user=json_decode($_SESSION['kr_merchant_user']);			
			if (is_array($user) && count($user)>=1){
				return $user[0]->is_commission;
			}
		}
		return false;
	}
	
	public function isMerchantCommission($merchant_id='')
	{
		
		$stmt="
		SELECT * FROM
		{{merchant}}
		WHERE
		merchant_id=".$this->q($merchant_id)."
		LIMIT 0,1
		";		
		if ( $res=$this->db_ext->rst($stmt)){
			if ($res[0]['is_commission']==2){
				return true;				
			}				
		}
		return false;
	}
	
	public function getMerchantCommission($merchant_id='')
	{				
		$stmt="
		SELECT * FROM
		{{merchant}}
		WHERE
		merchant_id=".$this->q($merchant_id)."
		LIMIT 0,1
		";		
		if ( $res=$this->db_ext->rst($stmt)){
			return $res[0]['percent_commision'];
		}
		return false;
	}
		
   public function merchantList2($as_list=true)
    {
    	$data='';    	
    	$stmt="SELECT * FROM
    	{{merchant}}
    	WHERE status in ('active')
    	AND
    	is_commission='2'
    	ORDER BY merchant_name ASC
    	";
    	$data[]=t("All Merchant");
    	if ($res=$this->db_ext->rst($stmt)){    		
    		if ( $as_list==TRUE){
    			foreach ($res as $val) {    				
    			    $data[$val['merchant_id']]=ucwords($val['merchant_name']);
    			}
    			return $data;
    		} else return $res;    	
    	}
    	return false;
    }	
    
    public function getTotalCommission()
    {
    	$total_commission_status=Yii::app()->functions->getOptionAdmin('total_commission_status');
		if (!empty($total_commission_status)){
			$total_commission_status=json_decode($total_commission_status);
		} else {
			$total_commission_status=array('paid');
		}    	
    	$status='';
    	if (is_array($total_commission_status) && count($total_commission_status)>=1){
    		foreach ($total_commission_status as $val) {    			
    			$status.="'$val',";
    		}
    		$status=substr($status,0,-1);
    	} else $status="'paid'";

    	$and='';
    	if ( Yii::app()->functions->getOptionAdmin('admin_exclude_cod_balance')==2){
    		$and=" AND payment_type NOT IN ('cod','pyr','ccr')";    
    	}
    	
    	$stmt="SELECT sum(total_commission) as total_commission
    	FROM
    	{{order}}
    	WHERE status IN ($status)
    	$and
    	";    	
    	//dump($stmt);
    	if ( $res=$this->db_ext->rst($stmt)){
    		if ( $res[0]['total_commission']==""){
    			return 0;
    		} 
    		return $res[0]['total_commission'];    			
    	}	
    	return false;    	
    }
    
   public function getTotalCommissionToday()
    {
    	$total_commission_status=Yii::app()->functions->getOptionAdmin('total_commission_status');
		if (!empty($total_commission_status)){
			$total_commission_status=json_decode($total_commission_status);
		} else {
			$total_commission_status=array('paid');
		}    	
    	$status='';
    	if (is_array($total_commission_status) && count($total_commission_status)>=1){
    		foreach ($total_commission_status as $val) {    			
    			$status.="'$val',";
    		}
    		$status=substr($status,0,-1);
    	} else $status="'paid'";
    	    	        
    	$start_date=date("Y-m-d");
    	$end_date=date("Y-m-d");
    	$and=" AND date_created BETWEEN  '".$start_date." 00:00:00' AND 
	    		        '".$end_date." 23:59:00'
	    		 ";	    		
    	    	
    	
    	
    	if ( Yii::app()->functions->getOptionAdmin('admin_exclude_cod_balance')==2){
    		$and.=" AND payment_type NOT IN ('cod','pyr','ccr')";
    	}
    	
    	$stmt="SELECT sum(total_commission) as total_commission
    	FROM
    	{{order}}
    	WHERE status IN ($status)    	
    	$and
    	";    	        	
    	if ( $res=$this->db_ext->rst($stmt)){    		
    		if ( $res[0]['total_commission']==""){
    			return 0;
    		} 
    		return $res[0]['total_commission'];    			
    	}	
    	return false;    	
    }    
    

   public function getTotalCommissionLast()
    {
    	$total_commission_status=Yii::app()->functions->getOptionAdmin('total_commission_status');
		if (!empty($total_commission_status)){
			$total_commission_status=json_decode($total_commission_status);
		} else {
			$total_commission_status=array('paid');
		}    	
    	$status='';
    	if (is_array($total_commission_status) && count($total_commission_status)>=1){
    		foreach ($total_commission_status as $val) {    			
    			$status.="'$val',";
    		}
    		$status=substr($status,0,-1);
    	} else $status="'paid'";
    	    	        
    	$end_date=date("Y-m-d");
    	$start_date=date('Y-m-d', strtotime ('-30 days'));				
    	$and=" AND date_created BETWEEN  '".$start_date." 00:00:00' AND 
	    		        '".$end_date." 23:59:00'
	    		 ";	    		
    	    	
    	if ( Yii::app()->functions->getOptionAdmin('admin_exclude_cod_balance')==2){
    		$and.=" AND payment_type NOT IN ('cod','pyr','ccr')";
    	}
    	
    	$stmt="SELECT sum(total_commission) as total_commission
    	FROM
    	{{order}}
    	WHERE status IN ($status)    	
    	$and
    	";    	    	
    	if ( $res=$this->db_ext->rst($stmt)){    		
    		if ( $res[0]['total_commission']==""){
    			return 0;
    		} 
    		return $res[0]['total_commission'];    			
    	}	
    	return false;    	
    }        
	    
	public function seo_friendly_url($string){
	    $string = str_replace(array('[\', \']'), '', $string);
	    $string = preg_replace('/\[.*\]/U', '', $string);
	    $string = preg_replace('/&(amp;)?#?[a-z0-9]+;/i', '-', $string);
	    $string = htmlentities($string, ENT_COMPAT, 'utf-8');
	    $string = preg_replace('/&([a-z])(acute|uml|circ|grave|ring|cedil|slash|tilde|caron|lig|quot|rsquo);/i', '\\1', $string );
	    $string = preg_replace(array('/[^a-z0-9]/i', '/[-]+/') , '-', $string);
	    return strtolower(trim($string, '-'));
	}   


    public function getMerchantBalance($merchant_id='')
    {	
		
       $stmt="
		SELECT a.merchant_id,a.merchant_name,(total_earning-total_withdrawal) as saldo,
		IFNULL(j.total_earning, 0) total_earning,IFNULL(k.total_withdrawal, 0) total_withdrawal
		FROM 
		{{merchant}} a 
		LEFT JOIN 
		(
		select d.merchant_id,sum(d.earning) as total_earning
		from 
		{{order_merchant}} d
		JOIN {{order}} z
		ON z.order_id=d.order_id
		WHERE z.status='successful' 
		AND d.merchant_id=$merchant_id 
		GROUP BY merchant_id 
		) as j ON j.merchant_id=a.merchant_id 
		
		LEFT JOIN (
		select merchant_id,sum(amount) as total_withdrawal
		from 
		{{withdrawal}} w 
		WHERE status='paid' 
		AND w.merchant_id=$merchant_id
		GROUP BY merchant_id 
		) as k ON k.merchant_id=a.merchant_id 
		WHERE a.merchant_id=$merchant_id
		ORDER BY merchant_name ASC
		";
		
		$val = query($stmt);
		$balance = $val[0]['total_earning']-$val[0]['total_withdrawal'];
		queryNoFetch('UPDATE {{merchant}} SET saldo=? WHERE merchant_id=?',array($balance,$merchant_id));
		return $balance;
	}	
    
   public function getMerchantBalanceThisMonth($merchant_id='')
   {
    	
    	$status=$this->getCommissionOrderStats();
    	
    	$query_date = date("Y-m-d");
		$start_date=date('Y-m-01', strtotime($query_date));
		$end_date=date('Y-m-t', strtotime($query_date));
		$and =" AND date_created BETWEEN  '".$start_date." 00:00:00' AND 
    		        '".$end_date." 23:59:00'
    		 ";	    		
    	
    	$stmt="SELECT sum(total_commission) as total_commission,
    	sum(total_w_tax) as total_w_tax,
    	count(*) as total_order
    	FROM
    	{{order}}
    	WHERE status IN ($status)
    	AND merchant_id=".Yii::app()->functions->q($merchant_id)."
    	$and
    	";    	       	
    	if ( $res=$this->db_ext->rst($stmt)){    		    		
    		return $res[0];
    	}	
    	return false;    	
    }	
    
   public function getMerchantTotalSales($merchant_id='')
   {    	
   	    $status=$this->getCommissionOrderStats();
    	$stmt="SELECT 
    	sum(total_w_tax) as total_w_tax,
    	count(*) as total_order
    	FROM
    	{{order}}
    	WHERE status IN ($status)
    	AND merchant_id=".Yii::app()->functions->q($merchant_id)."    	
    	";       	
    	if ( $res=$this->db_ext->rst($stmt)){    		    		
    		return $res[0];
    	}	
    	return false;    	
    }	    
        
    public function getCommissionOrderStats()
    {
    	$total_commission_status=Yii::app()->functions->getOptionAdmin('total_commission_status');
		if (!empty($total_commission_status)){
			$total_commission_status=json_decode($total_commission_status);
		} else {
			$total_commission_status=array('paid');
		}    	
    	$status='';
    	if (is_array($total_commission_status) && count($total_commission_status)>=1){
    		foreach ($total_commission_status as $val) {    			
    			$status.="'$val',";
    		}
    		$status=substr($status,0,-1);
    	} else $status="'paid'";
    	
    	return $status;
    }
    
    public function getCommissionOrderStatsArray()
    {
    	$total_commission_status=Yii::app()->functions->getOptionAdmin('total_commission_status');
		if (!empty($total_commission_status)){
			$total_commission_status=json_decode($total_commission_status);
		} else {
			$total_commission_status=array('paid');
		}    	
    	/*$status='';
    	if (is_array($total_commission_status) && count($total_commission_status)>=1){
    		foreach ($total_commission_status as $val) {    			
    			$status.="'$val',";
    		}
    		$status=substr($status,0,-1);
    	} else $status="'paid'";*/
    	
    	return $total_commission_status;
    }
        
    public function getLastTwoMonths()
    {
    	$a=date("F Y"); 
    	$b=date("F Y",strtotime("-1 Months")); 
    	$c=date("F Y",strtotime("-2 Months")); 
    	return array(
    	  date("Y-m-d")=>$a,
    	  date("Y-m-d",strtotime("-1 Months"))=>$b,
    	  date("Y-m-d",strtotime("-2 Months"))=>$c
    	);
    }
    
    public function payoutRequest($payment_method='',$data='')
    {
    	$wd_days_process=Yii::app()->functions->getOptionAdmin("wd_days_process");
		
    	if (!is_numeric($wd_days_process)){
    		$wd_days_process=5;
    	}
        $process_date=date("Y-m-d", strtotime (" +$wd_days_process days"));
    	switch ($payment_method) {

    		case "bank":    
    		    $wd_bank_fields=yii::app()->functions->getOptionAdmin('wd_bank_fields');	    		    
    		    $mtid=Yii::app()->functions->getMerchantID();			        
    			$params=array(
    			  'merchant_id'=>$this->getMerchantID(),
    			  'payment_type'=>$data['payment_type'],
    			  'payment_method'=>$data['payment_method'],
    			  'amount'=>$data['amount'],
    			  'date_created'=>date('c'),
    			  'account_name'=>$data['account_name'],
    			  'bank_account_number'=>$data['bank_account_number'],
    			  'bank_name'=>isset($data['bank_name'])?$data['bank_name']:'',
    			  'bank_branch'=>isset($data['bank_branch'])?$data['bank_branch']:'',
    			  'date_to_process'=>$process_date,
    			  'date_to_process'=>$process_date,
				  'current_balance'=>$data['current_balance'],
    			);    	    			
    			if (!empty($wd_bank_fields)){
    				$params['bank_type']=$wd_bank_fields;
    			}    		
    			    		    			
    			if (isset($data['default_account_bank'])){
	    			if ( $data['default_account_bank']==2){
	    				Yii::app()->functions->updateOption("merchant_payout_bank_account",
    	                json_encode($params),$this->getMerchantID());
	    			}
    			}
    				
    			if ( $this->db_ext->insertData("{{withdrawal}}",$params)){
    				//return Yii::app()->db->getLastInsertID();
    				return array(
    				  'id'=>Yii::app()->db->getLastInsertID(),
    				  'token'=>$token
    				);
    			}    			
    			break;
    			    			
    		default:
    			break;
    	}
    	
    	return true;
    }   
    
    public function getWithdrawalInformation($id='')
    {
    	$stmt="SELECT * FROM
    	{{withdrawal}}
    	WHERE
    	withdrawal_id=".$this->q($id)."
    	LIMIT 0,1
    	";
    	if ( $res=$this->db_ext->rst($stmt)){
    		return $res[0];
    	}
    	return false;    		
    }
    
    public function getWithdrawalInformationByToken($token='')
    {
    	$stmt="SELECT * FROM
    	{{withdrawal}}
    	WHERE
    	withdrawal_token=".$this->q($token)."
    	LIMIT 0,1
    	";
    	if ( $res=$this->db_ext->rst($stmt)){
    		return $res[0];
    	}
    	return false;    		
    }    
    
    public function getMerchantWithdrawal($merchant_id='',$status='')
    {    	
    	$and="";
    	$temp_status='';
    	if ( is_array($status) && count($status)>=1){
    		foreach ($status as $val) {
    			$temp_status.="'$val',";
    		}    		
    		$temp_status=substr($temp_status,0,-1);
    		$and=" AND status IN ($temp_status) ";
    	}
    	$stmt="SELECT * FROM
    	{{withdrawal}}
    	WHERE
    	merchant_id=".$this->q($merchant_id)."
    	$and
    	ORDER BY withdrawal_id DESC    	
    	";    	
    	if ( $res=$this->db_ext->rst($stmt)){
    		return $res;
    	}
    	return false;    		
    }    


    public function getMerchantFailedWithdrawal($merchant_id='')
    {    	
    	$and="AND status NOT IN ('paid','pending','approved')";    	
    	$stmt="SELECT * FROM
    	{{withdrawal}}
    	WHERE
    	merchant_id=".$this->q($merchant_id)."
    	$and
    	ORDER BY withdrawal_id DESC    	
    	";    	
    	if ( $res=$this->db_ext->rst($stmt)){
    		return $res;
    	}
    	return false;    		
    }    
        
    public function withdrawalStatus()
    {
    	return array(
    	   'pending'=>t("pending"),
    	   'paid'=>t("paid"),
    	   'cancel'=>t("cancel"),
    	   'reversal'=>t("reversal"),
    	   'denied'=>t("denied"),
    	   'processing'=>t("processing")
    	);
    }

    public function displayDate($date_to_format='')
    {
    	if ( $date_to_format==""){
    		return '';
    	} else {
	    	$date=date('M d,Y G:i:s',strtotime($date_to_format));  
	        $date=Yii::app()->functions->translateDate($date);
	        return $date;
    	}
    }
    
    function dateDifference($start, $end )
    {
        $uts['start']=strtotime( $start );
		$uts['end']=strtotime( $end );
		if( $uts['start']!==-1 && $uts['end']!==-1 )
		{
		if( $uts['end'] >= $uts['start'] )
		{
		$diff    =    $uts['end'] - $uts['start'];
		if( $days=intval((floor($diff/86400))) )
		    $diff = $diff % 86400;
		if( $hours=intval((floor($diff/3600))) )
		    $diff = $diff % 3600;
		if( $minutes=intval((floor($diff/60))) )
		    $diff = $diff % 60;
		$diff    =    intval( $diff );            
		return( array('days'=>$days, 'hours'=>$hours, 'minutes'=>$minutes, 'seconds'=>$diff) );
		}
		else
		{			
		return false;
		}
		}
		else
		{			
		return false;
		}
		return( false );
     }    
     
     public function validateMerchantUserFromMerchantUser($username='',$email='',$id='')
     {
     	$and="";    	        	    	
    	$msg='';
    	
     	$stmt1="SELECT * FROM
     	{{merchant_user}}
     	WHERE
     	username=".$this->q($username)."
     	$and
     	LIMIT 0,1
     	";     	
     	if ($res1=$this->db_ext->rst($stmt1)){
     		$msg=t("Username already exist");
     	}
     	
     	$stmt1="SELECT * FROM
     	{{merchant_user}}
     	WHERE
     	contact_email=".$this->q($email)."
     	$and
     	LIMIT 0,1
     	";     	
     	if ($res1=$this->db_ext->rst($stmt1)){
     		$msg=t("Email address already exist");
     	}
     	     	
     	if (empty($msg)){
     		return false;
     	}     	
     	return $msg;
     }  
     
    public function merchantList3($as_list=true,$with_select=false)
    {
    	$data='';
    	$DbExt=new DbExt;
    	$stmt="SELECT * FROM
    	{{merchant}}    	
    	ORDER BY merchant_name ASC
    	";
    	if ( $with_select){
    		//$data[]=t("Please select");
    		$data[]=t("All Merchant");
    	}
    	if ($res=$DbExt->rst($stmt)){    		
    		if ( $as_list==TRUE){
    			foreach ($res as $val) {    				
    			    $data[$val['merchant_id']]=ucwords($val['merchant_name']);
    			}
    			return $data;
    		} else return $res;    	
    	}
    	return false;
    }     
        
    public function getBankDepositInstruction()
    {
    	$sender=$this->getOptionAdmin("admin_deposit_sender");
    	$subject=$this->getOptionAdmin("admin_deposit_subject");
    	$content=$this->getOptionAdmin("admin_deposit_instructions");
    	return array(
    	  'sender'=>$sender,
    	  'subject'=>$subject,
    	  'content'=>$content
    	);
    }
    
    public function validateAdminSession()
    {
    	$this->has_session=false;
    	if(isset($_SESSION['kr_user_session'])){
    		
    		$allowed=$this->getOptionAdmin('website_admin_mutiple_login');    		
    		if ( $allowed==""){    			
    			if (empty($_SESSION['kr_user_session'])){
    				return false;
    			}
    			return true;
    		}
    		
    		$admin_id=$this->getAdminId();
    		$kr_user_session=$_SESSION['kr_user_session'];
    		$stmt="SELECT session_token
    		FROM {{admin_user}}
    		WHERE
    		admin_id=".$this->q($admin_id)."
    		LIMIT 0,1
    		";    		
    		if ($res=$this->db_ext->rst($stmt)){    			
    			if ( $kr_user_session==$res[0]['session_token']){
    				return true;
    			}
    			$this->has_session=true;
    		}
    	}
    	return false;
    }
    
    public function validateMerchantSession()
    {
    	$this->has_session=false;
    	if(isset($_SESSION['kr_merchant_user_session'])){
    		
    		$merchant_id=$this->getMerchantID();
    		$session=$_SESSION['kr_merchant_user_session'];
    		
    		$allowed=$this->getOptionAdmin('website_merchant_mutiple_login');      			
    		if ( $allowed==""){
    			if (empty($_SESSION['kr_merchant_user_session'])){
    				return false;
    			}
    			return true;
    		}
    		    		    		    		
    		if ( $_SESSION['kr_merchant_user_type']=="admin"){
    			$stmt="SELECT session_token
	    		FROM {{merchant}}
	    		WHERE
	    		merchant_id=".$this->q($merchant_id)."
	    		LIMIT 0,1
	    		";    	
    		} else {
    			$merchant_user_id='';
    		    $user_info=json_decode($_SESSION['kr_merchant_user'],true);
    		    if (is_array($user_info) && count($user_info)>=1){
    			    $merchant_user_id=$user_info[0]['merchant_user_id'];
    		    }
    		
	    		$stmt="SELECT session_token
	    		FROM {{merchant_user}}
	    		WHERE
	    		merchant_user_id=".$this->q($merchant_user_id)."
	    		LIMIT 0,1
	    		";    	
    		}    		
    		if ($res=$this->db_ext->rst($stmt)){    			    			
    			if ( $session==$res[0]['session_token']){
    				return true;
    			}
    			$this->has_session=true;
    		}
    	}
    	return false;
    }    
    
    public function getShippingRates($mtid='')
    {
    	$stmt="SELECT * FROM
    	{{shipping_rate}}
    	WHERE
    	merchant_id=".Yii::app()->functions->q($mtid)."
    	ORDER BY id ASC
    	";
    	if ( $res=$this->db_ext->rst($stmt)){
    		return $res;
    	}
    	return false;
    }     
    
    public function isMerchantOpenTimes($merchant_id='',$full_booking_day='',$booking_time='')
    {
	   $business_hours=Yii::app()->functions->getBusinnesHours($merchant_id);
	   //dump($business_hours);	   
		if (is_array($business_hours) && count($business_hours)>=1){
			if (!array_key_exists($full_booking_day,$business_hours)){
				return false;
			} else {
				if (!empty($booking_time)){
					if (array_key_exists($full_booking_day,$business_hours)){						
						$selected_date=$business_hours[$full_booking_day];										
						//dump($selected_date);
						$temp_selected=explode(",",$selected_date);	
						//dump($temp_selected);	
												
						if(is_array($temp_selected) && count($temp_selected)>=1){							
							if ( empty($temp_selected[0])){
							    if (!empty($temp_selected[1])){
							    	$temp_selected[0]=$temp_selected[1];
							    }
							}
						}
						
						$selected_date=explode("-",$temp_selected[0]);
						//dump($selected_date);
						$t1=trim($selected_date[0]);
						$t2=trim($selected_date[1]);
												
						if ( !Yii::app()->functions->checkBetweenTime($booking_time,$t1,$t2)){	
							if (isset($selected_date[1])){								
								$selected_date=explode("-",$temp_selected[1]);								
								$t1=trim($selected_date[0]);
						        $t2=trim($selected_date[1]);						        
						        if ( Yii::app()->functions->checkBetweenTime($booking_time,$t1,$t2)){
						        	return true;
						        } 
							}							
							return false;
						}
					}
				}
			}
		}							
		return true;							
    }
    
    public function checkBetweenTime($current_time='',$sunrise='',$sunset='')
    {    	
    	/*refference http://stackoverflow.com/questions/15911312/how-to-check-if-time-is-between-two-times-in-php*/    	
      	/*$current_time = "09:55 AM";
        $sunrise = "09:00 AM";
        $sunset = "07:30 PM";*/    
      	/*dump($current_time);   
      	dump($sunrise);
      	dump($sunset);*/
		$date1 = DateTime::createFromFormat('H:i a', $current_time);		
		$date2 = DateTime::createFromFormat('H:i a', $sunrise);		
		$date3 = DateTime::createFromFormat('H:i a', $sunset);		
		if ($date1 > $date2 && $date1 < $date3) {			
		    return true;
		} 
		return false;
    }  

    public function prettyLink($link='')
    {
    	if (!preg_match("/http/i", $link)) {
		   $link="http://".$link;
        } 
        return $link;
    }
    
	public function getMerchantCommissionDetails($merchant_id='')
	{
		
		$stmt="
		SELECT * FROM
		{{merchant}}
		WHERE
		merchant_id=".$this->q($merchant_id)."
		LIMIT 0,1
		";		
		if ( $res=$this->db_ext->rst($stmt)){
			return array(
			  'is_commission'=>$res[0]['is_commission'],
			  'commision_type'=>$res[0]['commision_type'],
			  'percent_commision'=>$res[0]['percent_commision']
			);
		}
		return false;
	}        	

	public function FormatDateTime($date='',$time=true)
	{
		if ($date=="0000-00-00"){
    		return ;
    	}    
    	if ($date=="0000-00-00 00:00:00"){
    		return ;
    	}
    	if ( !empty($date)){    		
    		$date_f=Yii::app()->functions->getOptionAdmin("website_date_format");
    		$time_f=Yii::app()->functions->getOptionAdmin("website_time_format");       		
    		if (!empty($date_f)){
    			if ( $time==TRUE){
    			    $date_ouput = date("$date_f $time_f",strtotime($date));	
    			} else $date_ouput = date("$date_f",strtotime($date));	    			
    			return $this->translateDate($date_ouput);
    		} else {
    			if ( $time==TRUE){
    		        $date_ouput= date('M d,Y G:i:s',strtotime($date));	
    			} else $date_ouput= date('M d,Y',strtotime($date));	
    		    return $this->translateDate($date_ouput);
    		}
    	}
    	return false;
	}
	
	public function timeFormat($time='',$is_display=false)
	{
		if(empty($time)){
			return false;
		}
		
		$time_format=Yii::app()->functions->getOptionAdmin("website_time_picker_format");
		//dump($time_format);	
		switch ($time_format){
			case "12":
				if ( $is_display==true){
					return date("g:i A", strtotime($time));
				} else return date("G:i", strtotime($time));
				break;
			default:
				if ( $is_display==true){
					return date("G:i", strtotime($time));
				} else return date("G:i", strtotime($time));
				break;	
		}
		return $time;
	}
	
	public function sendVerificationCode($mobile='',$code='')
	{		
		$msg=t("Your verificatio code is")." ".$code;;		
		if ( $res = $this->sendSMS($mobile,$msg)){			
			$params=array(
			  'contact_phone'=>$mobile,
			  'sms_message'=>$msg,
			  'status'=>isset($res['msg'])?$res['msg']:'',
			  'gateway_response'=>isset($res['raw'])?$res['raw']:'',
			  'gateway'=>$res['sms_provider'],
			  'date_created'=>date('c'),
			  'ip_address'=>$_SERVER['REMOTE_ADDR']
			);
			$DbExt=new DbExt;
			$DbExt->insertData("{{sms_broadcast_details}}",$params);
			return true;
		}
		return false;
	}
	
    public function getCategoryList2($merchant_id='')
	{
		$join_item = "";
		$and_where = "";
		if (!empty($merchant_id)) {
			$join_item = "JOIN {{item}} b 
							  ON b.category=a.cat_id
							  ";
			$and_where = "AND b.merchant_id=".$merchant_id;
		}
		
		$data_feed='';
		$stmt="
			SELECT 
				a.cat_id,
				a.category_name,
				a.category_name_trans,
				a.category_description_trans,
				a.category_description,
				a.photo,
				a.status,
				a.sequence,
				a.date_created,
				a.date_modified
			FROM
				{{category}} a
			$join_item
			WHERE 
			a.status in ('publish','published')
			$and_where
			GROUP BY a.cat_id
			ORDER BY a.sequence ASC
			
		";
		// dump($stmt);
		$connection=Yii::app()->db;
		$rows=$connection->createCommand($stmt)->queryAll(); 				
		if (is_array($rows) && count($rows)>=1){
			if ($this->data=="list"){
				foreach ($rows as $val) {									   
				   $data_feed[$val['cat_id']]=array(
				     'category_name'=>$val['category_name'],
				     'category_description'=>$val['category_description'],
				     // 'dish'=>$val['dish'],
				     'category_name_trans'=>$val['category_name_trans'],
				     'category_description_trans'=>$val['category_description_trans']
				   );
				}
				return $data_feed;
			} else return $rows;
		}
		return FALSE;
	}  
    public function getCategoryList3($merchant_id='')
	{
		$result = queryO("SELECT group_concat(category separator ',') as `cat_ids` FROM {{item}} WHERE merchant_id=$merchant_id");
		//make cat_id list is unique no repeat (1,1,2,2,3) => (1,2,3)
		$cat_ids = implode(',',array_unique(explode(",",$result[0]->cat_ids)));
		
		// file_put_contents('tes2.txt',var_export($result,true));
		
		$data_feed='';
		$stmt="
		SELECT * FROM {{category}}
		WHERE status in ('publish','published') AND cat_id in ($cat_ids)
		ORDER BY sequence ASC
		";
		$connection=Yii::app()->db;
		$rows=$connection->createCommand($stmt)->queryAll(); 				
		if (is_array($rows) && count($rows)>=1){
			if ($this->data=="list"){
				foreach ($rows as $val) {									   
				   $data_feed[$val['cat_id']]=array(
				     'category_name'=>$val['category_name'],
				     'category_description'=>$val['category_description'],
				     'category_name_trans'=>$val['category_name_trans'],
				     'category_description_trans'=>$val['category_description_trans']
				   );
				}
				return $data_feed;
			} else return $rows;
		}
		return FALSE;
	}  

	
	public function AA($tag='')
	{		
		if ( $access=$this->AAccess()){			
			if (in_array($tag,(array)$access)){
				return true;
			}
		}
		return false;
	}
	
	public function AAccess()
	{
		$info=$this->getAdminInfo();		
		if (is_object($info)){
			$access=!empty($info->user_access)?json_decode($info->user_access):false;
			if ($access!=false){
				return $access;
			}
		}
		return false;
	}
	
	public function AAmenuList()
	{
		$menu_list='';
		$menu=$this->adminMenu();		
		foreach ($menu['items'] as $val) {
			$menu_list[]=$val['tag'];
			if (isset($val['items'])){
				if (is_array($val['items']) && count($val['items'])>=1){
					foreach ($val['items'] as $sub_val) {
						$menu_list[]=$sub_val['tag'];
					}
				}
			}
		}		
		return $menu_list;
	}
	

    public function getAddressBookByID($id='')
    {
    	$db_ext=new DbExt;    	
    	$stmt="SELECT * FROM
    	       {{address_book}}
    	       WHERE
    	       id='$id'    	       
    	       LIMIT 0,1
    	";    	    	
    	if ($res=$db_ext->rst($stmt)){    		
    		return $res[0];
    	}
    	return false;
    } 	    
    
    public function hasAddressDefault($client_id='')
    {
    	$db_ext=new DbExt;    	
    	$stmt="SELECT 
    	       concat(street,' ',city,' ',state,' ',zipcode) as address,
    	       id,location_name,country_code
    	       FROM
    	       {{address_book}}
    	       WHERE
    	       client_id='$client_id'    	       
    	       AND
    	       as_default ='2'
    	       LIMIT 0,1
    	";    	    	
    	if ($res=$db_ext->rst($stmt)){    		
    		return $res[0];
    	}
    	return false;
    }

    public function showAddressBook()   
    {
    	if ( Yii::app()->functions->isClientLogin()){
    		$client_id=Yii::app()->functions->getClientId();
    		if ( $res=$this->hasAddressDefault($client_id)){    			
    			return $res;
    		}
    	}
    	return false;
    }
    
    public function getAddressBookByClient($client_id='')
    {
    	$db_ext=new DbExt;    	
    	$stmt="SELECT  
    	       concat(street,' ',city,' ',state,' ',zipcode) as address,
    	       id,location_name,country_code
    	       FROM
    	       {{address_book}}
    	       WHERE
    	       client_id =".$this->q($client_id)."
    	       ORDER BY street ASC    	       
    	";    	    	
    	if ($res=$db_ext->rst($stmt)){    		
    		return $res;
    	}
    	return false;
    } 	        
    
    public function addressBook($client_id='')
    {
    	$list='';
    	if ( $res=$this->getAddressBookByClient($client_id)){
    		foreach ($res as $val) {    			
    			$list[$val['id']]=$val['address']." ".$this->countryCodeToFull($val['country_code']);
    		}
    	}
    	return $list;
    }
    
    public function getLanguageField()
    {
    	$lang_list='';
    	$db_ext=new DbExt;
    	$stmt="SELECT lang_id,country_code,language_code
    	 FROM {{languages}} 
    	 WHERE
    	 status in ('publish','published')
    	 ";	
    	if ($res=$db_ext->rst($stmt)){    		
    		foreach ($res as $val) {    			
    			$lang_list[$val['lang_id']]=$val['language_code'];
    		}    		
    	}
    	return $lang_list;    
    }       
    
    public function multipleField()
    {
    	if ( Yii::app()->functions->getOptionAdmin('enabled_multiple_translation')==2){
    		return true;
    	}
    	return false;
    }
    
    public function geoCoding($lat='',$lng='')
    {    	    	    	
		// return false;
		// gdump("geoCoding".$_SERVER['REQUEST_URI']."
// ".$address."
// ");
	
	
    	$protocol = isset($_SERVER["https"]) ? 'https' : 'http';
		if ($protocol=="http"){
			$url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=".$lat.",".$lng."&sensor=true";
		} else $url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=".$lat.",".$lng."&sensor=true";
		
		$google_geo_api_key=getOptionA('google_geo_api_key');
		if (!empty($google_geo_api_key)){
			$url=$url."&key=".urlencode($google_geo_api_key);
		}
		
    	$data = @file_get_contents($url);
    	if (!empty($data)){
    	    $result = json_decode($data,true);    	    	   
    	    //dump($result);
    	    if (!isset($result['results'])){
    	    	return false;
    	    }
    	    if (is_array($result['results']) && count($result['results'])>=2){
    	        $location = array();
    	         foreach ($result['results'][0]['address_components'] as $component) {
	               switch ($component['types']) {
				      case in_array('street_number', $component['types']):
				        $location['street_number'] = $component['long_name'];
				        break;
				      case in_array('route', $component['types']):
				        $location['street'] = $component['long_name'];
				        break;
				      case in_array('neighborhood', $component['types']):
				        $location['street2'] = $component['long_name'];
				        break;  
				      case in_array('sublocality', $component['types']):
				        $location['sublocality'] = $component['long_name'];
				        break;
				      case in_array('locality', $component['types']):
				        $location['locality'] = $component['long_name'];
				        break;
				      case in_array('administrative_area_level_2', $component['types']):
				        $location['admin_2'] = $component['long_name'];
				        break;
				      case in_array('administrative_area_level_1', $component['types']):
				        $location['admin_1'] = $component['long_name'];
				        break;
				      case in_array('postal_code', $component['types']):
				        $location['postal_code'] = $component['long_name'];
				        break;
				      case in_array('country', $component['types']):
				        $location['country'] = $component['long_name'];
				        $location['country_code'] = $component['short_name'];
				        break;
				   }
    	         } 	    	             	        
    	         return $location;
    	    }
    	} 
    	return false;
    }

       
}/* END CLASS*/


/**********************************************************************
FUNCTIONS
**********************************************************************/
function getOption($mtid='',$key='')
{
	return Yii::app()->functions->getOption($key,$mtid);  
}

function getOptionA($key='')
{
	return Yii::app()->functions->getOptionAdmin($key);  
}

function FormatDateTime($date='',$time=true)
{
	return Yii::app()->functions->FormatDateTime($date,$time);
}

function timeFormat($time='',$is_display=false)
{
	return Yii::app()->functions->timeFormat($time,$is_display);
}

function cleanNumber($string='')
{
	return preg_replace("/[^0-9^.]/","",$string);
}

function explodeData($data)
{
	return Yii::app()->functions->explodeData($data);
}

function prettyFormat($price='',$merchant_id='')
{	
	return Yii::app()->functions->prettyFormat($price,$merchant_id);
}

function standardPrettyFormat($price='')
{
    return Yii::app()->functions->standardPrettyFormat($price);
}

function normalPrettyPrice($price='')
{
	return Yii::app()->functions->normalPrettyPrice($price);
}

function unPrettyPrice($price)
{
	return Yii::app()->functions->unPrettyPrice($price);	
}

function arrayKeyExists($needle, $haystack)
{
    $result = array_key_exists($needle, $haystack);
    if ($result) return $result;
    foreach ($haystack as $v) {
        if (is_array($v)) {
            $result = arrayKeyExists($needle, $v);
        }
        if ($result) return $result;
    }
    return $result;
}

function getSelectedItemArray($key='',$array='')
{		
	if (is_array($array) && count($array)>=1){
		foreach ($array as $keys=>$val) {
			if ( $key == $keys){
				return $val;
			}
		}
	}
	return false;
}

function getCurrencyCode()
{
	return Yii::app()->functions->getCurrencyCode();
}

function baseCurrency()
{
	return Yii::app()->functions->getCurrencyCode();
}


function baseUrl()
{
	return Yii::app()->request->baseUrl;;
}

function getMerchantID()
{
	Yii::app()->functions->getMerchantID();
}

function isIsset($data='')
{
	if ( isset($data)){
		return $data;
	}
	return '';
}

function getDistance($from='',$to='',$debug=false)
{
	return Yii::app()->functions->getDistance($from,$to,$debug);
}

function prettyDate($date='',$full=false)
{
	return Yii::app()->functions->prettyDate($date,$full);
}

function getDeliveryDistance($from_address='',$merchant_address='',$country_code='')
{	
	$miles=0;
	$miles_raw=0;
	if($distance=getDistance($from_address,$merchant_address,$country_code,true)){	    				
        $miles=$distance->rows[0]->elements[0]->distance->text;
		$miles_raw=str_replace(array(" ","mi"),"",$miles); 		
		$km=$distance->rows[0]->elements[0]->distance->value;
		$kms=($km * 0.621371 / 1000);
	}	    		
	return $miles_raw;		    					    					    		
}

function getDeliveryDistance2($from_address='',$merchant_address='',$country_code='')
{	
	$miles=0;
	$miles_raw=0;
	$kms=0;
	if($distance=getDistance($from_address,$merchant_address,$country_code,true)){	    						
        $miles=$distance->rows[0]->elements[0]->distance->text;
		$miles_raw=str_replace(array(" ","mi"),"",$miles); 		
		$km=$distance->rows[0]->elements[0]->distance->value;
		//$kms=($km * 0.621371 / 1000);
		$kms=miles2kms( unPrettyPrice($miles_raw));
		$kms=standardPrettyFormat($kms);
	}	    		
	
	return array(
	   'mi'=>$miles_raw,
	   'km'=>$kms
	);		    					    					    		
}

function miles2kms($miles) {
	$ratio = 1.609344;
	$kms = $miles * $ratio;
	return $kms;
} 

function ft2kms($ft='') {
	$ratio = 0.0003048;
	$ft = $ft * $ratio;
	return $ft;
} 

function adminCurrencySymbol()
{
	return Yii::app()->functions->adminCurrencySymbol();
}

function adminCurrencyCode()
{
	return Yii::app()->functions->adminCurrencyCode();
}

function sendEmail($to='',$from='',$subject='',$body='')
{
	return Yii::app()->functions->sendEmail($to,$from,$subject,$body);
}

function customNumberFormat($n, $precision = 3) {
    if ($n < 1000000) {
        // Anything less than a million
        $n_format = number_format($n);
    } else if ($n < 1000000000) {
        // Anything less than a billion
        $n_format = number_format($n / 1000000, $precision) . 'M';
    } else {
        // At least a billion
        $n_format = number_format($n / 1000000000, $precision) . 'B';
    }

    return $n_format;
}

function send($url,$api,$amount,$redirect){ 
    $ch = curl_init(); 
    curl_setopt($ch,CURLOPT_URL,$url); 
     
    curl_setopt($ch,CURLOPT_POSTFIELDS,"api=$api&amount=$amount&redirect=$redirect"); 
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); 
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,true); 
    $res = curl_exec($ch); 
    curl_close($ch); 
    return $res; 
} 
 
function get($url,$api,$trans_id,$id_get){ 
    $ch = curl_init(); 
    curl_setopt($ch,CURLOPT_URL,$url);  curl_setopt($ch,CURLOPT_POSTFIELDS,"api=$api&id_get=$id_get&trans_id=$trans_id"); 
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); 
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,true); 
    $res = curl_exec($ch); 
    curl_close($ch); 
    return $res; 
} 


function t($message='')
{
	return Yii::t("default",$message);
}

function uploadURL()
{
	return Yii::app()->request->baseUrl."/upload";
}

function assetsURL()
{
	return Yii::app()->request->baseUrl."/assets";
}

function ccController()
{
	return "/".Yii::app()->controller->id;
}

function displayPrice($currency='',$amount='')
{
	return Yii::app()->functions->displayPrice($currency,$amount);
}

function getWebsiteName()
{
	return Yii::app()->functions->getWebsiteName();
}

function smarty($search='',$value='',$subject='')
{	
   return Yii::app()->functions->smarty($search,$value,$subject);
}

function getAdminGlobalSender()
{
	$from=Yii::app()->functions->getOptionAdmin("global_admin_sender_email");	    				
	if (empty($from)){
		$from='no-reply@'.$_SERVER['HTTP_HOST'];
	}
	return $from;
}

function initialStatus()
{
	return 'initial_order';
}

function websiteUrl()
{
	return Yii::app()->getBaseUrl(true);
}

function membershipType($is_commission='')
{
	if ($is_commission==2){
		return t("Commission");
	} else return t("Membership");
}

function withdrawalStatus()
{
	return Yii::app()->functions->withdrawalStatus();
}

function displayDate($date='')
{
	return Yii::app()->functions->displayDate($date);
}

function createUrl($url='')
{
	return Yii::app()->createUrl($url);
}

function qTranslate($text='',$key='',$data='',$cookie_lang_id='kr_lang_id')
{		
	if (Yii::app()->functions->getOptionAdmin("enabled_multiple_translation")!=2){
		return stripslashes($text);
	}
	$key=$key."_trans";			
	$id=isset($_COOKIE[$cookie_lang_id])?$_COOKIE[$cookie_lang_id]:'';		
	if ( $id>0){
		if (is_array($data) && count($data)>=1){
			if (isset($data[$key])){
				if (array_key_exists($id,(array)$data[$key])){
					if (!empty($data[$key][$id])){
					    return stripslashes($data[$key][$id]);
					}
				}
			}
		}
	}	
	return stripslashes($text);
}

function okToDecode()
{
	$version=phpversion();		
	if ( $version>5.3){
		return true;
	}
	return false;
}

function geoCoding($lat='',$lng='')
{
	return Yii::app()->functions->geoCoding($lat,$lng);
}

function q($data='')
{
	return Yii::app()->db->quoteValue($data);
}	

function getNextClientID()
{
	$DbExt=new DbExt; 
	$stmt="
	SHOW TABLE STATUS WHERE name='{{client}}'
	";		
	if ($res=$DbExt->rst($stmt)){
		return $res[0]['Auto_increment'].Yii::app()->functions->generateRandomKey(3);
	}
	return false;
}

function clearString($text='')
{
	if(!empty($text)){
	   return stripslashes($text);
	}
	return ;
}