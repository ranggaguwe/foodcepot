<?php
class EmailTPL extends CApplicationComponent
{
	
	public static function forgotPass($data='',$token='')
	{
      $website_title=Yii ::app()->functions->getOptionAdmin('website_title');
      $url=Yii::app()->getBaseUrl(true)."/store/forgotPassword/?token=".$token;      
	  return <<<HTML
	  <p>Halo $data[first_name]</p>
	  <br/>
	  <p>Klik pada link di bawah untuk merubah password Anda.</p>
	  <p><a href="$url">$url</a></p>
	  <p>Terimakasih.</p>
	  <p>- $website_title</p>
HTML;
	}
	
	public static function merchantActivationCode($data='')
	{
	$website_url=Yii::app()->getBaseUrl(true)."/merchant";
    $website_title=Yii::app()->functions->getOptionAdmin('website_title');
    
    $email_tpl_activation=Yii::app()->functions->getOptionAdmin('email_tpl_activation');    
    if (!empty($email_tpl_activation)){
    	$email_tpl_activation=Yii::app()->functions->smarty("merchant_name",$data['merchant_name'],$email_tpl_activation); 
    	$email_tpl_activation=Yii::app()->functions->smarty("activation_key",$data['activation_key'],$email_tpl_activation); 
    	$email_tpl_activation=Yii::app()->functions->smarty("website_title",$website_title,$email_tpl_activation); 
    	$email_tpl_activation=Yii::app()->functions->smarty("website_url",$website_url,$email_tpl_activation); 
    	return $email_tpl_activation;
    }
    
	return <<<HTML
	<p>Halo $data[merchant_name]<br/></p>
	<p>Terimakasih telah bergabung dengan kami!</p>
	<p>Kode aktivasi Anda adalah $data[activation_key]</p>
	
	<p>Klik <a href="$website_url">disini</a> untuk login atau kunjungi $website_url</p>
	
	<p>Terimakasih.</p>
	<p>- $website_title</p>
HTML;
	}
	
	public static function merchantActivationCodePlain()
	{		
	return <<<HTML
	<p>Halo {merchant_name}<br/></p>
	<p>Terimakasih telah bergabung dengan kami!</p>
	<p>Kode aktivasi Anda adalah {activation_key}</p>
	
	<p>Klik <a href="{website_url}">disini</a> untuk login atau kunjungi {website_url}</p>
	
	<p>Terimakasih.</p>
	<p>- {website_title}</p>
HTML;
	}	
	
	public static function merchantForgotPass($data='',$code='')
	{
	  $website_title=Yii::app()->functions->getOptionAdmin('website_title');
	  
	  $email_tpl_forgot=Yii::app()->functions->getOptionAdmin('email_tpl_forgot');	
	  if (!empty($email_tpl_forgot)){
	  	  $email_tpl_forgot=Yii::app()->functions->smarty("merchant_name",$data['merchant_name'],$email_tpl_forgot); 
	  	  $email_tpl_forgot=Yii::app()->functions->smarty("verification_code",$code,$email_tpl_forgot); 
	  	  $email_tpl_forgot=Yii::app()->functions->smarty("website_title",$website_title,$email_tpl_forgot); 	  	  
	  	  return $email_tpl_forgot;	  	  
	  }
			  
	  return <<<HTML
	  <p>Halo $data[merchant_name]<br/></p>
	  <p>Kode verifikasi Anda adalah $code</p>
	  <p>Terimakasih.</p>
	<p>- $website_title</p>
HTML;
	}
	
	public static function merchantForgotPassPlain()
	{	 
	  return <<<HTML
	  <p>Halo {merchant_name}<br/></p>
	  <p>Verifikasi kode Anda adalah {verification_code}</p>
	  <p>Terimakasih.</p>
	<p>- {website_title}</p>
HTML;
	}	
	
	public static function salesReceipt($data='',$item_details='')
	{				
		$tr="";
		if (is_array($data) && count($data)>=1){
			foreach ($data as $val) {				
				$tr.="<tr>";
				$tr.="<td>".$val['label']."</td>";
				$tr.="<td>".$val['value']."</td>";
				$tr.="</tr>";
			}
		}
		
		$mid=isset($item_details['total']['mid'])?$item_details['total']['mid']:'';
		//dump($mid);
		
		$tr.="<tr>";
		$tr.="<td colspan=\"2\">&nbsp;</td>";
		$tr.="</tr>";
		if (isset($item_details['item'])){
			if (is_array($item_details['item']) && count($item_details['item'])>=1){
				foreach ($item_details['item'] as $item) {
					//dump($item);
					$notes='';
					$item_total=$item['qty']*$item['discounted_price'];
					if (!empty($item['order_notes'])){
					    $notes="<p>".$item['order_notes']."</p>";
					}
					// $cookref='';
					// if (!empty($item['cooking_ref'])){
					    // $cookref="<p>".$item['cooking_ref']."</p>";
					// }
					$size='';
					if (!empty($item['size_words'])){
					    $size="<p>".$item['size_words']."</p>";
					}
					

					
					$tr.="<tr>";
				    $tr.="<td>".$item['qty']." ".$item['item_name'].$size.$notes."</td>";
				    $tr.="<td>".prettyFormat($item_total,$mid)."</td>";
				    $tr.="</tr>";
				    
				    if (isset($item['sub_item'])){
				    	if (is_array($item['sub_item']) && count($item['sub_item'])>=1){
					    	foreach ($item['sub_item'] as $itemsub) {				    		
					    		$subitem_total=$itemsub['addon_qty']*$itemsub['addon_price'];				    		
					    		$tr.="<tr>";
					            $tr.="<td style=\"text-indent:10px;\">".$itemsub['addon_name']."</td>";
					            $tr.="<td>".prettyFormat($subitem_total,$mid)."</td>";
					            $tr.="</tr>";
					    	}
				    	}
				    }
				    
				}
			}
		}
		$tr.="<tr>";
		$tr.="<td colspan=\"2\">&nbsp;</td>";
		$tr.="</tr>";
		//dump($item_details['total']);	
		//dump($item_details['total']);
		
		if (isset($item_details['total'])){
			

			$tr.="<tr>";
			$tr.="<td>".Yii::t("default","Subtotal").":</td>";
			$tr.="<td>".prettyFormat($item_details['total']['subtotal'],$mid)."</td>";
			$tr.="</tr>";
			
			if (!empty($item_details['total']['delivery_charges'])):
			$tr.="<tr>";
			$tr.="<td>".Yii::t("default","Delivery Fee").":</td>";
			$tr.="<td>".prettyFormat($item_details['total']['delivery_charges'],$mid)."</td>";
			$tr.="</tr>";
			endif;
			
			if (!empty($item_details['total']['packaging'])):
			$tr.="<tr>";
			$tr.="<td>".Yii::t("default","Packaging").":</td>";
			$tr.="<td>".prettyFormat($item_details['total']['packaging'],$mid)."</td>";
			$tr.="</tr>";
			endif;
			
			$tr.="<tr>";
			$tr.="<td>".Yii::t("default","Tax")." ".$item_details['total']['tax_amt']."%</td>";
			$tr.="<td>".prettyFormat($item_details['total']['taxable_total'],$mid)."</td>";
			$tr.="</tr>";
			
			if (!isset($item_details['total']['card_fee'])){
				$item_details['total']['card_fee']='';
			}
			
			if ($item_details['total']['card_fee']>0):
			$tr.="<tr>";
			$tr.="<td>".Yii::t("default","Card Fee").":</td>";
			$tr.="<td>".prettyFormat($item_details['total']['card_fee'],$mid)."</td>";
			$tr.="</tr>";
			endif;
			
			if ($item_details['total']['tips']>0.001){
				$tr.="<tr>";
				$tr.="<td>".Yii::t("default","Tips")." " .$item_details['total']['tips_percent'] . ":</td>";
				$tr.="<td>".prettyFormat($item_details['total']['tips'],$mid)."</td>";
				$tr.="</tr>";
			}
			
			$tr.="<tr>";
			$tr.="<td>".Yii::t("default","Total").":</td>";
			$tr.="<td>".$item_details['total']['curr'].prettyFormat($item_details['total']['total'],$mid)."</td>";
			$tr.="</tr>";
		}
		ob_start();
		?>
		<div style="display: block;max-height: 70px;max-width: 200px;">
		<?php echo Widgets::receiptLogo();?>
		</div>
		<h3><?php echo Yii::t("default","Order Details")?></h3>		
		<table border="0">
		<?php echo $tr;?>		
		</table>
		<?php	
		$receipt = ob_get_contents();
        ob_end_clean();
        return $receipt;
	}
	
	public static function receiptTPL()
	{
		return <<<HTML
<p>Salam {customer-name},</p>
<br/><br/>
<p> Terimakasih untuk berbelanja pada kami. Kami berharap Anda senang dengan belanjaan Anda. Nomor order Anda adalah  {receipt-number}. Kami sudah memasukan order detailnya di bawah:	</p>
<br/>
 {receipt}	
	
<br/><br/>
<p> Terimakasih.</p>
HTML;
	}
	
	public static function adminForgotPassword($newpass='')
	{	 
	  return <<<HTML
	  <p>Password Anda sudah direset ke : $newpass</p>
	  <p>Terimakasih.</p>	
HTML;
	}	
	
	public static function merchantChangeStatus()
	{	 
	  return <<<HTML
	  <p>Halo {owner_name}<br/></p>
	  <p>Merchant Anda {merchant_name} sudah berubah status ke {status}</p>
	  <br/>
	  <p>{website_title}</p>
	  <p>Terimakasih.</p>	
HTML;
	}		
	
	public static function receiptMerchantTPL()
	{
		return <<<HTML
<p>Halo admin,</p>
<br/>
<p>Berikut ada order baru dengan nomor referensi {receipt-number} dari customer {customer-name}</p>
<br/>
 {receipt}	
<br/>
<p> Terimakasih.</p>
HTML;
	}	
	
	public function payoutRequest()
	{
		return <<<HTML
<p>Halo {merchant-name},</p>
<br/>
<p>Kami hanya memberi tahu Anda bahwa kami mendapat permintaan Anda untuk melakukan penarikan sebesar {payout-amount} via {payment-method} ke {account}</p>
<br/> 
	
<p>
Kamu dapat mengcancel request ini sebelum tanggal {cancel-date} disini:<br/>
{cancel-link}
</p>

<p>
Kami akan menyelesaikan proses ini pada {process-date} (atau hari kerja berikutnya),tetapi bisa memakan waktu hingga 7 hari untuk muncul di akun Anda. Sebuah email konfirmasi kedua akan dikirim pada saat tsb.
</p>

<br/>
<p> Terimakasih</p>
HTML;
	}
	
	public function payoutProcess()
	{
return <<<HTML
<p>Halo {merchant-name},</p>
<br/>
<p>Kami baru memproses  penarikan Anda sebesar {payout-amount} via {payment-method}.</p>
<p>Penarikan dikirim ke {acoount}</p>
<br/> 

<br/>
<p> Terima kasih</p>
HTML;
	}
	
	public static function signupEmailVerification()
	{
		return <<<HTML
<p>Halo {firstname},</p>
<br/><br/>
<p>Verifikasi kode Anda adalah : {code}</p>
<br/>
	
<br/><br/>
<p> Terimakasih</p>
HTML;
	}				
		
} /*END CLASS*/