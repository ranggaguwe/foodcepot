<?php


if (!isset($_SESSION)) { session_start(); }

if (!class_exists('AjaxAdmin'))
{
	class AjaxAdmin extends DbExt
	{
		public $data;
		public $code=2;
		public $msg;
		public $details;
		
		public function __construct()
		{
			// set website timezone
			$website_timezone=Yii::app()->functions->getOptionAdmin("website_timezone");		 
		    if (!empty($website_timezone)){
		 	   Yii::app()->timeZone=$website_timezone;
		    }		 
			
			$mtid=Yii::app()->functions->getMerchantID();		 		    
		    $mt_timezone=Yii::app()->functions->getOption("merchant_timezone",$mtid);	   	   	    	
    	    if (!empty($mt_timezone)){    	 	
    		   Yii::app()->timeZone=$mt_timezone;
    	    }		     	
		}	
		
		public function otableNodata()
		{
			if (isset($_GET['sEcho'])){
				$feed_data['sEcho']=$_GET['sEcho'];
			} else $feed_data['sEcho']=1;	   
			     
	        $feed_data['iTotalRecords']=0;
	        $feed_data['iTotalDisplayRecords']=0;
	        $feed_data['aaData']=array();		
	        echo json_encode($feed_data);
	    	die();
		}
    
		public function otableOutput($feed_data='')
		{
    	  echo json_encode($feed_data);
    	  die();
        }
    
		public function output($debug=FALSE)
		{
    	    $resp=array('code'=>$this->code,'msg'=>$this->msg,'details'=>$this->details);
    	    if ($debug){
    		    dump($resp);
    	    }
    	    return json_encode($resp);    	    
		}
		
		public function login()
		{			
								
            /** check if admin has enabled the google captcha*/    	    	
	    	if ( getOptionA('captcha_admin_login')==2){
	    		if ( GoogleCaptcha::checkCredentials()){
	    			if ( !GoogleCaptcha::validateCaptcha()){
	    				$this->msg=GoogleCaptcha::$message;
	    				return false;
	    			}	    		
	    		}	    	
	    	} 
	    	
			$DbExt=new DbExt;
			$stmt="SELECT * FROM
			       {{admin_user}}
			       WHERE
			       username=".Yii::app()->db->quoteValue($this->data['username'])."
			       AND
			       password=".Yii::app()->db->quoteValue(md5($this->data['password']))."
			       LIMIT 0,1
			";
			if ( $res=$DbExt->rst($stmt)){								
				//Yii::app()->request->cookies['kr_user'] = new CHttpCookie('kr_user', json_encode($res));  		
				
				//dump($res);
				$_SESSION['kr_user']=json_encode($res);
				//dump($_SESSION['kr_user']);
				$this->code=1;												
				
				$session_token=Yii::app()->functions->generateRandomKey().md5($_SERVER['REMOTE_ADDR']);				
				$params=array(
				  'session_token'=>$session_token,
				  'last_login'=>date('c')
				);
				$this->updateData("{{admin_user}}",$params,'admin_id',$res[0]['admin_id']);
								
				$_SESSION['kr_user_session']=$session_token;			
				
	    		$this->msg=Yii::t("default","Login Successful");
			} else $this->msg=Yii::t("default","Either username or password is invalid.");
		}
		
		public function addMerchant()
		{			
			if (empty($this->data['id'])){				
				if ( empty($this->data['username']) && empty($this->data['password'])){
					$this->msg=Yii::t("default","username & password is required");
					return ;
				} else {
					$params['username']=$this->data['username'];
					$params['password']=md5($this->data['password']);
				}
			} else {
				if (!empty($this->data['password'])){
					$params['username']=$this->data['username'];
					$params['password']=md5($this->data['password']);
				}
			}					

			
			if (empty($this->data['id'])){					
			    if (Yii::app()->functions->isMerchantExist($this->data['contact_email'])){
					$this->msg=Yii::t("default","Sorry you input email address that is already registered in our records.");
					return ;
				}		
				if (!isset($this->data['package_id'])){
					$this->msg=Yii::t("default","ERROR: Missing package id");
					return ;
				}	
				
				if ( !$package=Yii::app()->functions->getPackagesById($this->data['package_id'])){
					$this->msg=Yii::t("default","ERROR: Package information not found");
					return ;
				}		
								
				if ( $t=Yii::app()->functions->validateUsername($this->data['username']) ){				
					$this->msg=Yii::t("default","Merchant Username is already been taken");
					return ;
				}			
			 } else {
			 	 if ( !empty($this->data['password'])){
			 	 if ( Yii::app()->functions->validateUsername($this->data['username'],$this->data['id']) ){
			 	 	$this->msg=Yii::t("default","Merchant Username is already been taken");
					return ;
			 	 }			     
			 	 }
			 	 			 	 
				 if ( Yii::app()->functions->validateMerchantEmail($this->data['contact_email'],$this->data['id']) ){
				     $this->msg=Yii::t("default","Merchant Email address is already been taken");
				      return ;
			     }	    
			 	 
			 }		

			if (!empty($this->data['merchant_slug'])){
				$params['merchant_slug']=FunctionsV3::verifyMerchantSlug(
				  Yii::app()->functions->seo_friendly_url($this->data['merchant_slug']),
				  $this->data['id']
				);
			} else {	
			    $params['merchant_slug']=Yii::app()->functions->createSlug($this->data['merchant_name']);
			}
			$params['merchant_name']=addslashes($this->data['merchant_name']);
			$params['merchant_phone']=addslashes($this->data['merchant_phone']);
			$params['contact_name']=addslashes($this->data['contact_name']);
			$params['contact_phone']=$this->data['contact_phone'];
			$params['contact_email']=$this->data['contact_email'];
			$params['country_code']=$this->data['country_code'];
			$params['country_code']=Yii::app()->functions->adminSetCounryCode();
			$params['street']=$this->data['street'];
			$params['city']=$this->data['city'];
			$params['post_code']=$this->data['post_code'];
			$params['cuisine']=json_encode($this->data['cuisine']);
			$params['service']=$this->data['service'];
			$params['status']=$this->data['status'];
		    $params['date_created']=date('c');
		    $params['ip_address']=$_SERVER['REMOTE_ADDR'];
		    $params['membership_expired']=isset($this->data['membership_expired'])?$this->data['membership_expired']:'';
		    $params['is_featured']=isset($this->data['is_featured'])?$this->data['is_featured']:1;
		    $params['package_id']=isset($this->data['package_id'])?$this->data['package_id']:"";
		    
		    $params['state']=isset($this->data['state'])?$this->data['state']:'';		    
		    $params['is_commission']=isset($this->data['is_commission'])?$this->data['is_commission']:1;
		    $params['percent_commision']=isset($this->data['percent_commision'])?$this->data['percent_commision']:0;
		    
		    $params['abn']=isset($this->data['abn'])?$this->data['abn']:'';		    
		    		    
		    $params['commision_type']=isset($this->data['commision_type'])?$this->data['commision_type']:'';		    
  	    
		    if (isset($this->data['package_id'])){
		    	if ($package=Yii::app()->functions->getPackagesById($this->data['package_id'])){		    		
		    		if ($package['promo_price']>=1){
		    			$params['package_price']=$package['promo_price'];
		    		} else $params['package_price']=$package['price'];		    	
		    	}		    
		    }		
		   
		    $params['is_ready']=isset($this->data['is_ready'])?$this->data['is_ready']:1;
		    
		    if (empty($this->data['id'])){	
		    	if ( $this->insertData("{{merchant}}",$params)){
		    		$this->details=Yii::app()->db->getLastInsertID();
		    		$this->code=1;
		    		$this->msg=Yii::t("default","Successful");		    		
		    		
		    		$mtid=$this->details;
	                Yii::app()->functions->updateOption("merchant_switch_master_cod",
	    	        isset($this->data['merchant_switch_master_cod'])?$this->data['merchant_switch_master_cod']:''
	    	        ,$mtid);
	    	        
	    	        Yii::app()->functions->updateOption("merchant_switch_master_ccr",
	    	        isset($this->data['merchant_switch_master_ccr'])?$this->data['merchant_switch_master_ccr']:''
	    	        ,$mtid);
	    	        
	    	        Yii::app()->functions->updateOption("merchant_switch_master_pyr",
	    	        isset($this->data['merchant_switch_master_pyr'])?$this->data['merchant_switch_master_pyr']:''
	    	        ,$mtid);
	    	        
				    Yii::app()->functions->updateOption("merchant_latitude",
			    	isset($this->data['merchant_latitude'])?$this->data['merchant_latitude']:''
			    	,$mtid);
			    	
			    	Yii::app()->functions->updateOption("merchant_longitude",
			    	isset($this->data['merchant_longitude'])?$this->data['merchant_longitude']:''
			    	,$mtid);
	    	        	    	        
		    	}
		    } else {		    	
		    	unset($params['date_created']);
				$params['date_modified']=date('c');		
				$params['merchant_slug']=FunctionsV3::verifyMerchantSlug($params['merchant_slug'],$this->data['id']);
						
				$res = $this->updateData('{{merchant}}' , $params ,'merchant_id',$this->data['id']);
				if ($res){
					$this->code=1;
	                $this->msg=Yii::t("default",'Merchant updated.');  
	                
	                $mtid=$this->data['id'];
	                Yii::app()->functions->updateOption("merchant_switch_master_cod",
	    	        isset($this->data['merchant_switch_master_cod'])?$this->data['merchant_switch_master_cod']:''
	    	        ,$mtid);
	    	        
	    	        Yii::app()->functions->updateOption("merchant_switch_master_ccr",
	    	        isset($this->data['merchant_switch_master_ccr'])?$this->data['merchant_switch_master_ccr']:''
	    	        ,$mtid);
	    	        
	    	        Yii::app()->functions->updateOption("merchant_switch_master_pyr",
	    	        isset($this->data['merchant_switch_master_pyr'])?$this->data['merchant_switch_master_pyr']:''
	    	        ,$mtid);
	    	        
	    	         Yii::app()->functions->updateOption("merchant_latitude",
			    	isset($this->data['merchant_latitude'])?$this->data['merchant_latitude']:''
			    	,$this->data['id']);
			    	
			    	Yii::app()->functions->updateOption("merchant_longitude",
			    	isset($this->data['merchant_longitude'])?$this->data['merchant_longitude']:''
			    	,$this->data['id']);
	                	    	        
				} else $this->msg=Yii::t("default","ERROR: cannot update");
		    }	
		}
		
		private function jsonResponse()
		{
			$resp=array('code'=>$this->code,'msg'=>$this->msg,'details'=>$this->details);
			echo CJSON::encode($resp);
			Yii::app()->end();
		}
		public function merchantListx()
		{
			$slug=$this->data['slug'];
			$stmt="SELECT a.*,
			(
			select title
			from
			{{packages}}
			where
			package_id = a.package_id
			) as package_name
			
			 FROM
			{{merchant}} a	
			ORDER BY merchant_id DESC
			";
			if ( $res=$this->rst($stmt)){
				foreach ($res as $val) {	
					$date=date('M d,Y G:i:s',strtotime($val['date_created']));
					$date=Yii::app()->functions->translateDate($date);
					
					$action="<div class=\"options\">
    	    		<a href=\"$slug/id/$val[merchant_id]\" >".Yii::t("default","Edit")."</a>
    	    		<a href=\"javascript:;\" class=\"row_del\" rev=\"$val[merchant_id]\" >".Yii::t("default","Delete")."</a>
    	    		</div>";
					
					$val['package_name']=isset($val['package_name'])?$val['package_name']:'';
					
					if ($val['status']=="expired"){
					   $class='uk-badge-danger';
					} elseif ( $val['status']=="pending"){
						$class='';
					} elseif ($val['status']=="active"){
						$class='uk-badge-success';
					}				
					$membershipdate=prettyDate($val['membership_expired']);
					$membershipdate=Yii::app()->functions->translateDate($membershipdate);					
					
					$url_login=baseUrl()."/merchant/autologin/id/".$val['merchant_id']."/token/".$val['password'];
					$link_login='<br/><br/>
					<a target="_blank" href="'.$url_login.'"><div class="uk-badge">'.t("AutoLogin").'</div></a>
					';
					
					$feed_data['aaData'][]=array(
					  $val['merchant_id'],stripslashes($val['merchant_name']).$action,
					  $val['street'],
					  $val['city'],
					  $val['country_code'],
					  $val['merchant_phone']." / ".$val['contact_phone'],
					  $val['package_name']."<br/>".$membershipdate,
					  $val['activation_key'],
					  membershipType($val['is_commission']),
					  $date."<br/><div class=\"uk-badge $class\">".strtoupper(Yii::t("default",$val['status']))."</div>".$link_login
					);
				}
				$this->otableOutput($feed_data);
			}
			$this->otableNodata();
		}
		
		public function rowDelete()
		{			
			if (!isset($this->data['tbl']))
			{	
				$this->msg=Yii::t("default","Missing parameters");
				return ;		 				 
			}			
			
			if ($this->data['tbl']=="merchantSponsoredList"){
				$params=array('is_sponsored'=>1,'date_modified'=>date('c'));
				if ($this->updateData("{{merchant}}",$params,'merchant_id',$this->data['row_id'])){
					$this->code=1;
					$this->msg=Yii::t("default","Successfully remove.");
				} else $this->msg=Yii::t("default","ERROR: cannot execute query.");			
				return ;
			}		
						
			if ($this->data['tbl']=="merchant"){
				$functionk=new FunctionsK();
				if ( $functionk->getMerchantOrders($this->data['row_id'])){
					$this->msg=t("Sorry but you cannot delete this merchant it has reference on order tables");
					return ;
				}			
			}
			
			$whereid=$this->data['whereid'];
			$tbl=Yii::app()->db->tablePrefix.$this->data['tbl'];
			$query = "DElETE FROM $tbl WHERE $whereid=". Yii::app()->db->quoteValue($this->data['row_id']) ." "; 
			if (Yii::app()->db->createCommand($query)->query()){
			     $this->msg=Yii::t("default","Successfully deleted.");
                 $this->code=1;	        
                 
                 if ($this->data['tbl']=="merchant"){
                 	$stmt_del="DELETE  FROM {{option}}
                 	WHERE
                 	merchant_id=".Yii::app()->db->quoteValue($this->data['row_id'])."
                 	 ";
                 	if ( $this->data['row_id'] >=1){
                 	    Yii::app()->db->createCommand($stmt_del)->query();
                 	}
                 }			
                             			 
			} else $this->msg=Yii::t("default","ERROR: cannot execute query.");
		}
		
		public function merchantLogin()
		{
			
            /** check if admin has enabled the google captcha*/    	    	
	    	if ( getOptionA('captcha_merchant_login')==2){
	    		if ( GoogleCaptcha::checkCredentials()){
	    			if ( !GoogleCaptcha::validateCaptcha()){
	    				$this->msg=GoogleCaptcha::$message;
	    				return false;
	    			}	    		
	    		}	    	
	    	} 
			
			Yii::app()->functions->updateMerchantSponsored();
		    
		
			$stmt="SELECT * FROM
			       {{merchant}}
			       WHERE
			       username=".Yii::app()->db->quoteValue($this->data['username'])."
			       AND
			       password=".Yii::app()->db->quoteValue(md5($this->data['password']))."
			       LIMIT 0,1
			";							
			if ( $res=$this->rst($stmt)){	
				if ($res[0]['status']=="active" || $res[0]['status']=="expired"){
					//Yii::app()->request->cookies['kr_merchant_user'] = new CHttpCookie('kr_merchant_user', json_encode($res));
					$_SESSION['kr_merchant_user']=json_encode($res);
										
                     $session_token=Yii::app()->functions->generateRandomKey().md5($_SERVER['REMOTE_ADDR']);				
					 $params=array(
					  'session_token'=>$session_token,
					  'last_login'=>date('c')
					 );
					 $this->updateData("{{merchant}}",$params,'merchant_id',$res[0]['merchant_id']);
					 
					 $_SESSION['kr_merchant_user_session']=$session_token;
					 $_SESSION['kr_merchant_user_type']='admin';
					
					$this->code=1;
					$this->code=1;
		    		$this->msg=Yii::t("default","Login Successful");
				} else $this->msg=Yii::t("default","Login Failed. You account status is ".$res[0]['status']);
			} else {
				//$this->msg=Yii::t("default","Either username or password is invalid.");
				$this->merchantUserLogin();
			}		
		}
		
		public function merchantUserLogin()
		{
		   $stmt="SELECT a.*,
		          (
		            select merchant_name
		            from
		            {{merchant}}
		            where
		            merchant_id=a.merchant_id
		          ) as merchant_name,
		          (
		            select merchant_slug
		            from
		            {{merchant}}
		            where
		            merchant_id=a.merchant_id
		          ) as merchant_slug
		          
		           FROM
			       {{merchant_user}} a
			       WHERE
			       username=".Yii::app()->db->quoteValue($this->data['username'])."
			       AND
			       password=".Yii::app()->db->quoteValue(md5($this->data['password']))."
			       AND
			       status='active'
			       LIMIT 0,1
			";				   
			if ( $res=$this->rst($stmt)){	
				//dump($res);
				if ($res[0]['status']=="active" || $res[0]['status']=="expired"){
					//Yii::app()->request->cookies['kr_merchant_user'] = new CHttpCookie('kr_merchant_user', json_encode($res));
					$_SESSION['kr_merchant_user']=json_encode($res);
					
					$this->code=1;					
		    		$this->msg=Yii::t("default","Login Successful");
		    		
		    		 $session_token=Yii::app()->functions->generateRandomKey().md5($_SERVER['REMOTE_ADDR']);						 							    		 
					 $_SESSION['kr_merchant_user_session']=$session_token;
					 $_SESSION['kr_merchant_user_type']='merchant_user';
		    		
		    		$params=array(
		    		  'last_login'=>date('c'),
		    	 	  'ip_address'=>$_SERVER['REMOTE_ADDR'],
		    		  'session_token'=>$session_token
		    		);
		    		$this->updateData("{{merchant_user}}",$params,'merchant_user_id',$res[0]['merchant_user_id']);
		    		
				} else $this->msg=Yii::t("default","Login Failed. You account status is ".$res[0]['status']);
			} else $this->msg=Yii::t("default","Either username or password is invalid.");
		}
		
	    public function categoryList()
		{
			
			$slug=$this->data['slug'];
			$stmt="
			SELECT * FROM
			{{category}}
			ORDER BY cat_id DESC
			";
			$connection=Yii::app()->db;
    	    $rows=$connection->createCommand($stmt)->queryAll();     	    
    	    if (is_array($rows) && count($rows)>=1){
    	    	foreach ($rows as $val) {    	 
    	    		$chk="<input type=\"checkbox\" name=\"row[]\" value=\"$val[cat_id]\" class=\"chk_child\" >";   		
    	    		$option="<div class=\"options\">
    	    		<a href=\"$slug/id/$val[cat_id]\" >".Yii::t("default","Edit")."</a>
    	    		<a href=\"javascript:;\" class=\"row_del\" rev=\"$val[cat_id]\" >".Yii::t("default","Delete")."</a>
    	    		</div>";
    	    		
    	    		/*$date=date('M d,Y G:i:s',strtotime($val['date_created']));    	    		
    	    		$date=Yii::app()->functions->translateDate($date);*/
    	    		$date=FormatDateTime($val['date_created']);
    	    		
    	    		if (!empty($val['photo'])){
    	    			$img=Yii::app()->request->baseUrl."/upload/$val[photo]";
    	    		    $photo="<img class=\"uk-thumbnail uk-thumbnail-mini\" src=\"$img\" >";	
    	    		} else $photo='';
    	    		
    	    		$feed_data['aaData'][]=array(
    	    		  $chk,stripslashes($val['category_name']).$option,
    	    		  stripslashes($val['category_description']),
    	    		  $photo,
    	    		  $date."<div>".Yii::t("default",$val['status'])."</div>"
    	    		);
    	    	}
    	    	$this->otableOutput($feed_data);
    	    }     	    
    	    $this->otableNodata();
		}
		
	    public function uploadImage()
	    {	    	
	    	$qqfile=$_GET['qqfile'];
	    	if (preg_match("/.php/i", $qqfile)) {
	    		if (isset($_GET['currentController'])){
	    			if ($_GET['currentController']!="admin"){
	    				$this->msg=Yii::t("default","Invalid file");
	    				return ;
	    			} else {
	    				// check language file if has errors
	    			}	    		
	    		}	    	
	    	}
	    	
	    	$path_to_upload=Yii::getPathOfAlias('webroot')."/upload/";	    		    	
		    if(!file_exists($path_to_upload)) {	
               if (!@mkdir($path_to_upload,0777)){
               	    $this->msg=Yii::t("default","Cannot create upload folder. Please create the upload folder manually on your rood directory with 777 permission.");
               	    return ;
               }		    
		    }
		    
		    /*create htaccess file*/
		    $htaccess='<Files *>';
		    $htaccess.=PHP_EOL;
		    $htaccess.='SetHandler default-handler';
		    $htaccess.=PHP_EOL;
		    //$htaccess.='php_flag engine off';
		    $htaccess.=PHP_EOL;
		    $htaccess.='</Files>';
		    $htfile=$path_to_upload.'.htaccess';		    
		    if (!file_exists($htfile)){
		    	$myfile = fopen($htfile, "w") or die("Unable to open file!".$htfile);    
                fwrite($myfile, $htaccess);        
                fclose($myfile);
		    }	    
		    
		    if (isset($this->data['qqfile']) && !empty($this->data['qqfile'])){
		        $input = fopen("php://input", "r");
		        $temp = tmpfile();
		        $realSize = stream_copy_to_stream($input, $temp);
		
		        $pathinfo = pathinfo($this->data['qqfile']);	  		        
		        $time=time();
		        $file_name=$time."-".$pathinfo['filename'].".".$pathinfo['extension'];		        
		        $file_name=str_replace(" ","-",$file_name);
		        $path=$path_to_upload.$file_name;
		        		
		        $target = fopen($path, "w");        
		        fseek($temp, 0, SEEK_SET);
	            stream_copy_to_stream($temp, $target);
				
	            $this->code=1;
		        $this->msg=Yii::t("default","Upload Completed");
		        $this->details=array(
		           'file'=>$file_name,
		           'id'=>time().Yii::app()->functions->generateRandomKey(10)
		        );			    
	        } else $this->msg=Yii::t("default","File is empty");
	    }
		
		public function addCategory()
		{			
													
			$params=array(
			  'category_name'=>addslashes($this->data['category_name']),
			  'category_description'=>addslashes($this->data['category_description']),
			  'photo'=>isset($this->data['photo'])?addslashes($this->data['photo']):'',
			  'status'=>addslashes($this->data['status']),
			  'date_created'=>date('c'),
			);				
			
			if (isset($this->data['category_name_trans'])){				
				if (okToDecode()){
					$params['category_name_trans']=json_encode($this->data['category_name_trans'],
					JSON_UNESCAPED_UNICODE);
				} else $params['category_name_trans']=json_encode($this->data['category_name_trans']);				
			}
			if (isset($this->data['category_description_trans'])){
				if (okToDecode()){
					$params['category_description_trans']=json_encode($this->data['category_description_trans'],
					JSON_UNESCAPED_UNICODE);
				} else $params['category_description_trans']=json_encode($this->data['category_description_trans']);
			}
						
						
			$command = Yii::app()->db->createCommand();
			if (isset($this->data['id']) && is_numeric($this->data['id'])){				
				unset($params['date_created']);
				$params['date_modified']=date('c');				
				$res = $command->update('{{category}}' , $params , 
				'cat_id=:cat_id' , array(':cat_id'=> addslashes($this->data['id']) ));
				if ($res){
					$this->code=1;
	                $this->msg=Yii::t("default",'Category updated.');  
				} else $this->msg=Yii::t("default","ERROR: cannot update");
			} else {				
				if ($res=$command->insert('{{category}}',$params)){
					$this->details=Yii::app()->db->getLastInsertID();	                
	                $this->code=1;
	                $this->msg=Yii::t("default",'Category added.');  	                
	            } else $this->msg=Yii::t("default",'ERROR. cannot insert data.');
			}
		}
	    

	    public function FoodItemAdd()
	    {
	    	$mtid=Yii::app()->functions->getMerchantID();
	    	if (!Yii::app()->functions->validateMerchantCanPost($mtid) ){
	    		if (isset($this->data['id']) && is_numeric($this->data['id'])){				
	    		} else {	
	    		   $this->msg=Yii::t("default","Sorry but you reach the limit of adding food item. Please upgrade your membership");
	    		   return ;
	    		}
	    	}
			if ($this->data['weight'] > 0 && is_numeric($this->data['weight'])){
			} else {
			   $this->msg=Yii::t("default","Weight must be a number and more than zero");
			   return ;
			}
			$params=array(
			  'date_created'=>date('c'),
			  // 'ip_address'=>$_SERVER['REMOTE_ADDR'],
			  'merchant_id'=>Yii::app()->functions->getMerchantID(),
			  'item_name'=>isset($this->data['item_name'])?$this->data['item_name']:"",
			  'item_description'=>isset($this->data['item_description'])?$this->data['item_description']:'',
			  'status'=>$this->data['status'],
			  'uom'=>$this->data['uom'],
			  'tax'=>$this->data['tax'],
			  'weight'=>$this->data['weight'],
			  'stock'=>$this->data['stock'],
			  'min_order'=>$this->data['min_order'],
			  'condition'=>$this->data['condition'],
			  'category'=>$this->data['category']?$this->data['category']:"",
			  'retail_price'=>$this->data['retail_price'],
			  'photo'=>isset($this->data['photo'])?$this->data['photo']:"",
			  // 'non_taxable'=>isset($this->data['non_taxable'])?$this->data['non_taxable']:1,
			  'gallery_photo'=>isset($this->data['gallery_photo'])?json_encode($this->data['gallery_photo']):""
			);
						
			if (isset($this->data['item_name_trans'])){
				if (okToDecode()){
				    $params['item_name_trans']=json_encode($this->data['item_name_trans'],
				    JSON_UNESCAPED_UNICODE);
				} else $params['item_name_trans']=json_encode($this->data['item_name_trans']);
			}	    
			if (isset($this->data['item_description_trans'])){
				if (okToDecode()){
				   $params['item_description_trans']=json_encode($this->data['item_description_trans'],
				   JSON_UNESCAPED_UNICODE);
				} else $params['item_description_trans']=json_encode($this->data['item_description_trans']);
			}	    
			
						
			$command = Yii::app()->db->createCommand();
			if (isset($this->data['id']) && is_numeric($this->data['id'])){

				//wholesale price
				$DbExt=new DbExt;
				$DbExt->qry("DELETE FROM {{wholesale_price}} WHERE item_id=".$this->data['id']);

				unset($params['date_created']);
				$params['date_modified']=date('c');				
				$res = $command->update('{{item}}' , $params , 
				'item_id=:item_id' , array(':item_id'=>addslashes($this->data['id'])));
				if ($res){
					$this->code=1;
	                $this->msg=Yii::t("default",'Item updated.');  
				} else $this->msg=Yii::t("default","ERROR: cannot update");
			} else {				
				if ($res=$command->insert('{{item}}',$params)){
					$this->details=Yii::app()->db->getLastInsertID();	
	                $this->code=1;
	                $this->msg=Yii::t("default",'Item added.');  	                
	            } else $this->msg=Yii::t("default",'ERROR. cannot insert data.');
			}
			
			$command = Yii::app()->db->createCommand();
			
			if(is_numeric($this->data['id'])) {
				$item_id = $this->data['id'];
			} else {
				$item_id = Yii::app()->db->getLastInsertId();
			}
			if (isset($this->data['wprice']) && count($this->data['wprice'])>=1){
				foreach ($this->data['wprice'] as $key=>$val) {
					if (!empty($val)){
						$params=array(
							'item_id'=>$item_id,
							'start'=>$this->data['start'][$key],
							'end'=>$this->data['end'][$key],
							'price'=>$val,
						);
						$command->insert('{{wholesale_price}}',$params);
					}
				}
				//set wholesale_flag
				$command = Yii::app()->db->createCommand();
				$res = $command->update('{{item}}' , array('wholesale_flag'=>1) , 
				'item_id=:item_id' , array(':item_id'=>$item_id));
			} else {
				$command = Yii::app()->db->createCommand();
				$res = $command->update('{{item}}' , array('wholesale_flag'=>0) , 
				'item_id=:item_id' , array(':item_id'=>$item_id));
			}
			//end wholesale price
	    }
	    
	    public function FoodItemList()
	    {	  
	    	Yii::app()->functions->data="list";
	    	// $cat_list=Yii::app()->functions->getCategoryList();
	    	//dump($size_list);
	    	
	        $slug=$this->data['slug'];
			$stmt="
			SELECT * FROM
			{{item}} 
			WHERE merchant_id=".Yii::app()->functions->getMerchantID()."
			ORDER BY item_id DESC
			";
			$connection=Yii::app()->db;
    	    $rows=$connection->createCommand($stmt)->queryAll();     	    
    	    if (is_array($rows) && count($rows)>=1){
    	    	foreach ($rows as $val) {    	   
    	    		// $categories='';  	    	
    	    		// $category=isset($val['category'])?(array)json_decode($val['category']):false;    	    		
    	    		// if ( is_array($category) && count($category)>=1){
    	    			// foreach ($category as $valcat) {    	    		
    	    				// if (array_key_exists($valcat,(array)$cat_list)){
    	    					// $categories.=$cat_list[$valcat] .", ";
    	    				// }    	    			
    	    			// }
    	    			// $categories=!empty($categories)?substr($categories,0,-2):"";
    	    		// }    	    	
    	    		    	    		
    	    		// $price_list='';
    	    		// $price=isset($val['price'])?(array)json_decode($val['price']):false;
    	    		// if ( is_array($price) && count($price)>=1){
    	    			// foreach ($price as $key_price=>$val_price) {    	    		
    	    				// if (array_key_exists($key_price,(array)$size_list)){
    	    					// $price_list.=getCurrencyCode().prettyFormat($val_price)." ".ucwords($size_list[$key_price]). "<br/>";
    	    				// }    	    		
    	    			// }
    	    		// }    	    	
    	    		
    	    		$chk="<input type=\"checkbox\" name=\"row[]\" value=\"$val[item_id]\" class=\"chk_child\" >";   		
    	    		$option="<div class=\"options\">
    	    		<a href=\"$slug/id/$val[item_id]\" >".Yii::t("default","Edit")."</a>
    	    		<a href=\"javascript:;\" class=\"row_del\" rev=\"$val[item_id]\" >".Yii::t("default","Delete")."</a>
    	    		</div>";
    	    		
    	    		/*$date=date('M d,Y G:i:s',strtotime($val['date_created']));    	    		
    	    		$date=Yii::app()->functions->translateDate($date);*/
    	    		$date=FormatDateTime($val['date_created']);
    	    		
    	    		if (!empty($val['photo'])){
    	    			$img=Yii::app()->request->baseUrl."/upload/$val[photo]";
    	    		    $photo="<img class=\"uk-thumbnail uk-thumbnail-mini\" src=\"$img\" >";	
    	    		} else $photo='';
    	    		$feed_data['aaData'][]=array(
    	    		  $chk,$val['item_name'].$option,
    	    		  //Yii::app()->functions->limitText($val['item_description']),
    	    		  $val['item_description'],
    	    		  // $categories,
    	    		  // $price_list,
    	    		  $photo,
    	    		  CHtml::checkBox('food_not_available',
    	    		  $val['not_available']==2?true:false,
    	    		  array(
    	    		    'class'=>'not_available',
    	    		    'value'=>$val['item_id']
    	    		  )),
    	    		  $date."<div>".Yii::t("default",$val['status'])."</div>"
    	    		);
    	    	}
    	    	$this->otableOutput($feed_data);
    	    }     	    
    	    $this->otableNodata();	
	    }	
	    
	    public function UpdateMerchant()
	    {	    	
	    	$merchant_id=Yii::app()->functions->getMerchantID();
	    	if (!empty($this->data['password'])){
				$params['username']=$this->data['username'];
				$params['password']=md5($this->data['password']);
		    }
		    
		    if (!empty($this->data['password'])){
		    	if ( Yii::app()->functions->validateUsername($this->data['username'],$merchant_id) ){
		    		$this->msg=Yii::t("default","Merchant Username is already been taken");
		    		return ;
		    	}		    
		    }	    		
		    
		    if ( Yii::app()->functions->validateMerchantEmail($this->data['contact_email'],$merchant_id) ){
		    	$this->msg=Yii::t("default","Merchant Email address is already been taken");
		    	return ;
		    }	    
		    /*dump($merchant_id);
		    dump($params);
		    die();*/
		    
		    $params['merchant_name']=isset($this->data['merchant_name'])?$this->data['merchant_name']:"";
			$params['merchant_phone']=isset($this->data['merchant_phone'])?$this->data['merchant_phone']:'';
			$params['contact_name']=isset($this->data['contact_name'])?$this->data['contact_name']:'';
			$params['contact_phone']=isset($this->data['contact_phone'])?$this->data['contact_phone']:'';
			// $params['contact_email']=isset($this->data['contact_email'])?$this->data['contact_email']:'';
			$params['country_code']=isset($this->data['country_code'])?$this->data['country_code']:'';
			$params['street']=isset($this->data['street'])?$this->data['street']:'';
			$params['city']=isset($this->data['city'])?$this->data['city']:'';
			$params['post_code']=isset($this->data['post_code'])?$this->data['post_code']:'';
			$params['cuisine']=isset($this->data['cuisine'])?json_encode($this->data['cuisine']):'';
			$params['service']=isset($this->data['service'])?$this->data['service']:'';			
		    $params['date_created']=date('c');
		    $params['ip_address']=$_SERVER['REMOTE_ADDR'];
		    
		    $params['state']=isset($this->data['state'])?$this->data['state']:'';
		    $params['abn']=isset($this->data['abn'])?$this->data['abn']:'';
		    
		    $merchant_id=Yii::app()->functions->getMerchantID();
		    
		    $params['merchant_slug']=FunctionsV3::verifyMerchantSlug(
		      Yii::app()->functions->seo_friendly_url($this->data['merchant_slug']),$merchant_id
		    );
		   
	    	Yii::app()->functions->updateOption("merchant_latitude",
	    	isset($this->data['merchant_latitude'])?$this->data['merchant_latitude']:''
	    	,$merchant_id);
	    	
	    	Yii::app()->functions->updateOption("merchant_longitude",
	    	isset($this->data['merchant_longitude'])?$this->data['merchant_longitude']:''
	    	,$merchant_id);
	    	
	    	Yii::app()->functions->updateOption("merchant_information",
	    	isset($this->data['merchant_information'])?$this->data['merchant_information']:''
	    	,$merchant_id);
		    		    
		    unset($params['date_created']);
			$params['date_modified']=date('c');				
													
			$res = $this->updateData('{{merchant}}' , $params ,'merchant_id',Yii::app()->functions->getMerchantID());
			if ($res){
				$this->code=1;
                $this->msg=Yii::t("default",'Merchant updated.');  
			} else $this->msg=Yii::t("default","ERROR: cannot update");		    
	    }	
	    
	    public function merchantSettings()
	    {	    	
	    	
	    	/** reverse back to 24 hour format if format is 12 hour*/
	    	if ( Yii::app()->functions->getOptionAdmin("website_time_picker_format") =="12"){
		    	if (is_array($this->data['stores_open_starts'])){
		    		foreach ($this->data['stores_open_starts'] as $key=>$val) {
		    			//dump($key."=>".$val);
		    			$this->data['stores_open_starts'][$key]=timeFormat($val);
		    		}
		    	}
		    	
		    	if (is_array($this->data['stores_open_ends'])){
		    		foreach ($this->data['stores_open_ends'] as $key=>$val) {
		    			//dump($key."=>".$val);
		    			$this->data['stores_open_ends'][$key]=timeFormat($val);
		    		}
		    	}
	    	}
	    		    
	    	/*dump(json_encode($this->data['stores_open_starts']));
	    	dump($this->data);	    	
	    	die();*/
	    	
	    	$merchant_id=Yii::app()->functions->getMerchantID();
	    	
	    	/*Yii::app()->functions->updateOption("merchant_currency",
	    	isset($this->data['merchant_currency'])?$this->data['merchant_currency']:''
	    	,$merchant_id);*/
	    	
	    	/*Yii::app()->functions->updateOption("merchant_decimal",
	    	isset($this->data['merchant_decimal'])?$this->data['merchant_decimal']:''
	    	,$merchant_id);*/
	    	
	    	/*Yii::app()->functions->updateOption("merchant_use_separators",
	    	isset($this->data['merchant_use_separators'])?$this->data['merchant_use_separators']:''
	    	,$merchant_id);*/
	    	
	    	Yii::app()->functions->updateOption("merchant_minimum_order",
	    	isset($this->data['merchant_minimum_order'])?$this->data['merchant_minimum_order']:''
	    	,$merchant_id);
	    	
	    	Yii::app()->functions->updateOption("merchant_tax",
	    	isset($this->data['merchant_tax'])?$this->data['merchant_tax']:''
	    	,$merchant_id);
	    	
	    	Yii::app()->functions->updateOption("merchant_delivery_charges",
	    	isset($this->data['merchant_delivery_charges'])?$this->data['merchant_delivery_charges']:''
	    	,$merchant_id);
	    	
	    	Yii::app()->functions->updateOption("stores_open_day",
	    	isset($this->data['stores_open_day'])?json_encode($this->data['stores_open_day']):''
	    	,$merchant_id);
	    	
	    	Yii::app()->functions->updateOption("stores_open_starts",
	    	isset($this->data['stores_open_starts'])?json_encode($this->data['stores_open_starts']):''
	    	,$merchant_id);
	    	
	    	Yii::app()->functions->updateOption("stores_open_ends",
	    	isset($this->data['stores_open_ends'])?json_encode($this->data['stores_open_ends']):''
	    	,$merchant_id);
	    	
	    	Yii::app()->functions->updateOption("stores_open_custom_text",
	    	isset($this->data['stores_open_custom_text'])?json_encode($this->data['stores_open_custom_text']):''
	    	,$merchant_id);
	    		    	
	    	//if (isset($this->data['photo'])){
	    	Yii::app()->functions->updateOption("merchant_photo",
	    	isset($this->data['photo'])?$this->data['photo']:''
	    	,$merchant_id);
	    	//}
	    		    	
	    		    		    	
	    	Yii::app()->functions->updateOption("merchant_delivery_estimation",
	    	isset($this->data['merchant_delivery_estimation'])?$this->data['merchant_delivery_estimation']:''
	    	,$merchant_id);
	    	
	    	Yii::app()->functions->updateOption("merchant_delivery_miles",
	    	isset($this->data['merchant_delivery_miles'])?$this->data['merchant_delivery_miles']:''
	    	,$merchant_id);
	    	
	    	/*Yii::app()->functions->updateOption("merchant_delivery_charges_type",
	    	isset($this->data['merchant_delivery_charges_type'])?$this->data['merchant_delivery_charges_type']:''
	    	,$merchant_id);*/
	    	
	    	Yii::app()->functions->updateOption("merchant_photo_bg",
	    	isset($this->data['photo2'])?$this->data['photo2']:''
	    	,$merchant_id);
	    	
	    	Yii::app()->functions->updateOption("merchant_disabled_cod",
	    	isset($this->data['merchant_disabled_cod'])?$this->data['merchant_disabled_cod']:''
	    	,$merchant_id);
	    	
	    	Yii::app()->functions->updateOption("merchant_disabled_ccr",
	    	isset($this->data['merchant_disabled_ccr'])?$this->data['merchant_disabled_ccr']:''
	    	,$merchant_id);
	    	
	    	Yii::app()->functions->updateOption("merchant_extenal",
	    	isset($this->data['merchant_extenal'])?$this->data['merchant_extenal']:''
	    	,$merchant_id);
	    	
	    	Yii::app()->functions->updateOption("merchant_enabled_voucher",
	    	isset($this->data['merchant_enabled_voucher'])?$this->data['merchant_enabled_voucher']:''
	    	,$merchant_id);
	    	
	    	Yii::app()->functions->updateOption("merchant_distance_type",
	    	isset($this->data['merchant_distance_type'])?$this->data['merchant_distance_type']:''
	    	,$merchant_id);
	    	
	    	Yii::app()->functions->updateOption("merchant_timezone",
	    	isset($this->data['merchant_timezone'])?$this->data['merchant_timezone']:''
	    	,$merchant_id);
	    	
	    	Yii::app()->functions->updateOption("merchant_close_msg",
	    	isset($this->data['merchant_close_msg'])?$this->data['merchant_close_msg']:''
	    	,$merchant_id);
	    	
	    	Yii::app()->functions->updateOption("merchant_preorder",
	    	isset($this->data['merchant_preorder'])?$this->data['merchant_preorder']:''
	    	,$merchant_id);
	    	
	    	/*Yii::app()->functions->updateOption("merchant_table_booking",
	    	isset($this->data['merchant_table_booking'])?$this->data['merchant_table_booking']:''
	    	,$merchant_id);*/
	    	
	    	Yii::app()->functions->updateOption("merchant_maximum_order",
	    	isset($this->data['merchant_maximum_order'])?$this->data['merchant_maximum_order']:''
	    	,$merchant_id);
	    	
	    	Yii::app()->functions->updateOption("merchant_packaging_charge",
	    	isset($this->data['merchant_packaging_charge'])?$this->data['merchant_packaging_charge']:''
	    	,$merchant_id);
	    	
	    	Yii::app()->functions->updateOption("merchant_close_msg_holiday",
	    	isset($this->data['merchant_close_msg_holiday'])?$this->data['merchant_close_msg_holiday']:''
	    	,$merchant_id);
	    	
	    	Yii::app()->functions->updateOption("merchant_holiday",
	    	isset($this->data['merchant_holiday'])?json_encode($this->data['merchant_holiday']):''
	    	,$merchant_id);
	    		    	
	    	Yii::app()->functions->updateOption("merchant_activated_menu",
	    	isset($this->data['merchant_activated_menu'])?$this->data['merchant_activated_menu']:''
	    	,$merchant_id);
	    	
	    	Yii::app()->functions->updateOption("spicydish",
	    	isset($this->data['spicydish'])?$this->data['spicydish']:''
	    	,$merchant_id);
	    	
	    	Yii::app()->functions->updateOption("merchant_required_delivery_time",
	    	isset($this->data['merchant_required_delivery_time'])?$this->data['merchant_required_delivery_time']:''
	    	,$merchant_id);
	    	
	    	Yii::app()->functions->updateOption("merchant_close_store",
	    	isset($this->data['merchant_close_store'])?$this->data['merchant_close_store']:''
	    	,$merchant_id);	    		    	
	    	
	    	Yii::app()->functions->updateOption("merchant_packaging_increment",
	    	isset($this->data['merchant_packaging_increment'])?$this->data['merchant_packaging_increment']:''
	    	,$merchant_id);	    		    	
	    	
	    	Yii::app()->functions->updateOption("merchant_show_time",
	    	isset($this->data['merchant_show_time'])?$this->data['merchant_show_time']:''
	    	,$merchant_id);	    		    	
	    	
	    	Yii::app()->functions->updateOption("merchant_enabled_tip",
	    	isset($this->data['merchant_enabled_tip'])?$this->data['merchant_enabled_tip']:''
	    	,$merchant_id);	    		    	
	    	
	    	Yii::app()->functions->updateOption("merchant_tip_default",
	    	isset($this->data['merchant_tip_default'])?$this->data['merchant_tip_default']:''
	    	,$merchant_id);	    		    	
	    		    	
	    	Yii::app()->functions->updateOption("merchant_minimum_order_pickup",
	    	isset($this->data['merchant_minimum_order_pickup'])?$this->data['merchant_minimum_order_pickup']:''
	    	,$merchant_id);	    		    	
	    	
	    	Yii::app()->functions->updateOption("merchant_maximum_order_pickup",
	    	isset($this->data['merchant_maximum_order_pickup'])?$this->data['merchant_maximum_order_pickup']:''
	    	,$merchant_id);	    		    	
	    	
	    	Yii::app()->functions->updateOption("merchant_disabled_ordering",
	    	isset($this->data['merchant_disabled_ordering'])?$this->data['merchant_disabled_ordering']:''
	    	,$merchant_id);	    		    	
	    		    
	    	Yii::app()->functions->updateOption("merchant_tax_charges",
	    	isset($this->data['merchant_tax_charges'])?$this->data['merchant_tax_charges']:''
	    	,$merchant_id);	    

	    	Yii::app()->functions->updateOption("stores_open_pm_start",
	    	isset($this->data['stores_open_pm_start'])?json_encode($this->data['stores_open_pm_start']):''
	    	,$merchant_id);
	    		    	
	    	Yii::app()->functions->updateOption("stores_open_pm_ends",
	    	isset($this->data['stores_open_pm_ends'])?json_encode($this->data['stores_open_pm_ends']):''
	    	,$merchant_id);    	
	    	
	    	Yii::app()->functions->updateOption("food_option_not_available",
	    	isset($this->data['food_option_not_available'])?$this->data['food_option_not_available']:''
	    	,$merchant_id);  
	    	
	    	Yii::app()->functions->updateOption("order_verification",
	    	isset($this->data['order_verification'])?$this->data['order_verification']:''
	    	,$merchant_id);  
	    	
	    	Yii::app()->functions->updateOption("order_sms_code_waiting",
	    	isset($this->data['order_sms_code_waiting'])?$this->data['order_sms_code_waiting']:''
	    	,$merchant_id);  
	    	
	    	Yii::app()->functions->updateOption("disabled_food_gallery",
	    	isset($this->data['disabled_food_gallery'])?$this->data['disabled_food_gallery']:''
	    	,$merchant_id);  
	    	
	    	$this->code=1;
	    	$this->msg=Yii::t("default","Settings saved.");
	    }	
			    
	    public function AlertSettings()
	    {	    	
	    	$merchant_id=Yii::app()->functions->getMerchantID();
	    	
	    	Yii::app()->functions->updateOption("merchant_notify_email",
	    	isset($this->data['merchant_notify_email'])?$this->data['merchant_notify_email']:''
	    	,$merchant_id);
	    	
	    	Yii::app()->functions->updateOption("enabled_alert_notification",
	    	isset($this->data['enabled_alert_notification'])?$this->data['enabled_alert_notification']:''
	    	,$merchant_id);
	    	
	    	Yii::app()->functions->updateOption("enabled_alert_sound",
	    	isset($this->data['enabled_alert_sound'])?$this->data['enabled_alert_sound']:''
	    	,$merchant_id);
	    	
	    	$this->code=1;
	    	$this->msg=Yii::t("default","Settings saved.");
	    }	
	    
	    public function socialSettings()
	    {
	    	$merchant_id=Yii::app()->functions->getMerchantID();
	    	
	    	Yii::app()->functions->updateOption("facebook_page",
	    	isset($this->data['facebook_page'])?$this->data['facebook_page']:''
	    	,$merchant_id);
	    	
	    	Yii::app()->functions->updateOption("twitter_page",
	    	isset($this->data['twitter_page'])?$this->data['twitter_page']:''
	    	,$merchant_id);
	    	
	    	Yii::app()->functions->updateOption("google_page",
	    	isset($this->data['google_page'])?$this->data['google_page']:''
	    	,$merchant_id);
	    	
	    	$this->code=1;
	    	$this->msg=Yii::t("default","Settings saved.");
	    }	
	    
	    public function sortItem()
	    {	    	
	    	$DbExt=new DbExt;
	    	if ( isset($this->data['table']) && isset($this->data['sort_field'])){
		    	if (!empty($this->data['table']) && is_array($this->data['sort_field'])){
		    		$tbl=$this->data['table'];
		    		if (is_array($this->data['sort_field']) && count($this->data['sort_field'])>=1){
		    			$x=1;
		    			foreach ($this->data['sort_field'] as $item_id) {
		    				$params=array(
		    				 'sequence'=>$x
		    				);		    				
		    				$DbExt->updateData("{{{$tbl}}}",$params,$this->data['whereid'],$item_id);
		    				$x++;
		    			}
		    			$this->code=1;
		    			$this->msg=Yii::t("default","Sort saved.");
		    		} else $this->msg=Yii::t("default","Missing parameters");
		    	} else $this->msg=Yii::t("default","Missing parameters");
	    	} else $this->msg=Yii::t("default","Missing parameters");
	    }	
	    
	    
	    public function viewFoodItem()
	    {	    	
	    	if (isset($this->data['item_id'])){
	    		require_once 'food-item.php';
	    	} else {
	    		?>
	    		<p class="uk-alert uk-alert-danger"><?php echo Yii::t("default","Sorry but we cannot find what you are looking for.")?></p>
	    		<?php
	    	}
	    	die();
	    }	
	    
	    public function addToCart()
	    {

	    	//unset($_SESSION['kr_item']);
	    	$this->msg=t("Food Item added to cart");
	    	if (isset($this->data['item_id'])){
	    		$item=$this->data;
	    		
	    		/** check if item is taxable*/	  
	    		if ( $food_info=Yii::app()->functions->getFoodItem($item['item_id'])){	    			
	    			$item['non_taxable']=$food_info['non_taxable'];
					$item['retail_price']=$food_info['retail_price'];
					$item['merchant_id']=$food_info['merchant_id'];
	    		}
	    		
	    		unset($item['action']);
	    		if (is_numeric($this->data['row'])){
	    			$row=$this->data['row']-1;
					$qty = $item['qty'];
					//set wholesale_price
					// dump($item);
					$price = $item['retail_price'];
					$wholesale_price = FunctionsV3::getWhere('wholesale_price','item_id',$item['item_id']);
					$item['price'] = $item['retail_price'];
					foreach($wholesale_price as $w) {
						if($qty >= $w['start'] && $qty <= $w['end']) {
							$item['price'] = $w['price'];
						}
					}
	    			$_SESSION['kr_item'][$row]=$item;
	    			$this->msg=t("Cart updated");
	    		} else {
	    			
	    			$found=false;	  
	    			$found_key='';  			
	    			if (!isset($_SESSION['kr_item'])){
	    				$_SESSION['kr_item']='';
	    			}
	    			if(is_array($_SESSION['kr_item']) && count($_SESSION['kr_item'])>=1){
	    			   $x=0;
	    			   foreach ($_SESSION['kr_item'] as $key=> $val) {	 
						   
	    			   	   if ($val['item_id']==$item['item_id']){	    			   	   	      			   	   	  
	    			   	   	   $found_key=$key;
	    			   	   	   $found=true;
	    			   	   	   $notes=false;
	    			   	   	   if ( $item['notes']==$val['notes']){
	    			   	   	   	  $notes=true;
	    			   	   	   }
    			   	   	   	   
    			   	   	   	   /*check size*/
    			   	   	   	   $item_size=false;
    			   	   	   	   if ($item['price']==$val['price']){
    			   	   	   	   	  $item_size=true;
    			   	   	   	   }
	    			   	   }

	    			   }/* end foreach*/
	    			}	  
	    				    				    			
	    			/*if ( $found==false){
	    				echo 'false';
	    			} else echo "true=>$found_key";	    				   
	    			die(); */ 
	    			if ( $found==false){
						$qty = $item['qty'];
						//set wholesale_price
						// dump($item);
						$price = $item['retail_price'];
						$wholesale_price = FunctionsV3::getWhere('wholesale_price','item_id',$item['item_id']);
						$item['price'] = $item['retail_price'];
						foreach($wholesale_price as $w) {
							if($qty >= $w['start'] && $qty <= $w['end']) {
								$item['price'] = $w['price'];
							}
						}
						// dump($item);
						$_SESSION['kr_item'][]=$item;
	    			} else {
						$_SESSION['kr_item'][$found_key]['qty']+=$item['qty'];
						$qty = $_SESSION['kr_item'][$found_key]['qty'];
						$wholesale_price = FunctionsV3::getWhere('wholesale_price','item_id',$item['item_id']);
						$price = $item['retail_price'];
						foreach($wholesale_price as $w) {
							if($qty >= $w['start'] && $qty <= $w['end']) {
								$price = $w['price'];
							}
						}
						$_SESSION['kr_item'][$found_key]['price'] = $price;
	    			}
	    		}
				// dump($_SESSION['kr_item']);
				// dump($item);
	    		// file_put_contents('out.txt',var_export($_SESSION['kr_item'],true));
	    		$this->code=1;
	    	} else $this->msg=Yii::t("default","Item id is required");	    
	    }	
	    
	    public function loadItemCart()
	    {
	    	/*dump($this->data);
	    	dump($_SESSION['kr_item']);*/
	    	
	    	// if (isset($this->data['merchant_id'])){
	    		// $current_merchant_id=$this->data['merchant_id'];
	    		// if (isset($_SESSION['kr_item'])) {
		    		// if (is_array($_SESSION['kr_item']) && count($_SESSION['kr_item'])>=1){
		    			// foreach ($_SESSION['kr_item'] as $key=>$temp_item) {	    				
		    				// if ( $temp_item['merchant_id']!=$current_merchant_id){
		    					// unset($_SESSION['kr_item'][$key]);
		    				// }
		    			// }
		    		// }
	    		// }
	    	// }
	    		    		    		    		    		
	    	//dump($_SESSION['kr_item']);
			$receipt = false;
	    	if(isset($this->data['receipt'])) {
				$receipt = true;
			}
	    	Yii::app()->functions->displayOrderHTML($this->data, isset($_SESSION['kr_item'])?$_SESSION['kr_item']:'',$receipt );
	    	$this->code=Yii::app()->functions->code;
	    	$this->msg=Yii::app()->functions->msg;
	    	$this->details=Yii::app()->functions->details;	    	
	    }	
	    
	    public function deleteItem()
	    {
	    	if ( isset($_SESSION['kr_item'][$this->data['row']])){
	    			    		
	    		//if (is_numeric($row_api_id)){
	    		if (isset($_SESSION['kr_item'][$this->data['row']]['row_api_id'])){
	    			$row_api_id=$_SESSION['kr_item'][$this->data['row']]['row_api_id'];	 
	    			$ApiFunctions=new ApiFunctions;
	    			$ApiFunctions->deleteItemFromCart($row_api_id);
	    		}	    	
	    		
	      	   unset($_SESSION['kr_item'][$this->data['row']]);
	    	}
	    	$this->code=1;
	    	$this->msg="";
	    }
	    
	    public function setDeliveryOptions()
	    {
	    	
	       if ( isset($this->data['delivery_type']) &&  !empty($this->data['delivery_type'])){

			   if($this->data['delivery_type'] == 'economy') {

				   //driver_id tidak boleh kosong jika user memilih economy
				   if ( !isset($this->data['driver_id']) ||  empty($this->data['driver_id'])){
					   $this->msg=t("You have to choose your driver");
					   return ;
				   }
				   $hour = query('SELECT value FROM {{driver_hour}}');
				   $list_hour = array();
				   foreach ($hour as $h) {
					   $list_hour[] = $h['value'];
				   }
				   //kalau jam yang masuk tidak tersedia
				   if(!in_array($this->data['delivery_time'],$list_hour)) {
					    $this->msg=t("Delivery time for Economy order is not valid");
						return ;
				   }
			   }
		   } else {
				 $this->msg=t("You have to choose order type Express or Economy");
				 return ;
		   }

	       /** check if time is non 24 hour format */	    
	       if ( yii::app()->functions->getOptionAdmin('website_time_picker_format')=="12"){
	       	   if (!empty($this->data['delivery_time'])){
	       	      $this->data['delivery_time']=date("G:i", strtotime($this->data['delivery_time']));	       	      
	       	   }
	       }
	       	       
	       /**check if customer chooose past time */
	       if ( isset($this->data['delivery_time'])){
	       	  if(!empty($this->data['delivery_time'])){
	       	  	 $time_1=date('Y-m-d g:i:s a');
	       	  	 $time_2=$this->data['delivery_date']." ".$this->data['delivery_time'];
	       	  	 $time_2=date("Y-m-d g:i:s a",strtotime($time_2));	       	  	 
	       	  	 $time_diff=Yii::app()->functions->dateDifference($time_2,$time_1);	       	  	
	       	  	 if (is_array($time_diff) && count($time_diff)>=1){
	       	  	     if ( $time_diff['hours']>0){	       	  	     	
		       	  	     $this->msg=t("Sorry but you have selected time that already past");
		       	  	     return ;	       	  	     	
	       	  	     }	       	  	
	       	  	 }	       	  
	       	  }	       
	       }		       

			   
	       $_SESSION['kr_delivery_options']['delivery_type']=$this->data['delivery_type'];
	       $_SESSION['kr_delivery_options']['delivery_date']=$this->data['delivery_date'];
	       $_SESSION['kr_delivery_options']['delivery_time']=$this->data['delivery_time'];
	       $_SESSION['kr_delivery_options']['delivery_asap']=$this->data['delivery_asap']=="undefined"?"":1;	       	       	       
	       $_SESSION['kr_delivery_options']['driver_id']=$this->data['driver_id'];
		   
	       $time=isset($this->data['delivery_time'])?$this->data['delivery_time']:'';	       
	       $full_booking_time=$this->data['delivery_date']." ".$time;
		   $full_booking_day=strtolower(date("D",strtotime($full_booking_time)));			
		   $booking_time=date('h:i A',strtotime($full_booking_time));			
		   if (empty($time)){
		   	  $booking_time='';
		   }

		   $merchant_id=isset($this->data['merchant_id'])?$this->data['merchant_id']:'';		   
		   if ( !Yii::app()->functions->isMerchantOpenTimes($merchant_id,$full_booking_day,$booking_time)){	
				$date_close=date("F,d l Y h:ia",strtotime($full_booking_time));
				$date_close=Yii::app()->functions->translateDate($date_close);
				$this->msg=t("Sorry but we are closed on")." ".$date_close;
				$this->msg.="<br/>";
				$this->msg.=t("Please check merchant opening hours");
			    return ;
			}					
	       
	       $this->code=1;$this->msg=Yii::t("default","OK");	       
	    }
	    
	    public function clientRegistration()
	    {
	    	
	    	/** check if admin has enabled the google captcha*/    	    	
	    	if ( getOptionA('captcha_customer_signup')==2){
	    		if ( GoogleCaptcha::checkCredentials()){
	    			if ( !GoogleCaptcha::validateCaptcha()){
	    				$this->msg=GoogleCaptcha::$message;
	    				return false;
	    			}	    		
	    		}	    	
	    	} 
	    	
	    	/*add confirm password */
	    	if (isset($this->data['cpassword'])){
	    		if ($this->data['cpassword'] != $this->data['password']){
	    			$this->msg=t("Confirm password does not match");
	    			return ;
	    		}	    	
	    	}	    	
	    	
	    	/*check if email address is blocked*/
	    	if ( FunctionsK::emailBlockedCheck($this->data['email_address'])){
	    		$this->msg=t("Sorry but your email address is blocked by website admin");
	    		return ;
	    	}	    
	    	
	    	if ( FunctionsK::mobileBlockedCheck($this->data['contact_phone'])){
	    		$this->msg=t("Sorry but your mobile number is blocked by website admin");
	    		return ;
	    	}	    	
	    		    	
	    	/*check if mobile number already exist*/
	        $functionk=new FunctionsK();
	        if ( !$res=Yii::app()->functions->isClientExist($this->data['email_address']) ){
		        if ( $functionk->CheckCustomerMobile($this->data['contact_phone'])){
		        	$this->msg=t("Sorry but your mobile number is already exist in our records");
		        	return ;
		        }	  
	        }
	        	        
	    	if ( !$res=Yii::app()->functions->isClientExist($this->data['email_address']) ){
	    		$params=array(
	    		  'first_name'=>$this->data['first_name'],
	    		  // 'last_name'=>$this->data['last_name'],
	    		  'email_address'=>$this->data['email_address'],
	    		  'password'=>md5($this->data['password']),
	    		  'date_created'=>date('c'),
	    		  'ip_address'=>$_SERVER['REMOTE_ADDR'],
	    		  'contact_phone'=>$this->data['contact_phone']
	    		);
	    		
	    		/** send verification code */
                $verification=Yii::app()->functions->getOptionAdmin("website_enabled_mobile_verification");	    
		    	if ( $verification=="yes"){
		    		$code=Yii::app()->functions->generateRandomKey(5);
		    		Yii::app()->functions->sendVerificationCode($this->data['contact_phone'],$code);
		    		$params['mobile_verification_code']=$code;
		    		$params['status']='pending';
		    	}	    	  
		    	
		    	/*send email verification added on version 3*/
		    	$email_code=Yii::app()->functions->generateCode(10);
		    	$email_verification=getOptionA('theme_enabled_email_verification');
		    	if ($email_verification==2){
		    		$params['email_verification_code']=$email_code;
		    		$params['status']='pending';
		    		FunctionsV3::sendEmailVerificationCode($params['email_address'],$email_code,$params);
		    	}
	    	
		    	/** update 2.3*/
		    	if (isset($this->data['custom_field1'])){
		    		$params['custom_field1']=!empty($this->data['custom_field1'])?$this->data['custom_field1']:'';
		    	}
		    	if (isset($this->data['custom_field2'])){
		    		$params['custom_field2']=!empty($this->data['custom_field2'])?$this->data['custom_field2']:'';
		    	}
		    			    	
	    		if ( $this->insertData("{{client}}",$params)){
	    			$this->details=Yii::app()->db->getLastInsertID();	    		
	    			$this->code=1;
	    			$this->msg=Yii::t("default","Registration successful");

	    			if ( $verification=="yes"){	    				
	    				$this->msg=t("We have sent verification code to your mobile number");
	    			} elseif ( $email_verification ==2 ){ 
	    				$this->msg=t("We have sent verification code to your email address");
	    			} else {
	    			   Yii::app()->functions->clientAutoLogin($this->data['email_address'],$this->data['password']);
	    			}	    			
	    			
	    			/*sent welcome email*/
	    			FunctionsK::sendCustomerWelcomeEmail($this->data);
	    				    			
	    			
	    		} else $this->msg=Yii::t("default","Something went wrong during processing your request. Please try again later.");
	    	} else {	    			    		
	    		$verification=Yii::app()->functions->getOptionAdmin("website_enabled_mobile_verification");	    
		    	if ( $verification=="yes"){
		    		if (strlen($res['mobile_verification_code'])>=2 && $res['status']=='pending'){
		    			$this->msg=t("Found existing registration");
		    			$this->code=1;
		    			$this->details=$res['client_id'];
		    			return ;
		    		}		    	
		    	}
	    	   $this->msg=Yii::t("default","Sorry but your email address already exist in our records.");
	    	}
	    }		    	 
	    
	    public function clientRegistrationModal()
	    {
	    	
	    	/** check if admin has enabled the google captcha*/    	    	
	    	/*if ( getOptionA('captcha_customer_signup')==2){
	    		if ( GoogleCaptcha::checkCredentials()){
	    			if ( !GoogleCaptcha::validateCaptcha()){
	    				$this->msg=GoogleCaptcha::$message;
	    				return false;
	    			}	    		
	    		}	    	
	    	}*/ 	    		    		    
	    	$this->clientRegistration();	    	
	    }   
	    
	    public function clientLogin()
	    {	
	    		    		    	
	    	/** check if admin has enabled the google captcha*/    	    	
	    	if ( $this->data['action']=="clientLogin" || $this->data['action']=="clientLoginModal"){
		    	if ( getOptionA('captcha_customer_login')==2){
		    		if ( GoogleCaptcha::checkCredentials()){
		    			if ( !GoogleCaptcha::validateCaptcha()){	    				
		    				$this->msg=GoogleCaptcha::$message;
		    				return false;
		    			}	    		
		    		}	    	
		    	}
	    	}
	    		    	
	    	/*check if email address is blocked by admin*/	    	
	    	if ( FunctionsK::emailBlockedCheck($this->data['username'])){
	    		$this->msg=t("Sorry but your email address is blocked by website admin");
	    		return ;
	    	}	    	
	    		    	
	    	if (!isset($this->data['password_md5'])){
	    		$this->data['password_md5']='';
	    	}	    
	    	if ( Yii::app()->functions->clientAutoLogin($this->data['username'],
	    	    $this->data['password'],$this->data['password_md5']) ){
	    		$this->code=1;
	    		$this->msg=Yii::t("default","Login Okay");
	    	} else {
	    		/*check if user has pending application like mobile verification and email*/
	    		if ( $res=FunctionsV3::login($this->data['username'],$this->data['password'])){
	    			if (strlen($res['mobile_verification_code'])>=2 && $res['status']=='pending'){
	    				$this->msg=t("Found existing registration");
	    				$this->code=3;
	    				$this->details=$res['client_id'];
	    			} elseif (strlen($res['email_verification_code'])>=2 && $res['status']=='pending' ){	
	    				$this->code=4;
	    				$this->details=$res['client_id'];
	    				$this->msg=t("we have sent you email with verification");
	    			} else $this->msg=Yii::t("default","Login Failed. Either username or password is incorrect");
	    		} else $this->msg=Yii::t("default","Login Failed. Either username or password is incorrect"); 
	    	}
	    }
	    
	    public function clientLoginModal()
	    {
	    	$this->clientLogin();
	    }	
	    
	    public function placeOrder()
	    {	
	    	// set merchant timezone 
	    	// $mtid=$_SESSION['kr_merchant_id'];	    	
	    	// $mt_timezone=Yii::app()->functions->getOption("merchant_timezone",$mtid);	   	   	    	
	    	// if (!empty($mt_timezone)){
	    		// Yii::app()->timeZone=$mt_timezone;
	    	// }
			
	    	if (!isset($this->data['street'])){
				$this->msg=t("Delivery Address is required");
				return ;
			}
			
	    	if (!isset($this->data['street']) || empty($this->data['street'])){
				$this->msg=t("Delivery Address is required");
				return ;
			}

	    	

	    		    		    		    		    		    	  
	    	/*guest checkout*/
	    	if (isset($this->data['is_guest_checkout'])){
	    		$Validator=new Validator;
	    		
	    		if (empty($this->data['email_address'])){
	    			$this->data['email_address']=str_replace(" ","_",$this->data['first_name']).
	    			Yii::app()->functions->generateRandomKey()."@".$_SERVER['HTTP_HOST'];
	    		}	    	
	    			    			    	
	    		if ( !$res_check=Yii::app()->functions->isClientExist($this->data['email_address']) ){	  
	    			$this->data['password']=isset($this->data['password'])?$this->data['password']:'';
	    			if (empty($this->data['password'])){
	    				$this->data['password']=Yii::app()->functions->generateRandomKey();
	    			}	  
	    			

	    			/*check if email address is blocked*/
			    	if ( FunctionsK::emailBlockedCheck($this->data['email_address'])){
			    		$this->msg=t("Sorry but your email address is blocked by website admin");
			    		return ;
			    	}
			    	
			    	if ( FunctionsK::mobileBlockedCheck($this->data['contact_phone'])){
			    		$this->msg=t("Sorry but your mobile number is blocked by website admin");
			    		return ;
			    	}	  
			    	
			    	$functionk=new FunctionsK();

	    			
	    			$params=array(
		    		  'first_name'=>$this->data['first_name'],
		    		  'last_name'=>$this->data['last_name'],
		    		  'email_address'=>$this->data['email_address'],
		    		  'password'=>md5($this->data['password']),
		    		  'street'=>$this->data['street'],
		    		  'city'=>$this->data['city'],
		    		  'state'=>$this->data['state'],
		    		  'zipcode'=>$this->data['zipcode'],
		    		  'contact_phone'=>$this->data['contact_phone'],
		    		  'location_name'=>$this->data['location_name'],
		    		  'date_created'=>date('c'),
		    		  'ip_address'=>$_SERVER['REMOTE_ADDR'],
		    		  'contact_phone'=>$this->data['contact_phone']
		    		);
		    				    		
		    				    		
			    	if ( $this->insertData("{{client}}",$params)){
		    			Yii::app()->functions->clientAutoLogin($this->data['email_address'],$this->data['password']);
		    		} else $Validator->msg[]=t("Something went wrong during processing your request. Please try again later.");
	    		} else $Validator->msg[]=t("Sorry but your email address already exist in our records.");
	    		
	    		if (!$Validator->validate()){
	    			$this->msg=$Validator->getErrorAsHTML();
	    			return ;
	    		}	    	
	    	}	    
	    	/*guest checkout*/	    	
	    	
	    	// $this->data['merchant_id']=$_SESSION['kr_merchant_id'];	 
	    	
	    	// $default_order_status=Yii::app()->functions->getOption("default_order_status",$_SESSION['kr_merchant_id']);	    		    	
	    	$order_item=$_SESSION['kr_item'];
			
	    	if (is_array($order_item) && count($order_item)>=1){
	    		//dump($this->data);

	    		
	    		Yii::app()->functions->displayOrderHTML($this->data,$_SESSION['kr_item']);
				$details = Yii::app()->functions->details;

				
	    		if ( Yii::app()->functions->code==1) {
					$raw=Yii::app()->functions->details['raw'];
					
					if($raw['ret_delivery'] == false) {
						$this->msg=t("Your address is invalid.");
						return ;
					}
					
					//Validasi cek apakah belanja user melebihi saldo
					if($this->data['payment_opt'] == 'credit') {
						$client_id = Yii::app()->functions->getClientId();
						$client = query("SELECT * FROM {{client}} WHERE client_id=?",array($client_id));
						$saldo = $client[0]['saldo'];

						if ($saldo) {
							//apakah total belanja melebihi saldo
							if($raw['total']['total'] > $saldo) {
								$this->msg=t("Your balance is insufficient. We recommend you to top up your balance first.");
								$this->output();
								return ;
							}
						} else {
							$this->msg=t("Your balance is empty. Please topup your balance.");
							$this->output();
							return ;
						}
					}
					
	    			if (is_array($raw) && count($raw)>=1){
						$order_item2 = $raw['item'];
						// vdump($raw['item']);
						// vdump("----");
						// vdump($order_item);
						
						//penyesuaian data 
						for($i=0;$i<count($order_item2);$i++) {
							$order_item2[$i]['discount']="";
							$order_item2[$i]['currentController']="store";
							$order_item2[$i]['notes']=$order_item2[$i]['order_notes'];
							$order_item2[$i]['price']=$order_item2[$i]['normal_price'];
						}

	    				$params=array(
	    				  'merchant_id'=>$this->data['merchant_id'],
						  // 'merchant_ids'=>$this->data['merchant_id'],
	    				  'client_id'=>Yii::app()->functions->getClientId(),
	    				  'json_details'=>json_encode($order_item2),
	    				  'trans_type'=>isset($_SESSION['kr_delivery_options']['delivery_type'])?$_SESSION['kr_delivery_options']['delivery_type']:'',
						  'weight'=>isset($raw['total']['weight_total'])?$raw['total']['weight_total']:'',
	    				  'payment_type'=>isset($this->data['payment_opt'])?$this->data['payment_opt']:'',
	    				  'sub_total'=>isset($raw['total']['subtotal'])?$raw['total']['subtotal']:'',
	    				  'tax'=>isset($raw['total']['tax'])?$raw['total']['tax']:'',
	    				  'taxable_total'=>isset($raw['total']['taxable_total'])?$raw['total']['taxable_total']:'',
	    				  'total_w_tax'=>isset($raw['total']['total'])?$raw['total']['total']:'',
	    				  'delivery_charge'=>isset($raw['total']['delivery_charges'])?$raw['total']['delivery_charges']:'',
	    				  'delivery_date'=>isset($_SESSION['kr_delivery_options']['delivery_date'])?$_SESSION['kr_delivery_options']['delivery_date']:'',
	    				  'delivery_time'=>isset($_SESSION['kr_delivery_options']['delivery_time'])?$_SESSION['kr_delivery_options']['delivery_time']:'',
	    				  'delivery_asap'=>isset($_SESSION['kr_delivery_options']['delivery_asap'])?$_SESSION['kr_delivery_options']['delivery_asap']:'',
	    				  'date_created'=>date('c'),
	    				  'ip_address'=>$_SERVER['REMOTE_ADDR'],
	    				  'delivery_instruction'=>isset($this->data['delivery_instruction'])?$this->data['delivery_instruction']:'',
	    				  'cc_id'=>isset($this->data['cc_id'])?$this->data['cc_id']:'',
	    				  'order_change'=>isset($this->data['order_change'])?$this->data['order_change']:'',
	    				  'payment_provider_name'=>isset($this->data['payment_provider_name'])?$this->data['payment_provider_name']:'',
						  'planned_driver'=>$this->data['driver_id'],
	    				);
	    				
	    				  			
						$status="pending";
	    					    					    				
	    				/*PROMO*/	    				
	    				if (isset($raw['total']['discounted_amount'])){
		    				if ($raw['total']['discounted_amount']>=0.1){	    					
		    				    $params['discounted_amount']=$raw['total']['discounted_amount'];
		    				    $params['discount_percentage']=$raw['total']['merchant_discount_amount'];
		    				}
	    				}
	    				
	    		        	    		        	    		        	    		       
	    		        /*if has address book selected*/
	    		        if ( isset($this->data['address_book_id'])){
	    		        	if ($address_book=Yii::app()->functions->getAddressBookByID($this->data['address_book_id'])){
	    		        		$this->data['street']=$address_book['street'];
	    		        		$this->data['city']=$address_book['city'];
	    		        		$this->data['state']=$address_book['state'];
	    		        		$this->data['zipcode']=$address_book['zipcode'];
	    		        		$this->data['location_name']=$address_book['location_name'];
	    		        	}
	    		        }
	    		        
	    		        $country_code='';
	    		        $country_name='';
	    		        
	    		        if ( Yii::app()->functions->getOptionAdmin('website_enabled_map_address')==2){
		    		        if (isset($this->data['map_address_toogle'])){
		    		        	if ( $this->data['map_address_toogle']==2){
		    		        		$geo_res=geoCoding($this->data['map_address_lat'],
		    		        		$this->data['map_address_lng']);
		    		        	    if ($geo_res){
		    		        			//dump($geo_res);
		    		        			$this->data['street']=isset($geo_res['street_number'])?$geo_res['street_number']." ":'';
		    		        			$this->data['street'].=isset($geo_res['street'])?$geo_res['street']." ":'';
		    		        			$this->data['street'].=isset($geo_res['street2'])?$geo_res['street2']." ":'';
		    		        			
		    		        			$this->data['city']=$geo_res['locality'];
		    		        			$this->data['state']=$geo_res['admin_1'];
		    		        			$this->data['zipcode']=isset($geo_res['postal_code'])?$geo_res['postal_code']:'';
		    		        			$country_code=$geo_res['country_code'];
		    		        			$country_name=$geo_res['country'];
		    		        		} else {
		    		        			$this->msg=t("Sorry but something wrong when geocoding your address");
		    		        			return false;
		    		        		}
		    		        	}
		    		        }		
	    		        }

	    		        
	    				if ( $this->insertData("{{order}}",$params)){
		    				$order_id=Yii::app()->db->getLastInsertID();
		    				

		    				/** save to address book*/
		    				if (!isset($this->data['saved_address'])){
		    					$this->data['saved_address']='';
		    				}
				            if ( $this->data['saved_address']==2) {
				            	$sql_up="UPDATE {{address_book}}
					     		SET as_default='1' 	     		
					     		";
					     		$this->qry($sql_up);
				            	$params_i=array(
				            	  'client_id'=>Yii::app()->functions->getClientId(),
				            	  'street'=>$this->data['street'],
				            	  'city'=>$this->data['city'],
				            	  'state'=>$this->data['state'],
				            	  'zipcode'=>$this->data['zipcode'],
				            	  'location_name'=>$this->data['location_name'],
				            	  'date_created'=>date('c'),
				            	  'ip_address'=>$_SERVER['REMOTE_ADDR'],
				            	  'country_code'=>Yii::app()->functions->adminCountry(true),
				            	  'as_default'=>2
				            	);
				            	$this->insertData("{{address_book}}",$params_i);
				            }

				            
							$list_merchant = array();
							$tax_merchants = array();
								//CEKK ULANGGG ()
		    				foreach ($raw['item'] as $val) {
								$total_per_item = $val['qty']*($val['normal_price']);
								if(isset($list_merchant[$val['merchant_id']])) {
									$list_merchant[$val['merchant_id']] = $total_per_item + $list_merchant[$val['merchant_id']];
								} else {
									$list_merchant[$val['merchant_id']] = $total_per_item;
								}
								
								//kalkulasi tax untuk setiap merchant
								//sesuai produknya masing2
								if(isset($tax_merchants[$val['merchant_id']])) {
									//jika key merchant_id sudah diset
									$tax_merchants[$val['merchant_id']] += $val['tax'] * $total_per_item / 100;
								} else {
									//jika key merchant_id belum diset
									$tax_merchants[$val['merchant_id']] = $val['tax'] * $total_per_item / 100;
								}
								
		    					$params_order_details=array(
		    					  'order_id'=>$order_id,
								  'merchant_id'=>$val['merchant_id'],
		    					  'client_id'=>Yii::app()->functions->getClientId(),
		    					  'item_id'=>isset($val['item_id'])?$val['item_id']:'',
		    					  'item_name'=>isset($val['item_name'])?$val['item_name']:'',
		    					  'order_notes'=>isset($val['order_notes'])?$val['order_notes']:'',
		    					  'normal_price'=>isset($val['normal_price'])?$val['normal_price']:'',
		    					  'discounted_price'=>isset($val['discounted_price'])?$val['discounted_price']:'',
		    					  'qty'=>isset($val['qty'])?$val['qty']:'',
								  'tax'=>isset($val['tax'])?$val['tax']:'',
								  'weight'=>isset($val['weight'])?$val['weight']:'',
								  'photo'=>isset($val['photo'])?$val['photo']:'',
								  'uom'=>isset($val['uom'])?$val['uom']:'',
		    					  'non_taxable'=>isset($val['non_taxable'])?$val['non_taxable']:1
		    					);
		    					$this->insertData("{{order_details}}",$params_order_details);
		    				}
		    				
							$merchants_as_value = array();
							
							//list merchant berisi merchant_id dan earning
							//tax_merchants berisi tax untuk merchant tertentu. identifikasinya melalui key yang berisi merchant_id
		    				foreach ($list_merchant as $merchant_id => $subtotal_merchant) {
		    					$params_merchants=array(
		    					  'order_id'=>$order_id,
								  'merchant_id'=>$merchant_id,
								  'subtotal'=>$subtotal_merchant,
								  'part_status'=>'pending',
								  'tax'=>$tax_merchants[$merchant_id],
								  'earning'=>$subtotal_merchant+$tax_merchants[$merchant_id],
								  //earning merchant adalah penjumlahan dari subtotal ditambah tax
		    					);
		    					$this->insertData("{{order_merchant}}",$params_merchants);
								$merchants_as_value[] = $merchant_id;
		    				}
							
							$str_merchants = implode(',',$merchants_as_value);
							
							//get merchants
							$merchants = query("SELECT merchant_id,merchant_name,contact_name,contact_phone,street,city,state,post_code,latitude,longitude FROM {{view_merchant}} WHERE merchant_id IN(".$str_merchants.")");
							
							/** add delivery address */
			    				$params_address=array(
			    				  'order_id'=>$order_id,
			    				  'client_id'=>Yii::app()->functions->getClientId(),
			    				  'street'=>isset($this->data['street'])?$this->data['street']:'',
			    				  'city'=>isset($this->data['city'])?$this->data['city']:'',
			    				  'state'=>isset($this->data['state'])?$this->data['state']:'',
			    				  'zipcode'=>isset($this->data['zipcode'])?$this->data['zipcode']:'',
			    				  'location_name'=>isset($this->data['location_name'])?$this->data['location_name']:'',
			    				  'country'=>Yii::app()->functions->adminCountry(),
			    				  'date_created'=>date('c'),
			    				  'ip_address'=>$_SERVER['REMOTE_ADDR'],
			    				  'contact_phone'=>$this->data['contact_phone'],
								  'origin_merchant'=>json_encode($merchants),
								  'latitude'=>$raw['ret_delivery']['client_lat'],
								  'longitude'=>$raw['ret_delivery']['client_lng'],
			    				);
			    				
			    				if (!empty($country_name)){
			    					$params_address['country']=$country_name;
			    				}			
			    				
			    				$this->insertData("{{order_delivery_address}}",$params_address);
			    				
			    				/** quick update mobile*/
			    				$params_mobile=array(
			    				  'contact_phone'=>$this->data['contact_phone'],
			    				  'location_name'=>isset($this->data['location_name'])?$this->data['location_name']:''
			    				 );
			    				$this->updateData("{{client}}",$params_mobile,'client_id',
			    				Yii::app()->functions->getClientId());
							
							
							if($this->data['payment_opt'] == 'credit' && isset($saldo)) {
								$new_saldo = $saldo-$raw['total']['total'];
								queryNoFetch("UPDATE {{client}} SET saldo=? WHERE client_id=?",array($new_saldo,$client_id));
							}
							
		    				$this->code=1;
							$this->msg=Yii::t("default","Please wait while we redirect...");
		    				$this->details=array(
		    				  'order_id'=>$order_id,
		    				  'payment_type'=>$this->data['payment_opt']
		    				);
		    				
		    				//Yii::app()->functions->updateClient($this->data);
		    				
	    				} else $this->msg=Yii::t("default","ERROR: Cannot insert records.");
	    			} else $this->msg=Yii::t("default","ERROR: Something went wrong");	    		
	    		} else $this->msg=Yii::app()->functions->msg;
	    	} else $this->msg=Yii::t("default","Sorry but your order is empty");	    
	    }
	    
	    public function addRating()
	    {	    	
	    	$DbExt=new DbExt;
	    	
	    		    	    	
	    	if ( empty($this->data['merchant_id'])){
	    		$this->msg=Yii::t("default","Merchant ID is missing");
	    	    return ;
	    	}	    
	    	
	    	
	    	if (Yii::app()->functions->isClientLogin()){	    		
	    		$client_id=Yii::app()->functions->getClientId();	    		 	   
	    	    $params=array(
	    	      'merchant_id'=>$this->data['merchant_id'],
	    	      'ratings'=>$this->data['value'],
	    	      'client_id'=>$client_id,
	    	      'date_created'=>date('c'),
	    	      'ip_address'=>$_SERVER['REMOTE_ADDR']
	    	    );	    	    
	    	    
	    	    /** check if user has bought from the merchant*/	    	    
	    	    if ( Yii::app()->functions->getOptionAdmin('website_reviews_actual_purchase')=="yes"){
		    	    $functionk=new FunctionsK();
		    	    if (!$functionk->checkIfUserCanRateMerchant($client_id,$this->data['merchant_id'])){
		    	    	$this->code=3;
		    	    	$this->msg=t("Reviews are only accepted from actual purchases!");
		    	    	return ;
		    	    }	    		    	    
	    	    }
	    	    	    	    
	    	    if ( !$res=Yii::app()->functions->isClientRatingExist($this->data['merchant_id'],$client_id) ){	    
	    	    	$DbExt->insertData("{{rating}}",$params);
	    	    	$this->code=1;	 
	    	    	$this->msg=Yii::t("default","Successful"); 
	    	    } else {	    	    	
	    	    	$rating_id=$res['id'];	    	    	
	    	    	$update=array(
	    	    	  'ratings'=>$this->data['value'],
	    	    	   'date_created'=>date('c'),
	    	           'ip_address'=>$_SERVER['REMOTE_ADDR']
	    	        );
	    	    	if ( $DbExt->updateData("{{rating}}",$update,'id',$rating_id) ){
	    	    		$this->code=1;	 
	    	    	    $this->msg=Yii::t("default","Successful"); 
	    	    	} else {
	    	    		$this->msg=Yii::t("default","Sorry there's an error white updating you rating.");	    	    
	    	    		$this->code=3;
	    	    	}	    	    
	    	    }	    	
	    	} else $this->msg=Yii::t("default","Sorry but you need to login before you can make a rating.");	    	
	    }
	    

	public function actionGetEconomyDriver()
	{
		if (!isset($this->data['cart'])){
			$this->msg=$this->t("List item id is missing");
			$this->output();
		}
		if (!isset($this->data['latitude']) || !isset($this->data['longitude'])){
			$this->msg=$this->t("We need your location, please input latitude and longitude");
			$this->output();
		}
		
		//1 mendapatkan cart => list_item_id kemudian dikonvert menjadi lokasi2 merchant nya
		//2 dari lokasi2 merchant 
		//(lokasi customer tidak perlu dipertimbangkan karena driver 
		//pasti membawa barang belanjaan dulu dari merchant2 kemudian 
		//menuju customer)
		//3 hitung jarak driver2 yang terdekat (dihitung dari region driver, bukan dari lokasi driver saat ini) 
		//  dan available pada hari itu 
		//  ada maksimum distancenya (sebutlah maksium distance 40km dari merchant)
		//4 jika tidak ada yang di dalam radius maka output response tidak ada driver yang tersedia 
		//  pada jarak tersebut
		//	jika ada driver maka return driver tsb
		
		
		$cart = json_decode($this->data['cart']);
		$weight_total = 0;
		foreach($cart as $c) {
			//hitung weight total
			$res = query("SELECT weight FROM {{item}} WHERE item_id=?",array($c->item_id));
			foreach($res as $food_info){
				$weight_total += $food_info['weight']?$food_info['weight']*$c->qty:(500*$c->qty);
			}
			$list_item_id[] = $c->item_id;
		}
		$list_item_id = implode(',',$list_item_id);
		//1
		// var_dump($list_item_id);
		$merchants = query("SELECT m.latitude,m.longitude,m.merchant_id FROM {{item}} i 
					JOIN {{view_merchant}} m 
					ON i.merchant_id=m.merchant_id 
					WHERE i.item_id IN($list_item_id) GROUP BY m.merchant_id",array());
		// var_dump($merchants);
		//metode region
		$distance_exp=6371;

		$drivers = array();
		$big_drivers = array();
		$day_number = date('N'); // output: current day.
		$day_number--;
		$motor_max_weight = Yii::app()->functions->getOptionAdmin('motor_max_weight');
		
		// var_dump($day_number);
		foreach($merchants as $m) {
			$DbExt=new DbExt; 
			$DbExt->qry("SET SQL_BIG_SELECTS=1");
			$total_records=0;
			$data='';
			$c = Yii::app()->db->createCommand();
			$lat = $m['latitude'];
			$long = $m['longitude'];
			//get near driver
			if(!empty($lat) && !empty($long)) {
				$sql_near_location = ", 
								 ( $distance_exp * acos( cos( radians($lat) ) * cos( radians( location_lat ) ) 
								 * cos( radians( location_lng ) - radians($long) ) 
								 + sin( radians($lat) ) * sin( radians( location_lat ) ) ) ) 
								 AS distance ";
				$radius = 40;
				$c->select('d.driver_id,d.first_name,d.location_lat,d.location_lng,d.region,d.pickupstandby
							'.$sql_near_location);
				$c->from('{{driver}} d');
				$c->having('distance < :home_search_radius',array(':home_search_radius'=>$radius));		
				
				// $c->join('{{driver_economy}} e','d.driver_id=e.driver_id');
				$c->group('d.driver_id');
				$c->where('1');
				
				//validasi motor/mobil (weight dalam gram, motor_max_weight dalam kg)
				if($weight_total/1000 > $motor_max_weight) {
					//berat order > beban maksimal motor maka pakai mobil
					$c->andWhere("d.transport_type_id LIKE '%mobil%'");
				} else {
					//beban masih jangkauan motor
					$c->andWhere("d.transport_type_id LIKE '%motor%'");
				}
				
				//ambil driver yang hari ini aktif di driver ekonomy
				// $c->andWhere('e.day= :day_number',array(':day_number'=>$day_number));
				// $c->andWhere('e.available=1');
				// $c->limit($this->data['limit'],($this->data['page']-1)*$this->data['limit']);
				$r = $c->queryAll();
				// var_dump($r);
				//save driver data for future
				$big_drivers[] = $r;
				
				//ambil yang jaraknya terdekat antara driver dengan salah satu merchant
				foreach($r as $s) {
					// var_dump($s);
					if(isset($driver[$s['driver_id']])) {
						//jika jarak berikutnya lebih kecil maka ambil yang lebih kecil
						if($driver[$s['driver_id']] > $s['distance']) {
							$driver[$s['driver_id']] = $s['distance'];
						}
					} else {
						$driver[$s['driver_id']] = $s['distance'];
					}
				}
			}

		}
		
		//didapatkan jarak total dari suatu driver ke seluruh merchant
		asort($driver);
		//ambil 7 terbaik
		$driver = array_slice($driver, 0, 10,true); 
		
		$hour = query("SELECT * FROM {{driver_hour}}");
		
		//gunakan data query sebelumnya ($big_drivers)
		//utk meminimalkan query ke database
		$active_hour = array('03:00','06:00','09:00','12:00','15:00','17:00','21:00','23:59');
		foreach($big_drivers as $b) {
			foreach($b as $d){
				if(isset($driver[$d['driver_id']])) {
					// $active_hour = array();
					// foreach($hour as $v) {
						// //melakukan pengecekan pada jam tertentu apakah aktif
						// if($d[$v['label']]) {
							// $active_hour[] = $v['value'];
						// }
					// }
					$driver[$d['driver_id']] = array(
						'driver_id' => $d['driver_id'],
						'name' => $d['first_name'],
						'region' => $d['region'],
						'active_hour' => $active_hour,
						'pickupstandby' => $d['pickupstandby'],
					);
				}
			}
		}
		
		$m = 0;
		$driver2 = array();
		//buang key nya sebelum return output
		foreach($driver as $d) {
			$m++;
			$driver2[] = $d;
		}
				
		if(empty($driver2)) {
			$this->msg=AddonMobileApp::t("Driver is currenty not available, sorry");
		} else {
			$this->details=$driver2;
			$this->code=1;
			$this->msg="OK";
		}
		// $this->output();
	}
	
	
		public function loadDriverEconomy()
		{
			
			//fungsi ini melakukan pemanggilan fungsi actionGetEconomyDriver utk mendapatkan driver
			//list driver
			//fungsi ini kemudian merender data tersebut utk ditampilkan ke user
			
			// dump($_SESSION['kr_item']);
			if(!isset($_SESSION['kr_item']) || !isset($_SESSION['client_location']['lat'])) {
				die;
			}
			$latitude = "";
			$longitude = "";
			if(isset($_SESSION['client_location']['lat'])) {
				if($_SESSION['client_location']['lat']) {
					$latitude = $_SESSION['client_location']['lat'];
					$longitude = $_SESSION['client_location']['long'];
				}
			}
			$this->data['latitude'] = $latitude;
			$this->data['longitude'] = $longitude;
			$this->data['cart'] = json_encode($_SESSION['kr_item']);
			$this->actionGetEconomyDriver();
			// dump($this->details);
			
			if($this->code==1){
				?>
				<style>
				.driver-selected {
					background-color: #ff7f50;
				}
				.standby-driver:hover {
					background-color: #ff7f50;
				}
				</style>
				<h4 class="center"><b><?=t('Select Driver')?></b></h4><div class="block-3 margin-top-20"  style="display:block">
				<?php
				$pickarray = array('pickup'=>'Pickup');
				$standarray = array('pickup'=>'Standby');
				foreach($this->details as $d) {
					?>
					<div data-id="<?=$d['driver_id']?>" class="media box-driver <?=$d['pickupstandby']=='standby'?'standby-driver':'pickup-driver'?>" >
						<div class="<?=$d['pickupstandby']=='pickup'?'shade-pickup':'shade-standby'?>"></div>
						<div class="media-left">
							<a href="#">
							<img style="width:80px;" class="media-object" src="<?=assetsURL()."/images/Driver.png"?>" alt="...">
							</a>
						</div>
						<div class="media-body">
							<div class="row">
								<div class="col-md-12">
									<span class="pull-left"><?=$d['name']?></span>
									<span class="pull-right"><?=$d['region']?></span>
								</div>
								<div class="col-md-6 left-driver">
									<div class="outer"><div class="middle"><div class="inner">
									<?php 	$val = ($d['pickupstandby']=="standby")?"standby":"";
											echo CHtml::radioButton('pick'.$n++,$val,$pickarray) . ' Stand By';
									?>
									</div></div></div>
								</div>
								<div class="col-md-6 right-driver" >
									<div class="outer"><div class="middle"><div class="inner">
									<?php	$val = ($d['pickupstandby']=="pickup")?"pickup":"";
											echo CHtml::radioButton('pick'.$n++,$val,$pickarray) . ' Pickup';
									?>
									</div></div></div>
								</div>
							</div>
						</div>
						
					</div>
					<?php
				}
				?>	
					<h4 class="center margin-top-20"><b><?=t('Delivery Time')?></b></h4>
					<p class="center"><?=t('Date')?> : <?=date('d F Y', time())?></p>
					<div >
						<p class="center">
							<input type="checkbox" class="chkclass" data-id="03:00"/>03:00 &nbsp;
							<input type="checkbox" class="chkclass"  data-id="06:00"/>06:00 &nbsp;
							<input type="checkbox" class="chkclass"  data-id="09:00"/>09:00 &nbsp;
							<input type="checkbox" class="chkclass"  data-id="12:00"/>12:00 &nbsp;
						</p>
						<p class="center">
							
							<input type="checkbox" class="chkclass" data-id="15:00"/>15:00 &nbsp;
							<input type="checkbox" class="chkclass"  data-id="17:00"/>17:00 &nbsp;
							<input type="checkbox" class="chkclass"  data-id="21:00"/>21:00 &nbsp;
							<input type="checkbox" class="chkclass"  data-id="23:59"/>23:59 &nbsp;
							
						</p>
					</div>
					<p class="center margin-top-40">
					<a href="javascript:;" class="orange-button medium checkout">Order</a>

					</p>
					

				<?php
				echo "</div>";
			}
			die;
		}
		public function loadExpressEconomy()
		{
			//fungsi ini menampilkan pilihan tombol Express/Economy dan
			//juga step pilih tanggal dan waktu pengiriman
			//step pilih driver pada Economy kemudian difollowup oleh fungsi loadDriverEconomy
	    	?>
			<style>
			.ui-timepicker-minutes {
				background-color:#d3d3d3;
			}
.ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default {
    border: none;
    background: none;
    font-weight: normal;
    color: #555555;
}
.ui-timepicker td{
	padding:2px;
}
.ui-timepicker-title {
	text-align:center;
}
.ui-state-active, .ui-widget-content .ui-state-active, .ui-widget-header .ui-state-active {
    border: 1px solid #ca3c3c;
    background: #ca3c3c;
    font-weight: normal;
    color: #ffffff;
}
			</style>
	    	<div class="block-1 view-receipt-pop">
				 <h4 class="center"><b><?=t('Choose Order Type')?></b></h4>
				 <br/>
				 <p class="center">
					 <a href="javascript:;" class="green-button medium express-btn">Express</a>&nbsp;&nbsp;&nbsp;
					 <a href="javascript:;" class="orange-button medium economy-btn">Economy</a>
				 </p>
			</div>
			<div class="block-2 margin-top-20" style="display:none">
				<h4 class="center margin-bottom-20"><b><?=t('Delivery Time')?></b></h4>
				<?php echo CHtml::hiddenField('delivery_date3',$now)?>
				<div class="center">
					<?php echo CHtml::textField('delivery_date4',
									date('Y-m-d'),array('class'=>"j_date grey-fields",'placeholder'=>Yii::t("default","Delivery Date"),'data-id'=>'delivery_date4'))?>
				</div>
				<div class="center margin-top-10">
					<?php echo CHtml::textField('delivery_time4',$now_time,
					array('class'=>"timepick grey-fields",'placeholder'=>Yii::t("default","Delivery Hour")))?>
				</div>
				<p class="center margin-top-20">
					<a href="javascript:;" class="orange-button medium checkout">Order</a>
				</p>
		   </div>
			 <br/>
	    	</div> <!--view-receipt-pop-->
			<script type="text/javascript">
			var today_date ='<?=date('Y-m-d')?>';
			jQuery(document).ready(function() {
				initdatepicker();

				function initdatepicker(){
					$( ".j_date" ).datepicker({
						dateFormat: 'yy-mm-dd',
						prevText:'',
						nextText:'',
						buttonImageOnly: true,
						changeMonth: true,
						changeYear: true,
						yearRange: "2016:2025",
						onSelect : function( element, object ) {
							dump($(this).val());
							$("#delivery_date").val($(this).val());
						}
					});
					var show_period=false;
					if ( $("#website_time_picker_format").exists() ){		
						if ( $("#website_time_picker_format").val()=="12"){
							show_period=true;
						}
					}
					dump(show_period);
					jQuery('.timepick').timepicker({
						showPeriod: show_period,      
						hourText: js_lang.Hour,       
						minuteText: js_lang.Minute,  
						amPmText: [js_lang.AM, js_lang.PM],
					});
					$( document ).on( "change", ".timepick", function() {
						$("#delivery_time").val($(this).val());
					});
				}
				
			});

			</script>
	    	<?php
	    	die();
		}
		
	    public function loginModal()
	    {
	    	require_once 'login-modal.php';
	    	die();
	    }

	    public function loadTopMenu()
	    {	    	
	    	ob_start();	
	    	if ( Yii::app()->functions->isClientLogin()):
	    	?>
	    	    <div class="uk-button-dropdown" data-uk-dropdown="{mode:'click'}">
			    <button class="uk-button">
			       <i class="uk-icon-user"></i> <?php echo ucwords(Yii::app()->functions->getClientName());?> <i class="uk-icon-caret-down"></i>
			    </button>	    
			    <div class="uk-dropdown" style="">
			        <ul class="uk-nav uk-nav-dropdown">            	            
			            <li>
			            <a href="<?php echo Yii::app()->request->baseUrl; ?>/store/Profile">
			            <i class="uk-icon-user"></i> <?php echo Yii::t("default","Profile")?></a>
			            </li>
			            <li>
			            <a href="<?php echo Yii::app()->request->baseUrl; ?>/store/orderHistory">
			            <i class="uk-icon-gear"></i> <?php echo Yii::t("default","Order History")?></a>
			            </li>
			            
			            <?php if (Yii::app()->functions->getOptionAdmin('disabled_cc_management')==""):?>
			            <li>
			            <a href="<?php echo Yii::app()->request->baseUrl; ?>/store/Cards">
			            <i class="uk-icon-gear"></i> <?php echo Yii::t("default","Credit Cards")?></a>
			            </li>
			            <?php endif;?>
			            
			            <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/store/logout">
			            <i class="uk-icon-sign-out"></i> <?php echo Yii::t("default","Logout")?></a></li>
			        </ul>
			     </div>
			 </div> <!--uk-button-dropdown-->
	    	<?php	    	
	    	$forms = ob_get_contents();
            ob_end_clean();
            $this->code=1;
            $this->details=$forms;
	    	else :
	    	$this->code=2;
	    	endif;	    	
	    }    
	    
	    public function removeRating()
	    {	    	
	    	if ( Yii::app()->functions->isClientLogin()){
	    		$client_id=Yii::app()->functions->getClientId();
	    		Yii::app()->functions->removeRatings($this->data['merchant_id'],$client_id);
	    		$this->code=1;
	    		$this->msg="OK";
	    	} else $this->msg=Yii::t("default","Cannot remove ratings user is not login.");	    
	    }
	    
	    public function loadRatings()
	    {	    	
	    	$initial_rating='';
			$client_id=Yii::app()->functions->getClientId();  
			if ($ratings=Yii::app()->functions->getRatings($this->data['merchant_id'])){
				$this->code=1;
				$this->details=$ratings;
			} else $this->msg=Yii::t("default","Ratings not available.");    			
	    }	
	    
        public function addReviews()
	    {	    		    		    	
	    	$DbExt=new DbExt;
	    	if ( Yii::app()->functions->isClientLogin()){
		    	$client_id=Yii::app()->functions->getClientId();  		    	
		    	$params=array(
		    	  'merchant_id'=>$this->data['merchant-id'],
		    	  'client_id'=>$client_id,
		    	  'review'=>$this->data['review_content'],
		    	  'date_created'=>date('c'),
		    	  'rating'=>$this->data['initial_review_rating']
		    	);		    	
		    	
		    	/** check if user has bought from the merchant*/		    	
		    	if ( Yii::app()->functions->getOptionAdmin('website_reviews_actual_purchase')=="yes"){
		    		$functionk=new FunctionsK();
		    	    if (!$functionk->checkIfUserCanRateMerchant($client_id,$this->data['merchant-id'])){
		    	    	$this->msg=t("Reviews are only accepted from actual purchases!");
		    	    	return ;
		    	    }
		    	    		    	    	    	   
		    	    if (!$functionk->canReviewBasedOnOrder($client_id,$this->data['merchant-id'])){
		    		   $this->msg=t("Sorry but you can make one review per order");
		    	       return ;
		    	    }	  		   
		    	    
		    	    if ( $ref_orderid=$functionk->reviewByLastOrderRef($client_id,$this->data['merchant-id'])){
		    	    	$params['order_id']=$ref_orderid;
		    	    }
		    	}
		    	
		    	/*dump($params);		    	
		    	die();*/
		    	
		    	if ( $DbExt->insertData("{{review}}",$params)){
		    		$this->code=1;
		    		$this->msg=Yii::t("default","Your review has been published.");
		    		
		    		if (isset($this->data['initial_review_rating'])){
			    		Yii::app()->functions->updateRatings($this->data['merchant-id'],
			    		$this->data['initial_review_rating'],$client_id
			    		);
		    		}
		    		
		    				    		
		    	} else $this->msg=Yii::t("default","ERROR: cannot insert records.");
	    	} else $this->msg=Yii::t("default","Sorry but you need to login to write a review. You will be directed to page login.");
	    }
	    
	    public function loadReviews()
	    {	    		    		    		    		    	
	    	$client_id=Yii::app()->functions->getClientId();
	    	if ( $res=Yii::app()->functions->getReviewsList($this->data['merchant_id']) ){	    		
	    		ob_start();
	    		foreach ($res as $val) {	    		
	    		$pretyy_date=PrettyDateTime::parse(new DateTime($val['date_created']));
	    		$pretyy_date=Yii::app()->functions->translateDate($pretyy_date);
	    		?>
				<div class=" box-grey rounded5 margin-vert-10">
	    		<div  id="#review-<?php echo $val['id']?>" class="row row-review">
	    		   <div class="col-md-2 col-xs-2 border center into-row"
						style="width: 95px;padding: 0 0px 0 15px;"
				   >
	    		   
	    		     <!--<i class="ion-android-contact"></i>-->
	    		     <img src="<?php echo FunctionsV3::getAvatar($val['client_id']);?>" class="img-avatar-round">
	    		     <?php echo $pretyy_date;?>
	    		     
	    		   </div> <!--col-->
	    		   <div class="col-md-7 col-xs-7 border into-row" style="padding-left:8px">
						<b style="text-transform:capitalize"><?php echo $val['client_name']?></b>
						<div class="rating-stars" data-score="<?php echo $val['rating']?>"></div>
						<p class="read-more"><?php echo nl2br($val['review'])?></p>
	    		   </div> <!--col-->
	    		   <div class="col-md-3 center col-xs-3 border into-row">
	    		   <?php if ( $val['client_id']==$client_id ):?>
	    		    <a href="javascript:;" data-id="<?php echo $val['id']?>" class="delete-review btn btn-default pull-right margin-horiz-10 inline">
	    		    <?php echo t("Delete")?>
	    		    </a>
	    		    <a href="javascript:;" data-id="<?php echo $val['id']?>" class="edit-review btn btn-default pull-right margin-horiz-10 inline">
	    		    <?php echo t("Edit")?>
	    		    </a>

	    		   <?php endif;?>
	    		   </div> <!--col-->

	    		</div><!-- row-->
				</div>
	    		<?php
	    		}
	    		$html = ob_get_contents();
                ob_end_clean();   
                
                $this->code=1;
	            $this->msg="OK";	    		
	             $this->details=$html;
	    	} else $this->msg=Yii::t("default","No reviews yet.");	
	    }
	    

	    public function editReview()
	    {
	    	require_once 'edit-review.php';
	    	die();
	    }	
	    
	    public function updateReview()
	    {
	    	$DbExt=new DbExt;	    	
	    	if (!isset($this->data['review_content'])){	 
	    		$this->msg=Yii::t("default","Review content is required.");
	    		return ;
	    	}
	    	if (empty($this->data['review_content'])){	 
	    		$this->msg=Yii::t("default","Review content is required.");
	    		return ;
	    	}
	    	if (!isset($this->data['id'])){	 
	    		$this->msg=Yii::t("default","Review ID is missig");
	    		return ;
	    	}
	    	if ( $this->data['web_session_id']!=session_id()){	 
	    		$this->msg=Yii::t("default","Sorry but you cannot post directly to this action");
	    		return ;
	    	}	    		    	
	    	$params=array('review'=>$this->data['review_content'],
	    	 'date_created'=>date('c'),
	    	 'ip_address'=>$_SERVER['REMOTE_ADDR']
	    	);	    	
	    	if ( $DbExt->updateData("{{review}}",$params,'id',$this->data['id'])){
	    		$this->code=1;
	    		$this->msg=Yii::t("default","Your review has been updated.");
	    	} else $this->msg=Yii::t("default","ERROR: cannot update reviews.");	   
	    }	
	    
	    public function deleteReview()
	    {	    	
	    	$DbExt=new DbExt;
	    	$stmt="DELETE FROM
	    	{{review}}
	    	WHERE
	    	id='".$this->data['id']."'
	    	";
	    	if ( $DbExt->qry($stmt)){
	    		$this->code=1;
	    		$this->msg=Yii::t("default","Succssful");
	    	} else $this->msg=Yii::t("default","ERROR: Failed deleting reviews.");
	    }
	    
	    public function viewReceipt()
	    {
	    	// if (isset($this->data['backend'])){
	    		// $params=array(
	    		  // 'viewed'=>2
	    		// );
	    		// $this->updateData("{{order}}",$params,'order_id',$this->data['id']);
	    	// }	    
	    	require_once 'view-receipt.php';
	    	die();
	    }	
	    
	    public function addToOrder()
	    {	    	
	    	if (isset($this->data['order_id'])){
	    		if ( $res=Yii::app()->functions->getOrder($this->data['order_id'])){	    			
	    			$json_details=!empty($res['json_details'])?json_decode($res['json_details'],true):false;
	    			if ( $json_details!=false){	    				
	    				$json_details=(array)$json_details;
	    			}	    			    		
	    			$_SESSION['kr_merchant_slug']=$res['merchant_slug'];
	    			$_SESSION['kr_merchant_id']=$res['merchant_id'];
	    			$_SESSION['kr_item']=$json_details;
	    			$this->code=1;
	    			$this->msg="ok";
	    			$this->details=array(
	    			  'merchant_slug'=>$res['merchant_slug'],
	    			  'merchant_id'=>$res['merchant_id'],
	    			);
	    		}	    	
	    	} else $this->msg=Yii::t("default","Missing Order id");
	    }	
	    
	    public function removeLogo()
	    {	    	
	    	if (Yii::app()->functions->isMerchantLogin()){
	    		$DbExt=new DbExt;
		    	$merchant_id=Yii::app()->functions->getMerchantID();	    
		    	$stmt="Delete  FROM
		    	{{option}}
		    	WHERE
		    	option_name='merchant_photo'
		    	AND
		    	merchant_id='$merchant_id'
		    	";
		    	$DbExt->qry($stmt);
		    	$this->code=1;
		    	$this->msg=Yii::t("default","Successful");
	    	} else $this->msg=Yii::t("default","ERROR: Your session has expired.");
	    }	
	    
	    public function salesReport()
	    {	    		  
	    	$and='';  
	    	if (isset($this->data['start_date']) && isset($this->data['end_date']))	{
	    		if (!empty($this->data['start_date']) && !empty($this->data['end_date'])){
	    		$and=" AND date_created BETWEEN  '".$this->data['start_date']." 00:00:00' AND 
	    		        '".$this->data['end_date']." 23:59:00'
	    		 ";
	    		}
	    	}
	    	
	    	$order_status_id='';
	    	$or='';
	    	if (isset($this->data['stats_id'])){
		    	if (is_array($this->data['stats_id']) && count($this->data['stats_id'])>=1){
		    		foreach ($this->data['stats_id'] as $stats_id) {		    			
		    			$order_status_id.="'$stats_id',";
		    		}
		    		if ( !empty($order_status_id)){
		    			$order_status_id=substr($order_status_id,0,-1);
		    		}		    	
		    	}	    
	    	}
	    	
	    	if ( !empty($order_status_id)){	    		
	    		$and.= " AND status IN ($order_status_id)";
	    	}	    	    	
	    	 
	    	//dump($and);	    	
	    	
	    	$DbExt=new DbExt;
	    	$merchant_id=Yii::app()->functions->getMerchantID();	    
	    	$stmt="SELECT a.*,b.earning,b.part_status,
	    	(
	    	select concat(first_name,' ',last_name)
	    	from
	    	{{client}}
	    	where
	    	client_id=a.client_id
	    	) as client_name,
	    	
	    	(
	    	select group_concat(item_name)
	    	from
	    	{{order_details}}
	    	where
			merchant_id='$merchant_id' AND
	    	order_id=a.order_id
	    	) as item
	    	
	    	FROM
	    	{{order}} a
			JOIN {{order_merchant}} b
			ON b.order_id=a.order_id
	    	WHERE
	    	b.merchant_id='$merchant_id'
	    	AND status NOT IN ('".initialStatus()."')
	    	$and
	    	ORDER BY order_id DESC
	    	LIMIT 0,2000
	    	";
	    	/*dump($this->data);
	    	dump($stmt);*/
	    	
	    	$_SESSION['kr_export_stmt']=$stmt;	    	
	    		    		    	
	    	if ( $res=$DbExt->rst($stmt)){
	    		foreach ($res as $val) {	    			    			
	    			$action="<a data-id=\"".$val['order_id']."\" class=\"edit-order\" href=\"javascript:\">".Yii::t("default","Edit")."</a>";
	    			$action.="<a data-mtid=\"".$merchant_id."\" data-id=\"".$val['order_id']."\" class=\"view-receipt-merchant\" href=\"javascript:\">".Yii::t("default","View")."</a>";
	    			
	    			// $action.="<a data-id=\"".$val['order_id']."\" class=\"view-order-history\" href=\"javascript:\">".Yii::t("default","History")."</a>";
	    			
	    			/*$date=prettyDate($val['date_created'],true);
	    			$date=Yii::app()->functions->translateDate($date);*/
	    			$date=FormatDateTime($val['date_created']);
	    			
	    			$feed_data['aaData'][]=array(
	    			  $val['order_id'],
	    			  ucwords($val['client_name']),
	    			  $val['item'],
	    			  ucwords(Yii::t("default",$val['trans_type'])),
	    			  strtoupper(Yii::t("default",$val['payment_type'])),
	    			  prettyFormat($val['earning'],$merchant_id),
	    			  ucwords($val['status']),
					  ucwords($val['part_status']),
	    			  $date,
	    			  $action
	    		    );
	    		}
	    		$this->otableOutput($feed_data);
	    	}	   
	    	$this->otableNodata();
	    }
	    
	    public function editOrder()
	    {	    		    	
	    	?>
	    	<div class="view-receipt-pop">
	    	 <h3><?php echo Yii::t("default",'Change Order Status')?></h3>
	    	 
	    	 <?php if ( $res=Yii::app()->functions->getOrderInfo($this->data['id']) ):
					$mtid = Yii::app()->functions->getMerchantID();
					$ord_merchant = query("SELECT * FROM {{order_merchant}} WHERE merchant_id=? AND order_id=?",array($mtid,$this->data['id']));
					$ord_merchant = $ord_merchant[0];
					
					if (isset($this->data['id'])){
					   queryNoFetch("UPDATE {{order_merchant}} SET viewed=2 WHERE order_id=? AND merchant_id=?",array($this->data['id'],$mtid));
					}
					
			 ?>
	    	    <form id="frm-pop" class="frm-pop uk-form uk-form-horizontal" method="POST" onsubmit="return false;">
	    	    <?php echo CHtml::hiddenField('action','updateOrder2')?>
	    	    <?php echo CHtml::hiddenField('order_id',$this->data['id']);
					//jika yang melakukan perubahan order adalah admin
					if(Yii::app()->functions->isAdminLogin() && isset($this->data['mtid'])){
					  echo CHtml::hiddenField('mtid',$this->data['mtid']);
					}
				?>
	    	    
		    	 <div class="uk-form-row">
		    	  <label class="uk-form-label"><?php echo Yii::t("default",'Status')?></label>
		    	  <?php echo CHtml::dropDownList('status',$ord_merchant['part_status'],(array)orderStatusMerchant(),array(
		    	  'class'=>"uk-form-width-large"
		    	  ))?>
		    	 </div>
		    	 
		    	 
		    	 <div class="uk-form-row">
		    	  <label class="uk-form-label"><?php echo Yii::t("default",'Remarks')?></label>
		    	  <?php 
		    	  echo CHtml::textArea('remarks','',array(
		    	    'class'=>"uk-form-width-large"
		    	  ));
		    	  ?>		    	  
		    	 </div>
		    	 
		    	 <!--Driver-->
		    	 <?php 
		    	 /*Yii::app()->setImport(array(			
				    'application.modules.driver.components.*',
			     ));
			     Driver::AdminStatusTpl();*/
		    	 ?>		    	 
		    	 <!--Driver-->
		    	 
		    	 
		    	 <div class="action-wrap">
		    	   <?php echo CHtml::submitButton('Submit',		    	  
		    	   array('value'=>Yii::t("default",'Submit'),'class'=>"uk-button uk-form-width-medium uk-button-success"))?>
		    	 </div>
	    	   </form> 
	    	 <?php else:?>
	    	 <p class="uk-text-danger"><?php echo Yii::t("default","Error: Order not found")?></p>
	    	 <?php endif;?>
	    	</div> <!--view-receipt-pop-->	    					    
			<script type="text/javascript">
			$.validate({ 	
			    form : '#frm-pop',    
			    onError : function() {      
			    },
			    onSuccess : function() {     
			      form_submit('frm-pop');
			      return false;
			    }  
			});		
			</script>
	    	<?php
	    	die();
	    }
	    
	    public function updateOrder()
	    {	    	

	    	$DbExt=new DbExt;
	    	$merchant_id=Yii::app()->functions->getMerchantID();	    	    	

	    	$mt_timezone=Yii::app()->functions->getOption("merchant_timezone",$merchant_id);
    	    if (!empty($mt_timezone)){
    		   Yii::app()->timeZone=$mt_timezone;
    	    }
    	    
    	    /*dump($this->data);
    	    die();*/
    	    /** check if merchant has initiate widthrawals*/
    	    // if ( Yii::app()->functions->isMerchantCommission($merchant_id)){    	    	
    	    	// if ( FunctionsK::validateChangeOrder($this->data['order_id'])){
    	    		// $this->msg=t("Sorry but you cannot change the order status of this order it has reference already on the withdrawals that you made");
    	    		// return;
    	    	// }    	    
    	    // }	        	    

    	    $date_now=date('Y-m-d');

	    	if (isset($this->data['order_id'])){
	    		$order_id=$this->data['order_id'];	    		
	    		if ( $resp=Yii::app()->functions->verifyOrderIdByOwner($order_id,$merchant_id) ){
	    			$params=array('status'=>$this->data['status'],'date_modified'=>date('c'),'viewed'=>2);		
	    			
	    			/*check if merchant can change the status*/
	    			$can_edit=Yii::app()->functions->getOptionAdmin('merchant_days_can_edit_status');
	    			
	    			if (is_numeric($can_edit) && !empty($can_edit)){
	    				
	    				//$date_now=date('Y-m-d g:i:s a');
	    				//$date_now=date('Y-m-d');
	    				
	    				$base_option=getOptionA('merchant_days_can_edit_status_basedon');	    				
	    				
	    				if ( $base_option==2){	    					
	    					$date_created=date("Y-m-d",
	    					strtotime($resp['delivery_date']." ".$resp['delivery_time']));		
	    				} else $date_created=date("Y-m-d",strtotime($resp['date_created']));	    						    			
	    				/*dump($date_created);			    			
	    				dump($date_now);*/
	    					    				
	    				/*$date1=date_create($date_created);
	    				$date2=date_create($date_now);
	    				$interval = date_diff($date1,$date2);
	    				dump($interval);	    				
	    				if (is_object($interval)){
	    					echo $interval->d;
	    				}*/
	    					    				
		    			$date_interval=Yii::app()->functions->dateDifference($date_created,$date_now);
		    			if (is_array($date_interval) && count($date_interval)>=1){		    				
		    				if ( $date_interval['days']>$can_edit){
		    					$this->msg=t("Sorry but you cannot change the order status anymore. Order is lock by the website admin");
		    					$this->details=json_encode($date_interval);
		    					return ;
		    				}		    			
		    			}	    		
		    			
		    			
	    			}
	    			
	    			$mechant_sms_enabled= Yii::app()->functions->getOptionAdmin("mechant_sms_enabled");
	    			
	    			/*check if order has past for 2 days*/
	    			$this->details['order_id']=$order_id;
	    			$this->details['show_sms']=2;
	    			$date_created=date("Y-m-d g:i:s a",strtotime($resp['date_created']));
	    			$date_interval=Yii::app()->functions->dateDifference($date_created,$date_now);	    			
	    			if (is_array($date_interval) && count($date_interval)>=1){	    				
	    				if ( $date_interval['days']>2){
	    					$this->details['show_sms']=1;
	    				}
	    			}
	    			
	    			if ( $mechant_sms_enabled=="yes"){
	    				$this->details['show_sms']=1;
	    			}	    		
	    			
	    			/**check if admin has disabled the sending of sms*/
	    			if (getOptionA('merchant_changeorder_sms')==2){
	    				$this->details['show_sms']=1;
	    			}
	    				    			
	    			if ($DbExt->updateData('{{order}}',$params,'order_id',$order_id)){
	    				$this->code=1;
	    				$this->msg=Yii::t("default","Status saved.");
	    				
	    				/*Now we insert the order history*/	    		
	    				$params_history=array(
	    				  'order_id'=>$order_id,
	    				  'status'=>$this->data['status'],
	    				  'remarks'=>isset($this->data['remarks'])?$this->data['remarks']:'',
	    				  'date_created'=>date('c'),
	    				  'ip_address'=>$_SERVER['REMOTE_ADDR']
	    				);	    				
	    				$DbExt->insertData("{{order_history}}",$params_history);

	    				    				
	    				if (FunctionsV3::hasModuleAddon("mobileapp")){
		    				/** Mobile save logs for push notification */
					    	Yii::app()->setImport(array(			
							  'application.modules.mobileapp.components.*',
						    ));
					    	AddonMobileApp::savedOrderPushNotification($this->data);
	    				}
				    	
	    				if (FunctionsV3::hasModuleAddon("driver")){
					    	/*Driver app*/
					    	Yii::app()->setImport(array(			
							  'application.modules.driver.components.*',
						    ));
						    Driver::addToTask($order_id);
	    				}
				    	
				    	
	    			} else $this->msg=Yii::t("default","ERROR: cannot update order.");
	    		} else $this->msg=Yii::t("default","This Order does not belong to you.");
	    	} else $this->msg=Yii::t("default","Missing parameters");	    
	    }


	    public function updateOrder2()
	    {
		
    	$Validator=new Validator;
		$req=array(
		  // 'token'=>$this->t("token is required"),
		  // 'mtid'=>$this->t("merchant id is required"),
		  // 'user_type'=>$this->t("user type is required"),
		  'order_id'=>t("order id is required"),
		  'status'=>t("order status is required")
		);
		$mtid = Yii::app()->functions->getMerchantID();
		//jika yang melakukan update order adalah admin
		if(Yii::app()->functions->isAdminLogin() && isset($this->data['mtid'])){
			$mtid = $this->data['mtid'];
		}
		
		$Validator->required($req,$this->data);
		if ($Validator->validate()){
			$DbExt=new DbExt;
			// if ( true || $res=merchantApp::validateToken($this->data['mtid'],
			    // $this->data['token'],$this->data['user_type'])){
			if ( $res= query("SELECT * FROM {{merchant}} WHERE merchant_id=?",array($mtid))){
				$res=$res[0];
			    $merchant_id=$mtid;
			    $order_id=$this->data['order_id'];

    	            	        
    	        /*check if merchant can change the status*/
	    	    $can_edit=Yii::app()->functions->getOptionAdmin('merchant_days_can_edit_status');	    	    
	    	    if (is_numeric($can_edit) && !empty($can_edit)){
	    	    	
		    	    $date_now=date('Y-m-d');
		    	    $base_option=getOptionA('merchant_days_can_edit_status_basedon');	
		    	    
		    	    $resp=Yii::app()->functions->getOrderInfo($order_id);
		    	    
		    	    if ( $base_option==2){	    					
						$date_created=date("Y-m-d",
						strtotime($resp['delivery_date']." ".$resp['delivery_time']));		
					} else $date_created=date("Y-m-d",strtotime($resp['date_created']));

					$date_interval=Yii::app()->functions->dateDifference($date_created,$date_now);					
	    			if (is_array($date_interval) && count($date_interval)>=1){		    				
	    				if ( $date_interval['days']>$can_edit){
	    					$this->msg=t("Sorry but you cannot change the order status anymore. Order is lock by the website admin");
	    					$this->details=json_encode($date_interval);
	    					$this->output();
	    				}		    			
	    			}	    		
	    	    }			   
			    
			    //hanya admin dan merchant bersangkutan yang dapat merubah status order
			    if ( Yii::app()->functions->isAdminLogin() || $resp=Yii::app()->functions->verifyOrderIdByOwner($order_id,$merchant_id) ){
				
					$order_status=$this->data['status'];
					$order_status2 = str_replace('_merchant','',$order_status);
					//ubah status order secara keseluruhan
					
					//ACCEPT PARTIAL part_status 
					//semua merchant mesti accepted agar overal_statusnya accepted
					
					queryNoFetch("UPDATE {{order_merchant}} SET part_status=? WHERE order_id=? AND merchant_id=?",array($order_status2,$order_id,$merchant_id));

					// $r = query("SELECT * FROM {{order_merchant}} WHERE (part_status='' OR part_status='declined') AND order_id=?",array($order_id));
					$record_history = false;
					
					//ambil status saat ini baik status2 masing-masing merchant (part_status) juga status keseluruhan overall status
					$r2 = query("SELECT * FROM {{order_merchant}} WHERE order_id=?",array($order_id));
					$order = query("SELECT * FROM {{order}} WHERE order_id=?",array($order_id));
					$order = $order[0];
					$overall_status = $order['status'];
					
					$list_part_status = array();
					$all_merchant_accepted = true;
					foreach($r2 as $p ) {
						$list_part_status[] = $p['part_status'];
						if(($p['part_status'] !== 'accepted')) {
							$all_merchant_accepted = false;
						}
					}
					
					//jika didecline 
					if($order_status2 == 'declined' &&  ($overall_status == 'pending' || $overall_status == 'initial_order')) {
						//jika ordernya credit kemudian decline maka saldo mesti dikembalikan ke user
						//tetapi overall status mesti pending karena user berpotensi melakukan decline berkali2
						if($order['payment_type'] == 'credit') {
							$client = query("SELECT * FROM {{client}} WHERE client_id=?",array($order['client_id'])); $client=$client[0];
							$new_saldo = $client['saldo']+$order['total_w_tax'];
							queryNoFetch("UPDATE {{client}} SET saldo=? WHERE client_id=?",array($new_saldo,$order['client_id']));
						}
						$params=array(
						  'status'=>$order_status2,
						  'date_modified'=>date('c'),
						);
						$DbExt=new DbExt;
						if ($DbExt->updateData('{{order}}',$params,'order_id',$order_id)){
							$this->code=1;
							$this->msg=t("order status successfully changed");
						}
						$record_history = true;
						
					} elseif($order_status2 == 'accepted'  && $overall_status == 'declined') {
						//order sudah decline tapi user mengaccept (tidak bisa)
						$this->msg=t("Order has been declined.");
						$this->details=array(
							 'order_id'=>$order_id
						);
						
					} elseif($order_status2 == 'accepted'  && ($all_merchant_accepted == false) && $overall_status !== 'declined') {
						//order sudah accept tapi masih ada merchant yang belum approve
						$this->code=1;
						$this->msg=t("Order ID").":$order_id ".t("has been accepted. Wait other merchant to accept this order");
						$this->details=array(
							 'order_id'=>$order_id
						);
						$record_history = true;
					} elseif($all_merchant_accepted &&  ($overall_status == 'pending' || $overall_status == 'initial_order' || $overall_status == 'accepted')) {
						//jika semua merchant accepted dan status overallnya pending
						
						//jika masih ada part_status yang kosong artinya belum semua merchant yang approve
						// jika part status sudah tidak ada yang kosong (semua merchant melakukan approval)
						// if(!$r) {
						$params=array(
						  'status'=>$order_status2,
						  'date_modified'=>date('c'),
						);
						
						
						if ($DbExt->updateData('{{order}}',$params,'order_id',$order_id)){
							$this->code=1;
							$this->msg=t("order status successfully changed");
							
							if(merchantApp::hasModuleAddon("mobileapp")){	    				   
							   $push_log='';
							   $push_log['order_id']=$order_id;
							   $push_log['status']=$order_status;
							   $push_log['remarks']=$this->data['remarks'];

							   Yii::app()->setImport(array(
							   'application.modules.mobileapp.components.*',
							   ));        
							   AddonMobileApp::savedOrderPushNotification($push_log);
							}
							
							if (FunctionsV3::hasModuleAddon("driver")){
								
								/*Driver app*/
								Yii::app()->setImport(array(
								  'application.modules.driver.components.*',
								));
								Driver::addToTask($order_id);
							}

							$this->code=1;
								$this->msg=t("Order ID").":$order_id ".t("has been accepted.");
								$this->details=array(
								 'order_id'=>$order_id
							);
							$record_history = true;
						} else $this->msg=t("ERROR: cannot update order.");
    	        	}

					if($record_history) {
						/*Now we insert the order history*/ 		
						$params_history=array(
						  'order_id'=>$order_id,
						  'status'=>$order_status2,
						  'remarks'=>isset($this->data['remarks'])?$this->data['remarks']:'',
						  'date_created'=>date('c'),
						  'ip_address'=>$_SERVER['REMOTE_ADDR']
						);
						$DbExt->insertData("{{order_history}}",$params_history);
					} else {
						$this->msg=t("Order is not updated in this state");
					}
			    } else $this->msg=t("This Order does not belong to you");			    
			} else {
				$this->code=3;
				$this->msg=$this->t("you session has expired or someone login with your account");
			}
		} else $this->msg=merchantApp::parseValidatorError($Validator->getError());	    	
		// $this->output();   		
		
	    }

	    
	     public function chartTotalSales()
	    {
	    	$merchant_id=Yii::app()->functions->getMerchantID();	    
	    	
	    	$data='';
	    	$db_ext=new DbExt();    	
	    	$date_now=date('Y-m-d 23:00:59');
	    	$start_date=date('Y-m-d 00:00:00',strtotime($date_now . "-30 days"));
	    	$stmt="SELECT DATE_FORMAT(date_created, '%M-%D') as date_created_format,SUM(total_w_tax) as total
	    	FROM
	    	{{order}}
	    	WHERE
	    	date_created BETWEEN '$start_date' AND '$date_now'
	    	AND
	    	merchant_id ='$merchant_id'
	    	AND status NOT IN ('".initialStatus()."')
	    	GROUP BY DATE_FORMAT(date_created, '%M-%D')
	    	ORDER BY date_created ASC
	    	";
	    	//dump($stmt);
	    	if ($res=$db_ext->rst($stmt)){
	    		foreach ($res as $val) {
	    			//$data[$val['date_created_format']]=prettyFormat($val['total'],$merchant_id);
	    			$t=explode("-",$val['date_created_format']);
	    			if (is_array($t) && count($t)>=1){
	    				$tt=Yii::t("default",$t[0])."-".$t[1];
	    			    $data[$tt]=unPrettyPrice($val['total']);
	    			} else $data[$val['date_created_format']]=unPrettyPrice($val['total']);
	    		}
	    	}	    	    	
	    	echo Yii::app()->functions->formatAsChart($data);
	    	die();
	    }		   

	    public function chartByItem()
	    {	    	
	    	$merchant_id=Yii::app()->functions->getMerchantID();	    
	    		    
	    	$data='';
	    	$db_ext=new DbExt();		    	
	    	$date_now=date('Y-m-d 23:00:59');
	    	$start_date=date('Y-m-d 00:00:00',strtotime($date_now . "-30 days"));
	    	$stmt="SELECT a.item_name,a.discounted_price,	    	       
	    	       SUM(qty) as total,
	    	       b.date_created
	    	       FROM
	    	       {{view_order_details}} a
	    	       left join {{order}} b
	    	       ON
	    	       a.order_id=b.order_id
	    	       WHERE
	    	       b.date_created BETWEEN '$start_date' AND '$date_now'
	    	       AND a.merchant_id ='$merchant_id'
	    	       AND a.status NOT IN ('".initialStatus()."')
	    	       GROUP BY item_id
	    	       ORDER BY item_name ASC
	    	";	    	
	    	//dump($stmt);
	    	if ($res=$db_ext->rst($stmt)){	    			    		
	    		foreach ($res as $val) {
	    			$val['item_name']=trim($val['item_name']);
	    			$data[$val['item_name']]=$val['discounted_price']*$val['total'];
	    		}
	    	}	    	    	
	    	echo Yii::app()->functions->formatAsChart($data);
	    	die();
	    }
	    
        public function recentOrder()
	    {	    		    
	    	
	    	$DbExt=new DbExt;
	    	$merchant_id=Yii::app()->functions->getMerchantID();	    
	    	$stmt="SELECT a.*,
	    	(
	    	select concat(first_name,' ',last_name)
	    	from
	    	{{client}}
	    	where
	    	client_id=a.client_id
	    	) as client_name,
	    	
	    	(
	    	select concat(contact_phone)
	    	from
	    	{{client}}
	    	where
	    	client_id=a.client_id
	    	) as contact_phone,
	    	
	    	(
	    	select group_concat(item_name)
	    	from
	    	{{order_details}}
	    	where
	    	order_id=a.order_id
	    	) as item
	    	
	    	FROM
	    	{{order}} a
	    	WHERE
	    	merchant_id='$merchant_id'	    	
	    	AND date_created like '".date('Y-m-d')."%'
	    	AND status NOT in ('".initialStatus()."')
	    	ORDER BY date_created DESC	    	
	    	";
	    	//dump($this->data);
	    	//dump($stmt);
	    	if ( $res=$DbExt->rst($stmt)){
	    		//dump($res);
	    		foreach ($res as $val) {	    			
	    			$new='';
	    			$action="<a data-id=\"".$val['order_id']."\" class=\"edit-order\" href=\"javascript:\">".Yii::t("default","Edit")."</a>";
	    			$action.="<a data-id=\"".$val['order_id']."\" class=\"view-receipt\" href=\"javascript:\">".Yii::t("default","View")."</a>";
	    			
	    			$action.="<a data-id=\"".$val['order_id']."\" class=\"view-order-history\" href=\"javascript:\">".Yii::t("default","History")."</a>";
	    			
	    			if ($val['viewed']==1){
	    				$new=" <div class=\"uk-badge\">".Yii::t("default","NEW")."</div>";
	    			}	    		
	    			
	    			/*$date=prettyDate($val['date_created'],true);
	    			$date=Yii::app()->functions->translateDate($date);*/
	    			$date=FormatDateTime($val['date_created']);
	    			
	    			$item=FunctionsV3::translateFoodItemByOrderId(
	    			  $val['order_id'],
	    			  'kr_merchant_lang_id'
	    			);
	    			
	    			$feed_data['aaData'][]=array(
	    			  $val['order_id'],
	    			  ucwords($val['client_name']).$new,
	    			  $val['contact_phone'],
	    			  $item,
	    			  ucwords(Yii::t("default",$val['trans_type'])),
	    			  strtoupper(Yii::t("default",$val['payment_type'])),
	    			  prettyFormat($val['sub_total'],$merchant_id),
	    			  prettyFormat($val['taxable_total'],$merchant_id),
	    			  prettyFormat($val['total_w_tax'],$merchant_id),
	    			  t($val['status']),
	    			  $date,
	    			  $action
	    		    );
	    		}
	    		$this->otableOutput($feed_data);
	    	}	   
	    	$this->otableNodata();
	    }		    
	    
	    
	    public function export()
	    {	    	    	
	    	$merchant_id=Yii::app()->functions->getMerchantID();
	    	$db_ext=new DbExt();
	    	if (!empty($_SESSION['kr_export_stmt'])){
	    		$stmt= $_SESSION['kr_export_stmt'];
	    		$pos=strpos($stmt,"LIMIT");
	    		$stmt=substr($stmt,0,$pos);	 	    		
	    		switch ($this->data['rpt']) {
	    			case 'sales-report':
	    				if ($res=$db_ext->rst($stmt)){		    					
	    					$csvdata=array();
    	    	            $datas=array();  
    	    	            foreach ($res as $val) {	    			    			
				    			$item='';				    			
				    			$date=date('m-d-Y G:i:s',strtotime($val['date_created']));    	    		
			    	    		$latestdata[]=array(    	    		  
			    	    		  $val['order_id'],
			    	    		  $val['client_name'],
			    	    		  $val['item'],
			    	    		  $val['trans_type'],
			    	    		  $val['payment_type'],
			    	    		  prettyFormat($val['sub_total'],$merchant_id),
			    	    		  prettyFormat($val['tax'],$merchant_id),
			    	    		  prettyFormat($val['total_w_tax'],$merchant_id),
			    	    		  $val['status'],
			    	    		  $date
			    	    		);    	    		
				    		}				    		 	
				    		unset($data);
    	                    $data=$latestdata;    	                    
    	                    
	    				}	
	    				
	    				if (is_array($data) && count($data)>=1){
			    	    	$csvdata=array();
			    	    	$datas=array();
			    	        foreach ($data as $val) {    	        	
			    	            foreach ($val as $key => $vals) {
			    	            	$datas[]=$vals;
			    	            }
			    	            $csvdata[]=$datas;
			    	            unset($datas);
			    	        }	
			    	    }    	    
	    				
			    	    $header=array(
			    	    Yii::t("default","Ref#"),
			    	    Yii::t("default","Client Name"),
			    	    Yii::t("default","Item"),
			    	    Yii::t("default","Trans Type"),
			    	    Yii::t("default","Payment Type"),
			    	    Yii::t("default","Total"),
			    	    Yii::t("default","Tax"),
			    	    Yii::t("default","Total W/Tax"),
			    	    Yii::t("default","Status"),
			    	    Yii::t("default","Date"));
			    	    $filename = $this->data['rpt'].'-'. date('c') .'.csv';    	    
				    	$excel  = new ExcelFormat($filename);
				    	$excel->addHeaders($header);
                        $excel->setData($csvdata);	  
                        $excel->prepareExcel();	 
                        exit;
	    				break;
	    		
	    			case "sales-summary-report":	
	    			   $has_date_range=false;
                       if (isset($_SESSION['rpt_date_range'])){
	    			   	   if (is_array($_SESSION['rpt_date_range'])){
	    			   	   	   $has_date_range=true;
	    			   	   }
	    			   }
	    			   
	    			    if ($res=$db_ext->rst($stmt)){	    		
			    		    foreach ($res as $val) {	
			    			    $total_amt='';
			    			    if ( $has_date_range==true){
			    			    	$data[]=array(
				    			       $val['item_id'],
				    			       $val['item_name'],
				    			       $val['size'],
				    			       prettyFormat($val['discounted_price'],$merchant_id),
				    			       prettyFormat($val['total_qty'],$merchant_id),
				    			       prettyFormat($val['total_qty']*$val['discounted_price'],$merchant_id),
				    			       $_SESSION['rpt_date_range']['start_date'],
							    	   $_SESSION['rpt_date_range']['end_date'],
				    			    );
			    			    } else {			    			    
				    			    $data[]=array(
				    			       $val['item_id'],
				    			       $val['item_name'],
				    			       $val['size'],
				    			       prettyFormat($val['discounted_price'],$merchant_id),
				    			       prettyFormat($val['total_qty'],$merchant_id),
				    			       prettyFormat($val['total_qty']*$val['discounted_price'],$merchant_id)
				    			    );
			    			    }
				    		}			    		
			    	    }			  			    	    			    	  
			    	    $header=array(
			    	    Yii::t("default","Item"),
			    	    Yii::t("default","Item Name"),
			    	    Yii::t("default","Size"),
			    	    Yii::t("default","Item Price"),
			    	    Yii::t("default","Total Qty"),
			    	    Yii::t("default","Total Amount")
			    	    );		
			    	    if ( $has_date_range==true) {
    			   	   	   $header[]=t("Start Date");
    			   	   	   $header[]=t("End Date");
	    			    }			    	    
			    	    $filename = $this->data['rpt'].'-'. date('c') .'.csv';    	    
				    	$excel  = new ExcelFormat($filename);
				    	$excel->addHeaders($header);
                        $excel->setData($data);	  
                        $excel->prepareExcel();	
                        exit; 
	    			    break;
	    			    
	    			case "rptSalesMerchant":   	    			   
	    			   if ($res=$db_ext->rst($stmt)){	    		
			    		    foreach ($res as $val) {				    			    
			    			    $data[]=array(
			    			       $val['merchant_id'],
			    			       $val['merchant_name'],
	    			               $val['contact_name'],
	    			               $val['contact_phone']." / ".$val['contact_email'],
	    			               $val['street']." ".$val['city']." ".$val['state']." ".$val['country_code']." ".$val['post_code'],
	    			               ucwords($val['package_name']),
	    			               $val['status'],
			    			       Yii::app()->functions->prettyDate($val['date_created'],true)
			    			    );
				    		}			    		
			    	    }					    	    
			    	    $header=array(
			    	    Yii::t("default","MerchantID"),
			    	    Yii::t("default","MerchantName"),
			    	    Yii::t("default","ContactName"),
			    	    Yii::t("default","Contact"),
			    	    Yii::t("default","Address"),
			    	    Yii::t("default","Package"),
			    	    Yii::t("default","Status"),
			    	    Yii::t("default","Date")
			    	    );			    	  
			    	    $filename = $this->data['rpt'].'-'. date('c') .'.csv';    	    
				    	$excel  = new ExcelFormat($filename);
				    	$excel->addHeaders($header);
                        $excel->setData($data);	  
                        $excel->prepareExcel();	
                        exit; 
	    			    break;
	    			break;
	    			
	    			case 'rptAdminSalesMerchant':
	    			if ($res=$db_ext->rst($stmt)){	    		
			    		    foreach ($res as $val) {
			    		    	$date=prettyDate($val['date_created'],true);
	    			            $date=Yii::app()->functions->translateDate($date);
			    			    $data[]=array(
			    			        $val['order_id'],
					    			  ucwords($val['client_name']),
					    			  $val['item'],
					    			  ucwords(Yii::t("default",$val['trans_type'])),
					    			  strtoupper(Yii::t("default",$val['payment_type'])),
					    			  standardPrettyFormat($val['sub_total'],$merchant_id),
					    			  standardPrettyFormat($val['tax'],$merchant_id),
					    			  standardPrettyFormat($val['total_w_tax'],$merchant_id),
					    			  ucwords($val['status']),
					    			  $date,
			    			    );
				    		}			    		
			    	    }							    	    
			    	    $header=array(
			    	    Yii::t("default","Ref#"),
			    	    Yii::t("default","Name"),
			    	    Yii::t("default","Item"),
			    	    Yii::t("default","TransType"),
			    	    Yii::t("default","Payment Type"),
			    	    Yii::t("default","Total"),			    	    
			    	    Yii::t("default","Tax"),
			    	    Yii::t("default","Total W/Tax"),
			    	    Yii::t("default","Status"),
			    	    Yii::t("default","Date")
			    	    );			    	  
			    	    $filename = $this->data['rpt'].'-'. date('c') .'.csv';    	    
				    	$excel  = new ExcelFormat($filename);
				    	$excel->addHeaders($header);
                        $excel->setData($data);	  
                        $excel->prepareExcel();	
                        exit; 
                        break;

	    			case "rptCustomerList":	 			
	    				if ($res=$db_ext->rst($_SESSION['kr_export_stmt'])){	
	    					foreach ($res as $val) {
	    						$data[]=array(
	    						   $val['email_address'],
	    						   $val['first_name'],
	    						   $val['last_name']
	    						);
	    					}	    					
	    				}		    				
    					$header=array(
    					 t("Email"),
    					 t("firstname"),
    					 t("lastname"),
    					);
	    				$filename = $this->data['rpt'].'-'. date('c') .'.csv';    	    
				    	$excel  = new ExcelFormat($filename);
				    	$excel->addHeaders($header);
                        $excel->setData($data);	  
                        $excel->prepareExcel();	
                        exit; 
	    				break;
	    				
	    			   

	    			   	    	

                  
                      
                    case "rptmerchantsalesummary": 	
                        
                       $has_date_range=false;
                       if (isset($_SESSION['rpt_date_range'])){
	    			   	   if (is_array($_SESSION['rpt_date_range'])){
	    			   	   	   $has_date_range=true;
	    			   	   }
	    			   }
                       if ($res=$db_ext->rst($_SESSION['kr_export_stmt'])){
                    		 foreach ($res as $val) {			    		    	
                    		 	if ($has_date_range==true){
                    		 		$data[]=array(
							    	   $val['merchant_name'],
							    	   normalPrettyPrice($val['total_sales']+0),
							    	   normalPrettyPrice($val['total_commission']+0),
							    	   normalPrettyPrice($val['total_earnings']+0),
							    	   $_SESSION['rpt_date_range']['start_date'],
							    	   $_SESSION['rpt_date_range']['end_date'],
				    			    );
                    		 	} else {
				    			    $data[]=array(
							    	   $val['merchant_name'],
							    	   normalPrettyPrice($val['total_sales']+0),
							    	   normalPrettyPrice($val['total_commission']+0),
							    	   normalPrettyPrice($val['total_earnings']+0),						    	   
				    			    );
                    		 	}
				    		}			 
                    	}                    	
                       $header=array(
	    			    t("Merchant Name"),
	    			    t("Total Sales"),
	    			    t("Total Commission"),
	    			    t("Merchant Earnings"),
	    			    //t("Approved No. Of Guests")
	    			   );
	    			   if ( $has_date_range==true) {
    			   	   	   $header[]=t("Start Date");
    			   	   	   $header[]=t("End Date");
	    			   }
	    			   	    			   
	    			    $filename = $this->data['rpt'].'-'. date('c') .'.csv';    	    
				    	$excel  = new ExcelFormat($filename);
				    	$excel->addHeaders($header);
                        $excel->setData($data);	  
                        $excel->prepareExcel();	
                    	
                        exit;                      
                    	break; 
                    	

                    	

                    	
                    case "rpt_incomingwithdrawal":
                    	
                    	 if ($res=$db_ext->rst($_SESSION['kr_export_stmt'])){
                    		 foreach ($res as $val) {	
                    		 	   $date=date('M d,Y G:i:s',strtotime($val['date_created']));  
 	    		                   $date=Yii::app()->functions->translateDate($date);
	    		
	    		                   $date_created=displayDate($val['date_created']);
	    		                   $date_to_process=displayDate($val['date_to_process']);
	    				    		    	
	    		                   $method=t("Paypal to")." ".$val['account'];
	    		                   if ( $val['payment_method']=="bank"){
	    			                    $method=t("Bank to")." ".$val['bank_account_number'];
	    		                   }
	    		                   	    		
                    		       $data[]=array(
								      $val['withdrawal_id'],
						    		  $val['merchant_name'],
						    		  $method,
						    		  normalPrettyPrice($val['amount']),
						    		  normalPrettyPrice($val['current_balance']),
						    		  $val['status'],
						    		  $date_created,
						    		  $date_to_process,	    		
				    			    );
				    		}			 
                    	}                    	
                    	$header=array(
	    			    t("ID"),
	    			    t("Merchant Name"),
	    			    t("Payment Method"),
	    			    t("Amount"),	    			    
	    			    t("From Balance"),
	    			    t("Status"),
	    			    t("Date Of Request"),
	    			    t("Date to process")
	    			   );
	    			 
	    			   
	    			    $filename = $this->data['rpt'].'-'. date('c') .'.csv';    	    
				    	$excel  = new ExcelFormat($filename);
				    	$excel->addHeaders($header);
                        $excel->setData($data);	  
                        $excel->prepareExcel();	                    	
                        exit;                  
                    	break; 	
	    			default:
	    				break;
	    		}
	    	} else echo Yii::t("default","Error: Something went wrong. please try again.");
	    }
	    
		
		public function adminSettings()
		{			
	    	Yii::app()->functions->updateOptionAdmin("merchants_max_distance",
	    	isset($this->data['merchants_max_distance'])?$this->data['merchants_max_distance']:'' );
			
	    	Yii::app()->functions->updateOptionAdmin("motor_max_weight",
	    	isset($this->data['motor_max_weight'])?$this->data['motor_max_weight']:'' );
			
			//shuttle motor
	    	Yii::app()->functions->updateOptionAdmin("shuttle_motor_flat_range",
	    	isset($this->data['shuttle_motor_flat_range'])?$this->data['shuttle_motor_flat_range']:'' );

	    	Yii::app()->functions->updateOptionAdmin("shuttle_motor_flat_cost",
	    	isset($this->data['shuttle_motor_flat_cost'])?$this->data['shuttle_motor_flat_cost']:'' );

	    	Yii::app()->functions->updateOptionAdmin("shuttle_motor_continue",
	    	isset($this->data['shuttle_motor_continue'])?$this->data['shuttle_motor_continue']:'' );
			//motor
	    	Yii::app()->functions->updateOptionAdmin("motor_flat_range",
	    	isset($this->data['motor_flat_range'])?$this->data['motor_flat_range']:'' );

	    	Yii::app()->functions->updateOptionAdmin("motor_flat_cost",
	    	isset($this->data['motor_flat_cost'])?$this->data['motor_flat_cost']:'' );

	    	Yii::app()->functions->updateOptionAdmin("motor_continue",
	    	isset($this->data['motor_continue'])?$this->data['motor_continue']:'' );

			
			//shuttle car
	    	Yii::app()->functions->updateOptionAdmin("shuttle_car_flat_range",
	    	isset($this->data['shuttle_car_flat_range'])?$this->data['shuttle_car_flat_range']:'' );

	    	Yii::app()->functions->updateOptionAdmin("shuttle_car_flat_cost",
	    	isset($this->data['shuttle_car_flat_cost'])?$this->data['shuttle_car_flat_cost']:'' );

	    	Yii::app()->functions->updateOptionAdmin("shuttle_car_continue",
	    	isset($this->data['shuttle_car_continue'])?$this->data['shuttle_car_continue']:'' );
			//car
	    	Yii::app()->functions->updateOptionAdmin("car_flat_range",
	    	isset($this->data['car_flat_range'])?$this->data['car_flat_range']:'' );

	    	Yii::app()->functions->updateOptionAdmin("car_flat_cost",
	    	isset($this->data['car_flat_cost'])?$this->data['car_flat_cost']:'' );

	    	Yii::app()->functions->updateOptionAdmin("car_continue",
	    	isset($this->data['car_continue'])?$this->data['car_continue']:'' );


			Yii::app()->functions->updateOptionAdmin("website_date_picker_format",
	    	isset($this->data['website_date_picker_format'])?$this->data['website_date_picker_format']:'');
	    	
			Yii::app()->functions->updateOptionAdmin("website_time_picker_format",
	    	isset($this->data['website_time_picker_format'])?$this->data['website_time_picker_format']:'');
	    	
			Yii::app()->functions->updateOptionAdmin("website_date_format",
	    	isset($this->data['website_date_format'])?$this->data['website_date_format']:'');
	    	
	    	Yii::app()->functions->updateOptionAdmin("website_time_format",
	    	isset($this->data['website_time_format'])?$this->data['website_time_format']:'');
	    	
			Yii::app()->functions->updateOptionAdmin("merchant_sigup_status",
	    	isset($this->data['merchant_sigup_status'])?$this->data['merchant_sigup_status']:'');
	    	
	    	Yii::app()->functions->updateOptionAdmin("admin_country_set",
	    	isset($this->data['admin_country_set'])?$this->data['admin_country_set']:'');
	    	
	    	Yii::app()->functions->updateOptionAdmin("admin_currency_set",
	    	isset($this->data['admin_currency_set'])?$this->data['admin_currency_set']:'');
	    		    	
	    	Yii::app()->functions->updateOptionAdmin("home_search_text",
	    	isset($this->data['home_search_text'])?$this->data['home_search_text']:'' );	    	
	    	
	    	Yii::app()->functions->updateOptionAdmin("home_search_subtext",
	    	isset($this->data['home_search_subtext'])?$this->data['home_search_subtext']:'' );	    	


	    	Yii::app()->functions->updateOptionAdmin("website_title",
	    	isset($this->data['website_title'])?$this->data['website_title']:'' );	    	
	    	
	    	Yii::app()->functions->updateOptionAdmin("website_address",
	    	isset($this->data['website_address'])?$this->data['website_address']:'' );	    	
	    	
	    	Yii::app()->functions->updateOptionAdmin("website_contact_phone",
	    	isset($this->data['website_contact_phone'])?$this->data['website_contact_phone']:'' );	    	
	    	
	    	Yii::app()->functions->updateOptionAdmin("website_contact_email",
	    	isset($this->data['website_contact_email'])?$this->data['website_contact_email']:'' );	    	
	    	
	    	Yii::app()->functions->updateOptionAdmin("admin_decimal_place",
	    	isset($this->data['admin_decimal_place'])?$this->data['admin_decimal_place']:'' );	    	
	    	
	    	Yii::app()->functions->updateOptionAdmin("admin_use_separators",
	    	isset($this->data['admin_use_separators'])?$this->data['admin_use_separators']:'' );	  
	    	
	    	Yii::app()->functions->updateOptionAdmin("admin_currency_position",
	    	isset($this->data['admin_currency_position'])?$this->data['admin_currency_position']:'' );

	    	Yii::app()->functions->updateOptionAdmin("global_admin_sender_email",
	    	isset($this->data['global_admin_sender_email'])?$this->data['global_admin_sender_email']:'' );
	    	
	    	Yii::app()->functions->updateOptionAdmin("merchant_disabled_registration",
	    	isset($this->data['merchant_disabled_registration'])?$this->data['merchant_disabled_registration']:'' );	    	

	    	Yii::app()->functions->updateOptionAdmin("website_timezone",
	    	isset($this->data['website_timezone'])?$this->data['website_timezone']:'' );
	    	
	    	Yii::app()->functions->updateOptionAdmin("website_reviews_actual_purchase",
	    	isset($this->data['website_reviews_actual_purchase'])?$this->data['website_reviews_actual_purchase']:'' );
	    	
	    	Yii::app()->functions->updateOptionAdmin("website_terms_merchant",
	    	isset($this->data['website_terms_merchant'])?$this->data['website_terms_merchant']:'' );
	    	Yii::app()->functions->updateOptionAdmin("website_terms_merchant_url",
	    	isset($this->data['website_terms_merchant_url'])?$this->data['website_terms_merchant_url']:'' );
	    	
	    	Yii::app()->functions->updateOptionAdmin("website_terms_customer",
	    	isset($this->data['website_terms_customer'])?$this->data['website_terms_customer']:'' );
	    	Yii::app()->functions->updateOptionAdmin("website_terms_customer_url",
	    	isset($this->data['website_terms_customer_url'])?$this->data['website_terms_customer_url']:'' );
	    		    	
	    	Yii::app()->functions->updateOptionAdmin("admin_thousand_separator",
	    	isset($this->data['admin_thousand_separator'])?$this->data['admin_thousand_separator']:'' );
	    	
	    	Yii::app()->functions->updateOptionAdmin("admin_decimal_separator",
	    	isset($this->data['admin_decimal_separator'])?$this->data['admin_decimal_separator']:'' );
	    	
	    	Yii::app()->functions->updateOptionAdmin("merchant_can_edit_reviews",
	    	isset($this->data['merchant_can_edit_reviews'])?$this->data['merchant_can_edit_reviews']:'' );

	    	Yii::app()->functions->updateOptionAdmin("google_geo_api_key",
	    	isset($this->data['google_geo_api_key'])?$this->data['google_geo_api_key']:'' );
	    	
	    	Yii::app()->functions->updateOptionAdmin("customer_ask_address",
	    	isset($this->data['customer_ask_address'])?$this->data['customer_ask_address']:'' );
	    	
	    	Yii::app()->functions->updateOptionAdmin("theme_enabled_email_verification",
	    	isset($this->data['theme_enabled_email_verification'])?$this->data['theme_enabled_email_verification']:'');
	    	

	    	Yii::app()->functions->updateOptionAdmin("google_use_curl",
	    	isset($this->data['google_use_curl'])?$this->data['google_use_curl']:'');
	    	

	    	$this->code=1;
	    	$this->msg=Yii::t("default","Setting saved");
		}
		
		public function merchantSignUp()
		{		    
						
            /** check if admin has enabled the google captcha*/    	    	
	    	if ( getOptionA('captcha_merchant_signup')==2){
	    		if ( GoogleCaptcha::checkCredentials()){
	    			if ( !GoogleCaptcha::validateCaptcha()){
	    				$this->msg=GoogleCaptcha::$message;
	    				return false;
	    			}	    		
	    		}	    	
	    	}
	    	
	    	if (isset($this->data['cpassword'])){
	    		if ($this->data['cpassword']!=$this->data['password']){
	    			$this->msg=t("Confirm password does not match");
	    			return ;
	    		}
	    	}
	    				
			if (Yii::app()->functions->isMerchantExist($this->data['contact_email'])){
				$this->msg=Yii::t("default","Sorry you input email address that is already registered in our records.");
				return ;
			}		
			if (!isset($this->data['package_id'])){
				$this->msg=Yii::t("default","ERROR: Missing package id");
				return ;
			}	
			
			if ( !$package=Yii::app()->functions->getPackagesById($this->data['package_id'])){
				$this->msg=Yii::t("default","ERROR: Package information not found");
				return ;
			}		
			
			
			$package_price=0;
			if ( $package['promo_price']>=1){
				$package_price=$package['promo_price'];
			} else $package_price=$package['price'];
			
			$expiration=$package['expiration'];
			$membership_expired = date('Y-m-d', strtotime ("+$expiration days"));
			
			$status=Yii::app()->functions->getOptionAdmin('merchant_sigup_status');
			$token=md5($this->data['merchant_name'].date('c'));
		    $params=array(
		      'merchant_name'=>addslashes($this->data['merchant_name']),
		      'merchant_phone'=>$this->data['merchant_phone'],
		      'contact_name'=>$this->data['contact_name'],
		      'contact_phone'=>$this->data['contact_phone'],
		      'contact_email'=>$this->data['contact_email'],
		      'street'=>$this->data['street'],
		      'city'=>$this->data['city'],
		      'post_code'=>$this->data['post_code'],
		      'cuisine'=>json_encode($this->data['cuisine']),
		      'username'=>$this->data['username'],
		      'password'=>md5($this->data['password']),
		      'status'=>$status,
		      'date_created'=>date('c'),
		      'ip_address'=>$_SERVER['REMOTE_ADDR'],
		      'activation_token'=>$token,
		      'activation_key'=>Yii::app()->functions->generateRandomKey(5),
		      'merchant_slug'=>Yii::app()->functions->createSlug($this->data['merchant_name']),
		      'package_id'=>$this->data['package_id'],
		      'package_price'=>$package_price,
		      'membership_expired'=>$membership_expired,
		      'payment_steps'=>2,
		      //'country_code'=>Yii::app()->functions->adminSetCounryCode(),
		      'country_code'=>"ID",//$this->data['country_code'],
		      'state'=>$this->data['state'],
		      'abn'=>isset($this->data['abn'])?$this->data['abn']:'',
		      'service'=>isset($this->data['service'])?$this->data['service']:'',
		    );				 
		    		    
		    
		    if ( !Yii::app()->functions->validateUsername($this->data['username']) ){

		    	if ($respck=Yii::app()->functions->validateMerchantUserFromMerchantUser($params['username'],
		    	    $params['contact_email'])){
		    		$this->msg=$respck;
		    		return ;		    		
		    	}		    
		    	
			    if ($this->insertData("{{merchant}}",$params)){
			    	$this->code=1;
			    	$this->msg=Yii::t("default","Successful");
			    	$this->details=$token;
			    	
			    	// send email activation key
			    	$tpl=EmailTPL::merchantActivationCode($params);
		            $sender=Yii::app()->functions->getOptionAdmin('website_contact_email');
		            $to=$this->data['contact_email'];		    		  
		            if (!sendEmail($to,$sender,"Merchant Registration",$tpl)){		    	
		            	//$this->details="failed";
		            } //else $this->details="ok mail";
		            			    				    				    	
			    } else $this->msg=Yii::t("default","Sorry but we cannot add your information. Please try again later");
		    } else $this->msg=Yii::t("default","Sorry but your username is alread been taken.");
		}
		
		public function activationMerchant()
		{		   
		   $merchant_status=Yii::app()->functions->getOptionAdmin("merchant_sigup_status");		   
			
		   if (isset($this->data['activation_code']) && isset($this->data['token'])){
		      $stmt="SELECT * FROM		   
		      {{merchant}}
		      WHERE
		      activation_token='".$this->data['token']."'
		      LIMIT 0,1
		      ";
		      if ($res=$this->rst($stmt)){		      	 		      	 
		      	 if ($res[0]['status']=="active"){
		      	 	$this->msg=Yii::t("default","Merchant is already activated.");
		      	 } else {		      
			      	 if ($res[0]['activation_key']==$this->data['activation_code']){
			      	 	$this->code=1;		      	 	
			      	 	$this->msg=Yii::t("default","Merchant Successfully activated.");
			      	 	$this->details=$this->data['token'];
			      	 	
			      	 	$params=array('status'=>"active",'date_activated'=>date('c'),'ip_address'=>$_SERVER['REMOTE_ADDR']);
			      	 				      	 	
			      	 	/*If payment was offline don't set the the status to active*/
			      	 	if ( $package_info=FunctionsV3::getMerchantPaymentMembership($res[0]['merchant_id'],
			      	 	$res[0]['package_id'])){			      	 		
			      	 		$offline_payment=FunctionsV3::getOfflinePaymentList();			      	 		
			      	 		if ( array_key_exists($package_info['payment_type'],(array)$offline_payment)){
			      	 			unset($params['status']);
			      	 		}
			      	 	}			      	 
			      	 				      	 	
			      	 	$this->updateData("{{merchant}}",$params,'merchant_id',$res[0]['merchant_id']);
			      	 	
			      	 } else $this->msg=Yii::t("default","Invalid Activation code.");		      
		      	 }
		      } else $this->msg=Yii::t("default","Sorry but we cannot find your information.");
		   } else $this->msg=Yii::t("default","ERROR: Missing parameters");
		}
		
		public function merchantPayment()
		{
					
			$token=isset($this->data['token'])?$this->data['token']:'';
			if (isset($this->data['payment_opt'])){
				if (!$merchant=Yii::app()->functions->getMerchantByToken($token)){
					$this->msg=Yii::t("default","ERROR: cannot get merchant information");
					return ;
				}
				if ( $this->data['payment_opt']=="ccr" || $this->data['payment_opt']=="ocr" ){
					if (is_numeric($this->data['cc_id'])){

						if (isset($this->data['renew'])){
							
							$membership_info=Yii::app()->functions->upgradeMembership($merchant['merchant_id'],$this->data['package_id']);
							$params=array(
							  'package_id'=>$this->data['package_id'],
							  'merchant_id'=>$merchant['merchant_id'],
							  'price'=>$membership_info['package_price'],
							  'payment_type'=>$this->data['payment_opt'],
							  'mt_id'=>$this->data['cc_id'],
							  'date_created'=>date('c'),
							  'ip_address'=>$_SERVER['REMOTE_ADDR'],
							  'membership_expired'=>$membership_info['membership_expired']
							);							
							$this->insertData("{{package_trans}}",$params);
							
							$params_update=array(
							  'package_id'=>$this->data['package_id'],
							  'package_price'=>$membership_info['package_price'],
							  'membership_expired'=>$membership_info['membership_expired'],
							  'membership_purchase_date'=>date('c'),
							  'status'=>'active'
							);
							
							$this->updateData("{{merchant}}",$params_update,'merchant_id',$merchant['merchant_id']);
							$this->code=1;
							$this->msg=Yii::t("default","Payment Successful");
							
						} else {					
							$params=array(
							  'package_id'=>$merchant['package_id'],
							  'merchant_id'=>$merchant['merchant_id'],
							  'price'=>$merchant['package_price'],
							  'payment_type'=>$this->data['payment_opt'],
							  'mt_id'=>$this->data['cc_id'],
							  'date_created'=>date('c'),
							  'ip_address'=>$_SERVER['REMOTE_ADDR']
							);
							
							if ( $package=Yii::app()->functions->getPackagesById($merchant['package_id'])){
								$expiration=$package['expiration'];
	                            $membership_expired = date('Y-m-d', strtotime ("+$expiration days"));							
	                            $params['membership_expired']=$membership_expired;
				            }	
				            if ($this->insertData("{{package_trans}}",$params)){
							$this->code=1;
							$this->msg=Yii::t("default","Payment Successful");
							$this->details=$token;
							
							$this->updateData("{{merchant}}",
							   array(
							     'payment_steps'=>3,
							     'membership_purchase_date'=>date('c')
							   )
							   ,'merchant_id',$merchant['merchant_id']);
							   
						    } else $this->msg=Yii::t("default","ERROR: Cannot insert records.");
						}
			            												
					} else $this->msg=Yii::t("default","Please select credit card.");			

				} else {
					
					if ( isset($this->data['payment_opt'])){
						
						$this->code=1;    
						$this->msg=Yii::t("default","Please wait while we redirect you");
                        $this->details=Yii::app()->request->baseUrl."/store/merchantsignup/Do/step3b/token/$token/gateway/".$this->data['payment_opt'];                      
                        
                        if (isset($this->data['renew'])){
                      	  $this->details=Yii::app()->request->baseUrl."/store/merchantsignup/Do/step3b/token/$token/gateway/".$this->data['payment_opt']."/renew/1/package_id/".$this->data['package_id'];
                         }                      	                          
						
					} else $this->msg=Yii::t("default","No payment method has been selected.");					
				}
				
			} else $this->msg=Yii::t("default","Please select payment option");		
		}
		
		public function merchantFreePayment()
		{						
			if (!$merchant=Yii::app()->functions->getMerchantByToken($this->data['token'])){			
				$this->msg=Yii::t("default","ERROR: Cannot get package information");
				return ;
			}	
			$this->updateData("{{merchant}}",
							   array('payment_steps'=>3),'merchant_id',$merchant['merchant_id']);
		}
		
		public function rptSalesMerchant()
		{			
			$and='';  
	    	if (isset($this->data['start_date']) && isset($this->data['end_date']))	{
	    		if (!empty($this->data['start_date']) && !empty($this->data['end_date'])){
	    		$and=" WHERE date_created BETWEEN  '".$this->data['start_date']." 00:00:00' AND 
	    		        '".$this->data['end_date']." 23:59:00'
	    		 ";
	    		}
	    	}
	    	
	    	$order_status_id='';
	    	$or='';
	    	if (isset($this->data['stats_id'])){
		    	if (is_array($this->data['stats_id']) && count($this->data['stats_id'])>=1){
		    		foreach ($this->data['stats_id'] as $stats_id) {		    			
		    			$order_status_id.="'$stats_id',";
		    		}
		    		if ( !empty($order_status_id)){
		    			$order_status_id=substr($order_status_id,0,-1);
		    		}		    	
		    	}	    
	    	}
	    	
	    	if ( !empty($order_status_id)){	    		
	    		if (empty($and)){
	    			$and.= " WHERE status IN ($order_status_id)";
	    		} else $and.= " AND status IN ($order_status_id)";
	    	}	    	    	
	    		    
	    	
	    	$DbExt=new DbExt;
	    	$stmt="SELECT a.*,
	    	(
	    	select title 
	    	from
	    	{{packages}} 
	    	where
	    	package_id=a.package_id
	    	) as package_name
	    	 FROM
	    	{{merchant}} a	    	
	    	$and
	    	LIMIT 0,2000
	    	";	    	
	    	//dump($stmt);
	    	$_SESSION['kr_export_stmt']=$stmt;
	    	
	    	if ($res=$this->rst($stmt)){	    		
	    		foreach ($res as $val) {	   
	    			
	    		   $action="<a data-id=\"".$val['merchant_id']."\" class=\"edit-merchant-status\" href=\"javascript:\">".Yii::t("default","Edit")."</a>"; 				
	    		    /*$date=Yii::app()->functions->prettyDate($val['date_created']);
	    		    $date=Yii::app()->functions->translateDate($date);    			*/
	    		    
	    		    $date=FormatDateTime($val['date_created'],false);
	    		    
	    			$feed_data['aaData'][]=array(
	    			  $val['merchant_id'],
	    			  $val['merchant_name'],
	    			  $val['contact_name'],
	    			  $val['contact_phone']." / ".$val['contact_email'],
	    			  $val['street']." ".$val['city']." ".$val['state']." ".$val['post_code'],
	    			  $date."<br/>".ucwords($val['status']),
	    			  $action
	    			);	    			
	    		}
	    		$this->otableOutput($feed_data);
	    	}		
	    	$this->otableNodata();	
		}
		
		public function editPayment()
		{
  	       $status_list=paymentStatus();
	    	?>
	    	<div class="view-receipt-pop">
	    	 <h3><?php echo Yii::t("default","Change Order Status")?></h3>
	    	 
	    	 <?php if ( $res=Yii::app()->functions->getMerchantPaymentByID($this->data['id']) ):?>
	    	    <form id="frm-pop" class="frm-pop uk-form uk-form-horizontal" method="POST" onsubmit="return false;">
	    	    <?php echo CHtml::hiddenField('action','updatePaymennt')?>
	    	    <?php echo CHtml::hiddenField('id',$this->data['id'])?>
	    	    
		    	 <div class="uk-form-row">
		    	  <label class="uk-form-label"><?php echo Yii::t("default","Status")?></label>
		    	  <?php echo CHtml::dropDownList('status',$res['status'],(array)$status_list,array(
		    	  'class'=>"uk-form-width-large"
		    	  ))?>
		    	 </div>
		    	 
		    	 <div class="action-wrap">
		    	   <?php echo CHtml::submitButton('Submit',
		    	   array('class'=>"uk-button uk-form-width-medium uk-button-success"))?>
		    	 </div>
	    	   </form> 
	    	 <?php else:?>
	    	 <p class="uk-text-danger"><?php echo Yii::t("default","Error: Order not found")?></p>
	    	 <?php endif;?>
	    	</div> <!--view-receipt-pop-->	    					    
			<script type="text/javascript">
			$.validate({ 	
			    form : '#frm-pop',    
			    onError : function() {      
			    },
			    onSuccess : function() {     
			      form_submit('frm-pop');
			      return false;
			    }  
			});		
			</script>
	    	<?php
	    	die();
		}
		
		public function updatePaymennt()
		{			
			if(isset($this->data['status']) && isset($this->data['id'])){
				$params=array('status'=>$this->data['status']);
				if ($this->updateData('{{package_trans}}',$params,'id',$this->data['id'])){
					$this->code=1;
					$this->msg=Yii::t("default","Successfully updated.");
				} else $this->msg=Yii::t("default","ERROR: cannot update records.");
			} else $this->msg=Yii::t("default","ERROR: Missing parameters");		
		}
		
		
		public function newMerchantRegList()
		{
			
			$datenow=date('Y-m-d');
		    $and="WHERE date_created like '$datenow%' ";
	    	$DbExt=new DbExt;	    		
	    	$stmt="SELECT a.*,
	    	(
	    	select title 
	    	from
	    	{{packages}} 
	    	where
	    	package_id=a.package_id
	    	) as package_name,
	    	
	    	(
	    	select payment_type
	    	from
	    	{{package_trans}}
	    	where
	    	merchant_id=a.merchant_id
	    	ORDER BY id DESC
	    	LIMIT 0,1
	    	) as payment_type
	    	
	    	 FROM
	    	{{merchant}} a	    	
	    	$and
	    	";	   
	    	//dump($stmt);
	    	if ( $res=$DbExt->rst($stmt)){	    		
	    		foreach ($res as $val) {	    		   
	    		   $action="<a data-id=\"".$val['merchant_id']."\" class=\"edit-merchant-status\" href=\"javascript:\">".Yii::t("default","Edit")."</a>";
	    		   $feed_data['aaData'][]=array(
	    		      $val['merchant_id'],
	    		      stripslashes($val['merchant_name']),
	    		      $val['contact_name'],
	    		      $val['contact_phone']." / ".$val['contact_email'],
	    		      $val['street']." ".$val['city']." ".$val['state']." ".$val['post_code'],
	    		      $val['status'],
	    		      //Yii::app()->functions->prettyDate($val['date_created'],true),
	    		      Yii::app()->functions->FormatDateTime($val['date_created'],true),
	    		      $action
	    		   );	
	    		}	    		
	    		$this->otableOutput($feed_data);
	    	}	    	
	    	$this->otableNodata();
		}
		
		public function editMerchantStatus()
		{					
  	        $status_list=clientStatus();
	    	?>
	    	<div class="view-receipt-pop">
	    	 <h3>Change Order Status</h3>
	    	 
	    	 <?php if ( $res=Yii::app()->functions->getMerchant($this->data['id']) ):?>
	    	    <form id="frm-pop" class="frm-pop uk-form uk-form-horizontal" method="POST" onsubmit="return false;">
	    	    <?php echo CHtml::hiddenField('action','updateMerchantStatus')?>
	    	    <?php echo CHtml::hiddenField('id',$this->data['id'])?>
	    	    
		    	 <div class="uk-form-row">
		    	  <label class="uk-form-label"><?php echo Yii::t("default","Status")?></label>
		    	  <?php echo CHtml::dropDownList('status',$res['status'],(array)$status_list,array(
		    	  'class'=>"uk-form-width-large"
		    	  ))?>
		    	 </div>
		    	 
		    	 <div class="action-wrap">
		    	   <?php echo CHtml::submitButton('Submit',
		    	   array('class'=>"uk-button uk-form-width-medium uk-button-success"))?>
		    	 </div>
	    	   </form> 
	    	 <?php else:?>
	    	 <p class="uk-text-danger"><?php echo Yii::t("default","Error: Order not found")?></p>
	    	 <?php endif;?>
	    	</div> <!--view-receipt-pop-->	    					    
			<script type="text/javascript">
			$.validate({ 	
			    form : '#frm-pop',    
			    onError : function() {      
			    },
			    onSuccess : function() {     
			      form_submit('frm-pop');
			      return false;
			    }  
			});		
			</script>
	    	<?php
	    	die();
		}


		
		public function updateMerchantStatus()
		{			
			if (isset($this->data['id'])){
				$params=array('status'=>$this->data['status']);				
				if ($this->updateData("{{merchant}}",$params,'merchant_id',$this->data['id'])){
					$this->code=1;
					$this->msg=Yii::t("default","Status Updated.");
				} else $this->msg=Yii::t("default","Error; cannot update status.");
			} else $this->msg=Yii::t("default","Missing parameters");
		}		
		
		public function sponsoreMerchantAdd()
		{		
			if (isset($this->data['merchant_id'])){
				$params=array(
				  'is_sponsored'=>2,
				  'sponsored_expiration'=>$this->data['expiration'],
				  'date_modified'=>date('c')
				);				
				if ( $this->updateData("{{merchant}}",$params,'merchant_id',$this->data['merchant_id'])){
					$this->code=1;
					$this->msg=Yii::t("default","Successful");
					//$this->details=$this->data['merchant_id'];
				} else $this->msg=Yii::t("default","ERROR: cannot update records.");
			} else $this->msg=Yii::t("default","Missing parameters");
		}	
		
		public function sponsoredMerchantList()
		{
			$slug=websiteUrl().'/admin/'.$this->data['slug'];
			$stmt="SELECT * FROM
			{{merchant}}
			WHERE
			status in ('active')
			AND
			is_sponsored='2'
			ORDER BY merchant_name ASC
			";
			if ($res=$this->rst($stmt)){
			   foreach ($res as $val) {				   	    
					$action="<div class=\"options\">
    	    		<a href=\"$slug/Do/Add/?id=$val[merchant_id]\" >".Yii::t("default","Edit")."</a>
    	    		<a href=\"javascript:;\" class=\"row_del\" rev=\"$val[merchant_id]\" >".Yii::t("default","Remove")."</a>
    	    		</div>";		   	   
				   /*$date=Yii::app()->functions->prettyDate($val['sponsored_expiration']);					
				   $date=Yii::app()->functions->translateDate($date);*/
				   $date=FormatDateTime($val['sponsored_expiration'],false);
			   	   $feed_data['aaData'][]=array(
			   	      $val['merchant_id'],
			   	      $val['merchant_name'].$action,
			   	      $date
			   	   );			       
			   }
			   $this->otableOutput($feed_data);
			}
			$this->otableNodata();
		}
		
		public function currencyList()
		{
			$slug=$this->data['slug'];
			$stmt="SELECT * FROM
			{{currency}}
			ORDER BY date_created DESC
			";
			if ($res=$this->rst($stmt)){
			   foreach ($res as $val) {				   	    			   	    
					$action="<div class=\"options\">
    	    		<a href=\"$slug/Do/Add/?id=$val[currency_code]\" >".Yii::t("default","Edit")."</a>
    	    		<a href=\"javascript:;\" class=\"row_del\" rev=\"$val[currency_code]\" >".Yii::t("default","Delete")."</a>
    	    		</div>";		   	   
					
				   /*$date=Yii::app()->functions->prettyDate($val['date_created']);	
				   $date=Yii::app()->functions->translateDate($date);	*/
				   $date=FormatDateTime($val['date_created']);
			   	   $feed_data['aaData'][]=array(
			   	      $val['currency_code'].$action,
			   	      $val['currency_symbol'],
			   	      $date
			   	   );			       
			   }
			   $this->otableOutput($feed_data);
			}
			$this->otableNodata();
		}
		
		public function addCurrency()
		{			
			$Validator=new Validator;
			$req=array(
			  'currency_code'=>Yii::t("default","Currency Code is required"),
			  'currency_symbol'=>Yii::t("default","Currency Symbol is required")
			);		
			$Validator->required($req,$this->data);
			if ($Validator->validate()){
				$params=array(
				  'currency_code'=>$this->data['currency_code'],
				  'currency_symbol'=>$this->data['currency_symbol'],
				  'ip_address'=>$_SERVER['REMOTE_ADDR']
				);
			   if (empty($this->data['id'])){	
			    	if ( $this->insertData("{{currency}}",$params)){
			    		    $this->details=Yii::app()->db->getLastInsertID();
				    		$this->code=1;
				    		$this->msg=Yii::t("default","Successful");				    		
				    	}
				    } else {		    	
				    	unset($params['date_created']);
						$params['date_modified']=date('c');				
						$res = $this->updateData('{{currency}}' , $params ,'currency_code',$this->data['id']);
						if ($res){
							$this->code=1;
			                $this->msg=Yii::t("default",'Currency updated.');  
					} else $this->msg=Yii::t("default","ERROR: cannot update");
			    }	
			} else $this->msg=$Validator->getErrorAsHTML();		
		}
				
		public function merchantSetReady()
		{
			$mtid=Yii::app()->functions->getMerchantID();
			if (isset($this->data['status'])){
				$params=array(
				  'is_ready'=>$this->data['status'],
				  'date_modified'=>date('c'),
				  'ip_address'=>$_SERVER['REMOTE_ADDR']
				);			

							
				if ( $_SESSION['kr_merchant_user_type']=="merchant_user"){
					$user_info=Yii::app()->functions->getMerchantInfo();					
					$user_id=$user_info[0]->merchant_user_id;					
					if (!$data=Yii::app()->functions->getMerchantUserInfo($user_id)){
						$this->msg=t("Sorry but you dont have permission");
						return ;
					} else $user_access=json_decode($data['user_access'],true);					
					if (!in_array('can_published_merchant',$user_access)){
						$this->msg=t("Sorry but you dont have permission");
						return ;
					}
				}
				
				if ( $this->updateData("{{merchant}}",$params,'merchant_id',$mtid)){
					$this->code=1;
					if ( $this->data['status']==2){
					    $this->msg=Yii::t("default","Successful Merchant is now published.");
					} else $this->msg=Yii::t("default","Successful");
				} else $this->msg=Yii::t("default","ERROR: cannot update status");			
			} else $this->msg=Yii::t("default","Missing parameters");		
		}	
		
		public function merchantStatus()
		{
			$mtid=Yii::app()->functions->getMerchantID();					
			if ( $res=Yii::app()->functions->getMerchant($mtid)){												
				$this->code=1;
				$this->msg=$res['is_ready'];
				$this->details=array(
				 'status'=>$res['status'],				 
				 'display_status'=>strtoupper(Yii::t("default",$res['status'])),
				 'is_commission'=>$res['is_commission']
				);
			} else $this->msg=Yii::t("default","ERROR:");
		}
		
		public function OrderStatusList()
		{
			$this->OrderStatusListMerchant(false);
		}
		
		public function OrderStatusListMerchant($as_merchant=true)
		{
			$mtid=Yii::app()->functions->getMerchantID();
			$where="WHERE merchant_id='$mtid'";
			if ( $as_merchant==FALSE){
				$where='';
			}
		    $slug=$this->data['slug'];
			$stmt="SELECT * FROM
			{{order_status}}
			$where
			ORDER BY description ASC
			";			
			if ($res=$this->rst($stmt)){
			   foreach ($res as $val) {				   	    			   	    
					$action="<div class=\"options\">
    	    		<a href=\"$slug/Do/Add/?id=$val[stats_id]\" >".Yii::t("default","Edit")."</a>
    	    		<a href=\"javascript:;\" class=\"row_del\" rev=\"$val[stats_id]\" >".Yii::t("default","Delete")."</a>
    	    		</div>";		   	   
				   /*$date=Yii::app()->functions->prettyDate($val['date_created']);	
				   $date=Yii::app()->functions->translateDate($date);	*/
				   $date=FormatDateTime($val['date_created']);
			   	   $feed_data['aaData'][]=array(
			   	      $val['stats_id'],
			   	      $val['description'].$action,
			   	      $date
			   	   );			       
			   }
			   $this->otableOutput($feed_data);
			}
			$this->otableNodata();			
		}	

		
	public function TopUpConfirmation() {
		
		if (!isset($this->data['from_bank'])){
			$this->msg=$this->t("Bank from is missing");
			$this->output();
		}
		if (!isset($this->data['amount'])){
			$this->msg=$this->t("Transfer Amount is missing");
			$this->output();
		}
		if (!isset($this->data['from_account'])){
			$this->msg=$this->t("From Account is missing");
			$this->output();
		}
		 
		if ( $client_id=Yii::app()->functions->getClientId()){

				// Path to move uploaded files
				$target_path = "upload/";
				// final file url that is being uploaded
				$file_upload_url = Yii::app()->getBaseUrl(true)  . 'upload/' . $target_path;

				// if (isset($_FILES['image']['name'])) {
					
					// $target_path = $target_path . basename($_FILES['image']['name']);
					
					// reading other post parameters
					// try {
						// Throws exception incase file is not being moved
						
						// $fname = $_FILES['image']['name'];
						// $rawBaseName = pathinfo($fname, PATHINFO_FILENAME );
						// $extension = pathinfo($fname, PATHINFO_EXTENSION );
						// $counter = 0;
						// while(file_exists("upload/".$fname)) {
							// $fname = $rawBaseName . $counter . '.' . $extension;
							// $counter++;
						// };
						// if (!move_uploaded_file($_FILES['image']['tmp_name'], $target_path.$fname)) {
							// // $response['message'] = 'Could not move the file!';
							// $this->msg=Yii::t("default","Failed Upload.");
						// } else {
							// File successfully uploaded
							// $filepath = $file_upload_url . $fname;
							$params=array(
							  'client_id'=>$client_id,
							  'amount'=>$this->data['amount'],
							  'from_bank'=>$this->data['from_bank'],
							  'from_account'=>$this->data['from_account'],
							  'to_bank'=>$this->data['to_bank'],
							  'to_account'=>$this->data['to_account'],
							  'description'=>$this->data['description'],
							  'image'=>$this->data['photo2'],
							  'date_created'=>date('c'),
							  'status'=>'pending',
							);
							// $DbExt=new DbExt;
							if ( $this->insertData("{{topup_confirm}}",$params)){
								$this->code=1;
								// $this->details=array(
								  // 'image'=>$filepath,
								// );
								$this->msg=Yii::t("default","You have confirm your topup process. Please wait for approval.");	    		    	
							} else $this->msg=Yii::t("default","ERROR: cannot insert records.");		
						// }
					// } catch (Exception $e) {
						// // Exception occurred. Make error flag true
						// // $response['message'] = $e->getMessage();
						// $this->msg=Yii::t("default","Process Failed".$e->getMessage());
					// }
				// } else {
					// // File parameter is missing
					// // $response['message'] = 'Not received any file!F';
					// $this->msg=Yii::t("default","File is missing");
				// }
		} else $this->msg=$this->t("it seems that your token has expired. please re login again");

	}

		
		public function updateClientProfile()
		{
			$client_id=Yii::app()->functions->getClientId();			
			if (!is_numeric($client_id)){
				$this->msg=Yii::t("default","ERROR: Your session has expired.");
				return ;
			}		
					
			$func=new FunctionsK();
			if ($func->CheckCustomerMobile($this->data['contact_phone'],$client_id)){
				$this->msg=t("Sorry but your mobile number is already exist in our records");
				return;
			}					
			
			$params=array(
			  'first_name'=>isset($this->data['first_name'])?$this->data['first_name']:'',
			  'last_name'=>isset($this->data['last_name'])?$this->data['last_name']:'',
			  'street'=>isset($this->data['street'])?$this->data['street']:'',
			  'city'=>isset($this->data['city'])?$this->data['city']:'',
			  'state'=>isset($this->data['state'])?$this->data['state']:'',
			  'zipcode'=>isset($this->data['zipcode'])?$this->data['zipcode']:'',
			  'contact_phone'=>isset($this->data['contact_phone'])?$this->data['contact_phone']:'',
			  'date_modified'=>date('c'),
			  'ip_address'=>$_SERVER['REMOTE_ADDR']
			);
			
			
            /** update 2.3*/
	    	if (isset($this->data['custom_field1'])){
	    		$params['custom_field1']=!empty($this->data['custom_field1'])?$this->data['custom_field1']:'';
	    	}
	    	if (isset($this->data['custom_field2'])){
	    		$params['custom_field2']=!empty($this->data['custom_field2'])?$this->data['custom_field2']:'';
	    	}
	    			    				
			if (isset($this->data['password'])){
				if (!empty($this->data['password'])){
					$params['password']=md5($this->data['password']);
				}			
			}					
			
			if (isset($this->data['password'])){
				if (!empty($this->data['password'])){
					if (  $this->data['password']!=$this->data['cpassword'] ){
						$this->msg=t("Confirm password does not match.");
						return ;
					}
				}
			}		
						
			if ($this->updateData("{{client}}",$params,'client_id',$client_id)){
				$this->code=1;
				$this->msg=Yii::t("default","Profile Updated.");
			} else $this->msg=Yii::t("default","ERROR: cannot update profile.");
		}
		
	    public function contactSettings()
	    {	    	
	    	if (!isset($this->data['contact_content'])){
	        	$this->data['contact_content']='';
	        }
	        if (!isset($this->data['contact_map'])){
	        	$this->data['contact_map']='';
	        }
	        if (!isset($this->data['map_latitude'])){
	        	$this->data['map_latitude']='';
	        }
	        if (!isset($this->data['map_longitude'])){
	        	$this->data['map_longitude']='';
	        }
	        if (!isset($this->data['contact_email_receiver'])){
	        	$this->data['contact_email_receiver']='';
	        }
	        if (!isset($this->data['contact_field'])){
	        	$this->data['contact_field']='';
	        }
	    
	        if (is_array($this->data['contact_field']) && count($this->data['contact_field'])>=1){
	        yii::app()->functions->updateOptionAdmin('contact_content',$this->data['contact_content']);
	        yii::app()->functions->updateOptionAdmin('contact_map',$this->data['contact_map']);
	        yii::app()->functions->updateOptionAdmin('map_latitude',$this->data['map_latitude']);
	        yii::app()->functions->updateOptionAdmin('map_longitude',$this->data['map_longitude']);
	        yii::app()->functions->updateOptionAdmin('contact_email_receiver',$this->data['contact_email_receiver']);
	        yii::app()->functions->updateOptionAdmin('contact_field',json_encode($this->data['contact_field']));
	        $this->code=1;
	    	$this->msg=Yii::t("default","Settings saved.");
	        } else $this->msg=Yii::t("default","Contact field must have 1 or more fields");
	    }		
	    
	    public function adminSocialSettings()
	    {	    	
	    	yii::app()->functions->updateOptionAdmin('social_flag',isset($this->data['social_flag'])?$this->data['social_flag']:"");
	    	
	    	yii::app()->functions->updateOptionAdmin('fb_flag',isset($this->data['fb_flag'])?$this->data['fb_flag']:"");
	    	yii::app()->functions->updateOptionAdmin('fb_app_id',isset($this->data['fb_app_id'])?$this->data['fb_app_id']:"");
	    	yii::app()->functions->updateOptionAdmin('fb_app_secret',isset($this->data['fb_app_secret'])?$this->data['fb_app_secret']:"");
	    	
	    	yii::app()->functions->updateOptionAdmin('admin_fb_page',isset($this->data['admin_fb_page'])?$this->data['admin_fb_page']:"");
	    	
	    	yii::app()->functions->updateOptionAdmin('admin_twitter_page',isset($this->data['admin_twitter_page'])?$this->data['admin_twitter_page']:"");
	    	
	    	yii::app()->functions->updateOptionAdmin('admin_google_page',isset($this->data['admin_google_page'])?$this->data['admin_google_page']:"");
	    	
	    	yii::app()->functions->updateOptionAdmin('google_client_id',
	    	isset($this->data['google_client_id'])?$this->data['google_client_id']:"");
	    	
	    	yii::app()->functions->updateOptionAdmin('google_client_secret',
	    	isset($this->data['google_client_secret'])?$this->data['google_client_secret']:"");
	    	
	    	yii::app()->functions->updateOptionAdmin('google_client_redirect_ulr',
	    	isset($this->data['google_client_redirect_ulr'])?$this->data['google_client_redirect_ulr']:"");
	    	
	    	yii::app()->functions->updateOptionAdmin('google_login_enabled',
	    	isset($this->data['google_login_enabled'])?$this->data['google_login_enabled']:"");

			
	    	yii::app()->functions->updateOptionAdmin('admin_intagram_page',
	    	isset($this->data['admin_intagram_page'])?$this->data['admin_intagram_page']:"");
	    	
			
			
	    	yii::app()->functions->updateOptionAdmin('admin_youtube_url',
	    	isset($this->data['admin_youtube_url'])?$this->data['admin_youtube_url']:"");
	    	
	    	$this->code=1;
	    	$this->msg=Yii::t("default","Settings saved.");
	    }
	    
	    public function contacUsSubmit()
	    {	    	
	    	unset($this->data['action']);
	    	foreach ($this->data as $key=>$val) {    		
	    	    $required[$key]=ucwords($key). " ". Yii::t("default","is required");
	    	}    		    	
	    		    	
	    	$validator=new Validator;
	    	$validator->required($required,$this->data);
	    	if ($validator->validate()){    		
	    		$tpl="<p>".Yii::t("default","Hi admin")."</p>";
    		    $tpl.="<p>".Yii::t("default","There is someone fill the contact form")."<p>";
	    		$tpl.="<p>".Yii::t("default","see below information")."<p>";
	    		foreach ($this->data as $key=>$val) {    		
	    			$tpl.=ucwords($key)." : $val<br/>";
	    		}    			    		    		
	    		$subject=Yii::t("default","New Contact Us");	    		
	    		$to=yii::app()->functions->getOptionAdmin('contact_email_receiver');	    			    		
				//$from='no-reply@'.$_SERVER['HTTP_HOST'].".com";					
				//$from='no-reply@'.$_SERVER['HTTP_HOST'];
				$from="";
				
				if (empty($to)){
					$this->msg=Yii::t("default","ERROR: no email to send.");
					return ;
				}	    	
							
				if ( Yii::app()->functions->sendEmail($to,$from,$subject,$tpl) ){
					$this->code=1;    		
	    		    $this->msg=Yii::t("default","Your message was sent successfully. Thanks.");
				} else $this->msg=Yii::t("default","ERROR: Cannot sent email.");	    		
	    	} else $this->msg=$validator->getErrorAsHTML();
	    }
	    
	    public function ratingList()
	    {
			$slug=$_GET['slug'];
			$stmt="SELECT * FROM
			{{rating_meaning}}		
			ORDER BY rating_start ASC						
			";						
			if ($res=$this->rst($stmt)){
			   foreach ($res as $val) {				   	    			   	    
					$action="<div class=\"options\">
    	    		<a href=\"$slug/Do/Add/?id=$val[id]\" >".Yii::t("default","Edit")."</a>
    	    		<a href=\"javascript:;\" class=\"row_del\" rev=\"$val[id]\" >".Yii::t("default","Delete")."</a>
    	    		</div>";		   	   
			   	   $feed_data['aaData'][]=array(
			   	      $val['rating_start'].$action,
			   	      $val['rating_end'],
			   	      $val['meaning']
			   	   );			       
			   }
			   $this->otableOutput($feed_data);
			}
			$this->otableNodata();				    	
	    }
	    
	    public function addRatings()
	    {	    		
		    $Validator=new Validator;
			$req=array(
			  'meaning'=>Yii::t("default","Rating is required")
			);		
			$Validator->required($req,$this->data);
			if ($Validator->validate()){
				$params=array(
				  'rating_start'=>$this->data['rating_start'],
				  'rating_end'=>$this->data['rating_end'],
				  'meaning'=>$this->data['meaning'],
				  'date_created'=>date('c'),
				  'ip_address'=>$_SERVER['REMOTE_ADDR']				  
				);				
			   if (empty($this->data['id'])){	
			    	if ( $this->insertData("{{rating_meaning}}",$params)){
			    		   $this->details=Yii::app()->db->getLastInsertID();
				    		$this->code=1;
				    		$this->msg=Yii::t("default","Successful");				    		
				    	}
				    } else {		    	
				    	unset($params['date_created']);
						$params['date_modified']=date('c');				
						$res = $this->updateData('{{rating_meaning}}' , $params ,'id',$this->data['id']);
						if ($res){
							$this->code=1;
			                $this->msg=Yii::t("default",'Rating updated.');  
					} else $this->msg=Yii::t("default","ERROR: cannot update");
			    }	
			} else $this->msg=$Validator->getErrorAsHTML();		
	    }
	    
	    public function FBRegister()
	    {	    	
	        if (isset($this->data['email'])){
	    		$params=array(
	    		  'first_name'=>addslashes($this->data['first_name']),
	    		  'last_name'=>addslashes($this->data['last_name']),
	    		  'email_address'=>addslashes($this->data['email']),
	    		  'social_strategy'=>'fb',
	    		  'password'=>md5(addslashes($this->data['id'])),
	    		  'date_created'=>date('c'),
	    		  'ip_address'=>$_SERVER['REMOTE_ADDR']    		
	    		);    		
	    		if ($social_info=yii::app()->functions->accountExistSocial($this->data['email'])){      		    
	    		    /*AUTO LOGIN*/	            	    		    
		            $this->data['username']=addslashes($this->data['email']);
		            $this->data['password']=addslashes($social_info[0]['password']);
		            $this->data['password_md5']=addslashes($social_info[0]['password']);
		            $this->clientLogin();
	    	    } else {   	    	    	    
	    		     $command = Yii::app()->db->createCommand();
					if ($res=$command->insert('{{client}}',$params)){		
						
						/*POINTS PROGRAM*/		
						$client_id=Yii::app()->db->getLastInsertID();	  
						if (FunctionsV3::hasModuleAddon("pointsprogram")){
	    			       PointsProgram::signupReward($client_id);	
						}		            
	    			    				
			            $this->code=1;
			            $this->msg=Yii::t("default",'Information has been saved.');    
			            
			            /*AUTO LOGIN*/	            
			            $this->data['username']=addslashes($this->data['email']);
			            $this->data['password']=addslashes($this->data['id']);
			            $this->clientLogin();
			            			            
			        } else $this->msg=Yii::t("default",'ERROR. cannot insert data.');
	    	    }
	    	} else $this->msg=Yii::t("default","Unexpected response. please login again.");
	    }
	    
	    public function forgotPassword()
	    {
	    	
	    	$Validator=new Validator;
			$req=array(
			  'username-email'=>Yii::t("default","Email is required")
			);		
			$Validator->required($req,$this->data);
			if ($Validator->validate()){
				if ( $res=yii::app()->functions->isClientExist($this->data['username-email']) ){					
					$token=md5(date('c'));
					$params=array('lost_password_token'=>$token);					
					if ($this->updateData("{{client}}",$params,'client_id',$res['client_id'])){
						$this->code=1;						
						$this->msg=Yii::t("default","We sent your forgot password link, Please follow that link. Thank You.");
												
						//send email					
						$tpl=EmailTPL::forgotPass($res,$token);
					    //$sender=Yii::app()->functions->getOptionAdmin('website_contact_email');
					    $sender='';
		                $to=$res['email_address'];		                
		                if (!sendEmail($to,$sender,Yii::t("default","Forgot Password"),$tpl)){		    			                	
		                	$this->details="failed";
		                } else $this->details="mail ok";		
						
					} else $this->msg=Yii::t("default","ERROR: Cannot update records");				
				} else $this->msg=Yii::t("default","Sorry but your Email address does not exist in our records.");
			} else $this->msg=$Validator->getErrorAsHTML();
	    }
	    
	    public function changePassword()
	    {	    		    		    	
	    	$Validator=new Validator;
			$req=array(
			  'token'=>Yii::t("default","Token is missing")
			);		
			if ($this->data['password']!=$this->data['confirm_password']){
				$this->msg=Yii::t("default","Confirm password does not match.");
				return ;
			}	    
			$Validator->required($req,$this->data);
			if ($Validator->validate()){
	    		if ( $res=Yii::app()->functions->getLostPassToken($this->data['token'])){	    			
	    			$params=array(
	    			  'password'=>md5($this->data['password']),
	    			  'date_modified'=>date('c'),
	    			  'ip_address'=>$_SERVER['REMOTE_ADDR'],
	    			  'lost_password_token'=>md5(date('c'))
	    			);	    		
	    			if ( $this->updateData("{{client}}",$params,'client_id',$res['client_id'])){
	    				$this->code=1;
	    				$this->msg=Yii::t("default","Successful. Your password has been changed.");
	    			} else $this->msg=Yii::t("default","ERROR");	    		
	    		} else $this->msg=Yii::t("default","ERROR: Cannot update password");
	    	} else $this->msg=Yii::t("default","Token is missing");
	    }
	    
	    public function adminProfile()
	    {	    
	    	$params=array(
	    	  'first_name'=>$this->data['first_name'],
	    	  'last_name'=>$this->data['last_name'],
	    	  'user_lang'=>isset($this->data['user_lang'])?$this->data['user_lang']:'',
	    	  'date_modified'=>date('c'),
	    	  'ip_address'=>$_SERVER['REMOTE_ADDR'],
	    	  'email_address'=>isset($this->data['email_address'])?$this->data['email_address']:''
	    	);
	    	if (isset($this->data['password'])){
	    		if (!empty($this->data['password'])){
	    			if ($this->data['password']!=$this->data['cpassword']){
	    				$this->msg=Yii::t("default","Confirm password does not match.");
	    			   	return ;
	    			} else {
	    				$params['password']=md5($this->data['password']);
	    			}	    				
	    		}	    	
	    	}
	    	$admin_id=Yii::app()->functions->getAdminId(); 
	    	if ($this->updateData("{{admin_user}}",$params,'admin_id',$admin_id)){
	    		$this->code=1;
	    		$this->msg=Yii::t("default","Profile updated.");
	    	} else $this->msg=Yii::t("default","ERROR Cannot update.");
	    }
	    
	    public function AdminUserList()
	    {
	    	
		    $slug=Yii::app()->request->baseUrl."/admin/".$_GET['slug'];
			$stmt="SELECT * FROM
			{{admin_user}}		
			ORDER BY admin_id ASC						
			";						
			if ($res=$this->rst($stmt)){
			   foreach ($res as $val) {				   	    			   	    
					$action="<div class=\"options\">
    	    		<a href=\"$slug/Do/Add/?id=$val[admin_id]\" >".Yii::t("default","Edit")."</a>
    	    		<a href=\"javascript:;\" class=\"row_del\" rev=\"$val[admin_id]\" >".Yii::t("default","Delete")."</a>
    	    		</div>";		  
				   /*$date=Yii::app()->functions->prettyDate($val['date_created']);	
				   $date=Yii::app()->functions->translateDate($date); 	   */
				   $date=FormatDateTime($val['date_created']);
			   	   $feed_data['aaData'][]=array(
			   	      $val['admin_id'],
			   	      $val['username'].$action,
			   	      $val['first_name']." ".$val['last_name'],
			   	      $date
			   	   );			       
			   }
			   $this->otableOutput($feed_data);
			}
			$this->otableNodata();			    	
	    }
	    
	    public function addAdminUser()
	    {	    
	       $params=array(
	    	  'first_name'=>$this->data['first_name'],
	    	  'last_name'=>$this->data['last_name'],
	    	  'username'=>$this->data['username'],
	    	  'date_created'=>date('c'),
	    	  'ip_address'=>$_SERVER['REMOTE_ADDR'],
	    	  'email_address'=>isset($this->data['email_address'])?$this->data['email_address']:'',
	    	  'user_access'=>isset($this->data['user_access'])?json_encode($this->data['user_access']):''
	    	);
	    	
	    	if (isset($this->data['password'])){
	    		if (!empty($this->data['password'])){
	    			if ($this->data['password']!=$this->data['cpassword']){
	    				$this->msg=Yii::t("default","Confirm password does not match.");
	    			   	return ;
	    			} else {
	    				$params['password']=md5($this->data['password']);
	    			}	    				
	    		}	    	
	    	}
	    		    	
	    	if (empty($this->data['id'])){	
		    	if ( $this->insertData("{{admin_user}}",$params)){
		    		   $this->details=Yii::app()->db->getLastInsertID();
			    		$this->code=1;
			    		$this->msg=Yii::t("default","Successful");			    		
			    	}
			    } else {		    	
			    	unset($params['date_created']);
					$params['date_modified']=date('c');				
					$res = $this->updateData('{{admin_user}}' , $params ,'admin_id',$this->data['id']);
					if ($res){
						$this->code=1;
		                $this->msg=Yii::t("default",'User updated.');  
				} else $this->msg=Yii::t("default","ERROR: cannot update");
		    }	
	    }
	    
	    public function customPageList()
	    {
	        $slug=Yii::app()->request->baseUrl."/admin/".$_GET['slug'];
			$stmt="SELECT * FROM
			{{custom_page}}		
			ORDER BY slug_name ASC						
			";						
			if ($res=$this->rst($stmt)){
			   foreach ($res as $val) {				   	    			   	    
					$action="<div class=\"options\">
    	    		<a href=\"$slug/Do/Add/?id=$val[id]\" >".Yii::t("default","Edit")."</a>
    	    		<a href=\"javascript:;\" class=\"row_del\" rev=\"$val[id]\" >".Yii::t("default","Delete")."</a>
    	    		</div>";		   	  
				   /*$date=Yii::app()->functions->prettyDate($val['date_created']);	
				   $date=Yii::app()->functions->translateDate($date);	 */
				   $date=FormatDateTime($val['date_created']);
			   	   $feed_data['aaData'][]=array(
			   	      $val['id'],
			   	      $val['slug_name'],
			   	      $val['page_name'].$action,
			   	      //Yii::app()->functions->limitDescription($val['content']),
			   	      '<div class="limit-content">'.$val['content'].'</div>',
			   	      $date."<br/>".$val['status']
			   	   );			       
			   }
			   $this->otableOutput($feed_data);
			}
			$this->otableNodata();			    		    		
	    }
	    
	    public function addCustomPage()
	    {	    	
            $params=array(
	    	  'page_name'=>$this->data['page_name'],
	    	  'content'=>$this->data['content'],	    	  
	    	  'date_created'=>date('c'),
	    	  'ip_address'=>$_SERVER['REMOTE_ADDR'],
	    	  'status'=>$this->data['status'],
	    	  'icons'=>$this->data['icons'],
	    	  'open_new_tab'=>isset($this->data['open_new_tab'])?$this->data['open_new_tab']:1
	    	);
	    	
	    	if (isset($this->data['seo_title'])){
	    		$params['seo_title']=$this->data['seo_title'];
	    	}
	    	if (isset($this->data['meta_description'])){
	    		$params['meta_description']=$this->data['meta_description'];
	    	}
	    	if (isset($this->data['meta_keywords'])){
	    		$params['meta_keywords']=$this->data['meta_keywords'];
	    	}	    		  
	    	    		    	
	    	if (empty($this->data['id'])){	
	    		$params['slug_name']=strtolower(Yii::app()->functions->customPageCreateSlug($this->data['page_name']));	    	
		    	if ( $this->insertData("{{custom_page}}",$params)){
		    		$this->details=Yii::app()->db->getLastInsertID();
			    		$this->code=1;
			    		$this->msg=Yii::t("default","Successful");			    		
			    	}
			    } else {					    	
			    	unset($params['date_created']);
					$params['date_modified']=date('c');				
					$res = $this->updateData('{{custom_page}}' , $params ,'id',$this->data['id']);
					if ($res){
						$this->code=1;
		                $this->msg=Yii::t("default",'Page updated.');  
				} else $this->msg=Yii::t("default","ERROR: cannot update");
		    }		    	
	    }	
	    
	    public function assignCustomPage()
	    {	    	
	    	if (is_array($this->data['id']) && count($this->data['id'])>=1)
	    	{
	    		$x=1;
	    		foreach ($this->data['id'] as $key=>$id) {
	    			$params=array(
	    			  'assign_to'=>$this->data['assign_to'][$key],
	    			  'date_modified'=>date('c'),
	    			  'ip_address'=>$_SERVER['REMOTE_ADDR'],
	    			  'sequence'=>$x
	    			);	    			
	    			$this->updateData("{{custom_page}}",$params,'id',$id);
	    			$x++;
	    		}
	    	}
	    	$this->code=1;
	    	$this->msg=Yii::t("default","Successfully updated.");
	    }
	    
	    public function merchantForgotPass()
	    {	    	
	    	if ( isset($this->data['email_address'])){
	    		if ($res=yii::app()->functions->isMerchantExist($this->data['email_address']) ){	    			
	    			$params=array('lost_password_code'=> yii::app()->functions->generateCode());	    			
	    			if ( $this->updateData("{{merchant}}",$params,'merchant_id',$res[0]['merchant_id'])){
	    				$this->code=1;
	    				$this->msg=Yii::t("default","We have sent verification code in your email.");
	    				//send email	    				
	    				$tpl=EmailTPL::merchantForgotPass($res[0],$params['lost_password_code']);
	    				$sender=Yii::app()->functions->getOptionAdmin('website_contact_email');
		                $to=$res[0]['contact_email'];
		                if (!sendEmail($to,$sender,t("Merchant Forgot Password"),$tpl)){		    	
		                	$this->details="failed";
		                } else $this->details="ok mail";
	    				
	    			} else $this->msg=Yii::t("default","ERROR: Cannot update.");	    		
	    		} else $this->msg=Yii::t("default","Sorry but we cannot find your email address.");
	    	} else $this->msg=Yii::t("default","Email address is required");	    
	    }
	    
	    public function merchantChangePassword()
	    {	    	
	    	if (isset($this->data['lost_password_code'])){
	    		$stmt="SELECT * FROM
	    		{{merchant}}
	    		WHERE 
	    		lost_password_code='".$this->data['lost_password_code']."'
	    		AND
	    		contact_email='".$this->data['email']."'
	    		LIMIT 0,1
	    		";
	    		if ($res=$this->rst($stmt)){
	    			$merchant_id=$res[0]['merchant_id'];
	    			$params=array( 
	    			   'password'=>md5($this->data['new_password']),
	    			   'date_modified'=>date('c'),
	    			   'ip_address'=>$_SERVER['REMOTE_ADDR']
	    			); 
	    			if ($this->updateData("{{merchant}}",$params,'merchant_id',$merchant_id)){
	    				$this->msg=Yii::t("default","Change password succesful");
	    				$this->code=1;
	    			} else $this->msg=Yii::t("default","ERROR: cannot update records.");	    		
	    		} else $this->msg=Yii::t("default","Sorry but your verification code is incorrect");
	    	} else $this->msg=Yii::t("default","ERROR: missing parameters");    
	    }
	    
	    public function merchantResumeSignup()
	    {	    	
	    	$stmt="SELECT * FROM
	    	{{merchant}}
	    	WHERE
	    	contact_email='".$this->data['email_address']."'
	    	LIMIT 0,1
	    	";
	    	if ($res=$this->rst($stmt)){	    		
	    		if ($res[0]['status']=="active"){	    			
	    			$this->msg=Yii::t("default","Merchant is alread active.");
	    		} else {
	    		    $steps=$res[0]['payment_steps']+1;	    		    
	    		    $url=Yii::app()->getBaseUrl(true)."/store/merchantsignup/Do/step$steps/token/".$res[0]['activation_token'];
	    		    $this->code=1;
	    		    $this->msg=Yii::t("default","Successful");
	    		    $this->details=$url;
	    		}  
	    	} else $this->msg=Yii::t("default","Sorry but we cannot find your email address.");
	    }
	    
	    public function resendActivationCode()
	    {	    
	    	$stmt="SELECT * FROM
	    	{{merchant}}
	    	WHERE
	    	activation_token='".$this->data['token']."'
	    	LIMIT 0,1
	    	";
	    	if ($res=$this->rst($stmt)){	    		
	    		// send email activation key
		    	$tpl=EmailTPL::merchantActivationCode($res[0]);
	            $sender=Yii::app()->functions->getOptionAdmin('website_contact_email');
	            $to=$res[0]['contact_email'];	            
	            if (!sendEmail($to,$sender,Yii::t("default","Merchant Registration"),$tpl)){		    	
	            	$this->details="failed";
	            } else $this->details="ok mail";
	            $this->code=1;
	            $this->msg=Yii::t("default","We have sent the activation code to your email address.");
	    	} else $this->msg=Yii::t("default","Token is invalid.");
	    }
	    
		public function languageList()
		{
			
			$country_list=Yii::app()->functions->CountryList();
			$slug=$this->data['slug'];
			$stmt="SELECT * FROM
			{{languages}}
			ORDER BY date_created DESC
			";
			if ($res=$this->rst($stmt)){
			   foreach ($res as $val) {				   	    			   	    
					$action="<div class=\"options\">
    	    		<a href=\"$slug/Do/Add/?id=$val[lang_id]\" >".Yii::t("default","Edit")."</a>
    	    		<a href=\"javascript:;\" class=\"row_del\" rev=\"$val[lang_id]\" >".Yii::t("default","Delete")."</a>
    	    		</div>";		   	   
					
$country=array_key_exists($val['country_code'],(array)$country_list)?$country_list[$val['country_code']]:$val['country_code'];
					
                   /*$date=Yii::app()->functions->prettyDate($val['date_created']);  
                   $date=Yii::app()->functions->translateDate($date);*/
                   $date=FormatDateTime($val['date_created']);

			   	   $feed_data['aaData'][]=array(
			   	      $country.$action,
			   	      $val['language_code'],
			   	      $date."<div>".Yii::t("default","status")."</div>"
			   	   );			       
			   }
			   $this->otableOutput($feed_data);
			}
			$this->otableNodata();
		}
	    
		public function addLanguage()
		{			
			$this->data['id']=isset($this->data['id'])?$this->data['id']:'';
			$Validator=new Validator;
			$req=array(
			  'country_code'=>Yii::t("default","Country is required"),
			  'language_code'=>Yii::t("default","Language is required"),
			  'language_file'=>Yii::t("default","please upload language file"),
			);							
			
			if (!preg_match("/.php/i",$this->data['language_file'])) {
				$Validator->msg[]="Invalid language file must be a php file.";
			}
			
			$Validator->required($req,$this->data);
			if ($Validator->validate()){
				$params=array(
				  'country_code'=>$this->data['country_code'],
				  'language_code'=>$this->data['language_code'],
				  'date_created'=>date('c'),
				  'status'=>$this->data['status'],
				  'ip_address'=>$_SERVER['REMOTE_ADDR'],
				  'source_text'=>$this->data['language_file']
				);				
				if (is_numeric($this->data['id'])){
					unset($params['date_created']);
					$params['last_updated']=date('c');					
					if ($this->updateData("{{languages}}",$params,'lang_id',$this->data['id'])){
						$this->code=1;
						$this->msg=Yii::t("default","Successfully updared");
					} else $this->msg=Yii::t("default","ERROR: cannot update");				
				} else {
					if ( $this->insertData("{{languages}}",$params)){
						$this->details=Yii::app()->db->getLastInsertID();
			    		$this->code=1;
			    		$this->msg=Yii::t("default","Successful");			    		
			    	} else $this->msg=Yii::t("default","Failed. cannot insert records");
				}
			} else {
				$this->msg=$this->parseValidatorError($Validator->getError());
			}		
		}
		
		private function parseValidatorError($error='')
		{
			$error_string='';
			if (is_array($error) && count($error)>=1){
				foreach ($error as $val) {
					$error_string.="$val<br/>";
				}
			}
			return $error_string;		
		}	
		
		public function languageSettings()
		{
			yii::app()->functions->updateOptionAdmin('default_language',isset($this->data['default_language'])?$this->data['default_language']:"");
			

			yii::app()->functions->updateOptionAdmin('default_language_backend',
			isset($this->data['default_language_backend'])?$this->data['default_language_backend']:"");

	    	$this->code=1;
	    	$this->msg=Yii::t("default","Settings saved.");
		}		
		
		
		
		public function removeMerchantBg()
		{
			if (Yii::app()->functions->isMerchantLogin()){	    		
		    	$merchant_id=Yii::app()->functions->getMerchantID();	    
		    	Yii::app()->functions->updateOption("merchant_photo_bg","",$merchant_id);
		    	$this->code=1;$this->msg=Yii::t("default","Merchant background has been removed");
	    	} else $this->msg=Yii::t("default","ERROR: Your session has expired.");			
		}


	    public function getNewOrder()
	    {
	    	$list='';
	    	if ($res=Yii::app()->functions->newOrderList(1)){	    			    	
	    		$this->code=1;
	    		$this->msg=count($res);	    		
	    		//$this->details=$list;
	    		$order_list='';
	    		foreach ($res as $val) {	    			
	    			$order_list.="<div class=\"new-order-link\">";
	    			$order_list.="<a class=\"view-receipt\" data-id=\"$val[order_id]\" 
	    			href=\"javascript:;\">".t("Click here to view")." ". t("Reference #") .":". $val['order_id'] . "</a>";	    			
	    			$order_list.="<div>";
	    		}	    		
	    		$this->details=$order_list;
	    	} else $this->msg=Yii::t("default","No results");	    
	    }			
	    
	    public function initSelectPaymentProvider()
	    {	    		    	
	    	if (!isset($this->data['payment_opt'])){
	    		$this->msg=Yii::t("default","Please select Payment gateway.");
	    		return ;
	    	}	    
	    	
	    	$action='';
	    	$params='';
	    	switch ($this->data['payment_opt']) {
	    		case "pyp":
	    			$action="paypalInit";
	    			break;
	    		default:
	    			$action=$this->data['payment_opt']."init";	  
	    			break;
	    	}
	    	
	    	$this->code=1;
	    	$this->msg=t("Please wait while we redirect you");
	    	$this->details=Yii::app()->request->baseUrl."/".$this->data['controller']."/$action/?".$params;
	    }	
	    
		public function getLimitSellStatus()
		{
			$merchant_id=Yii::app()->functions->getMerchantID();			
			if ( !Yii::app()->functions->validateSellLimit($merchant_id)){
				$link="<a href=\"".Yii::app()->request->baseUrl."/merchant/MerchantStatus/"."\">".Yii::t("default","click here to upgrade")."</a>";
                $this->msg=Yii::t("default","You have reach the maximum limit of selling item. Please upgrade your membership.")." $link";
			} else $this->code=1;
		}
		
		public function MerchantUserList()
		{
			$merchant_id=Yii::app()->functions->getMerchantID();
		    $slug=$this->data['slug'];
	        $stmt="
			SELECT * FROM
			{{merchant_user}}
			WHERE						
			merchant_id=".Yii::app()->db->quoteValue($merchant_id)."
			ORDER BY merchant_user_id DESC
			";	        
			$connection=Yii::app()->db;
    	    $rows=$connection->createCommand($stmt)->queryAll();     	    
    	    if (is_array($rows) && count($rows)>=1){
    	    	foreach ($rows as $val) {    	     	    		
    	    		$chk="<input type=\"checkbox\" name=\"row[]\" value=\"$val[cook_id]\" class=\"chk_child\" >";     	    		
    	    		/*$date=date('M d,Y G:i:s',strtotime($val['date_created']));      	    		
$last_login=$val['last_login']=="0000-00-00 00:00:00"?"":date('M d,Y G:i:s',strtotime($val['last_login']));  */
    	    		
    	    		$last_login=FormatDateTime($val['last_login']);
    	    		
    	    		$status='';
    	    		if ( $val['status'] =="active"){
    	    			$status="<p class=\"uk-badge uk-badge-success\">".$val['status']."</p>";
    	    		} else $status="<p class=\"uk-badge uk-badge-danger\">".$val['status']."</p>";
    	    		
    	    		$action="<div class=\"options\">
    	    		<a href=\"$slug/id/$val[merchant_user_id]\" >".Yii::t("default","Edit")."</a>
    	    		<a href=\"javascript:;\" class=\"row_del\" rev=\"$val[merchant_user_id]\" >".Yii::t("default","Delete")."</a>
    	    		</div>";
    	    		
    	    		$last_login=Yii::app()->functions->translateDate($last_login);
    	    		$feed_data['aaData'][]=array(
    	    		  $chk,
    	    		  $val['first_name']." ".$val['last_name'].$action,
    	    		  $val['status'],
    	    		  $last_login,$val['ip_address']
    	    		);
    	    	}
    	    	$this->otableOutput($feed_data);
    	    }     	    
    	    $this->otableNodata();	
		}
		
		public function addMerchantUser()
		{
			$merchant_id=Yii::app()->functions->getMerchantID();		    
		    $params=array(
		      'merchant_id'=>$merchant_id,
		      'first_name'=>$this->data['first_name'],
		      'last_name'=>$this->data['last_name'],
		      'username'=>$this->data['username'],
		      //'password'=>md5($this->data['password']),
		      'user_access'=>json_encode($this->data['access']),
		      'date_created'=>date('c'),
		      'status'=>$this->data['status'],
		      'ip_address'=>$_SERVER['REMOTE_ADDR'],
		      'contact_email'=>$this->data['contact_email']
		    );	
		    		    
		    if (isset($this->data['password'])){
		    	if ( !empty($this->data['password'])){
		    	    $params['password']=md5($this->data['password']);
		    	}
		    }		
		    
		    /*dump($params);
		    die();
		    */
		    $FunctionsK=new FunctionsK;
		    if (empty($this->data['id'])){		
		    	if (empty($params['password'])){
		    		$this->msg="Password is required.";
		    		return ;
		    	}		   
		    			    	
		    	if ( $err_msg=$FunctionsK->validateMerchantUserAccount($this->data['username'],
		    	     $this->data['contact_email'])){
		    		 $this->msg=$err_msg;
		    		 return ;
		    	}		    		    	
		    	
		    	if ( !yii::app()->functions->validateMerchantUSername($this->data['username'])){	    	
			    	if ( $this->insertData("{{merchant_user}}",$params)){
			    		$this->details=Yii::app()->db->getLastInsertID();
			    		$this->code=1;
			    		$this->msg=Yii::t("default","Successful");			    		
			    	}
		    	} else $this->msg="Sorry your username is already exist. Please choose another username.";
		    } else {		    	
		    	unset($params['date_created']);
				$params['date_modified']=date('c');								
				
				if (!empty($params['username'])){
					if ( Yii::app()->functions->validateMerchantUser($params['username'],$merchant_id) ){
						$this->msg=Yii::t("default","Merchant Username is already been taken");
						return ;
					}
				}		    
								
				if ( $err_msg=$FunctionsK->validateMerchantUserAccount($this->data['username'],
		    	     $this->data['contact_email'],$this->data['id'])){
		    		 $this->msg=$err_msg;
		    		 return ;
		    	}
		    												
				$res = $this->updateData('{{merchant_user}}' , $params ,'merchant_user_id',$this->data['id']);
				if ($res){
					$this->code=1;
	                $this->msg=Yii::t("default",'Merchant User updated.');  
				} else $this->msg=Yii::t("default","ERROR: cannot update");
		    }			    
		}
		
		public function geoReverse()
		{			
			$url="http://maps.googleapis.com/maps/api/geocode/json?latlng=".$this->data['lat'].",".$this->data['lng']."&&sensor=false";			
			$resp=@file_get_contents($url);
			if ($resp){
				$resp=json_decode($resp,true);				
				$this->code=1;
				$this->msg="OK";
				$this->details=$resp['results'][0]['address_components'][0]['short_name'];
			} else $this->msg=Yii::t("default","Failed response from google API");
		}	
		
		public function customerReviews()
		{
			$merchant_id=Yii::app()->functions->getMerchantID();		    
			$slug=$this->data['slug'];
			$stmt="SELECT a.*,
			(
			select concat(first_name,' ',last_name)
			from {{client}}
			where
			client_id=a.client_id
			) client_name
			 FROM
			{{review}} a
			WHERE
			merchant_id='$merchant_id'
			ORDER BY id DESC
			";						
			if ($res=$this->rst($stmt)){
			   foreach ($res as $val) {				   	    			   	    
					$action="<div class=\"options\">
    	    		<a href=\"$slug/Do/Add/?id=$val[id]\" >".Yii::t("default","Edit")."</a>
    	    		<a href=\"javascript:;\" class=\"row_del\" rev=\"$val[id]\" >".Yii::t("default","Delete")."</a>
    	    		</div>";		   	  
					
					if ( $this->data['currentController']=="admin"){						
					} else {					
						if ( Yii::app()->functions->getOptionAdmin('merchant_can_edit_reviews')=="yes"){
							$action='';
						}
					}
					
				   /*$date=Yii::app()->functions->prettyDate($val['date_created']);	
				   $date=Yii::app()->functions->translateDate($date); */
				   $date=FormatDateTime($val['date_created']);
				   
			   	   $feed_data['aaData'][]=array(
			   	      ucwords($val['client_name']).$action,
			   	      $val['review'],
			   	      /*$val['order_id'],*/
			   	      $val['rating'],
			   	      $date."<br/><div class=\"uk-badge $class\">".strtoupper(Yii::t("default",$val['status']))."</div>"
			   	   );			       
			   }
			   $this->otableOutput($feed_data);
			}
			$this->otableNodata();	
		}
		
		public function UpdateCustomerReviews()
		{
			$db_ext=new DbExt;			
			if (isset($this->data['id'])){
				$params=array(
				  'review'=>$this->data['review'],
				  'status'=>$this->data['status'],
				  'ip_address'=>$_SERVER['REMOTE_ADDR']
				);
				if ($db_ext->updateData("{{review}}",$params,'id',$this->data['id'])){
					$this->code=1;
					$this->msg=Yii::t("default","Successful");
				} else $this->msg=Yii::t("default","ERROR: cannot update");
			} else $this->msg="";		
		}
		
		public function enterAddress()
		{
		   require_once "enter-address.php";
		}
		
		public function setAddress()
		{
			if (isset($this->data['client_address'])){				
				$_SESSION['kr_search_address']=$this->data['client_address'];
				if ($lat_res=Yii::app()->functions->geodecodeAddress($this->data['client_address'])){

					if($lat_res['lat']) {
						$_SESSION['client_location']=array(
						  'lat'=>$lat_res['lat'],
						  'long'=>$lat_res['long'],
						  'client_address'=>$this->data['client_address']
						);
						$this->msg=Yii::t("default","Successful");//.var_export($_SESSION['client_location'],true)
						$this->code=1;
					} else {
						$this->msg=Yii::t("default","Address not found ");//.var_export($_SESSION['client_location'],true)
					}
					
		    	} else {
					$this->msg=Yii::t("default","Address not found");		
				}
			} else $this->msg=Yii::t("default","Address is required");		
		}
		


		public function getGoogleCordinateStatus()
		{
			$merchant_id=Yii::app()->functions->getMerchantID();			
			$merchant_latitude=Yii::app()->functions->getOption("merchant_latitude",$merchant_id);
            $merchant_longitude=Yii::app()->functions->getOption("merchant_longitude",$merchant_id);            
            
            if (empty($merchant_longitude) || empty($merchant_latitude)){
            	$this->msg=Yii::t("default","Your merchant might not be searchable fixed this by adding coordinates on google map under merchant information");
            } else $this->code=1;
		}	
		
		
		public function customerList()
		{
			
			$slug=Yii::app()->request->baseUrl."/admin/".$_GET['slug'];
			$stmt="SELECT * FROM
			{{client}}		
			ORDER BY client_id DESC						
			";						
			$_SESSION['kr_export_stmt']=$stmt;
			
			if ($res=$this->rst($stmt)){
			   foreach ($res as $val) {				   	    			   	    
					$action="<div class=\"options\">
    	    		<a href=\"$slug/Do/Add/?id=$val[client_id]\" >".Yii::t("default","Edit")."</a>
    	    		<a href=\"javascript:;\" class=\"row_del\" rev=\"$val[client_id]\" >".Yii::t("default","Delete")."</a>
    	    		</div>";		   	  
				   /*$date=Yii::app()->functions->prettyDate($val['date_created']);	
				   $date=Yii::app()->functions->translateDate($date);	 */
				   $date=FormatDateTime($val['date_created']);
				   $address=$val['street']." ".$val['city']." ".$val['state']." ".$val['zipcode']." ".$val['country_code'];
			   	   $feed_data['aaData'][]=array(
			   	      $val['client_id'],
			   	      $val['first_name']." ".$val['last_name'].$action,
			   	      $val['email_address'],
			   	      $val['contact_phone'],
			   	      $address,
			   	      $date."<br/>".$val['status']
			   	   );			       
			   }
			   $this->otableOutput($feed_data);
			}
			$this->otableNodata();			    		    		
			
		}	
		public function custBalanceList()
		{
			$stmt="SELECT * FROM
			{{client}}		
			ORDER BY client_id DESC						
			";					
			$_SESSION['kr_export_stmt']=$stmt;
			
			if ($res=$this->rst($stmt)){
			   foreach ($res as $val) {	
				   $date=FormatDateTime($val['date_created']);
				   $address=$val['street']." ".$val['city']." ".$val['state']." ".$val['zipcode']." ".$val['country_code'];
			   	   $feed_data['aaData'][]=array(
			   	      $val['client_id'],
			   	      $val['first_name']." ".$val['last_name'],
			   	      $val['saldo'],
			   	   );			       
			   }
			   $this->otableOutput($feed_data);
			}
			$this->otableNodata();
		}	

		public function topupList()
		{
			// file_put_contents('topup.txt','get');
			$slug=Yii::app()->request->baseUrl."/admin/".$_GET['slug'];
			$stmt="SELECT a.*,b.first_name,b.last_name 
			FROM 
			{{topup_confirm}} a  
			JOIN {{client}} b 
			ON a.client_id=b.client_id
			ORDER BY status,id DESC 
			";
			$_SESSION['kr_export_stmt']=$stmt;
			
			if ($res=$this->rst($stmt)){
			   foreach ($res as $val) {
				   
				$actions="<a data-id=\"".$val['id']."\" class=\"edit-topup-status\" href=\"javascript:\">".Yii::t("default","Edit")."</a>"; 

					
				   /*$date=Yii::app()->functions->prettyDate($val['date_created']);	
				   $date=Yii::app()->functions->translateDate($date);	 */
				   $date=FormatDateTime($val['date_created']);
				   // $address=$val['street']." ".$val['city']." ".$val['state']." ".$val['zipcode']." ".$val['country_code'];
			   	   $feed_data['aaData'][]=array(
			   	      $val['id'],
			   	      $val['first_name']." ".$val['last_name']."<br/>".$actions,
			   	      $val['amount'],
					  '<b>'.t('from').'</b>: '.$val['from_bank']."<br/>".$val['from_account']."<br/><b>".t('to').'</b>: '.$val['to_bank']."<br/>".$val['to_account'],
			   	      $date."<br/>",$val['status']
			   	   );
			   }
			   // file_put_contents('topup.txt',var_export($feed_data,true));
			   $this->otableOutput($feed_data);
			}
			$this->otableNodata();			    		    		
			
		}	
		public function editTopupStatus()
		{   
		
	    	?>
	    	<div class="view-receipt-pop2">
	    	 <h3><?=t('Change Topup Status')?></h3>
	    	 <p ><?=t('Topup Transaction that has been approved or declined cannot changed in the future. After you approved topup transaction, the amount will be added to customer balance.')?></p>
			<?php if ( $res=query("	SELECT a.*,b.first_name,b.last_name 
									FROM {{topup_confirm}} a 
									JOIN {{client}} b ON b.client_id=a.client_id 
									WHERE id=?",array($this->data['id']))): 
									
						$res=$res[0];
									?>
				
	    	    <form id="frm-pop" class="frm-pop uk-form uk-form-horizontal" method="POST" onsubmit="return false;">
	    	    <?php echo CHtml::hiddenField('action','updateTopupStatus')?>
	    	    <?php echo CHtml::hiddenField('id',$this->data['id'])?>
				
				<div class="uk-grid">
				<div class="uk-width-1-3">
					<div class="uk-form-row">
					<label class="uk-form-label"><?=t('Customer')?> </label>
					<p class="uk-form-label frm-name"><b><?=$res['first_name'].' '.$res['last_name'] ?></b></p>
					</div>
					
					<div class="uk-form-row">
					<label class="uk-form-label"><?=t('Amount')?> </label>
					<p class="uk-form-label frm-amount"><b><?=$res['amount']?></b></p>
					</div>
					
					<div class="uk-form-row">
					  <label class="uk-form-label"><?php echo Yii::t("default","Admin Message")?></label>
					  <?php 
					  echo CHtml::textField('admin_message',
					  isset($res['admin_message'])?$res['admin_message']:""
					  ,array('class'=>"uk-form-width-large"))
					  ?>
					</div>

					<div class="uk-form-row">
					<label class="uk-form-label"><?php echo Yii::t("default","Status")?></label>
					<?php echo CHtml::dropDownList('status',$res['status'],statusList3(),array(
					'class'=>"uk-form-width-large"
					))?>
					</div>
					
				</div> 
				<div class="uk-width-2-3">
					<div class="uk-form-row">
					<label class="uk-form-label"><?=t('Screenshot Proof of Payment')?> </label>
					<?php if(!empty($res['image'])) { ?>
					<img style='width:100% !important' src="<?=uploadURL().'/'.$res['image']?>" alt="" title="" class="uk-thumbnail " >
					<?php } ?>
					</div>
				</div>

		    	 
		    	 <div class="action-wrap">
		    	   <?php echo CHtml::submitButton('Submit',
		    	   array('class'=>"uk-button uk-form-width-medium uk-button-success"))?>
		    	 </div>
	    	   </form> 
	    	 <?php else:?>
	    	 <p class="uk-text-danger"><?php echo Yii::t("default","Error: Order not found")?></p>
	    	 <?php endif;?>
	    	</div> <!--view-receipt-pop-->	    					    
			<script type="text/javascript">
			$.validate({ 	
			    form : '#frm-pop',    
			    onError : function() {      
			    },
			    onSuccess : function() {     
			      form_submit('frm-pop');
			      return false;
			    }  
			});		
			</script>
	    	<?php
	    	die();
		}
		
		public function updateTopupStatus()
		{
			if ( $res=query("	SELECT a.status,a.amount,b.client_id,b.saldo 
								FROM {{topup_confirm}} a 
								JOIN {{client}} b ON b.client_id=a.client_id 
								WHERE id=?",array($this->data['id']))) {
				$res = $res[0];
				if($res['status'] !== 'approved' && $res['status'] !== 'declined') {
					$params=array(
					  'admin_message'=>$this->data['admin_message'],
					  'status'=>$this->data['status'],
					  'date_modified'=>date('c')
					);
					$command = Yii::app()->db->createCommand();
					
					$res2 = $command->update('{{topup_confirm}}' , $params , 
					'id=:id' , array(':id'=>addslashes($this->data['id'])));
					
					if($this->data['status'] == 'approved'){
						//saldo is added 
						$saldo = $res['saldo'] + $res['amount'];
						$params=array(
						  'saldo'=>$saldo,
						);
						$res3 = $command->update('{{client}}' , $params , 
						'client_id=:client_id' , array(':client_id'=>addslashes($res['client_id'])));
					} else {$res3 = true;}
					
					if ($res2 && $res3) {
						$this->code=1;
						$this->msg=Yii::t("default",'Status updated.');
					} else $this->msg=Yii::t("default","ERROR: cannot update");
					
				} else $this->msg=Yii::t("default","You cannot edit topup that is approved or declined");
			}
		}
		
	
		public function customerAdd()
		{			
			$params=array(
			  'first_name'=>addslashes($this->data['first_name']),
			  'last_name'=>addslashes($this->data['last_name']),
			  'email_address'=>$this->data['email_address'],
			  'street'=>$this->data['street'],
			  'city'=>$this->data['city'],
			  'state'=>$this->data['state'],
			  'zipcode'=>$this->data['zipcode'],
			  'status'=>$this->data['status'],
			  'date_created'=>date('c'),
			  'ip_address'=>$_SERVER['REMOTE_ADDR']			  
			);		
			if (isset($this->data['password'])){
				if (!empty($this->data['password'])){
					$params['password']=md5($this->data['password']);
				}
			}			
						
           /** update 2.3*/
	    	if (isset($this->data['custom_field1'])){
	    		$params['custom_field1']=!empty($this->data['custom_field1'])?$this->data['custom_field1']:'';
	    	}
	    	if (isset($this->data['custom_field2'])){
	    		$params['custom_field2']=!empty($this->data['custom_field2'])?$this->data['custom_field2']:'';
	    	}	    			    
			
			$command = Yii::app()->db->createCommand();
			if (isset($this->data['id']) && is_numeric($this->data['id'])){				
				unset($params['date_created']);
				$params['date_modified']=date('c');				
				$res = $command->update('{{client}}' , $params , 
				'client_id=:client_id' , array(':client_id'=>addslashes($this->data['id'])));
				if ($res){
					$this->code=1;
	                $this->msg=Yii::t("default",'Client updated.');  
				} else $this->msg=Yii::t("default","ERROR: cannot update");
			} else {				
				if ($res=$command->insert('{{client}}',$params)){
					$this->details=Yii::app()->db->getLastInsertID();	
	                $this->code=1;
	                $this->msg=Yii::t("default",'Client added.');  	                
	            } else $this->msg=Yii::t("default",'ERROR. cannot insert data.');
			}
			
		}
		
		public function bookingAlertSettings()
		{		
			$merchant_id=Yii::app()->functions->getMerchantID();
						
			Yii::app()->functions->updateOption("merchant_booking_alert",
	    	isset($this->data['merchant_booking_alert'])?$this->data['merchant_booking_alert']:''
	    	,$merchant_id);
	    	
	    	Yii::app()->functions->updateOption("merchant_booking_approved_tpl",
	    	isset($this->data['merchant_booking_approved_tpl'])?$this->data['merchant_booking_approved_tpl']:''
	    	,$merchant_id);
	    	
	    	Yii::app()->functions->updateOption("merchant_booking_denied_tpl",
	    	isset($this->data['merchant_booking_denied_tpl'])?$this->data['merchant_booking_denied_tpl']:''
	    	,$merchant_id);
	    	
	    	Yii::app()->functions->updateOption("merchant_booking_subject",
	    	isset($this->data['merchant_booking_subject'])?$this->data['merchant_booking_subject']:''
	    	,$merchant_id);
	    	
	    	Yii::app()->functions->updateOption("merchant_booking_sender",
	    	isset($this->data['merchant_booking_sender'])?$this->data['merchant_booking_sender']:''
	    	,$merchant_id);
	    	
	    	Yii::app()->functions->updateOption("merchant_booking_receiver",
	    	isset($this->data['merchant_booking_receiver'])?$this->data['merchant_booking_receiver']:''
	    	,$merchant_id);
	    	
	    	Yii::app()->functions->updateOption("merchant_booking_tpl",
	    	isset($this->data['merchant_booking_tpl'])?$this->data['merchant_booking_tpl']:''
	    	,$merchant_id);
	    	
	    	Yii::app()->functions->updateOption("merchant_booking_receive_subject",
	    	isset($this->data['merchant_booking_receive_subject'])?$this->data['merchant_booking_receive_subject']:''
	    	,$merchant_id);
	    	
	    	Yii::app()->functions->updateOption("max_booked",
	    	isset($this->data['max_booked'])?json_encode($this->data['max_booked']):''
	    	,$merchant_id);
	    	
	    	Yii::app()->functions->updateOption("fully_booked_msg",
	    	isset($this->data['fully_booked_msg'])?$this->data['fully_booked_msg']:''
	    	,$merchant_id);	    	
	    	
	    	Yii::app()->functions->updateOption("accept_booking_sameday",
	    	isset($this->data['accept_booking_sameday'])?$this->data['accept_booking_sameday']:''
	    	,$merchant_id);	    	
	    	
	    	Yii::app()->functions->updateOption("merchant_table_booking",
	    	isset($this->data['merchant_table_booking'])?$this->data['merchant_table_booking']:''
	    	,$merchant_id);	    	
	    	
	    	$this->code=1;
	    	$this->msg=Yii::t("default","Settings saved.");	
		}
			
		
		public function rptIncomingOrders()
		{		
			$and='';  
	    	if (isset($this->data['start_date']) && isset($this->data['end_date']))	{
	    		if (!empty($this->data['start_date']) && !empty($this->data['end_date'])){
	    		$and=" AND date_created BETWEEN  '".$this->data['start_date']." 00:00:00' AND 
	    		        '".$this->data['end_date']." 23:59:00'
	    		 ";
	    		}
	    	}
	    	
	    	$order_status_id='';
	    	$or='';
	    	if (isset($this->data['stats_id'])){
		    	if (is_array($this->data['stats_id']) && count($this->data['stats_id'])>=1){
		    		foreach ($this->data['stats_id'] as $stats_id) {		    			
		    			$order_status_id.="'$stats_id',";
		    		}
		    		if ( !empty($order_status_id)){
		    			$order_status_id=substr($order_status_id,0,-1);
		    		}		    	
		    	}	    
	    	}
	    	
	    	if ( !empty($order_status_id)){	    		
	    		$and.= " AND status IN ($order_status_id)";
	    	}	    	    	
	    	 
	    	//dump($and);	    	
	    	
	    	$DbExt=new DbExt;
	    	$merchant_id=Yii::app()->functions->getMerchantID();	    
	    	$stmt="SELECT a.*,
	    	(
	    	select concat(first_name,' ',last_name)
	    	from
	    	{{client}}
	    	where
	    	client_id=a.client_id
	    	) as client_name,
	    	
	    	(
	    	select group_concat(item_name)
	    	from
	    	{{order_details}}
	    	where
	    	order_id=a.order_id
	    	) as item,
	    	
	    	(
	    	select merchant_name
	    	from
	    	{{merchant}}
	    	where
	    	merchant_id=a.merchant_id
	    	) as merchant_name
	    	
	    	FROM
	    	{{order}} a	    	
	    	
	    	WHERE a.status NOT IN ('".initialStatus()."')
	    	
	    	ORDER BY order_id DESC
	    	LIMIT 0,100
	    	";
	    		    		    		   
	    	if ( $res=$DbExt->rst($stmt)){
	    		foreach ($res as $val) {	
	    			
	    			$merchant_id=$val['merchant_id'];
	    			
	    			$action="<a data-id=\"".$val['order_id']."\" class=\"edit-order\" href=\"javascript:\">".Yii::t("default","Edit")."</a>";
	    			$action="<br/><a data-id=\"".$val['order_id']."\" class=\"view-receipt\" href=\"javascript:\">".Yii::t("default","View")."</a>";
	    			$date=FormatDateTime($val['date_created']);
	    			$date=Yii::app()->functions->translateDate($date);
	    			
	    			$item=FunctionsV3::translateFoodItemByOrderId(
	    			  $val['order_id'],
	    			  'kr_admin_lang_id'
	    			);
	    			
	    			$feed_data['aaData'][]=array(
	    			  $val['order_id'],
	    			  ucwords($val['merchant_name']),
	    			  ucwords($val['client_name']),
	    			  $item,
	    			  ucwords(Yii::t("default",$val['trans_type'])),
	    			  strtoupper(Yii::t("default",$val['payment_type'])),
	    			  prettyFormat($val['sub_total'],$merchant_id),
	    			  prettyFormat($val['taxable_total'],$merchant_id),
	    			  prettyFormat($val['total_w_tax'],$merchant_id),
	    			  ucwords($val['status']),
	    			  $date.$action,
	    			  $action
	    		    );
	    		}
	    		$this->otableOutput($feed_data);
	    	}	   
	    	$this->otableNodata();
		}	
		
		public function rptAdminSalesRpt()
		{
	    	$and='';  
	    	if (isset($this->data['start_date']) && isset($this->data['end_date']))	{
	    		if (!empty($this->data['start_date']) && !empty($this->data['end_date'])){
	    		$and=" AND a.date_created BETWEEN  '".$this->data['start_date']." 00:00:00' AND 
	    		        '".$this->data['end_date']." 23:59:00'
	    		 ";
	    		}
	    	}
	    	
	    	$order_status_id='';
	    	$or='';
	    	if (isset($this->data['stats_id'])){
		    	if (is_array($this->data['stats_id']) && count($this->data['stats_id'])>=1){
		    		foreach ($this->data['stats_id'] as $stats_id) {		    			
		    			$order_status_id.="'$stats_id',";
		    		}
		    		if ( !empty($order_status_id)){
		    			$order_status_id=substr($order_status_id,0,-1);
		    		}
		    	}
	    	}
	    	
	    	if ( !empty($order_status_id)){	    		
	    		$and.= " AND a.status IN ($order_status_id)";
	    	}	    	    	
	    	 	    	
	    	$DbExt=new DbExt;
			
	    	$merchant_id= isset($this->data['merchant_id'])?$this->data['merchant_id']:'';
	    	if(isset($this->data['merchant_id']) && $this->data['merchant_id'] != 0) {
				$filter_merchant = " AND b.merchant_id='$merchant_id'";
			} else {
				$filter_merchant = "";
			}
			
	    	$stmt="SELECT a.*,
	    	(
	    	select concat(first_name,' ',last_name)
	    	from
	    	{{client}}
	    	where
	    	client_id=a.client_id
	    	) as client_name,
			c.merchant_name,b.earning,b.merchant_id,b.part_status
	    	
	    	FROM
	    	{{order}} a
			JOIN {{order_merchant}} b
			ON b.order_id=a.order_id
			JOIN {{merchant}} c
			ON c.merchant_id=b.merchant_id
	    	WHERE
	    	a.status NOT IN ('".initialStatus()."')	    	
	    	$and
			$filter_merchant
	    	ORDER BY a.order_id DESC
	    	LIMIT 0,2000
	    	";
	    	/*dump($this->data);
	    	dump($stmt);*/
	    	
	    	$_SESSION['kr_export_stmt']=$stmt;	    	
	    		    		    	
	    	if ( $res=$DbExt->rst($stmt)){
	    		foreach ($res as $val) {	 
				
	    			$action="<a data-mtid=\"".$val['merchant_id']."\" data-id=\"".$val['order_id']."\" class=\"edit-order-admin\" href=\"javascript:\">".Yii::t("default","Edit")."</a>";
	    			$action.="<a data-id=\"".$val['order_id']."\" class=\"view-receipt\" href=\"javascript:\">".Yii::t("default","View")."</a>";
					// $action.="<a data-id=\"".$val['order_id']."\" class=\"view-order-history\" href=\"javascript:\">".Yii::t("default","History")."</a>";
	    			/*$date=prettyDate($val['date_created'],true);
	    			$date=Yii::app()->functions->translateDate($date);*/
	    			$date=FormatDateTime($val['date_created']);
	    			
	    			$feed_data['aaData'][]=array(
	    			  $val['order_id'],
					  $val['merchant_name'],
	    			  ucwords($val['client_name']),
	    			  strtoupper(Yii::t("default",$val['payment_type'])),
	    			  prettyFormat($val['earning']),
	    			  ucwords($val['status']),
					  ucwords($val['part_status']),
	    			  $date,
	    			  $action
	    		    );
	    		}
	    		$this->otableOutput($feed_data);
	    	}	   
	    	$this->otableNodata();			
		}


		public function rptAdminListOrder()
		{
	    	$and='';  
	    	if (isset($this->data['start_date']) && isset($this->data['end_date']))	{
	    		if (!empty($this->data['start_date']) && !empty($this->data['end_date'])){
	    		$and=" AND date_created BETWEEN  '".$this->data['start_date']." 00:00:00' AND 
	    		        '".$this->data['end_date']." 23:59:00'
	    		 ";
	    		}
	    	}
	    	
	    	$order_status_id='';
	    	$or='';
	    	if (isset($this->data['stats_id'])){
		    	if (is_array($this->data['stats_id']) && count($this->data['stats_id'])>=1){
		    		foreach ($this->data['stats_id'] as $stats_id) {		    			
		    			$order_status_id.="'$stats_id',";
		    		}
		    		if ( !empty($order_status_id)){
		    			$order_status_id=substr($order_status_id,0,-1);
		    		}		    	
		    	}	    
	    	}
	    	
	    	if ( !empty($order_status_id)){	    		
	    		$and.= " AND status IN ($order_status_id)";
	    	}	    	    	
	    	 	    	
	    	$DbExt=new DbExt;
	    	$merchant_id= isset($this->data['merchant_id'])?$this->data['merchant_id']:'';
	    	
	    	$stmt="SELECT a.*,
	    	(
	    	select concat(first_name,' ',last_name)
	    	from
	    	{{client}}
	    	where
	    	client_id=a.client_id
	    	) as client_name,
	    	
	    	(
	    	select group_concat(item_name)
	    	from
	    	{{order_details}}
	    	where
	    	order_id=a.order_id
	    	) as item
	    	
	    	FROM
	    	{{order}} a
	    	WHERE
	    	merchant_id='$merchant_id'
	    	AND status NOT IN ('".initialStatus()."')	    	
	    	$and
	    	ORDER BY order_id DESC
	    	LIMIT 0,2000
	    	";
	    	/*dump($this->data);
	    	dump($stmt);*/
	    	
	    	$_SESSION['kr_export_stmt']=$stmt;	    	
	    		    		    	
	    	if ( $res=$DbExt->rst($stmt)){
	    		foreach ($res as $val) {	    			    			
	    			$action="<a data-id=\"".$val['order_id']."\" class=\"edit-order\" href=\"javascript:\">".Yii::t("default","Edit")."</a>";
	    			$action.="<a data-id=\"".$val['order_id']."\" class=\"view-receipt\" href=\"javascript:\">".Yii::t("default","View")."</a>";
	    			/*$date=prettyDate($val['date_created'],true);
	    			$date=Yii::app()->functions->translateDate($date);*/
	    			$date=FormatDateTime($val['date_created']);
	    			
	    			$feed_data['aaData'][]=array(
	    			  $val['order_id'],
	    			  ucwords($val['client_name']),
	    			  $val['item'],
	    			  ucwords(Yii::t("default",$val['trans_type'])),
	    			  strtoupper(Yii::t("default",$val['payment_type'])),
	    			  prettyFormat($val['sub_total'],$merchant_id),
	    			  prettyFormat($val['tax'],$merchant_id),
	    			  prettyFormat($val['total_w_tax'],$merchant_id),
	    			  ucwords($val['status']),
	    			  $date
	    			  //$action
	    		    );
	    		}
	    		$this->otableOutput($feed_data);
	    	}	   
	    	$this->otableNodata();			
		}
		


		public function emailSettings()
		{
			if (!isset($this->data['email_provider'])){
				$this->msg=t("please select email provider");
				return ;
			}
			Yii::app()->functions->updateOptionAdmin("smtp_host",
	    	isset($this->data['smtp_host'])?$this->data['smtp_host']:'');
	    	
	    	Yii::app()->functions->updateOptionAdmin("smtp_port",
	    	isset($this->data['smtp_port'])?$this->data['smtp_port']:'');
	    	
	    	Yii::app()->functions->updateOptionAdmin("smtp_username",
	    	isset($this->data['smtp_username'])?$this->data['smtp_username']:'');
	    	
	    	Yii::app()->functions->updateOptionAdmin("smtp_password",
	    	isset($this->data['smtp_password'])?$this->data['smtp_password']:'');
	    	
	    	Yii::app()->functions->updateOptionAdmin("email_provider",
	    	isset($this->data['email_provider'])?$this->data['email_provider']:'');
	    
			Yii::app()->functions->updateOptionAdmin("email_tpl_activation",
	    	isset($this->data['email_tpl_activation'])?$this->data['email_tpl_activation']:'');
	    	
	    	Yii::app()->functions->updateOptionAdmin("email_tpl_forgot",
	    	isset($this->data['email_tpl_forgot'])?$this->data['email_tpl_forgot']:'');
	    	
	    	Yii::app()->functions->updateOptionAdmin("email_tpl_customer_reg",
	    	isset($this->data['email_tpl_customer_reg'])?$this->data['email_tpl_customer_reg']:'');
	    	
	    	Yii::app()->functions->updateOptionAdmin("email_tpl_customer_subject",
	    	isset($this->data['email_tpl_customer_subject'])?$this->data['email_tpl_customer_subject']:'');

	    	Yii::app()->functions->updateOptionAdmin("mandrill_api_key",
	    	isset($this->data['mandrill_api_key'])?$this->data['mandrill_api_key']:'');
	    	
	    	$this->code=1;
	    	$this->msg=Yii::t("default","Setting saved");		  
		}
				
		public function gallerySettings()
		{			
			$merchant_id=Yii::app()->functions->getMerchantID();
						
			Yii::app()->functions->updateOption("merchant_gallery",
	    	isset($this->data['photo'])?json_encode($this->data['photo']):''
	    	,$merchant_id);
	    	
	    	Yii::app()->functions->updateOption("gallery_disabled",
	    	isset($this->data['gallery_disabled'])?$this->data['gallery_disabled']:''
	    	,$merchant_id);
	    	
	    	$this->code=1;
	    	$this->msg=Yii::t("default","Setting saved");
		}						
		
	    public function adminForgotPass()
	    {	    	
	    	if ( isset($this->data['email_address'])){
	    		if ($res=yii::app()->functions->isAdminExist($this->data['email_address']) ){	    			
	    			$new_pass=yii::app()->functions->generateCode();
	    			$params=array(
	    			  'lost_password_code'=> $new_pass,
	    			  'password'=>md5($new_pass)
	    			  );	    			
	    			if ( $this->updateData("{{admin_user}}",$params,'admin_id',$res[0]['admin_id'])){
	    				$this->code=1;
	    				$this->msg=Yii::t("default","An email address was sent to your.");		    				
	    				$tpl=EmailTPL::adminForgotPassword($new_pass);	    					    			
	    				$sender=Yii::app()->functions->getOptionAdmin('website_contact_email');
		                $to=$res[0]['email_address'];		                
		                if (!sendEmail($to,$sender,t("Admin Forgot Password"),$tpl)){		    	
		                	$this->details="failed $new_pass";
		                } else $this->details="ok mail $new_pass";
	    				
	    			} else $this->msg=Yii::t("default","ERROR: Cannot update.");	    		
	    		} else $this->msg=Yii::t("default","Sorry but we cannot find your email address.");
	    	} else $this->msg=Yii::t("default","Email address is required");	    
	    }	    	
	    
		public function paymentProviderList()
		{
		    $slug=$this->data['slug'];
			$stmt="SELECT * FROM
			{{payment_provider}}
			ORDER BY id DESC
			";
			if ($res=$this->rst($stmt)){
			   foreach ($res as $val) {				   	    			   	    
					$action="<div class=\"options\">
    	    		<a href=\"$slug/Do/Add/?id=$val[id]\" >".Yii::t("default","Edit")."</a>
    	    		<a href=\"javascript:;\" class=\"row_del\" rev=\"$val[id]\" >".Yii::t("default","Delete")."</a>
    	    		</div>";		   	   
				   /*$date=Yii::app()->functions->prettyDate($val['date_created']);
				   $date=Yii::app()->functions->translateDate($date);*/
				   $date=FormatDateTime($val['date_created']);
			   	   $feed_data['aaData'][]=array(
			   	      $val['id'],
			   	      $val['payment_name'].$action,
			   	      '<img src="'.uploadURL()."/".$val['payment_logo'].'" class="uk-thumbnail uk-thumbnail-mini" >',
			   	      $date."<br/>".$val['status']
			   	   );			       
			   }
			   $this->otableOutput($feed_data);
			}
			$this->otableNodata();
		}
		

		
		public function merchantOffers()
		{
			$merchant_id=Yii::app()->functions->getMerchantID();		    
			$slug=$this->data['slug'];
			$stmt="SELECT * FROM
			{{offers}}
			WHERE
			merchant_id=".$this->q($merchant_id)."
			ORDER BY offers_id DESC
			";						
			if ($res=$this->rst($stmt)){
			   foreach ($res as $val) {				   	    			   	    
					$action="<div class=\"options\">
    	    		<a href=\"$slug/Do/Add/?id=$val[offers_id]\" >".Yii::t("default","Edit")."</a>
    	    		<a href=\"javascript:;\" class=\"row_del\" rev=\"$val[offers_id]\" >".Yii::t("default","Delete")."</a>
    	    		</div>";		   	  
					
				   /*$date=Yii::app()->functions->prettyDate($val['date_created']);	
				   $date=Yii::app()->functions->translateDate($date); */
				   $date=FormatDateTime($val['date_created']);
				   
			   	   $feed_data['aaData'][]=array(
			   	      $val['offers_id'],
			   	      standardPrettyFormat($val['offer_percentage'])." %".$action,
			   	      standardPrettyFormat($val['offer_price']),
			   	      FormatDateTime($val['valid_from'],false),
			   	      FormatDateTime($val['valid_to'],false),
			   	      
			   	      $date."<br/><div class=\"uk-badge $class\">".strtoupper(Yii::t("default",$val['status']))."</div>"
			   	   );			       
			   }
			   $this->otableOutput($feed_data);
			}
			$this->otableNodata();	
		}		
		
		public function q($data='')
		{
			return Yii::app()->db->quoteValue($data);
		}	
		
	    public function sendEmailMerchant()
	    {
	    	require_once 'sendemail-merchant.php';
	    	die();
	    }	
	    
	    public function sendEmailToMerchant()
	    {	    	
	    	if (isset($this->data['id'])){
	    		if ( $res=Yii::app()->functions->getMerchant($this->data['id'])){	    			
	    			$tpl=$this->data['email_content'];
	    			$tpl=smarty('merchant_name',$res['merchant_name'],$tpl);
	    			$tpl=smarty('status',$res['status'],$tpl);
	    			$tpl=smarty('owner_name',$res['contact_name'],$tpl);
	    			$tpl=smarty('website_title',Yii::app()->functions->getOptionAdmin("website_title"),$tpl);	    			
	    			$merchant_email=$res['contact_email'];	    			
	    			if ( !empty($merchant_email)){
	    				$from=getAdminGlobalSender();
	    				if (sendEmail($merchant_email,$from,$this->data['subject'],$tpl)){
	    					$this->code=1;
	    				    $this->msg=t("Email has been sent");
	    				} else $this->msg=t("Failed sending email");
	    			} else $this->msg=t("Merchant has no email address provided");	    		
	    		} else $this->msg=t("Merchant information not found");
	    	} else $this->msg=t("Missing merchant id");
	    }
	    

		public function addCustomPageLink()
		{		
			$params=array(
	    	  'page_name'=>$this->data['page_name'],
	    	  'content'=>$this->data['content'],	    	  
	    	  'date_created'=>date('c'),
	    	  'ip_address'=>$_SERVER['REMOTE_ADDR'],
	    	  'status'=>$this->data['status'],
	    	  'is_custom_link'=>2,
	    	  'open_new_tab'=>isset($this->data['open_new_tab'])?$this->data['open_new_tab']:1
	    	);
	    		    	
	    	if (empty($this->data['id'])){		    		
		    	if ( $this->insertData("{{custom_page}}",$params)){
		    		$this->details=Yii::app()->db->getLastInsertID();
			    		$this->code=1;
			    		$this->msg=Yii::t("default","Successful");			    		
			    	}
			    } else {					    	
			    	unset($params['date_created']);
					$params['date_modified']=date('c');				
					$res = $this->updateData('{{custom_page}}' , $params ,'id',$this->data['id']);
					if ($res){
						$this->code=1;
		                $this->msg=Yii::t("default",'Page updated.');  
				} else $this->msg=Yii::t("default","ERROR: cannot update");
		    }		    		
		}	
		

		
	    public function saveMerchantBarclaySettings()
		{
			
			$merchant_id=Yii::app()->functions->getMerchantID();
						
			Yii::app()->functions->updateOption("merchant_enabled_barclay",
	    	isset($this->data['merchant_enabled_barclay'])?$this->data['merchant_enabled_barclay']:'',$merchant_id);
	    	
	    	Yii::app()->functions->updateOption("merchant_mode_barclay",
	    	isset($this->data['merchant_mode_barclay'])?$this->data['merchant_mode_barclay']:'',$merchant_id);
	    	
	    	Yii::app()->functions->updateOption("merchant_sandbox_barclay_pspid",
	    	isset($this->data['merchant_sandbox_barclay_pspid'])?$this->data['merchant_sandbox_barclay_pspid']:'',$merchant_id);
	    	
	    	Yii::app()->functions->updateOption("merchant_sandbox_barclay_password",
	    	isset($this->data['merchant_sandbox_barclay_password'])?$this->data['merchant_sandbox_barclay_password']:'',
	    	$merchant_id);
	    	
	    	Yii::app()->functions->updateOption("merchant_live_barclay_pspid",
	    	isset($this->data['merchant_live_barclay_pspid'])?$this->data['merchant_live_barclay_pspid']:'',
	    	$merchant_id);
	    	
	    	Yii::app()->functions->updateOption("merchant_live_barclay_password",
	    	isset($this->data['merchant_live_barclay_password'])?$this->data['merchant_live_barclay_password']:'',
	    	$merchant_id);
	    	
	    	Yii::app()->functions->updateOption("merchant_bcy_currency",
	    	isset($this->data['merchant_bcy_currency'])?$this->data['merchant_bcy_currency']:'',
	    	$merchant_id);
	    	
	    	Yii::app()->functions->updateOption("merchant_bcy_language",
	    	isset($this->data['merchant_bcy_language'])?$this->data['merchant_bcy_language']:'',$merchant_id);
	    	
	    	Yii::app()->functions->updateOption("merchant_bcy_font",
	    	isset($this->data['merchant_bcy_font'])?$this->data['merchant_bcy_font']:'',$merchant_id);
	    	
	    	Yii::app()->functions->updateOption("merchant_bcy_language",
	    	isset($this->data['merchant_bcy_language'])?$this->data['merchant_bcy_language']:'',$merchant_id);
	    	
	    	Yii::app()->functions->updateOption("merchant_bcy_bgcolor",
	    	isset($this->data['merchant_bcy_bgcolor'])?$this->data['merchant_bcy_bgcolor']:'',$merchant_id);
	    	
	    	Yii::app()->functions->updateOption("merchant_bcy_buttontextcolor",
	    	isset($this->data['merchant_bcy_buttontextcolor'])?$this->data['merchant_bcy_buttontextcolor']:'',
	    	$merchant_id);
	    	
	    	Yii::app()->functions->updateOption("merchant_bcy_table_bgcolor",
	    	isset($this->data['merchant_bcy_table_bgcolor'])?$this->data['merchant_bcy_table_bgcolor']:'',
	    	$merchant_id);
	    	
	    	Yii::app()->functions->updateOption("merchant_bcy_table_textcolor",
	    	isset($this->data['merchant_bcy_table_textcolor'])?$this->data['merchant_bcy_table_textcolor']:'',
	    	$merchant_id);
	    	
	    	Yii::app()->functions->updateOption("merchant_bcy_textcolor",
	    	isset($this->data['merchant_bcy_textcolor'])?$this->data['merchant_bcy_textcolor']:'',$merchant_id);
	    	
	    	Yii::app()->functions->updateOption("merchant_bcy_title",
	    	isset($this->data['merchant_bcy_title'])?$this->data['merchant_bcy_title']:'',$merchant_id);
	    	
	    	Yii::app()->functions->updateOption("merchant_bcy_buttoncolor",
	    	isset($this->data['merchant_bcy_buttoncolor'])?$this->data['merchant_bcy_buttoncolor']:'',$merchant_id);
	    	
	    	Yii::app()->functions->updateOption("merchant_bcy_listype",
	    	isset($this->data['merchant_bcy_listype'])?$this->data['merchant_bcy_listype']:'',$merchant_id);
	    	
	    	Yii::app()->functions->updateOption("merchant_bcy_logo",
	    	isset($this->data['merchant_bcy_logo'])?$this->data['merchant_bcy_logo']:'',$merchant_id);
	    	
	    	$this->code=1;
	    	$this->msg=Yii::t("default","Setting saved");
	    	
		}		
		

		
	} /*END AjaxAdmin*/			
}/* END CLASS*/