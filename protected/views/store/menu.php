<style>
.top-menu-wrapper {
    position: inherit;
	color:black;
}
#menu a {
    color: #ffffff;
}
.logo-desktop {
	display:block;
}
</style>
<?php
$gallery=Yii::app()->functions->getOption("merchant_gallery",$merchant_id);
$gallery=!empty($gallery)?json_decode($gallery):false;

$merchant_photo_bg=getOption($merchant_id,'merchant_photo_bg');
if ( !file_exists(FunctionsV3::uploadPath()."/$merchant_photo_bg")){
	$merchant_photo_bg='';
} 
// dump($merchant_photo_bg);
/*RENDER MENU HEADER FILE*/
$ratings=Yii::app()->functions->getRatings($merchant_id);   
$merchant_info=array(   
  'merchant_id'=>$merchant_id ,
  'minimum_order'=>$data['minimum_order'],
  'ratings'=>$ratings,
  'merchant_address'=>$data['merchant_address'],
  'cuisine'=>$data['cuisine'],
  'merchant_name'=>$data['merchant_name'],
  'background'=>$merchant_photo_bg,
  'merchant_website'=>$merchant_website,
  'merchant_logo'=>FunctionsV3::getMerchantLogo($merchant_id)
);
// $this->renderPartial('/front/menu-header',$merchant_info);

/* MERCHANT INFO  JSON */
$cs = Yii::app()->getClientScript();
$cs->registerScript(
  'merchant_information',
  "var merchant_information =".json_encode($merchant_info)."",
  CClientScript::POS_HEAD
);

/*PROGRESS ORDER BAR*/
// $this->renderPartial('/front/order-progress-bar',array(
   // 'step'=>3,
   // 'show_bar'=>true
// ));

$now=date('Y-m-d');
$now_time='';

$checkout=FunctionsV3::isMerchantcanCheckout($merchant_id); 
$menu=Yii::app()->functions->getMerchantMenu($merchant_id); 

echo CHtml::hiddenField('is_merchant_open',isset($checkout['code'])?$checkout['code']:'' );

/*hidden TEXT*/
echo CHtml::hiddenField('merchant_slug',$data['merchant_slug']);
echo CHtml::hiddenField('merchant_id',$merchant_id);
echo CHtml::hiddenField('is_client_login',Yii::app()->functions->isClientLogin());

echo CHtml::hiddenField('website_disbaled_auto_cart',
Yii::app()->functions->getOptionAdmin('website_disbaled_auto_cart'));

$hide_foodprice=Yii::app()->functions->getOptionAdmin('website_hide_foodprice');
echo CHtml::hiddenField('hide_foodprice',$hide_foodprice);

echo CHtml::hiddenField('accept_booking_sameday',getOption($merchant_id
,'accept_booking_sameday'));

echo CHtml::hiddenField('customer_ask_address',getOptionA('customer_ask_address'));

echo CHtml::hiddenField('merchant_required_delivery_time',
  Yii::app()->functions->getOption("merchant_required_delivery_time",$merchant_id));   
  
/** add minimum order for pickup status*/
$merchant_minimum_order_pickup=Yii::app()->functions->getOption('merchant_minimum_order_pickup',$merchant_id);
if (!empty($merchant_minimum_order_pickup)){
	  echo CHtml::hiddenField('merchant_minimum_order_pickup',$merchant_minimum_order_pickup);
	  
	  echo CHtml::hiddenField('merchant_minimum_order_pickup_pretty',
         displayPrice(baseCurrency(),prettyFormat($merchant_minimum_order_pickup)));
}
 
$merchant_maximum_order_pickup=Yii::app()->functions->getOption('merchant_maximum_order_pickup',$merchant_id);
if (!empty($merchant_maximum_order_pickup)){
	  echo CHtml::hiddenField('merchant_maximum_order_pickup',$merchant_maximum_order_pickup);
	  
	  echo CHtml::hiddenField('merchant_maximum_order_pickup_pretty',
         displayPrice(baseCurrency(),prettyFormat($merchant_maximum_order_pickup)));
}  

/*add minimum and max for delivery*/
$minimum_order=Yii::app()->functions->getOption('merchant_minimum_order',$merchant_id);
if (!empty($minimum_order)){
	echo CHtml::hiddenField('minimum_order',unPrettyPrice($minimum_order));
	echo CHtml::hiddenField('minimum_order_pretty',
	 displayPrice(baseCurrency(),prettyFormat($minimum_order))
	);
}
$merchant_maximum_order=Yii::app()->functions->getOption("merchant_maximum_order",$merchant_id);
 if (is_numeric($merchant_maximum_order)){
 	echo CHtml::hiddenField('merchant_maximum_order',unPrettyPrice($merchant_maximum_order));
    echo CHtml::hiddenField('merchant_maximum_order_pretty',baseCurrency().prettyFormat($merchant_maximum_order));
 }

$is_ok_delivered=1;
if (is_numeric($merchant_delivery_distance)){
	if ( $distance>$merchant_delivery_distance){
		$is_ok_delivered=2;
		/*check if distance type is feet and meters*/
		if($distance_type=="ft" || $distance_type=="mm" || $distance_type=="mt"){
			$is_ok_delivered=1;
		}
	}
} 

echo CHtml::hiddenField('is_ok_delivered',$is_ok_delivered);
echo CHtml::hiddenField('merchant_delivery_miles',$merchant_delivery_distance);
echo CHtml::hiddenField('unit_distance',$distance_type);
echo CHtml::hiddenField('from_address', FunctionsV3::getSessionAddress() );

echo CHtml::hiddenField('merchant_close_store',getOption($merchant_id,'merchant_close_store'));
/*$close_msg=getOption($merchant_id,'merchant_close_msg');
if(empty($close_msg)){
	$close_msg=t("This restaurant is closed now. Please check the opening times.");
}*/
echo CHtml::hiddenField('merchant_close_msg',
isset($checkout['msg'])?$checkout['msg']:t("Sorry merchant is closed."));

echo CHtml::hiddenField('disabled_website_ordering',getOptionA('disabled_website_ordering'));
echo CHtml::hiddenField('web_session_id',session_id());

echo CHtml::hiddenField('merchant_map_latitude',$data['latitude']);
echo CHtml::hiddenField('merchant_map_longitude',$data['longitude']);
echo CHtml::hiddenField('merchant_name',$data['merchant_name']);


/*add meta tag for image*/
Yii::app()->clientScript->registerMetaTag(
Yii::app()->getBaseUrl(true).FunctionsV3::getMerchantLogo($merchant_id)
,'og:image');
?>

<div class="sections section-menu section-grey2">
<div class="container">
  <div class="row">

     <div class="col-md-9 border menu-left-content">
		<div class="row well well-white padding-10">
			<div class="col-md-12 " style="border-radius:7px;height:300px;width:100%;background-size:100%;background-image:url('<?php echo empty($merchant_photo_bg)?assetsURL()."/images/b-2.jpg":uploadURL()."/$merchant_photo_bg"; ?>')"></div>
			<div class="col-md-12 ">
				<h1><?=$merchant_info['merchant_name']?></h1>
				<h4 style="font-weight:300"><?=$data['street'].','.$data['city'] ?></h4>
				<?php //dump($data);?>
			</div>
		</div>
        <div class="tabs-wrapper row" id="menu-tab-wrapper">
	    <ul id="tabs" class="margin-bottom-0">
		    <li>
		      <span><?php echo t("Overview")?></span>
		      <i class="ion-ios-information-outline"></i>
		    </li>
			<?php if ($theme_info_tab==""):?>
		    <li class="active">
		       <span><?php echo t("Menu")?></span>
		       <i class="ion-fork"></i>
		    </li>
		    <?php endif;?>
		    <?php if ($theme_hours_tab==""):?>
		    <li>
		       <span><?php echo t("Opening Hours")?></span>
		       <i class="ion-clock"></i>
		    </li>
		    <?php endif;?>
		    
		    <?php if ($theme_reviews_tab==""):?>
		    <li class="view-reviews">
		       <span><?php echo t("Reviews")?></span>
		       <i class="ion-ios-star-half"></i>
		    </li>
		    <?php endif;?>
		    
		    <?php if ($theme_map_tab==""):?>
		    <li class="view-merchant-map">
		       <span><?php echo t("Map")?></span>
		       <i class="ion-ios-navigate-outline"></i>
		    </li>
		    <?php endif;?>
		    
		    <?php if ($photo_enabled):?>
		      <li class="view-merchant-photos">
		       <span><?php echo t("Photos")?></span>
		       <i class="ion-images"></i>
		      </li>
		    <?php endif;?>
		    

		    
		</ul>
	    <style>
		.info-value {
			color: #c22a39;

			font-size: 17px !important;
		}
		.info-field {
			font-size: 22px !important;
			margin-top:10px;
		}
		.img-square {
			margin-right:20px;float:left;width:135px;height:135px;border-radius:5px;background-size:cover;
		}
		.img-avatar-round {
			width:70px;height:70px;border-radius:6px;
			display: block;
			margin: auto;
		}
		.menu-img {
			opacity:0.5;
		}
		</style>
		<ul id="tab">
	    <!--OVERVIEW-->

	    <li>
	        <div class="box-grey rounded5 " style="margin-top:0;">
				<div class="row">
					<div class="col-md-4">
						<h3 class="info-field"><?=t('Phone Number')?></h3>
						<p class="info-value"><?=$data['merchant_phone']?></p>
						<h3 class="info-field"><?=t('Address')?></h3>
						<p class="info-value"><?=$data['street'].', '.$data['city'].', '.$data['state']?></p>
					</div>
					<div class="col-md-8">
						<h3 class="info-field"><?=t('Information')?></h3>
						<p class="info-value"><?=getOption($merchant_id,'merchant_information')?></p>
					</div>

				</div>
				<?php 
				  // dump($data);
				?>
	        </div>
			<div class="box-grey rounded5 " style="margin-top:0;">
				<div class="row">
					<div class="col-md-12">
						<h3 class="info-field"><?=t('Menu')?></h3>
							<?php 
							$r = query("SELECT a.* FROM {{item}} a 
											WHERE a.status='publish' AND a.merchant_id=?
											LIMIT 0,5
											",array($merchant_info['merchant_id']));
							?>
							<?php for($i=0;$i<5;$i++) {
							if($r[$i]['photo']) {
								?>
										<a href="javascript:;" 
											class="menu-item <?php echo $r[$i]['not_available']==2?"item_not_available":''?>"
											rel="<?php echo $r[$i]['item_id']?>"
											data-single="" 
										>
											<div class="img-square" style="background-image:url('<?php echo uploadURL()."/".$r[$i]['photo'];?>')" ></div>
										</a>
								<?php
							} else {
								?><div class="img-square menu-img" style="background-image:url('<?php echo assetsURL()."/images/item.jpg";?>')" ></div><?php
							}
							} ?>
					</div>
				</div>
	        </div>
			<div class="box-grey rounded5 " style="margin-top:0;">
				<div class="row">
					<div class="col-md-12">
						<h3 class="info-field"><?=t('Photo')?></h3>
							<?php 	$max_photo = count($gallery) < 5 ? count($gallery) > 5: 5;
									for($i=0;$i<5;$i++) {
									if($gallery[$i]) {
										?><div class="img-square " style="background-image:url('<?php echo uploadURL()."/".$gallery[$i];?>')" ></div><?php
									} else {
										?><div class="img-square menu-img" style="background-image:url('<?php echo assetsURL()."/images/item.jpg";?>')" ></div><?php
									}
									} 
							?>
					</div>
				</div>
	        </div>
			
			<div class="text-right">
			<a href="javascript:;" class="write-review-new btn btn-danger inline bottom10 upper">
			<?php echo t("write a review")?>
			</a>
			</div>
			<div class="review-input-wrap box-grey rounded5 margin-vert-10">
			<div class="bottom10 row ">
			<form class="forms" id="forms" onsubmit="return false;">
			<?php echo CHtml::hiddenField('action','addReviews')?>
			<?php echo CHtml::hiddenField('currentController','store')?>
			<?php echo CHtml::hiddenField('merchant-id',$merchant_id)?>
			<?php echo CHtml::hiddenField('initial_review_rating','')?>	        
			   <div class="col-md-12 border">
				<div class="input-group input-group-www">
				  <span class="input-group-addon" style="background:white">
					<i class="fa fa-pencil"></i>
				  </span>
				  
				  <input style="border-left:0" type="text" class="form-control grey-inputs" placeholder="Write a review..."  required="1" name="review_content"  class="review_content" id="review_content">
				  <span class="input-group-btn">
					<button class="btn btn-success inline" type="submit"><?php echo t("PUBLISH REVIEW")?></button>
				  </span>
				</div><!-- /input-group -->
				 <div class="margin-top-5">
				 <div class="raty-stars" data-score="5"></div>   
				 </div>				
				
			   </div> <!--col-->	        
			</form>
			</div> <!--review-input-wrap-->
			</div>
			<h3 class="info-field"><?=t('Reviews')?></h3>
			
			<div class=" merchant-review-wrap2" style="margin-top:0;"></div>
			
			
			<script>
		    // initReadMore();
			</script>
		</li>
	    
	    <!--END OVERVIEW-->
		
		<!--MENU-->
		<?php if ($theme_info_tab==""):?>
	    <li class="active">
	        <div class="row">
			 <div class="col-md-3 col-xs-4 border category-list">
				<div class="theiaStickySidebar">
				 <?php 
				 $this->renderPartial('/front/menu-category',array(
				  'merchant_id'=>$merchant_id,
				  'menu'=>$menu			  
				 ));
				 ?>
				</div>
			 </div> <!--col-->
			 <div class="col-md-9 col-xs-8 border" id="menu-list-wrapper">
			 <?php 
			 $admin_activated_menu=getOptionA('admin_activated_menu');			 
			 $admin_menu_allowed_merchant=getOptionA('admin_menu_allowed_merchant');
			 if ($admin_menu_allowed_merchant==2){			 	 
			 	 $temp_activated_menu=getOption($merchant_id,'merchant_activated_menu');			 	 
			 	 if(!empty($temp_activated_menu)){
			 	 	 $admin_activated_menu=$temp_activated_menu;
			 	 }
			 }			 
			 switch ($admin_activated_menu)
			 {
			 	case 1:
			 		$this->renderPartial('/front/menu-merchant-2',array(
					  'merchant_id'=>$merchant_id,
					  'menu'=>$menu,
					  'merchant_info'=>$merchant_info,
					  'disabled_addcart'=>$disabled_addcart,
					  'distance'=>$distance,
					));
			 		break;
			 		
			 	case 2:
			 		$this->renderPartial('/front/menu-merchant-2',array(
					  'merchant_id'=>$merchant_id,
					  'menu'=>$menu,
					  'merchant_info'=>$merchant_info,
					  'disabled_addcart'=>$disabled_addcart,
					  'distance'=>$distance,
					));
			 		break;
			 			
			 	default:	
				 	$this->renderPartial('/front/menu-merchant-2',array(
					  'merchant_id'=>$merchant_id,
					  'menu'=>$menu,
					  'merchant_info'=>$merchant_info,
					  'disabled_addcart'=>$disabled_addcart,
					  'tc'=>$tc,
					  'distance'=>$distance,
					));
			    break;
			 }			 
			 ?>			
			 </div> <!--col-->
			</div> <!--row-->
	    </li>
	    <?php endif;?>
		<!--END MENU-->
	    
	    
	    <!--OPENING HOURS-->
	    <?php if ($theme_hours_tab==""):?>
	    <li>	       	     
	    <?php
	    $this->renderPartial('/front/merchant-hours',array(
	      'merchant_id'=>$merchant_id
	    )); ?>           
	    </li>
	    <?php endif;?>
	    <!--END OPENING HOURS-->
	    
	    <!--MERCHANT REVIEW-->
	    <?php if ($theme_reviews_tab==""):?>
	    <li class="review-tab-content">	       	     
	    <?php $this->renderPartial('/front/merchant-review',array(
	      'merchant_id'=>$merchant_id
	    )); ?>           
	    </li>
	    <?php endif;?>
	    <!--END MERCHANT REVIEW-->
	    
	    <!--MERCHANT MAP-->
	    <?php if ($theme_map_tab==""):?>
	    <li>	        	
	    <?php $this->renderPartial('/front/merchant-map'); ?>        
	    </li>
	    <?php endif;?>
	    <!--END MERCHANT MAP-->
	    
	    
	    <!--PHOTOS-->
	    <?php if ($photo_enabled):?>
	    <li>
	    <?php 
		// dump($gallery);
	    $this->renderPartial('/front/merchant-photos',array(
	      'merchant_id'=>$merchant_id,
	      'gallery'=>$gallery
	    )); ?>        
	    </li>
	    <?php endif;?>
	    <!--END PHOTOS-->
	    

	    

	    
	    
	   </ul>
	   </div>
     
     </div> <!-- menu-left-content-->
     
     <?php if (getOptionA('disabled_website_ordering')!="yes"):?>
     <div id="menu-right-content" class="col-md-3 border menu-right-content <?php echo $disabled_addcart=="yes"?"hide":''?>" >
     
     <div class="theiaStickySidebar">
      <div class="box-grey rounded  relative">
      
        <div class="star-float"></div>
      
        <!--DELIVERY INFO-->
        <div class="inner center">
         <button type="button" class="close modal-close-btn" data-dismiss="modal" aria-label="Close">
           <span aria-hidden="true">&times;</span>
         </button> 
                   
            <?php if ($data['service']==3):?>
            <p class="bold"><?php echo t("Distance Information")?></p>
            <?php else :?>
	        <p class="bold"><?php echo t("Delivery Information")?></p>
	        <?php endif;?>
	        
	        <p>
	        <?php 
	        if ($distance){
	        	echo t("Distance to Merchant").": ".number_format($distance,1)." $distance_type";
	        } else echo  t("Distance").": ".t("not available");
	        ?>
	        </p>
	        
	        <p class="delivery-fee-wrap hidden">
	        <?php //echo t("Delivery Est")?>: <?php //echo FunctionsV3::getDeliveryEstimation($merchant_id)?></p>
	        
	        <p class="delivery-fee-wrap hidden">
	        <?php 
	        // if (!empty($merchant_delivery_distance)){
	        	// echo t("Delivery Distance Covered").": ".$merchant_delivery_distance." $distance_type_orig";
	        // } else echo  t("Delivery Distance Covered").": ".t("not available");
	        ?>
	        </p>
	        
	        <p class="delivery-fee-wrap hidden">
	        <?php 
	        // if ($delivery_fee){
	             // echo t("Delivery Fee").": ".FunctionsV3::prettyPrice($delivery_fee);
	        // } else echo  t("Delivery Fee").": ".t("Free Delivery");
	        ?>
	        </p>
	        
	        <a href="javascript:;" class="top10 green-color change-address block text-center">
	        [<?php echo t("Change Your Address here")?>]
	        </a>
	        
        </div>
        <!--END DELIVERY INFO-->
        
        <!--CART-->
        <div class="inner line-top relative">
        
           <i class="order-icon your-order-icon"></i>
           
           <p class="bold center"><?php echo t("Your Order")?></p>
           
           <div class="item-order-wrap"></div>
           
           
           <!--MAX AND MIN ORDR-->
           <?php if ($minimum_order>0):?>
           <div class="delivery-min">
              <p class="small center"><?php echo Yii::t("default","Subtotal must exceed")?> 
              <?php echo displayPrice(baseCurrency(),prettyFormat($minimum_order,$merchant_id))?>
           </div>
           <?php endif;?>
           
           <?php if ($merchant_minimum_order_pickup>0):?>
           <div class="pickup-min">
              <p class="small center"><?php echo Yii::t("default","Subtotal must exceed")?> 
              <?php echo displayPrice(baseCurrency(),prettyFormat($merchant_minimum_order_pickup,$merchant_id))?>
           </div>
           <?php endif;?>
              
	        <a href="javascript:;" class="clear-cart">[<?php echo t("Clear Order")?>]</a>
           
        </div> <!--inner-->
        <!--END CART-->
        
        <!--DELIVERY OPTIONS-->
        <div class="inner line-top relative delivery-option center">
           <?php echo CHtml::hiddenField('delivery_type',"")?>
           <?php echo CHtml::hiddenField('driver_id',"")?>
           <?php echo CHtml::hiddenField('delivery_date',date('Y-m-d'))?>
           <?php echo CHtml::hiddenField('delivery_time',"")?>
		   
           <?php if ( $checkout['code']==1):?>
			   <a href="javascript:;" class="margin-bottom-50 orange-button medium checkout2" style="font-family: Open Sans;font-weight: 400;font-size: 15px;"><?=t('ORDER')?></a>
			   
              
           <?php else :?>
              <?php if ( $checkout['holiday']==1):?>
                 <?php echo CHtml::hiddenField('is_holiday',$checkout['msg'],array('class'=>'is_holiday'));?>
                 <p class="text-danger"><?php echo $checkout['msg']?></p>
              <?php else :?>
                 <p class="text-danger"><?php echo $checkout['msg']?></p>
                 <p class="small">
                 <?php echo Yii::app()->functions->translateDate(date('F d l')."@".timeFormat(date('c'),true));?></p>
              <?php endif;?>
           <?php endif;?>
                                                                
        </div> <!--inner-->
        <!--END DELIVERY OPTIONS-->
        
      </div> <!-- box-grey-->
      </div> <!--end theiaStickySidebar-->
     
     </div> <!--menu-right-content--> 
     <?php endif;?>
  
  </div> <!--row-->
</div> <!--container-->
</div> <!--section-menu-->