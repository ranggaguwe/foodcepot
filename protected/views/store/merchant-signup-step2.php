<?php
$this->renderPartial('/front/banner-receipt',array(
   'h1'=>t("Login & Signup Merchant"),
   'sub_text'=>t("")
));

/*PROGRESS ORDER BAR*/
$this->renderPartial('/front/progress-merchantsignup',array(
   'step'=>2,
   'show_bar'=>true
));

echo CHtml::hiddenField('mobile_country_code',Yii::app()->functions->getAdminCountrySet(true));
?>



<div class="sections section-grey2">

  <div class="container">
  
  <div class="row">  
  <div class="col-md-8 border">
	<h2><?=t('Merchant Signup')?></h2>
    <div class="box-grey round top-line-green">
             
     <?php if (is_array($data) && count($data)>=1):?>
     
       <form class="forms" id="forms" onsubmit="return false;">
	  <?php echo CHtml::hiddenField('action','merchantSignUp')?>
	  <?php echo CHtml::hiddenField('currentController','store')?>
	  <?php echo CHtml::hiddenField('package_id',$data['package_id'])?>
 

      
      <div class="row top30">
        <div class="col-md-3 "><?php echo t("Merchant name")?></div>
        <div class="col-md-8 ">
             <?php echo CHtml::textField('merchant_name',
			  isset($data['merchant_name'])?$data['merchant_name']:""
			  ,array(
			  'class'=>'grey-fields full-width',
			  'data-validation'=>"required"
			  ))?>
        </div>
      </div>
      
     <?php if ( getOptionA('merchant_reg_abn')=="yes"):?>
     <div class="row top10">
        <div class="col-md-3 "><?php echo t("ABN")?></div>
        <div class="col-md-8 ">
              <?php echo CHtml::textField('abn',
			  isset($data['merchant_name'])?$data['abn']:""
			  ,array(
			  'class'=>'grey-fields full-width',
			  'data-validation'=>"required"
			  ))?>
        </div>
      </div>
     <?php endif;?>
      
     <div class="row top10">
        <div class="col-md-3"><?php echo t("Merchant phone")?></div>
        <div class="col-md-8">
         <?php echo CHtml::textField('merchant_phone',
		  isset($data['merchant_phone'])?$data['merchant_phone']:""
		  ,array(
		  'class'=>'grey-fields full-width',
		  ))?>    
        </div>
      </div>
      
      <div class="row top10">
        <div class="col-md-3"><?php echo t("Contact name")?></div>
        <div class="col-md-8">
		  <?php echo CHtml::textField('contact_name',
		  isset($data['contact_name'])?$data['contact_name']:""
		  ,array(
		  'class'=>'grey-fields full-width',
		  'data-validation'=>"required"
		  ))?>           
        </div>
      </div>
      
      <div class="row top10">
        <div class="col-md-3"><?php echo t("Contact phone")?></div>
        <div class="col-md-8">
		  <?php echo CHtml::textField('contact_phone',
		  isset($data['contact_phone'])?$data['contact_phone']:""
		  ,array(
		  'class'=>'grey-fields full-width mobile_inputs',
		  'data-validation'=>"required"
		  ))?>           
        </div>
      </div>
      
      <div class="row top10">
        <div class="col-md-3"><?php echo t("Email")?></div>
        <div class="col-md-8">
		  <?php echo CHtml::textField('contact_email',
		  isset($data['contact_email'])?$data['contact_email']:""
		  ,array(
		  'class'=>'grey-fields full-width',
		  'data-validation'=>"email"
		  ))?>           
        </div>
      </div> 
      
      <div class="row top10">
        <div class="col-md-3"></div>
        <div class="col-md-8">
        <p class="text-muted text-small"><?php echo t("Important: Please enter your correct email. we will sent an activation code to your email")?></p>
        </div>
      </div>   
      
      
      <div class="row top10">
        <div class="col-md-3"><?php echo t("Street address")?></div>
        <div class="col-md-8">
		  <?php echo CHtml::textField('street',
		  isset($data['street'])?$data['street']:""
		  ,array(
		  'class'=>'grey-fields full-width',
		  'data-validation'=>"required"
		  ))?>           
        </div>
      </div>
      
      <div class="row top10">
        <div class="col-md-3"><?php echo t("City")?></div>
        <div class="col-md-8">
		  <?php echo CHtml::textField('city',
		  isset($data['city'])?$data['city']:""
		  ,array(
		  'class'=>'grey-fields full-width',
		  'data-validation'=>"required"
		  ))?>           
        </div>
      </div>
      
      <div class="row top10">
        <div class="col-md-3"><?php echo t("Post code/Zip code")?></div>
        <div class="col-md-8">
		  <?php echo CHtml::textField('post_code',
		  isset($data['post_code'])?$data['post_code']:""
		  ,array(
		  'class'=>'grey-fields full-width',
		  'data-validation'=>"required"
		  ))?>           
        </div>
      </div>
      
      
      <div class="row top10">
        <div class="col-md-3"><?php echo t("State/Region")?></div>
        <div class="col-md-8">
		  <?php echo CHtml::textField('state',
		  isset($data['state'])?$data['state']:""
		  ,array(
		  'class'=>'grey-fields full-width',
		  'data-validation'=>"required"
		  ))?>           
        </div>
      </div>
      

      
      <div class="top15">
      <?php FunctionsV3::sectionHeader('Login Information');?>
      </div>
      
      <div class="row top10">
        <div class="col-md-3"><?php echo t("Username")?></div>
        <div class="col-md-8">
		<?php echo CHtml::textField('username',
		  ''
		  ,array(
		  'class'=>'grey-fields full-width',
		  'data-validation'=>"required"
		  ))?>
        </div>
      </div>
      
      <div class="row top10">
        <div class="col-md-3"><?php echo t("Password")?></div>
        <div class="col-md-8">
		  <?php echo CHtml::passwordField('password',
		  ''
		  ,array(
		  'class'=>'grey-fields full-width',
		  'data-validation'=>"required"
		  ))?>           
        </div>
      </div>
      
      <div class="row top10">
        <div class="col-md-3"><?php echo t("Confirm Password")?></div>
        <div class="col-md-8">
		  <?php echo CHtml::passwordField('cpassword',
		  ''
		  ,array(
		  'class'=>'grey-fields full-width',
		  'data-validation'=>"required"
		  ))?>           
        </div>
      </div>
      
      <?php if ($kapcha_enabled==2):?>      
      <div class="top10 capcha-wrapper">        
        <div id="kapcha-1"></div>
      </div>
      <?php endif;?>
      
      <?php if ( $terms_merchant=="yes"):?>
      <?php $terms_link=Yii::app()->functions->prettyLink($terms_merchant_url);?>
      <div class="row top10">
        <div class="col-md-3"></div>
        <div class="col-md-8">
          <?php 
		  echo CHtml::checkBox('terms_n_condition',false,array(
		   'value'=>2,
		   'class'=>"",
		   'data-validation'=>"required"
		  ));
		  echo " ". t("I Agree To")." <a href=\"$terms_link\" target=\"_blank\">".t("The Terms & Conditions")."</a>";
		  ?>  
        </div>
      </div>
      <?php endif;?>
      
      <div class="row top10">
        <div class="col-md-3"></div>
        <div class="col-md-8">
          <input type="submit" value="<?php echo t("Next")?>" class="orange-button inline medium">
        </div>
      </div>
      
      </form>
      
      <?php else :?>
      <p class="text-danger"><?php echo t("Sorry but we cannot find what you are looking for.")?></p>
      <?php endif;?>
       
    </div> <!--box-grey-->
    
   </div> <!--col-->
   <div class="col-md-4 border sticky-div text-center">
	<a href="<?php echo Yii::app()->createUrl('/merchant' )?>" class="btn btn-danger btn-lg">&nbsp;&nbsp;<?=t('Merchant Login')?>&nbsp;&nbsp;
	</a>
   </div>
   <div class="col-md-4 border sticky-div hidden">
	   
       <div class="box-grey round" id="change-package-wrap">
           
          <?php 
          $p_list='';
          if (is_array($package_list) && count($package_list)>=1){
          	  foreach ($package_list as $val) {
          	  	  $p_list[$val['package_id']]=$val['title'];
          	  }
          }    
          echo CHtml::hiddenField('change_package_url',
             Yii::app()->createUrl('/store/merchantsignup?do=step2&package_id=')
          ) ;
          ?>
          <?php //FunctionsV3::sectionHeader('Change Package');?>
          
          <div class="top10">
            <?php 
            echo CHtml::dropDownList('change_package',
            isset($_GET['package_id'])?$_GET['package_id']:''
            ,(array)$p_list,array(
              'class'=>'grey-fields full-width',
            ));
            ?>          
          </div>
           
           <div class="top25">

           </div>
           
       </div> <!--box-->
   </div> <!--col-->
   
   </div> <!--row--> 
  </div> <!--container-->  
</div> <!--sections-->