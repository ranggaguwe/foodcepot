<?php
$this->renderPartial('/front/default-header',array(
   'h1'=>t("Payment Option"),
   'sub_text'=>t("choose your payment")
));?>

<?php 
$this->renderPartial('/front/order-progress-bar',array(
   'step'=>4,
   'show_bar'=>true
));

$s=$_SESSION;
$continue=false;

$merchant_address='';		
if ($merchant_info=Yii::app()->functions->getMerchant($s['kr_merchant_id'])){	
	$merchant_address=$merchant_info['street']." ".$merchant_info['city']." ".$merchant_info['state'];
	$merchant_address.=" "	. $merchant_info['post_code'];
}

$client_info='';

if (isset($is_guest_checkout)){
	$continue=true;	
} else {	
	$client_info = Yii::app()->functions->getClientInfo(Yii::app()->functions->getClientId());
	
	// if (isset($s['kr_search_address'])){	
		// $temp=explode(",",$s['kr_search_address']);		
		// if (is_array($temp) && count($temp)>=2){
			// $street=isset($temp[0])?$temp[0]:'';
			// $city=isset($temp[1])?$temp[1]:'';
			// $state=isset($temp[2])?$temp[2]:'';
		// }
		// if ( isset($client_info['street'])){
			// if ( empty($client_info['street']) ){
				// $client_info['street']=$street;
			// }
		// }
		// if ( isset($client_info['city'])){
			// if ( empty($client_info['city']) ){
				// $client_info['city']=$city;
			// }
		// }
		// if ( isset($client_info['state'])){
			// if ( empty($client_info['state']) ){
				// $client_info['state']=$state;
			// }
		// }
	// }
	
	$client_info['street'] = "";
	if(isset($_SESSION['kr_search_address'])) {
		// var_dump("location");
		$client_info['street'] = $_SESSION['kr_search_address'];
	} else {
		// var_dump("location4");
	   //alternatif menggunakan data dari addressbook
	   //dicek dulu di google 
		$address_list=Yii::app()->functions->addressBook(Yii::app()->functions->getClientId());
		$myaddress = array_pop($address_list);
		if ($lat_res=Yii::app()->functions->geodecodeAddress($myaddress)) {
			if($lat_res['lat']) {
				$_SESSION['kr_search_address']=$myaddress;
				$_SESSION['client_location']=array(
				  'lat'=>$lat_res['lat'],
				  'long'=>$lat_res['long'],
				  'client_address'=>$myaddress
				);
				$client_info['street'] = $myaddress;
			}
		}
	}



	
	
	if (isset($s['kr_merchant_id']) && Yii::app()->functions->isClientLogin() && is_array($merchant_info) ){
		$continue=true;
	}
}
echo CHtml::hiddenField('mobile_country_code',Yii::app()->functions->getAdminCountrySet(true));

echo CHtml::hiddenField('admin_currency_set',getCurrencyCode());

echo CHtml::hiddenField('admin_currency_position',
Yii::app()->functions->getOptionAdmin("admin_currency_position"));
?>


<div class="sections section-grey2 section-payment-option">
   <div class="container">
           
     <?php if ( $continue==TRUE):?>
     <?php 
     $merchant_id=$s['kr_merchant_id'];
     echo CHtml::hiddenField('merchant_id',$merchant_id);
     ?>
     <div class="col-md-7 border">
          
     <div class="box-grey rounded">
     <form id="frm-delivery" class="frm-delivery" method="POST" onsubmit="return false;">
     <?php 
     echo CHtml::hiddenField('action','placeOrder');
     echo CHtml::hiddenField('country_code',$merchant_info['country_code']);
     echo CHtml::hiddenField('currentController','store');
     echo CHtml::hiddenField('delivery_type',$s['kr_delivery_options']['delivery_type']);
	 echo CHtml::hiddenField('driver_id',$s['kr_delivery_options']['driver_id']);
     echo CHtml::hiddenField('cart_tip_percentage','');
     echo CHtml::hiddenField('cart_tip_value','');
     echo CHtml::hiddenField('client_order_sms_code');
     echo CHtml::hiddenField('client_order_session');
	 echo CHtml::hiddenField('street',$client_info['street']);
	 
     if (isset($is_guest_checkout)){
     	echo CHtml::hiddenField('is_guest_checkout',2);
     }     
     ?>
     
     <?php if ( $s['kr_delivery_options']['delivery_type']=="pickup"):?> 
     
          <h3><?php echo Yii::t("default","Pickup information")?></h3>
          <p class="uk-text-bold"><?php echo $merchant_address;?></p>
                   
          
          
          
          
     <?php else :?> <!-- DELIVERY-->                          	       	      
          
		  <?php FunctionsV3::sectionHeader('Order Type')?>		  
		  <p>
	        <?php //echo clearString(ucwords($merchant_info['merchant_name']))?> <?php //echo Yii::t("default","Restaurant")?> 
	        <?php echo "<span  style='font-size:15px'>".Yii::t("default",ucwords($s['kr_delivery_options']['delivery_type'])) . "</span> ";

	        ?>
	       </p>
		   
	       <?php FunctionsV3::sectionHeader('Pickup Time');
				  echo '<span  style="font-size:15px">Tanggal '.date("M d Y",strtotime($s['kr_delivery_options']['delivery_date'])).
				  " pada pukul ". $s['kr_delivery_options']['delivery_time']."</span> ";
		   ?>
		   
	       <div class="top10">
	  
	        <?php FunctionsV3::sectionHeader('Deliver To')?> 
	        	       
	        <?php if (isset($is_guest_checkout)):?>	         	        
	         <div class="row top10">
                <div class="col-md-10">
                 <?php echo CHtml::textField('first_name','',array(
	               'class'=>'grey-fields full-width',
	               'placeholder'=>Yii::t("default","First Name"),
	               'data-validation'=>"required"
	              ))?>
	             </div> 
              </div>
              
              <div class="row top10">
                <div class="col-md-10">
                 <?php echo CHtml::textField('last_name','',array(
	               'class'=>'grey-fields full-width',
	               'placeholder'=>Yii::t("default","Last Name"),
	               'data-validation'=>"required"
	              ))?>
	             </div> 
              </div>
	        <?php endif;?> <!--$is_guest_checkout-->
	        
	        <?php if ( $website_enabled_map_address==2 ):?>
	        <div class="top10">
            <?php Widgets::AddressByMap()?>
            </div>
            <?php endif;?>
            

            
            <div class="address-block">
              <div class="row top10">
                <div class="col-md-10">
					<span style="font-size:16px"><?=$client_info['street']?></span> &nbsp;&nbsp;<a href="javascript:;" class="btn btn-success change-address text-center">
					<?php echo t("Change Address")?>
					</a>
	             </div> 
              </div>
			  

              
            </div> <!--address-block-->  
              <?php FunctionsV3::sectionHeader('Contact Phone')?> 
              <div class="row top10">
                <div class="col-md-10">
                 <?php echo CHtml::textField('contact_phone',
                 isset($client_info['contact_phone'])?$client_info['contact_phone']:''
                 ,array(
	               'class'=>'grey-fields mobile_inputs full-width',
	               'placeholder'=>Yii::t("default","Mobile Number"),
	               'data-validation'=>"required"  
	              ))?>
	             </div> 
              </div>  
              
             <?php if (isset($is_guest_checkout)):?>
             <div class="row top10">
                <div class="col-md-10">
                 <?php echo CHtml::textField('email_address','',array(
	               'class'=>'grey-fields full-width',
	               'placeholder'=>Yii::t("default","Email address"),              
	              ))?>
	             </div> 
              </div>
                                          
             <?php endif;?> 
                                      
            
             <?php if (isset($is_guest_checkout)):?>
             <?php FunctionsV3::sectionHeader('Optional')?>		  
             <div class="row top10">
                <div class="col-md-10">
                 <?php echo CHtml::passwordField('password','',array(
	               'class'=>'grey-fields full-width',
	               'placeholder'=>Yii::t("default","Password"),               
	              ))?>
	             </div> 
              </div>
             <?php endif;?>
             
	       </div> <!--top10--> 
	        	        	               
     <?php endif;?> <!-- ENDIF DELIVERY-->
     
     
     <div class="top25">
     <?php 
	 $this->renderPartial('/front/payment-list',array(
	   'merchant_id'=>$merchant_id,
	   'payment_list'=>FunctionsV3::getAdminPaymentList($merchant_id)
	 ));
	 ?>
	 </div>
     
     </form>    
     
     
     </div> <!--box rounded-->
     
     </div> <!--left content-->
     
     <div class="col-md-5 border sticky-div"><!-- RIGHT CONTENT STARTS HERE-->
     
       <div class="box-grey rounded  relative top-line-green">
       
       <i class="order-icon your-order-icon"></i>
       
	       <div class="order-list-wrap" style="	font-family: 'Raleway';font-weight: 500;font-size: 15px;">   
	       
	         <p class="bold center" style="font-size:16px"><?php echo t("Your Order")?></p>
	         <div class="item-order-wrap2"></div>
	       
	         
	         <?php 
	         $minimum_order=Yii::app()->functions->getOption('merchant_minimum_order',$merchant_id);
	         $maximum_order=getOption($merchant_id,'merchant_maximum_order');	         
	         if ( $s['kr_delivery_options']['delivery_type']=="pickup"){
	          	  $minimum_order=Yii::app()->functions->getOption('merchant_minimum_order_pickup',$merchant_id);
	          	  $maximum_order=getOption($merchant_id,'merchant_maximum_order_pickup');	         
	         }  
	         ?>
	         
	         <?php 
	         if (!empty($minimum_order)){
	         	echo CHtml::hiddenField('minimum_order',unPrettyPrice($minimum_order));
	            echo CHtml::hiddenField('minimum_order_pretty',baseCurrency().prettyFormat($minimum_order));
	            ?>
	            <p class="small center"><?php echo t("Subtotal must exceed")?> 
                 <?php echo baseCurrency().prettyFormat($minimum_order,$merchant_id)?>
                </p>      
	            <?php
	         }
	         if($maximum_order>0){
	         	echo CHtml::hiddenField('maximum_order',unPrettyPrice($maximum_order));
	         	echo CHtml::hiddenField('maximum_order_pretty',baseCurrency().prettyFormat($maximum_order));
	         }
	         ?>
	         
	         <?php if ( getOptionA('captcha_order')==2 || getOptionA('captcha_customer_signup')==2):?>             
             <div class="top10 capcha-wrapper">
             <?php //GoogleCaptcha::displayCaptcha()?>
             <div id="kapcha-1"></div>
             </div>
             <?php endif;?>          
             
           
	          <div class="text-center top25">
	          <a href="javascript:;" class="place_order green-button medium inline block">
	          <?php echo t("Place Order")?>
	          </a>
	          </div>
	         
	       </div> <!-- order-list-wrap-->       
	   </div> <!--box-grey-->    
     
     </div> <!--right content-->
     
     <?php else :?>      
       <div class="box-grey rounded">
      <p class="text-danger">
      <?php echo t("Something went wrong Either your visiting the page directly or your session has expired.")?></p>
      </div>
     <?php endif;?>
   
   </div>  <!--container-->
</div> <!--section-payment-option-->