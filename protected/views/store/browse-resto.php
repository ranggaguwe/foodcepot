

<?php 
// if (isset($_GET['location'])) {
	// $location = $_GET['location'];
// } else {
	// $location = "";
// }
// if (isset($_GET['category'])) {
	// $category = $_GET['category'];
// } else {
	// $category = "";
// }
// if (isset($_GET['satuan'])) {
	// $satuan = $_GET['satuan'];
// } else {
	// $satuan = "";
// }
// if (isset($_GET['o'])) {
	// $output = $_GET['o'];
// } else {
	// $output = "";
// }
?>
<?php 
if(isset($_POST)) {
	$_GET = $_POST;
}
if (isset($_GET['keyword'])) {
	$keyword=$_GET['keyword'];
} else {
	$keyword = "";
}
if (isset($_GET['location'])) {
	if($_GET['location'] == 'semua') {
		$location = array('label' => t('Semua'),'value' => 'semua');
	} else if ($_GET['location'] == 'city') {
		$location = array('label' => t('Kota'),'value' => 'city');
	} else if ($_GET['location'] == 'current') {
		$location = array('label' => t('Lokasi Saat Ini'),'value' => 'current');
	} else {
		$location = array('label' => '-- Lokasi --','value' => '');
	}
} else {
	$location = array('label' => '-- Lokasi --','value' => '');
}
if (isset($_GET['category'])) {
	$category = $_GET['category'];
} else {
	$category = "";
}
if (isset($_GET['satuan'])) {
	if($_GET['satuan'] == 'grosir') {
		$satuan = array('label' => t('Grosir'),'value' => 'grosir');
	} else if ($_GET['satuan'] == 'eceran') {
		$satuan = array('label' => t('Eceran'),'value' => 'eceran');
	} else {
		$satuan = array('label' => '-- Satuan --','value' => '');
	}
} else {
	$satuan = array('label' => '-- Satuan --','value' => '');
}
if (isset($_GET['o'])) {
	if($_GET['o'] == 'product') {
		$output = array('label' => t('Product'),'value' => 'product');
	} else if ($_GET['o'] == 'merchant') {
		$output = array('label' => 'Merchant','value' => 'merchant');
	} else {
		$output = array('label' => 'Merchant','value' => 'merchant');
	}
} else {
	$output = array('label' => 'Merchant','value' => 'merchant');
}
?>
<div class="mobile-banner-wrap relative">
 <div class="layer"></div>
 <img class="mobile-banner" >
</div>

<div  id="parallax-wrap" class="parallax-search" 
data-parallax="scroll" data-position="center" data-bleed="0" 
data-image-src="<?php echo assetsURL()."/images/banner.jpg"?>">


<style>
@media (min-width: 992px) {
.parallax-search {
    min-height: 550px;
}
.home-title {
	color:#fff;
	margin-top:0;
    font-family: 'Open Sans';
    font-weight: 700;
    font-size: 56px;
    /*text-transform: uppercase;*/
    text-shadow: 1px 3px 3px rgba(0, 0, 0, 0.35);
}
.home-desc {
	color:#fff;
	text-shadow: 1px 3px 3px rgba(0, 0, 0, 0.35);
	font-size:18px;
}
}
</style>
<?php
$h1 = "Pencarian";
$sub_text = "";//"Anda dapat melakukan filter dan pencarian berdasarkan kategori, grosir/eceran, kata kunci, dan juga lokasi, sehingga Anda dapat menemukan apa yang Anda butuhkan.";
?>
<div class="search-wraps2 hidden-xs hidden-sm hidden-md">
	<h1><?php echo isset($h1)?$h1:''?></h1>
	<p><?php echo isset($sub_text)?$sub_text:''?></p>
</div> <!--search-wraps-->

<div class="search-wraps hidden-lg ">
	<h1><?php echo isset($h1)?$h1:''?></h1>
	<p><?php echo isset($sub_text)?$sub_text:''?></p>
</div> <!--search-wraps-->

<style>
.box-filter .form-control {
	height: 40px;
	border-radius:0px;
	width: 100%;
}
@media (max-width: 1200px) {
	.btn-find {
		margin:auto;
		float:none;
		display:block;
	}
}
.col-filter {
	margin-bottom:10px;
}
@media (min-width: 992px) {
	.col-filter {
		padding-left:0px;
		padding-right:0px;
		width:25%;
		margin-bottom:10px;
	}
	.col-two {
		padding-left: 0px !important;
		margin-left: -10px;
		z-index: 2;
	}

	.col-three {
		padding-left: 0px !important;
		margin-left: -10px;
		z-index: 3;
	}
	.col-four {
		padding-left: 0px !important;
		margin-left: -10px;
		z-index: 3;		
	}
	.col-five {
		z-index: 3;		
		width:100%;
	}

}
@media (min-width: 1200px) {
	.col-filter {
		padding-left:0px;
		padding-right:0px;
		width:21%;
	}
	.col-two {
		padding-left: 0px !important;
		margin-left: -10px;
		z-index: 2;
	}

	.col-three {
		padding-left: 0px !important;
		margin-left: -10px;
		z-index: 3;
	}
	.col-four {
		padding-left: 0px !important;
		margin-left: -10px;
		z-index: 3;		
	}
	.col-five {
		z-index: 3;		
		width:18%;
	}
}
.btn-find {
	width: 180px;
    font-size: 20px;
    font-family: 'Helvetica Neue';
	background-color:#bf1e2e;
}
.btn-find:hover {
	color: #fff;
	background-color: #dc2e40;
	border-color: #bf1e2e;
}
			div.selectBox{width:100%;position: relative; display: inline-block; cursor: default; text-align: left; line-height: 30px; clear: both; color: rgb(114, 97, 97);}
			span.selected{width: 87%; text-indent: 10px; border: 2px solid #e5e5e5; border-right: none; border-top-left-radius: 5px; border-bottom-left-radius: 5px; background: #fff; overflow: hidden;font-size: 17px;color: #708090;
                        }
			span.selectArrow{width: 13%; background: #fff;border: 2px solid #e5e5e5;border-left:none; color: #dc0101; border-top-right-radius: 5px; border-bottom-right-radius: 5px; text-align: center; font-size: 30px; -webkit-user-select: none; -khtml-user-select: none; -moz-user-select: none; -o-user-select: none; user-select: none; }
			@media (min-width: 992px) {
				span.selected{width: 82%;}
				span.selectArrow{width: 17%;}
				
			}
			span.selectArrow,span.selected{position: relative; float: left; height: 50px; z-index: 1;padding: 8px;}
			ul.selectOptions{    width: 98%;
				position: absolute;
				top: 49px;
				left: 0;
				border: 1px solid #e5e5e5;
				overflow: hidden;
				background: rgb(250, 250, 250);
				padding-top: 2px;
				margin: 0;
				list-style: none inside none;
				padding-left: 0;
				z-index: 7;
				border-radius: 5px;
				display: none;
			}
			li.selectOption{
				display: block !important;
				line-height: 20px;
				padding: 8px 0 5px 10px !important;
				font-size: 15px !important;
				list-style: none;
				margin: 0;
				height: 40px;
			}
			li.selectOption:hover{color: #f6f6f6;background: #4096ee;}
			.box-filter .form-control {
				height: 50px;
				border-radius:5px;
				border: 1px solid #708090;
				color:#708090;
				padding-left: 18px;
				border: 2px solid #e5e5e5;
			}
			.box-filter {
				font-family: 'Open Sans';
				font-size: 16px;
				/*color: #3a3838;*/
				
			}

		</style>
	
<?php 
$latitude = "";
$longitude = "";
$address = "";
if(isset($_SESSION['client_location']['lat'])) {
	if($_SESSION['client_location']['lat']) {
		$latitude = $_SESSION['client_location']['lat'];
		$longitude = $_SESSION['client_location']['long'];
	}
	$address=$_SESSION['client_location']['client_address'];
}

?>
<div class="container box-filter" style="margin-top:30px">
<form class="filter-forms" onsubmit="return formfilter()">
<input type="hidden" id="sortby" name="sortby" value="" />
<input type="hidden" id="action" name="action" value="searchMerchant" />
<input type="hidden" id="address" name="address" value="<?=$address?>"/>
<input type="hidden" id="url_ajax" name="url_ajax" value="<?= Yii::app()->baseUrl ?>/mobileapp/api"/>
<div class="row margin-bottom-10">
	<div class="col-md-3 col-filter"> 
		<div class='selectBox'>
			<?php 
			$result = query("SELECT * FROM {{category}}");
			$category_name= "-- Kategori --";
			foreach($result as $r) {
				if($category == $r['cat_id']){
					$category_name = $r['category_name'];
				}
			}
			?>
			<input type="hidden" name="category" value="<?=$category?>" class="se-category"/>
			<span class='selected selected-category' ><?=$category_name?></span>
			<span class='selectArrow'><span class="fa fa-angle-down"></span> </span>
			<ul class="selectOptions" data-class="se-category">
				<li class="selectOption" data-value="">-- <?=t('Category')?> --</li>
				<?php 
				foreach($result as $r) {
					echo '<li class="selectOption" data-value="'.$r['cat_id'].'" '. (($category == $r['cat_id']) ? 'selected':'') .' >'.$r['category_name'].'</li>';
				}
				?>
			</ul>
		</div>
	</div>
	<div class="col-md-3 col-filter col-two">  	
		<input type="text" class=" form-control keyword" name="keyword" value="<?=$keyword?>" placeholder="<?=t('Search Product or Merchant')?>">
	</div>
	<div class="col-md-3 col-filter col-three">
		<div class='selectBox'>
			<input name="sortby" type="hidden" value="distance" class="se-sortby"/>
			<input name="satuan" type="hidden" value="<?=$satuan['value']?>" class="se-satuan"/>
			<span class='selected'><?=$satuan['label']?></span>
			<span class='selectArrow'><span class="fa fa-angle-down"></span>  </span>
			<ul class="selectOptions" data-class="se-satuan" >
				<li class="selectOption" data-value="">-- Satuan --</li>
				<li class="selectOption" data-value="grosir"><?=t('Wholesale')?></li>
				<li class="selectOption" data-value="eceran"><?=t('Retail')?></li>
			</ul>
		</div>
	</div>
	<div class="col-md-3 col-filter col-four">
		<div class='selectBox'>
			<input type="hidden" name="location" value="<?=$location['value']?>" class="se-location"/>
			<span class='selected'><?=$location['label']?></span>
			<span class='selectArrow'><span class="fa fa-angle-down"></span>  </span>
			<ul class="selectOptions" data-class="se-location"  >
				<li class="selectOption" data-value="">-- <?=t('Location')?> --</li>
				<li class="selectOption" data-value="semua"><?=t('All')?></li>
				<li class="selectOption" data-value="city"><?=t('City')?></li>
				<li class="selectOption" data-value="current"><?=t('Current Location')?></li>
			</ul>
		</div>
		
		<input  style="display:none" type="text" class="margin-top-10 form-control city" name="city" id="city" placeholder="<?=t('Type City Location ...')?>" disabled>
	</div>
	<div class="col-md-3 col-five">
		<button class="btn btn-danger btn-lg btn-find">Cari</button>
	</div>
</div>
<div class="row margin-top-20">

	<div class="col-md-4 hidden">
		<div class='selectBox'>
			<input type="hidden" name="output" value="<?=$output['value']?>" class="se-output"/>
			<span class='selected'><?=$output['label']?></span>
			<span class='selectArrow'><span class="fa fa-angle-down"></span>  </span>
			<ul class="selectOptions"  data-class="se-output">
				<li class="selectOption" data-value="merchant"><?=t('Merchant')?></li>
				<li class="selectOption" data-value="product"><?=t('Product')?></li>
			</ul>
		</div>
	</div>
	<div class="col-md-3 col-md-offset-9">
		
	</div>
</div>
</form>
</div>


</div> <!--parallax-container-->


<div class="sections section-grey2 section-browse" id="section-browse">
 <div class="container-fluid">
	<?php
		if (is_array($list['list']) && count($list['list'])>=1){
			$this->renderPartial('/front/browse-list',array(
			   'list'=>$list,
			   'tabs'=>$tabs
			));
		} else echo '<p class="text-danger">'.t("No restaurant found").'</p>';
	?>
 </div><!-- container-->

</div> <!--sections-->