<?php 
if (isset($_GET['keyword'])) {
	$keyword=$_GET['keyword'];
} else {
	$keyword = "";
}
if (isset($_GET['location'])) {
	if($_GET['location'] == 'semua') {
		$location = array('label' => t('Semua'),'value' => 'semua');
	} else if ($_GET['location'] == 'city') {
		$location = array('label' => t('Kota'),'value' => 'city');
	} else if ($_GET['location'] == 'current') {
		$location = array('label' => t('Lokasi Saat Ini'),'value' => 'current');
	} else {
		$location = array('label' => 'Lokasi','value' => '');
	}
} else {
	$location = array('label' => 'Lokasi','value' => '');
}
if (isset($_GET['category'])) {
	$category = $_GET['category'];
} else {
	$category = "";
}
if (isset($_GET['satuan'])) {
	if($_GET['satuan'] == 'grosir') {
		$satuan = array('label' => t('Grosir'),'value' => 'grosir');
	} else if ($_GET['satuan'] == 'eceran') {
		$satuan = array('label' => t('Eceran'),'value' => 'eceran');
	} else {
		$satuan = array('label' => '-- Satuan --','value' => '');
	}
} else {
	$satuan = array('label' => '-- Satuan --','value' => '');
}
if (isset($_GET['o'])) {
	if($_GET['o'] == 'product') {
		$output = array('label' => t('Product'),'value' => 'product');
	} else if ($_GET['o'] == 'merchant') {
		$output = array('label' => 'Merchant','value' => 'merchant');
	} else {
		$output = array('label' => 'Merchant','value' => 'merchant');
	}
} else {
	$output = array('label' => 'Merchant','value' => 'merchant');
}
?>
<?php
$kr_search_adrress = FunctionsV3::getSessionAddress();

$home_search_text=Yii::app()->functions->getOptionAdmin('home_search_text');
if (empty($home_search_text)){
	$home_search_text=Yii::t("default","Find food stores near you");
}

$home_search_subtext=Yii::app()->functions->getOptionAdmin('home_search_subtext');
if (empty($home_search_subtext)){
	$home_search_subtext=Yii::t("default","Order Delivery Food Online From Available Merchants");
}

$home_search_mode=Yii::app()->functions->getOptionAdmin('home_search_mode');
$placholder_search=Yii::t("default","Street Address,City,State");
if ( $home_search_mode=="postcode" ){
	$placholder_search=Yii::t("default","Enter your postcode");
}
$placholder_search=Yii::t("default",$placholder_search);
?>

<img class="mobile-home-banner" src="<?php echo assetsURL()."/images/banner.jpg"?>">

<div id="parallax-wrap" class="parallax-container margin-bottom-50" 
data-parallax="scroll" data-position="center" data-bleed="0" 
data-image-src="<?php echo assetsURL()."/images/banner.jpg"?>">


<style>
.col-filter {
	margin-bottom:10px;
}
@media (min-width: 1200px) {

	.parallax-container {
		border: 0px solid red;
		min-height: 670px;
		width: 100%;
		background: rgb(0, 0, 0);
		background: rgba(0,0,0,0.09);
	}

}
</style>


<div class="search-wraps">
<div class="row" style="">
	<div class="col-md-12 col-lg-8 col-lg-offset-2">
		<h1 class="home-title"><?php echo t("Cari Kebutuhan Pangan <br/>Anda Disini")?></h1>
		<p class="home-desc"><?php echo t("Anda dapat mencari apa yang Anda butuhkan seperti sembako, produk makanan, kebutuhan sehari-hari, disini. Anda dapat melakukan pemesanan dari berbagai merchant yang tersedia. Anda tinggal order, bayar, dan produk yang Anda pesan kami antar ke rumah Anda. Praktis kan :)")?></p>
	</div>
</div>
</div> <!--search-wraps-->

<style>
.search-wraps h1
{
color:#fff;
text-align:center;
font-weight: 600;
font-family: 'Raleway', sans-serif;
font-size: 45px;
}
.search-wraps p,
.search-wraps p a
{
text-align:center;
color:#fff;
margin-bottom:20px;
font-family: "Raleway",sans-serif;
    font-weight: 500;
}
.col-filter {
	margin-bottom:10px;
}
.box-filter .form-control {
	height: 50px;
    border-radius: 5px;
    border: 1px solid #708090;
    color: #708090;
    border: 2px solid #e5e5e5;
    padding-top: 3px;
    font-family: 'Helvetica Neue';
}
@media (min-width: 992px) {
	.col-one {
		margin-left:3%;
	}
	#city {
		width:100%;
	}
}
@media (max-width: 992px) {
	
	.keyword {
		width:100%;
	}
	
	#city {
		width:100%;
	}

}

.btn-find {
	width: 180px;
    font-size: 20px;
    font-family: 'Helvetica Neue';
	background-color:#bf1e2e;
}
.btn-find:hover {
	color: #fff;
	background-color: #dc2e40;
	border-color: #bf1e2e;
}
			div.selectBox{width:100%;position: relative; display: inline-block; cursor: default; text-align: left; line-height: 30px; clear: both; color: rgb(114, 97, 97);}

			span.selected{width: 87%; text-indent: 10px; border: 2px solid #e5e5e5; border-right: none; border-top-left-radius: 5px; border-bottom-left-radius: 5px; background-color: #fff; overflow: hidden;font-size: 17px;color: #708090;
						}
			span.selectArrow{width: 13%; background: #fff;border: 2px solid #e5e5e5;border-left:none; color: #d24552; border-top-right-radius: 5px; border-bottom-right-radius: 5px; text-align: center; font-size: 30px; -webkit-user-select: none; -khtml-user-select: none; -moz-user-select: none; -o-user-select: none; user-select: none; }
			span.selectArrow,span.selected{position: relative; float: left; height: 50px; z-index: 1;padding: 8px;}
			ul.selectOptions{    width: 98%;
				position: absolute;
				top: 49px;
				left: 0;
				border: 1px solid #e5e5e5;
				overflow: hidden;
				background: rgb(250, 250, 250);
				padding-top: 2px;
				margin: 0;
				list-style: none inside none;
				padding-left: 0;
				z-index: 7;
				border-radius: 5px;
				display: none;
			}
			li.selectOption{
				display: block !important;
				line-height: 20px;
				padding: 8px 0 5px 10px !important;
				font-size: 15px !important;
				list-style: none;
				margin: 0;
				height: 40px;
			}
			li.selectOption:hover{color: #f6f6f6;background: #4096ee;}
			.box-filter .form-control {
				height: 50px;
				border-radius:5px;
				border: 1px solid #708090;
				color:#708090;
				padding-left: 18px;
				border: 2px solid #e5e5e5;
			}
			.box-filter {
				font-family: 'Open Sans';
				font-size: 16px;
				/*color: #3a3838;*/
				
			}
			.keyword {
				background-repeat: no-repeat;background-size: 20px;background-position: 10px 10px;background-image:url('<?php echo assetsURL()."/images/search.png";?>');
				font-family: 'Helvetica Neue';
				font-size:17px;
				padding-left: 48px !important;
			}
			.keyword::-webkit-input-placeholder { /* Chrome/Opera/Safari */
			  color: #aeb3b7;
			}
			.keyword::-moz-placeholder { /* Firefox 19+ */
			  color: #aeb3b7;
			}
			.keyword:-ms-input-placeholder { /* IE 10+ */
			  color: #aeb3b7;
			}
			.keyword:-moz-placeholder { /* Firefox 18- */
			  color: #aeb3b7;
			}

			.selected-location {
				background-repeat: no-repeat;background-size: 23px;background-position: 13px 13px;background-image:url('<?php echo assetsURL()."/images/Location.png";?>');
				padding-left: 43px !important;
				font-family: 'Helvetica Neue';
			}
			
		</style>

<div class="container box-filter" style="margin-top:50px">
<form class="filter-forms" action="<?= Yii::app()->baseUrl ?>/store/browse"   method="post">
<input type="hidden" id="sortby" name="sortby" value="" />
<input type="hidden" id="action" name="action" value="searchMerchant" />
<input type="hidden" id="address" name="address" value="<?=$address?>"/>
<input type="hidden" id="url_ajax" name="url_ajax" value="<?= Yii::app()->baseUrl ?>/mobileapp/api"/>
<div class="row margin-bottom-10">
	<div class="col-md-4 col-filter col-one">
		<div class='selectBox selectBox-location'>
			<input type="hidden" name="location" value="<?=$location['value']?>" class="se-location"/>
			<span class='selected selected-location'><?=$location['label']?></span>
			<span class='selectArrow selectArrow-location'><span class="fa fa-angle-down"></span>  </span>
			<ul class="selectOptions" data-class="se-location"  >
				<li class="selectOption" data-value=""><?=t('Location')?></li>
				<li class="selectOption" data-value="semua"><?=t('All')?></li>
				<li class="selectOption" data-value="city"><?=t('City')?></li>
				<li class="selectOption" data-value="current"><?=t('Current Location')?></li>
			</ul>
		</div>
		
		<input  type="text" style="display:none" class="margin-top-10 form-control city" name="city" id="city" placeholder="<?=t('Type City Location and press enter...')?>" disabled>
	</div>
	<div class="col-md-4 col-filter col-md-offset-1 hidden"> 
		<div class='selectBox'>
			<?php 
			$result = query("SELECT * FROM {{category}}");
			$category_name= "-- Kategori --";
			foreach($result as $r) {
				if($category == $r['cat_id']){
					$category_name = $r['category_name'];
				}
			}
			?>
			<input type="hidden" name="category" value="<?=$category?>" class="se-category"/>
			<span class='selected selected-category' ><?=$category_name?></span>
			<span class='selectArrow'><span class="fa fa-angle-down"></span> </span>
			<ul class="selectOptions" data-class="se-category">
				<li class="selectOption" data-value="">-- <?=t('Category')?> --</li>
				<?php 
				foreach($result as $r) {
					echo '<li class="selectOption" data-value="'.$r['cat_id'].'" '. (($category == $r['cat_id']) ? 'selected':'') .' >'.$r['category_name'].'</li>';
				}
				?>
			</ul>
		</div>
	</div>
	<div class="col-md-5 col-filter  ">  	
		<input style="" type="text" class=" form-control keyword" name="keyword" value="<?=$keyword?>" placeholder="<?=t('Search Product or Merchant')?>">
	</div>
	<div class="col-md-3 col-filter hidden">
		<div class='selectBox'>
			<input name="sortby" type="hidden" value="" class="se-sortby"/>
			<input name="satuan" type="hidden" value="<?=$satuan['value']?>" class="se-satuan"/>
			<span class='selected'><?=$satuan['label']?></span>
			<span class='selectArrow'><span class="fa fa-angle-down"></span>  </span>
			<ul class="selectOptions" data-class="se-satuan" >
				<li class="selectOption" data-value="">-- Satuan --</li>
				<li class="selectOption" data-value="grosir"><?=t('Wholesale')?></li>
				<li class="selectOption" data-value="eceran"><?=t('Retail')?></li>
			</ul>
		</div>
	</div>
	<div class="col-md-2 col-filter ">
		<button class="btn btn-danger btn-lg btn-find">Cari</button>
	</div>
</div>
<div class="row margin-top-20">

	<div class="col-md-4 hidden">
		<div class='selectBox'>
			<input type="hidden" name="output" value="<?=$output['value']?>" class="se-output"/>
			<span class='selected'><?=$output['label']?></span>
			<span class='selectArrow'><span class="fa fa-angle-down"></span>  </span>
			<ul class="selectOptions"  data-class="se-output">
				<li class="selectOption" data-value="merchant"><?=t('Merchant')?></li>
				<li class="selectOption" data-value="product"><?=t('Product')?></li>
			</ul>
		</div>
	</div>
	<div class="col-md-3 col-md-offset-9">
		
	</div>
</div>
</form>
</div>



</div> <!--parallax-container-->



<div class="main-content margin-top-50 homecategory">
<div class="container" style="margin-bottom:70px;">
	<div class="row margin-top-10">
		<div class="col-md-12 ">
				<div class="block">
					<div class="block-heading" style="margin-top:0;margin-bottom:0">
						<h2><?=t("Categories")?></h2>
					</div>
				</div>
		</div>
	</div>
</div>
<div class="container">
<?php 
$result = FunctionsV3::queryO('SELECT * FROM {{category}}');
// dump($result);
foreach ($result as $r) {
?>
		<div class="col-md-4 margin-bottom-50">
			<a target="_blank" href="<?= Yii::app()->createUrl('/store/browse?category='.$r->cat_id.'&o=product')?>">
			<?php if($r->photo) {
				?>
				<img width="100%" src="<?php echo uploadURL()."/".$r->photo;?>"/>
				<?php
			} else {
				?>
				<img width="100%" src="<?php echo assetsURL()."/images/category.jpg";?>"/>
				<?php
			}
			?>
			</a>
		</div>

<?php 
}
?>
</div><!-- container -->
</div>

<div class="sections section-feature-resto">

<div class="container margin-top-10 ">
<?php 
$distance_exp = 6000;
$sql_near_location = "";
if(isset($_SESSION['client_location']['lat'])) {
	if($_SESSION['client_location']['lat']) {
		$lat = $_SESSION['client_location']['lat'];
		$long = $_SESSION['client_location']['long'];
		
		$sql_near_location = ", 
						 ( $distance_exp * acos( cos( radians($lat) ) * cos( radians( latitude ) ) 
						 * cos( radians( longitude ) - radians($long) ) 
						 + sin( radians($lat) ) * sin( radians( latitude ) ) ) ) 
						 AS distance ";
	}
}

$result = FunctionsV3::queryO("SELECT b.merchant_id,b.merchant_name,b.merchant_slug,CONCAT(b.street) address,b.city,a.wholesale_flag $sql_near_location
								FROM {{view_merchant}} b 
								LEFT JOIN {{item}} a
								ON a.merchant_id=b.merchant_id 
								WHERE a.status='publish'
								GROUP BY a.merchant_id
								
								LIMIT 0,9
								");
// dump($result);
?>

	<div class="row">
		<div class="col-md-12 margin-bottom-10">
				<div class="block">
					<div class="block-heading">
						<h2><?=t("Hot List Merchants")?></h2>
					</div>
				</div>
		</div>
	</div>
	
	<style>
	.thumb-red {
		background-color: #bf1e2e;
		height: 40px;
		width: 70%;
		color: white;
		font-size: 13px;
		float: left;
		display: table;
		padding-left: 10px;
		font-family: 'Open Sans';
		font-weight: 500;
	}
	.thumb-inside2 {
		display: table-cell; vertical-align: middle;
		font-size:15px;
		height:44px;
		padding-left:3px;
	}
	.thumb-rating {
		height: 44px;
		width: 100%;
		font-size: 13px;
		float: left;
		padding: 9px 8px;
		font-family: 'Open Sans';
		font-weight: 500;
		text-align:center;
	}
	.btn-beli2 {
		background-color: #bf1e2e;
		color: white;
		height: 44px;
		width: 30%;
		border-left: 1px solid white;
		display: inline-block;
		padding: 12px 8px !important;
		font-size: 14px;
		padding: 10px 8px;
		text-align: center;
		font-family: 'Raleway';
		font-weight: 500;
	}
	.capt-retail-price2 {
		font-size: 16px;
	}
	.caption-merchant2 {
		height:60px;
		font-family: 'Raleway';
		font-weight: 500;
		padding:5px !important;
		font-size: 15px;
	}
	@media (min-width: 992px) {
		.sponsored-merchant {
			width:20%;
			padding-left:0;
			padding-right:0;
			margin-top:3px;
		}
		.sidebar-filter {
			width:18.66666%;
			height: auto;
			min-height: 1100px;
		}
		.result-merchant {
			width:61.33333%;
		}
		.result-merchant .box-grey {
			height: auto;
			min-height: 1100px;
		}		

	}

	</style>
	<div class="row">
		<?php foreach ($result as $r) { ?>
				<div class="template-merchant ">
					<div class="col-sm-6 col-md-6 col-lg-4">
						<a href="<?= Yii::app()->createUrl('store/menu/merchant/'.$r->merchant_slug)?>">
						<div class="thumbnail no-padding thumb-search" style="height:340px;border: 0px solid #dcd9d9;">
							<div src="" class="img-responsive logo-search2" style="background-image:url('<?php echo FunctionsV3::getMerchantLogo($r->merchant_id);?>')">
								<?php if($r->wholesale_flag == 0) {?>
								<span class="center label label-info" style="position: absolute;right: 25px;top: 15px;font-size: 13px;">eceran</span>
								<?php } else { ?>
								<span class="center label label-success" style="position: absolute;right: 25px;top: 15px;font-size: 13px;">grosir</span>
								<span class="center label label-info" style="position: absolute;right: 25px;top: 43px;font-size: 13px;">eceran</span>
								<?php } ?>
								<span class="capt-distance2 label label-warning"><?=isset($r->distance)?number_format($r->distance, 2, ',', ' ')." km":""?></span>
							</div>
							<div class="caption caption-merchant2 margin-top-0">
								<span class="capt-search2 "><?=$r->merchant_name?></span>
								<span class="capt-retail-price2 "><?=$r->address?></span>
								<span class="capt-city"></span>
							</div>
							<div class="thumb-red"><div class="thumb-inside2"><div><?=$r->city?></div></div> </div><span class="btn-beli2">LIHAT</span>

						</div>
						</a>
					</div>
				</div>
		<?php } ?>
	</div>
</div>
</div>


<div class="sections section-feature-resto">

<div class="container margin-top-0 ">
<?php 
$result = FunctionsV3::queryO("SELECT a.*,b.merchant_name,b.merchant_slug,CONCAT(b.street,', ',b.city) address,COUNT(c.id) wholesale $sql_near_location
								FROM {{item}} a 
								JOIN {{view_merchant}} b 
								ON a.merchant_id=b.merchant_id 
								LEFT JOIN {{wholesale_price}} c
								ON c.item_id=a.item_id
								WHERE a.status='publish'
								AND a.photo != ''
								GROUP BY a.item_id
								ORDER BY a.item_id DESC
								LIMIT 0,6
								");
// dump($result);
?>

	<div class="row">
		<div class="col-md-12 margin-bottom-10">
				<div class="block">
					<div class="block-heading">
						<h2><?=t("Hot List Products")?></h2>
					</div>
				</div>
		</div>
	</div>
	<style>
	.label-grosir-eceran {
	/*font-size: 13px;
    position: absolute;
    transform: rotate(45deg) !important;
    right: -40px;
    top: 13px;
    height: 30px;
    content: 'eceran';
    padding: 8px 20px;
    border-radius: 0px;
    width: 140px;
    text-transform: uppercase;
    font-family: 'Open Sans';
	*/
	position: absolute;right: 15px;top: 15px;font-size: 13px;
	}
.capt-price {
	right: 28px;
    position: absolute;
    bottom: 45px;
    font-size: 15px;
    background: #e6e3e3;
    color: #228b22;
    font-family: 'Raleway';
    font-weight: 500;
}
	</style>
	
	<div class="row">
		<?php foreach ($result as $r) { ?>
		<div class="template-merchant"> 
			<div class="col-sm-6 col-md-6 col-lg-4">

						<a href="<?= Yii::app()->createUrl('store/menu/merchant/'.$r->merchant_slug)?>">
						<div class="thumbnail no-padding thumb-search" style="height:340px;border: 0px solid #dcd9d9;">
							<div src="" class="img-responsive logo-search2" style="background-image:url('<?php echo uploadURL()."/".$r->photo;?>')">
								<?php if($r->wholesale <= 0) {?>
								<span class="center label label-info" style="position: absolute;right: 25px;top: 15px;font-size: 13px;">eceran</span>
								<?php } else { ?>
								<span class="center label label-success" style="position: absolute;right: 25px;top: 15px;font-size: 13px;">grosir</span>
								<span class="center label label-info" style="position: absolute;right: 25px;top: 43px;font-size: 13px;">eceran</span>
								<?php } ?>
								<span class="capt-distance2 label label-warning"><?=isset($r->distance)?number_format($r->distance, 2, ',', ' ')." km":""?></span>
							</div>
							<div class="caption caption-merchant2 margin-top-0">
								<span class="capt-search2 "><?=$r->item_name?></span>
								<span class="capt-retail-price2 ">Eceran <?=FunctionsV3::prettyPriceUnit($r->retail_price,$data['uom'])?></span>
								<span class="capt-city"><?=$r->city?></span>
							</div>
							<div class="thumb-red"><div class="thumb-inside2"><div><?=$r->merchant_name?></div></div> </div><span class="btn-beli2">BELI</span>
						</div>
						</a>
			</div>
		</div>
		<?php } ?>
	</div>
	<div class="row hot-list hidden">
		<?php foreach ($result as $r) { ?>
		<div class="col-md-4 grid" style="border-radius:7px">
			
			<figure class="effect-sadie"  style="border-radius:7px">
				<div class="item-box"  style="border-radius:7px">
				<?php 
				if($r->photo) {
					?><img  style="border-radius:7px" class="item-img" src="<?php echo uploadURL()."/".$r->photo;?>"/><?php
				} else {
					?><img class="item-img" src="<?php echo assetsURL()."/images/item.jpg";?>"/><?php
				}
				?>
				</div>
				<div class="black-opacity"></div>
				<figcaption   style="border-radius:7px">
					<a href="<?php echo Yii::app()->createUrl('/store/product/'. $r->item_id );?>"></a>
					<h2 class="h2" style="font-size:23px"><?=$r->item_name?></h2>
					<p><?php echo $r->merchant_name;//if(strlen($r->item_description) < 70) { echo $r->item_description;} else {echo substr($r->item_description,0,70).'...';}?></p>
					<?php if($r->wholesale > 0) {?>
					<span class="label label-info label-grosir">GROSIR </span>
					<?php } else {?>
					<span class="label label-success label-eceran">ECERAN </span>
					<?php } ?>
				</figcaption>
			</figure>
			
		</div>
		<?php } ?>
	</div>
</div>
</div>



<!--FEATURED RESTAURANT SECIONS-->
<?php if ($disabled_featured_merchant=="" && false):?>
<?php if ( getOptionA('disabled_featured_merchant')!="yes"):?>
<?php if ($res=Yii::app()->functions->getFeatureMerchant2()):?>
<div class="sections section-feature-resto">
<div class="container">
  <h2><?php echo t("Featured Restaurants")?></h2>
  
  <div class="row">
  <?php foreach ($res as $val): //dump($val);?>
  <?php $address= $val['street']." ".$val['city'];
        $address.=" ".$val['state']." ".$val['post_code'];
        
        $ratings=Yii::app()->functions->getRatings($val['merchant_id']);
  ?>   
  
    <a href="<?php echo Yii::app()->createUrl('/store/menu/merchant/'. trim($val['merchant_slug']) )?>">
    <div class="col-md-5 border-light ">
    
        <div class="col-md-3 col-sm-3">
           <img class="logo-small" src="<?php echo FunctionsV3::getMerchantLogo($val['merchant_id']);?>">
        </div> <!--col-->
        
        <div class="col-md-9 col-sm-9">
        
          <div class="row">
              <div class="col-sm-5">
		          <div class="rating-stars" data-score="<?php echo $ratings['ratings']?>"></div>   
	          </div>
	          <div class="col-sm-2 merchantopentag">
	          <?php echo FunctionsV3::merchantOpenTag($val['merchant_id'])?>   
	          </div>
          </div>
          
          <h4 class="concat-text"><?php echo clearString($val['merchant_name'])?></h4>
          
          <p class="concat-text"><?php echo $address?></p>                             
          <?php echo FunctionsV3::displayServicesList($val['service'])?>          
        </div> <!--col-->
        
    </div> <!--col-6-->
    </a>
    <div class="col-md-1"></div>
      
  <?php endforeach;?>
  </div> <!--end row-->

  
</div> <!--container-->
</div>
<?php endif;?>
<?php endif;?>
<?php endif;?>
<!--END FEATURED RESTAURANT SECIONS-->



<?php if ($theme_show_app==2 || true):?>
<!--MOBILE APP SECTION-->
<div id="parallax-wrap" class="parallax-container parallax-container-2 parallax-home margin-top-50" 
data-parallax="scroll" data-position="top" data-bleed="0" 
data-image-src="<?php echo assetsURL()."/images/banner.jpg"?>" style="min-height:470px">

<div id="mobile-app-sections" class="container">
<div class="container-medium">
  <div class="row margin-top-30">
     <div class="col-xs-5 into-row border app-image-wrap">
       <img class="app-phone" style="height:350px" src="<?php echo assetsURL()."/images/getapp.png";?>">
     </div> <!--col-->
     <div class="col-xs-7 into-row border" style="color:#fff">
       <h2>Kini telah tersedia aplikasinya dalam genggaman Anda! </h2>
       <p>Aplikasi telah tersedia dalam versi Android maupun Ios, Anda dapat mendownload aplikasinya di Appstore maupun Playstore.</p>
       <p>Dengan menggunakan aplikasi, proses belanja anda bisa lebih nyaman
       <div class="row border" id="getapp-wrap">
         <div class="col-xs-5 border">
           <a href="<?php echo $theme_app_ios?>" target="_blank">
           <img class="get-app" src="<?php echo assetsURL()."/images/get-app-store.png"?>">
           </a>
         </div>
         <div class="col-xs-5 border">
           <a href="<?php echo $theme_app_android?>" target="_blank">
             <img class="get-app" src="<?php echo assetsURL()."/images/get-google-play.png"?>">
           </a>
         </div>
       </div> <!--row-->

     </div> <!--col-->
	 


  </div> <!--row-->

  </div> <!--container-medium-->
  
  <div class="mytable border" id="getapp-wrap2">
     <div class="mycol border">
           <a href="<?php echo $theme_app_ios?>" target="_blank">
           <img class="get-app" src="<?php echo assetsURL()."/images/get-app-store.png"?>">
           </a>
     </div> <!--col-->
     <div class="mycol border">
          <a href="<?php echo $theme_app_android?>" target="_blank">
             <img class="get-app" src="<?php echo assetsURL()."/images/get-google-play.png"?>">
           </a>
     </div> <!--col-->
  </div> <!--mytable-->
  
  
</div> <!--container-->

</div>
<!--END MOBILE APP SECTION-->
<?php endif;?>


	   <?php 
			// var_dump($homestat);
			// regenerate $value because it is not found in cache
			// and save it in cache for later use:
			$cat = query("SELECT COUNT(*) as total FROM {{category}}");
			$item = query("SELECT COUNT(*) as total FROM {{item}} WHERE status='publish'");
			$review = query("SELECT COUNT(*) as total FROM {{review}} ");
			$merchant = query("SELECT COUNT(*) as total FROM {{merchant}} WHERE status='active'");
			$order = query("SELECT COUNT(*) as total FROM {{order}}");
			$client = query("SELECT COUNT(*) as total FROM {{client}}");
			$h = array(
				'cat' => $cat[0]['total'],
				'item' => $item[0]['total'],
				'review' => $review[0]['total'],
				'order' => $order[0]['total'],
				'client' => $client[0]['total'],
				'merchant' => $merchant[0]['total'],
			);
	   ?>
<style>
.homestat h1 {
	font-size: 49px;
    font-weight: 100;
    font-family: "Raleway";
    color: #656565;
}
.homestat h3 {
	font-size: 24px;
    font-weight: 300;
    font-family: "Raleway";
    color: #656565;
}
.box-stat {
	/*border: 1px solid #adacac;*/
    margin: 8px;
    border-radius: 50%;
    height: 170px;
    width: 180px;
    padding-top: 17px;
	background-color:#efefef;
}
</style>
<div class="sections section-feature-resto margin-top-20">
<div class="container ">
  <div class="row homestat">
       <div class="col-sm-2 center">
		   <div class="box-stat">
			   <h1><?=$h['cat']?></h1>
			   <h3><?=t('Categories')?></h3>
		   </div>
	   </div>
       <div class="col-sm-2 center">
		   <div class="box-stat">
			   <h1><?=$h['item']?></h1>
			   <h3><?=t('Products')?></h3>
		   </div>
	   </div>
       <div class="col-sm-2 center">
		   <div class="box-stat">
			   <h1><?=$h['review']?></h1>
			   <h3><?=t('Reviews')?></h3>
		   </div>
	   </div>
       <div class="col-sm-2 center">
		   <div class="box-stat">
		   <h1><?=$h['order']?></h1>
		   <h3><?=t('Orders')?></h3>
		   </div>
	   </div>
       <div class="col-sm-2 center">
		   <div class="box-stat">
		   <h1><?=$h['client']?></h1>
		   <h3><?=t('Customers')?></h3>
		   </div>
	   </div>
       <div class="col-sm-2 center">
		   <div class="box-stat">
		   <h1><?=$h['merchant']?></h1>
		   <h3><?=t('Merchants')?></h3>
		   </div>
	   </div>
  </div> <!--row-->
</div>
</div>
<script>
	// var this2 = document.getElementsByClassName('h2');
	// console.log(this2);
    // styleHook = 'classNameToUse' || 'secondWord';
    // var text = '',
        // words = [];
    // for (var i = 0, len = this2.length; i<len; i++){
        // words = (this2[i].textContent || this2[i].innerText).split(/\s+/);
        // if (words[1]) {
            // words[1] = '<span class="' + styleHook + '">' + words[1] + '</span>';
            // this2[i].innerHTML = words.join(' ');
        // }
    // }
</script>
 
