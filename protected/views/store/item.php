<?php 
$item_found=false;
if (is_array($data) && count($data)>=1){
	$data=$data[0];
	$item_found=true;
} else $data['item_name']='';

$slug=isset($this_data['slug'])?$this_data['slug']:'';

$this->renderPartial('/front/mobile_header',array(
    'slug'=> $slug,
    'title'=>$data['item_name']
));
?>

<div class="container">

<?php if ($item_found==true):?>
<?php 
$row='';
$item_data='';
$price_select='';
$size_select='';
if (array_key_exists("row",(array)$this_data)){
	$row=$this_data['row'];	
	$item_data=$_SESSION['kr_item'][$row];
	//dump($item_data);
	$price=Yii::app()->functions->explodeData($item_data['price']);
	if (is_array($price) && count($price)>=1){
		$price_select=isset($price[0])?$price[0]:'';
		$size_select=isset($price[1])?$price[1]:'';
	}
	$row++;
}
$disabled_website_ordering=Yii::app()->functions->getOptionAdmin('disabled_website_ordering');
$hide_foodprice=Yii::app()->functions->getOptionAdmin('website_hide_foodprice');
echo CHtml::hiddenField('hide_foodprice',$hide_foodprice);
?>


<form class="frm-fooditem" id="frm-fooditem" method="POST" onsubmit="return false;">
<?php echo CHtml::hiddenField('action','addToCart')?>
<?php echo CHtml::hiddenField('item_id',$this_data['item_id'])?>
<?php echo CHtml::hiddenField('row',isset($row)?$row:"")?>
<?php echo CHtml::hiddenField('merchant_id',isset($data['merchant_id'])?$data['merchant_id']:'')?>
<?php echo CHtml::hiddenField('discount',isset($data['discount'])?$data['discount']:"" )?>
<?php echo CHtml::hiddenField('currentController','store')?>



<div class="container view-food-item-wrap" id="mobile-view-food-item">
   
  <!--ITEM NAME & DESCRIPTION-->
  <div class="row">
    <div class="col-md-3 ">              
       <img src="<?php echo FunctionsV3::getFoodDefaultImage($data['photo']);?>" alt="<?php echo $data['item_name']?>" title="<?php echo $data['item_name']?>" class="logo-small">
    </div> <!--col-->
    <div class="col-md-9 ">
       <p class="bold"><?php echo qTranslate($data['item_name'],'item_name',$data)?></p>
       <p><?php echo qTranslate($data['item_description'],'item_description',$data)?></p>
    </div> <!--col-->
  </div> <!--row-->
  <!--ITEM NAME & DESCRIPTION--
     
  <!--FOOD ITEM GALLERY-->
  <?php if (getOption($data['merchant_id'],'disabled_food_gallery')!=2):?>  
  <?php $gallery_photo=!empty($data['gallery_photo'])?json_decode($data['gallery_photo']):false; ?>
     <?php if (is_array($gallery_photo) && count($gallery_photo)>=1):?>
      <div class="section-label">
        <a class="section-label-a">
          <span class="bold">
          <?php echo t("Gallery")?></span>
          <b></b>
        </a>     
        <div class="food-gallery-wrap row ">
          <?php foreach ($gallery_photo as $gal_val):?>
          <div class="col-md-3 ">
            <a href="<?php echo websiteUrl()."/upload/$gal_val"?>">
              <div class="food-pic" style="background:url('<?php echo websiteUrl()."/upload/$gal_val"?>')"></div>
              <img style="display:none;" src="<?php echo websiteUrl()."/upload/$gal_val"?>" alt="" title="">
            </a>
          </div> <!--col-->         
          <?php endforeach;?>
        </div> <!--food-gallery-wrap-->   
      </div> <!--section-label-->
     <?php endif;?>
  <?php endif;?>
  <!--FOOD ITEM GALLERY-->
    
  
  <!--PRICE-->
  <div class="section-label">
    <a class="section-label-a">
      <span class="bold">
      <?php echo t("Price")?></span>
      <b></b>
    </a>     
    <div class="row">
    <?php if (is_array($data['prices']) && count($data['prices'])>=1):?>  
      <?php foreach ($data['prices'] as $price):?>
          <?php $price['price']=Yii::app()->functions->unPrettyPrice($price['price'])?>
          <div class="col-md-5 ">
             <?php if ( !empty($price['size'])):?>
                 <?php echo CHtml::radioButton('price',
		          $size_select==$price['size']?true:false
		          ,array(
		            'value'=>$price['price']."|".$price['size'],
		            'class'=>"price_cls item_price"
		          ))?>
		          <?php echo qTranslate($price['size'],'size',$price)?>
              <?php else :?>
                  <?php echo CHtml::radioButton('price',
		            count($price['price'])==1?true:false
		            ,array(
		            'value'=>$price['price'],
		            'class'=>"item_price"
		          ))?>
             <?php endif;?>
             
             <?php if (isset($price['price'])):?>  
                <?php if (is_numeric($data['discount'])):?>
                    <span class="line-tru"><?php echo FunctionsV3::prettyPrice($price['price'])?></span>
                    <span class="text-danger"><?php echo FunctionsV3::prettyPrice($price['price']-$data['discount'])?></span>
                <?php else :?>
                    <?php echo FunctionsV3::prettyPrice($price['price'])?>
                 <?php endif;?>
             <?php endif;?>
             
          </div> <!--col-->
      <?php endforeach;?>
    <?php endif;?>
    </div> <!--row-->
  </div>        
  <!--PRICE-->
  
  <!--QUANTITY-->
  <?php if (is_array($data['prices']) && count($data['prices'])>=1):?>
  <div class="section-label">
    <a class="section-label-a">
      <span class="bold">
      <?php echo t("Quantity")?></span>
      <b></b>
    </a>     
    <div class="row">
       <div class="col-md-1 col-xs-1 border into-row-2 text-to-right">
          <a href="javascript:;" class="green-button inline qty-minus" ><i class="ion-minus"></i></a>
       </div>
       <div class="col-md-2 col-xs-2 border into-row-2">
          <?php echo CHtml::textField('qty',
	      isset($item_data['qty'])?$item_data['qty']:1
	      ,array(
	      'class'=>"uk-form-width-mini numeric_only qty",      
	      ))?>
       </div>
       <div class="col-md-1 col-xs-1 border into-row-2 text-to-left">
         <a href="javascript:;" class="qty-plus green-button inline"><i class="ion-plus"></i></a>
       </div>
       <div class="col-md-8 col-xs-8 border into-row">
         <a href="javascript:;" class="special-instruction orange-button inline"><?php echo t("Special Instructions")?></a>
       </div>
    </div> <!--row-->
  </div> <!-- section-label--> 
  
  <div class="notes-wrap">
  <?php echo CHtml::textArea('notes',
  isset($item_data['notes'])?$item_data['notes']:""
  ,array(
   'class'=>'uk-width-1-1',
   'placeholder'=>Yii::t("default","Special Instructions")
  ))?>
  </div> <!--notes-wrap-->
  
  <?php else :?>
  <!--do nothing-->
  <?php endif;?>  
  <!--QUANTITY-->
    
  
  

  
  
  


<?php if ($disabled_website_ordering==""):?>
<div class="section-label top25">
<a class="section-label-a">
  <span class="bold">
  &nbsp;
  </span>
  <b></b>
</a>        
</div>  
<div class="row food-item-actions">
  <div class="col-md-6 col-xs-6 border">
       <a href="<?php echo Yii::app()->createUrl('/store/menu/merchant/'.$slug) ?>" 
     class="center upper-text green-button inline"><?php echo t("Back")?></a>
       
  </div>
  <div class="col-md-6 col-xs-6 border ">
 
  
  <input type="submit" value="<?php echo empty($row)?Yii::t("default","add to cart"):Yii::t("default","update cart");?>" 
     class="add_to_cart orange-button upper-text">
  
  </div>
</div>
<?php endif;?>
  
</div> <!--view-item-wrap-->

<?php else :?>
<p class="text-danger top25 center"><?php echo t("Sorry but we cannot find what you are looking for.")?></p>
<?php endif;?>

</div> <!--container-->