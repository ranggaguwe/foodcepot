<form class="uk-form uk-form-horizontal admin-settings-page forms" id="forms">
<?php echo CHtml::hiddenField('action','emailSettings')?>

<?php 
$email_provider=Yii::app()->functions->getOptionAdmin('email_provider');
?>
  
  <div class="uk-form-row">  
  <ul>
   <li><?php 
   echo CHtml::radioButton('email_provider',
   $email_provider=="phpmail"?true:false
   ,array(
    'class'=>"icheck",
    'value'=>"phpmail"
   ));
   echo "<span>".t("use php mail functions")."</span>";
   ?>
   </li>
   <li><?php 
   echo CHtml::radioButton('email_provider',
   $email_provider=="smtp"?true:false
   ,array(
    'class'=>"icheck",
    'value'=>'smtp'
   ));
   echo "<span>".t("use SMTP")."</span>";
   ?></li>
   
   <li><?php 
   echo CHtml::radioButton('email_provider',
   $email_provider=="mandrill"?true:false
   ,array(
    'class'=>"icheck",
    'value'=>'mandrill'
   ));
   echo "<span>".t("use Mandrill API")."</span>";
   ?></li>
   
  </ul>
</div>



<div class="uk-form-row">
  <label class="uk-form-label"><?php echo Yii::t("default","SMTP host")?></label>  
  <?php 
  echo CHtml::textField('smtp_host',
  Yii::app()->functions->getOptionAdmin('smtp_host'),
  array(
    'class'=>"uk-form-width-large"    
  ))
  ?> 
</div>

<div class="uk-form-row">
  <label class="uk-form-label"><?php echo Yii::t("default","SMTP port")?></label>  
  <?php 
  echo CHtml::textField('smtp_port',
  Yii::app()->functions->getOptionAdmin('smtp_port'),
  array(
    'class'=>"uk-form-width-large"    
  ))
  ?> 
</div>

<div class="uk-form-row">
  <label class="uk-form-label"><?php echo Yii::t("default","Username")?></label>  
  <?php 
  echo CHtml::textField('smtp_username',
  Yii::app()->functions->getOptionAdmin('smtp_username'),
  array(
    'class'=>"uk-form-width-large"    
  ))
  ?> 
</div>

<div class="uk-form-row">
  <label class="uk-form-label"><?php echo Yii::t("default","Password")?></label>  
  <?php 
  echo CHtml::textField('smtp_password',
  Yii::app()->functions->getOptionAdmin('smtp_password'),
  array(
    'class'=>"uk-form-width-large"    
  ))
  ?> 
</div>

<p class="uk-text-danger"><?php echo t("Note: When using SMTP make sure the port number is open in your server")?>.<br/>
<?php echo t("You can ask your hosting to open this for you")?>.
</p>



<h3><?php echo t("Mandrill API")?></h3>


<div class="uk-form-row">
  <label class="uk-form-label"><?php echo Yii::t("default","API KEY")?></label>  
  <?php 
  echo CHtml::textField('mandrill_api_key',
  Yii::app()->functions->getOptionAdmin('mandrill_api_key'),
  array(
    'class'=>"uk-form-width-large"    
  ))
  ?> 
</div>

<hr></hr>

<hr>

<h2><?php echo Yii::t("default","Email Template")?></h2>

<?php 
$email_tpl_activation=Yii::app()->functions->getOptionAdmin('email_tpl_activation');
if (empty($email_tpl_activation)){	
	$email_tpl_activation=EmailTPL::merchantActivationCodePlain();
}

$email_tpl_forgot=Yii::app()->functions->getOptionAdmin('email_tpl_forgot');
if (empty($email_tpl_forgot)){		
	$email_tpl_forgot=EmailTPL::merchantForgotPassPlain();
}
?>


<div class="uk-form-row">
  <h3><?php echo t("customer welcome email template")?></h3>
  
<div class="uk-form-row">
<?php echo CHtml::textField('email_tpl_customer_subject',
getOptionA('email_tpl_customer_subject'),array(
  'class'=>"uk-form-width-large",
  "placeholder"=>t("Email Subject")
))?>
</div>
  
  <?php  
  echo CHtml::textArea('email_tpl_customer_reg',
  getOptionA('email_tpl_customer_reg'),
  array(
    'class'=>"big-textarea"    
  ))
  ?> 
</div>

<p style="margin:0;"><?php echo t("Available Tags")?>:</p>
<ul>
 <li><?php echo t("{website_name}")?></li>
 <li><?php echo t("{client_name}")?></li>
 <li><?php echo t("{email_address}")?></li> 
</ul>

<hr/>

<div class="uk-form-row">
  <h3><?php echo t("merchant activation email template")?></h3>
  <?php 
  echo CHtml::textArea('email_tpl_activation',
  $email_tpl_activation,
  array(
    'class'=>"big-textarea"    
  ))
  ?> 
</div>

<p style="margin:0;"><?php echo t("Available Tags")?>:</p>
<ul>
 <li><?php echo t("{merchant_name}")?></li>
 <li><?php echo t("{activation_key}")?></li>
 <li><?php echo t("{website_title}")?></li>
 <li><?php echo t("{website_url}")?></li>
</ul>

<hr/>

<div class="uk-form-row">
  <h3><?php echo t("merchant forgot password email template")?></h3>
  <?php 
  echo CHtml::textArea('email_tpl_forgot',
  $email_tpl_forgot,
  array(
    'class'=>"big-textarea"    
  ))
  ?> 
</div>

<p style="margin:0;"><?php echo t("Available Tags")?>:</p>
<ul>
 <li><?php echo t("{merchant_name}")?></li>
 <li><?php echo t("{website_title}")?></li>
 <li><?php echo t("{verification_code}")?></li>
</ul>




<div class="uk-form-row">
<label class="uk-form-label"></label>
<input type="submit" value="<?php echo Yii::t("default","Save")?>" class="uk-button uk-form-width-medium uk-button-success">
<a href="javascript:;" class="test-email uk-button"><?php echo t("Click here to send Test Email")?></a>
</div>

</form>