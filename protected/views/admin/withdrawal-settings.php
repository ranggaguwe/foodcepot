<form class="uk-form uk-form-horizontal forms" id="forms">
<?php echo CHtml::hiddenField('action','withdrawalSettings')?>

<?php 
$payoutRequest=EmailTPL::payoutRequest();
$payoutProcess=EmailTPL::payoutProcess();

$wd_template_payout_subject=yii::app()->functions->getOptionAdmin('wd_template_payout_subject');
$wd_template_process_subject=yii::app()->functions->getOptionAdmin('wd_template_process_subject');
if (empty($wd_template_payout_subject)){
	$wd_template_payout_subject=t("Your Request for Withdrawal was Received");
}
if (empty($wd_template_process_subject)){
	$wd_template_process_subject=t("Your Request for Withdrawal has been Processed");
}
?>


<h3><?php echo t("Settings")?></h3>

<div class="uk-form-row">
<label class="uk-form-label"><?php echo t("Disable Withdrawal")?>:</label>  
<?php echo CHtml::checkBox('wd_payout_disabled',
yii::app()->functions->getOptionAdmin('wd_payout_disabled')==2?true:false,
array(
 'class'=>"icheck",
 'value'=>2
))?>
</div>

<div class="uk-form-row">
<label class="uk-form-label"><?php echo t("Enabled Notification")?>?</label>  
<?php echo CHtml::checkBox('wd_payout_notification',
yii::app()->functions->getOptionAdmin('wd_payout_notification')==2?true:false,
array(
 'class'=>"icheck",
 'value'=>2
))?>
</div>

<!--<div class="uk-form-row">
<label class="uk-form-label"><?php echo t("Minimum Payout Amount")?>:</label>  
<?php 
echo CHtml::textField('wd_minimum_amount',yii::app()->functions->getOptionAdmin('wd_minimum_amount'),array(
 'class'=>"numeric_only"
));
?>
<span style="padding-left:10px;">
<?php echo Yii::app()->functions->adminCurrencySymbol();?>
</span>
<p class="uk-text-muted"><?php echo t("Minimum Amount that merchant can only withdraw")?></p>
</div>-->


<div class="uk-form-row">
<label class="uk-form-label"><?php echo t("Days to process")?>:</label>  
<?php 
echo CHtml::textField('wd_days_process',yii::app()->functions->getOptionAdmin('wd_days_process'),array(
 'class'=>"numeric_only"
))
?>
<span style="padding-left:10px;"><?php echo t("days")?></span>
<p class="uk-text-muted"><?php echo t("How many days the payout will be process")?></p>
</div>

<hr></hr>

<h3><?php echo t("Payment method")?></h3>








<div class="uk-form-row">
<label class="uk-form-label"><?php echo t("Minimum Payout Amount")?>:</label>  
<?php 
echo CHtml::textField('wd_bank_minimum',yii::app()->functions->getOptionAdmin('wd_bank_minimum'),array(
 'class'=>"numeric_only"
));
?>
<span style="padding-left:10px;">
<?php echo Yii::app()->functions->adminCurrencySymbol();?>
</span>
</div>

<hr>
</hr>




<div class="uk-form-row">
<label class="uk-form-label"></label>
<input type="submit" value="<?php echo Yii::t("default","Save")?>" class="uk-button uk-form-width-medium uk-button-success">
</div>


</form>