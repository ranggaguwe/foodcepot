<style>
@media (min-width: 960px) {
.uk-form-horizontal .uk-form-label {
    width: 300px;
    margin-top: 5px;
    float: left;
}
}
</style>
<form class="uk-form uk-form-horizontal admin-settings-page forms" id="forms">
<?php echo CHtml::hiddenField('action','adminSettings')?>

<h2><?php echo Yii::t("default","Delivery Setting")?></h2>

<div class="uk-grid">
<div class="uk-width-1-1">
<h4><?php echo Yii::t("default","Shuttle Motor (For Economy)")?></h4>
</div>
<div class="uk-width-1-4">
  <label class="uk-form-label"><?php echo Yii::t("default","Flat First Range")?></label>  
  0-
  <?php 
  echo CHtml::textField('shuttle_motor_flat_range',
  Yii::app()->functions->getOptionAdmin('shuttle_motor_flat_range'),
  array(
    'class'=>"uk-form-width-small",
  ))
  ?> km 
</div>
<div class="uk-width-1-5">
  <label class="uk-form-label"><?php echo Yii::t("default","Flat Cost")?></label>  
  Rp 
  <?php 
  echo CHtml::textField('shuttle_motor_flat_cost',
  Yii::app()->functions->getOptionAdmin('shuttle_motor_flat_cost'),
  array(
    'class'=>"uk-form-width-small",
  ))
  ?> 
</div>
<div class="uk-width-1-4">
  <label class="uk-form-label"><?php echo Yii::t("default","Continue Cost")?></label>  
  Rp 
  <?php 
  echo CHtml::textField('shuttle_motor_continue',
  Yii::app()->functions->getOptionAdmin('shuttle_motor_continue'),
  array(
    'class'=>"uk-form-width-small",
  ))
  ?>/km 
</div>
</div>
<div class="uk-grid">
<div class="uk-width-1-1">
<h4><?php echo Yii::t("default","Motor (For Express)")?></h4>
</div>
<div class="uk-width-1-4">
  <label class="uk-form-label"><?php echo Yii::t("default","Flat First Range")?></label>  
  0-
  <?php 
  echo CHtml::textField('motor_flat_range',
  Yii::app()->functions->getOptionAdmin('motor_flat_range'),
  array(
    'class'=>"uk-form-width-small",
  ))
  ?> km 
</div>
<div class="uk-width-1-5">
  <label class="uk-form-label"><?php echo Yii::t("default","Flat Cost")?></label>  
  Rp 
  <?php 
  echo CHtml::textField('motor_flat_cost',
  Yii::app()->functions->getOptionAdmin('motor_flat_cost'),
  array(
    'class'=>"uk-form-width-small",
  ))
  ?> 
</div>
<div class="uk-width-1-4">
  <label class="uk-form-label"><?php echo Yii::t("default","Continue Cost")?></label>  
  Rp 
  <?php 
  echo CHtml::textField('motor_continue',
  Yii::app()->functions->getOptionAdmin('motor_continue'),
  array(
    'class'=>"uk-form-width-small",
  ))
  ?>/km 
</div>

</div><!-- uk-grid-->


<div class="uk-grid">
<div class="uk-width-1-1">
<h4><?php echo Yii::t("default","Shuttle Mobil (For Economy)")?></h4>
</div>
<div class="uk-width-1-4">
  <label class="uk-form-label"><?php echo Yii::t("default","Flat First Range")?></label>  
  0-
  <?php 
  echo CHtml::textField('shuttle_car_flat_range',
  Yii::app()->functions->getOptionAdmin('shuttle_car_flat_range'),
  array(
    'class'=>"uk-form-width-small",
  ))
  ?> km 
</div>
<div class="uk-width-1-5">
  <label class="uk-form-label"><?php echo Yii::t("default","Flat Cost")?></label>  
  Rp 
  <?php 
  echo CHtml::textField('shuttle_car_flat_cost',
  Yii::app()->functions->getOptionAdmin('shuttle_car_flat_cost'),
  array(
    'class'=>"uk-form-width-small",
  ))
  ?> 
</div>
<div class="uk-width-1-4">
  <label class="uk-form-label"><?php echo Yii::t("default","Continue Cost")?></label>  
  Rp 
  <?php 
  echo CHtml::textField('shuttle_car_continue',
  Yii::app()->functions->getOptionAdmin('shuttle_car_continue'),
  array(
    'class'=>"uk-form-width-small",
  ))
  ?>/km 
</div>
</div>
<div class="uk-grid">
<div class="uk-width-1-1">
<h4><?php echo Yii::t("default","Mobil (For Express)")?></h4>
</div>
<div class="uk-width-1-4">
  <label class="uk-form-label"><?php echo Yii::t("default","Flat First Range")?></label>  
  0-
  <?php 
  echo CHtml::textField('car_flat_range',
  Yii::app()->functions->getOptionAdmin('car_flat_range'),
  array(
    'class'=>"uk-form-width-small",
  ))
  ?> km 
</div>
<div class="uk-width-1-5">
  <label class="uk-form-label"><?php echo Yii::t("default","Flat Cost")?></label>  
  Rp 
  <?php 
  echo CHtml::textField('car_flat_cost',
  Yii::app()->functions->getOptionAdmin('car_flat_cost'),
  array(
    'class'=>"uk-form-width-small",
  ))
  ?> 
</div>
<div class="uk-width-1-4">
  <label class="uk-form-label"><?php echo Yii::t("default","Continue Cost")?></label>  
  Rp 
  <?php 
  echo CHtml::textField('car_continue',
  Yii::app()->functions->getOptionAdmin('car_continue'),
  array(
    'class'=>"uk-form-width-small",
  ))
  ?>/km 
</div>



</div><!-- uk-grid-->
<div class="uk-form-row" style="margin-top:20px;margin-bottom:20px">
  <label class="uk-form-label"><?php echo Yii::t("default","Maximum Weight for Motor")?></label>  
  <?php 
  echo CHtml::textField('motor_max_weight',
  Yii::app()->functions->getOptionAdmin('motor_max_weight'),
  array(
    'class'=>"uk-form-width-small",
    'placeholder'=>"in kg"
  ))
  ?> 
</div>

<div class="uk-form-row" style="margin-top:20px;margin-bottom:20px">
  <label class="uk-form-label"><?php echo Yii::t("default","Merchants Max Distance")?></label>  
  <?php 
  echo CHtml::textField('merchants_max_distance',
  Yii::app()->functions->getOptionAdmin('merchants_max_distance'),
  array(
    'class'=>"uk-form-width-small",
    'placeholder'=>"in km"
  ))
  ?> 
</div>

<hr/>
<h2><?php echo Yii::t("default","Website")?></h2>
<div class="uk-form-row">
  <label class="uk-form-label"><?php echo Yii::t("default","Title")?></label>  
  <?php 
  echo CHtml::textField('website_title',
  Yii::app()->functions->getOptionAdmin('website_title'),
  array(
    'class'=>"uk-form-width-large",
    'placeholder'=>"Title of the website"
  ))
  ?> 
</div>

<?php 
$country_list=require_once "CountryCode.php";
$country_list2=$country_list;
//array_unshift($country_list2,t("All"));

$merchant_specific_country=Yii::app()->functions->getOptionAdmin('merchant_specific_country');
if (!empty($merchant_specific_country)){
	$merchant_specific_country=json_decode($merchant_specific_country);
}
?>

<hr/>

<h2><?php echo Yii::t("default","Google API Key")?></h2>

<div class="uk-form-row">
  <label class="uk-form-label"><?php echo t("Geocoding API Key")?></label>
  <?php 
  echo CHtml::textField('google_geo_api_key',
  Yii::app()->functions->getOptionAdmin('google_geo_api_key'),array(
    'class'=>"uk-form-width-large"    
  ));
  ?>
</div>

<p class="uk-text-small uk-text-muted">
<span style="color:red;"><?php echo t("Note")?>:</span>
<?php echo t("these section is now mandatory in order for your search functions will work 100%")?><br/>
<?php echo t("enabled Google Maps Distance Matrix API, Google Maps Geocoding API and Google Maps JavaScript API in your google developer account")?></p>






<div class="uk-form-row">
  <label class="uk-form-label"><?php echo Yii::t("default","Use CURL")?>?</label>  
  <?php 
  echo CHtml::checkBox('google_use_curl',
   Yii::app()->functions->getOptionAdmin('google_use_curl')==2?true:false
   ,array(
   'class'=>"icheck",
   'value'=>2
  ))
  ?>    
</div>


<hr></hr>


<h2><?php echo Yii::t("default","Customer popup address options")?></h2>

<div class="uk-form-row">
  <label class="uk-form-label"><?php echo Yii::t("default","Disabled popup asking customer address")?>?</label>  
  <?php 
  echo CHtml::checkBox('customer_ask_address',
   Yii::app()->functions->getOptionAdmin('customer_ask_address')==2?true:false
   ,array(
   'class'=>"icheck",
   'value'=>2
  ))
  ?>  
</div>

<hr/>

<h2><?php echo Yii::t("default","Login & Signup")?></h2>


<div class="uk-form-row">
  <label class="uk-form-label"><?php echo Yii::t("default","Enabled Email Verification")?></label>  
  <?php 
  echo CHtml::checkBox('theme_enabled_email_verification',
   Yii::app()->functions->getOptionAdmin('theme_enabled_email_verification')==2?true:false
   ,array(
   'class'=>"icheck theme_enabled_email_verification",
   'value'=>2
  ))
  ?>  
</div>

<hr/>

<h2><?php echo Yii::t("default","Terms and Conditions")?></h2>

<div class="uk-form-row">
  <label class="uk-form-label"><?php echo Yii::t("default","Enabled On Merchant Signup")?>?</label>  
  <?php 
  echo CHtml::checkBox('website_terms_merchant',
   Yii::app()->functions->getOptionAdmin('website_terms_merchant')=="yes"?true:false
   ,array(
   'class'=>"icheck",
   'value'=>"yes"
  ))
  ?>  
</div>

<div class="uk-form-row url_1">
  <label class="uk-form-label"><?php echo Yii::t("default","URL Link")?></label>  
  <?php 
  echo CHtml::textField('website_terms_merchant_url',
  Yii::app()->functions->getOptionAdmin('website_terms_merchant_url'),array(
  'class'=>"uk-form-width-large"
  ));
  ?>  
</div>

<div class="uk-form-row">
  <label class="uk-form-label"><?php echo Yii::t("default","Enabled On Customer Signup")?>?</label>  
  <?php 
  echo CHtml::checkBox('website_terms_customer',
   Yii::app()->functions->getOptionAdmin('website_terms_customer')=="yes"?true:false
   ,array(
   'class'=>"icheck",
   'value'=>"yes"
  ))
  ?>  
</div>

<div class="uk-form-row url_2">
  <label class="uk-form-label"><?php echo Yii::t("default","URL Link")?></label>  
  <?php 
  echo CHtml::textField('website_terms_customer_url',
  Yii::app()->functions->getOptionAdmin('website_terms_customer_url'),array(
  'class'=>"uk-form-width-large"
  ));
  ?>    
</div>

<hr/>

<h2><?php echo Yii::t("default","Reviews")?></h2>

<div class="uk-form-row">
  <label class="uk-form-label"><?php echo Yii::t("default","Allow only those who has actual purchases")?></label>  
  <?php 
  echo CHtml::checkBox('website_reviews_actual_purchase',
   Yii::app()->functions->getOptionAdmin('website_reviews_actual_purchase')=="yes"?true:false
   ,array(
   'class'=>"icheck",
   'value'=>"yes"
  ))
  ?>  
</div>

<div class="uk-form-row">
  <label class="uk-form-label"><?php echo Yii::t("default","Disabled Merchant can edit review or delete")?></label>  
  <?php 
  echo CHtml::checkBox('merchant_can_edit_reviews',
   Yii::app()->functions->getOptionAdmin('merchant_can_edit_reviews')=="yes"?true:false
   ,array(
   'class'=>"icheck",
   'value'=>"yes"
  ))
  ?>  
</div>

<hr/>
<h2><?php echo Yii::t("default","Website Timezone")?></h2>

<div class="uk-form-row">
<label class="uk-form-label"><?php echo Yii::t("default","Time Zone")?></label>
<?php 
echo CHtml::dropDownList('website_timezone',
Yii::app()->functions->getOptionAdmin("website_timezone")
,Yii::app()->functions->timezoneList())
?>
</div>


<div class="uk-form-row">
<label class="uk-form-label"><?php echo Yii::t("default","Date Format")?></label>
<?php 
echo CHtml::textField('website_date_format',
Yii::app()->functions->getOptionAdmin("website_date_format")
,array(
  'class'=>'',
  'placeholder'=>"M d,Y",
  "maxlength"=>20
));
echo " ".t("Default")." M d,Y"
?>
</div>
<p class="uk-text-muted uk-text-small"><?php echo t("Note: must be a valid php date format")?>
<br/><a target="_blank" href="http://php.net/manual/en/function.date.php">http://php.net/manual/en/function.date.php</a>
</p>


<div class="uk-form-row">
<label class="uk-form-label"><?php echo Yii::t("default","Time Format")?></label>
<?php 
echo CHtml::textField('website_time_format',
Yii::app()->functions->getOptionAdmin("website_time_format")
,array(
  'class'=>'',
  'placeholder'=>"G:i:s",
  "maxlength"=>20
));
echo " ".t("Default")." G:i:s"
?>
</div>
<p class="uk-text-muted uk-text-small"><?php echo t("Note: must be a valid php time format")?>
<br/><a target="_blank" href="http://php.net/manual/en/function.date.php">http://php.net/manual/en/function.date.php</a>
</p>

<div class="uk-form-row">
<label class="uk-form-label"><?php echo Yii::t("default","Date Picker Format")?></label>
<?php 
echo CHtml::dropDownList('website_date_picker_format',
Yii::app()->functions->getOptionAdmin("website_date_picker_format")
,array(
'yy-mm-dd'=>"yy-mm-dd - default",
'mm-dd-yy'=>"mm-dd-yy",
'dd-mm-yy'=>"dd-mm-yy",
'yy-M-d'=>"yy-M-d",
'M dd,yy'=>"M d, Y",
));
?>
</div>


<div class="uk-form-row">
<label class="uk-form-label"><?php echo Yii::t("default","Time Picker Format")?></label>
<?php 
echo CHtml::dropDownList('website_time_picker_format',
Yii::app()->functions->getOptionAdmin("website_time_picker_format")
,array(
'24'=>t("24 hour format"),
 '12'=>t("12 hour format"),
));
?>
</div>


<hr/>

<h2><?php echo Yii::t("default","Merchant Registration")?></h2>

<div class="uk-form-row">
  <label class="uk-form-label"><?php echo Yii::t("default","Disabled Registration")?>?</label>  
  <?php 
  echo CHtml::checkBox('merchant_disabled_registration',
   Yii::app()->functions->getOptionAdmin('merchant_disabled_registration')=="yes"?true:false
   ,array(
   'class'=>"icheck",
   'value'=>"yes"
  ))
  ?>
  <p class="uk-text-muted"><?php echo Yii::t("default","Check this if you want to disabled merchant registration")?></p>
</div>




<div class="uk-form-row">
  <label class="uk-form-label"><?php echo Yii::t("default","Registration Status")?></label>  
  <?php 
  echo CHtml::dropDownList('merchant_sigup_status',
  Yii::app()->functions->getOptionAdmin('merchant_sigup_status')
  ,clientStatus(),array(
   'class'=>"uk-form-width-large"
  ));
  ?>
  <p class="uk-text-muted"><?php echo Yii::t("default","The status of the merchant after registration")?></p>
</div>





<hr/>
<h2><?php echo Yii::t("default","Address & Currency")?></h2>
<div class="uk-form-row">
  <label class="uk-form-label"><?php echo Yii::t("default","Country")?></label>  
  <?php 
  echo CHtml::dropDownList('admin_country_set',
  Yii::app()->functions->getOptionAdmin('admin_country_set')
  ,$country_list,array(
    'class'=>"uk-form-width-large",
    'data-validation'=>"required"
  ));
  ?> 
</div>

<div class="uk-form-row">
<label class="uk-form-label"><?php echo Yii::t('default',"Address")?></label>  
<?php 
  echo CHtml::textField('website_address',
  Yii::app()->functions->getOptionAdmin('website_address'),
  array(
    'class'=>"uk-form-width-large"    
  ))
  ?> 
</div>

<div class="uk-form-row">
<label class="uk-form-label"><?php echo Yii::t('default',"Contact Phone Number")?></label>  
<?php 
  echo CHtml::textField('website_contact_phone',
  Yii::app()->functions->getOptionAdmin('website_contact_phone'),
  array(
    'class'=>"uk-form-width-large"    
  ))
  ?> 
</div>

<div class="uk-form-row">
<label class="uk-form-label"><?php echo Yii::t('default',"Contact email")?></label>  
<?php 
  echo CHtml::textField('website_contact_email',
  Yii::app()->functions->getOptionAdmin('website_contact_email'),
  array(
    'class'=>"uk-form-width-large" ,
    //'data-validation'=>"email"
  ))
  ?> 
</div>

<div class="uk-form-row">
<label class="uk-form-label"><?php echo Yii::t('default',"Global Sender email")?></label>  
<?php 
  echo CHtml::textField('global_admin_sender_email',
  Yii::app()->functions->getOptionAdmin('global_admin_sender_email'),
  array(
    'class'=>"uk-form-width-large" ,
    //'data-validation'=>"email"
  ))
  ?> 
</div>
<p class="uk-text-muted">(<?php echo t("This email address will be use when sending email")?>)</p>

<div class="uk-form-row">
  <label class="uk-form-label"><?php echo Yii::t("default","Currency Code")?></label>  
  <?php 
  echo CHtml::dropDownList('admin_currency_set',
  Yii::app()->functions->getOptionAdmin('admin_currency_set')
  ,
  (array)Yii::app()->functions->currencyList()
  ,array(
    'class'=>"uk-form-width-large",
    'data-validation'=>"required"
  ));
  ?> 
</div>

<div class="uk-form-row">
  <label class="uk-form-label"><?php echo Yii::t("default","Currency code position")?></label>  
  <?php 
  echo CHtml::dropDownList('admin_currency_position',
  Yii::app()->functions->getOptionAdmin('admin_currency_position')
  ,
  (array)Yii::app()->functions->currencyPosition()
  ,array(
    'class'=>"uk-form-width-large",
    'data-validation'=>"required"
  ));
  ?> 
</div>

<?php $admin_decimal_place=Yii::app()->functions->getOptionAdmin('admin_decimal_place');?>
<?php $admin_use_separators=Yii::app()->functions->getOptionAdmin('admin_use_separators');?>
<div class="uk-form-row">
  <label class="uk-form-label"><?php echo Yii::t("default","Decimal Places")?></label>
  <?php echo CHtml::dropDownList('admin_decimal_place',empty($admin_decimal_place)?0:$admin_decimal_place,Yii::app()->functions->decimalPlacesList()
  ,array(
  'class'=>'uk-form-width-large',
  'data-validation'=>"required"
  ))?>
</div>

<div class="uk-form-row">
  <label class="uk-form-label"><?php echo Yii::t("default","Use 1000 Separators(,)")?>?</label>
  <?php 
  echo CHtml::checkBox('admin_use_separators',
  $admin_use_separators=="yes"?true:false
  ,array(
    'value'=>"yes",
    'class'=>"icheck"
  ))
  ?>
</div>

<div class="uk-form-row">
  <label class="uk-form-label"><?php echo Yii::t("default","Thousand Separators")?></label>
  <?php 
  echo CHtml::textField('admin_thousand_separator',
  Yii::app()->functions->getOptionAdmin('admin_thousand_separator'),array(
   'class'=>"uk-form-width-small",
   'maxlength'=>1
  ));
  ?>
</div>
<p class="uk-text-muted">(<?php echo t("leave empty to use standard comma separators")?>)</p>

<div class="uk-form-row">
  <label class="uk-form-label"><?php echo Yii::t("default","Decimal Separators")?></label>
  <?php 
  echo CHtml::textField('admin_decimal_separator',
  Yii::app()->functions->getOptionAdmin('admin_decimal_separator'),array(
    'class'=>"uk-form-width-small",
    'maxlength'=>1
  ));
  ?>
</div>
<p class="uk-text-muted">(<?php echo t("leave empty to use standard decimal separators")?>)</p>

<hr>

<div class="uk-form-row">
  <label class="uk-form-label"><?php echo Yii::t("default","Home Title Text")?></label>  
  <?php 
  echo CHtml::textField('home_search_text',
  Yii::app()->functions->getOptionAdmin('home_search_text'),
  array(
    'class'=>"uk-form-width-large",
    'placeholder'=>""
  ))
  ?> 
</div>

<div class="uk-form-row">
  <label class="uk-form-label"><?php echo Yii::t("default","Home SubTitle Text")?></label>  
  <?php 
  echo CHtml::textField('home_search_subtext',
  Yii::app()->functions->getOptionAdmin('home_search_subtext'),
  array(
    'class'=>"uk-form-width-large",
    'placeholder'=>""
  ))
  ?> 
</div>



<div class="uk-form-row">
<label class="uk-form-label"></label>
<input type="submit" value="<?php echo Yii::t("default","Save")?>" class="uk-button uk-form-width-medium uk-button-success">
</div>

</form>