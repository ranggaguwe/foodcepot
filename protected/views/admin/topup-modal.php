<div class="modal fade topup-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
    
      <div class="modal-header">
         <button aria-label="Close" data-dismiss="modal" class="close" type="button">
           <span aria-hidden="true"><i class="ion-android-close"></i></span>
         </button> 
        <h4 id="mySmallModalLabel" class="modal-title">
        <?php echo t("Edit Topup Status")?>
        </h4> 
      </div>  
      
		<div class="modal-body">

      <form id="frm" class="frm uk-form uk-form-horizontal forms" method="POST" onsubmit="return false;">
      <?php echo CHtml::hiddenField('action','addAgent')?>
      <?php echo CHtml::hiddenField('id','')?>
      <div class="inner">
		<div class="uk-grid">
		<div class="uk-width-1-3">
			<div class="uk-form-row">
			<label class="uk-form-label"><?=t('Customer')?> </label>
			<p class="uk-form-label frm-name"></p>
			</div>
			
			<div class="uk-form-row">
			<label class="uk-form-label"><?=t('Amount')?> </label>
			<p class="uk-form-label frm-amount"></p>
			</div>
			
			<div class="uk-form-row">
			  <label class="uk-form-label"><?php echo Yii::t("default","Admin Message")?></label>
			  <?php 
			  echo CHtml::textField('city',
			  isset($data['city'])?$data['city']:""
			  ,array('class'=>"uk-form-width-large",'data-validation'=>"required"))
			  ?>
			</div>

			<div class="uk-form-row">
			  <label class="uk-form-label"><?php echo Yii::t("default","Status")?></label>
			  <?php 
			  echo CHtml::dropDownList('status','',
				  array(
					'approved'=>t('Approved'),
					'pending'=>t('Pending'),
					'declined'=>t('Declined')	
					),
				  array('class'=>'uk-form-width-large frm-status')
			  )
			  ?>
			</div>
			 
			<div class="uk-form-row">
				<label class="uk-form-label"></label>
				<button type="submit" class="uk-button  uk-button-success"><?php echo t("Submit")?></button>
				<button type="button" data-id=".new-agent" 
					class="close-modal uk-button  uk-button-default"><?php echo t("Cancel")?></button>
			</div>
        
			
		</div> 
		<div class="uk-width-2-3">
			<div class="uk-form-row">
			<label class="uk-form-label"><?=t('Screenshot Proof of Payment')?> </label>
			<p class="uk-form-label frm-image">
			</p>
			</div>
			</div> 
		</div>

      </div> <!--inner-->  
      </form>  
      
      </div> <!--body-->
    
    </div>
  </div>
</div>
