<?php
$merchant_id=Yii::app()->functions->getMerchantID();
$merchant_currency=Yii::app()->functions->getOption("merchant_currency",$merchant_id);
$merchant_decimal=Yii::app()->functions->getOption("merchant_decimal",$merchant_id);
$merchant_use_separators=Yii::app()->functions->getOption("merchant_use_separators",$merchant_id);
$merchant_minimum_order=Yii::app()->functions->getOption("merchant_minimum_order",$merchant_id);
$merchant_tax=Yii::app()->functions->getOption("merchant_tax",$merchant_id);
$merchant_delivery_charges=Yii::app()->functions->getOption("merchant_delivery_charges",$merchant_id);
$stores_open_day=Yii::app()->functions->getOption("stores_open_day",$merchant_id);
$stores_open_starts=Yii::app()->functions->getOption("stores_open_starts",$merchant_id);
$stores_open_ends=Yii::app()->functions->getOption("stores_open_ends",$merchant_id);
$stores_open_custom_text=Yii::app()->functions->getOption("stores_open_custom_text",$merchant_id);

$stores_open_day=!empty($stores_open_day)?(array)json_decode($stores_open_day):false;
$stores_open_starts=!empty($stores_open_starts)?(array)json_decode($stores_open_starts):false;
$stores_open_ends=!empty($stores_open_ends)?(array)json_decode($stores_open_ends):false;
$stores_open_custom_text=!empty($stores_open_custom_text)?(array)json_decode($stores_open_custom_text):false;

$merchant_photo=Yii::app()->functions->getOption("merchant_photo",$merchant_id);
$merchant_delivery_estimation=Yii::app()->functions->getOption("merchant_delivery_estimation",$merchant_id);
$merchant_delivery_charges_type=Yii::app()->functions->getOption("merchant_delivery_charges_type",$merchant_id);

$merchant_photo_bg=Yii::app()->functions->getOption("merchant_photo_bg",$merchant_id);

$merchant_extenal=Yii::app()->functions->getOption("merchant_extenal",$merchant_id);
$merchant_maximum_order=Yii::app()->functions->getOption("merchant_maximum_order",$merchant_id);

$merchant_switch_master_cod=Yii::app()->functions->getOption("merchant_switch_master_cod",$merchant_id);
$merchant_switch_master_ccr=Yii::app()->functions->getOption("merchant_switch_master_ccr",$merchant_id);

$merchant_minimum_order_pickup=Yii::app()->functions->getOption("merchant_minimum_order_pickup",$merchant_id);
$merchant_maximum_order_pickup=Yii::app()->functions->getOption("merchant_maximum_order_pickup",$merchant_id);

$stores_open_pm_start=Yii::app()->functions->getOption("stores_open_pm_start",$merchant_id);
$stores_open_pm_start=!empty($stores_open_pm_start)?(array)json_decode($stores_open_pm_start):false;

$stores_open_pm_ends=Yii::app()->functions->getOption("stores_open_pm_ends",$merchant_id);
$stores_open_pm_ends=!empty($stores_open_pm_ends)?(array)json_decode($stores_open_pm_ends):false;

$FunctionsK=new FunctionsK();
$tips_list=$FunctionsK->tipsList(true);
?>

<div id="error-message-wrapper"></div>

<form class="uk-form uk-form-horizontal forms" id="forms">
<?php echo CHtml::hiddenField('action','merchantSettings')?>

<div class="uk-form-row"> 
 <label class="uk-form-label"><?php echo Yii::t('default',"Merchant Logo")?></label>
  <div style="display:inline-table;margin-left:1px;" class="button uk-button" id="photo"><?php echo Yii::t('default',"Browse")?></div>	  
  <DIV  style="display:none;" class="photo_chart_status" >
	<div id="percent_bar" class="photo_percent_bar"></div>
	<div id="progress_bar" class="photo_progress_bar">
	  <div id="status_bar" class="photo_status_bar"></div>
	</div>
  </DIV>		  
</div>

<?php if (!empty($merchant_photo)):?>
<div class="uk-form-row"> 
<?php else :?>
<div class="input_block preview">
<?php endif;?>
<label><?php echo Yii::t('default',"Preview")?></label>
<div class="image_preview">
 <?php if (!empty($merchant_photo)):?>
 <input type="hidden" name="photo" value="<?php echo $merchant_photo;?>">
 <img class="uk-thumbnail uk-thumbnail-small" src="<?php echo Yii::app()->request->baseUrl."/upload/".$merchant_photo;?>?>" alt="" title="">

 <div>
   <a href="javascript:;" class="remove-merchant-logo"><?php echo Yii::t("default","Remove Logo")?></a>
 </div> 
 <?php endif;?>
</div>
</div>


<div class="uk-form-row"> 
 <label class="uk-form-label"><?php echo Yii::t('default',"Merchant Header/Background")?></label>
  <div style="display:inline-table;margin-left:1px;" class="button uk-button" id="photo2"><?php echo Yii::t('default',"Browse")?></div>	  
  <DIV  style="display:none;" class="photo2_chart_status" >
	<div id="percent_bar" class="photo2_percent_bar"></div>
	<div id="progress_bar" class="photo2_progress_bar">
	  <div id="status_bar" class="photo2_status_bar"></div>
	</div>
  </DIV>		  
</div>
<p class="uk-text-muted uk-text-small"><?php echo Yii::t("default","Filename of image must not have spaces")?></p>

<?php if (!empty($merchant_photo_bg)):?>
<div class="uk-form-row"> 
<?php else :?>
<div class="input_block preview">
<?php endif;?>
<label><?php echo Yii::t('default',"Preview")?></label>
<div class="image_preview2">
 <?php if (!empty($merchant_photo_bg)):?>
 <input type="hidden" name="photo2" value="<?php echo $merchant_photo_bg;?>">
 <img class="uk-thumbnail uk-thumbnail-small" src="<?php echo Yii::app()->request->baseUrl."/upload/".$merchant_photo_bg;?>?>" alt="" title="">

 <div>
   <a href="javascript:;" class="remove-merchant-bg"><?php echo Yii::t("default","Remove")?></a>
 </div>
 <?php endif;?>
</div>
</div>

<hr/>



<h3><?php echo Yii::t("default","Tax & Delivery Charges")?></h3>

<div class="uk-form-row">
  <label class="uk-form-label"><?php echo Yii::t("default","Tax")?></label>
  <?php 
  echo CHtml::textField('merchant_tax',$merchant_tax,array(
    'class'=>"numeric_only"
  ))
  ?>%  
</div>





<?php $days=Yii::app()->functions->getDays();?>
<h3><?php echo Yii::t("default","Store Hours")?></h3>

<div class="uk-form-row">
<label class="uk-form-label"><?php echo Yii::t("default","Time Zone")?></label>
<?php 
echo CHtml::dropDownList('merchant_timezone',
Yii::app()->functions->getOption("merchant_timezone",$merchant_id)
,Yii::app()->functions->timezoneList())
?>
</div>


<div class="uk-form-row">
  <label class="uk-form-label">
  <?php echo Yii::t("default","Store days(s) Open:")?>  
  </label>  
  <div class="clear"></div>
  <p class="uk-text-muted"><?php echo Yii::t("default","If days has not been selected then merchant will be set to open")?></p>
  <ul class="uk-list uk-list-striped">
  <?php foreach ($days as $key=>$val):?>
  <li>
  <div class="uk-grid" >
  
    <div class="uk-width-1-6" style="width:5%;">
    <?php echo CHtml::checkBox('stores_open_day[]',
    in_array($key,(array)$stores_open_day)?true:false
    ,array('value'=>$key,'class'=>"icheck"))?>
    </div>
    
    <div class="uk-width-1-6" style="width:12%"><?php echo ucwords(Yii::app()->functions->translateDate($val));?></div>    
    <div class="uk-width-1-6" style="width:12%">
      <?php echo CHtml::textField("stores_open_starts[$key]",
      array_key_exists($key,(array)$stores_open_starts)?timeFormat($stores_open_starts[$key],true):""
      ,array('placeholder'=>Yii::t("default","Start"),'class'=>"timepick"));?>
    </div>
    
    <div class="uk-width-1-6" style="width:5%;"><?php echo Yii::t("default","To")?></div>
    <div class="uk-width-1-6" style="width:12%">
      <?php echo CHtml::textField("stores_open_ends[$key]",
      array_key_exists($key,(array)$stores_open_ends)?timeFormat($stores_open_ends[$key],true):""
      ,array('placeholder'=>Yii::t("default","End"),'class'=>"timepick"));?>
    </div>
    
    <div class="uk-width-1-6" style="width:5%;">
    /
    </div>
        
    <div class="uk-width-1-6" style="width:12%">
      <?php echo CHtml::textField("stores_open_pm_start[$key]",
      array_key_exists($key,(array)$stores_open_pm_start)?timeFormat($stores_open_pm_start[$key],true):""
      ,array('placeholder'=>Yii::t("default","Start"),'class'=>"timepick"));?>
    </div>
    <div class="uk-width-1-6" style="width:5%;"><?php echo Yii::t("default","To")?></div>
    <div class="uk-width-1-6" style="width:12%">
      <?php echo CHtml::textField("stores_open_pm_ends[$key]",
      array_key_exists($key,(array)$stores_open_pm_ends)?timeFormat($stores_open_pm_ends[$key],true):""
      ,array('placeholder'=>Yii::t("default","End"),'class'=>"timepick"));?>
    </div>
    
    <div class="uk-width-1-6" style="width:12%">
      <?php echo CHtml::textField("stores_open_custom_text[$key]",
      array_key_exists($key,(array)$stores_open_custom_text)?$stores_open_custom_text[$key]:""
      ,array('placeholder'=>Yii::t("default","Custom text")));?>
     </div>
   </div>    
  </li>
  <?php endforeach;?>
  </ul>
</div>


<div class="uk-form-row">
<label class="uk-form-label"></label>
<input type="submit" value="<?php echo Yii::t("default","Save")?>" class="uk-button uk-form-width-medium uk-button-success">
</div>

</form>