<?php 
$mtid=Yii::app()->functions->getMerchantID();
$r = query("SELECT IFNULL(SUM(earning), 0) total_earning 
			FROM {{order_merchant}} a 
			JOIN  {{order}} b 
			ON a.order_id=b.order_id 
			WHERE a.merchant_id=? AND b.status='successful'
			GROUP BY a.merchant_id
			",array($mtid));
$total_earning = $r[0]['total_earning'];

$query_date = date("Y-m-d");
$start_date=date('Y-m-01', strtotime($query_date));
$end_date=date('Y-m-t', strtotime($query_date));
$and =" AND date_created BETWEEN  '".$start_date." 00:00:00' AND 
			'".$end_date." 23:59:00'
		";
$r2 = query("SELECT IFNULL(SUM(earning), 0) total_earning 
			FROM {{order_merchant}} a 
			JOIN  {{order}} b 
			ON a.order_id=b.order_id 
			WHERE a.merchant_id=? AND b.status='successful'
			$and
			GROUP BY a.merchant_id
			",array($mtid));
$earning_this_month = $r[0]['total_earning'];
?>

<div class="earnings-wrap">

<div class="table">
  <ul>
  
  <li>
   <div class="rounded-box rounded">
     <p><?php echo t("Sales Earning This Month")?>:</p>
     <h3><?php echo displayPrice(adminCurrencySymbol(),normalPrettyPrice($earning_this_month));?></h3>
   </div>
  </li>
  
  <li>
   <div class="rounded-box rounded">
     <p><?php echo t("Your balance")?>:</p>
     <h3 class=""><?=displayPrice(adminCurrencySymbol(),normalPrettyPrice(Yii::app()->functions->getMerchantBalance($mtid)))?></h3>     
     <a href="<?php echo websiteUrl()."/merchant/withdrawals"?>"><?php echo t("Withdraw money")?></a>
   </div>
  </li>
  
  
  <li>
   <div class="rounded-box rounded">
     <p><?php echo t("Total Sales")?>:</p>
     <h3><?php echo displayPrice(adminCurrencySymbol(),normalPrettyPrice($total_earning));?></h3>
     <!--<P class="small"><?php echo t("based on list price of each item")?></P>-->
   </div>
  </li>
  
  </ul>
  <div class="clear"></div>
</div> <!--table-->

</div> <!--earnings-wrap-->