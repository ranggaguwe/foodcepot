<div class="uk-width-1">
<a href="<?php echo Yii::app()->request->baseUrl; ?>/merchant/FoodItem/Do/Add" class="uk-button"><i class="fa fa-plus"></i> <?php echo Yii::t("default","Add New")?></a>
<a href="<?php echo Yii::app()->request->baseUrl; ?>/merchant/FoodItem" class="uk-button"><i class="fa fa-list"></i> <?php echo Yii::t("default","List")?></a>
<a href="<?php echo Yii::app()->request->baseUrl; ?>/merchant/FoodItem/Do/Sort" class="uk-button"><i class="fa fa-sort-alpha-asc"></i> <?php echo Yii::t("default","Sort")?></a>
</div>


<div class="spacer"></div>

<div id="error-message-wrapper"></div>

<form class="uk-form uk-form-horizontal forms" id="forms">
<?php echo CHtml::hiddenField('action','FoodItemAdd')?>
<?php echo CHtml::hiddenField('id',isset($_GET['id'])?$_GET['id']:"");?>
<?php if (!isset($_GET['id'])):?>
<?php echo CHtml::hiddenField("redirect",Yii::app()->request->baseUrl."/merchant/FoodItem/Do/Add")?>
<?php endif;?>

<?php 
$price='';
$category='';
$multi_option_Selected='';
$multi_option_value_selected='';

if (isset($_GET['id'])){
	if (!$data=Yii::app()->functions->getFoodItem2($_GET['id'])){
		echo "<div class=\"uk-alert uk-alert-danger\">".
		Yii::t("default","Sorry but we cannot find what your are looking for.")."</div>";
		return ;
	}		
	$category=isset($data['category'])?(array)json_decode($data['category']):false;
	$price=isset($data['price'])?(array)json_decode($data['price']):false;	
	$multi_option_Selected=isset($data['multi_option'])?(array)json_decode($data['multi_option']):false;
	$multi_option_value_selected=isset($data['multi_option_value'])?(array)json_decode($data['multi_option_value']):false;	
	
	$wholesale_price=Yii::app()->functions->getWholeSalePrice($data['item_id']);
	
	
}
?>                                 


<div class="uk-grid">
    <div class="uk-width-1-2">
    
    <?php if ( Yii::app()->functions->multipleField()==2):?>
    
    <?php 
	Widgets::multipleFields(array(
	  'Food Item Name','Description'
	),array(
	  'item_name','item_description'
	),$data,array(true,false),array('text','textarea'));
	?>
	<div class="spacer"></div>

    <?php else :?>
	<div class="uk-form-row">
	<label class="uk-form-label"><?php echo Yii::t("default","Food Item Name")?></label>
	<?php echo CHtml::textField('item_name',
	isset($data['item_name'])?$data['item_name']:""
	,array(
	'class'=>'uk-form-width-large',
	'data-validation'=>"required"
	))?>
	</div>
	
	
	

	<div class="uk-form-row">
	<label class="uk-form-label"><?php echo Yii::t("default","Description")?></label>
	<?php echo CHtml::textArea('item_description',
	isset($data['item_description'])?$data['item_description']:""
	,array(
	'class'=>'uk-form-width-large big-textarea'	
	))?>
	</div>
	
	<?php endif;?>
	
	<div class="uk-form-row">
	<label class="uk-form-label"><?php echo Yii::t("default","Unit of Measurement (UOM) (ex: kg,pieces,gram)")?></label>
	<?php echo CHtml::textField('uom',
	isset($data['uom'])?$data['uom']:""
	,array(
	'class'=>'uk-form-width-large',
	'data-validation'=>"required"
	))?>
	</div>
	
	<div class="uk-form-row">
	<label class="uk-form-label"><?php echo Yii::t("default","Weight Per 1 Unit (in gram)")?></label>
	<?php echo CHtml::textField('weight',
	isset($data['weight'])?$data['weight']:""
	,array(
	'class'=>'uk-form-width-large',
	'data-validation'=>"required"
	))?>
	</div>
	
	<div class="uk-form-row">
	<label class="uk-form-label"><?php echo Yii::t("default","Stock")?></label>
	<?php echo CHtml::textField('stock',
	isset($data['stock'])?$data['stock']:""
	,array(
	'class'=>'uk-form-width-large',
	'data-validation'=>"required"
	))?>
	</div>

	<div class="uk-form-row" style="display:none">
	<label class="uk-form-label"><?php echo Yii::t("default","Minimum Order")?></label>
	<?php echo CHtml::textField('min_order',
	isset($data['min_order'])?$data['min_order']:""
	,array(
	'class'=>'uk-form-width-large',
	))?>
	</div>
	
	<div class="uk-form-row" >
	<label class="uk-form-label"><?php echo Yii::t("default","Tax in percent (Example value: 2.2)")?></label>
	<?php echo CHtml::textField('tax',
	isset($data['tax'])?$data['tax']:""
	,array(
	'class'=>'uk-form-width-large',
	))?>
	</div>
	
	<div class="uk-form-row">
	<label class="uk-form-label"><?php echo Yii::t("default","Condition")?></label>
	<?php echo CHtml::dropDownList('condition',
	isset($data['condition'])?$data['condition']:"",
	(array)conditionList(),
	array(
	'class'=>'uk-form-width-large',
	'data-validation'=>"required"
	))?>
	</div>

	<?php 
	Yii::app()->functions->data='list';
	?>
	
    </div> <!--END uk-width-1-2-->
    
    <div class="uk-width-1-2">
	<div class="uk-form-row">
	<label class="uk-form-label"><?php echo Yii::t("default","Status")?></label>
	<?php echo CHtml::dropDownList('status',
	isset($data['status'])?$data['status']:"",
	(array)statusList(),          
	array(
	'class'=>'uk-form-width-medium',
	'data-validation'=>"required"
	))?>
	</div>
	
	
	
<div class="uk-form-row"> 
 <label class="uk-form-label"><?php echo Yii::t('default',"Featured Image")?></label>
  <div style="display:inline-table;margin-left:1px;" class="button uk-button" id="photo"><?php echo Yii::t('default',"Browse")?></div>	  
  <DIV  style="display:none;" class="photo_chart_status" >
	<div id="percent_bar" class="photo_percent_bar"></div>
	<div id="progress_bar" class="photo_progress_bar">
	  <div id="status_bar" class="photo_status_bar"></div>
	</div>
  </DIV>		  
</div>

<?php if (!empty($data['photo'])):?>
<div class="uk-form-row"> 
<?php else :?>
<div class="input_block preview">
<?php endif;?>
<label><?php echo Yii::t('default',"Preview")?></label>
<div class="image_preview">
 <?php if (!empty($data['photo'])):?>
 <input type="hidden" name="photo" value="<?php echo $data['photo'];?>">
 <img class="uk-thumbnail uk-thumbnail-small" src="<?php echo Yii::app()->request->baseUrl."/upload/".$data['photo'];?>?>" alt="" title="">
 <?php endif;?>
</div>
</div>


<!--GALLERY -->
<div class="uk-form-row"> 
 <label class="uk-form-label"><?php echo Yii::t('default',"Gallery Image")?></label>
  <div style="display:inline-table;margin-left:1px;" class="button uk-button" id="foodgallery"><?php echo Yii::t('default',"Browse")?></div>	  
  <DIV  style="display:none;" class="foodgallery_chart_status" >
	<div id="percent_bar" class="foodgallery_percent_bar"></div>
	<div id="progress_bar" class="foodgallery_progress_bar">
	  <div id="status_bar" class="foodgallery_status_bar"></div>
	</div>
  </DIV>		  
</div>

<div class="uk-form-row"> 
  <div class="input_block foodgallery_preview">
  <?php if (!empty($data['gallery_photo'])): $gallery_photo=json_decode($data['gallery_photo']);?> 
  <?php if (is_array($gallery_photo) && count($gallery_photo)>=1):?>  
  <?php foreach ($gallery_photo as $val_gal):  $class_gal = time().Yii::app()->functions->generateRandomKey(10);?>
    <li class="<?php echo $class_gal?>"> 
      <img class="uk-thumbnail uk-thumbnail-mini" src="<?php echo websiteUrl()."/upload/$val_gal"?>">
      <?php echo CHtml::hiddenField('gallery_photo[]',$val_gal)?>
      <p><a href="javascript:rm_foodGallery('<?php echo $class_gal?>')"><?php echo t("Remove image")?></a></p>
    </li>
  <?php endforeach;?>
  <?php endif;?>
  <?php endif;?>
  </div>
</div>
<!--GALLERY -->
	
	<?php 	
	Yii::app()->functions->data='list';
	$category_list=Yii::app()->functions->getAllCategory();		
	?>

	<div class="uk-form-row">
	<label class="uk-form-label"><?php echo Yii::t("default","Category")?></label>
	<?php echo CHtml::dropDownList('category',
	isset($data['category'])?$data['category']:"",
	$category_list,
	array(
	'class'=>'uk-form-width-medium',
	'data-validation'=>"required"
	))?>
	</div>

	<div class="uk-form-row">
	<label class="uk-form-label uk-h4"><?php echo Yii::t("default","Retail Price")?></label>
	<?php echo CHtml::textField('retail_price',
	isset($data['retail_price'])?$data['retail_price']:""
	,array(
	'class'=>'uk-form-width-large',
	'data-validation'=>"required"
	))?>
	</div>


	<div class="uk-form-row">
	  <label class="uk-form-label uk-h4"><?php echo Yii::t("default","Wholesale Price")?></label>  
	  <div class="clear"></div>

	<ul class="uk-list uk-list-striped wholesale_wrap_parent">
	<li>
	  <div class="uk-grid">
	    <div class="uk-width-1-4"><?php echo Yii::t("default","Start Qty")?></div>
		<div class="uk-width-1-4"><?php echo Yii::t("default","End Qty")?></div>
	    <div class="uk-width-1-4"><?php echo Yii::t("default","Price")?></div>
	    <div class="uk-width-1-4"></div>
	  </div>
	</li>	
	
	<?php 
	//wholesale_price
	if ( is_array($wholesale_price) && count($wholesale_price)>=1):?>
	    <?php $x=1;?>
		<?php foreach ($wholesale_price as $w):?>			
		<li class="<?php echo $x==count($wholesale_price)?"":"";?>">
		  <div class="uk-grid">
		    <div class="uk-width-1-4">
		      <?php echo CHtml::textField('start[]',$w['start'],
		      array('class'=>'uk-form-width-medium numeric_only'))?>
		    </div>
		    <div class="uk-width-1-4">
		      <?php echo CHtml::textField('end[]',$w['end'],
		      array('class'=>'uk-form-width-medium numeric_only'))?>
		    </div>
		    <div class="uk-width-1-4">
		      <?php echo CHtml::textField('wprice[]',$w['price'],
		      array('class'=>'uk-form-width-medium numeric_only'))?>
		    </div>
		    <div class="uk-width-1-4">
				<a href="javascript:;" class="removeprice"><i class="fa fa-minus-square"></i></a>
		    </div>
		  </div>
	    </li>
		<?php $x++;?>
		<?php endforeach;?>
	<?php endif;?>
	
    <?php //endif;?>
    
    <li>
      <a href="javascript:;" class="addnewprice2"><i class="fa fa-plus-circle"></i></a>
    </li>
   
	</ul>
	</div>



	  
	
	<div class="uk-form-row" style="display:none">
	  <label class="uk-form-label uk-h3"><?php echo t("Tax")?></label>  
	<div class="clear"></div>
		
	  <ul class="uk-list uk-list-striped">
	  	  
	  <li>
	  <?php echo CHtml::checkBox('non_taxable',
	  $data['non_taxable']==2?true:false
	  ,array(
	   'class'=>"icheck",
	   'value'=>2
	  ))?>	  	  	  
	  <?php echo t("Non taxable")?>
	  </li>
	  </ul>	
	</div>	
	
	
	
    </div><!-- END uk-width-1-2-->
</div> <!--END uk-grid-->

<div class="spacer"></div>


<div class="uk-form-row">
<label class="uk-form-label"></label>
<input type="submit" value="<?php echo Yii::t("default","Save")?>" class="uk-button uk-form-width-medium uk-button-success">
</div>

</form>
<div style="display:none">
	<li class="wholesale_wrap">
	  <div class="uk-grid">
		<div class="uk-width-1-4">
		      <?php echo CHtml::textField('start[]','',
		      array('class'=>'uk-form-width-medium numeric_only'))?>
		</div>
		<div class="uk-width-1-4">
		  <?php echo CHtml::textField('end[]','',
		  array('class'=>'uk-form-width-medium numeric_only'))?>
		</div>
	     <div class="uk-width-1-4">
	      <?php echo CHtml::textField('wprice[]','',
	      array('class'=>'uk-form-width-medium numeric_only'))?>
	    </div>
	    <div class="uk-width-1-4">
	    <a href="javascript:;" class="removeprice2"><i class="fa fa-minus-square"></i></a>
	    </div>
	  </div>
    </li>
</div>