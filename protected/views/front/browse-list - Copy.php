<?php 
if (isset($_GET['location'])) {
	$location = $_GET['location'];
} else {
	$location = "";
}
if (isset($_GET['category'])) {
	$category = $_GET['category'];
} else {
	$category = "";
}
if (isset($_GET['satuan'])) {
	$satuan = $_GET['satuan'];
} else {
	$satuan = "";
}
if (isset($_GET['o'])) {
	$output = $_GET['o'];
} else {
	$output = "";
}
?>

		<div class="template-merchant hidden"> 
			<div class="col-sm-6 col-md-6 col-lg-4">
				<a href="<?= Yii::app()->createUrl('store/menu/merchant')?>/vurlmerchant">
				<div class="thumbnail no-padding thumb-merchant" style="height:290px;border: 1px solid #dcd9d9;">
					<div src="" class="img-responsive logo-merchant" style="background-image:url('vlogo')">
						<!--<span class="center label label-info" style="position: absolute;right: 25px;top: 15px;font-size: 13px;">vsatuan</span>-->
						<span class="capt-distance2 label label-warning">vdistance</span>
					</div>
					<div class="caption  margin-top-0">
						<h3 class="capt-title2 ">vtitle</h3>
						<p class="capt-address ">vaddress</p>
					</div>
				</div>
				</a>
			</div>
		</div>
		<div class="template-product2 hidden"> 
			<div class="col-sm-6 col-md-6 col-lg-4">
				<a href="<?= Yii::app()->createUrl('store/menu/merchant')?>/vurlmerchant">
				<div class="thumbnail no-padding thumb-merchant" style="height:320px;border: 1px solid #dcd9d9;">
					<div src="" class="img-responsive logo-merchant" style="background-image:url('<?= uploadURL()."/"?>vphoto')">
						<span class="center label label-info" style="position: absolute;right: 25px;top: 15px;font-size: 13px;">vsatuan</span>
						<span class="capt-distance2 label label-warning">vdistance</span>
					</div>
					<div class="caption  margin-top-0">
						<h3 class="capt-title2">vtitle</h3>
						<p class="capt-address">vmerchant<br/>vaddress</p>
					</div>
				</div>
				</a>
			</div>
		</div>
		<div class="template-product hidden"> 
			<div class="col-sm-4 col-md-3 ">
				
				<div class="thumbnail no-padding thumb-product" style="height:425px;border: 1px solid #dcd9d9;">
					<a style="" class=" btn btn-danger btn-sm btn-product"><span class="glyphicon glyphicon-thumbs-up"></span> Quick View</a> 
					<div src="" class="img-responsive margin-top-0" style="background-size:cover;height:200px;width:100%;background-image:url('<?= uploadURL()."/"?>vphoto')"></div>
					
					<div class="caption center margin-top-30">
						<h3 class="capt-title center" style="color:#be1e2d">vtitle</h3>
						<p class="capt-price center" >vprice</p>						
						<a class="caption-merchant" href="<?= Yii::app()->createUrl('store/menu/merchant')?>/vurlmerchant">vmerchant</a>
						<span class="center label label-info" style="position: absolute;right: 25px;top: 40px;font-size: 13px;">vsatuan</span>
						<span class="capt-city label label-warning">vcity</span><span class="capt-distance label label-warning">vdistance</span>
					</div>
				</div>
			</div>
		</div>

		
<div class="sections section-feature-resto">
<div class="container margin-top-50 hidden">
	<div class="row">
		<div class="col-md-12 margin-bottom-40">
				<div class="block">
					<div class="block-heading"  style="margin-top:0;margin-bottom:0">
						<h2><?=t("Search Products")?></h2>
					</div>
				</div>
		</div>
	</div>
</div>


<div class="container box-filter" style="margin-top:30px">
<form class=" filter-forms">
<input type="hidden" id="action" name="action" value="searchProduct"/>
<input type="hidden" id="latitude" name="latitude" value="-6.182368372015954"/>
<input type="hidden" id="longitude" name="longitude" value="106.78606855117187"/>
<input type="hidden" id="address" name="address" value="Jalan Supratman Bandung"/>
<input type="hidden" id="url_ajax" name="url_ajax" value="<?= Yii::app()->baseUrl ?>/mobileapp/api"/>
<div class="row margin-bottom-10">
	<div class="col-md-4"> 

		<style type='text/css'>
			div.selectBox{width:100%;position: relative; display: inline-block; cursor: default; text-align: left; line-height: 30px; clear: both; color: rgb(114, 97, 97);}
			span.selected{width: 88.5%; text-indent: 10px; border: 1px solid #708090; border-right: none; border-top-left-radius: 5px; border-bottom-left-radius: 5px; background: #f6f6f6; overflow: hidden;font-size: 17px;color: #708090;
                        }
			span.selectArrow{width: 40px; background: #adabab;border: 1px solid #708090;border-left:none; color: #fff; border-top-right-radius: 5px; border-bottom-right-radius: 5px; text-align: center; font-size: 13px; -webkit-user-select: none; -khtml-user-select: none; -moz-user-select: none; -o-user-select: none; user-select: none; }
			span.selectArrow,span.selected{position: relative; float: left; height: 40px; z-index: 1;padding: 4px;}
			ul.selectOptions{    width: 99%;
				position: absolute;
				top: 40px;
				left: 0;
				border: 1px solid #adabab;
				overflow: hidden;
				background: rgb(250, 250, 250);
				padding-top: 2px;
				margin: 0;
				list-style: none inside none;
				padding-left: 0;
				z-index: 7;
				border-radius: 5px;
				display: none;
			}
			li.selectOption{
				display: block !important;
				line-height: 20px;
				padding: 8px 0 5px 10px !important;
				font-size: 15px !important;
				list-style: none;
				margin: 0;
				height: 40px;
			}
			li.selectOption:hover{color: #f6f6f6;background: #4096ee;}
			.box-filter .form-control {
				height: 40px;
				border-radius:5px;
				border: 1px solid #708090;
				color:#708090;
			}
			.box-filter {
				font-family: 'Open Sans';
				font-size: 16px;
				/*color: #3a3838;*/
			}
		</style>
	
		<div class='selectBox'>
			<input type="hidden" value="" class="se-category"/>
			<span class='selected' ></span>
			<span class='selectArrow'>&#9660 </span>
			<ul class="selectOptions" data-class="se-category">
				<li class="selectOption" data-value="value 1">-- <?=t('Category')?> --</li>
				<?php 
				$result = query("SELECT * FROM {{category}}");
				foreach($result as $r) {
					echo '<li class="selectOption" data-value="'.$r['cat_id'].'" '. (($category == $r['cat_id']) ? 'selected':'') .' >'.$r['category_name'].'</li>';
				}
				?>
			</ul>
		</div>
		<select name="category" class="hidden form-control category select-filter">
		<option value="">-- <?=t('Category')?> --</option>
		<?php 
		$result = query("SELECT * FROM {{category}}");
		foreach($result as $r) {
			echo "<option value='".$r['cat_id']."' ". (($category == $r['cat_id']) ? 'selected':'') ." >".$r['category_name']."</option>";
		}
		?>
		</select>
	</div>
	<div class="col-md-4">  	
		<div class="input-group hidden">
			<input  type="text" class="form-control" id="exampleInputAmount" placeholder="Type Keywords and press enter...">
			<div style="background-color: #bf202f;color: #fff;border: 1px solid #bf202f;" class="input-group-addon"><i class="fa fa-search"></i></div>
		</div>
		<input type="text" class=" form-control keyword" name="keyword"  placeholder="<?=t('Type Keywords and press enter...')?>">
	</div>
	<div class="col-md-4">
		<div class='selectBox'>
			<input type="hidden" value="" class="se-location"/>
			<span class='selected'></span>
			<span class='selectArrow'>&#9660 </span>
			<ul class="selectOptions" data-class="se-location"  >
				<li class="selectOption" data-value="">-- <?=t('Location')?> --</li>
				<li class="selectOption" data-value="semua"><?=t('All')?></li>
				<li class="selectOption" data-value="city"><?=t('City')?></li>
				<li class="selectOption" data-value="current"><?=t('Current Location')?></li>
			</ul>
		</div>
		<select name="location" class="hidden form-control select-filter location">
			<option value="">-- <?=t('Location')?> --</option>
			<option value="semua" <?= $location == 'all'?'selected':''?> ><?=t('All')?></option>
			<option value="city" <?= $location == 'city'?'selected':''?> ><?=t('City')?></option>
			<option value="current" <?= $location == 'current'?'selected':''?> ><?=t('Current Location')?></option>
		</select>
	</div>
</div>
<div class="row margin-top-20">
	<div class="col-md-4">
		<div class='selectBox'>
			<input name="satuan" type="hidden" value="" class="se-satuan"/>
			<span class='selected'></span>
			<span class='selectArrow'>&#9660 </span>
			<ul class="selectOptions" data-class="se-satuan" >
				<li class="selectOption" data-value="">-- Satuan --</li>
				<li class="selectOption" data-value="grosir"><?=t('Wholesale')?></li>
				<li class="selectOption" data-value="eceran"><?=t('Retail')?></li>
			</ul>
		</div>
		<select name="satuan" class="hidden form-control select-filter">
		<option value="">-- <?=t('Satuan')?> --</option>
		<option value="grosir" <?= $satuan == 'grosir'?'selected':''?> ><?=t('Wholesale')?></option>
		<option value="eceran" <?= $satuan == 'eceran'?'selected':''?> ><?=t('Retail')?></option>
		</select>
	</div>
	<div class="col-md-4">
		<div class='selectBox'>
			<input type="hidden" value="merchant" class="se-output"/>
			<span class='selected'></span>
			<span class='selectArrow'>&#9660 </span>
			<ul class="selectOptions"  data-class="se-output">
				<li class="selectOption" data-value="merchant"><?=t('Merchant')?></li>
				<li class="selectOption" data-value="product"><?=t('Product')?></li>
			</ul>
		</div>
		<select name="output" class="hidden form-control select-filter output-filter">
		<option value="merchant" <?= $output == 'merchant'?'selected':''?> ><?=t('Merchant')?></option>
		<option value="product" <?= $output == 'product'?'selected':''?> ><?=t('Product')?></option>
		</select>
	</div>
	<div class="col-md-4">
		<div class="input-group hidden">
			<input type="text" class="form-control" id="exampleInputAmount" placeholder="Type City Location and press enter...">
			<div style="background-color: #bf202f;color: #fff;border: 1px solid #bf202f;" class="input-group-addon"><i class="fa fa-search"></i></div>
		</div>
		<input  type="text" class=" form-control city" name="city" id="city" placeholder="<?=t('Type City Location and press enter...')?>" disabled>
	</div>
</div>
</form>
</div>

</div>


<div class="result-merchant infinite-container" id="restuarant-list">

</div> <!--result-merchant-->

<?php             
if (isset($cuisine_page)){
	//$page_link=Yii::app()->createUrl('store/cuisine/'.$category.'/?');
	$page_link=Yii::app()->createUrl('store/cuisine/?category='.urlencode($_GET['category']));
} else $page_link=Yii::app()->createUrl('store/browse/?tab='.$tabs);

 echo CHtml::hiddenField('current_page_url',$page_link);
 require_once('pagination.class.php'); 
 $attributes                 =   array();
 $attributes['wrapper']      =   array('id'=>'pagination','class'=>'pagination');			 
 $options                    =   array();
 $options['attributes']      =   $attributes;
 $options['items_per_page']  =   FunctionsV3::getPerPage();
 $options['maxpages']        =   1;
 $options['jumpers']=false;
 $options['link_url']=$page_link.'&page=##ID##';			
 $pagination =   new pagination( $list['total'] ,((isset($_GET['page'])) ? $_GET['page']:1),$options);		
 $data   =   $pagination->render();
 ?>             

 
 
 
 
 



