

<div class="mobile-banner-wrap relative">
 <div class="layer"></div>
 <img class="mobile-banner" src="<?php echo assetsURL()."/images/banner.jpg"?>">
</div>

<div id="parallax-wrap" class="parallax-search" 
data-parallax="scroll" data-position="top" data-bleed="10" 
data-image-src="<?php echo assetsURL()."/images/banner.jpg"?>">

<div class="search-wraps">
	<h1><?php echo $h1?></h1>
	<p><?php echo $sub_text?></p>
</div> <!--search-wraps-->

</div> <!--parallax-container-->