<div class="text-right">
<a href="javascript:;" class="write-review-new btn btn-danger inline bottom10 upper">
<?php echo t("write a review")?>
</a>
</div>
<div class="review-input-wrap box-grey rounded5 margin-vert-10">
<div class="bottom10 row ">
<form class="forms" id="forms-review" onsubmit="return false;">
<?php echo CHtml::hiddenField('action','addReviews')?>
<?php echo CHtml::hiddenField('currentController','store')?>
<?php echo CHtml::hiddenField('merchant-id',$merchant_id)?>
	<input type="hidden" value="" name="initial_review_rating" id="initial_review_rating2"> 
   <div class="col-md-12 border">
	<div class="input-group input-group-www">
	  <span class="input-group-addon" style="background:white">
		<i class="fa fa-pencil"></i>
	  </span>
	  
	  <input style="border-left:0" type="text" class="form-control grey-inputs" placeholder="Write a review..."  required="1" name="review_content" class="review_content" id="review_content">
	  <span class="input-group-btn">
		<button class="btn btn-success inline" type="submit"><?php echo t("PUBLISH REVIEW")?></button>
	  </span>
	</div><!-- /input-group -->
	 <div class="margin-top-5">
	 <div class="raty-stars raty-stars2" data-score="5"></div>   
	 </div>				
	
   </div> <!--col-->	        
</form>
</div> <!--review-input-wrap-->
</div>

<div class="merchant-review-wrap" style="margin-top:0;"></div>