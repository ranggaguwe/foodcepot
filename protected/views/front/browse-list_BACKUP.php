<style>
.caption {
	color:#5d5b5b;
}
.caption-merchant {
	color: #5d5b5b;
    font-size: 15px;
    margin-bottom: 17px;
    display: block;
    font-family: 'Helvetica Neue';
}
.capt-price {
    font-size: 17px;
    font-family: 'Helvetica Neue';
}
.btn-product {
	background: #be1e2d;
	width: 100%;
    border-radius: 0;
    position: relative;
    top: 230px;
}
.capt-title {
	font-size: 22px;
	font-family: 'Raleway';
	
}
.capt-city {
    font-size: 15px;
    margin-left: 10px;
	float:left;
}
.capt-distance {
    font-size: 15px;
    margin-right: 10px;
	float:right;
}
.capt-title2 {
	font-size: 22px;
    position: absolute;
    bottom: 65px;
    left: 40px;
    color: #be1e2d;
    font-family: 'Raleway';
    
}
.capt-address {
	font-size: 17px;
	position: absolute;
    bottom: 30px;
    left: 40px;
	
}

.capt-distance2 {
    font-size: 15px;
	position: absolute;
    top: 160px;
    right: 40px;
}
.logo-merchant {
	background-size: cover;
    height: 200px;
    width: 100%;
    background-repeat: no-repeat;
}
.thumb-merchant {
	background-color: #f3ebeb;
}

</style>
		<div class="row hidden">
			<div class="col-sm-4 col-md-6 ">
				<div class="thumbnail thumb-merchant no-padding" style="height:200px;border: 1px solid #dcd9d9;">
					<div src="" class="img-responsive margin-top-0" style="background-size:cover;height:200px;width:100%;background-image:url('/foodapp/upload/1469766130-images-(21).jpg')">
						<span class="center label label-info" style="position: absolute;right: 25px;top: 15px;font-size: 13px;">grosir</span>
						<h3 class="capt-title2 center">Beras Putih </h3>
						<p class="capt-address center">Jl. Setiabudi No.23, Jakarta Utara </p>
						<span class="capt-distance2 label label-warning">4,46 km</span>
					</div>
				</div>
			</div>
			<div class="col-sm-4 col-md-6 ">
				<div class="thumbnail no-padding" style="height:200px;border: 1px solid #dcd9d9;">
					<div src="" class="img-responsive margin-top-0" style="background-size:cover;height:200px;width:100%;background-image:url('/foodapp/upload/1469766130-images-(21).jpg')">
						<span class="center label label-info" style="position: absolute;right: 25px;top: 15px;font-size: 13px;">grosir</span>
						<h3 class="capt-title2 center">Beras Putih </h3>
						<p class="capt-address center">Jl. Setiabudi No.23, Jakarta Utara </p>
						<span class="capt-distance2 label label-warning">4,46 km</span>
					</div>
				</div>
			</div>
		</div>
		<div class="template-merchant hidden"> 
			<div class="col-sm-6 col-md-6 col-lg-4">
				<a href="<?= Yii::app()->createUrl('store/menu/merchant')?>/vurlmerchant">
				<div class="thumbnail no-padding thumb-merchant" style="height:290px;border: 1px solid #dcd9d9;">
					<div src="" class="img-responsive logo-merchant" style="background-image:url('vlogo')">
						<span class="center label label-info" style="position: absolute;right: 25px;top: 15px;font-size: 13px;">grosir</span>
						<span class="capt-distance2 label label-warning">vdistance</span>
					</div>
					<div class="caption center margin-top-0">
						<h3 class="capt-title2 center">vtitle</h3>
						<p class="capt-address center">vaddress</p>
					</div>
				</div>
				</a>
			</div>
		</div>
		<div class="template-product hidden"> 
			<div class="col-sm-4 col-md-3 ">
				
				<div class="thumbnail no-padding thumb-product" style="height:425px;border: 1px solid #dcd9d9;">
					<a style="" class=" btn btn-danger btn-sm btn-product"><span class="glyphicon glyphicon-thumbs-up"></span> Quick View</a> 
					<div src="" class="img-responsive margin-top-0" style="background-size:cover;height:200px;width:100%;background-image:url('<?= uploadURL()."/"?>vphoto')"></div>
					
					<div class="caption center margin-top-30">
						<h3 class="capt-title center" style="color:#be1e2d">vtitle</h3>
						<p class="capt-price center" >vprice</p>						
						<a class="caption-merchant" href="<?= Yii::app()->createUrl('store/menu/merchant')?>/vurlmerchant">vmerchant</a>
						<span class="center label label-info" style="position: absolute;right: 25px;top: 40px;font-size: 13px;">vsatuan</span>
						<span class="capt-city label label-warning">vcity</span><span class="capt-distance label label-warning">vdistance</span>
					</div>
				</div>
			</div>
		</div>


<div class="result-merchant infinite-container" id="restuarant-list">

<?php foreach ($list['list'] as $val):?>
<?php
$merchant_id=$val['merchant_id'];
$ratings=Yii::app()->functions->getRatings($merchant_id);   
$merchant_delivery_distance=getOption($merchant_id,'merchant_delivery_miles');
$distance_type='';

/*fallback*/
if ( empty($val['latitude'])){
	if ($lat_res=Yii::app()->functions->geodecodeAddress($val['merchant_address'])){        
		$val['latitude']=$lat_res['lat'];
		$val['longitude']=$lat_res['long'];
	} 
}
?>

<div class="infinite-item">
   <div class="inner">
   
   <?php if ( $val['is_sponsored']==2):?>
       <div class="ribbon"><span>Sponsored</span></div>
    <?php endif;?>
   
     <div class="row"> 
        <div class="col-md-6 borderx">
        
         <div class="row borderx" style="padding: 10px;padding-bottom:0;">
             <div class="col-md-3 borderx ">
		       <a href="<?php echo Yii::app()->createUrl('store/menu/merchant/'.$val['merchant_slug'])?>">
		        <img class="logo-small"src="<?php echo FunctionsV3::getMerchantLogo($merchant_id);?>">
		       </a>
		       <div class="top10"><?php echo FunctionsV3::displayServicesList($val['service'])?></div>		               
		       
		       <div class="top10">
		         <?php FunctionsV3::displayCashAvailable($merchant_id)?>
		       </div>
		       
		     </div> <!--col-->
		     <div class="col-md-9 borderx">
		     
		     <div class="mytable">
		         <div class="mycol">
		            <div class="rating-stars" data-score="<?php echo $ratings['ratings']?>"></div>   
		         </div>
		         <div class="mycol">
		            <p><?php echo $ratings['votes']." ".t("Reviews")?></p>
		         </div>
		         <div class="mycol">
		            <?php echo FunctionsV3::merchantOpenTag($merchant_id)?>                
		         </div>		         		         		         
		      </div> <!--mytable-->
	       
		      <h2><?php echo clearString($val['merchant_name'])?></h2>
	          <p class="merchant-address concat-text"><?php echo $val['merchant_address']?></p>   
	          
		     
              <p><?php echo t("Minimum Order").": ".FunctionsV3::prettyPrice($val['minimum_order'])?></p>
              
              <?php if($val['service']!=3):?>
              <p><?php echo t("Delivery Est")?>: <?php echo FunctionsV3::getDeliveryEstimation($merchant_id)?></p>
              <?php endif;?>
              
              <p>
		        <?php 
		        if($val['service']!=3){
			        if (!empty($merchant_delivery_distance)){
			        	echo t("Delivery Distance").": ".$merchant_delivery_distance." $distance_type";
			        } else echo  t("Delivery Distance").": ".t("not available");
		        }
		        ?>
		       </p>
		       		       
		       <?php if($val['service']!=3):?>
		        <p class="top15"><?php echo FunctionsV3::getFreeDeliveryTag($merchant_id)?></p>
		       <?php endif;?>
		        
		        <a href="<?php echo Yii::app()->createUrl('store/menu/merchant/'.$val['merchant_slug'])?>" 
		        class="orange-button rounded3 medium bottom10 inline-block">
		        <?php echo t("View menu")?>
		        </a>
		                      
		     </div> <!--col-->
         </div> <!--row-->         
         
        </div> <!--col-->
        
        <!--MAP-->
        <div class="col-md-6 with-padleft" style="padding-left:0; border-left:1px solid #C9C7C7;" >
          <div class="browse-list-map active" 
		        data-lat="<?php echo $val['latitude']?>" data-long="<?php echo $val['longitude']?>">
             
          </div> <!--browse-list-map-->
        </div> <!--col-->
        
     </div> <!--row-->
   </div> <!--inner-->
</div> <!--infinite-item-->
<?php endforeach;?>
</div> <!--result-merchant-->

<div class="search-result-loader">
    <i></i>
    <p><?php echo t("Loading more restaurant...")?></p>
 </div> <!--search-result-loader-->

<?php             
if (isset($cuisine_page)){
	//$page_link=Yii::app()->createUrl('store/cuisine/'.$category.'/?');
	$page_link=Yii::app()->createUrl('store/cuisine/?category='.urlencode($_GET['category']));
} else $page_link=Yii::app()->createUrl('store/browse/?tab='.$tabs);

 echo CHtml::hiddenField('current_page_url',$page_link);
 require_once('pagination.class.php'); 
 $attributes                 =   array();
 $attributes['wrapper']      =   array('id'=>'pagination','class'=>'pagination');			 
 $options                    =   array();
 $options['attributes']      =   $attributes;
 $options['items_per_page']  =   FunctionsV3::getPerPage();
 $options['maxpages']        =   1;
 $options['jumpers']=false;
 $options['link_url']=$page_link.'&page=##ID##';			
 $pagination =   new pagination( $list['total'] ,((isset($_GET['page'])) ? $_GET['page']:1),$options);		
 $data   =   $pagination->render();
 ?>             

 
 
 
 
 



