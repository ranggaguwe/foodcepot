<?php 
if (isset($_GET['location'])) {
	$location = $_GET['location'];
} else {
	$location = "";
}
if (isset($_GET['category'])) {
	$category = $_GET['category'];
} else {
	$category = "";
}
if (isset($_GET['satuan'])) {
	$satuan = $_GET['satuan'];
} else {
	$satuan = "";
}
if (isset($_GET['o'])) {
	$output = $_GET['o'];
} else {
	$output = "";
}
?>
<div class="mobile-banner-wrap relative">
 <div class="layer"></div>
 <img class="mobile-banner" >
</div>

<div  id="parallax-wrap" class="parallax-search" 
data-parallax="scroll" data-position="center" data-bleed="0" 
data-image-src="<?php echo assetsURL()."/images/banner.jpg"?>">


<style>

.parallax-search {
    min-height: 400px;
}
.home-title {
	color:#fff;
	margin-top:0;
    font-family: 'Open Sans';
    font-weight: 700;
    font-size: 56px;
    /*text-transform: uppercase;*/
    text-shadow: 1px 3px 3px rgba(0, 0, 0, 0.35);
}
.home-desc {
	color:#fff;
	text-shadow: 1px 3px 3px rgba(0, 0, 0, 0.35);
	font-size:18px;
}
</style>
<div class="search-wraps">
	<h1><?php echo isset($h1)?$h1:''?></h1>
	<p><?php echo isset($sub_text)?$sub_text:''?></p>
</div> <!--search-wraps-->

<style>
.box-filter .form-control {
	height: 40px;
	border-radius:0px;
}
</style>

<div class="container box-filter hidden" style="margin-top:100px">
<form class=" filter-forms">
<input type="hidden" id="action" name="action" value="searchProduct"/>
<input type="hidden" id="latitude" name="latitude" value="-6.182368372015954"/>
<input type="hidden" id="longitude" name="longitude" value="106.78606855117187"/>
<input type="hidden" id="address" name="address" value="Jalan Supratman Bandung"/>
<input type="hidden" id="url_ajax" name="url_ajax" value="<?= Yii::app()->baseUrl ?>/mobileapp/api"/>
<div class="row margin-bottom-10">
	<div class="col-md-3">  
		<select name="category" class="form-control category select-filter">
		<option value="">-- <?=t('Category')?> --</option>
		<?php 
		$result = query("SELECT * FROM {{category}}");
		foreach($result as $r) {
			echo "<option value='".$r['cat_id']."' ". (($category == $r['cat_id']) ? 'selected':'') ." >".$r['category_name']."</option>";
		}
		?>
		</select>
	</div>
	<div class="col-md-3">  
		<input type="text" class="form-control keyword" name="keyword"  placeholder="<?=t('Type Keywords and press enter...')?>">
	</div>
	<div class="col-md-3">
		<select name="satuan" class="form-control select-filter">
		<option value="">-- <?=t('Satuan')?> --</option>
		<option value="grosir" <?= $satuan == 'grosir'?'selected':''?> ><?=t('Wholesale')?></option>
		<option value="eceran" <?= $satuan == 'eceran'?'selected':''?> ><?=t('Retail')?></option>
		</select>
	</div>
	<div class="col-md-3">
		<select name="location" class="form-control select-filter location">
			<option value="">-- <?=t('Location')?> --</option>
			<option value="semua" <?= $location == 'all'?'selected':''?> ><?=t('All')?></option>
			<option value="city" <?= $location == 'city'?'selected':''?> ><?=t('City')?></option>
			<option value="current" <?= $location == 'current'?'selected':''?> ><?=t('Current Location')?></option>
		</select>
	</div>
	
</div>
<div class="row">

	<div class="col-md-offset-9 col-md-3 hidden">
		<select name="output" class="form-control select-filter output-filter">
		<option value="merchant" <?= $output == 'merchant'?'selected':''?> ><?=t('Merchant')?></option>
		<option value="product" <?= $output == 'product'?'selected':''?> ><?=t('Product')?></option>
		</select>
	</div>
	<div class="col-md-offset-9 col-md-3">
		<input  type="text" class="form-control city" name="city" id="city" placeholder="<?=t('Type City Location and press enter...')?>" disabled>
	</div>
</div>
</form>
</div>
</div> <!--parallax-container-->

