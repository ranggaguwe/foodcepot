
<?php if(is_array($menu) && count($menu)>=1):?>
<?php foreach ($menu as $val): //dump($val);?>
<div class="section-label menu-cat cat-<?php echo $val['category_id']?>">
    <a class="section-label-a">
      <span class="bold">
      <?php echo qTranslate($val['category_name'],'category_name',$val)?>
      </span>
      <b></b>
    </a>     
</div>    
<?php if (!empty($val['category_description'])):?>
<p class="small"><?php echo $val['category_description']?></p>
<?php endif;?>
<div class="row menu-2 border">

<?php $x=0?>
<?php if (is_array($val['item']) && count($val['item'])>=1):?>
<?php foreach ($val['item'] as $val_item):?>
<?php 
// dump($val_item);
?>

<div class="template-merchant"> 
	<div class="col-sm-12 col-md-6 col-lg-6">
		<a href="javascript:;" 
		class="menu-item <?php echo $val_item['not_available']==2?"item_not_available":''?>"
		rel="<?php echo $val_item['item_id']?>"
		data-single="" 
		>				
		<div class="thumbnail no-padding thumb-search" style="height:340px;border: 0px solid #dcd9d9;">
			<div src="" class="img-responsive logo-search3" style="background-image:url('<?php echo uploadURL()."/".$val_item['photo'];?>')">
				<?php if(!$val_item['wholesale_flag']) {?>
				<span class="center label label-info" style="position: absolute;right: 25px;top: 15px;font-size: 13px;">eceran</span>
				<?php } else { ?>
				<span class="center label label-success" style="position: absolute;right: 25px;top: 15px;font-size: 13px;">grosir</span>
				<span class="center label label-info" style="position: absolute;right: 25px;top: 43px;font-size: 13px;">eceran</span>
				<?php } ?>
				<span class="capt-distance4 label label-warning"><?=isset($distance)?number_format($distance, 2, ',', ' ')." km":""?></span>
			</div>
			<div class="caption caption-merchant2 margin-top-0">
				<span class="capt-search2 "><?=$val_item['item_name']?></span>
				<span class="capt-retail-price2 ">Eceran <?=FunctionsV3::prettyPriceUnit($val_item['retail_price'],$val_item['uom'])?></span>
				<span class="capt-city"><?=$val_item['city']?></span>
			</div>
			<div class="thumb-red"><div class="thumb-inside2"><div><?=$merchant_info['merchant_name']?></div></div> </div><span class="btn-beli2">BELI</span>
		</div>
		</a>
	</div>
</div>

<div class="template-product2 hidden"> 
	<div class="col-sm-12 col-md-6 col-lg-6">
		<a href="javascript:;" 
		class="menu-item <?php echo $val_item['not_available']==2?"item_not_available":''?>"
		rel="<?php echo $val_item['item_id']?>"
		data-single="" 
		>
		<div class="thumbnail no-padding thumb-merchant" style="height:290px;border: 1px solid #dcd9d9;">
			<div src="" class="img-responsive logo-merchant" style="background-image:url('<?php echo uploadURL()."/".$val_item['photo'];?>')">
				<span class="center label label-<?=$val_item['wholesale_flag']?'info':'success'?>" style="position: absolute;right: 25px;top: 15px;font-size: 13px;"><?=$val_item['wholesale_flag']?'grosir':'eceran'?></span>
			</div>
			<div class="caption  margin-top-0">
				<h3 class="capt-title2"><?php echo qTranslate($val_item['item_name'],'item_name',$val_item)?></h3>
				<p class="capt-address">Eceran : <?=$val_item['currency_retail_price']?></p>
			</div>
		</div>
		</a>
	</div>
</div>
<div class="col-md-4 hidden" style="padding-left:10px;padding-right:10px;">
   <div class="box-grey rounded5 " style="height:350px">
     <div class="food-thumbnail" 
        style="background:url('<?php echo uploadURL()."/".$val_item['photo'];?>');">
     </div>
     <p class="bold top10 center"><?php echo qTranslate($val_item['item_name'],'item_name',$val_item)?></p>

	 <p class='center'><span class="label label-success label-eceran center">ECERAN</span> </p>
	 <?php echo "<p class='center'>".$val_item['currency_retail_price']."</p>";
	 if ($val_item['wholesale_price'] !== false) {
		 ?><p class='center'><span class="label label-info label-grosir center">GROSIR </span></p><?php
			foreach($val_item['wholesale_price'] as $wp) {
				?>
					<p class='center'><?=$wp['start'].' - '.$wp['end'].' '.$val_item['uom']?> 
					<?=$wp['currency_price']?><?=empty($data['uom'])?'':'/'.$data['uom']?></p>
				<?php
			}
	 }
	 ?>

     <?php 
     if (strlen($val_item['item_description'])<59){
        echo '<div class="dummy-link"></div>';
     }
     ?>
     
     <?php if ( $disabled_addcart==""):?>
     <div class="center top10 food-price-wrap">
     <a href="javascript:;" 
     class="dsktop btn btn-danger inline rounded3 menu-item <?php echo $val_item['not_available']==2?"item_not_available":''?>"
     rel="<?php echo $val_item['item_id']?>"
     data-single="" 
      >
     <?php echo t('Quick View') ?>
     </a>
     
     <a href="javascript:;" 
     class="mbile orange-button inline rounded3 menu-item <?php echo $val_item['not_available']==2?"item_not_available":''?>"
     rel="<?php echo $val_item['item_id']?>"
     data-single="" 
      >
     <?php echo t('view') ?>
     </a>
    
     </div>
     <?php endif;?>
     
   </div> <!--box-grey-->
</div> <!--col-->
<?php endforeach;?>
<?php else :?>
<div class="col-md-6 border">
<p class="small text-danger"><?php echo t("no item found on this category")?></p>
</div>
<?php endif;?>


</div> <!--row-->
<?php endforeach;?>

<?php else :?>
<p class="text-danger"><?php echo t("This merchant has not published their menu yet.")?></p>
<?php endif;?>