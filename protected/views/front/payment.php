
<div class="box-grey rounded" style="margin-top:0;">
<div class="row">
<div class="col-md-12">
	<h3><?=t('Balance')?></h3> <h1><?=FunctionsV3::prettyPrice($data['saldo'])//dump($data)?></h1>
</div>	
<div class="col-md-12">
	<h3><?=t('Topup')?></h3> 
	
	<!-- Button trigger modal -->
	<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal">
	  <?=t('Click here to see topup instruction')?>
	</button>	
	<br/><br/>
	<p style="font-weight:bold"><?=t('Your unique code is').' <big>'.($data['client_id'] % 1000)."</big>. ".t('Amount of your transfer must followed with your unique code.')?></p>
</div>	
</div>
<h3><?=t('Form Confirmation')?></h3> 
<form class="forms2" id="forms2" onsubmit="return false;">
<?php echo CHtml::hiddenField('action','TopUpConfirmation')?>
<?php echo CHtml::hiddenField('currentController','store')?>

<?php //FunctionsV3::sectionHeader('Profile');?>

<div class="row bottom10">
  <div class="col-md-6">
    <p class="text-small"><?php echo t("Amount that is Transfered")?></p>
    <?php 
	  echo CHtml::textField('amount','',
	  array(
	    'class'=>'grey-fields full-width',
	    'data-validation'=>"required"
	  ));
	  ?>     
  </div>
</div> <!--row-->
<div class="row bottom10">
  <div class="col-md-6">
	<h5><?=t('Bank Source')?></h5>
    <p class="text-small"><?php echo t("Bank Name")?></p>
	<?php 
	echo CHtml::textField('from_bank','',
	array(
	'class'=>'grey-fields full-width margin-bottom-10',
	'data-validation'=>"required"
	));
	?>
    <p class="text-small margin-top-10"><?php echo t("Account Number")?></p>
    <?php 
	  echo CHtml::textField('from_account','',
	  array(
	    'class'=>'grey-fields full-width',
	    'data-validation'=>"required",
	  ));
	  ?>
  </div>
  <div class="col-md-6">
	<h5><?=t('Bank To')?></h5>
    <p class="text-small"><?php echo t("Bank Name")?></p>
	<?php 
	echo CHtml::textField('to_bank','',
	array(
	'class'=>'grey-fields full-width margin-bottom-10',
	'data-validation'=>"required"
	));
	?>
    <p class="text-small margin-top-10"><?php echo t("Account Number")?></p>
    <?php 
	  echo CHtml::textField('to_account','',
	  array(
	    'class'=>'grey-fields full-width',
	    'data-validation'=>"required",
	  ));
	  ?>
  </div>
</div> <!--row-->
<div class="row bottom10">
  <div class="col-md-6">
    <p class="text-small"><?php echo t("Description")?></p>
	 <?php 
  echo CHtml::textField('description','',
  array(
    'class'=>'grey-fields full-width margin-bottom-10 ',
  ));
  ?>
<div class="uk-form-row"> 
 <label class="uk-form-label"><?php echo Yii::t('default',"Screenshot Proof of Payment")?></label>
  <div style="display:inline-table;margin-left:1px;" class="button uk-button btn btn-info" id="photo2"><?php echo Yii::t('default',"Browse")?></div>	  
  <DIV  style="display:none;" class="photo_chart_status" >
	<div id="percent_bar" class="photo_percent_bar"></div>
	<div id="progress_bar" class="photo_progress_bar">
	  <div id="status_bar" class="photo_status_bar"></div>
	</div>
  </DIV>		  
</div>  


<div class="input_block preview">
<label><?php echo Yii::t('default',"Preview")?></label>
<div class="image_preview2">
</div>
</div>

  
  </div>

</div> <!--row-->


<div class="row">  
  <div class="col-md-6">
	<input type="submit" value="<?php echo t("Confirm")?>" class="green-button medium">  
 </div>	
</div>


</form>
</div> <!--box-->

<div class="box-grey rounded section-order-history" style="margin-top:0;">

<div class="row">
<div class="col-md-12">
	<h3><?=t('List Topup Confirmation')?></h3> 
</div>
</div>
<?php 
$data2 = query("SELECT * FROM {{topup_confirm}} WHERE client_id=? ORDER BY id DESC",array($data['client_id']));
if (is_array($data2) && count($data2)>=1):?>

   <table class="table table-striped">
     <tbody>
     <?php foreach ($data2 as $val):?>     
      <tr class="tr_mobile">
        <td>
        <div class="mytable">
         <div class="mycol" style="width: 10%;"><i style="font-size: 30px;" class="ion-android-arrow-dropright"></i></div>
         <div class="mycol ">
           
           
             <p><?php echo t("ID")?># <?php echo $val['id']?></p>
           
           
           <a href="<?php echo Yii::app()->createUrl('/ajax/viewreceipt',array('order_id'=>$val['order_id'],'post_type'=>'get'))?>"
           class="view-receipt-mobile"  >
             <p><?php echo t("Order")?># <?php echo $val['id']?></p>
           </a>
           
            <p><?php echo t("Placed on");?> 
            <?php echo Yii::app()->functions->translateDate(prettyDate($val['date_created']))?></p>
         </div>
       </div>
        </td>
        
        <td> 
        <p><?php echo t("AMOUNT")?></p>
        <p><?php echo FunctionsV3::prettyPrice($val['amount'])?></p>         
        </td>
        
        <td>
          <a href="javascript:;" class="view-order-history" data-id="<?php echo $val['order_id'];?>">
          <p class="green-text top10 "><?php echo t($val['status'])?></p>
          </a>
        </td>
      </tr>      

      <?php endforeach;?>
     </tbody>
   </table>
<?php else :?>
   <p class="text-danger"><?php echo t("Topup Confirmation history is empty")?></p>
<?php endif;?>


</div> <!--box-grey-->
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><?=t('Topup Instruction')?></h4>
      </div>
      <div class="modal-body">
<pre>
Proses Topup  dilakukan melalui proses bank transfer. 
Berikut ini adalah tahap untuk melakukan topup.
1. Transferkan dengan menggunakan kode unik. 
   Misalkan kode unik Anda Adalah 321. Maka jika Anda 
   ingin mentransfer 50.000, maka transferkanlah 50.321, 
   agar system kami dapat dengan mudah mendeteksi proses 
   transfer Anda. Sehingga saldo Anda menjadi Rp 50.321,-
2. Transferkan ke Rek bank Berikut ini

   Bank BNI 
   No Rek 123123
   Atas Nama :Admin FoodPot
   
   Bank BCA
   No Rek 123213213
   Atas Nama : Admin FoodPot
   
3. Setelah Anda melakukan transfer, lakukan konfirmasi 
   di halaman ini serta screenshot bukti pembayarannya kalau ada
4. Status Anda masih berstatus pending saat Anda melakukan 
   konfirmasi. Kami akan melakukan pengecekan, jika proses 
   transfer benar, maka kami akan mengubahnya berstatus sukses 
   dan saldo Anda bertambah.
</pre>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
