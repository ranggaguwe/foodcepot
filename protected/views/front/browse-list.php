
		<div class="template-merchant hidden">
			<div class="col-sm-6 col-md-6 col-lg-4">
				<a href="<?= Yii::app()->createUrl('store/menu/merchant')?>/vurlmerchant">
				<div class="thumbnail no-padding thumb-search" style="height:260px;border: 0px solid #dcd9d9;">
					<div src="" class="img-responsive logo-search" style="background-image:url('vlogo')">
						vsatuan
						<span class=" capt-distance3 label label-warning">vdistance</span>
					</div>
					<div class="caption caption-merchant margin-top-0">
						<span class="capt-search ">vlabel1</span>
						<span class="capt-retail-price ">vlabel2</span>
						<span class="capt-city">vlabel3</span>
					</div>
					<div class="thumb-red"><div class="thumb-inside"><div>vlabel4</div></div> </div><span class="btn-beli">BELI</span>
					<div class="thumb-rating">
					vlabel5
					</div>
				</div>
				</a>
			</div>
		</div>
		<div class="template-product2 hidden"> 
			<div class="col-sm-6 col-md-6 col-lg-4">
				<a href="<?= Yii::app()->createUrl('store/menu/merchant')?>/vurlmerchant">
				<div class="thumbnail no-padding thumb-merchant" style="height:320px;border: 1px solid #dcd9d9;">
					<div src="" class="img-responsive logo-merchant" style="background-image:url('<?= uploadURL()."/"?>vphoto')">
						<span class="center label label-info" style="position: absolute;right: 25px;top: 15px;font-size: 13px;">vsatuan</span>
						<span class="capt-distance2 label label-warning">vdistance</span>
					</div>
					<div class="caption  margin-top-0">
						<h3 class="capt-title2">vtitle</h3>
						<p class="capt-address">vmerchant<br/>vaddress</p>
					</div>
				</div>
				</a>
			</div>
		</div>
		<div class="template-product hidden"> 
			<div class="col-sm-4 col-md-3 ">
				<div class="thumbnail no-padding thumb-product" style="height:425px;border: 1px solid #dcd9d9;">
					<a style="" class=" btn btn-danger btn-sm btn-product"><span class="glyphicon glyphicon-thumbs-up"></span> Quick View</a> 
					<div src="" class="img-responsive margin-top-0" style="background-size:cover;height:200px;width:100%;background-image:url('<?= uploadURL()."/"?>vphoto')"></div>
					
					<div class="caption center margin-top-30">
						<h3 class="capt-title center" style="color:#be1e2d">vtitle</h3>
						<p class="capt-price center" >vprice</p>						
						<a class="caption-merchant" href="<?= Yii::app()->createUrl('store/menu/merchant')?>/vurlmerchant">vmerchant</a>
						<span class="center label label-info" style="position: absolute;right: 25px;top: 40px;font-size: 13px;">vsatuan</span>
						<span class="capt-city label label-warning">vcity</span><span class="capt-distance label label-warning">vdistance</span>
					</div>
				</div>
			</div>
		</div>

		


<div class="row" style="margin-left:0;margin-right:0">
	<div class="col-md-2 box-grey rounded5 sidebar-filter">
		<h2 style="text-align:left">Filter</h2>
		<hr style="margin-top: 10px;color: black;border-top: 1px solid black;"/>
		<h3 style="margin-bottom: 7px">Sortir </h3>
			<div class="choose-sortby" data-id="views">Popularitas</div>
			<div class="sortby-desc" data-id="">Tertinggi - Terendah</div>
			<div class="choose-sortby" data-id="price">Harga</div>
			<div class="sortby-desc" data-id="">Terendah - Tertinggi</div>
			<div class="choose-sortby" data-id="date_created">Terbaru</div>
			<div class="sortby-desc" data-id="">Terbaru - Terlama</div>
			<div class="choose-sortby choose-bold" data-id="">Jarak Terdekat</div>
			<div class="sortby-desc" data-id="">Terdekat - Terjauh</div>
		<br/>
		<h3 style="margin-bottom: 7px">Kategori</h3>
		<?php 
		$result = FunctionsV3::queryO('SELECT * FROM {{category}}');
		// dump($result);
		foreach ($result as $r) {
		?>
		<div class="">
			<div class="choose-cat" data-id="<?=$r->cat_id?>" data-name="<?=$r->category_name?>"><?=$r->category_name?></div>
		</div>
		<?php 
		}
		?>
	</div>
	
	<style>
	.sortby-desc {
		cursor: pointer;
		cursor: hand;
		font-family: 'Helvetica Neue';
		/* font-weight: 700; */
		font-size: 13px;
		line-height: 1;
		margin-bottom: 9px;
		color: #bf1e2e;
	}
	.choose-sortby {
		cursor: pointer;
		cursor: hand;
		line-height: 1.5;
		font-family: 'Helvetica Neue';
		/* font-weight: 700; */
		font-size: 17px;
	}
	.choose-bold {
		color: #000;
		font-weight: bold;
	}
	.thumb-red {
		background-color: #bf1e2e;
		height: 40px;
		width: 70%;
		color: white;
		font-size: 13px;
		float: left;
		display: table;
		padding-left: 10px;
		font-family: 'Open Sans';
		font-weight: 500;
	}
	.thumb-inside {
		display: table-cell; vertical-align: middle;
	}
	.thumb-rating {
		height: 40px;
		width: 100%;
		font-size: 13px;
		float: left;
		padding: 9px 8px;
		font-family: 'Open Sans';
		font-weight: 500;
		text-align:center;
	}
	.btn-beli {
		background-color: #bf1e2e;
		color: white;
		height: 40px;
		width: 30%;
		border-left: 1px solid white;
		display: inline-block;
		padding: 6px 8px;
		font-size: 14px;
		padding: 10px 8px;
		text-align: center;
		font-family: 'Raleway';
		font-weight: 500;
	}
	.capt-retail-price {
		font-size: 14px;
	}
	.caption-merchant {
		height:35px;
		font-family: 'Raleway';
		font-weight: 500;
		padding:5px !important;
		font-size: 15px;
	}
	@media (min-width: 992px) {
		.sponsored-merchant {
			width:20%;
			padding-left:0;
			padding-right:0;
			margin-top:3px;
		}
		.sidebar-filter {
			width:18.66666%;
			height: auto;
			min-height: 1100px;
		}
		.result-merchant {
			width:61.33333%;
		}
		.result-merchant .box-grey {
			height: auto;
			min-height: 1100px;
		}		

	}

	</style>
	<div class="result-merchant infinite-container col-md-8 " id="restuarant-list">
	</div>
	<div class="sponsored-merchant col-md-2">
		<span style="font-family:'Open Sans'">Sponsor</span>
		<?php 
		$sql_near_location = "";
		$result = FunctionsV3::queryO("SELECT b.merchant_id, b.merchant_name,b.merchant_slug,CONCAT(b.street,', ',b.city) address $sql_near_location
										FROM {{merchant}} b 
										WHERE b.is_sponsored=2 AND (b.sponsored_expiration > '".date("m-d-Y")."')
										ORDER BY b.sponsored_expiration ASC
										LIMIT 0,100
										");
		// dump($result);
		foreach ($result as $r) { ?>
		<div class="template-merchant" style="margin-top:4px"> 
			<a href="<?= Yii::app()->createUrl('store/menu/merchant/'.$r->merchant_slug)?>">
			<div class="thumbnail no-padding thumb-sponsor" >
				<div src="" class="img-responsive logo-sponsor" style="background-image:url('<?=AddonMobileApp::getMerchantLogo($r->merchant_id)?>')">
				</div>
				<div class="caption  margin-top-0">
					<h3 class="capt-sponsor "><?=$r->merchant_name?></h3>
					<p class="capt-sponsor-desc "><?=$r->address?></p>
				</div>
			</div>
			</a>
		</div>
		<?php } ?>


	
	
	
	</div>
	<div class="col-md-12 hidden">
	<?php 
	// $res = query("SELECT * FROM {{kabupaten}} WHERE nama LIKE '%kota%'");
	// $hasil = array();
	// foreach($res as $r) {
		// $ganti = ucwords(strtolower(str_replace('KOTA ','',$r['nama'])));
		// $hasil[] = array(
			// 'id'=>$r['id'],
			// 'nama'=>$ganti,
		// ); 
	// }	
	
	// echo json_encode($hasil);

	?>
	</div>
</div> <!--result-merchant-->

<?php             
if (isset($cuisine_page)){
	//$page_link=Yii::app()->createUrl('store/cuisine/'.$category.'/?');
	$page_link=Yii::app()->createUrl('store/cuisine/?category='.urlencode($_GET['category']));
} else $page_link=Yii::app()->createUrl('store/browse/?tab='.$tabs);

 echo CHtml::hiddenField('current_page_url',$page_link);
 require_once('pagination.class.php'); 
 $attributes                 =   array();
 $attributes['wrapper']      =   array('id'=>'pagination','class'=>'pagination');			 
 $options                    =   array();
 $options['attributes']      =   $attributes;
 $options['items_per_page']  =   FunctionsV3::getPerPage();
 $options['maxpages']        =   1;
 $options['jumpers']=false;
 $options['link_url']=$page_link.'&page=##ID##';			
 $pagination =   new pagination( $list['total'] ,((isset($_GET['page'])) ? $_GET['page']:1),$options);		
 $data   =   $pagination->render();
 ?>             

 
 
 
 
 



