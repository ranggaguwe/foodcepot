<?php
$row='';
$item_data='';
$price_select='';
$size_select='';
if (array_key_exists("row",(array)$this->data)){
	$row=$this->data['row'];	
	$item_data=$_SESSION['kr_item'][$row];
	//dump($item_data);
	$price=Yii::app()->functions->explodeData($item_data['price']);
	if (is_array($price) && count($price)>=1){
		$price_select=isset($price[0])?$price[0]:'';
		$size_select=isset($price[1])?$price[1]:'';
	}
	$row++;
}


$data=Yii::app()->functions->getItemById2($this->data['item_id']);

//dump($data);
$disabled_website_ordering=Yii::app()->functions->getOptionAdmin('disabled_website_ordering');
$hide_foodprice=Yii::app()->functions->getOptionAdmin('website_hide_foodprice');
echo CHtml::hiddenField('hide_foodprice',$hide_foodprice);
?>

<?php if (is_array($data) && count($data)>=1):?>
<?php 
$data=$data[0];

// dump($data);
$viewer = query("SELECT * FROM {{item_viewer}} WHERE item_id=? AND ip_address=? AND date>'".date('Y-m-d H:i:s', strtotime('-12 hours'))."'",array($this->data['item_id'],$_SERVER['REMOTE_ADDR']));
// dump($viewer);
if(empty($viewer)) {
	//viewer ditambahkan jika ip address belum tercatat
	queryNoFetch("INSERT INTO {{item_viewer}} (item_id,ip_address,date) VALUES (?,?,now())",array($this->data['item_id'],$_SERVER['REMOTE_ADDR']));
	queryNoFetch("UPDATE {{item}} SET views=? WHERE item_id=?",array($data['views']+1,$this->data['item_id']));
}

?>

<form class="frm-fooditem" id="frm-fooditem" method="POST" onsubmit="return false;">
<?php echo CHtml::hiddenField('action','addToCart')?>
<?php echo CHtml::hiddenField('item_id',$this->data['item_id'])?>
<?php echo CHtml::hiddenField('row',isset($row)?$row:"")?>
<?php echo CHtml::hiddenField('merchant_id',isset($data['merchant_id'])?$data['merchant_id']:'')?>


<?php echo CHtml::hiddenField('discount',isset($data['discount'])?$data['discount']:"" )?>
<?php echo CHtml::hiddenField('currentController','store')?>


<div class="container  view-food-item-wrap">
   
  <!--ITEM NAME & DESCRIPTION-->
  <div class="row">
	<div class="col-md-12"><h3 style="font-weight: bold;font-size: 20px;"><?php echo qTranslate($data['item_name'],'item_name',$data)?></h3></div>
    <div class="col-md-5 ">              
       <img src="<?php echo FunctionsV3::getFoodDefaultImage($data['photo']);?>" alt="<?php echo $data['item_name']?>" title="<?php echo $data['item_name']?>" class=" thumbnail" style="width:100%">
    </div> <!--col-->
    <div class="col-md-7 ">
       <p><?php echo qTranslate($data['item_description'],'item_description',$data)?></p>
    </div> <!--col-->
  </div> <!--row-->
  <!--ITEM NAME & DESCRIPTION-->
     
  <!--FOOD ITEM GALLERY-->
  <?php if (getOption($data['merchant_id'],'disabled_food_gallery')!=2):?>  
  <?php $gallery_photo=!empty($data['gallery_photo'])?json_decode($data['gallery_photo']):false; ?>
     <?php if (is_array($gallery_photo) && count($gallery_photo)>=1):?>
      <div class="section-label margin-bottom-20">
        <a class="section-label-a">
          <span class="bold">
          <?php echo t("Gallery")?></span>
          <b></b>
        </a>     
        <div class="food-gallery-wrap row ">
          <?php foreach ($gallery_photo as $gal_val):?>
          <div class="col-md-3 ">
            <a href="<?php echo websiteUrl()."/upload/$gal_val"?>">
              <div class="food-pic" style="background:url('<?php echo websiteUrl()."/upload/$gal_val"?>')"></div>
              <img style="display:none;" src="<?php echo websiteUrl()."/upload/$gal_val"?>" alt="" title="">
            </a>
          </div> <!--col-->         
          <?php endforeach;?>
        </div> <!--food-gallery-wrap-->   
      </div> <!--section-label-->
     <?php endif;?>
  <?php endif;?>
  <!--FOOD ITEM GALLERY-->
    
  <!--PRICE-->
  <div class="section-label">
  
    <div class="row">
		<div class="col-md-4">
			<b><?php echo t("Retail Price")?></b><br/>
			<?=FunctionsV3::prettyPriceUnit($data['retail_price'],$data['uom'])?>
		</div>
		<div class="col-md-6">
			<b><?php echo t("Wholesale Price")?></b><br/>

			<table class="table table-striped margin-top-10 additional-product"  style="border: 1px solid #ddd;">
				<tbody>
					<?php 
					$wholesale_price = Yii::app()->functions->getWhere('wholesale_price','item_id',$this->data['item_id']);
					if(!empty($wholesale_price)) {
						foreach($wholesale_price as $wp) {
							?>
							<tr>
								<td style="text-align:center;"><?=$wp['start'].' - '.$wp['end'].' '.$data['uom']?> </td>
								<td style="text-align:center;"><?=FunctionsV3::prettyPriceUnit($wp['price'],$data['uom'])?></td>
							</tr>
							<?php
						}
					} else {
						echo t('Not available');
					}
					?>
				</tbody>
			</table>
			
			
		</div>
    </div> <!--row-->
  </div>        
  <!--PRICE-->
  <!--PRICE-->
  <div class="section-label hidden">
    <a class="section-label-a">
      <span class="bold">
      <?php echo t("Wholesale Price")?></span>
      <b></b>
    </a>     
    <div class="row">
		<div class="col-md-5">

		</div>
    </div> <!--row-->
  </div>        
  <!--PRICE-->

  <!--PRICE-->
  <div class="section-label">
    <a class="section-label-a">
      <span class="bold">
      <?php echo t("Product Detail")?></span>
      <b></b>
    </a>     
    <div class="row">
		<div class="col-md-12">
			<style>
			.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
				border-top: 1px solid #a1a5a0;
				font-size:13px;
			}
			.table-detail-product th{
				color: #c53737;				
				font-weight: 600 !important;
			}
			</style>
<table class="table table-detail-product" style="border: 1px solid #a1a5a0;background-color:#eaeae9"> 
<thead> 
</thead> 
	<tbody> 
	<tr> 
		<th scope="row"><?=t('Sold')?></th> 
		<td><?=(isset($data['sold'])?$data['sold']:0).' '.$data['uom']?></td> 
		<th scope="row"><?=t('Views')?></th> 
		<td><?=(isset($data['views'])?$data['views']:0). ' '. t("times") ?></td> 
	</tr> 
	<tr> 
		<th scope="row"><?=t('Stock')?></th> 
		<td><?=(isset($data['stock'])?$data['stock']:0).' '.$data['uom']?></td> 
		<th scope="row"><?=t('Condition')?></th> 
		<td><?=t($data['condition'])?></td> 
	</tr> 
	<tr> 
		<th scope="row"><?=t('Weight Per Unit')?></th> 
		<td><?=($data['weight']>=1000)?($data['weight']/1000).' kg':$data['weight'].' gram'?></td> 
		<th scope="row"><?=t('Unit')?></th> 
		<td><?=$data['uom']?></td> 
	</tr> 
	<tr> 
		<th scope="row"><?=t('Tax')?></th> 
		<td><?=$data['tax']?> %</td> 
		<th scope="row"><?=t('Category')?></th> 
		<td><?=$data['category_name']?></td> 
	</tr> 
	</tbody> 
</table>

		</div>
    </div> <!--row-->
  </div>        
  <!--PRICE-->

  <!--QUANTITY-->
  <div class="section-label margin-bottom-20">
    <a class="section-label-a">
      <span class="bold">
      <?php echo t("Quantity")?></span>
      <b></b>
    </a>     
    <div class="row">
       <div class="col-md-12 col-xs-12 border into-row">
		  <a href="javascript:;" class="green-button inline qty-minus" ><i class="ion-minus"></i></a>
          <?php echo CHtml::textField('qty',
	      isset($item_data['qty'])?$item_data['qty']:1
	      ,array(
	      'class'=>"uk-form-width-mini numeric_only qty", 
	      'maxlength'=>5,     
		  'style'=>"width: 50px;padding: 6px;float: left;"
	      ))?>
		  <a href="javascript:;" class="qty-plus green-button inline"><i class="ion-plus"></i></a>&nbsp;&nbsp;

			<?php if ($disabled_website_ordering==""):?>
				 <input type="submit" value="<?php echo empty($row)?Yii::t("default","add to cart"):Yii::t("default","Update");?>" 
				 class="add_to_cart btn btn-success ">&nbsp;&nbsp;
				 <a href="javascript:close_fb();" class="center  close-btn btn btn-success inline"><?php echo t("Close")?></a>
				   </div>
			<?php endif;?>
    </div> <!--row-->
  </div> <!-- section-label--> 
  
  <div class="notes-wrap">
  <?php echo CHtml::textArea('notes',
  isset($item_data['notes'])?$item_data['notes']:""
  ,array(
   'class'=>'uk-width-1-1',
   'placeholder'=>Yii::t("default","Special Instructions")
  ))?>
  </div> <!--notes-wrap-->
  <!--QUANTITY-->
    

</div> <!--view-item-wrap-->
</form>
<?php else :?>
<p class="text-danger"><?php echo Yii::t("default","Sorry but we cannot find what you are looking for.")?></p>
<?php endif;?>
<script type="text/javascript">
jQuery(document).ready(function() {	
	var hide_foodprice=$("#hide_foodprice").val();	
	if ( hide_foodprice=="yes"){
		$(".hide-food-price").hide();
		$("span.price").hide();		
		$(".view-item-wrap").find(':input').each(function() {			
			$(this).hide();
		});
	}
	

	var price_cls=$(".price_cls:checked").length; 	
	if ( price_cls<=0){
		var x=0
		$( ".price_cls" ).each(function( index ) {
			if ( x==0){
				dump('set check');
				$(this).attr("checked",true);
			}
			x++;
		});
	}
		

if ( $(".food-gallery-wrap").exists()){
	  $('.food-gallery-wrap').magnificPopup({
      delegate: 'a',
      type: 'image',
      closeOnContentClick: false,
      closeBtnInside: false,
      mainClass: 'mfp-with-zoom mfp-img-mobile',
      image: {
        verticalFit: true,
        titleSrc: function(item) {
          return '';
        }
      },
      gallery: {
        enabled: true
      },
      zoom: {
        enabled: true,
        duration: 300, // don't foget to change the duration also in CSS
        opener: function(element) {
          return element.find('img');
        }
      }      
    });
}
   	
});	 /*END READY*/
</script>