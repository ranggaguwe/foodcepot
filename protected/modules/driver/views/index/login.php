

<div class="container">
  <div class="login-wrap rounded">
     
    <form id="frm" class="frm rounded3" method="POST" onsubmit="return false;">
    <?php echo CHtml::hiddenField('action','login')?>
    <?php echo CHtml::hiddenField('user_type','1')?>

    <div>
    <?php 
    echo CHtml::textField('username','',array(
      'placeholder'=>Driver::t("Username"),
      'class'=>"lightblue-fields rounded",
      'required'=>true
    ));
    ?>
    </div>
    
    <div class="top20">
    <?php 
    echo CHtml::passwordField('password','',array(
      'placeholder'=>Driver::t("Password"),
      'class'=>"lightblue-fields rounded",
      'required'=>true
    )); 
    ?>
    </div>
    
    
    <div class="top20">
    <button class="green-button medium rounded"><?php echo Driver::t("Login")?></button>
    </div>
    </form>
  
  </div> <!--login-wrap-->
</div> <!--container-->