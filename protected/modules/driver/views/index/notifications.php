
<div id="layout_1">
<?php 
$this->renderPartial('/tpl/layout1_top',array(   
));
?> 
</div> <!--layout_1-->

<div class="parent-wrapper">

 <div class="content_1 ">   
   <?php 
   $this->renderPartial('/tpl/menu',array(   
   ));
   ?>
 </div> <!--content_1-->
 
 <div class="content_main">

   <div class="nav_option">
      <div class="row">
        <div class="col-md-6 ">
         <b><?php echo t("Notifications")?></b>
        </div> <!--col-->
        <div class="col-md-6  text-right">
            
         <!--  <a class="green-button left rounded" href="javascript:;"><?php echo t("Add Task")?></a>
           <a class="orange-button left rounded" href="javascript:;"><?php echo t("Refresh")?></a>-->
         
        </div> <!--col-->
      </div> <!--row-->
   </div> <!--nav_option-->
  
   <div class="inner">
   
   
 
  <form id="frm" class="frm form-horizontal">
	 <?php echo CHtml::hiddenField('action','SaveNotification')?>

      
   
   <?php 
   $list=Driver::notificationListDelivery();  
   ?> 
     
   <h4><?php echo t("Delivery Notifications")?></h4>
   
   <table class="table table-striped">
   <thead>
    <tr>
     <th><?php echo t("Triggers")?></th>
     <th><?php echo t("Mobile Push")?></th>
     <th><?php echo t("Email")?></th>
     <th><?php echo t("Actions")?></th>
    </tr>
   </thead>
   <tbody>
   
   <?php foreach ($list['DELIVERY'] as $key=>$val):?>   
    <tr>
     <td><?php echo $key;?></td>
     <td><?php 
     //echo "DELIVERY_".$val[0]."<br/>";
     echo CHtml::checkBox("DELIVERY_".$val[0], 
     getOptionA("DELIVERY_".$val[0])==1?true:false
      ,array('class'=>""))?></td>
      
     <td style="display:none"><?php echo CHtml::checkBox("DELIVERY_".$val[1],
     getOptionA("DELIVERY_".$val[1])==1?true:false
     ,array('class'=>" hidden"))?></td>
     
     <td><?php echo CHtml::checkBox("DELIVERY_".$val[2],
     getOptionA("DELIVERY_".$val[2])==1?true:false
     ,array('class'=>""))?></td>
     
     <td><a href="javascript:;" class="notification_tpl" data-id="<?php echo "DELIVERY_".$key?>">
       <i class="ion-edit"></i>
     </a></td>
    </tr>
    <?php endforeach;?>
       
   
   </tbody>
   </table>
   
   <div class="top20">&nbsp;</div>
   
   <h4><?php echo t("Driver Notifications")?></h4>
   
    <table class="table table-striped">
   <thead>
    <tr>
     <th><?php echo t("Triggers")?></th>
     <th><?php echo t("Mobile Push")?></th>
     <th><?php echo t("Email")?></th>
     <th><?php echo t("Actions")?></th>
    </tr>
   </thead>
   <tbody>
     <tr>
     <td><?php echo t("ASSIGN_TASK")?></td>
     <td>
     <?php echo CHtml::checkBox('ASSIGN_TASK_PUSH', 
     getOptionA("ASSIGN_TASK_PUSH")==1?true:false
      ,array('class'=>""))?>
     </td>

     <td>
     <?php echo CHtml::checkBox('ASSIGN_TASK_EMAIL', 
     getOptionA("ASSIGN_TASK_EMAIL")==1?true:false
      ,array('class'=>""))?>
     </td>
     
     <td><a href="javascript:;" class="notification_tpl" data-id="ASSIGN_TASK"><i class="ion-edit"></i></a></td>
     
     </tr>
     
     <tr>
     <td><?php echo t("UPDATE_TASK")?></td>
     <td>
     <?php echo CHtml::checkBox('UPDATE_TASK_PUSH', 
     getOptionA("UPDATE_TASK_PUSH")==1?true:false
      ,array('class'=>""))?>
     </td>

     <td>
     <?php echo CHtml::checkBox('UPDATE_TASK_EMAIL', 
     getOptionA("UPDATE_TASK_EMAIL")==1?true:false
      ,array('class'=>"",'disabled'=>true))?>
     </td>
     
     <td><a href="javascript:;" class="notification_tpl" data-id="UPDATE_TASK"><i class="ion-edit"></i></a></td>
     
     </tr>


     
   </tbody>
   </table>
   
    <div class="top20">&nbsp;</div>
   
   <h4><?php echo t("Auto assign Notifications")?></h4>
   
     <table class="table table-striped">
   <thead>
    <tr>
     <th><?php echo t("Triggers")?></th>
     <th><?php echo t("Mobile Push")?></th>
     <th><?php echo t("Email")?></th>
     <th><?php echo t("Actions")?></th>
    </tr>
   </thead>
   <tbody>
     <tr>
     <td><?php echo t("FAILED_AUTO_ASSIGN")?></td>
     <td>
     <?php echo CHtml::checkBox('FAILED_AUTO_ASSIGN_PUSH', 
     getOptionA("FAILED_AUTO_ASSIGN_PUSH")==1?true:false
      ,array('class'=>"",'disabled'=>true))?>
     </td>

     <td>
     <?php echo CHtml::checkBox('FAILED_AUTO_ASSIGN_EMAIL', 
     getOptionA("FAILED_AUTO_ASSIGN_EMAIL")==1?true:false
      ,array('class'=>""))?>
     </td>
     
     <td>
     <a href="javascript:;" class="notification_tpl" data-id="FAILED_AUTO_ASSIGN"><i class="ion-edit"></i></a>
     </td>
     
     </tr>
     

   </table>  
   
   <div class="top20">&nbsp;</div>
   
     <div class="form-group">
	    <label class="col-sm-2 control-label"></label>
	    <div class="col-sm-6">
		  <button type="submit" class="orange-button medium rounded">
		  <?php echo Driver::t("Save")?>
		  </button>
	    </div>	 
	  </div>
	  
   </form>
    
   </div> <!--inner-->
 
 </div> <!--content_2-->

</div> <!--parent-wrapper-->