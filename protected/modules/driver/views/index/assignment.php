
<div id="layout_1">
<?php 
$this->renderPartial('/tpl/layout1_top',array(   
));
?> 
</div> <!--layout_1-->

<div class="parent-wrapper">

 <div class="content_1 ">   
   <?php 
   $this->renderPartial('/tpl/menu',array(   
   ));
   ?>
 </div> <!--content_1-->
 
 <div class="content_main">

   <div class="nav_option">
      <div class="row">
        <div class="col-md-6 ">
         <b><?php echo t("Assignment")?></b>
        </div> <!--col-->
        <div class="col-md-6  text-right">
                     
        </div> <!--col-->
      </div> <!--row-->
   </div> <!--nav_option-->
  
   <div class="inner">
      
   <form id="frm" class="frm">
   <?php echo CHtml::hiddenField('action',"saveAssigmentSettings")?>
      
   <p class="text-muted"><?php echo t("Automatically assign the tasks to your driver")?>.</p>
   
   <div class="text-muted top20" style="width:50%;">    
     <div class="panel-body">
	<div class="row">
         <div class="col-md-1">
      <?php echo CHtml::checkBox('driver_enabled_auto_assign',
      Driver::getOption('driver_enabled_auto_assign')==1?true:false
      ,array(
        'value'=>1,
        'class'=>""
      ))?>
		</div>
      <div class="col-md-5">
      <?php echo Driver::t("Enable Auto Assignment")?>
	  </div>
	  </div>
        <div class="row margin-top-10">
         <div class="col-md-1"><?php echo CHtml::checkBox('driver_include_offline_driver',
         Driver::getOption('driver_include_offline_driver')==1?true:false
         ,array(
           'value'=>1,
           'class'=>""
         ))?></div>
         <div class="col-md-5"><?php echo Driver::t("Include Offline Driver")?></div>
       </div>
       
       <div class="row top10">
         <div class="col-md-3"><?php echo Driver::t("Notify Email")?></div>
         <div class="col-md-6">
           <?php echo CHtml::textField('driver_autoassign_notify_email',
           Driver::getOption("driver_autoassign_notify_email"),array(
             'class'=>"form-control"
           ))?>
           <p class="text-muted small-font"><?php echo Driver::t("Email address that will receive email if unable to auto assign")?>.</p>
         </div>
       </div>
       

       
     
     </div> <!--body-->
   </div> <!--panel-->
    
   
     <div class="form-group">
	    <button type="submit" class="orange-button medium rounded">
		  <?php echo Driver::t("Save")?>
		  </button>
	  </div>
   
   </form> 
   
   </div> <!--inner-->
 
 </div> <!--content_2-->

</div> <!--parent-wrapper-->