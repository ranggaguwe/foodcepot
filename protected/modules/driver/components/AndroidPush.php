<?php
class AndroidPush
{
	
    public static function sendPushOLD($api_key='',$device_id='',$message='')
    {    	
    	if (empty($api_key)){
    		return array(
    		  'success'=>0,
    		  'results'=>array(
    		     array(
    		       'error'=>'missing api key'
    		     )
    		  )
    		);
    	}
    	if (empty($device_id)){
    		return array(
    		  'success'=>0,
    		  'results'=>array(
    		     array(
    		       'error'=>'missing device id'
    		     )
    		  )
    		);
    	}
    	    	
    	$url = 'https://android.googleapis.com/gcm/send';
		$fields = array(
           'registration_ids' => array($device_id),
           'data' => $message,
        );
        //dump($fields);
        
        $headers = array(
		  'Authorization: key=' . $api_key,
		  'Content-Type: application/json'
        );
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));		
		$result = curl_exec($ch);
		if ($result === FALSE) {		    
		   return array(
    		  'success'=>0,
    		  'results'=>array(
    		     array(
    		       'error'=>'Curl failed: '. curl_error($ch)
    		     )
    		  )
    		);
		}
		
        curl_close($ch);
        //echo $result; 
        $result=!empty($result)?json_decode($result,true):false;
        //dump($result);
        if ($result==false){
        	return array(
    		  'success'=>0,
    		  'results'=>array(
    		     array(
    		       'error'=>'invalid response from push service'
    		     )
    		  )
    		);
        }
        return $result;   
    }
    public static function sendPush($platform='Android',$api_key='',$device_id='',$message='')
    {    	
    	if (empty($api_key)){
    		return array(
    		  'success'=>0,
    		  'results'=>array(
    		     array(
    		       'error'=>'missing api key'
    		     )
    		  )
    		);
    	}
    	if (empty($device_id)){
    		return array(
    		  'success'=>0,
    		  'results'=>array(
    		     array(
    		       'error'=>'missing device id'
    		     )
    		  )
    		);
    	}
    	    	
    	// $url = 'https://fcm.googleapis.com/fcm/send';//'https://android.googleapis.com/gcm/send';
		// $fields = array(
           // 'registration_ids' => array($device_id),
           // 'data' => $message,
        // );
        // //dump($fields);
        
        // $headers = array(
		  // 'Authorization: key=' . $api_key,
		  // 'Content-Type: application/json'
        // );
        // //dump($headers);
        
        // $ch = curl_init();
        // curl_setopt($ch, CURLOPT_URL, $url);
        // curl_setopt($ch, CURLOPT_POST, true);
        // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        // curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));		
		// $result = curl_exec($ch);
		// if ($result === FALSE) {
		    // //die('Curl failed: ' . curl_error($ch));
		   // return array(
    		  // 'success'=>0,
    		  // 'results'=>array(
    		     // array(
    		       // 'error'=>'Curl failed: '. curl_error($ch)
    		     // )
    		  // )
    		// );
		// }
		
        // curl_close($ch);
        // //echo $result; 
        // $result=!empty($result)?json_decode($result,true):false;
        // //dump($result);
        // if ($result==false){
        	// return array(
    		  // 'success'=>0,
    		  // 'results'=>array(
    		     // array(
    		       // 'error'=>'invalid response from push service'
    		     // )
    		  // )
    		// );
        // }
		
		
		
		//--------------FIREBASEE------------
		
define( 'API_ACCESS_KEY', $api_key );
// $registrationIds = array( $device_id );
// // prep the bundle
// $msg = array
// (
	// 'body' 	=> $message,
	// 'title'		=> $message,
	// 'vibrate'	=> 1,
	// 'sound'		=> 1,
// );
// $fields = array
// (
	// 'registration_ids' 	=> $registrationIds,
	// 'notification'			=> $msg
// );
 
// $headers = array
// (
	// 'Authorization: key=' . API_ACCESS_KEY,
	// 'Content-Type: application/json'
// );
 
// $ch = curl_init();
// curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
// curl_setopt( $ch,CURLOPT_POST, true );
// curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
// curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
// curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
// curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
// $result = curl_exec($ch );
// curl_close( $ch );
// echo "ok";
// echo $result;
// echo "ok";

		
		
		//-------------END FIREBASE-----------
		
$url = 'https://fcm.googleapis.com/fcm/send';
    $priority="high";
    $notification= array('title' => 'Some title','body' => 'hi' );
	echo "deviceId";
	dump($device_id);
    $fields = array(
         'to' => $device_id,
         'notification' => $notification
        );


    $headers = array(
        'Authorization:key=' . API_ACCESS_KEY,
        'Content-Type: application/json'
        );

   $ch = curl_init();
   curl_setopt($ch, CURLOPT_URL, $url);
   curl_setopt($ch, CURLOPT_POST, true);
   curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
   curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
   curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);  
   curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
   curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
    // echo json_encode($fields);
   $result = curl_exec($ch);           
   echo curl_error($ch);
   if ($result === FALSE) {
       die('Curl failed: ' . curl_error($ch));
   }
   echo "result11";
   dump($result);
   curl_close($ch);
   return $result;
   
   //---END FIREBASE AGAIN----
		
		
        return $result;   
    }

} /*end class*/