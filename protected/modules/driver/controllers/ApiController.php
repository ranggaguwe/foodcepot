<?php
class ApiController extends CController
{	
	public $data;
	public $code=2;
	public $msg='';
	public $details='';
	
	public function __construct()
	{
		// $this->data=$_GET;
		$this->data=$_POST;
		if (isset($_GET['post_type'])){
			if ($_GET['post_type']=="get"){
				$this->data=$_GET;	
			}
		}
		//convert mtid as driver id
		if(isset($this->data['mtid'])) {
			$this->data['driver_id'] = $this->data['mtid'];
		}
		
		$website_timezone=Yii::app()->functions->getOptionAdmin("website_timezone");		 
	    if (!empty($website_timezone)){
	 	   Yii::app()->timeZone=$website_timezone;
	    }		 
	}
	
	public function beforeAction($action)
	{				
		/*check if there is api has key*/		
		$action=Yii::app()->controller->action->id;				
		if(isset($this->data['api_key'])){
			if(!empty($this->data['api_key'])){			   
			   $continue=true;
			   if($action=="getLanguageSettings" || $action=="GetAppSettings"){
			   	  $continue=false;
			   }
			   if($continue){
			   	   $key=getOptionA('driver_api_hash_key');			   	   
				   if(trim($key)!=trim($this->data['api_key'])){
				   	 $this->msg=$this->t("api hash key is not valid");
			         $this->output();
			         Yii::app()->end();
				   }
			   }			
			}
		}		
		return true;
	}	
	
	public function actionIndex(){
		echo 'Api is working';
	}		
	
	private function q($data='')
	{
		return Yii::app()->db->quoteValue($data);
	}
	
	private function t($message='')
	{
		return Yii::t("default",$message);
	}
		
    private function output()
    {	   
		if(empty($this->details)) {
		   $resp=array(
			 'code'=>$this->code,
			 'msg'=>$this->msg,
			 'request'=>$this->data
		   );
		} else {
		   $resp=array(
			 'code'=>$this->code,
			 'msg'=>$this->msg,
			 'details'=>$this->details,
			 'request'=>$this->data
		   );
		}
	   
	   
	   
	   if (isset($this->data['debug'])){
	   	   dump($resp);
	   }
	   
	   if (!isset($_GET['callback'])){
  	   	   $_GET['callback']='';
	   }    
	   
		echo json_encode($resp,JSON_UNESCAPED_SLASHES);
	   // if (isset($_GET['json']) && $_GET['json']==TRUE){
	   	   // echo CJSON::encode($resp);
	   // } else echo $_GET['callback'] . '('.CJSON::encode($resp).')';		    	   	   	  
	   Yii::app()->end();
    }		
	
	public function actionSignUp()
	{
		$DbExt=new DbExt;
		$params=array(
		  'first_name'=>isset($this->data['contact_name'])?$this->data['contact_name']:'',
		  // 'last_name'=>isset($this->data['last_name'])?$this->data['last_name']:'',
		  'email'=>isset($this->data['contact_email'])?$this->data['contact_email']:'',
		  'phone'=>isset($this->data['contact_phone'])?$this->data['contact_phone']:'',
		  'username'=>isset($this->data['username'])?$this->data['username']:'',
		  'password'=>isset($this->data['password'])?md5($this->data['password']):'',
		  'transport_type_id'=>isset($this->data['jenis_kendaraan'])?$this->data['jenis_kendaraan']:'',
		  'transport_description'=>isset($this->data['transport_description'])?$this->data['transport_description']:'',
		  'licence_plate'=>isset($this->data['licence_plate'])?$this->data['licence_plate']:'',
		  'color'=>isset($this->data['color'])?$this->data['color']:'',
		  'status'=>'pending',
		  'date_created'=>date('c'),
		  'ip_address'=>$_SERVER['REMOTE_ADDR']
		);
		
		$params['user_type']=Driver::getUserType();
		$params['user_id']=Driver::getUserId();
		
		if(!isset($this->data['id'])){
			$this->data['id']='';
		}
		
		if(is_numeric($this->data['id'])){
			unset($params['date_created']);
			$params['date_modified']=date('c');
			
			if(empty($this->data['password'])){
			   unset($params['password']);
			}
			
			/*dump($params);
			die();
			*/
			if ( $DbExt->updateData("{{driver}}",$params,'driver_id',$this->data['id'])){
				$this->code=1;
			    $this->msg=Driver::t("Successfully updated");
			    $this->details='new-agent';
			    
			    
			} else $this->msg=Driver::t("failed cannot update record");
		} else {
			if ( $DbExt->insertData('{{driver}}',$params)){
				$this->code=1;
				$this->msg=Driver::t("Successful");
				$this->details='new-agent';
			} else $this->msg=Driver::t("failed cannot insert record");
		}
		$this->output();
	}
	
    public function actionLogin()
    {
    	if(!empty($this->data['username']) && !empty($this->data['password'])){
	    	if ( $res=Driver::driverAppLogin($this->data['username'],$this->data['password'])){	
	    		$token=md5(Driver::generateRandomNumber(5) . $this->data['username']);
	    		$params=array(
	    		  'last_login'=>date('c'),
	    		  'last_online'=>strtotime("now"),
	    		  'ip_address'=>$_SERVER['REMOTE_ADDR'],
	    		  'token'=>$token,
	    		  'device_id'=>isset($this->data['device_id'])?$this->data['device_id']:'',
	    		  'device_platform'=>isset($this->data['device_platform'])?$this->data['device_platform']:'Android'
	    		);	    		
	    		if(!empty($res['token'])){
	    			unset($params['token']);
	    			$token=$res['token'];
	    		}
	    		$db=new DbExt;
	    		if ( $db->updateData("{{driver}}",$params,'driver_id',$res['driver_id'])){	    			
	    			$this->code=1;
	    			$this->msg=self::t("Login Successful");
	    			
	    			
	    			$this->details=array(
					  'driver_id'=>$res['driver_id'],
					  'merchant_id'=>$res['driver_id'],
	    			  'username'=>$this->data['username'],
	    			  'password'=>$this->data['password'],
	    			  'remember'=>isset($this->data['remember'])?$this->data['remember']:'',
	    			  'todays_date'=>Yii::app()->functions->translateDate(date("M, d")),
	    			  'todays_date_raw'=>date("Y-m-d"),
	    			  'on_duty'=>$res['on_duty'],
	    			  'token'=>$token,
	    			  'duty_status'=>$res['on_duty'],
	    			);
	    		} else $this->msg=self::t("Login failed. please try again later");
	    	} else $this->msg=self::t("Login failed. either username or password is incorrect");
    	} else $this->msg=self::t("Please fill in your username and password");
    	$this->output();
    }

    public function actionForgotPassword()
    {
    	if (empty($this->data['email_address'])){
    		$this->msg=self::t("Email address is required");
    		$this->output();
    		Yii::app()->end();
    	}
    	$db=new DbExt;    	
    	if ( $res=Driver::driverForgotPassword($this->data['email_address'])){
    		$driver_id=$res['driver_id'];    		
    		$code=Driver::generateRandomNumber(5);
    		$params=array('forgot_pass_code'=>$code);
    		if($db->updateData('{{driver}}',$params,'driver_id',$driver_id)){
    			$this->code=1;
    			$this->msg=self::t("We have send the a password change code to your email");
    			
    			$tpl=EmailTemplate::forgotPasswordRequest();
    			$tpl=smarty('first_name',$res['first_name'],$tpl);
    			$tpl=smarty('code',$code,$tpl);
				// echo $code;
    			$subject='Forgot Password';
    			if ( sendEmail($res['email'],'',$subject,$tpl)){
    				$this->details="send email ok";
    			} else $this->msg="send email failed";
    			
    		} else $this->msg=self::t("Something went wrong please try again later");
    	} else $this->msg=self::t("Email address not found");
    	$this->output();
    }

    public function actionChangePasswordWithCode()
    {    	
    	$Validator=new Validator;
    	$req=array(
    	  'email_address'=>self::t("Email address is required"),
    	  'code'=>self::t("Code is required"),
    	  'newpass'=>self::t("New Password is required")
    	);
    	$Validator->required($req,$this->data);
    	if ( $Validator->validate()){
    		if ( $res=Driver::driverForgotPassword($this->data['email_address'])){    			
    			if ( $res['forgot_pass_code']==$this->data['code']){
    				$params=array( 
    				  'password'=>md5($this->data['newpass']),
    				  'date_modified'=>date('c'),
    				  'forgot_pass_code'=>Driver::generateRandomNumber(5)
    				 );
    				$db=new DbExt;    				
    				if ( $db->updateData("{{driver}}",$params,'driver_id',$res['driver_id'])){
    				    $this->code=1;
    				    $this->msg=self::t("Password successfully changed");
    				} else $this->msg=self::t("Something went wrong please try again later");    				
    			} else $this->msg=self::t("Invalid password code");
    		} else $this->msg=self::t("Email address not found");
    	} else $this->msg=Driver::parseValidatorError($Validator->getError());		
    	$this->output();
    }

    public function actionGetTodaysOrder()
    {    	
    	
    	$Validator=new Validator;
		$req=array(
		  'token'=>$this->t("token is required"),
		  'driver_id'=>$this->t("driver id is required"),
		  'user_type'=>$this->t("user type is required"),
		);
		$Validator->required($req,$this->data);
		if ($Validator->validate()){

			if ( $res=query("SELECT * FROM {{driver}} WHERE driver_id=? AND token=? AND user_type=?",array($this->data['driver_id'],$this->data['token'],$this->data['user_type']))){
			    $res=$res[0];

				
				$DbExt=new DbExt;	
				$stmt="
				SELECT 	a.*,b.viewed,b.task_id,d.street,
						d.city,d.state,d.zipcode,d.location_name,d.country,d.contact_phone,d.latitude,d.longitude,d.origin_merchant,
						concat(first_name,' ',last_name) as customer_name
				FROM {{order}} a
				JOIN {{driver_task}} b
				ON a.order_id=b.order_id 
				JOIN {{client}} c
				ON c.client_id=a.client_id
				JOIN {{order_delivery_address}} d
				ON a.order_id=d.order_id
				WHERE 
				b.driver_id=".$this->q($res['driver_id'])."				
				AND 
				b.date_created LIKE '".date("Y-m-d")."%'						
				AND 
				a.status NOT IN ('initial_order')
				ORDER BY date_created DESC
				LIMIT 0,100
				";
				
				$field_destination = array('client_id','customer_name','contact_phone','street','city','state','zipcode','location_name','country','latitude','longitude');
				
				if ( $res=$DbExt->rst($stmt)){					
					$this->code=1; $this->msg="OK";					
					foreach ($res as $val) {
						
						$dest_customer = array();
						foreach($field_destination as $f) {
							$dest_customer[$f] = $val[$f];
						}
						
						$data[]=array(						 
						  'order_id'=>$val['order_id'],
						  'task_id'=>$val['task_id'],
						  'viewed'=>$val['viewed'],
						  'status'=>t($val['status']),
						  'status_raw'=>strtolower($val['status']),
						  'trans_type'=>t($val['trans_type']),
						  'trans_type_raw'=>$val['trans_type'],
						  'total_w_tax'=>$val['total_w_tax'],						  
						  'total_w_tax_currency'=>merchantApp::prettyPrice($val['total_w_tax']),
						  'transaction_date'=>Yii::app()->functions->FormatDateTime($val['date_created'],true),
						  'transaction_time'=>Yii::app()->functions->timeFormat($val['date_created'],true),
						  'delivery_time'=>Yii::app()->functions->timeFormat($val['delivery_time'],true),
						  'delivery_asap'=>$val['delivery_asap']==1?t("ASAP"):'',
						  'delivery_date'=>Yii::app()->functions->FormatDateTime($val['delivery_date'],false),
						  'customer_name'=>!empty($val['customer_name'])?$val['customer_name']:$this->t('No name'),
						  'dest_customer' => $dest_customer,
						  'origin_merchant'=>json_decode($val['origin_merchant']),
						);
					}
					$this->code=1;
					$this->msg="OK";
					$this->details=$data;
				} else $this->msg=$this->t("no current orders");
			} else {
				$this->code=3;
				$this->msg=$this->t("you session has expired or someone login with your account");
			}
		} else $this->msg=merchantApp::parseValidatorError($Validator->getError());	    	
		$this->output();    	    
    }
    public function actionGetPendingOrders()
    {    	
    	
    	$Validator=new Validator;
		$req=array(
		
		  'token'=>$this->t("token is required"),
		  'driver_id'=>$this->t("driver id is required"),
		  'user_type'=>$this->t("user type is required"),
		);
		$Validator->required($req,$this->data);
		if ($Validator->validate()){

			if ( $res=query("SELECT * FROM {{driver}} WHERE driver_id=? AND token=? AND user_type=?",array($this->data['driver_id'],$this->data['token'],$this->data['user_type']))){
			    $res=$res[0];

				
				$DbExt=new DbExt;	
				$stmt="
				SELECT 	a.*,b.viewed,b.task_id,d.street,
						d.city,d.state,d.zipcode,d.location_name,d.country,d.contact_phone,d.latitude,d.longitude,d.origin_merchant,
						concat(first_name,' ',last_name) as customer_name
				FROM {{order}} a
				JOIN {{driver_task}} b
				ON a.order_id=b.order_id 
				JOIN {{client}} c
				ON c.client_id=a.client_id
				JOIN {{order_delivery_address}} d
				ON a.order_id=d.order_id
				WHERE 
				b.driver_id=".$this->q($res['driver_id'])."				
				AND 
				a.status IN ('pending','accepted','paid')
				ORDER BY date_created DESC
				LIMIT 0,100
				";
				
				$field_destination = array('client_id','customer_name','contact_phone','street','city','state','zipcode','location_name','country','latitude','longitude');
				
				if ( $res=$DbExt->rst($stmt)){					
					$this->code=1; $this->msg="OK";					
					foreach ($res as $val) {
						
						$dest_customer = array();
						foreach($field_destination as $f) {
							$dest_customer[$f] = $val[$f];
						}
						
						$data[]=array(						 
						  'order_id'=>$val['order_id'],
						  'task_id'=>$val['task_id'],
						  'viewed'=>$val['viewed'],
						  'status'=>t($val['status']),
						  'status_raw'=>strtolower($val['status']),
						  'trans_type'=>t($val['trans_type']),
						  'trans_type_raw'=>$val['trans_type'],
						  'total_w_tax'=>$val['total_w_tax'],						  
						  'total_w_tax_currency'=>merchantApp::prettyPrice($val['total_w_tax']),
						  'transaction_date'=>Yii::app()->functions->FormatDateTime($val['date_created'],true),
						  'transaction_time'=>Yii::app()->functions->timeFormat($val['date_created'],true),
						  'delivery_time'=>Yii::app()->functions->timeFormat($val['delivery_time'],true),
						  'delivery_asap'=>$val['delivery_asap']==1?t("ASAP"):'',
						  'delivery_date'=>Yii::app()->functions->FormatDateTime($val['delivery_date'],false),
						  'customer_name'=>!empty($val['customer_name'])?$val['customer_name']:$this->t('No name'),
						  'dest_customer' => $dest_customer,
						  'origin_merchant'=>json_decode($val['origin_merchant']),
						);
					}
					$this->code=1;
					$this->msg="OK";
					$this->details=$data;
				} else $this->msg=$this->t("no current orders");
			} else {
				$this->code=3;
				$this->msg=$this->t("you session has expired or someone login with your account");
			}
		} else $this->msg=merchantApp::parseValidatorError($Validator->getError());	    	
		$this->output();    	    
    }

    public function actionGetAllOrders()
    {
    	
    	$Validator=new Validator;
		$req=array(
		
		  'token'=>$this->t("token is required"),
		  'driver_id'=>$this->t("driver id is required"),
		  'user_type'=>$this->t("user type is required"),
		);
		$Validator->required($req,$this->data);
		if ($Validator->validate()){

			if ( $res=query("SELECT * FROM {{driver}} WHERE token=? AND user_type=?",array($this->data['token'],$this->data['user_type']))){
			    $res=$res[0];

				
				$DbExt=new DbExt;	
				$stmt="
				SELECT 	a.*,b.viewed,b.task_id,d.street,
						d.city,d.state,d.zipcode,d.location_name,d.country,d.contact_phone,d.latitude,d.longitude,d.origin_merchant,
						concat(first_name,' ',last_name) as customer_name
				FROM {{order}} a
				JOIN {{driver_task}} b
				ON a.order_id=b.order_id 
				JOIN {{client}} c
				ON c.client_id=a.client_id
				JOIN {{order_delivery_address}} d
				ON a.order_id=d.order_id
				WHERE 
				b.driver_id=".$this->q($res['driver_id'])."	
				ORDER BY date_created DESC
				LIMIT 0,100
				";
				
				$field_destination = array('client_id','customer_name','contact_phone','street','city','state','zipcode','location_name','country','latitude','longitude');
				
				if ( $res=$DbExt->rst($stmt)){					
					$this->code=1; $this->msg="OK";					
					foreach ($res as $val) {
						
						$dest_customer = array();
						foreach($field_destination as $f) {
							$dest_customer[$f] = $val[$f];
						}
						
						$data[]=array(						 
						  'order_id'=>$val['order_id'],
						  'task_id'=>$val['task_id'],
						  'viewed'=>$val['viewed'],
						  'status'=>t($val['status']),
						  'status_raw'=>strtolower($val['status']),
						  'trans_type'=>t($val['trans_type']),
						  'trans_type_raw'=>$val['trans_type'],
						  'total_w_tax'=>$val['total_w_tax'],						  
						  'total_w_tax_currency'=>merchantApp::prettyPrice($val['total_w_tax']),
						  'transaction_date'=>Yii::app()->functions->FormatDateTime($val['date_created'],true),
						  'transaction_time'=>Yii::app()->functions->timeFormat($val['date_created'],true),
						  'delivery_time'=>Yii::app()->functions->timeFormat($val['delivery_time'],true),
						  'delivery_asap'=>$val['delivery_asap']==1?t("ASAP"):'',
						  'delivery_date'=>Yii::app()->functions->FormatDateTime($val['delivery_date'],false),
						  'customer_name'=>!empty($val['customer_name'])?$val['customer_name']:$this->t('No name'),
						  'dest_customer' => $dest_customer,
						  'origin_merchant'=>json_decode($val['origin_merchant']),
						);
					}
					$this->code=1;
					$this->msg="OK";
					$this->details=$data;
				} else $this->msg=$this->t("no current orders");
			} else {
				$this->code=3;
				$this->msg=$this->t("you session has expired or someone login with your account");
			}
		} else $this->msg=merchantApp::parseValidatorError($Validator->getError());	    	
		$this->output();    	    
    }
    public function actionSalesReport()
    {
    	
    	$Validator=new Validator;
		$req=array(
		
		  'token'=>$this->t("token is required"),
		  'driver_id'=>$this->t("driver id is required"),
		  'user_type'=>$this->t("user type is required"),
		);
		$Validator->required($req,$this->data);
		if ($Validator->validate()){

			if ( $res=query("SELECT * FROM {{driver}} WHERE  token=? AND user_type=?",array($this->data['token'],$this->data['user_type']))){
			    $res=$res[0];

				
				$DbExt=new DbExt;	
				$stmt="
				SELECT 	a.*,b.viewed,b.task_id,d.street,
						d.city,d.state,d.zipcode,d.location_name,d.country,d.contact_phone,d.latitude,d.longitude,d.origin_merchant,
						concat(first_name,' ',last_name) as customer_name,
						b.earning
				FROM {{order}} a
				JOIN {{driver_task}} b
				ON a.order_id=b.order_id 
				JOIN {{client}} c
				ON c.client_id=a.client_id
				JOIN {{order_delivery_address}} d
				ON a.order_id=d.order_id
				WHERE 
				b.driver_id=".$this->q($res['driver_id'])."	
				ORDER BY date_created DESC
				LIMIT 0,100
				";
				
				$field_destination = array('client_id','customer_name','contact_phone','street','city','state','zipcode','location_name','country','latitude','longitude');
				
				if ( $res=$DbExt->rst($stmt)){					
					$this->code=1; $this->msg="OK";					
					foreach ($res as $val) {
						
						$dest_customer = array();
						foreach($field_destination as $f) {
							$dest_customer[$f] = $val[$f];
						}
						
						$data[]=array(						 
						  'order_id'=>$val['order_id'],
						  'earning'=>$val['earning'],
						  'earning_currency'=>merchantApp::prettyPrice($val['earning']),
						  'distance'=>str_replace('.',',',$val['distance'])." km",
						  'task_id'=>$val['task_id'],
						  'viewed'=>$val['viewed'],
						  'status'=>t($val['status']),
						  'status_raw'=>strtolower($val['status']),
						  'trans_type'=>t($val['trans_type']),
						  'trans_type_raw'=>$val['trans_type'],
						  'total_w_tax'=>$val['total_w_tax'],						  
						  'total_w_tax_currency'=>merchantApp::prettyPrice($val['total_w_tax']),
						  'transaction_date'=>Yii::app()->functions->FormatDateTime($val['date_created'],true),
						  'transaction_time'=>Yii::app()->functions->timeFormat($val['date_created'],true),
						  'delivery_time'=>Yii::app()->functions->timeFormat($val['delivery_time'],true),
						  'delivery_asap'=>$val['delivery_asap']==1?t("ASAP"):'',
						  'delivery_date'=>Yii::app()->functions->FormatDateTime($val['delivery_date'],false),
						  'customer_name'=>!empty($val['customer_name'])?$val['customer_name']:$this->t('No name'),
						  'dest_customer' => $dest_customer,
						  'origin_merchant'=>json_decode($val['origin_merchant']),
						);
					}
					$this->code=1;
					$this->msg="OK";
					$this->details=$data;
				} else $this->msg=$this->t("no current orders");
			} else {
				$this->code=3;
				$this->msg=$this->t("you session has expired or someone login with your account");
			}
		} else $this->msg=merchantApp::parseValidatorError($Validator->getError());	    	
		$this->output();    	    
    }

    public function actionOrderdDetails()
    {

        $Validator=new Validator;
		$req=array(
		  'token'=>$this->t("token is required"),
		  'driver_id'=>$this->t("merchant id is required"),
		  'user_type'=>$this->t("user type is required"),
		  'order_id'=>$this->t("order id is required")
		);
		$Validator->required($req,$this->data);
		if ($Validator->validate()){
			if ( $res=query("SELECT * FROM {{driver}} WHERE token=? AND user_type=?",array($this->data['token'],$this->data['user_type']))){
				 $res=$res[0];
			    
				if($data=query("SELECT 
								a.*,b.viewed,b.task_id,d.street,
								d.city,d.state,d.zipcode,d.location_name,d.country,d.contact_phone,d.latitude,d.longitude,d.origin_merchant,
								concat(first_name,' ',last_name) as customer_name
								FROM {{order}} a 
								JOIN {{driver_task}} b 
								ON a.order_id=b.order_id 
								JOIN {{client}} c
								ON c.client_id=a.client_id
								JOIN {{order_delivery_address}} d 
								ON a.order_id=d.order_id 
								WHERE a.order_id=? AND b.driver_id=? LIMIT 0,1",array($this->data['order_id'],$res['driver_id']))) {
					// var_dump($data);
					$data = $data[0];
					$cart = query("SELECT b.merchant_name,a.merchant_id, a.item_id, item_name,a.order_notes,a.normal_price,a.discounted_price,a.qty 
									FROM {{order_details}} a
									LEFT JOIN {{merchant}} b
									ON a.merchant_id=b.merchant_id
									WHERE order_id=".$this->data['order_id']." ");//AND merchant_id=".$this->data['mtid']."
					$weight_total = 0;
					for($i=0;$i<count($cart);$i++) {
						$cart[$i]['item_name'] = $cart[$i]['item_name']." (".$cart[$i]['merchant_name'].")";
						$cart[$i]['normal_price_currency']=merchantApp::prettyPrice($cart[$i]['normal_price']);
						$cart[$i]['discounted_price_currency']=merchantApp::prettyPrice($cart[$i]['discounted_price']);
						$cart[$i]['weight'] = $cart[$i]['weight']?$cart[$i]['weight']:500;
						$weight_total += $cart[$i]['qty']* $cart[$i]['weight'];
					}
					// var_dump($cart);
					$data_raw['cart'] = $cart;

					//dump($data);
					  $data_raw['order_id']=$data['order_id'];
					  $data_raw['subtotal']=$data['sub_total'];
					  $data_raw['subtotal_currency']=merchantApp::prettyPrice($data['sub_total']);
					  $data_raw['taxable_total']=$data['taxable_total'];
					  $data_raw['taxable_total_currency']=merchantApp::prettyPrice($data['taxable_total']);
					  $data_raw['delivery_charge']=$data['delivery_charge'];
					  $data_raw['delivery_charge_currency']=merchantApp::prettyPrice($data['delivery_charge']);
					  $data_raw['total']=$data['total_w_tax'];
					  $data_raw['total_currency']=merchantApp::prettyPrice($data['total_w_tax']);
					  // $data_raw['total']['tax_amt']=$data_raw['total']['tax_amt']."%";
					  $data_raw['weight_total']=($weight_total>=1000)?round($weight_total/1000)." kg":($weight_total)." gram";
					  $pos = Yii::app()->functions->getOptionAdmin('admin_currency_position'); 
					  $data_raw['currency_position']=$pos;
					  $delivery_date=$data['delivery_date'];
					  $data_raw['transaction_date']	= Yii::app()->functions->FormatDateTime($data['date_created']);						          $data_raw['delivery_date'] = Yii::app()->functions->FormatDateTime($delivery_date,false);
					  $data_raw['delivery_time'] = $data['delivery_time'];
					  $data_raw['status']=t($data['status']);
					  $data_raw['status_raw']=strtolower($data['status']);
					  $data_raw['trans_type']=t($data['trans_type']);
					  $data_raw['trans_type_raw']=$data['trans_type'];
					  $data_raw['payment_type']=strtoupper($data['payment_type']);
					  $data_raw['delivery_instruction']=$data['delivery_instruction'];
					  
					  // $customer_info = query("SELECT CONCAT(b.first_name,' ',b.last_name) as full_name,b.email_address,
													 // a.street,a.city,a.state,a.zipcode,a.location_name,a.contact_phone
											  // FROM {{order_delivery_address}} a 
											  // JOIN {{client}} b ON a.client_id=b.client_id 
											  // WHERE a.order_id=".$data['order_id']);
					  // $data_raw['customer_info']=$customer_info[0];

					$field_destination = array('client_id','customer_name','contact_phone','street','city','state','zipcode','location_name','country','latitude','longitude');
					$dest_customer = array();
					foreach($field_destination as $f) {
						$dest_customer[$f] = $data[$f];
					}
					$data_raw['dest_customer'] = $dest_customer;
					$data_raw['origin_merchant']= json_decode($data['origin_merchant']);
					
					  $this->code=1;
					  $this->msg="OK";				  
					  $this->details=$data_raw;
					  
					  // update the order id to viewed
					  querySQL("UPDATE {{driver_task}} SET viewed=2 WHERE order_id=".$this->data['order_id']." AND driver_id=".$res['driver_id']);

			    } else $this->msg=$this->t("order details not available");
			} else {
				$this->code=3;
				$this->msg=$this->t("you session has expired or someone login with your account");
			}
		} else $this->msg=merchantApp::parseValidatorError($Validator->getError());	    	
		$this->output();    	    	
    }

	//changeOrderStatus
    public function actionChangeOrderStatus()
    {
    	
    	if(isset($_GET['debug'])){
    	   dump($this->data);
    	}
    	
    	if ( !$token=Driver::getDriverByToken($this->data['token'])) {
    		$this->msg=self::t("Token not valid");
    		$this->output();
    		Yii::app()->end();
    	}
    	$driver_id=$token['driver_id'];

    	$driver_name=$token['first_name'] ." " .$token['last_name'];    	
    	
    	$db=new DbExt;	
    	
    	if (isset($this->data['status']) && isset($this->data['order_id'])){
    		
    		$task_info=Driver::getTaskFromOrder($this->data['order_id']);
    		if(!$task_info){
    			$this->msg=self::t("Task not found");
    			$this->output();
    			Yii::app()->end();
    		}
			//cek driver adalah owner dari task order tsb
    		if($task_info['driver_id'] !== $driver_id){
    			$this->msg=self::t("Sorry, currently you are not the owner of this order. So you can't change status of this order.");
    			$this->output();
    			Yii::app()->end();
    		}
			
    		$this->data['task_id'] = $task_info['task_id'];
			$task_id = $task_info['task_id'];
			
    		$params_history='';    		
    		$params_history['ip_address']=$_SERVER['REMOTE_ADDR'];
    	    $params_history['date_created']=date('c');
    	    $params_history['task_id']=$task_id;    	    
    	    $params_history['driver_id']=$driver_id;     	    
    	    $params_history['driver_location_lat']=isset($token['location_lat'])?$token['location_lat']:'';
    	    $params_history['driver_location_lng']=isset($token['location_lng'])?$token['location_lng']:'';
    	    
    		if($task_info['order_id']>0){
    		   $params_history['order_id']=$task_info['order_id'];
    		}
    				
    		
    		switch ($this->data['status']) {
    			
    			case "failed":
    			case "cancelled":    	
    			   $params=array('status'=>$this->data['status']);    				
    				// update task id
    				$db->updateData("{{driver_task}}",$params,'task_id',$task_id);
    				
    				$remarks=Driver::driverStatusPretty($driver_name,$this->data['status']);    				
    				$params_history['status']=$this->data['status'];
    				$params_history['remarks']=$remarks; 			    				
    				$params_history['reason']=isset($this->data['reason'])?$this->data['reason']:'' ; 
    				// insert history    				
    				$db->insertData("{{order_history}}",$params_history);
    				
    				$this->code=1;
    				$this->msg="OK";
    				$this->details=array(
					  'order_id'=>$this->data['order_id'],
    				  'task_id'=>$this->data['task_id'],
    				  'status'=>$params['status'],
    				);
    				
    				//update the order status
    				if($task_info['order_id']>0){
    					Driver::updateOrderStatus($task_info['order_id'],$this->data['status']);
    				}
    				
    				//send notification to customer
    				Driver::sendNotificationCustomer('DELIVERY_FAILED',$task_info);
    							
    				break;
    				
    			case "declined":
    				
					$params=array('status'=>"declined");    				    				
					$db->updateData("{{driver_task}}",$params,'task_id',$task_id);
					
					$remarks=Driver::driverStatusPretty($driver_name,'declined');    				
					$params_history['status']='declined';
					$params_history['remarks']=$remarks;    				    				
					// insert history    				
					$db->insertData("{{order_history}}",$params_history);
					
					//update the order status
					if($task_info['order_id']>0){
						Driver::updateOrderStatus($task_info['order_id'],$this->data['status']);
					}
					
					$this->code=1;
					$this->msg="OK";
					$this->details=array(
					  'order_id'=>$this->data['order_id'],
					  'task_id'=>$this->data['task_id'],
					  'status'=>$params['status'],
					);
    				
    				
    				//send email to admin or merchant
    				
    				break;
    				
				case "accepted":
    			case "accepted_driver":
					if($this->data['status'] == "accepted") {
						$this->data['status'] = "accepted_driver";
					}
    			    // double check if someone has already the accept task   			    
    			    if($task_info['status']!="unassigned"){        			    	
    			    	if ( $task_info['driver_id']!=$driver_id){			    	
    			           $this->msg=Driver::t("Sorry but this task is already been assigned to others");
    			           $this->output();
    			    	   Yii::app()->end();
    			    	}
    			    }
    			    
    				$params=array(
    				  'driver_id'=>$driver_id,
    				  'status'=>"accepted_driver",
    				);
    				// update task id    				
    				$db->updateData("{{driver_task}}",$params,'task_id',$task_id);
    				
    				$remarks=Driver::driverStatusPretty($driver_name,'accepted');
    				$params_history['status']='accepted_driver';
    				$params_history['remarks']=$remarks;
    				// insert history
    				$db->insertData("{{order_history}}",$params_history);
    				
    				//update the order status
    				if($task_info['order_id']>0){
    					Driver::updateOrderStatus($task_info['order_id'],$this->data['status']);
    				}
    				
    				$this->code=1;
    				$this->msg="OK";
    				$this->details=array(
					  'order_id'=>$this->data['order_id'],
    				  'task_id'=>$this->data['task_id'],
    				  'status'=>$params['status'],
    				);
    				
    				//update driver_assignment
    				$stmt_assign="UPDATE
    				{{driver_assignment}}
    				SET task_status='acknowledged'
    				WHERE task_id=".Driver::q($task_id)."
    				";
    				$db->qry($stmt_assign);
    				
    				//send notification to customer
   				    Driver::sendNotificationCustomer('DELIVERY_REQUEST_RECEIVED',$task_info);

    				break;
		   
				case "on_the_way":
    			case "ontheway":
    				 $params=array('status'=>"ontheway");
    				 $db->updateData("{{driver_task}}",$params,'task_id',$task_id);
    				// update task id
    				
    				$remarks=Driver::driverStatusPretty($driver_name,'ontheway');    				
    				$params_history['status']='ontheway';
    				$params_history['remarks']=$remarks;    				
    				// insert history
    				$db->insertData("{{order_history}}",$params_history);
    				
    				//update the order status
    				if($task_info['order_id']>0){
    					Driver::updateOrderStatus($task_info['order_id'],"ontheway");
    				}
    				
    				$this->code=1;
    				$this->msg="OK";
    				$this->details=array(
					  'order_id'=>$this->data['order_id'],
    				  'task_id'=>$this->data['task_id'],
    				  'status'=>$params['status'],
    				);    			
    				
    				//send notification to customer
    				Driver::sendNotificationCustomer('DELIVERY_DRIVER_STARTED',$task_info);
    				
    				break;
    				
    			case "successful":	 
				case "success":
    			    $params=array('status'=>"successful");
    			    $db->updateData("{{driver_task}}",$params,'task_id',$task_id);
    				// update task id
    				
    				$remarks=Driver::driverStatusPretty($driver_name,'successful');    				
    				$params_history['status']='successful';
    				$params_history['remarks']=$remarks;    				
    				// insert history
    				$db->insertData("{{order_history}}",$params_history);
    				
    				
    				//update the order status
    				if($task_info['order_id']>0){
    					Driver::updateOrderStatus($task_info['order_id'],$this->data['status']);
    				}
    				
    				$this->code=1;
    				$this->msg="OK";
    				$this->details=array(
					  'order_id'=>$this->data['order_id'],
    				  'task_id'=>$this->data['task_id'],
    				  'status'=>$params['status'],
    				);
					
    				//count sold for every item
					$query = query("SELECT a.item_id,SUM(qty) as sold FROM {{order_details}} a 
									JOIN {{item}} b ON a.item_id=b.item_id 
									JOIN {{order}} c ON a.order_id=c.order_id
									WHERE  c.status IN('successful','success')
									GROUP BY item_id");
					
					foreach($query as $q) {
						queryNoFetch("UPDATE {{item}} SET sold=? WHERE item_id=?",array($q['sold'],$q['item_id']));
					}
					
    				//send notification to customer
    				Driver::sendNotificationCustomer('DELIVERY_SUCCESSFUL',$task_info);
    				
    				break;
    				   
    			default:
    				$this->msg=self::t("Missing status");
    				break;
    		}
    	} else $this->msg=self::t("Missing parameters");
    	
    	$this->output();
    }
	public function actionOrderHistory()
	{
		if (!isset($this->data['order_id'])){
			$this->msg=$this->t("order is missing");
			$this->output();
			Yii::app()->end();
		}
		
    	if ( !$token=Driver::getDriverByToken($this->data['token'])) {
    		$this->msg=self::t("Token not valid");
    		$this->output();
    		Yii::app()->end();
    	}
		
		if(!query("SELECT * FROM {{driver_task}} WHERE order_id=? AND driver_id=?",array($this->data['order_id'],$token['driver_id']))){
    		$this->msg=self::t("Order not found");
    		$this->output();
    		Yii::app()->end();
		}
		
		if ( $res=merchantApp::getOrderHistory($this->data['order_id'])){
			  $data='';
			  foreach ($res as $val) {
				$data[]=array(
				  'id'=>$val['id'],
				  'status'=>t($val['status']),
				  // 'status_raw'=>strtolower($val['status']),
				  'remarks'=>$val['remarks'],
				  'date_created'=>Yii::app()->functions->FormatDateTime($val['date_created'],true),
				  'ip_address'=>$val['ip_address']
				);
			  }
			  $this->code=1;
			  $this->msg="OK";
			  $this->details=array(
				'order_id'=>$this->data['order_id'],
				'data'=>$data
			  );
		 } else {
			$this->msg=$this->t("No history found");			    	
			$this->details=$this->data['order_id'];
		 }

		$this->output();
	}

    //getprofile
    public function actionGetProfile()
    {    	
    	if ( !$token=Driver::getDriverByToken($this->data['token'])) {
    		$this->msg=self::t("Token not valid");
    		$this->output();
    		Yii::app()->end();
    	}     	
    	$driver_id=$token['driver_id'];    	
    	$info=Driver::driverInfo($driver_id);    	
    	$this->code=1;
    	$this->msg="OK";
    	$this->details=array(
    	  'contact_name'=>$info['first_name']." ".$info['last_name'],
    	  'contact_email'=>$info['email'],
    	  'contact_phone'=>$info['phone'],
    	  'transport_type_id'=>$info['transport_type_id'],
    	  'transport_type_id2'=>ucwords(self::t($info['transport_type_id'])),
    	  'transport_description'=>$info['transport_description'],
    	  'licence_plate'=>$info['licence_plate'],
    	  'color'=>$info['color'],
    	);
    	$this->output();     
    }
    
    public function actionGetTransport()
    {    	
    	$this->code=1;
    	$this->code=1;
    	$this->details=Driver::transportType();
    	$this->output();     
    }
  
    public function actionSaveProfile()
    {    	
    	if ( !$token=Driver::getDriverByToken($this->data['token'])) {
    		$this->msg=self::t("Token not valid");
    		$this->output();
    		Yii::app()->end();
    	}     	
    	$driver_id=$token['driver_id']; 
    	
    	$Validator=new Validator;
    	$req=array(
    	  'transport_type_id'=>self::t("Transport Type is required"),
    	  'transport_description'=>self::t("Description is required"),
    	  /*'licence_plate'=>self::t("License Plate is required"),
    	  'color'=>self::t("Color is required"),*/
    	);
    	if ( $this->data['transport_type_id']=="truck"){
    		unset($req);
    		$req=array(
    		  'transport_type_id'=>self::t("Transport Type is required")
    		);
    	}
    	$Validator->required($req,$this->data);
    	if ( $Validator->validate()){
    		$params=array(
			  'first_name'=>$this->data['contact_name'],
			  'email'=>$this->data['contact_email'],
			  'phone'=>$this->data['contact_phone'],
    		  'transport_type_id'=>$this->data['transport_type_id'],
    		  'transport_description'=>$this->data['transport_description'],
    		  'licence_plate'=>isset($this->data['licence_plate'])?$this->data['licence_plate']:'',
    		  'color'=>isset($this->data['color'])?$this->data['color']:'',
    		  'date_modified'=>date('c'),
    		  'ip_address'=>$_SERVER['REMOTE_ADDR']
    		);
    		$db=new DbExt;
    		if ( $db->updateData("{{driver}}",$params,'driver_id',$driver_id)){
    			$this->code=1;
    			$this->msg=self::t("Profile updated");
    		} else $this->msg=self::t("Something went wrong please try again later");
    	} else $this->msg=Driver::parseValidatorError($Validator->getError());
    	$this->output();     
    }
    //change password
    public function actionChangePassword()
    {
    	if ( !$token=Driver::getDriverByToken($this->data['token'])) {
    		$this->msg=self::t("Token not valid");
    		$this->output();
    		Yii::app()->end();
    	}     	
    	$driver_id=$token['driver_id']; 
    	
    	$Validator=new Validator;
    	$req=array(
    	  'oldpassword'=>self::t("Old password is required"),
    	  'password'=>self::t("New password is required"),
    	  'cpassword'=>self::t("Confirm password is required")    	  
    	);    	
    	if ( $this->data['password']!=$this->data['cpassword']){
    		$Validator->msg[]=self::t("Confirm password does not macth with your new password");
    	}
    	
    	$Validator->required($req,$this->data);
    	if ( $Validator->validate()){
    		    		    		
    		if (!Driver::driverAppLogin($token['username'],$this->data['oldpassword'])){
    			$this->msg=self::t("Current password is invalid");
    			$this->output();     
    			Yii::app()->end();
    		}    		
    		$params=array(
    		  'password'=>md5($this->data['password']),
    		  'date_modified'=>date('c'),
    		  'ip_address'=>$_SERVER['REMOTE_ADDR']
    		);
    		$db=new DbExt;
    		if ( $db->updateData("{{driver}}",$params,'driver_id',$driver_id)){
    			$this->code=1;
    			$this->msg=self::t("Password Successfully Changed");
    			$this->details=$this->data['password'];
    		} else $this->msg=self::t("Something went wrong please try again later");
    	} else $this->msg=Driver::parseValidatorError($Validator->getError());
    	$this->output();     
    }
    
    public function actionSettingPush()
    {
    	if ( !$token=Driver::getDriverByToken($this->data['token'])) {
    		$this->msg=self::t("Token not valid");
    		$this->output();
    		Yii::app()->end();
    	}     	
    	$driver_id=$token['driver_id']; 
    	    	
    	$params=array(
    	  'enabled_push'=>$this->data['enabled_push'],
    	  'date_modified'=>date('c'),
    	  'ip_address'=>$_SERVER['REMOTE_ADDR']
    	);
    	$db=new DbExt;
		if ( $db->updateData("{{driver}}",$params,'driver_id',$driver_id)){
			$this->code=1;
			$this->msg=self::t("Setting Saved");			
		} else $this->msg=self::t("Something went wrong please try again later");
		$this->output();     
    }
    
    public function actionGetSettings()
    {
    	if ( !$token=Driver::getDriverByToken($this->data['token'])) {
    		$this->msg=self::t("Token not valid");
    		$this->output();
    		Yii::app()->end();
    	}     	
    	$driver_id=$token['driver_id']; 
    	
    	$lang=Driver::availableLanguages();
    	
    	$resp=array(
    	  'enabled_push'=>$token['enabled_push'],
    	  'language'=>$lang
    	);
    	$this->code=1;
    	$this->msg="OK";
    	$this->details=$resp;
    	$this->output();     
    }
    public function actionGetNotification()
    {
    	if ( !$token=Driver::getDriverByToken($this->data['token'])) {
    		$this->msg=self::t("Token not valid");
    		$this->output();
    		Yii::app()->end();
    	}     	
    	$driver_id=$token['driver_id'];
    	if ( $res=Driver::getDriverNotifications($driver_id)) {
    		 $data='';
    		 foreach ($res as $val) {
    		 	$val['date_created']=Driver::prettyDate($val['date_created']);
    		 	//$val['date_created']=date("h:i:s",strtotime($val['date_created']));
    		 	$data[]=$val;
    		 }
    		 $this->code=1;
    		 $this->msg="OK";
    		 $this->details=$data;
    	} else $this->msg=self::t("No notifications");
    	$this->output();
    }

    public function actionRegisterMobile()
    {
    	if ( !$token=Driver::getDriverByToken($this->data['token'])) {
    		$this->msg=self::t("Token not valid");
    		$this->output();
    		Yii::app()->end();
    	}
		$params=array(
		  'device_id'=>isset($this->data['registrationId'])?$this->data['registrationId']:'',
		  'device_platform'=>isset($this->data['device_platform'])?$this->data['device_platform']:'Android'
		);
		
		$db=new DbExt;
		if ( $db->updateData("{{driver}}",$params,'driver_id',$token['driver_id'])){
			$this->code=1;
			$this->msg="Updated";
		}
		$this->output(); 
    }
    
	public function actionUpdateDriverLocation()
    {    	
    	//demo
    	//die();
    	
    	if ( !$token=Driver::getDriverByToken($this->data['token'])) {
    		$this->msg=self::t("Token not valid");
    		$this->output();
    		Yii::app()->end();
    	}     	
    	$driver_id=$token['driver_id'];
    	$params=array(
    	  'location_lat'=>$this->data['latitude'],
    	  'location_lng'=>$this->data['longitude'],
    	  'last_login'=>date('c'),
	      'last_online'=>strtotime("now")
    	);
    	$db=new DbExt;
    	if ( $db->updateData("{{driver}}",$params,'driver_id',$driver_id)){
    		$this->code=1;
    		$this->msg="Location set";
    	} else $this->msg="Failed";
    	$this->output();    	
    }
	
    public function actionGetLanguageSelection()
    {
    	if ($res=Yii::app()->functions->getLanguageList()){
			$set_lang_id=Yii::app()->functions->getOptionAdmin('set_lang_id');					
				$eng[]=array(
				  'lang_id'=>"en",
				  'country_code'=>"US",
				  'language_code'=>"English"
				);
				$res=array_merge($eng,$res);
			//}						
			$this->code=1;
			$this->msg="OK";
			$this->details=$res;
		} else $this->msg=Driver::t("no language available");
		$this->output();
    }
    

    //OrderDetails
    public function actionViewOrderDetails()
    {
    	if ( !$token=Driver::getDriverByToken($this->data['token'])) {
    		$this->msg=self::t("Token not valid");
    		$this->output();
    		Yii::app()->end();
    	}
    	$driver_id=$token['driver_id']; 
    	$order_id= $this->data['order_id'];
    	
		$_GET['backend']='true';
		if ( $data=Yii::app()->functions->getOrder2($order_id)){	
			//dump($data);					
			$json_details=!empty($data['json_details'])?json_decode($data['json_details'],true):false;
			if ( $json_details !=false){
			    Yii::app()->functions->displayOrderHTML(array(
			       'merchant_id'=>$data['merchant_id'],
			       'order_id'=>$order_id,
			       'delivery_type'=>$data['trans_type'],
			       'delivery_charge'=>$data['delivery_charge'],
			       'packaging'=>$data['packaging'],
			       'cart_tip_value'=>$data['cart_tip_value'],
				   'cart_tip_percentage'=>$data['cart_tip_percentage'],
				   'card_fee'=>$data['card_fee'],
				   'donot_apply_tax_delivery'=>$data['donot_apply_tax_delivery'],
				   'points_discount'=>isset($data['points_discount'])?$data['points_discount']:'' /*POINTS PROGRAM*/
			     ),$json_details,true,$order_id);
			     $data2=Yii::app()->functions->details;
			     unset($data2['html']);			     
			     $this->code=1;
			     $this->msg="OK";
			     
			     $admin_decimal_separator=getOptionA('admin_decimal_separator');
		         $admin_decimal_place=getOptionA('admin_decimal_place');
		         $admin_currency_position=getOptionA('admin_currency_position');
		         $admin_thousand_separator=getOptionA('admin_thousand_separator');
			     
			     $data2['raw']['settings']=Driver::priceSettings();
			     $data2['raw']['order_info']=array(
			       'order_id'=>$data['order_id'],
			       'order_change'=>$data['order_change'],
			     );
			     
			     /*order info*/			     
			     $merchant_info=Yii::app()->functions->getMerchant($data['merchant_id']);
		 $full_merchant_address=$merchant_info['street']." ".$merchant_info['city']. " ".$merchant_info['state']." ".$merchant_info['post_code'];
		         $order_info[]=array(
		           'label'=>Driver::t("Customer Name"),
		           'value'=>$data['full_name']
		         );
		         $order_info[]=array(
		           'label'=>Driver::t("Merchant Name"),
		           'value'=>stripslashes($data['merchant_name'])
		         );
		         $order_info[]=array(
		           'label'=>Driver::t("Telephone"),
		           'value'=>$data['merchant_contact_phone']
		         );
		         $order_info[]=array(
		           'label'=>Driver::t("Address"),
		           'value'=>$full_merchant_address
		         );
		         $order_info[]=array(
		           'label'=>Driver::t("TRN Type"),
		           'value'=>$data['trans_type']
		         );
		         $order_info[]=array(
		           'label'=>Driver::t("Payment Type"),
		           'value'=>strtoupper(t($data['payment_type']))
		         );
		         if ( $data['payment_provider_name']){
		         	$order_info[]=array(
		             'label'=>Driver::t("Card#"),
		             'value'=>$data['payment_provider_name']
		           );
		         }
		         
		         $order_info[]=array(
		           'label'=>Driver::t("Reference #"),
		           'value'=>Yii::app()->functions->formatOrderNumber($data['order_id'])
		         );
		         
		         if ( !empty($data['payment_reference'])){
                    $order_info[]=array(
		              'label'=>Driver::t("Payment Ref"),
		              'value'=>$data['payment_reference']
		            );
                 }
                 
                 
                 $trn_date=date('M d,Y G:i:s',strtotime($data['date_created']));
                 $order_info[]=array(
		            'label'=>Driver::t("TRN Date"),
		            'value'=>Yii::app()->functions->translateDate($trn_date)
		         );
		         
		         if (isset($data['delivery_date'])){
		         	if(!empty($data['delivery_date'])){		         		
		         		$delivery_date=prettyDate($data['delivery_date']);
                        $delivery_date=Yii::app()->functions->translateDate($delivery_date);                        
                         $order_info[]=array(
		                  'label'=>$data['trans_type']=="delivery"?Driver::t("Delivery Date"):Driver::t("Pickup Date"),
		                  'value'=>$delivery_date
		                 );
		         	}
		         }
		         
		         if (isset($data['delivery_time'])){
		         	if(!empty($data['delivery_time'])){
		         		$delivery_time=Yii::app()->functions->timeFormat($data['delivery_time'],true);
		         		$order_info[]=array(
		                  'label'=>$data['trans_type']=="delivery"?Driver::t("Delivery Time"):Driver::t("Pickup Time"),
		                  'value'=>$delivery_time
		                 );
		         	}
		         }
		         
		         if(isset($data['delivery_asap'])){
		         	if(!empty($data['delivery_asap'])){
		         		$order_info[]=array(
		                  'label'=>Driver::t("Deliver ASAP"),
		                  'value'=>$data['delivery_asap']==1?Driver::t("Yes"):""
		                );
		         	}
		         }
		         
		         if (!empty($data['client_full_address'])){
                    $delivery_address=$data['client_full_address'];
                 } else $delivery_address=$data['full_address'];	
                 
                 $order_info[]=array(
		            'label'=>Driver::t("Deliver to"),
		            'value'=>$delivery_address
		         ); 
		         
		         if(!empty($data['delivery_instruction'])){
		           $order_info[]=array(
		              'label'=>Driver::t("Delivery Instruction"),
		              'value'=>$data['delivery_instruction']
		           ); 
		         }
		         
		         if (!empty($data['location_name1'])){
                     $location_name=$data['location_name1'];
                 } else $location_name=$data['location_name'];

                 $order_info[]=array(
		            'label'=>Driver::t("Location Name"),
		            'value'=>$location_name
		         ); 
		         
		         $order_info[]=array(
		            'label'=>Driver::t("Contact Number"),
		            'value'=>!empty($data['contact_phone1'])?$data['contact_phone1']:$data['contact_phone']
		         ); 
		         
		         if($data['order_change']>0.1){
		         	$order_info[]=array(
		               'label'=>Driver::t("Change"),
		               'value'=>displayPrice( baseCurrency(), normalPrettyPrice($data['order_change']))
		            ); 
		         }
		         		         
		         //dump($order_info);
		         $this->msg=$order_info;			     
			     $this->details=$data2['raw'];			     
			     
			} else $this->msg = self::t("Record not found");
		} else $this->msg = self::t("Record not found");    	
    	$this->output();
    }

    public function actionClearNofications()
    {
    	if ( !$token=Driver::getDriverByToken($this->data['token'])) {
    		$this->msg=self::t("Token not valid");
    		$this->output();
    		Yii::app()->end();
    	}     	
    	$driver_id=$token['driver_id'];
    	$stmt="UPDATE 
    	{{driver_pushlog}}
    	SET
    	is_read='1'
    	WHERE
    	driver_id=".self::q($driver_id)."
    	AND
    	is_read='2'
    	";
    	$this->code=1;
    	$this->msg="OK";
    	$db=new DbExt;
    	$db->qry($stmt);
    	$this->output(); 
    }
    
    public function actionDeviceConnected()
    {
    	if (!isset($this->data['token'])){
    		$this->data['token']='';
    	}
    	if ( !$token=Driver::getDriverByToken($this->data['token'])) {
    		$this->msg="token not found";
    		$this->output();
    		Yii::app()->end();
    	}     	
    	$driver_id=$token['driver_id'];
    	Driver::updateLastOnline($driver_id);
    	$this->code=1;
    	$this->msg="OK";
    	$this->output(); 
    }
    
    public function actionLogout()
    {
    	if ( !$token=Driver::getDriverByToken($this->data['token'])) {
    		$this->msg=self::t("Token not valid");
    		$this->output();
    		Yii::app()->end();
    	} 
    	$driver_id=$token['driver_id'];
    	$params=array(    	  
    	  'last_online'=>time() - 300,
    	  'ip_address'=>$_SERVER['REMOTE_ADDR']
    	);
    	
    	$db=new DbExt;
    	$db->updateData('{{driver}}',$params,'driver_id',$driver_id);
    	$this->code=1;
    	$this->msg="OK";
    	$this->output();
    }


    public function actionviewTaskDescription()
    {
    	$this->actionTaskDetails();
    }
	public function actionTaskDetails()
    {    	
    	if ( !$token=Driver::getDriverByToken($this->data['token'])) {
    		$this->msg=self::t("Token not valid");
    		$this->output();
    		Yii::app()->end();
    	}   
    	if (isset($this->data['task_id'])){
    		if ( $res=Driver::getTaskId($this->data['task_id']) ){
    			    	    			    			
    			//check task belong to current driver    			    			
    			if ( $res['status']!="unassigned"){
	    			$driver_id=$token['driver_id'];
	    			if ($driver_id!=$res['driver_id']){
	    				$this->msg=Driver::t("Sorry but this task is already been assigned to others");
	    				$this->output();
	    				Yii::app()->end();
	    			}    			
    			}
    			
    			$this->code=1;
    			$this->msg=self::t("Task").":".$this->data['task_id'];
    			
    			$res['delivery_time']=Yii::app()->functions->timeFormat($res['delivery_date'],true);
    			$res['status']=self::t($res['status']);
    			$res['status_raw']=$res['status'];
    			$res['trans_type']=self::t($res['trans_type']);
    			$res['trans_type_raw']=$res['trans_type'];
    			
    			$res['history']=Driver::getDriverTaskHistory($this->data['task_id']);
    			
    			/*get signature if any*/
    			$res['customer_signature_url']='';
    			if (!empty($res['customer_signature'])){
    				$res['customer_signature_url']=Driver::uploadURL()."/".$res['customer_signature'];
    				if (!file_exists(Driver::uploadPath()."/".$res['customer_signature'])){
    					$res['customer_signature_url']='';
    				}
    			}
    			    						
    			$this->details=$res;
    		} else $this->msg=self::t("Task not found");
    	} else $this->msg=self::t("Task id is missing");
    	$this->output();
    }

    public function actionUpdateProfile2()
    {    	
    	if ( !$token=Driver::getDriverByToken($this->data['token'])) {
    		$this->msg=self::t("Token not valid");
    		$this->output();
    		Yii::app()->end();
    	}     	
    	$driver_id=$token['driver_id']; 
    	
    	$Validator=new Validator;
    	$req=array(
    	  'phone'=>self::t("Phone is required")    	  
    	);
    	$Validator->required($req,$this->data);
    	if ( $Validator->validate()){
    		$params=array(
    		  'phone'=>$this->data['phone'],
    		  'date_modified'=>date('c'),
    		  'ip_address'=>$_SERVER['REMOTE_ADDR']
    		);
    		$db=new DbExt;
    		if ( $db->updateData("{{driver}}",$params,'driver_id',$driver_id)){
    			$this->code=1;
    			$this->msg=self::t("Profile Successfully updated");
    		} else $this->msg=self::t("Something went wrong please try again later");
    	} else $this->msg=Driver::parseValidatorError($Validator->getError());
    	$this->output();     
    }
   
   
} /*end class*/