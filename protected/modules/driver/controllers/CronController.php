<?php
class CronController extends CController
{
	static $db;
	
	public function __construct()
	{		
		self::$db=new DbExt;
	}
	
	public function init()
	{			
		 // set website timezone
		 $website_timezone=Yii::app()->functions->getOptionAdmin("website_timezone");	 		 
		 if (!empty($website_timezone)){		 	
		 	Yii::app()->timeZone=$website_timezone;
		 }		 				 
	}
	
	public function actionIndex()
	{		
		
	}
	
	public function actionProcessPush()
	{
		$db=new DbExt;
		$status='';
		
		$ring_tone_filename = 'food_song';
		$api_key=Yii::app()->functions->getOptionAdmin('driver_push_api_key');		
		
		$driver_ios_push_mode=getOptionA('driver_ios_push_mode');		
		$driver_ios_pass_phrase=getOptionA('driver_ios_pass_phrase');		
		$driver_ios_push_dev_cer=getOptionA('driver_ios_push_dev_cer');
		$driver_ios_push_prod_cer=getOptionA('driver_ios_push_prod_cer');	
		
		$DriverIOSPush=new DriverIOSPush;
		$DriverIOSPush->pass_prase=$driver_ios_pass_phrase;
		$DriverIOSPush->dev_certificate=$driver_ios_push_dev_cer;
		$DriverIOSPush->prod_certificate=$driver_ios_push_prod_cer;
		
		$production=$driver_ios_push_mode=="production"?true:false;		
		
		$stmt="
		SELECT * FROM
		{{driver_pushlog}}
		WHERE
		status='pending'
		ORDER BY date_created ASC
		LIMIT 0,10
		";
		if ( $res=$db->rst($stmt)){
			foreach ($res as $val) {
				dump($val);
				$push_id=$val['push_id'];
				if (!empty($val['device_id'])){
					if(!empty($api_key)){
						$message=array(		 
						 'title'=>$val['push_title'],
						 'message'=>$val['push_message'],
						 'soundname'=>$ring_tone_filename,
						 'count'=>1,
						 'data'=>array(
						   'push_type'=>$val['push_type'],
						   'order_id'=>$val['order_id'],
						   'actions'=>$val['actions'],
						 )
					   );		
					   
					   dump($message);
					   
					   if ( strtolower($val['device_platform']) =="android"){
						   $resp=AndroidPush::sendPush($api_key,$val['device_id'],$message);
						   if(is_array($resp) && count($resp)>=1){
						   	   dump($resp);
				   	       	   if( $resp['success']>0){			   	       	   	   
				   	       	   	   $status="process";
				   	       	   } else {		   	       	   	   
				   	       	   	   $status=$resp['results'][0]['error'];
				   	       	   }
						   }  else $status="uknown push response";						
						      
					   } elseif ( strtolower($val['device_platform']) =="ios"  ) {
					   	
					   	   $additional_data=array(
					   	     'push_type'=>$val['push_type'],
						     'order_id'=>$val['order_id'],
						     'actions'=>$val['actions'],
					   	   );					   	   
					   	   if ( $DriverIOSPush->push($val['push_message'],$val['device_id'],$production,$additional_data) ){
					   	   	    $status="process";
					   	   } else $status=$DriverIOSPush->get_msg();
					   	   
					   } else {
					   	   $status="Uknown device";
					   }
					   				
					} else $status= "API key is empty";
				} else $status= "Device id is empty";
				
				$params=array(
				  'status'=>$status,
				  'date_process'=>date('c'),
				  'json_response'=>isset($resp)?json_encode($resp):'',
				  'ip_address'=>$_SERVER['REMOTE_ADDR']
				);
				dump($params);
				
				$db->updateData("{{driver_pushlog}}",$params,'push_id',$push_id);
				
			}
		} else echo 'no record to process';
	}
	
	public function actionAutoAssign()
	{
		
		$db=new DbExt;
		$distance_exp=3959;  $radius=3000;
		
		$date_now=date('Y-m-d');
		
		$stmt="SELECT a.*,b.weight FROM
		{{driver_task}} a
		JOIN {{order}} b
		ON a.order_id=b.order_id
		WHERE 1
		AND a.status IN ('unassigned','declined')  
		AND a.auto_assign_type=''
		
		ORDER BY a.task_id ASC
		LIMIT 0,2
		";
		dump($stmt);
		$motor_max_weight = Yii::app()->functions->getOptionAdmin('motor_max_weight');
		if ( $res=$db->rst($stmt)){			
			foreach ($res as $val) {
				
				dump($val);
				
				$user_type=$val['user_type'];
				$user_id=$val['user_id'];
				
				$driver_enabled_auto_assign = Driver::getOption('driver_enabled_auto_assign',$user_type,$user_id);
				$driver_include_offline_driver = Driver::getOption('driver_include_offline_driver',$user_type,$user_id);
				$driver_auto_assign_type = Driver::getOption('driver_auto_assign_type',$user_type,$user_id);
				$driver_assign_request_expire = Driver::getOption('driver_assign_request_expire',$user_type,$user_id);
				$assign_type = $driver_auto_assign_type;
				
				$notify_email = Driver::getOption('driver_autoassign_notify_email',$user_type,$user_id);
				
				dump("driver_enabled_auto_assign=>".$driver_enabled_auto_assign);
				dump("driver_include_offline_driver->".$driver_include_offline_driver);
				
				if(empty($driver_enabled_auto_assign)){
					echo "auto assign is disabled";		
					$db->updateData("{{driver_task}}",array(
					  'auto_assign_type'=>"none"
					),'task_id',$val['task_id']);
					continue;
				}

				$lat=$val['task_lat'];
				$lng=$val['task_lng'];
				$task_id=$val['task_id'];				
				dump($lat); dump($lng);
				
				$and='';
				$todays_date=date('Y-m-d');			
		        $time_now = time() - 600;
		        
		        $assignment_status="waiting for driver acknowledgement";
				
				if ( $driver_include_offline_driver==""){
					$and.=" AND a.on_duty ='1' ";
                    $and.=" AND a.last_online >='$time_now' ";
                    $and.=" AND a.last_login like '".$todays_date."%'";
				}
				
				$limit="LIMIT 0,1";
					$and.=" AND a.user_type=".Driver::q($user_type)." AND a.status='active '";
				    if ($user_type=="merchant"){
					  $and.=" AND a.user_id=".Driver::q($user_id)."";
				    }
				    if (!empty($val['driver_before'])){
					  $and.=" AND a.driver_id NOT IN(".$val['driver_before'].")";
				    }
					//validasi motor/mobil (weight dalam gram, motor_max_weight dalam kg)
					if($val['weight']/1000 > $motor_max_weight) {
						//berat order > beban maksimal motor maka pakai mobil
						$and.=" AND a.transport_type_id LIKE '%mobil%' ";
					} else {
						//beban masih jangkauan motor
						$and.=" AND a.transport_type_id LIKE '%motor%' ";
					}
					// $and.=" AND a.driver_id NOT IN (
					  // select driver_id
					  // from
					  // {{driver_assignment}}
					  // where
					  // driver_id=a.driver_id
					  // and
					  // task_id=".Driver::q($task_id)."
					// ) ";

					$stmt2="
					SELECT a.driver_id, a.first_name,a.last_name,a.location_lat,a.location_lng,
					a.user_type,a.user_id,
					a.on_duty, a.last_online, a.last_login
					, 
					( $distance_exp * acos( cos( radians($lat) ) * cos( radians( location_lat ) ) 
			        * cos( radians( location_lng ) - radians($lng) ) 
			        + sin( radians($lat) ) * sin( radians( location_lat ) ) ) ) 
			        AS distance
			        FROM {{driver}} a
					WHERE 1 $and
			        HAVING distance < $radius
					AND distance < 30
					ORDER BY distance ASC
					$limit
					";

				dump($stmt2);
				//die();
				$assigned_task='assigned';
				
				if ( $res2=$db->rst($stmt2)){
					foreach ($res2 as $val2) {
						
						//UPDATE TASK
						$params=array(
						  'driver_id'=>$val2['driver_id'],
						  'status'=>$assigned_task,
						  'date_modified'=>date('c'),
						  'ip_address'=>$_SERVER['REMOTE_ADDR'],
						  'driver_before'=>empty($val['driver_before'])?$val2['driver_id']:$val['driver_before'].",".$val2['driver_id']
						);
						$db->updateData("{{driver_task}}",$params,'task_id',$task_id);
						echo "<h3>driver_assignment</h3>";
						dump($params);
						
						//UPDATE HISTORI
						
						if ( $res=Driver::getTaskId($this->data['task_id'])){
							$status_pretty = Driver::prettyStatus($res['status'],$assigned_task);
							$params_history=array(
							  'order_id'=>$res['order_id'],
							  'remarks'=>$status_pretty,
							  'status'=>$assigned_task,
							  'date_created'=>date('c'),
							  'ip_address'=>$_SERVER['REMOTE_ADDR'],
							  'task_id'=>$task_id
							);
							$DbExt->insertData('{{order_history}}',$params_history);
						}
						
						//SEND NOTIF TO DRIVER
						$task_info=Driver::getTaskByDriverNTask($task_id, $val2['driver_id']);
						Driver::sendDriverNotification('ASSIGN_TASK',$task_info);	
					}
				} else {
					//send email
					if(!empty($notify_email)){
						dump($notify_email);				    		
						$email_enabled=getOptionA('FAILED_AUTO_ASSIGN_EMAIL');
						if($email_enabled){
						   $tpl=getOptionA('FAILED_AUTO_ASSIGN_EMAIL_TPL');
						   $tpl=Driver::smarty('TaskID',$task_id,$tpl);
						   $tpl=Driver::smarty('CompanyName',getOptionA('website_title'),$tpl);
						   dump($tpl);
						   sendEmail($notify_email,"","Unable to auto assign Task $task_id",$tpl);
						}
					}
					$assignment_status = "unable to auto assign";
				}
			} /*end foreach*/
			
			$less="-1";
			if($driver_assign_request_expire>0){
				$less="-$driver_assign_request_expire";
			}
			
			$params_task=array(
			 'auto_assign_type'=>$assign_type,
			 'assign_started'=>date('c',strtotime("$less min")),
			 'assignment_status'=> $assignment_status
			);
			dump($params_task);
			$db->updateData("{{driver_task}}",$params_task,'task_id',$task_id);
			
		} else echo 'no record to process';
		
		sleep(1);
		$url=Yii::app()->getBaseUrl(true)."/driver/cron/processautoassign";
		echo @file_get_contents($url);
	}

	public function actionProcessBulk()
	{
		$stmt="SELECT * FROM
		{{driver_bulk_push}}
		WHERE
		status='pending'
		ORDER BY bulk_id ASC
		LIMIT 0,1
		";
		if ( $res=self::$db->rst($stmt)){
			foreach ($res as $val) {
				$bulk_id=$val['bulk_id'];
				dump($val);
				$stmt2="SELECT a.* FROM
				{{driver}} a
				WHERE
				device_id !=''
				AND driver_id NOT IN (
				  select driver_id
				  from {{driver_pushlog}}
				  where
				  driver_id=a.driver_id
				  and
				  bulk_id=".Driver::q($bulk_id)."
				)
				ORDER BY driver_id ASC
				LIMIT 0,1000
				";
				dump($stmt2);
				if ( $res2=self::$db->rst($stmt2)){
					foreach ($res2 as $val2) {						
						$params=array(
						  'push_title'=>$val['push_title'],
						  'push_message'=>$val['push_message'],
						  'device_platform'=>$val2['device_platform'],
						  'driver_id'=>$val2['driver_id'],
						  'device_id'=>$val2['device_id'],
						  'push_type'=>"bulk",
						  'actions'=>"bulk",
						  'bulk_id'=>$bulk_id,
						  'date_created'=>date('c'),
						  'ip_address'=>$_SERVER['REMOTE_ADDR']
						);
						dump($params);
						self::$db->insertData("{{driver_pushlog}}",$params);
					}
				} else {
					echo "No records to process";
					self::$db->updateData("{{driver_bulk_push}}",
					   array('status'=>"process",'date_process'=>date('c'))
					   ,'bulk_id',$bulk_id);
				}
			}
		} else echo "No records to process";
	}
		
} /*end class*/