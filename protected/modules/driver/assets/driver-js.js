var ajax_request_2;
var ajax_request_3;
var ajax_request_4;


function callAjaxSilent(action,params,button)
{
	dump(ajax_url+"/"+action+"?"+params);
	
	ajax_request_3 = $.ajax({
		url: ajax_url+"/"+action, 
		data: params,
		type: 'post',                  		
		dataType: 'json',
		timeout: 7000,		
	 beforeSend: function() {
	 	if(ajax_request_3 != null) {
	 	   ajax_request_3.abort();
	 	   dump("ajax abort");	 	     
	 	} else {	 	   
	 	}
	 },
	 complete: function(data) {							
		ajax_request_3 = (function () { return; })();	
		//callAjaxSilent(action,params,button);
	 },
	 success: function (data) {	  	 	
	 	if (data.code==1){
	 	 	 switch (action)
	 	 	 {
	 	 	 
	 	 	 	case "getDashboardTask":
	 			
	 	 	 	$.each( data.details , function( key, val ) {     	 				
	 				if ( !empty(val)){	 					
	 					$(".task_"+key).html( val.html );
	 			        $(".task-total-"+key).html( val.total );
	 				} else {	 					
	 				   $(".task_"+key).html( '');
	 			       $(".task-total-"+key).html( "0" );
	 				}
	 			});	 			
	 				 			
	 			dump( "coordinatesy=>" + data.msg.length);
	 			//plotMainMap( data.msg  );	 
	 			if(driver_disabled_auto_refresh==2){
	 			   run_agent_dashboard=2;
	 			   map.removeMarkers();	 			
	 			   plotTaskMap( data.msg  );	
	 			}
	 			break;
	 			
	 	 	 	default:
	 			nAlert(data.msg,"success");
	 			break;	
	 	 	 }
	 	 } else {
	 	 	 switch ( action )
	 	 	 {
	 	 	 	case "getDashboardTask":	 	 			
	 			break;
	 				 			
	 			
	 	 	 	default :
	 			nAlert(data.msg,"warning");
	 			break;
	 	 	 }
	 	 }
	 },
	 error: function (request,error) {	    
	 	 	 		
	 }
    });   	
}


function formatAgetList(data,status)
{
    if (empty(data)){
    	return '';
    }
	var class_name='tag_offline';
	
	if (data.is_online==1){
		class_name='tag_online';
	}
	
	var html='';
	html+='<div class="row box locate-driver-onmap" data-lat="'+data.location_lat+'" data-lng="'+data.location_lng+'" >';
      html+='<div class="col-md-2 center"> ';
       html+='<div class="top10"><i class="ion-ios-circle-filled '+class_name+' "></i></div> ';
      html+='</div> <!--col-->';
      
      html+='<div class="col-md-7"> ';
        html+='<p class="agent_name">'+  data.first_name + " "+ data.last_name +'</p>  ';
        html+='<p class="connection_status">'+status+'</p>';
        html+="<a href=\"javascript:;\" data-id=\""+data.driver_id+"\" class=\"view-driver-details\">"+jslang.details+"</a>";
      html+='</div> <!--col-->';
      
      html+='<div class="col-md-3 center">';
         html+='<p class="number_of_task">'+data.total_task+'</p>';
         html+='<p class="text-muted">'+jslang.task+'</p>';
      html+='</div>';
      
    html+='</div> <!--row-->';
    return html;
}

function formatTableRow(data)
{
	var link='<a style="display:block;" data-id="'+data.task_id+'" class="task-details" href="javascript:;">'+data.task_id+'</a>';
	var html='';
      html+='<tr>';
      html+='<td>'+link+'</td>';
       html+='<td>'+data.customer_name+'</td>';
       html+='<td>'+data.trans_type+'</td>';
       html+='<td>'+data.delivery_address+'</td>';
       html+='<td><span class="tag '+data.status_raw+'">'+data.status+'</span></td>';
      html+='</tr>';
    return html;  
}
