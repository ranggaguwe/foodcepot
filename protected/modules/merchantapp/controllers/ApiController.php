<?php
class ApiController extends CController
{	
	public $data;
	public $code=2;
	public $msg='';
	public $details='';
	
	public function __construct()
	{
		// $this->data=$_GET;
		$this->data=$_POST;
		if (isset($_GET['post_type'])){
			if ($_GET['post_type']=="get"){
				$this->data=$_GET;	
			}
		}
		$website_timezone=Yii::app()->functions->getOptionAdmin("website_timezone");		 
	    if (!empty($website_timezone)){
	 	   Yii::app()->timeZone=$website_timezone;
	    }		 
	}
	
	public function beforeAction($action)
	{				
		/*check if there is api has key*/		
		$action=Yii::app()->controller->action->id;				
		if(isset($this->data['api_key'])){
			if(!empty($this->data['api_key'])){			   
			   $continue=true;
			   if($action=="getLanguageSettings" || $action=="registerMobile"){
			   	  $continue=false;
			   }
			   if($continue){
			   	   $key=getOptionA('merchant_app_hash_key');
				   if(trim($key)!=trim($this->data['api_key'])){
				   	 $this->msg=$this->t("api hash key is not valid");
			         $this->output();
			         Yii::app()->end();
				   }
			   }			
			}
		}		
		return true;
	}	
	
	public function actionIndex(){
		//throw new CHttpException(404,'The specified url cannot be found.');
	}		
	
	private function q($data='')
	{
		return Yii::app()->db->quoteValue($data);
	}
	
	private function t($message='')
	{
		return Yii::t("default",$message);
	}
		
    private function output()
    {
		if(empty($this->details)) {
		   $resp=array(
			 'code'=>$this->code,
			 'msg'=>$this->msg,
			 'request'=>$this->data
		   );
		} else {
		   $resp=array(
			 'code'=>$this->code,
			 'msg'=>$this->msg,
			 'details'=>$this->details,
			 'request'=>$this->data
		   );
		}
	   if (isset($this->data['debug'])){
	   	   dump($resp);
	   }
	   
	   if (!isset($_GET['callback'])){
  	   	   $_GET['callback']='';
	   }    

		echo json_encode($resp,JSON_UNESCAPED_SLASHES);
	   // if (isset($_GET['json']) && $_GET['json']==TRUE){
	   	   // echo CJSON::encode($resp);
	   // } else echo $_GET['callback'] . '('.CJSON::encode($resp).')';		    	   	   	  
	   Yii::app()->end();
    }	
    
		public function actionSignUp()
		{		    
						
            /** check if admin has enabled the google captcha*/    	    	
	    	// if ( getOptionA('captcha_merchant_signup')==2){
	    		// if ( GoogleCaptcha::checkCredentials()){
	    			// if ( !GoogleCaptcha::validateCaptcha()){
	    				// $this->msg=GoogleCaptcha::$message;
	    				// return false;
	    			// }	    		
	    		// }	    	
	    	// }
	    	
	    	if (isset($this->data['cpassword'])){
	    		if ($this->data['cpassword']!=$this->data['password']){
	    			$this->msg=t("Confirm password does not match");
					$this->output();
	    			return ;
	    		}
	    	}
	    				
			if (Yii::app()->functions->isMerchantExist($this->data['contact_email'])){
				$this->msg=Yii::t("default","Sorry you input email address that is already registered in our records.");
				$this->output();
				return ;
			}					
			
			
			$package_price=0;
			if ( $package['promo_price']>=1){
				$package_price=$package['promo_price'];
			} else $package_price=$package['price'];			
			
			$expiration=$package['expiration'];			
			$membership_expired = date('Y-m-d', strtotime ("+$expiration days"));							
			
			$status=Yii::app()->functions->getOptionAdmin('merchant_sigup_status');			
			$token=md5($this->data['merchant_name'].date('c'));
			$DbExt=new DbExt;
		    $params=array(
		      'merchant_name'=>addslashes($this->data['merchant_name']),
		      'merchant_phone'=>$this->data['merchant_phone'],
		      'contact_name'=>$this->data['contact_name'],
		      'contact_phone'=>$this->data['contact_phone'],
		      'contact_email'=>$this->data['contact_email'],
		      'street'=>$this->data['street'],
		      'city'=>$this->data['city'],
		      'post_code'=>$this->data['post_code'],
		      // 'cuisine'=>json_encode($this->data['cuisine']),
		      'username'=>$this->data['username'],
		      'password'=>md5($this->data['password']),
		      'status'=>$status,
		      'date_created'=>date('c'),
		      'ip_address'=>$_SERVER['REMOTE_ADDR'],
		      'activation_token'=>$token,
		      'activation_key'=>Yii::app()->functions->generateRandomKey(5),
		      'merchant_slug'=>Yii::app()->functions->createSlug($this->data['merchant_name']),
		      'package_id'=>1,//$this->data['package_id'],
		      'membership_expired'=>$membership_expired,
		      'payment_steps'=>2,
		      //'country_code'=>Yii::app()->functions->adminSetCounryCode(),
		      'country_code'=>"ID",//$this->data['country_code'],
		      'state'=>$this->data['state'],
		      'abn'=>isset($this->data['abn'])?$this->data['abn']:'',
		      'service'=>isset($this->data['service'])?$this->data['service']:'',
		    );
		    		    
		    
		    if ( !Yii::app()->functions->validateUsername($this->data['username']) ){
		    			    	
		    	if ($respck=Yii::app()->functions->validateMerchantUserFromMerchantUser($params['username'],
		    	    $params['contact_email'])){
		    		$this->msg=$respck;
					$this->output();
		    		return ;		    		
		    	}		    
		    	
			    if ($DbExt->insertData("{{merchant}}",$params)){
			    	$this->code=1;
			    	$this->msg=Yii::t("default","Successful");
			    	$this->details=array('token'=>$token,'activation_key'=>$params['activation_key']);
			    	
			    	// send email activation key
			    	$tpl=EmailTPL::merchantActivationCode($params);
		            $sender=Yii::app()->functions->getOptionAdmin('website_contact_email');
		            $to=$this->data['contact_email'];		    		  
		            if (!sendEmail($to,$sender,"Merchant Registration",$tpl)){		    	
		            	//$this->details="failed";
		            } //else $this->details="ok mail";
		            			    				    				    	
			    } else $this->msg=Yii::t("default","Sorry but we cannot add your information. Please try again later");
		    } else $this->msg=Yii::t("default","Sorry but your username is alread been taken.");
			$this->output();
		}

		public function actionSignupActivation()
		{		   
		   $merchant_status=Yii::app()->functions->getOptionAdmin("merchant_sigup_status");		   
		   $DbExt=new DbExt;
		   if (isset($this->data['activation_code']) && isset($this->data['token'])){
		      $stmt="SELECT * FROM		   
		      {{merchant}}
		      WHERE
		      activation_token='".$this->data['token']."'
		      LIMIT 0,1
		      ";
		      if ($res=$DbExt->rst($stmt)){		      	 		      	 
		      	 if ($res[0]['status']=="active"){
		      	 	$this->msg=Yii::t("default","Merchant is already activated.");
		      	 } else {		      
			      	 if ($res[0]['activation_key']==$this->data['activation_code']){
			      	 	$this->code=1;		      	 	
			      	 	$this->msg=Yii::t("default","Merchant Successfully activated.");
			      	 	$this->details=$this->data['token'];
			      	 	
			      	 	$params=array('status'=>"active",'date_activated'=>date('c'),'ip_address'=>$_SERVER['REMOTE_ADDR']);
			      	 				      	 	
			      	 	/*If payment was offline don't set the the status to active*/
			      	 	if ( $package_info=FunctionsV3::getMerchantPaymentMembership($res[0]['merchant_id'],
			      	 	$res[0]['package_id'])){			      	 		
			      	 		$offline_payment=FunctionsV3::getOfflinePaymentList();			      	 		
			      	 		if ( array_key_exists($package_info['payment_type'],(array)$offline_payment)){
			      	 			unset($params['status']);
			      	 		}
			      	 	}			      	 
			      	 				      	 	
			      	 	$DbExt->updateData("{{merchant}}",$params,'merchant_id',$res[0]['merchant_id']);
			      	 	
			      	 } else $this->msg=Yii::t("default","Invalid Activation code.");		      
		      	 }
		      } else $this->msg=Yii::t("default","Sorry but we cannot find your information.");
		   } else $this->msg=Yii::t("default","ERROR: Missing parameters");
		   $this->output();
		}
	//
    public function actionLogin()
    {
        $Validator=new Validator;
		$req=array(
		  'username'=>$this->t("username is required"),
		  'password'=>$this->t("password is required")
		);
		$Validator->required($req,$this->data);
		if ($Validator->validate()){
			if ( $res=merchantApp::login($this->data['username'],md5($this->data['password']))){				
				if ($res['status']=="active" || $res['status']=="expired"){
					
					/*get device information and update*/
					if (isset($this->data['merchant_device_id'])){
						if ( $resp=merchantApp::getDeviceInfo($this->data['merchant_device_id'])){							
							$record_id=$resp['id'];
							
							$params['merchant_id']=$res['merchant_id'];		
							$params['user_type']=$res['user_type'];
							$params['date_modified']=date('c');
							$params['status']='active';
							
							if ( $res['user_type']=="admin"){
							    $params['merchant_user_id']=0;
							} else {								
								$params['merchant_user_id']=$res['merchant_user_id'];
							}											
							$DbExt=new DbExt;
							$DbExt->updateData('{{mobile_device_merchant}}',$params,'id',$record_id);
							
							/*now update all device previous use by user*/
							if ( $res['user_type']=="admin"){								
								$stmt_update="UPDATE
								{{mobile_device_merchant}}																
								SET status='inactive'
								WHERE
								merchant_id =".merchantApp::q($res['merchant_id'])."
								AND
								user_type ='admin'
								AND id NOT IN ('$record_id')
								";
								$DbExt->qry($stmt_update);
							} else {								
								$stmt_update="UPDATE
								{{mobile_device_merchant}}																
								SET status='inactive'
								WHERE
								merchant_user_id  =".merchantApp::q($res['merchant_user_id'])."
								AND
								user_type ='user'
								AND id NOT IN ('$record_id')
								";
								$DbExt->qry($stmt_update);
							}												
						}
					}
					
					$this->msg=$this->t("Successul");
					$this->code=1;
					$this->details=array(
					  'token'=>$res['token'],
					  'info'=>array(
					    'username'=>$res['username'],
					    'merchant_name'=>$res['merchant_name'],					    
					    'contact_email'=>$res['contact_email'],
					    'user_type'=>$res['user_type'],
					    'merchant_id'=>$res['merchant_id']
					  )
					);
				} else $this->msg=$this->t("Login Failed. You account status is")." ".$res['status'];
			} else $this->msg=$this->t("either username or password is invalid");
		} else $this->msg=merchantApp::parseValidatorError($Validator->getError());	    	
		$this->output();
    }
    //
    public function actionGetTodaysOrder()
    {    	
    	
    	$Validator=new Validator;
		$req=array(
		  'token'=>$this->t("token is required"),
		  'mtid'=>$this->t("merchant id is required"),
		  'user_type'=>$this->t("user type is required"),
		);
		$Validator->required($req,$this->data);
		if ($Validator->validate()){
			if ( $res=merchantApp::validateToken($this->data['mtid'],
			    $this->data['token'],$this->data['user_type'])){
			    	
			    $DbExt=new DbExt;	
				$stmt="
				SELECT a.*,b.viewed,
				(
				select concat(first_name,' ',last_name)
				from 
				{{client}}
				where
				client_id=a.client_id
				limit 0,1				
				) as customer_name
				
				FROM
				{{order}} a
				JOIN {{order_merchant}} b
				ON a.order_id=b.order_id
				WHERE
				b.merchant_id=".$this->q($res['merchant_id'])."				
				AND
				date_created LIKE '".date("Y-m-d")."%'						
				AND 
				status NOT IN ('initial_order')					
				ORDER BY date_created DESC
				LIMIT 0,100
				";
				if ( $res=$DbExt->rst($stmt)){					
					$this->code=1; $this->msg="OK";					
					foreach ($res as $val) {							
						$data[]=array(						  
						  'order_id'=>$val['order_id'],
						  'viewed'=>$val['viewed'],
						  'status'=>t($val['status']),
						  'status_raw'=>strtolower($val['status']),
						  'trans_type'=>t($val['trans_type']),
						  'trans_type_raw'=>$val['trans_type'],
						  'total_w_tax'=>$val['total_w_tax'],						  
						  'total_w_tax_currency'=>merchantApp::prettyPrice($val['total_w_tax']),
						  'transaction_date'=>Yii::app()->functions->FormatDateTime($val['date_created'],true),
						  'transaction_time'=>Yii::app()->functions->timeFormat($val['date_created'],true),
						  'delivery_time'=>Yii::app()->functions->timeFormat($val['delivery_time'],true),
						  'delivery_asap'=>$val['delivery_asap']==1?t("ASAP"):'',
						  'delivery_date'=>Yii::app()->functions->FormatDateTime($val['delivery_date'],false),
						  'customer_name'=>!empty($val['customer_name'])?$val['customer_name']:$this->t('No name')
						);
					}
					$this->code=1;
					$this->msg="OK";
					$this->details=$data;
				} else $this->msg=$this->t("no current orders");
			} else {
				$this->code=3;
				$this->msg=$this->t("you session has expired or someone login with your account");
			}
		} else $this->msg=merchantApp::parseValidatorError($Validator->getError());	    	
		$this->output();    	    
    }
    //
    public function actionGetPendingOrders()
    {    	    
    	$Validator=new Validator;
		$req=array(
		  'token'=>$this->t("token is required"),
		  'mtid'=>$this->t("merchant id is required"),
		  'user_type'=>$this->t("user type is required"),
		);
		$Validator->required($req,$this->data);
		if ($Validator->validate()){
			if ( $res=merchantApp::validateToken($this->data['mtid'],
			    $this->data['token'],$this->data['user_type'])){
			    	
			    $_in="'pending'";
			    $pending_tabs=getOptionA('merchant_app_pending_tabs');
				if(!empty($pending_tabs)){
				   $pending_tabs=json_decode($pending_tabs,true);
				   if(is_array($pending_tabs) && count($pending_tabs)>=1){
				   	  $_in='';
				   	  foreach ($pending_tabs as $key=>$val) {
				   	      $_in.="'$val',";
				   	  }
				   	  $_in=substr($_in,0,-1);
				   }
				}		
								
			    $DbExt=new DbExt;	
				$stmt="
				SELECT a.*,				
				(
				select concat(first_name,' ',last_name)
				from 
				{{client}}
				where
				client_id=a.client_id
				limit 0,1				
				) as customer_name

				FROM
				{{order}} a
				JOIN {{order_merchant}} b
				ON a.order_id=b.order_id
				WHERE
				b.merchant_id=".$this->q($res['merchant_id'])."
				AND
				status IN ($_in)							
				ORDER BY date_created DESC
				LIMIT 0,100
				";
				if(isset($_GET['debug'])){
					dump($stmt);
				}
				if ( $res=$DbExt->rst($stmt)){					
					$this->code=1; $this->msg="OK";					
					foreach ($res as $val) {						
						$data[]=array(
						  'order_id'=>$val['order_id'],
						  'viewed'=>$val['viewed'],
						  'status'=>t($val['status']),
						  'status_raw'=>strtolower($val['status']),
						  'trans_type'=>t($val['trans_type']),
						  'trans_type_raw'=>$val['trans_type'],
						  'total_w_tax'=>$val['total_w_tax'],						  
						  'total_w_tax_currency'=>merchantApp::prettyPrice($val['total_w_tax']),
						  'transaction_date'=>Yii::app()->functions->FormatDateTime($val['date_created'],true),
						  'transaction_time'=>Yii::app()->functions->timeFormat($val['date_created'],true),
						  'delivery_time'=>Yii::app()->functions->timeFormat($val['delivery_time'],true),
						  'delivery_asap'=>$val['delivery_asap']==1?t("ASAP"):'',
						  'delivery_date'=>Yii::app()->functions->FormatDateTime($val['delivery_date'],false),
						  'customer_name'=>!empty($val['customer_name'])?$val['customer_name']:$this->t('No name')
						);
					}					
					$this->code=1;
					$this->msg="OK";
					$this->details=$data;
				} else $this->msg=$this->t("no pending orders");
			} else {
				$this->code=3;
				$this->msg=$this->t("you session has expired or someone login with your account");
			}
		} else $this->msg=merchantApp::parseValidatorError($Validator->getError());	    	
		$this->output();    	    
    }    
    //
    public function actionGetAllOrders()
    {
    	$Validator=new Validator;
		$req=array(
		  'token'=>$this->t("token is required"),
		  'mtid'=>$this->t("merchant id is required"),
		  'user_type'=>$this->t("user type is required"),
		);
		$Validator->required($req,$this->data);
		if ($Validator->validate()){
			if ( $res=merchantApp::validateToken($this->data['mtid'],
			    $this->data['token'],$this->data['user_type'])){
			    $merchant_data = $res;	
			    $DbExt=new DbExt;
				$stmt="
				SELECT a.*,
					concat(first_name,' ',last_name) as customer_name,
					d.street,d.city,d.state,d.zipcode,d.location_name,d.country,
					d.contact_phone,d.latitude,d.longitude,d.origin_merchant
				FROM
				{{order}} a
				JOIN {{order_merchant}} b
				ON a.order_id=b.order_id
				JOIN {{client}} c
				ON c.client_id=a.client_id
				JOIN {{order_delivery_address}} d
				ON a.order_id=d.order_id
				WHERE
				b.merchant_id=".$this->q($res['merchant_id'])."	
				AND a.status NOT IN ('initial_order')			
				ORDER BY date_created DESC
				LIMIT 0,100
				";			
				$field_destination = array('client_id','customer_name','contact_phone','street','city','state','zipcode','location_name','country','latitude','longitude');
				
				if ( $res=$DbExt->rst($stmt)){					
					$this->code=1; $this->msg="OK";					
					foreach ($res as $val) {
						
						$dest_customer = array();
						foreach($field_destination as $f) {
							$dest_customer[$f] = $val[$f];
						}
						
						//jangan tampilkan merchant lainnya selain merchant yang sedang login
						$orig_merchant = json_decode($val['origin_merchant']);
						$only_my_merchant = array();
						foreach($orig_merchant as $om) {
							if($om->merchant_id==$merchant_data['merchant_id']){
								$only_my_merchant[] = $om;
							}
						}
						
						$data[]=array(
						  'order_id'=>$val['order_id'],
						  'viewed'=>$val['viewed'],
						  'status'=>t($val['status']),
						  'status_raw'=>strtolower($val['status']),
						  'trans_type'=>t($val['trans_type']),
						  'trans_type_raw'=>$val['trans_type'],
						  'total_w_tax'=>$val['total_w_tax'],						  
						  'total_w_tax_currency'=>merchantApp::prettyPrice($val['total_w_tax']),
						  'transaction_date'=>Yii::app()->functions->FormatDateTime($val['date_created'],true),
						  'transaction_time'=>Yii::app()->functions->timeFormat($val['date_created'],true),
						  'delivery_time'=>Yii::app()->functions->timeFormat($val['delivery_time'],true),
						  'delivery_asap'=>$val['delivery_asap']==1?t("ASAP"):'',
						  'delivery_date'=>Yii::app()->functions->FormatDateTime($val['delivery_date'],false),
						  'customer_name'=>!empty($val['customer_name'])?$val['customer_name']:$this->t('No name'),
						  'dest_customer' => $dest_customer,
						  'origin_merchant'=>$only_my_merchant,
						);
					}					
					$this->code=1;
					$this->msg="OK";
					$this->details=$data;
				} else $this->msg=$this->t("no orders found");
			} else {
				$this->code=3;
				$this->msg=$this->t("you session has expired or someone login with your account");
			}
		} else $this->msg=merchantApp::parseValidatorError($Validator->getError());	    	
		$this->output();    	    
    }

	
    public function actionSalesReport()
    {
    	$Validator=new Validator;
		$req=array(
		  'token'=>$this->t("token is required"),
		  'mtid'=>$this->t("merchant id is required"),
		  'user_type'=>$this->t("user type is required"),
		);
		$Validator->required($req,$this->data);
		if ($Validator->validate()){
			if ( $res=merchantApp::validateToken($this->data['mtid'],
			    $this->data['token'],$this->data['user_type'])){
			    	
			    $DbExt=new DbExt;
				$stmt="
				SELECT a.*,
					concat(first_name,' ',last_name) as customer_name,
					d.street,d.city,d.state,d.zipcode,d.location_name,d.country,
					d.contact_phone,d.latitude,d.longitude,d.origin_merchant,b.earning
				FROM
				{{order}} a
				JOIN {{order_merchant}} b
				ON a.order_id=b.order_id
				JOIN {{client}} c
				ON c.client_id=a.client_id
				JOIN {{order_delivery_address}} d
				ON a.order_id=d.order_id
				WHERE
				b.merchant_id=".$this->q($res['merchant_id'])."	
				AND a.status IN ('successful')			
				ORDER BY date_created DESC
				LIMIT 0,100
				";			
				$field_destination = array('client_id','customer_name','contact_phone','street','city','state','zipcode','location_name','country','latitude','longitude');
				
				if ( $res=$DbExt->rst($stmt)){					
					$this->code=1; $this->msg="OK";					
					foreach ($res as $val) {
						
						$dest_customer = array();
						foreach($field_destination as $f) {
							$dest_customer[$f] = $val[$f];
						}
						
						$data[]=array(
						  'order_id'=>$val['order_id'],
						  'earning'=>$val['earning'],
						  'earning_currency'=>merchantApp::prettyPrice($val['earning']),
						  'viewed'=>$val['viewed'],
						  'status'=>t($val['status']),
						  'status_raw'=>strtolower($val['status']),
						  'trans_type'=>t($val['trans_type']),
						  'trans_type_raw'=>$val['trans_type'],
						  'total_w_tax'=>$val['total_w_tax'],						  
						  'total_w_tax_currency'=>merchantApp::prettyPrice($val['total_w_tax']),
						  'transaction_date'=>Yii::app()->functions->FormatDateTime($val['date_created'],true),
						  'transaction_time'=>Yii::app()->functions->timeFormat($val['date_created'],true),
						  'delivery_time'=>Yii::app()->functions->timeFormat($val['delivery_time'],true),
						  'delivery_asap'=>$val['delivery_asap']==1?t("ASAP"):'',
						  'delivery_date'=>Yii::app()->functions->FormatDateTime($val['delivery_date'],false),
						  'customer_name'=>!empty($val['customer_name'])?$val['customer_name']:$this->t('No name'),
						  'dest_customer' => $dest_customer,
						  'origin_merchant'=>json_decode($val['origin_merchant']),
						);
					}					
					$this->code=1;
					$this->msg="OK";
					$this->details=$data;
				} else $this->msg=$this->t("no orders found");
			} else {
				$this->code=3;
				$this->msg=$this->t("you session has expired or someone login with your account");
			}
		} else $this->msg=merchantApp::parseValidatorError($Validator->getError());	    	
		$this->output();    	    
    }


    //
    public function actionOrderdDetails()
    {

        $Validator=new Validator;
		$req=array(
		  'token'=>$this->t("token is required"),
		  'mtid'=>$this->t("merchant id is required"),
		  'user_type'=>$this->t("user type is required"),
		  'order_id'=>$this->t("order id is required")
		);
		$Validator->required($req,$this->data);
		if ($Validator->validate()){
			if ( $res=merchantApp::validateToken($this->data['mtid'],
			    $this->data['token'],$this->data['user_type'])){
			    $z = $res;	
			    // if ( $data=Yii::app()->functions->getOrderDetail($this->data['order_id'])){

				if($data=query("SELECT a.*,
									concat(first_name,' ',last_name) as customer_name,
									d.street,d.city,d.state,d.zipcode,d.location_name,d.country,
									d.contact_phone,d.latitude,d.longitude,d.origin_merchant,
									b.earning,b.tax as tax_merchant,
									b.subtotal as subtotal_merchant 
								FROM {{order}} a 
								JOIN {{order_merchant}} b 
								ON a.order_id=b.order_id 
								JOIN {{client}} c
								ON c.client_id=a.client_id
								JOIN {{order_delivery_address}} d 
								ON a.order_id=d.order_id 
								WHERE a.order_id=? AND b.merchant_id=? LIMIT 0,1",array($this->data['order_id'],$z['merchant_id']))) {
					// var_dump($data);
					$data = $data[0];					
					$cart = query("SELECT merchant_id, item_id, item_name,order_notes,normal_price,discounted_price,qty FROM {{order_details}} WHERE merchant_id=? AND order_id=? ",array($z['merchant_id'],$this->data['order_id']));//
					// $subtotal = 0;
					$weight_total = 0;
					for($i=0;$i<count($cart);$i++) {
						$cart[$i]['normal_price_currency']=merchantApp::prettyPrice($cart[$i]['normal_price']);
						$cart[$i]['discounted_price_currency']=merchantApp::prettyPrice($cart[$i]['discounted_price']);
						// $subtotal += $cart[$i]['qty']*$cart[$i]['normal_price'];
						
						$cart[$i]['weight'] = $cart[$i]['weight']?$cart[$i]['weight']:500;
						$weight_total += $cart[$i]['qty']* $cart[$i]['weight'];
					}
					// var_dump($cart);
					$data_raw['cart'] = $cart;

					//dump($data);
					  $data_raw['order_id']=$data['order_id'];
					  $data_raw['subtotal']=$data['subtotal_merchant'];//special merchant
					  $data_raw['subtotal_currency']=merchantApp::prettyPrice($subtotal);//special merchant
					  $data_raw['taxable_total']=$data['tax_merchant'];//special merchant
					  $data_raw['taxable_total_currency']=merchantApp::prettyPrice($data['tax_merchant']);//special merchant
					  $data_raw['delivery_charge']=$data['delivery_charge'];
					  $data_raw['delivery_charge_currency']=merchantApp::prettyPrice($data['delivery_charge']);
					  $data_raw['total']=$data['earning'];//special merchant
					  $data_raw['total_currency']=merchantApp::prettyPrice($data['earning']);//special merchant
					  // $data_raw['total']['tax_amt']=$data_raw['total']['tax_amt']."%";
					  $data_raw['weight_total']=($weight_total>=1000)?round($weight_total/1000)." kg":($weight_total)." gram";
					  $pos = Yii::app()->functions->getOptionAdmin('admin_currency_position'); 
					  $data_raw['currency_position']=$pos;
					  $delivery_date=$data['delivery_date'];
					  $data_raw['transaction_date']	= Yii::app()->functions->FormatDateTime($data['date_created']);						          $data_raw['delivery_date'] = Yii::app()->functions->FormatDateTime($delivery_date,false);
					  $data_raw['delivery_time'] = $data['delivery_time'];
					  $data_raw['status']=t($data['status']);
					  $data_raw['status_raw']=strtolower($data['status']);
					  $data_raw['trans_type']=t($data['trans_type']);
					  $data_raw['trans_type_raw']=$data['trans_type'];
					  $data_raw['payment_type']=strtoupper($data['payment_type']);
					  $data_raw['delivery_instruction']=$data['delivery_instruction'];
					  
					  // $customer_info = query("SELECT CONCAT(b.first_name,' ',b.last_name) as full_name,b.email_address,
													 // a.street,a.city,a.state,a.zipcode,a.location_name,a.contact_phone
											  // FROM {{order_delivery_address}} a 
											  // JOIN {{client}} b ON a.client_id=b.client_id 
											  // WHERE a.order_id=".$data['order_id']);
					  // $data_raw['customer_info']=$customer_info[0];

					$field_destination = array('client_id','customer_name','contact_phone','street','city','state','zipcode','location_name','country','latitude','longitude');
					$dest_customer = array();
					foreach($field_destination as $f) {
						$dest_customer[$f] = $data[$f];
					}
					$data_raw['dest_customer'] = $dest_customer;
					
					//jangan tampilkan merchant lainnya selain merchant yang sedang login
					$orig_merchant = json_decode($data['origin_merchant']);
					$only_my_merchant = array();
					foreach($orig_merchant as $om) {
						if($om->merchant_id==$this->data['mtid']){
							$only_my_merchant[] = $om;
						}
					}
					$data_raw['origin_merchant']= $only_my_merchant;
					
					  $this->code=1;
					  $this->msg="OK";				  
					  $this->details=$data_raw;
					  
					  // update the order id to viewed
					  querySQL("UPDATE {{order_merchant}} SET viewed=2 WHERE order_id=".$this->data['order_id']." AND merchant_id=".$this->data['mtid']);

			    } else $this->msg=$this->t("order details not available");
			} else {
				$this->code=3;
				$this->msg=$this->t("you session has expired or someone login with your account");
			}
		} else $this->msg=merchantApp::parseValidatorError($Validator->getError());	    	
		$this->output();    	    	
    }
    //
    public function actionAcceptOrders()
    {
    	
    	$Validator=new Validator;
		$req=array(
		  'token'=>$this->t("token is required"),
		  'mtid'=>$this->t("merchant id is required"),
		  'user_type'=>$this->t("user type is required"),
		  'order_id'=>$this->t("order id is required")
		);
		$Validator->required($req,$this->data);
		if ($Validator->validate()){
			if ( $res=merchantApp::validateToken($this->data['mtid'],
			    $this->data['token'],$this->data['user_type'])){

			    $merchant_id=$res['merchant_id'];
			    $order_id=$this->data['order_id'];
			    
			    if ( Yii::app()->functions->isMerchantCommission($merchant_id)){  
	    	    	if ( FunctionsK::validateChangeOrder($order_id)){
	    	    		$this->msg=t("Sorry but you cannot change the order status of this order it has reference already on the withdrawals that you made");
	    	    		$this->output();	    	    		
	    	    	}    	    
    	        }	        
    	        
    	        /*check if merchant can change the status*/
	    	    $can_edit=Yii::app()->functions->getOptionAdmin('merchant_days_can_edit_status');	    	    
	    	    if (is_numeric($can_edit) && !empty($can_edit)){
	    	    	
		    	    $date_now=date('Y-m-d');
		    	    $base_option=getOptionA('merchant_days_can_edit_status_basedon');	
		    	    
		    	    $resp=Yii::app()->functions->getOrderInfo($order_id);	    	   
		    	    
		    	    if ( $base_option==2){
						$date_created=date("Y-m-d",
						strtotime($resp['delivery_date']." ".$resp['delivery_time']));		
					} else $date_created=date("Y-m-d",strtotime($resp['date_created']));
					    			
					
					$date_interval=Yii::app()->functions->dateDifference($date_created,$date_now);					
	    			if (is_array($date_interval) && count($date_interval)>=1){		    				
	    				if ( $date_interval['days']>$can_edit){
	    					$this->msg=t("Sorry but you cannot change the order status anymore. Order is lock by the website admin");
	    					$this->details=json_encode($date_interval);
	    					$this->output();
	    				}		    			
	    			}	    		
	    	    }
    	        
	    	    //$order_status='pending';
	    	    $order_status='accepted';
	    	    
    	        if ( $resp=Yii::app()->functions->verifyOrderIdByOwner($order_id,$merchant_id) ){     	        	

					
					//ACCEPT PARTIAL part_status 
					//ALL merchant have to approve all order
					// $connection=Yii::app()->db;
					// $command = Yii::app()->db->createCommand();
					// $command->update('{{order_merchant}}', array(
						// 'part_status'=>$order_status,
					// ), 'order_id=:order_id AND merchant_id=:merchant_id', 
					// array(':order_id'=>$order_id,':merchant_id'=>$merchant_id));
					queryNoFetch("UPDATE {{order_merchant}} SET part_status=? WHERE order_id=? AND merchant_id=?",array($order_status,$order_id,$merchant_id));
					
					$r = query("SELECT * FROM {{order_merchant}} WHERE part_status='' AND order_id=?",array($order_id));
					// var_dump($r);
					//jika masih ada part_status yang kosong artinya belum semua merchant yang approve
					
					//jika part status sudah tidak ada yang kosong (semua merchant melakukan approval)
					if(!$r) {
							$params=array(
							  'status'=>$order_status,
							  'date_modified'=>date('c'),
							  // 'viewed'=>2
							);
							$DbExt=new DbExt;
							if ($DbExt->updateData('{{order}}',$params,'order_id',$order_id)){
								$this->code=1;
								$this->msg=t("Order ID").":$order_id ".t("has been accepted");
								$this->details=array(
								 'order_id'=>$order_id
								);
								
								$merchant = getOne('merchant','merchant_id',$merchant_id);
								$this->data['remarks'] = isset($this->data['remarks'])?$merchant['merchant_name']." : ".$this->data['remarks']:'';
								
								/*Now we insert the order history*/	    		
								$params_history=array(
								  'order_id'=>$order_id,
								  'status'=>$order_status,
								  'remarks'=>$this->data['remarks'],
								  'date_created'=>date('c'),
								  'ip_address'=>$_SERVER['REMOTE_ADDR']
								);	    				
								$DbExt->insertData("{{order_history}}",$params_history);
								
								/*now we send email and sms*/
								merchantApp::sendEmailSMS($order_id);
								
								// send push notification to client mobile app when order status changes
								if(merchantApp::hasModuleAddon("mobileapp")){	    				   
								   $push_log='';
								   $push_log['order_id']=$order_id;
								   $push_log['status']=$order_status;
								   $push_log['remarks']=$this->data['remarks'];  
															  
								   Yii::app()->setImport(array(
									'application.modules.mobileapp.components.*',
								   ));

								   AddonMobileApp::savedOrderPushNotification($push_log);
								}
								
								if (FunctionsV3::hasModuleAddon("driver")){
									
									/*Driver app*/
									Yii::app()->setImport(array(
									  'application.modules.driver.components.*',
									));
									
									Driver::addToTask($order_id);
								}
								
								//TODO 
								//send push notif to all merchant that this order 
								//has been accepted by ALL MERCHANT
								

								
								
							} else $this->msg=t("ERROR: cannot update order.");  
					} else {
    	        		$this->code=1;
    	        		$this->msg=t("Order ID").":$order_id ".t("has been accepted. Wait other merchant to accept this order.");
    	        		$this->details=array(
    	        		 'order_id'=>$order_id
    	        		);
					}


					
    	        } else $this->msg=$this->t("This Order does not belong to you");
    	            	        	    
			} else {
				$this->code=3;
				$this->msg=$this->t("you session has expired or someone login with your account");
			}  	
		} else $this->msg=merchantApp::parseValidatorError($Validator->getError());	    	
		$this->output();    	    	
    }
    //
    public function actionDeclineOrders()
    {
    	
    	$Validator=new Validator;
		$req=array(
		  'token'=>$this->t("token is required"),
		  'mtid'=>$this->t("merchant id is required"),
		  'user_type'=>$this->t("user type is required"),
		  'order_id'=>$this->t("order id is required")
		);
		$Validator->required($req,$this->data);
		if ($Validator->validate()){
			if ( $res=merchantApp::validateToken($this->data['mtid'],
			    $this->data['token'],$this->data['user_type'])){
			    				    
			    $merchant_id=$res['merchant_id'];
			    $order_id=$this->data['order_id'];		 

			    if ( Yii::app()->functions->isMerchantCommission($merchant_id)){  
	    	    	if ( FunctionsK::validateChangeOrder($order_id)){
	    	    		$this->msg=t("Sorry but you cannot change the order status of this order it has reference already on the withdrawals that you made");
	    	    		$this->output();	    	    		
	    	    	}    	    
    	        }	        
    	        
    	        /*check if merchant can change the status*/
	    	    $can_edit=Yii::app()->functions->getOptionAdmin('merchant_days_can_edit_status');	    	    
	    	    if (is_numeric($can_edit) && !empty($can_edit)){
	    	    	
		    	    $date_now=date('Y-m-d');
		    	    $base_option=getOptionA('merchant_days_can_edit_status_basedon');	
		    	    
		    	    $resp=Yii::app()->functions->getOrderInfo($order_id);
		    	    
		    	    if ( $base_option==2){	    					
						$date_created=date("Y-m-d",
						strtotime($resp['delivery_date']." ".$resp['delivery_time']));		
					} else $date_created=date("Y-m-d",strtotime($resp['date_created']));
					    			
					$date_interval=Yii::app()->functions->dateDifference($date_created,$date_now);					
	    			if (is_array($date_interval) && count($date_interval)>=1){		    				
	    				if ( $date_interval['days']>$can_edit){
	    					$this->msg=t("Sorry but you cannot change the order status anymore. Order is lock by the website admin");
	    					$this->details=json_encode($date_interval);
	    					$this->output();
	    				}
	    			}
	    	    }
			    
			    $order_status='decline';

					if ( $resp=Yii::app()->functions->verifyOrderIdByOwner($order_id,$merchant_id) ){

						$r = getOne('order','order_id',$order_id);
						//Status that is pending or decline can be decline
						if($r['status'] == 'pending' || $r['status'] == 'decline') {
								$params=array(
								  'status'=>$order_status,
								  'date_modified'=>date('c'),
								  // 'viewed'=>2
								);
								
								$DbExt=new DbExt;
								if ($DbExt->updateData('{{order}}',$params,'order_id',$order_id)){
									$this->code=1;
									//$this->msg=t("order has been declined");
									$this->msg=t("Order ID").":$order_id ".t("has been declined");
									$this->details=array(
									 'order_id'=>$order_id
									);
									
									queryNoFetch("UPDATE {{order_merchant}} SET part_status='decline' WHERE order_id=? AND merchant_id=?",array($order_id,$merchant_id));
									
									$merchant = getOne('merchant','merchant_id',$merchant_id);
									$this->data['remarks'] = isset($this->data['remarks'])?$merchant['merchant_name']." : ".$this->data['remarks']:'';
									
									/*Now we insert the order history*/
									$params_history=array(
									  'order_id'=>$order_id,
									  'status'=>$order_status,
									  'remarks'=>$this->data['remarks'],
									  'date_created'=>date('c'),
									  'ip_address'=>$_SERVER['REMOTE_ADDR']
									);			
									$DbExt->insertData("{{order_history}}",$params_history);
									
									/*now we send email and sms*/
									merchantApp::sendEmailSMS($order_id);
									
									// send push notification to client mobile app when order status changes
									if(merchantApp::hasModuleAddon("mobileapp")){
									   $push_log='';
									   $push_log['order_id']=$order_id;
									   $push_log['status']=$order_status;
									   $push_log['remarks']=$this->data['remarks'];       
																  
									   Yii::app()->setImport(array(			
									   'application.modules.mobileapp.components.*',
									   ));                          
									   AddonMobileApp::savedOrderPushNotification($push_log);
									   
									} else $this->msg=t("The order that has been approve or decline can't be changed.");
									
								} else $this->msg=t("ERROR: cannot update order.");
								
						} else $this->msg=t("The order that has been approve or decline can't be changed.");
						
					} else $this->msg=$this->t("This Order does not belong to you");
			    
			} else {
				$this->code=3;
				$this->msg=$this->t("you session has expired or someone login with your account");
			}  	
		} else $this->msg=merchantApp::parseValidatorError($Validator->getError());	    	
		$this->output();   
    }
    //
    public function actionChangeOrderStatus()
    {
		
    	$Validator=new Validator;
		$req=array(
		  'token'=>$this->t("token is required"),
		  'mtid'=>$this->t("merchant id is required"),
		  'user_type'=>$this->t("user type is required"),
		  'order_id'=>t("order id is required"),
		  'status'=>t("order status is required")
		);
		$mtid=$this->data['mtid'];
		// $mtid = Yii::app()->functions->getMerchantID();
		// //jika yang melakukan update order adalah admin
		// if(Yii::app()->functions->isAdminLogin() && isset($this->data['mtid'])){
			// $mtid = $this->data['mtid'];
		// }
		
		$Validator->required($req,$this->data);
		if ($Validator->validate()){
			$DbExt=new DbExt;
			if ( $res=merchantApp::validateToken($this->data['mtid'],
			    $this->data['token'],$this->data['user_type'])){
			// if ( $res= query("SELECT * FROM {{merchant}} WHERE merchant_id=?",array($mtid))){
				// $res=$res[0];
$merchant_data = $res;
			    $merchant_id=$merchant_data ['merchant_id'];
$mtid=$merchant_data ['merchant_id'];
			    $order_id=$this->data['order_id'];
				
    	            	        
    	        /*check if merchant can change the status*/
	    	    $can_edit=Yii::app()->functions->getOptionAdmin('merchant_days_can_edit_status');	    	    
	    	    if (is_numeric($can_edit) && !empty($can_edit)){
	    	    	
		    	    $date_now=date('Y-m-d');
		    	    $base_option=getOptionA('merchant_days_can_edit_status_basedon');	
		    	    
		    	    $resp=Yii::app()->functions->getOrderInfo($order_id);
		    	    
		    	    if ( $base_option==2){	    					
						$date_created=date("Y-m-d",
						strtotime($resp['delivery_date']." ".$resp['delivery_time']));		
					} else $date_created=date("Y-m-d",strtotime($resp['date_created']));

					$date_interval=Yii::app()->functions->dateDifference($date_created,$date_now);					
	    			if (is_array($date_interval) && count($date_interval)>=1){		    				
	    				if ( $date_interval['days']>$can_edit){
	    					$this->msg=t("Sorry but you cannot change the order status anymore. Order is lock by the website admin");
	    					$this->details=json_encode($date_interval);
	    					$this->output();
	    				}
	    			}
	    	    }
			    //hanya admin dan merchant bersangkutan yang dapat merubah status order
			    if ( $resp=Yii::app()->functions->verifyOrderIdByOwner($order_id,$res['merchant_id']) || Yii::app()->functions->isAdminLogin() ){
				
					$order_status=$this->data['status'];
					$order_status2 = str_replace('_merchant','',$order_status);
					//ubah status order secara keseluruhan
					
					//ACCEPT PARTIAL part_status 
					//semua merchant mesti accepted agar overal_statusnya accepted
					
					queryNoFetch("UPDATE {{order_merchant}} SET part_status=? WHERE order_id=? AND merchant_id=?",array($order_status2,$order_id,$merchant_id));

					// $r = query("SELECT * FROM {{order_merchant}} WHERE (part_status='' OR part_status='declined') AND order_id=?",array($order_id));
					$record_history = false;
					
					//ambil status saat ini baik status2 masing-masing merchant (part_status) juga status keseluruhan overall status
					$r2 = query("SELECT * FROM {{order_merchant}} WHERE order_id=?",array($order_id));
					$order = query("SELECT * FROM {{order}} WHERE order_id=?",array($order_id));
					$order = $order[0];
					$overall_status = $order['status'];
					
					$list_part_status = array();
					$all_merchant_accepted = true;
					foreach($r2 as $p ) {
						$list_part_status[] = $p['part_status'];
						if(($p['part_status'] !== 'accepted')) {
							$all_merchant_accepted = false;
						}
					}
					
					//jika didecline 
					if($order_status2 == 'declined' &&  ($overall_status == 'pending' || $overall_status == 'initial_order')) {
						//jika ordernya credit kemudian decline maka saldo mesti dikembalikan ke user
						//tetapi overall status mesti pending karena user berpotensi melakukan decline berkali2
						if($order['payment_type'] == 'credit') {
							$client = query("SELECT * FROM {{client}} WHERE client_id=?",array($order['client_id'])); $client=$client[0];
							$new_saldo = $client['saldo']+$order['total_w_tax'];
							queryNoFetch("UPDATE {{client}} SET saldo=? WHERE client_id=?",array($new_saldo,$order['client_id']));
						}
						$params=array(
						  'status'=>$order_status2,
						  'date_modified'=>date('c'),
						);
						$DbExt=new DbExt;
						if ($DbExt->updateData('{{order}}',$params,'order_id',$order_id)){
							$this->code=1;
							$this->msg=t("order status successfully changed");
						}
						$record_history = true;
						
					} elseif($order_status2 == 'accepted'  && $overall_status == 'declined') {
						//order sudah decline tapi user mengaccept (tidak bisa)
						$this->msg=t("Order has been declined.");
						$this->details=array(
							 'order_id'=>$order_id
						);
						
					} elseif($order_status2 == 'accepted'  && ($all_merchant_accepted == false) && $overall_status !== 'declined') {
						//order sudah accept tapi masih ada merchant yang belum approve
						$this->code=1;
						$this->msg=t("Order ID").":$order_id ".t("has been accepted. Wait other merchant to accept this order");
						$this->details=array(
							 'order_id'=>$order_id
						);
						$record_history = true;
					} elseif($all_merchant_accepted &&  ($overall_status == 'pending' || $overall_status == 'initial_order' || $overall_status == 'accepted')) {
						//jika semua merchant accepted dan status overallnya pending
						
						//jika masih ada part_status yang kosong artinya belum semua merchant yang approve
						// jika part status sudah tidak ada yang kosong (semua merchant melakukan approval)
						// if(!$r) {
						$params=array(
						  'status'=>$order_status2,
						  'date_modified'=>date('c'),
						);
						
						
						if ($DbExt->updateData('{{order}}',$params,'order_id',$order_id)){
							$this->code=1;
							$this->msg=t("order status successfully changed");
							
							if(merchantApp::hasModuleAddon("mobileapp")){	    				   
							   $push_log='';
							   $push_log['order_id']=$order_id;
							   $push_log['status']=$order_status;
							   $push_log['remarks']=$this->data['remarks'];

							   Yii::app()->setImport(array(
							   'application.modules.mobileapp.components.*',
							   ));        
							   AddonMobileApp::savedOrderPushNotification($push_log);
							}
							
							if (FunctionsV3::hasModuleAddon("driver")){
								
								/*Driver app*/
								Yii::app()->setImport(array(
								  'application.modules.driver.components.*',
								));
								Driver::addToTask($order_id);
							}

							$this->code=1;
								$this->msg=t("Order ID").":$order_id ".t("has been accepted.");
								$this->details=array(
								 'order_id'=>$order_id
							);
							$record_history = true;
						} else $this->msg=t("ERROR: cannot update order.");
    	        	}

					if($record_history) {
						/*Now we insert the order history*/ 		
						$params_history=array(
						  'order_id'=>$order_id,
						  'status'=>$order_status2,
						  'remarks'=>isset($this->data['remarks'])?$this->data['remarks']:'',
						  'date_created'=>date('c'),
						  'ip_address'=>$_SERVER['REMOTE_ADDR']
						);
						$DbExt->insertData("{{order_history}}",$params_history);
					} else {
						$this->msg=t("Order is not updated in this state");
					}
			    } else $this->msg=t("This Order does not belong to you ");			    
			} else {
				$this->code=3;
				$this->msg=$this->t("you session has expired or someone login with your account");
			}
		} else $this->msg=merchantApp::parseValidatorError($Validator->getError());	    	
		$this->output();   		

    	// $Validator=new Validator;
		// $req=array(
		  // 'token'=>$this->t("token is required"),
		  // 'mtid'=>$this->t("merchant id is required"),
		  // 'user_type'=>$this->t("user type is required"),
		  // 'order_id'=>$this->t("order id is required"),
		  // 'status'=>$this->t("order status is required")
		// );
		// $Validator->required($req,$this->data);
		// if ($Validator->validate()){
			// if ( true || $res=merchantApp::validateToken($this->data['mtid'],
			    // $this->data['token'],$this->data['user_type'])){
			    				    
			    // $merchant_id=$this->data['mtid'];//$res['merchant_id'];
			    // $order_id=$this->data['order_id'];		 

			    // if ( Yii::app()->functions->isMerchantCommission($merchant_id)){  
	    	    	// if ( FunctionsK::validateChangeOrder($order_id)){
	    	    		// $this->msg=t("Sorry but you cannot change the order status of this order it has reference already on the withdrawals that you made");
	    	    		// $this->output();	    	    		
	    	    	// }    	    
    	        // }
    	            	        
    	        // /*check if merchant can change the status*/
	    	    // $can_edit=Yii::app()->functions->getOptionAdmin('merchant_days_can_edit_status');	    	    
	    	    // if (is_numeric($can_edit) && !empty($can_edit)){
	    	    	
		    	    // $date_now=date('Y-m-d');
		    	    // $base_option=getOptionA('merchant_days_can_edit_status_basedon');	
		    	    
		    	    // $resp=Yii::app()->functions->getOrderInfo($order_id);
		    	    
		    	    // if ( $base_option==2){	    					
						// $date_created=date("Y-m-d",
						// strtotime($resp['delivery_date']." ".$resp['delivery_time']));		
					// } else $date_created=date("Y-m-d",strtotime($resp['date_created']));
					    			
					// $date_interval=Yii::app()->functions->dateDifference($date_created,$date_now);					
	    			// if (is_array($date_interval) && count($date_interval)>=1){		    				
	    				// if ( $date_interval['days']>$can_edit){
	    					// $this->msg=t("Sorry but you cannot change the order status anymore. Order is lock by the website admin");
	    					// $this->details=json_encode($date_interval);
	    					// $this->output();
	    				// }		    			
	    			// }	    		
	    	    // }			   
			    
			    
				
				
				
			    // if ( true || $resp=Yii::app()->functions->verifyOrderIdByOwner($order_id,$merchant_id) ){     	        	
				
				
					// $order_status=$this->data['status'];
					// $order_status2 = str_replace('_merchant','',$order_status);
					
					// //ACCEPT PARTIAL part_status 
					// //ALL merchant have to approve all order
					// queryNoFetch("UPDATE {{order_merchant}} SET part_status=? WHERE order_id=? AND merchant_id=?",array($order_status2,$order_id,$merchant_id));
					
					
					// $r = query("SELECT * FROM {{order_merchant}} WHERE (part_status='' OR part_status='declined') AND order_id=?",array($order_id));
					// // var_dump($r);
					// //jika masih ada part_status yang kosong artinya belum semua merchant yang approve
					
					// //jika part status sudah tidak ada yang kosong (semua merchant melakukan approval)
					// if(!$r) {
						
						
						// $params=array( 
						  // 'status'=>$order_status2,
						  // 'date_modified'=>date('c'),
						// );
						
						// $DbExt=new DbExt;
						// if ($DbExt->updateData('{{order}}',$params,'order_id',$order_id)){
							// $this->code=1;
							// $this->msg=t("order status successfully changed");
							
							// /*Now we insert the order history*/	    		
							// $params_history=array(
							  // 'order_id'=>$order_id,
							  // 'status'=>$order_status,
							  // 'remarks'=>isset($this->data['remarks'])?$this->data['remarks']:'',
							  // 'date_created'=>date('c'),
							  // 'ip_address'=>$_SERVER['REMOTE_ADDR']
							// );
							// $DbExt->insertData("{{order_history}}",$params_history);
							
							
							// // send push notification to client mobile app when order status changes
							// if(merchantApp::hasModuleAddon("mobileapp")){	    				   
							   // $push_log='';
							   // $push_log['order_id']=$order_id;
							   // $push_log['status']=$order_status;
							   // $push_log['remarks']=$this->data['remarks'];
														   
							   // Yii::app()->setImport(array(			
							   // 'application.modules.mobileapp.components.*',
							   // ));                  
							   // AddonMobileApp::savedOrderPushNotification($push_log);
							// }
							
							// if (FunctionsV3::hasModuleAddon("driver")){
								
								// /*Driver app*/
								// Yii::app()->setImport(array(
								  // 'application.modules.driver.components.*',
								// ));
								
								// Driver::addToTask($order_id);
							// }
							
							
							// $this->code=1;
								// $this->msg=t("Order ID").":$order_id ".t("has been accepted.");
								// $this->details=array(
								 // 'order_id'=>$order_id
							// );
						// } else $this->msg=t("ERROR: cannot update order.");
    	        	// } else {
						// $this->code=1;
							// $this->msg=t("Order ID").":$order_id ".t("has been accepted. Wait other merchant to accept this order.");
							// $this->details=array(
							 // 'order_id'=>$order_id
						// );
					// }
			    // } else $this->msg=$this->t("This Order does not belong to you");			    
			// } else {
				// $this->code=3;
				// $this->msg=$this->t("you session has expired or someone login with your account");
			// }  	
		// } else $this->msg=merchantApp::parseValidatorError($Validator->getError());	    	
		// $this->output();   
    }
    //
    public function actionForgotPassword()
    {
    	
    	if (isset($this->data['email_address'])){
    		if (empty($this->data['email_address'])){
    			$this->msg=t("email address is required");
    			$this->output();
    		}
    		
    		if ($res=merchantApp::getUserByEmail($this->data['email_address'])){
    		   
    		   $tbl="merchant";
    		   if ( $res['user_type']=="user"){
    		   	   $tbl="merchant_user";
    		   }    		
    		   $params=array('lost_password_code'=> yii::app()->functions->generateCode());	 
    		   
    		   $DbExt=new DbExt;
    		   if ( $DbExt->updateData("{{{$tbl}}}",$params,'merchant_id',$res['merchant_id'])){
    		   	   $this->code=1;
    		   	   $this->msg=t("We have sent verification code in your email.");
					file_put_contents('log10.txt',var_export($params['lost_password_code'],true));
    		   	   $tpl=EmailTPL::merchantForgotPass($res,$params['lost_password_code']);
    			   $sender=Yii::app()->functions->getOptionAdmin('website_contact_email');
	               $to=$res['contact_email'];	               
	               if (!sendEmail($to,$sender,t("Merchant Forgot Password"),$tpl)){		    	
	                	$email_stats="failed";
	                } else $email_stats="ok mail";
	                
	                $this->details=array(
	                  'email_stats'=>$email_stats,
	                  'user_type'=>$res['user_type'],
	                  'email_address'=>$this->data['email_address']
	                );
	                
    		   } else $this->msg=t("ERROR: Cannot update");
    		   
    		} else $this->msg=t("sorry but the email address you supplied does not exist in our records");
    		
    	} else $this->msg=t("email address is required");
    	$this->output();   
    }
    //
    public function actionChangePasswordWithCode()
    {        
    	
    	
        $Validator=new Validator;
		$req=array(
		  'code'=>$this->t("code is required"),
		  'newpass'=>$this->t("new passwords is required"),		  
		  'user_type'=>t("user type is missing"),
		  'email_address'=>$this->t("email address is required")
		);
		$Validator->required($req,$this->data);
		if ($Validator->validate()){
			
			if ( $res=merchantApp::getMerchantByCode($this->data['code'],$this->data['email_address'],
			$this->data['user_type'])){
								
				$params=array(
				  'password'=>md5($this->data['newpass']),
	    		  'date_modified'=>date('c'),
	    	      'ip_address'=>$_SERVER['REMOTE_ADDR']
				);			
								
				$DbExt=new DbExt;
				if ( $this->data['user_type']=="admin"){
					// update merchant table
					if ($DbExt->updateData("{{merchant}}",$params,'merchant_id',$res['merchant_id'])){
						$this->msg=t("You have successfully change your password");
	    				$this->code=1;
					} else $this->msg=t("ERROR: cannot update records.");
				} else {
					// update merchant user table merchant_user_id
					if ($DbExt->updateData("{{merchant_user}}",$params,'merchant_user_id',$res['merchant_user_id'])){
						$this->msg=t("You have successfully change your password");
	    				$this->code=1;
					} else $this->msg=t("ERROR: cannot update records.");
				}				
			} else $this->msg=t("verification code is invalid");
			
		} else $this->msg=merchantApp::parseValidatorError($Validator->getError());	    	
		$this->output(); 
    }
    //
    public function actionRegisterMobile()
    {    	
    	$DbExt=new DbExt;
		$params['device_id']=isset($this->data['registrationId'])?$this->data['registrationId']:'';
		$params['device_platform']=isset($this->data['device_platform'])?$this->data['device_platform']:'';
		$params['ip_address']=$_SERVER['REMOTE_ADDR'];
				
		$user_type='admin';
		if (!empty($this->data['token'])){
			if ( $info=merchantApp::getUserByToken($this->data['token'])){				
				$user_type=$info['user_type'];
				$params['merchant_id']=$info['merchant_id'];
				$params['user_type']=$user_type;
				if ($user_type=="user"){
				   	$params['merchant_user_id']=$info['merchant_user_id'];
				} else $params['merchant_user_id']=0;
			}
		}					
		if ( $res=merchantApp::getDeviceInfo($this->data['registrationId'])){
			$params['date_modified']=date('c');				
			$DbExt->updateData('{{mobile_device_merchant}}',$params,'id',$res['id']);
			$this->code=1;
			$this->msg="Updated";
		} else {
			$params['date_created']=date('c');
			$DbExt->insertData('{{mobile_device_merchant}}',$params);
			$this->code=1;
			$this->msg="OK";
		}
		$this->output(); 
    }
    //
    public function actionStatusList()
    {    	        	
    	if ( $res=merchantApp::validateToken($this->data['mtid'],
			    $this->data['token'],$this->data['user_type'])){
			    				    				 
			 if (!$order_info = Yii::app()->functions->getOrder($this->data['order_id'])){
			 	$this->msg=t("order records not found");
			 	$this->output(); 
			 }			    
			 
			 if ( $res=merchantApp::orderStatusList($this->data['mtid']) ) {  				 	
			 	$this->details=array(
			 	  'status'=>$order_info['status'],
			 	  'status_list'=>$res
			 	);
			 	$this->code=1;
			 	$this->msg="OK";
			 } else $this->msg=t("Status list not available");
        } else {
		    $this->code=3;
		    $this->msg=$this->t("you session has expired or someone login with your account");
		}    
		$this->output(); 
    }
    //
	public function actionGetLanguageSelection()
	{
		if ($res=Yii::app()->functions->getLanguageList()){
			$set_lang_id=Yii::app()->functions->getOptionAdmin('set_lang_id');			
			if (preg_match("/-9999/i", $set_lang_id)) {
				$eng[]=array(
				  'lang_id'=>"en",
				  'country_code'=>"US",
				  'language_code'=>"English"
				);
				$res=array_merge($eng,$res);
			}						
			$this->code=1;
			$this->msg="OK";
			$this->details=$res;
		} else $this->msg=$this->t("no language available");
		$this->output();
	}    
	
	public function actionSaveSettings()
	{		
		$Validator=new Validator;		
		$req=array(
		  'token'=>$this->t("token is required"),
		  'mtid'=>$this->t("merchant id is required"),
		  'user_type'=>$this->t("user type is required"),
		  'merchant_device_id'=>t("mobile device id is empty please restart the app")
		);
		$Validator->required($req,$this->data);
		if ($Validator->validate()){
			if ( $res=merchantApp::validateToken($this->data['mtid'],
			    $this->data['token'],$this->data['user_type'])){
				
			    $params=array(
			      'merchant_id'=>$this->data['mtid'],
				  'enabled_push'=>isset($this->data['enabled_push'])?1:2,
				  'date_modified'=>date('c'),
				  'ip_address'=>$_SERVER['REMOTE_ADDR'],			  
				);		
				
				$DbExt=new DbExt;
				if ( $resp=merchantApp::getDeviceInfo($this->data['merchant_device_id'])){					
					if ( $DbExt->updateData('{{mobile_device_merchant}}',$params,'id',$resp['id'])){
						$this->msg=$this->t("Setting saved");
						$this->code=1;
					} else $this->msg=$this->t("ERROR: Cannot update");
				} else $this->msg=$this->t("Device id not found please restart the app");
								
			} else {
				$this->code=3;
				$this->msg=$this->t("you session has expired or someone login with your account");
			}
		} else $this->msg=merchantApp::parseValidatorError($Validator->getError());	    	
		$this->output();
	}
    
	public function actionGetSettings()
	{		
		if (isset($this->data['device_id'])){
			if ( $resp=merchantApp::getDeviceInfo($this->data['device_id'])){					
				$this->code=1;
				$this->msg="OK";
				$this->details=$resp;
			} else $this->msg=$this->t("Device id not found please restart the app");
		} else $this->msg=$this->t("Device id not found please restart the app");
		$this->output();
	}
	
	public function actiongeoDecodeAddress()
	{
	
		if (isset($this->data['address'])){
			if ($res=Yii::app()->functions->geodecodeAddress($this->data['address'])){
				$this->code=1;
				$this->msg="OK";
				$res['address']=$this->data['address'];
				$this->details=$res;
			} else $this->msg=t("Error: cannot view location");
		} else $this->msg=$this->t("address is required");
		$this->output();
	}
	//
	public function actionOrderHistory()
	{
		if (!isset($this->data['order_id'])){
			$this->msg=$this->t("order is missing");
			$this->output();
		}	
		
		if ( $res=merchantApp::validateToken($this->data['mtid'],
			    $this->data['token'],$this->data['user_type'])){			    	
			 
			 if ( $res=merchantApp::getOrderHistory($this->data['order_id'])){
			 	  $data='';
			 	  foreach ($res as $val) {
			 	  	$data[]=array(
			 	  	  'id'=>$val['id'],
			 	  	  'status'=>t($val['status']),
			 	  	  // 'status_raw'=>strtolower($val['status']),
			 	  	  'remarks'=>$val['remarks'],
			 	  	  'date_created'=>Yii::app()->functions->FormatDateTime($val['date_created'],true),
			 	  	  'ip_address'=>$val['ip_address']
			 	  	);
			 	  }
			 	  $this->code=1;
			 	  $this->msg="OK";
			 	  $this->details=array(
			 	    'order_id'=>$this->data['order_id'],
			 	    'data'=>$data
			 	  );
			 } else {
			 	$this->msg=$this->t("No history found");			    	
			 	$this->details=$this->data['order_id'];
			 }
         } else {
				$this->code=3;
				$this->msg=$this->t("you session has expired or someone login with your account");
				$this->details=$this->data['order_id'];
		}
		$this->output();
	}
	//
	public function actionsaveProfile()
	{
		
		$Validator=new Validator;
		$req=array(
		  'token'=>$this->t("token is required"),
		  'mtid'=>$this->t("merchant id is required"),
		  'user_type'=>$this->t("user type is required"),
		  'contact_name'=>$this->t("name is required"),
		  'contact_email'=>$this->t("email is required"),
		  'contact_phone'=>$this->t("phone is required"),
		);

		if (isset($this->data['password']) && isset($this->data['cpassword'])){
			if ( $this->data['password']!=$this->data['cpassword']){
				$Validator->msg[]=$this->t("Confirm password does not match");
			}
		}
		
		$Validator->required($req,$this->data);
		if ($Validator->validate()){
			if ( $res=merchantApp::validateToken($this->data['mtid'],
			    $this->data['token'],$this->data['user_type'])){

			    $params=array(
			      // 'password'=>md5($this->data['password']),
				  'contact_name'=>$this->data['contact_name'],
				  'contact_email'=>$this->data['contact_email'],
				  'contact_phone'=>$this->data['contact_phone'],
			      'date_modified'=>date('c'),
			      'ip_address'=>$_SERVER['REMOTE_ADDR']
			    );
			    
			    $DbExt=new DbExt;	
			    switch ($res['user_type']) {
			    	case "user":
			    		if ( $DbExt->updateData('{{merchant_user}}',$params,'merchant_user_id',$res['merchant_user_id'])){
			    			$this->code=1;
			    			$this->msg=$this->t("Profile saved");
			    		} else $this->msg=$this->t("ERROR: Cannot update profile");
			    		break;
			    			    	
			    	default:
			    		if ( $DbExt->updateData('{{merchant}}',$params,'merchant_id',$res['merchant_id'])){
			    			$this->code=1;
			    			$this->msg=$this->t("Profile saved");
			    		} else $this->msg=$this->t("ERROR: Cannot update profile");
			    		break;
			    }
			} else {
				$this->code=3;
				$this->msg=$this->t("you session has expired or someone login with your account");
			}
		} else $this->msg=merchantApp::parseValidatorError($Validator->getError());	    	
		$this->output();	    	
	}
	//
	public function actionChangePassword()
	{
		
		$Validator=new Validator;		
		$req=array(
		  'token'=>$this->t("token is required"),
		  'mtid'=>$this->t("merchant id is required"),
		  'user_type'=>$this->t("user type is required"),
		  'password'=>$this->t("password is required"),
		  'cpassword'=>$this->t("confirm password is required")
		);
		
		if (isset($this->data['password']) && isset($this->data['cpassword'])){
			if ( $this->data['password']!=$this->data['cpassword']){
				$Validator->msg[]=$this->t("Confirm password does not match");
			}
		}
		
		$Validator->required($req,$this->data);
		if ($Validator->validate()){
			if ( $res=merchantApp::validateToken($this->data['mtid'],
			    $this->data['token'],$this->data['user_type'])){
					    
			    $params=array(
			      'password'=>md5($this->data['password']),
			      'date_modified'=>date('c'),
			      'ip_address'=>$_SERVER['REMOTE_ADDR']
			    );			    
			    
			    $DbExt=new DbExt;	
			    switch ($res['user_type']) {
			    	case "user":
			    		if ( $DbExt->updateData('{{merchant_user}}',$params,'merchant_user_id',$res['merchant_user_id'])){
			    			$this->code=1;
			    			$this->msg=$this->t("Password saved");
			    		} else $this->msg=$this->t("ERROR: Cannot update your password");
			    		break;
			    			    	
			    	default:
			    		if ( $DbExt->updateData('{{merchant}}',$params,'merchant_id',$res['merchant_id'])){
			    			$this->code=1;
			    			$this->msg=$this->t("Profile saved");
			    		} else $this->msg=$this->t("ERROR: Cannot update password");
			    		break;
			    }
			} else {
				$this->code=3;
				$this->msg=$this->t("you session has expired or someone login with your account");
			}
		} else $this->msg=merchantApp::parseValidatorError($Validator->getError());	    	
		$this->output();	    	
	}
	//
	public function actionGetProfile()
	{
		
		$Validator=new Validator;
		$req=array(
		  'token'=>$this->t("token is required"),
		  'mtid'=>$this->t("merchant id is required"),
		  'user_type'=>$this->t("user type is required"),		  
		);

		$Validator->required($req,$this->data);
		if ($Validator->validate()){
			if ( $res=merchantApp::validateToken($this->data['mtid'],
			    $this->data['token'],$this->data['user_type'])){
			    $this->code=1;
			    $this->msg="OK";
				$res2=merchantApp::getByToken($this->data['mtid'],$this->data['token'],$this->data['user_type']);
				$rating= getOne("rating","merchant_id",$this->data['mtid']);
				$res2["ratings"] = $rating ? $rating['ratings'] : false;
				
			    $this->details=$res2;
	    } else {
				$this->code=3;
				$this->msg=$this->t("you session has expired or someone login with your account");
			}
		} else $this->msg=merchantApp::parseValidatorError($Validator->getError());	    	
		$this->output();	 
	}
	//
	public function actionMerchantReviews()
	{
	
		if (isset($this->data['merchant_id'])){
			if ( $res=Yii::app()->functions->getReviewsList($this->data['merchant_id'])){
				$data='';
				foreach ($res as $val) {
					$prety_date=PrettyDateTime::parse(new DateTime($val['date_created']));
					$data[]=array(
					  'client_name'=>empty($val['client_name'])?$this->t("not available"):$val['client_name'],
					  'review'=>$val['review'],
					  'rating'=>$val['rating'],
					  'date_created'=>Yii::app()->functions->translateDate($prety_date)
					);
				}
				$this->code=1;$this->msg="OK";
				$this->details=$data;
			} else $this->msg=$this->t("no current reviews");
		} else $this->msg=$this->t("Merchant id is missing");
		$this->output();	
	}
	//
	public function actionGetLanguageSettings()
	{
		
		$mobile_dictionary=getOptionA('merchant_mobile_dictionary');
		$mobile_dictionary=!empty($mobile_dictionary)?json_decode($mobile_dictionary,true):false;
		if ( $mobile_dictionary!=false){
			$lang=$mobile_dictionary;
		} else $lang='';
		
		$mobile_default_lang='en';
		$default_language=getOptionA('default_language');
		if(!empty($default_language)){
			$mobile_default_lang=$default_language;
		}	
		
		if ( $mobile_default_lang=="en" || $mobile_default_lang=="-9999")
		{
			$this->details=array(
			  'settings'=>array(
			    //'default_lang'=>"ph"		    
			  ),
			  'translation'=>$lang
			);
		} else {
			$this->details=array(
			  'settings'=>array(
			    'default_lang'=>$mobile_default_lang		    
			  ),
			  'translation'=>$lang
			);
		}
		
		$this->code=1;
		$this->output();		
	}
	//
	public function actiongetNotification()
	{
		
	    $Validator=new Validator;		
		$req=array(
		  'token'=>$this->t("token is required"),
		  'mtid'=>$this->t("merchant id is required"),
		  'user_type'=>$this->t("user type is required"),		  
		);

		$Validator->required($req,$this->data);
		if ($Validator->validate()){
			if ( $res=merchantApp::validateToken($this->data['mtid'],
			    $this->data['token'],$this->data['user_type'])){
			   
			   if ( $resp=merchantApp::getMerchantNotification($res['merchant_id'],
			       $res['user_type'],$res['merchant_user_id'])){
			   		
			       	$data='';
			       	foreach ($resp as $val) {			       		
			       		$val['date_created']=Yii::app()->functions->FormatDateTime($val['date_created'],true);
			       		$data[]=$val;
			       	}
			       	
			       	$this->code=1;
			       	$this->msg="OK";
			       	$this->details=$data;
			       	
			    } else $this->msg=$this->t("no notifications");
			   
             } else {
				$this->code=3;
				$this->msg=$this->t("you session has expired or someone login with your account");
			}
		} else $this->msg=merchantApp::parseValidatorError($Validator->getError());	    	
		$this->output();	 			    	
	}
	
	public function actionsearchOrder()
	{
		$Validator=new Validator;		
		$req=array(
		  'token'=>$this->t("token is required"),
		  'mtid'=>$this->t("merchant id is required"),
		  'user_type'=>$this->t("user type is required"),		  
		);
						
		$Validator->required($req,$this->data);
		if ($Validator->validate()){
			if ( $res=merchantApp::validateToken($this->data['mtid'],
			    $this->data['token'],$this->data['user_type'])){			    			    
			   			    
			    if ( $resp=merchantApp::searchOrderByMerchantId(
			    $this->data['order_id_customername'] , $this->data['mtid'])){
			    	 
			    	$this->code=1; $this->msg="OK";					
					foreach ($resp as $val) {												
						$data[]=array(
						  'order_id'=>$val['order_id'],
						  'viewed'=>$val['viewed'],
						  'status'=>t($val['status']),
						  'status_raw'=>strtolower($val['status']),
						  'trans_type'=>t($val['trans_type']),
						  'trans_type_raw'=>$val['trans_type'],
						  'total_w_tax'=>$val['total_w_tax'],						  
						  'total_w_tax_currency'=>merchantApp::prettyPrice($val['total_w_tax']),
						  'transaction_date'=>Yii::app()->functions->FormatDateTime($val['date_created'],true),
						  'transaction_time'=>Yii::app()->functions->timeFormat($val['date_created'],true),
						  'delivery_time'=>Yii::app()->functions->timeFormat($val['delivery_time'],true),
						  'delivery_asap'=>$val['delivery_asap']==1?t("ASAP"):''
						);
					}					
					$this->code=1;
					$this->msg=$this->t("Search Results") ." (".count($data).") ".$this->t("Found records");
					$this->details=$data;
			    	 
			    } else $this->msg=$this->t("no results");
			   
             } else {
				$this->code=3;
				$this->msg=$this->t("you session has expired or someone login with your account");
			}
		} else $this->msg=merchantApp::parseValidatorError($Validator->getError());	    	
		$this->output();	 			 
	}
	
	
} /*end class*/