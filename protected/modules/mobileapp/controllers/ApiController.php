<?php
class ApiController extends CController
{	
	public $data;
	public $code=2;
	public $msg='';
	public $details='';
	public $radius="";
	public $is_featured=false;
	
	public function __construct()
	{
		// $this->data=$_GET;
		$this->data=$_POST;	
		if (isset($_GET['post_type'])){
			if ($_GET['post_type']=="get"){
				$this->data=$_GET;	
			}
		}
		
		$website_timezone=Yii::app()->functions->getOptionAdmin("website_timezone");		 
	    if (!empty($website_timezone)){
	 	   Yii::app()->timeZone=$website_timezone;
	    }		 
	}
	
	public function beforeAction($action)
	{
		/*check if there is api has key*/		
		$action=Yii::app()->controller->action->id;				
		if(isset($this->data['api_key'])){
			if(!empty($this->data['api_key'])){			   
			   $continue=true;
			   if($action=="getLanguageSettings" || $action=="registerMobile"){
			   	  $continue=false;
			   }
			   if($continue){
			   	   // $key=getOptionA('mobileapp_api_has_key');
				   // if(trim($key)!=trim($this->data['api_key'])){
				   	 // $this->msg=$this->t("api hash key is not valid");
			         // $this->output();
			         // Yii::app()->end();
				   // }
			   }			
			}
		}		
		return true;
	}	
	
	public function actionIndex(){
		//throw new CHttpException(404,'The specified url cannot be found.');
	}		
	
	private function q($data='')
	{
		return Yii::app()->db->quoteValue($data);
	}
	
	private function t($message='')
	{
		return Yii::t("default",$message);
	}

    private function output()
    {
		if(isset($this->data['cart'])) {
			$this->data['cart'] = "";
		}
	   $resp=array(
	     'code'=>$this->code,
	     'msg'=>$this->msg,
	     'details'=>$this->details,
	     'request'=>$this->data
	   );
	   if (isset($this->data['debug'])){
	   	   dump($resp);
	   }
	   // var_dump($resp);
	   if (!isset($_GET['callback'])){
  	   	   $_GET['callback']='';
	   }
	   // echo "tes";
	   
	   echo json_encode($resp,JSON_UNESCAPED_SLASHES);
	   // if (isset($_GET['json']) && $_GET['json']==TRUE){
	   	   // echo CJSON::encode($resp);
	   // } else echo $_GET['callback'] . '('.CJSON::encode($resp).')';
	   Yii::app()->end();
    }	
	//output versi 2
    private function out($code,$details,$msg)
    {
		if(isset($this->data['cart'])) {
			$this->data['cart'] = "";
		}
		
	   $resp=array(
	     'code'=>$code,
	     'msg'=>$this->t($msg),
	     'details'=>$details,
	     'request'=>$this->data
	   );
	   if (isset($this->data['debug'])){
	   	   dump($resp);
	   }
	   if (!isset($_GET['callback'])){
  	   	   $_GET['callback']='';
	   }
	   echo json_encode($resp,JSON_UNESCAPED_SLASHES);

	   Yii::app()->end();
    }	
	
	public function actionSearchMerchant($with_product = true)
	{
		
		if (isset($_GET['debug'])){
			dump($this->data);
		}
		
		// if ( !empty($this->data['address'])){
			 // if ( $res_geo=Yii::app()->functions->geodecodeAddress($this->data['address'])){
				 
				$home_search_unit_type=Yii::app()->functions->getOptionAdmin('home_search_unit_type');			 	
				$distance_exp=3959;
				if ($home_search_unit_type=="km"){
					$distance_exp=6371;
				}
				
				
				//penentuan latitude longitude

				$sql_near_location = "";
				$having_near_location = "";
				
				
				
				session_start();
				if(!empty($this->data['latitude']) && !empty($this->data['longitude'])) {
					//request from mobile
					$lat=$this->data['latitude'];
					$long=$this->data['longitude'];
				} elseif(isset($_SESSION['client_location']['lat']) && isset($_SESSION['client_location']['long'])) {
					$lat=$_SESSION['client_location']['lat'];
					$long=$_SESSION['client_location']['long'];
				}
				
				if(!empty($lat) && !empty($long)) {

					//request from mobile
				} elseif(!empty($this->data['address'])) {
					
					$res_geo=Yii::app()->functions->geodecodeAddress($this->data['address']);
					if($res_geo) {
						$lat=$res_geo['lat'];
						$long=$res_geo['long'];
					} else {
						$lat=-6.209801106813812;
						$long=106.78606855117187;
					}
					$this->data['latitude'] = $lat;
					$this->data['longitude'] = $long;

				} else {
					//jika tidak menggunakan latitude, maka menggunakan search address
					//jika kedua input parameter tsb tidak didapat maka posisi default 
					//di tengah2 jakarta
					$lat=-6.209801106813812;
					$long=106.78606855117187;
				}
				
				$sql_near_location = ", 
								 ( $distance_exp * acos( cos( radians($lat) ) * cos( radians( latitude ) ) 
								 * cos( radians( longitude ) - radians($long) ) 
								 + sin( radians($lat) ) * sin( radians( latitude ) ) ) ) 
								 AS distance ";

				if(isset($this->data['location'])) {
					if($this->data['location'] == 'current') {
						$home_search_radius = 40;
					} else {
						$home_search_radius = 5000;
					}
				} else {
					$home_search_radius = 5000;
				}
				//variabel radius terisi ketika digunakan fungsi nearme
				if(!empty($this->radius)) {
					$home_search_radius = $this->radius;
				}
				
				$DbExt=new DbExt; 
				$DbExt->qry("SET SQL_BIG_SELECTS=1");
				
			 	
				$total_records=0;
				$data="";
				
				$c = Yii::app()->db->createCommand();
				$c->select('m.merchant_id,m.merchant_name,m.merchant_slug,m.street,m.city,m.state,m.post_code,m.latitude,m.longitude,SUM(i.wholesale_flag) wholesale_sum '.$sql_near_location);
				$c->from('{{view_merchant}} m');
				
				// $c->leftJoin('{{wholesale_price}} w','m.item_id=w.item_id');
				
				if(isset($home_search_radius)) {
					$c->having('distance < :home_search_radius',array(':home_search_radius'=>$home_search_radius));
				}
				
				if(!empty($this->data['sortby']) && in_array($this->data['sortby'],array('date_created'))) {
					$c->order("m.".$this->data['sortby'].' ASC');
				} else {
					$c->order('distance ASC');
				}
				$c->where('1');
				if(!empty($this->data['keyword'])) {
					$c->andWhere(array('like', 'm.merchant_name', '%'.$this->data['keyword'].'%'));
				}
				//isfeatured untuk home screen
				if($this->is_featured) {
					$c->andWhere('m.is_featured=2');
				}
				//filter kategori dan satuan perlu pengecekan di dalam item suatu merchant (kedalaman)
				// if(!empty($this->data['category']) || !empty($this->data['satuan'])) {

					$c->leftJoin('{{item}} i','m.merchant_id=i.merchant_id');
					$c->group('m.merchant_id');
					// $c->where('i.category=:category', array(':category'=>$this->data['category']));
					
					if(!empty($this->data['category'])) {
						$c->andWhere('i.category=:category',array(':category'=>$this->data['category']));
					}
					if(!empty($this->data['satuan'])) {
						if($this->data['satuan'] == "grosir"){
							$c->andWhere('i.wholesale_flag=:cat',array(':cat'=>1));
						}
						if($this->data['satuan'] == "eceran"){
							$c->andWhere('i.wholesale_flag=:cat',array(':cat'=>0));
						}
					}
				
				// }
				
				if(!empty($this->data['city'])) {
					$c->andWhere(array('like', 'm.city', '%'.$this->data['city'].'%'));
				}
				if(!empty($this->data['page']) || !empty($this->data['limit'])) {
					$c->limit($this->data['limit'],($this->data['page']-1)*$this->data['limit']);
				}
				$data_merchant = $c->queryAll();
				// var_dump($r);
				
			 	if (isset($_GET['debug'])){
			 	   dump($stmt);
			 	}

					
				//AMBIL DATA PRODUK DULU BARU AMBIL DATA MERCHANT DARI DATABASE
				
				//DATA PRODUK
				//karena output produk dan merchant digabung jadi 
				//call fungsi output produknya\
				$data_product = array();

				if(isset($this->data['with_product'])) {
					//request from mobile
					if($this->data['with_product'] == "true") {
						$data_product = $this->actionSearchProduct(true);
					}
				} else {
					//request not from mobile
					if($with_product) {
						$data_product = $this->actionSearchProduct(true);
					}
				}
				
				if ($data_merchant || $data_product){
					
					$total_records = count($data_merchant)+count($data_product);
			 		foreach ($data_product as $val) {

			 			// $minimum_order=getOption($val['merchant_id'],'merchant_minimum_order');
			 			// if(!empty($minimum_order)){
				 			// $minimum_order=displayPrice(getCurrencyCode(),prettyFormat($minimum_order));		 			
			 			// }
						$data[]=array(
						  'merchant_id'=>$val['merchant_id'],
						  'merchant_name'=>$val['item_name'],
						  'merchant_slug'=>$val['merchant_slug'],
						  'address'=>$val['street']." ".$val['city']." ".$val['state'],
						  'address2'=>$val['street']." ".$val['city'],
						  'ratings'=>array('ratings'=>0,'votes'=>0),
						  'minimum_order'=>0,
						  'wholesale_flag'=>(int) $val['wholesale_flag'],
						  'logo'=>$val['photo'],
						  'map_coordinates'=>array(
							'latitude'=>!empty($val['latitude'])?$val['latitude']:'',
							'longitude'=>!empty($val['longitude'])?$val['longitude']:'',
						  ),
						  'distance'=>number_format((float)$val['distance'], 2, ',', ' ')." km",
						  'is_product'=>true,
						  'label1'=>$val['item_name'],
						  'label2'=>"Eceran ".AddonMobileApp::prettyPrice($val['retail_price']),
						  'label3'=>$val['city'],
						  'label4'=>$val['merchant_name'],
						  'label5'=>"",
						);
			 		}
			 		
					//DATA MERCHANT
			 		foreach ($data_merchant as $val) {

			 			// $minimum_order=getOption($val['merchant_id'],'merchant_minimum_order');
			 			// if(!empty($minimum_order)){
				 			// $minimum_order=displayPrice(getCurrencyCode(),prettyFormat($minimum_order));		 			
			 			// }
				        $ratings = Yii::app()->functions->getRatings($val['merchant_id']);
						$data[]=array(
						  'merchant_id'=>$val['merchant_id'],
						  'merchant_name'=>$val['merchant_name'],
						  'merchant_slug'=>$val['merchant_slug'],
						  'address'=>$val['street']." ".$val['city']." ".$val['state'],
						  'address2'=>$val['street']." ".$val['city'],
						  'ratings'=>Yii::app()->functions->getRatings($val['merchant_id']),
						  'minimum_order'=>0,
						  'wholesale_flag'=>($val['wholesale_sum']>0)?1:0,
						  'logo'=>AddonMobileApp::getMerchantLogo($val['merchant_id']),
						  'map_coordinates'=>array(
							'latitude'=>!empty($val['latitude'])?$val['latitude']:'',
							'longitude'=>!empty($val['longitude'])?$val['longitude']:'',
						  ),
						  'distance'=>number_format((float)$val['distance'], 2, ',', ' ')." km",
						  'is_product'=>false,
						  'label1'=>$val['merchant_name'],
						  'label2'=>$val['street']." ".$val['city'],
						  'label3'=>"",
						  'label4'=>$val['city'],
						  'label5'=>round($ratings['ratings']),
						);
			 		}


					
			 		$this->code=1;
			 		$this->msg=$this->t("Successful");
			 		$this->details=array(
			 		  'total'=>$total_records,
			 		  'data'=>$data,
			 		);
			 		
			 	} else $this->msg=$this->t("Data not found");
			 // } else $this->msg=$this->t("Error has occured failed geocoding address");
		// } else $this->msg=$this->t("Address is required");
		$this->output();
	}
	
	public function actionNearMe() {
		$this->radius = 30000;
		$this->actionSearchMerchant(false);
	}
	public function actionHomeMerchant() {
		$this->radius = 30000;
		$this->is_featured = true;
		$this->actionSearchMerchant(false);
	}
	public function actionSearchProduct($return_data_only = false)
	{
		//return_data_only digunakan jika hanya return variabel hasilnya saja
		//tida perlu print output 
		if (isset($_GET['debug'])){
			dump($this->data);
		}
				$home_search_unit_type=Yii::app()->functions->getOptionAdmin('home_search_unit_type');			 	
				$distance_exp=3959;
				if ($home_search_unit_type=="km"){
					$distance_exp=6371;
				}
				
				if(!empty($this->data['latitude']) && !empty($this->data['longitude'])) {
					//request from mobile
					$lat=$this->data['latitude'];
					$long=$this->data['longitude'];
				} elseif(isset($_SESSION['client_location']['lat']) && isset($_SESSION['client_location']['long'])) {
					$lat=$_SESSION['client_location']['lat'];
					$long=$_SESSION['client_location']['long'];
				}
				
				if(!empty($lat) && !empty($long)) {
					// vdump("aaa");
					//request from mobile
				} elseif(!empty($this->data['address'])) {
					// vdum("www");
					$res_geo=Yii::app()->functions->geodecodeAddress($this->data['address']);
					if($res_geo) {
						$lat=$res_geo['lat'];
						$long=$res_geo['long'];
					} else {
						$lat=-6.209801106813812;
						$long=106.78606855117187;
					}
					$this->data['latitude'] = $lat;
					$this->data['longitude'] = $long;

				} else {
					//jika tidak menggunakan latitude, maka menggunakan search address
					//jika kedua input parameter tsb tidak didapat maka posisi default 
					//di tengah2 jakarta
					$lat=-6.209801106813812;
					$long=106.78606855117187;
				}
				
				$sql_near_location = ", 
								 ( $distance_exp * acos( cos( radians($lat) ) * cos( radians( latitude ) ) 
								 * cos( radians( longitude ) - radians($long) ) 
								 + sin( radians($lat) ) * sin( radians( latitude ) ) ) ) 
								 AS distance ";
								 
				if(isset($this->data['location'])) {
					if($this->data['location'] == 'current') {
						$home_search_radius = 40;
					} else {
						$home_search_radius = 5000;
					}
				} else {
					$home_search_radius = 5000;
				}
				
				
				$DbExt=new DbExt; 
				$DbExt->qry("SET SQL_BIG_SELECTS=1");
				
			 	
				$total_records=0;
				$data='';
				
				$c = Yii::app()->db->createCommand();
				$c->select('i.wholesale_flag,i.item_id,i.item_name,i.item_description,i.retail_price,i.photo,i.category,i.wholesale_flag,m.merchant_id,m.merchant_name,m.merchant_slug,m.street,m.city,m.state,m.post_code,m.latitude,m.longitude'.$sql_near_location);
				$c->from('{{view_merchant}} m');
				
				if(isset($sql_near_location)) {
					$c->order('distance ASC');
				}
				
				if(isset($home_search_radius)) {
					$c->having('distance < :home_search_radius',array(':home_search_radius'=>$home_search_radius));
				}
				$c->join('{{item}} i','m.merchant_id=i.merchant_id');
				$c->group('i.item_id');
					// echo Yii::getVersion();
				if(!empty($this->data['sortby']) && in_array($this->data['sortby'],array('date_created','price','views'))) {
					$this->data['sortby'] = ($this->data['sortby'] =='price') ? "retail_price" : $this->data['sortby'];
					$c->order("i.".$this->data['sortby'].' ASC');
				} else {
					$c->order('distance ASC');
				}
				$c->where('1');
				
				if(!empty($this->data['keyword'])) {
					$c->andWhere(array('like', 'i.item_name', '%'.$this->data['keyword'].'%'));
				}
				if(!empty($this->data['category'])) {
					$c->andWhere('i.category=:category',array(':category'=>$this->data['category']));
				}
				if(!empty($this->data['is_featured']) ) {
					if($this->data['is_featured'] == 'yes'){
						$c->andWhere('i.is_featured=1');
					}
				}
				if(!empty($this->data['city'])) {
					$c->andWhere(array('like', 'm.city', '%'.$this->data['city'].'%'));
				}
				if(!empty($this->data['satuan'])) {
					if($this->data['satuan'] == "grosir"){
						$c->andWhere('i.wholesale_flag=:cat',array(':cat'=>1));
					}
					if($this->data['satuan'] == "eceran"){
						$c->andWhere('i.wholesale_flag=:cat',array(':cat'=>0));
					}
				}
				
				if(!empty($this->data['page']) || !empty($this->data['limit'])) {
					$c->limit($this->data['limit'],($this->data['page']-1)*$this->data['limit']);
				}
				$r = $c->queryAll();
				// var_dump($r);
				
			 	if (isset($_GET['debug'])){
			 	   dump($stmt);	
			 	}
			 	if ($r){
					$total_records = count($r);
					
			 		$this->code=1;
			 		$this->msg=$this->t("Successful");
			 		
			 		foreach ($r as $val) {
			 			$minimum_order=getOption($val['merchant_id'],'merchant_minimum_order');
			 			if(!empty($minimum_order)){
				 			$minimum_order=displayPrice(getCurrencyCode(),prettyFormat($minimum_order));		 			
			 			}
				        $base_url = Yii::app()->getBaseUrl(true);
						$data[]=array(
						  'item_id'=>$val['item_id'],
						  'item_name'=>$val['item_name'],
						  'item_description'=>$val['item_description'],
						  'retail_price'=>$val['retail_price'],
						  'retail_price_currency'=>AddonMobileApp::prettyPrice($val['retail_price']),
						  'photo'=>$base_url."/upload/".$val['photo'],
						  'satuan'=>$val['wholesale_flag']?'grosir':'eceran',
						  'wholesale_flag'=>$val['wholesale_flag'],
						  'category'=>$val['category'],
						  'merchant_id'=>$val['merchant_id'],
						  'merchant_name'=>$val['merchant_name'],
						  'merchant_slug'=>$val['merchant_slug'],
						  'address'=>$val['street'].", ".$val['city'].", ".$val['state'],
						  'address2'=>$val['street']." ".$val['city'],
						  'city'=>$val['city'],
						  'street'=>$val['street'],
						  'state'=>$val['state'],
						  // 'ratings'=>Yii::app()->functions->getRatings($val['merchant_id']),
						  'minimum_order'=>$minimum_order,
						  // 'logo'=>AddonMobileApp::getMerchantLogo($val['merchant_id']),
						  'map_coordinates'=>array(
							'latitude'=>!empty($val['latitude'])?$val['latitude']:'',
							'longitude'=>!empty($val['longitude'])?$val['longitude']:'',
						  ),
						  'distance'=>isset($val['distance'])?(number_format($val['distance'], 2, ',', ' ')." km"):"",
						);
			 		}

			 		$this->details=array(
			 		  'total'=>$total_records,
			 		  'data'=>$data
			 		);
			 		
			 	} else $this->msg=$this->t("No product found");

		if($return_data_only){
			return $data;
		} else {
			$this->output();
		}
	}
	//
	public function actionMerchantCategory()
	{	
		$data='';	
		if (!isset($this->data['merchant_id'])){
			$this->msg=$this->t("Merchant id is missing");
			$this->output();
		}
		if ( $data = AddonMobileApp::merchantInformation($this->data['merchant_id'])){				
 			if($data['category']=Yii::app()->functions->getCategoryList3($this->data['merchant_id'])){
 			  $data['has_category']=2;
 			} else $data['has_category']=1;
 			 			
 			$trans=getOptionA('enabled_multiple_translation'); 			
 			if ( $trans==2 && isset($_GET['lang_id'])){
 				$new='';
	 			if (AddonMobileApp::isArray($data['category'])){
	 				foreach ($data['category'] as $val) {	 					
	 					$val['category_name']=AddonMobileApp::translateItem('category',
	 					$val['category_name'],$val['cat_id']);
	 					$new[]=$val;
	 				}
	 				
	 				unset($data['category']);
	 				$data['category']=$new;
	 			}			 			
 			} 			
 			
 			$this->code=1;
			$this->msg=$this->t("Successful");			
			$this->details=$data;			
		} else $this->msg=$this->t("Restaurant not found");
				
		$this->output();
	}
	public function actionCategory()
	{	
		$data=array();
		$data['category'] = queryO("SELECT * FROM {{category}} WHERE status='publish'");

		$trans=getOptionA('enabled_multiple_translation'); 			
		if ( $trans==2 && isset($_GET['lang_id'])){
			$new='';
			if (AddonMobileApp::isArray($data['category'])){
				foreach ($data['menu_category'] as $val) {	 					
					$val['category_name']=	AddonMobileApp::translateItem('category',
											$val['category_name'],$val['cat_id']);
					$new[]=$val;
				}
				unset($data['category']);
				$data['category']=$new;
			}
		}
		
		$this->out(1,$data,"Successful");
	}
	//
	public function actionGetItemByCategory()
	{
		if (!isset($this->data['cat_id'])){
			$this->msg=$this->t("Category is is missing");
			$this->output();
		}
		if (!isset($this->data['merchant_id'])){
			$this->msg=$this->t("Merchant Id is is missing");
			$this->output();
		}
		
		$disabled_ordering=getOption($this->data['merchant_id'],'merchant_disabled_ordering');		
		
		if ($res=Yii::app()->functions->getItemByCategory($this->data['cat_id'],$this->data['merchant_id'])){
			
			for ($i=0;$i<count($res);$i++) {
				$res[$i]['photo'] = AddonMobileApp::getImage($res[$i]['photo']);
			}
			/*dump($item);
			die();*/
									
			$this->code=1;
			$this->msg=$this->t("Successful");						
			$merchant_info= AddonMobileApp::merchantInformation($this->data['merchant_id']);
			$category_info=Yii::app()->functions->getCategory($this->data['cat_id']);			
			$this->details=array(
			  'disabled_ordering'=>$disabled_ordering=="yes"?2:1,
			  'image_path'=>websiteUrl()."/upload",
			  'default_item_pic'=>'mobile-default-logo.png',
			  'merchant_info'=>$merchant_info,
			  'category_info'=>$category_info,
			  'item'=>$res
			);
		} else {
			$this->msg=t("No food item found");
			$category_info=Yii::app()->functions->getCategory($this->data['cat_id']);
			$merchant_info= AddonMobileApp::merchantInformation($this->data['merchant_id']);
			$this->details=array(
			  'merchant_info'=>$merchant_info,
			  'category_info'=>$category_info,
			);
		}
		$this->output();
	}
	//
	public function actionGetItemDetails()
	{
		if (!isset($this->data['item_id'])){
			$this->msg=$this->t("Item id is missing");
			$this->output();
		}

		if ( $res=Yii::app()->functions->getItemById2($this->data['item_id'])){			
			$data=$res[0];
			// var_dump($data);
			$data['photo']=AddonMobileApp::getImage($data['photo']);

			if (!empty($data['item_description'])){
			   $data['item_description']=strip_tags($data['item_description']);		
			}
			
			//die();

			$gallery_list='';
			$data['has_gallery']=1;
			if (!empty($data['gallery_photo'])){
				$gallery_photo=json_decode($data['gallery_photo']);
				if(is_array($gallery_photo) && count($gallery_photo)>=1){
					foreach ($gallery_photo as $pic) {
						$gallery_list[]=AddonMobileApp::getImage($pic);
					}
					$data['gallery_photo']=$gallery_list;
					$data['has_gallery']=2;
				}
			}
			
			$data['currency_code']=Yii::app()->functions->adminCurrencyCode();
			$data['currency_symbol']=getCurrencyCode();
						
			$this->code=1;
			$this->msg="OK";
			$this->details=$data;
		} else $this->msg=$this->t("Item not found");
		$this->output();
	}
	//
	public function actionLoadCart()
	{
		//dump($this->data);
		if (!isset($this->data['cart'])){
			$this->msg=$this->t("cart is missing");
			$this->output();
		}
		if (!isset($this->data['search_address'])){
			$this->msg=$this->t("search address is is missing");
			$this->output();
		}
				
		if ($this->data['transaction_type']=="null" || empty($this->data['transaction_type'])){
			$this->data['transaction_type']="express";
		}
		
		if (!isset($this->data['delivery_date'])){
			$this->data['delivery_date']='';
		}	
		if ($this->data['delivery_date']=="null" || empty($this->data['delivery_date'])){
			$this->data['delivery_date']=date("Y-m-d");
		}
		
		$mtid=$this->data['merchant_id'];
	    $merchant_info= AddonMobileApp::merchantInformation($mtid);
	    
	    
		$cart_content='';
		$subtotal=0;
		$taxable_total=0;
		
		Yii::app()->functions->data="list";
		
		$item_total=0;
		
		/*pts*/
		$weight_total = 0;
		$list_merchant = array();
		$tax_merchants = array();
		if(!empty($this->data['cart'])){			
			$cart=json_decode($this->data['cart'],true);
			//dump($cart);
			if (is_array($cart) && count($cart)>=1){
				
			    foreach ($cart as $val) {

			    	$item_price=0;
			    	$item_size='';
			    
					//price decision retail price or wholesal price
			    	$food=Yii::app()->functions->getFoodItem($val['item_id']);
					$item_price=$food['retail_price'];
					
					$wholesale_price = getWhere('wholesale_price','item_id',$val['item_id']);
					foreach($wholesale_price as $w) {
						if($qty >= $w['start'] && $qty <= $w['end']) {
							$item_price = $w['price'];
						}
					}
					

					
			    	/*check if item qty is less than 1*/
			    	if($val['qty']<1){
			    		$val['qty']=1;
			    	}

			    	$discounted_price=0;
			    	if ($val['discount']>0){
			    		$discounted_price=$item_price-$val['discount'];
			    		$subtotal+=($val['qty']*$discounted_price);
			    	} else {
			    		$subtotal+=($val['qty']*$item_price);
			    	}

			    	$item_total+=$val['qty'];

			    	$discount_amt=0;
			    	if (isset($val['discount'])){
			    		$discount_amt=$val['discount'];
			    	}
					
					$total_per_item = $val['qty']*($item_price-$discount_amt);

			    	//get merchants list from product
					$list_merchant[] = $food['merchant_id'];
					
					//kalkulasi tax untuk setiap merchant
					//sesuai produknya masing2
					if(isset($tax_merchants[$food['merchant_id']])) {
						//jika key merchant_id sudah diset
						$tax_merchants[$food['merchant_id']] += $food['tax'] * $total_per_item / 100;
					} else {
						//jika key merchant_id belum diset
						$tax_merchants[$food['merchant_id']] = $food['tax'] * $total_per_item / 100;
					}
					// var_dump($item_price);
			    	$cart_content[]=array(
			    	  'item_id'=>$val['item_id'],
			    	  'item_name'=>$food['item_name'],
			    	  'item_description'=>$food['item_description'],
			    	  'qty'=>$val['qty'],
			    	  'price'=>$item_price,
			    	  'price_currency'=>AddonMobileApp::prettyPrice($item_price),
			    	  'total'=>$val['qty']*($item_price-$discount_amt),
			    	  'total_currency'=>AddonMobileApp::prettyPrice($val['qty']* ($item_price-$discount_amt) ),
			    	  'discount'=>isset($val['discount'])?$val['discount']:'',
			    	  'discounted_price'=>$discounted_price,
			    	  'discounted_price_currency'=>AddonMobileApp::prettyPrice($discounted_price),
			    	  'order_notes'=>$val['order_notes'],
					  'uom'=>$food['uom'],
					  'weight_per_unit'=>$food['weight']?$food['weight']:500,
					  'weight_total'=>$food['weight']?$food['weight']*$val['qty']:(500*$val['qty']),
			    	);
					$weight_total += $food['weight']?$food['weight']*$val['qty']:(500*$val['qty']);
			    } /*end foreach*/
			    // var_dump($list_merchant);
			    			    
			    $ok_distance=2;
			    $delivery_charges=0;
			    $distance='';
			    
			    if ( $this->data['transaction_type']=="express" || $this->data['transaction_type']=="economy"){

					$ret_delivery = AddonMobileApp::calculateDeliveryCharges($list_merchant,$this->data,$weight_total,$this->data['transaction_type']);
					$distance=array(
						'unit'=>$ret_delivery['distance_type'],
						'distance'=>$ret_delivery['distance'],
						'distance_is_valid'=>$ret_delivery['valid_distance'],
					);
					$delivery_charges=$ret_delivery['delivery_fee'];
					// var_dump($ret_delivery);
					// if(!$ret_delivery['valid_distance']){
						// $ok_distance = 0;
					// }

			    }


				$cart_final_content=array(
				  'cart'=>$cart_content,
				  'sub_total'=>$subtotal,
				  'sub_total_currency'=>AddonMobileApp::prettyPrice($subtotal),
				);
								

				if ($delivery_charges>0){
					$cart_final_content['delivery_charge'] = $delivery_charges;
					$cart_final_content['delivery_charge_currency'] = AddonMobileApp::prettyPrice($delivery_charges);
				}
				
				$tax=0;
				foreach($tax_merchants as $t) {
					$tax += $t;
				}
				$cart_final_content['tax']=$tax;
				$cart_final_content['tax_currency']=AddonMobileApp::prettyPrice($tax);
				
				$cart_final_content['weight_total']=$weight_total;
				$grand_total=$subtotal+$delivery_charges+$tax;
				$cart_final_content['total']=$grand_total;
				$cart_final_content['total_currency']=AddonMobileApp::prettyPrice($grand_total);
				
				/*validation*/																
				$validation_msg='';
				if ($ok_distance==0) {
					$validation_msg=t("Sorry but the address is not found in the map, please use other familiar address.");
				}

				/*if(!$is_merchant_open = Yii::app()->functions->isMerchantOpen($mtid)){					
					$merchant_preorder=getOption($mtid,'merchant_preorder');
					if($merchant_preorder==1){
						$is_merchant_open=true;
					}				
				}			
				
				if (!$is_merchant_open){
					$validation_msg=$this->t("Sorry merchant is closed");
				}*/	
				
				$required_time=getOption( $mtid ,'merchant_required_delivery_time');
				$required_time=$required_time=="yes"?2:1;
				
				
			    $this->code=1;
			    $this->msg="OK";
			    $this->details=array(
			      'validation_msg'=>$validation_msg,
			      'transaction_type'=>$this->data['transaction_type'],
			      'delivery_date'=>$this->data['delivery_date'],
			      'delivery_time'=>isset($this->data['delivery_time'])?$this->data['delivery_time']:'',
			      'required_time'=>$required_time,
			      'currency_symbol'=>getCurrencyCode(),
			      'cart'=>$cart_final_content,	
			    );
			    if (AddonMobileApp::isArray($distance)){
			    	$this->details['distance']=$distance;
			    }
			} else $this->msg=$this->t("cart is empty");
		} else $this->msg=$this->t("cart is empty");
		
		if($this->code==2){
			$this->details=array(
			  'cart_total'=>displayPrice(getCurrencyCode(),prettyFormat(0)),
			);
		}
		$this->output();
	}

	//
	public function actionGetEconomyDriver()
	{		
		if (!isset($this->data['cart'])){
			$this->msg=$this->t("List item id is missing");
			$this->output();
		}
		if (!isset($this->data['latitude']) || !isset($this->data['longitude'])){
			$this->msg=$this->t("We need your location, please input latitude and longitude");
			$this->output();
		}
		
		//1 mendapatkan cart => list_item_id kemudian dikonvert menjadi lokasi2 merchant nya
		//2 dari lokasi2 merchant 
		//(lokasi customer tidak perlu dipertimbangkan karena driver 
		//pasti membawa barang belanjaan dulu dari merchant2 kemudian 
		//menuju customer)
		//3 hitung jarak driver2 yang terdekat (dihitung dari region driver, bukan dari lokasi driver saat ini) 
		//  dan available pada hari itu 
		//  ada maksimum distancenya (sebutlah maksium distance 40km dari merchant)
		//4 jika tidak ada yang di dalam radius maka output response tidak ada driver yang tersedia 
		//  pada jarak tersebut
		//	jika ada driver maka return driver tsb
		
		$weight_total = 0;
		$cart = json_decode($this->data['cart']);
		foreach($cart as $c) {
			//hitung weight total
			$res = query("SELECT weight FROM {{item}} WHERE item_id=?",array($c->item_id));
			foreach($res as $food_info){
				$weight_total += $food_info['weight']?$food_info['weight']*$c->qty:(500*$c->qty);
			}
			$list_item_id[] = $c->item_id;
		}
		$list_item_id = implode(',',$list_item_id);
		//1
		// var_dump($list_item_id);
		$merchants = query("SELECT m.latitude,m.longitude,m.merchant_id FROM {{item}} i 
					JOIN {{view_merchant}} m 
					ON i.merchant_id=m.merchant_id 
					WHERE i.item_id IN($list_item_id) GROUP BY m.merchant_id",array());
		// var_dump($merchants);
		//metode region
		$distance_exp=6371;

		$drivers = array();
		$big_drivers = array();
		$day_number = date('N'); // output: current day.
		$day_number--;
		$motor_max_weight = Yii::app()->functions->getOptionAdmin('motor_max_weight');

		// var_dump($day_number);
		foreach($merchants as $m) {
			$DbExt=new DbExt; 
			$DbExt->qry("SET SQL_BIG_SELECTS=1");
			$total_records=0;
			$data='';
			$c = Yii::app()->db->createCommand();
			$lat = $m['latitude'];
			$long = $m['longitude'];
			//get near driver
			if(!empty($lat) && !empty($long)) {
				$sql_near_location = ", 
								 ( $distance_exp * acos( cos( radians($lat) ) * cos( radians( location_lat ) ) 
								 * cos( radians( location_lng ) - radians($long) ) 
								 + sin( radians($lat) ) * sin( radians( location_lat ) ) ) ) 
								 AS distance ";
				$radius = 40;
				$c->select('d.driver_id,d.first_name,d.location_lat,d.location_lng,d.region,d.pickupstandby
							'.$sql_near_location);
				$c->from('{{driver}} d');
				$c->having('distance < :home_search_radius',array(':home_search_radius'=>$radius));		
				
				// $c->join('{{driver_economy}} e','d.driver_id=e.driver_id');
				$c->group('d.driver_id');
				$c->where('1');
				
				//validasi motor/mobil (weight dalam gram, motor_max_weight dalam kg)
				if($weight_total/1000 > $motor_max_weight) {
					//berat order > beban maksimal motor maka pakai mobil
					$c->andWhere("d.transport_type_id LIKE '%mobil%'");
				} else {
					//beban masih jangkauan motor
					$c->andWhere("d.transport_type_id LIKE '%motor%'");
				}
				//ambil driver yang hari ini aktif di driver ekonomy
				// $c->andWhere('e.day= :day_number',array(':day_number'=>$day_number));
				// $c->andWhere('e.available=1');
				// $c->limit($this->data['limit'],($this->data['page']-1)*$this->data['limit']);
				$r = $c->queryAll();
				// var_dump($r);
				//save driver data for future
				$big_drivers[] = $r;
				
				//ambil yang jaraknya terdekat antara driver dengan salah satu merchant
				foreach($r as $s) {
					// var_dump($s);
					if(isset($driver[$s['driver_id']])) {
						//jika jarak berikutnya lebih kecil maka ambil yang lebih kecil
						if($driver[$s['driver_id']] > $s['distance']) {
							$driver[$s['driver_id']] = $s['distance'];
						}
					} else {
						$driver[$s['driver_id']] = $s['distance'];
					}
				}
			}

		}
		
		//didapatkan jarak total dari suatu driver ke seluruh merchant
		asort($driver);
		//ambil 7 terbaik
		$driver = array_slice($driver, 0, 10,true); 
		
		$hour = query("SELECT * FROM {{driver_hour}}");
		
		//gunakan data query sebelumnya ($big_drivers)
		//utk meminimalkan query ke database
		$active_hour = array('00:00','03:00','06:00','09:00','12:00','15:00','17:00','21:00');
		foreach($big_drivers as $b) {
			foreach($b as $d){
				if(isset($driver[$d['driver_id']])) {
					// $active_hour = array();
					// foreach($hour as $v) {
						// //melakukan pengecekan pada jam tertentu apakah aktif
						// if($d[$v['label']]) {
							// $active_hour[] = $v['value'];
						// }
					// }
					$driver[$d['driver_id']] = array(
						'driver_id' => $d['driver_id'],
						'name' => $d['first_name'],
						'region' => $d['region'],
						'active_hour' => $active_hour,
						'pickupstandby' => $d['pickupstandby'],
					);
				}
			}
		}
		
		$m = 0;
		$driver2 = array();
		//buang key nya sebelum return output
		foreach($driver as $d) {
			$m++;
			$driver2[] = $d;
		}
				
		if(empty($driver2)) {
			$this->msg=AddonMobileApp::t("Driver is currenty not available, sorry");
		} else {
			$this->details=$driver2;
			$this->code=1;
			$this->msg="OK";
		}
		$this->output();
	}
	
	public function actionGetHour() {
		$r = query("SELECT id,value FROM {{driver_hour}}");
		$this->details=$r;
		$this->code=1;
		$this->msg="OK";		
		$this->output();
	}
	
	public function actionTopUpConfirmation() {
		
		if (!isset($this->data['from_bank'])){
			$this->msg=$this->t("Bank from is missing");
			$this->output();
		}
		if (!isset($this->data['amount'])){
			$this->msg=$this->t("Transfer Amount is missing");
			$this->output();
		}
		if (!isset($this->data['from_account'])){
			$this->msg=$this->t("From Account is missing");
			$this->output();
		}
		 
		if ( $res=AddonMobileApp::getClientTokenInfo($this->data['client_token'])){

				// Path to move uploaded files
				$target_path = "upload/";
				// final file url that is being uploaded
				$file_upload_url = Yii::app()->getBaseUrl(true)  . 'upload/' . $target_path;

				if (isset($_FILES['image']['name'])) {
					
					// $target_path = $target_path . basename($_FILES['image']['name']);
					
					// reading other post parameters
					try {
						// Throws exception incase file is not being moved
						
						$fname = $_FILES['image']['name'];
						$rawBaseName = pathinfo($fname, PATHINFO_FILENAME );
						$extension = pathinfo($fname, PATHINFO_EXTENSION );
						$counter = 0;
						while(file_exists("upload/".$fname)) {
							$fname = $rawBaseName . $counter . '.' . $extension;
							$counter++;
						};
						if (!move_uploaded_file($_FILES['image']['tmp_name'], $target_path.$fname)) {
							// $response['message'] = 'Could not move the file!';
							$this->msg=Yii::t("default","Failed Upload.");
						} else {
							// File successfully uploaded
							$filepath = $file_upload_url . $fname;
							$this->data['from_bank'];
							$params=array(
							  'client_id'=>$res['client_id'],
							  'amount'=>$this->data['amount'],
							  'from_bank'=>$this->data['from_bank'],
							  'from_account'=>$this->data['from_account'],
							  'to_bank'=>$this->data['to_bank'],
							  'to_account'=>$this->data['to_account'],
							  'description'=>$this->data['description'],
							  'image'=>$fname,
							  'date_created'=>date('c'),
							  'status'=>'pending',
							);
							$DbExt=new DbExt;    	
							if ( $DbExt->insertData("{{topup_confirm}}",$params)){
								$this->code=1;
								$this->details=array(
								  'image'=>$filepath,
								);
								$this->msg=Yii::t("default","You have confirm your topup process. Please wait for approval.");	    		    	
							} else $this->msg=Yii::t("default","ERROR: cannot insert records.");		
						}
					} catch (Exception $e) {
						// Exception occurred. Make error flag true
						// $response['message'] = $e->getMessage();
						$this->msg=Yii::t("default","Process Failed".$e->getMessage());
					}
				} else {
					// File parameter is missing
					// $response['message'] = 'Not received any file!F';
					$this->msg=Yii::t("default","File is missing");
				}
		} else $this->msg=$this->t("it seems that your token has expired. please re login again");

		$this->output();
	}

	public function actionTopUpList() {

		if ( $res=AddonMobileApp::getClientTokenInfo($this->data['client_token'])){
			$r = query("SELECT id,amount,status,admin_message,date_created FROM {{topup_confirm}} WHERE client_id=?",array($res['client_id']));
			$this->code=1;
			for($i=0;$i<count($r);$i++) {
				$r[$i]['currency_amount'] = AddonMobileApp::prettyPrice($r[$i]['amount']);
			}
			$this->details=$r;
			$this->msg=Yii::t("default","OK");
		} else $this->msg=$this->t("it seems that your token has expired. please re login again");
		$this->output();
	}
	public function actionSaldo() {

		if ( $res=AddonMobileApp::getClientTokenInfo($this->data['client_token'])){

		
		$str = "Proses Topup  dilakukan melalui proses bank transfer. Berikut ini adalah tahap untuk melakukan topup.<br/>1. Transferkan dengan menggunakan kode unik. Misalkan kode unik Anda Adalah 321. Maka jika Anda ingin mentransfer 50.000, maka transferkanlah 50.321, agar system kami dapat dengan mudah mendeteksi proses transfer Anda. Sehingga saldo Anda menjadi Rp 50.321,-<br/>2. Transferkan ke Rek bank Berikut ini<br/>Bank BNI <br/>No Rek 123123<br/>Atas Nama :Admin FoodPot<br/>Bank BCA<br/>No Rek 123213213<br/>Atas Nama : Admin FoodPot<br/>3. Setelah Anda melakukan transfer, lakukan konfirmasi di halaman ini serta screenshot bukti pembayarannya kalau ada<br/>4.	Status Anda masih berstatus pending saat Anda melakukan konfirmasi. Kami akan melakukan pengecekan, jika proses transfer benar, maka kami akan mengubahnya berstatus sukses dan saldo Anda bertambah<br/>";
			
			
			$this->code=1;
			$this->details=array(
				'currency_saldo'=> str_replace("Rp ","",AddonMobileApp::prettyPrice($res['saldo'])),
				'saldo'=> $res['saldo'],
				'kode_unik'=> ($res['client_id'] % 1000)."",
				'instruksi_topup'=>$str,
			);
			$this->msg=Yii::t("default","OK");
		} else $this->msg=$this->t("it seems that your token has expired. please re login again");
		$this->output();
	}
	//
	
	public function actionCheckOut()
	{
	
		if (!isset($this->data['merchant_id'])){
			$this->msg=$this->t("Merchant Id is is missing");
			$this->output();
		}		
		if (!isset($this->data['search_address'])){
			$this->msg=$this->t("search address is is missing");
			$this->output();
		}		
		if (empty($this->data['transaction_type'])){
			$this->msg=$this->t("transaction type is missing");
			$this->output();
		}	
		if (empty($this->data['delivery_date'])){
			$this->msg=$this->data['transaction_type']." ".$this->t("type is missing");
			$this->output();
		}		
		if (!empty($this->data['delivery_time'])){
   	       $this->data['delivery_time']=date("G:i", strtotime($this->data['delivery_time']));	       	      
   	    }
   	    
	   /**check if customer chooose past time */
       if ( isset($this->data['delivery_time'])){
       	  if(!empty($this->data['delivery_time'])){
       	  	 $time_1=date('Y-m-d g:i:s a');
       	  	 $time_2=$this->data['delivery_date']." ".$this->data['delivery_time'];
       	  	 $time_2=date("Y-m-d g:i:s a",strtotime($time_2));	       	  	        	  	 
       	  	 $time_diff=Yii::app()->functions->dateDifference($time_2,$time_1);	       	  	 
       	  	 if (is_array($time_diff) && count($time_diff)>=1){
       	  	     if ( $time_diff['hours']>0){	       	  	     	
	       	  	     $this->msg=t("Sorry but you have selected time that already past");
	       	  	     $this->output(); 	  	     	
       	  	     }	       	  	
       	  	 }	       	  
       	  }	       
       }		    

       $mtid=$this->data['merchant_id']; 	 
       
       $time=isset($this->data['delivery_time'])?$this->data['delivery_time']:'';	       
       $full_booking_time=$this->data['delivery_date']." ".$time;
	   $full_booking_day=strtolower(date("D",strtotime($full_booking_time)));			
	   $booking_time=date('h:i A',strtotime($full_booking_time));			
	   if (empty($time)){
	   	  $booking_time='';
	   }	    
	   	   	   	   
	   if ( !Yii::app()->functions->isMerchantOpenTimes($mtid,$full_booking_day,$booking_time)){	
			$date_close=date("F,d l Y h:ia",strtotime($full_booking_time));
			$date_close=Yii::app()->functions->translateDate($date_close);
			$this->msg=t("Sorry but we are closed on")." ".$date_close;
			$this->msg.="\n\t\n";
			$this->msg.=t("Please check merchant opening hours");
		    $this->output();
		}					 
			   
	   /*check if customer already login*/
	   $address_book='';
	   $next_step='checkoutSignup';
	   //if (!empty($this->data['client_token'])){
	   if ( $resp=AddonMobileApp::getClientTokenInfo($this->data['client_token'])) {
	   	  $next_step='shipping';
	   	  if ( $this->data['transaction_type']=="pickup" ){
	   	  	 $next_step='payment_method';
	   	  }	   	   	  
	   	  $address_book=AddonMobileApp::getAddressBook($resp['client_id']);	   	  
	   }	
	   
	   
	   
	   $this->code=1;
	   $this->msg=$address_book;
	   $this->details=$next_step;
	   $this->output();
	}
	//
	public function actionSignup()
	{	
				
		$Validator=new Validator;
		$req=array(
		  'first_name'=>$this->t("first name is required"),
		  'last_name'=>$this->t("last name is required"),
		  'contact_phone'=>$this->t("contact phone is required"),
		  'email_address'=>$this->t("email address is required"),
		  'password'=>$this->t("password is required"),
		  'cpassword'=>$this->t("confirm password is required"),
		);
		
		if ($this->data['password']!=$this->data['cpassword']){
			$Validator->msg[]=$this->t("confirm password does not match");
		}	
		
		$Validator->required($req,$this->data);
		if ($Validator->validate()){
			
			/*check if email address is blocked*/
	    	if ( FunctionsK::emailBlockedCheck($this->data['email_address'])){
	    		$this->msg=$this->t("Sorry but your email address is blocked by website admin");
	    		$this->output();
	    	}	   
	    	if ( FunctionsK::mobileBlockedCheck($this->data['contact_phone'])){
	    		$this->msg=$this->t("Sorry but your mobile number is blocked by website admin");
	    		$this->output();
	    	}	    	
	    	/*check if mobile number already exist*/
	        $functionk=new FunctionsK();
	        if ( $functionk->CheckCustomerMobile($this->data['contact_phone'])){
	        	$this->msg=$this->t("Sorry but your mobile number is already exist in our records");
	        	$this->output();
	        }	  
	        if ( !$res=Yii::app()->functions->isClientExist($this->data['email_address']) ){
	        	
	        	$token=AddonMobileApp::generateUniqueToken(15,$this->data['email_address']);
	        	$params=array(
	    		  'first_name'=>$this->data['first_name'],
	    		  'last_name'=>$this->data['last_name'],
	    		  'email_address'=>$this->data['email_address'],
	    		  'password'=>md5($this->data['password']),
	    		  'date_created'=>date('c'),
	    		  'ip_address'=>$_SERVER['REMOTE_ADDR'],
	    		  'contact_phone'=>$this->data['contact_phone'],
	    		  'token'=>$token,
	    		  'social_strategy'=>"mobile"
	    		);	    	    	

	    		$is_checkout=1;
	    		    		
	    		if ($this->data['transaction_type']=="pickup"){
	    			$this->data['next_step']='payment_option';
	    		}		    		
	    		
	    		/*check if the form is checkout*/
	    		if(isset($this->data['transaction_type'])){
		    	   if ($this->data['transaction_type']=="pickup"){
	    			   $is_checkout='payment_option';
	    		   }		    		
	    		   if ($this->data['transaction_type']=="delivery"){
	    			   $is_checkout='shipping_address';
	    		   }		    		
	    		}
	    		
	    		/*check if verification is enabled mobile or web*/
	    		$website_enabled_mobile_verification=getOptionA('website_enabled_mobile_verification');
	    		$theme_enabled_email_verification=getOptionA('theme_enabled_email_verification');
	    		
	    		$verification_type='';
	    		if ($website_enabled_mobile_verification=="yes"){
	    			$verification_type='mobile_verification';
	    			$sms_code=Yii::app()->functions->generateRandomKey(5);
	    			$params['mobile_verification_code']=$sms_code;
	    			$params['status']='pending';
	    			Yii::app()->functions->sendVerificationCode($this->data['contact_phone'],$sms_code);
	    			
	    		}	     
	    		if ($theme_enabled_email_verification==2){
	    			$verification_type='email_verification';
	    			$email_code=Yii::app()->functions->generateCode(10);
	    			$params['email_verification_code']=$email_code;
	    			$params['status']='pending';
	    			FunctionsV3::sendEmailVerificationCode($this->data['email_address'],
	    			$email_code,$this->data);
	    		}	     
	    		
	    		if(!empty($verification_type)){
	    			$this->data['next_step']=$verification_type;
	    		}
	    		
	    		$DbExt=new DbExt; 
	    		if ( $DbExt->insertData("{{client}}",$params)){
	    			$client_id=Yii::app()->db->getLastInsertID();
	    			$this->msg=$this->t("Registration successful");
	    			$this->code=1;
	    			
	    			
	    			$avatar=AddonMobileApp::getAvatar( $client_id , array() );
	    			
	    			$this->details=array(
	    			   'token'=>$token,
	    			   'next_step'=>$this->data['next_step'],
	    			   'is_checkout'=>$is_checkout,
	    			   'client_id'=>$client_id,
	    			   'avatar'=>$avatar,
	    			   'client_name_cookie'=>$this->data['first_name']
	    			 ); 
	    			
	    			FunctionsK::sendCustomerWelcomeEmail($this->data);

	    			//update device client id
		   	   	   if (isset($this->data['device_id'])){
		   	   	       AddonMobileApp::updateDeviceInfo($this->data['device_id'],$client_id);
		   	   	   }    			
	    			
	    		} else $this->msg=$this->t("Something went wrong during processing your request. Please try again later.");
	        } else $this->msg=$this->t("Sorry but your email address already exist in our records.");	    				
		} else $this->msg=AddonMobileApp::parseValidatorError($Validator->getError());		
		$this->output();
	}
	//Tiz
	public function actionGetPaymentOptions()
	{		
		if (!isset($this->data['merchant_id'])){
			$this->msg=$this->t("Merchant Id is is missing");
			$this->output();
		}

		$mtid=$this->data['merchant_id'];
		
		/*ADD CHECKING DISTANCE OF NEW ADDRESS */
		//dump($this->data);
		if(!isset($this->data['transaction_type'])){
			$this->data['transaction_type']='';
		}
		if ( $this->data['transaction_type']=="delivery"){
			$client_address=$this->data['street']." ";
			$client_address.=$this->data['city']." ";
			$client_address.=$this->data['state']." ";
			$client_address.=$this->data['zipcode']." ";
			
			$merchant_info='';
			if (!$merchantinfo=AddonMobileApp::getMerchantInfo($mtid)){
				$this->msg=$this->t("Merchant Id is is missing");
				$this->output();
				Yii::app()->end();
			} else {
				$merchant_address=$merchantinfo['street']." ";
				$merchant_address.=$merchantinfo['city']." ";
				$merchant_address.=$merchantinfo['state']." ";
				$merchant_address.=$merchantinfo['post_code']." ";
				$merchant_info=array(
				  'merchant_id'=>$merchantinfo['merchant_id'],
				  'address'=>$merchant_address,
				  'delivery_fee_raw'=>getOption($mtid,'merchant_delivery_charges')
				);
			}
			// if($distance_new=AddonMobileApp::getDistanceNew($merchant_info,$client_address)){
			   // if(isset($_GET['debug'])){
			      // dump($distance_new);
			   // }
			   // $merchant_delivery_distance=getOption($mtid,'merchant_delivery_miles'); 
			   // if($distance_new['distance_type_raw']=="ft" || $distance_new['distance_type_raw']=="millimeter"){
	    	   	 // // do nothing
	    	   // } else {		   	  
	    	   	  // if(is_numeric($merchant_delivery_distance)){
		    	   	  // if ($merchant_delivery_distance<=$distance_new['distance']){
			    	  	 // $this->msg=$this->t("Sorry but this merchant delivers only with in ").
			    	  	 // $merchant_delivery_distance . " ". $distance_new['distance_type'];
			    	  	 // $this->details=3;
					     // $this->output();
					     // Yii::app()->end();
			    	  // }
	    	   	  // }
	    	   // }
			// } else {
				 // $this->msg=$this->t("Failed calculating distance please try again");
	    	  	 // $this->details=3;
			     // $this->output();
			     // Yii::app()->end();
			// }
		}
		/*ADD CHECKING DISTANCE OF NEW ADDRESS */
		
		/*SAVE TO ADDRESS*/
		if ( $this->data['transaction_type']=="delivery"){
		    if(!isset($this->data['save_address'])){
		    	$this->data['save_address']='';
		    }
		    if ($this->data['save_address']==2){
		    	if ( $client=AddonMobileApp::getClientTokenInfo($this->data['client_token'])){
		    		$params_address=array(
		    		  'client_id'=>$client['client_id'],
		    		  'street'=>isset($this->data['street'])?$this->data['street']:'',
		    		  'city'=>isset($this->data['city'])?$this->data['city']:'',
		    		  'state'=>isset($this->data['state'])?$this->data['state']:'',
		    		  'zipcode'=>isset($this->data['zipcode'])?$this->data['zipcode']:'',
		    		  'location_name'=>isset($this->data['location_name'])?$this->data['location_name']:'',
		    		  'country_code'=>Yii::app()->functions->getOptionAdmin('admin_country_set'),
		    		  'date_created'=>date('c'),
		    		  'ip_address'=>$_SERVER['REMOTE_ADDR']
		    		);
		    		$DbExt=new DbExt; 
		    		$DbExt->insertData("{{address_book}}",$params_address);
		    	}
		    }
		}
		/*SAVE TO ADDRESS*/
		
		$merchant_payment_list='';
		
		/*LIST OF PAYMENT AVAILABLE FOR MOBILE*/
		$mobile_payment=array('cod','paypal','pyr','pyp','atz','stp');
			
		$payment_list=getOptionA('paymentgateway');
		$payment_list=!empty($payment_list)?json_decode($payment_list,true):false;		
		
		$pay_on_delivery_flag=false;
		$paypal_flag=false;
		
		$paypal_credentials='';
		
		$stripe_publish_key='';
				
		/*check master switch for offline payment*/
		if(is_array($payment_list) && count($payment_list)>=1){
		   $payment_list=array_flip($payment_list);
		   
		    $merchant_switch_master_cod=getOption($mtid,'merchant_switch_master_cod');
			if($merchant_switch_master_cod==2){
			   unset($payment_list['cod']);
			}
			$merchant_switch_master_pyr=getOption($mtid,'merchant_switch_master_pyr');
			if($merchant_switch_master_pyr==2){
			   unset($payment_list['pyr']);
			}
		}
		
		if(is_array($payment_list) && count($payment_list)>=1){
		   $payment_list=array_flip($payment_list);
		}		

		if (AddonMobileApp::isArray($payment_list)){			
			foreach ($mobile_payment as $val) {				
				if(in_array($val,(array)$payment_list)){					
					switch ($val) {
						case "cod":			
						    if (Yii::app()->functions->isMerchantCommission($mtid)){
						    	$merchant_payment_list[]=array(
								  'icon'=>'fa-usd',
								  'value'=>$val,
								  'label'=>$this->t("Cash On delivery")
								);
						    	continue;
						    }
							if ( getOption($mtid,'merchant_disabled_cod')!="yes"){
								$merchant_payment_list[]=array(
								  'icon'=>'fa-usd',
								  'value'=>$val,
								  'label'=>$this->t("Cash On delivery")
								);
							}
							break;
					
						case "paypal":	
						case "pyp":	
												  
						  /*admin*/
						  if (Yii::app()->functions->isMerchantCommission($mtid)){						  	
						  	  if ( getOptionA('adm_paypal_mobile_enabled')=="yes"){
						  	  							  	  							  	  
						  	    $paypal_credentials=array(
							      'mode' => getOptionA('adm_paypal_mobile_mode'),
							      'card_fee'=>getOptionA('admin_paypal_fee')
							    );			  
							    if ( strtolower($paypal_credentials['mode'])=="sandbox"){
							  	   $paypal_credentials['client_id_sandbox']=getOptionA('adm_paypal_mobile_clientid');
							  	   $paypal_credentials['client_id_live']='';
							    } else {
							  	   $paypal_credentials['client_id_live']=getOptionA('adm_paypal_mobile_clientid');
							  	   $paypal_credentials['client_id_sandbox']='';
							    }						  
							  }
							  
							  if (!empty($paypal_credentials['client_id_live']) || 
							   !empty($paypal_credentials['client_id_sandbox']) ){
							     $paypal_flag=true;
							  }
							  
							  if ($paypal_flag){
							     $merchant_payment_list[]=array(
							       'icon'=>'fa-paypal',
							        'value'=>$val,
							        'label'=>$this->t("Paypal")
							     );
							  }
							  
						  	  continue;
						  }
						  
						  /*merchant*/
						  if (getOption($mtid,'mt_paypal_mobile_enabled') =="yes"){						      
							  							 							  
							  $paypal_credentials=array(
							    'mode' => strtolower(getOption($mtid,'mt_paypal_mobile_mode')),
							    'card_fee'=>getOption($mtid,'merchant_paypal_fee')							    
							    //'mode' => "nonetwork"
							  );
							  if ( strtolower($paypal_credentials['mode'])=="sandbox"){
							  	 $paypal_credentials['client_id_sandbox']=getOption($mtid,'mt_paypal_mobile_clientid');
							  	 $paypal_credentials['client_id_live']='';
							  } else {
							  	 $paypal_credentials['client_id_live']=getOption($mtid,'mt_paypal_mobile_clientid');
							  	 $paypal_credentials['client_id_sandbox']='';
							  }			
							  
							  if (!empty($paypal_credentials['client_id_live']) || 
							   !empty($paypal_credentials['client_id_sandbox']) ){
							     $paypal_flag=true;
							  }
							  
							  if ($paypal_flag){
							  	$merchant_payment_list[]=array(
							      'icon'=>'fa-paypal',
							      'value'=>$val,
							      'label'=>$this->t("Paypal")
							    );
							  }						  
							  				  
						   }
						   break;
						
						case "pyr":	
						    $pay_on_delivery_flag=true;
						   if (Yii::app()->functions->isMerchantCommission($mtid)){
						   	   $merchant_payment_list[]=array(
							    'icon'=>'fa-cc-visa',
							    'value'=>$val,
							    'label'=>$this->t("Pay On Delivery")
							   );
						   	   continue;
						   }
						   if ( getOption($mtid,'merchant_payondeliver_enabled')=="yes"){
						      $merchant_payment_list[]=array(
							    'icon'=>'fa-cc-visa',
							    'value'=>$val,
							    'label'=>$this->t("Pay On Delivery")
							  );
						   }
						   break;
						   
						case "atz":
							if (Yii::app()->functions->isMerchantCommission($mtid)){
								$merchant_payment_list[]=array(
								   'icon'=>'ion-card',
								   'value'=>$val,
								   'label'=>$this->t("Authorize.net")
								);
							} else {
								if(getOption($mtid,'merchant_enabled_autho')=="yes"){
									$merchant_payment_list[]=array(
									   'icon'=>'ion-card',
									   'value'=>$val,
									   'label'=>$this->t("Authorize.net")
									);
								}
							}
							break;
							
					   case "stp":
					   	
							if (Yii::app()->functions->isMerchantCommission($mtid)){
								
								$stripe_enabled=getOptionA('admin_stripe_enabled');
								if($stripe_enabled!="yes"){
									continue;
								}
								
								$mode=Yii::app()->functions->getOptionAdmin('admin_stripe_mode');  
			                    $mode=strtolower($mode);								
								if ( $mode=="sandbox"){
								   	$stripe_publish_key=getOptionA('admin_sandbox_stripe_pub_key');
								} else {
									$stripe_publish_key=getOptionA('admin_live_stripe_pub_key');
								}
								if(!empty($stripe_publish_key)){
									$merchant_payment_list[]=array(
									   'icon'=>'ion-card',
									   'value'=>$val,
									   'label'=>$this->t("Stripe")
									);
								}
							} else {
								if(getOption($mtid,'stripe_enabled')=="yes"){
									
									$stripe_enabled=getOption($mtid,'stripe_enabled');
									if($stripe_enabled!="yes"){
										continue;
									}
								
									$mode=Yii::app()->functions->getOption('stripe_mode',$mtid);   
				                    $mode=strtolower($mode);
				                    if ( $mode=="sandbox"){
									   $stripe_publish_key=getOption($mtid,'sandbox_stripe_pub_key');
				                    } else {
				                       $stripe_publish_key=getOption($mtid,'live_stripe_pub_key'); 
				                    }
									if(!empty($stripe_publish_key)){
										$merchant_payment_list[]=array(
										   'icon'=>'ion-card',
										   'value'=>$val,
										   'label'=>$this->t("Stripe")
										);
									}
								}
							}
							break;	
							   
						default:
							break;
					}					
				}			
			}
			
			$pay_on_delivery_list='';
			if ($pay_on_delivery_flag){
				if ($list=Yii::app()->functions->getPaymentProviderListActive()){
					foreach ($list as $val_payment) {						
						$pay_on_delivery_list[]=array(
						  'payment_name'=>$val_payment['payment_name'],
						  'payment_logo'=>AddonMobileApp::getImage($val_payment['payment_logo']),
						);
					}
				}				
			}
						
			if (AddonMobileApp::isArray($merchant_payment_list)){			
				
				/*pts*/
				$points_balance=0;
				if (AddonMobileApp::hasModuleAddon('pointsprogram')){
					if (getOptionA('points_enabled')==1){
						if ( $client=AddonMobileApp::getClientTokenInfo($this->data['client_token'])){
							$client_id=$client['client_id'];
						} else $client_id=0;
						$points_balance=PointsProgram::getTotalEarnPoints($client_id);
					}
				}
				
				$this->code=1;
				$this->msg="OK";
				$this->details=array(
				  // 'voucher_enabled'=>getOption($mtid,'merchant_enabled_voucher'),
				  'payment_list'=>$merchant_payment_list,
				  'pay_on_delivery_flag'=>$pay_on_delivery_flag,
				  'pay_on_delivery_list'=>$pay_on_delivery_list,
				  'paypal_flag'=>$paypal_flag==true?1:2,
				  'paypal_credentials'=>$paypal_credentials,
				  // 'stripe_publish_key'=>$stripe_publish_key,
				  // 'pts'=>array(
				    // 'balance'=>$points_balance,
				    // 'pts_label_input'=>getOptionA('pts_label_input')
				  // )
				);
			} else $this->msg=$this->t("sorry but all payment options is not available");		
		} else $this->msg=$this->t("sorry but all payment options is not available");
				
		$this->output();	
	}
	//
	public function actionPlaceOrder()
	{
		
		$DbExt=new DbExt; 
		
		if (isset($this->data['next_step'])){
			unset($this->data['next_step']);
		}

		$Validator=new Validator;
		$req=array(
		  // 'merchant_id'=>$this->t("Merchant Id is is missing"),
		  'cart'=>$this->t("cart is empty"),
		  'transaction_type'=>$this->t("transaction type is missing"),
		  'payment_list'=>$this->t("payment method is missing"),
		  'client_token'=>$this->t("client token is missing")
		);

		// $mtid=$this->data['merchant_id'];
		
		if ( !$client=AddonMobileApp::getClientTokenInfo($this->data['client_token'])){
			$Validator->msg[]=$this->t("sorry but your session has expired please login again");
		}
		$client_id=$client['client_id'];
						

		
		/*$this->msg='Your order has been placed. Reference # 123';
		$this->code=1;
	    $this->details=array(
	       'next_step'=>'receipt',
	       'order_id'=>123,
	       'payment_type'=>$this->data['payment_list']
	    );
        $this->output();*/
		
		//dump($this->data);

		$Validator->required($req,$this->data);
		if ($Validator->validate()){
			// file_put_contents('log2.txt',var_export(AddonMobileApp::computeCart($this->data),true));
			if ( $res=AddonMobileApp::computeCart($this->data)){
				if (empty($res['validation_msg'])){
				   $json_data=AddonMobileApp::cartMobile2WebFormat($res,$this->data);
				   
				   if (AddonMobileApp::isArray($json_data)) {
						
						if($res['ret_delivery'] == false) {
							$this->msg=t("Your address is invalid.");
							$this->output();
							return ;
						}
						//Validasi cek apakah belanja user melebihi saldo
						if($this->data['payment_list'] == 'credit') {
							// $client_id = Yii::app()->functions->getClientId();
							$client = query("SELECT * FROM {{client}} WHERE client_id=?",array($client_id));
							$saldo = $client[0]['saldo'];
							if ($saldo) {
								//apakah total belanja melebihi saldo
								if($res['grand_total'] > $saldo) {
									$this->msg=t("Your balance is insufficient. We recommend you to top up your balance first.");
									$this->output();
									return ;
								}
							} else {
								$this->msg=t("Your balance is empty. Please topup your balance.");
								$this->output();
								return ;
							}
							
						}
						
					   $cart=$res['cart'];	
					   //dump($cart);
					   
					   
					   $status="pending";
					   					   
					   $params=array(
					    // 'merchant_id'=>$this->data['merchant_id'],
					    'client_id'=>$client_id,
					    'json_details'=>json_encode($json_data),
					    'trans_type'=>$this->data['transaction_type'],
					    //'payment_type'=>Yii::app()->functions->paymentCode($this->data['payment_list']),
					    'payment_type'=>$this->data['payment_list'],
					    'sub_total'=>isset($cart['sub_total'])?$cart['sub_total']['amount']:0,
						
					    'tax'=>isset($cart['tax'])?$cart['tax']['tax']:0,
					    'taxable_total'=>isset($cart['tax'])?$cart['tax']['amount_raw']:0,
					    'total_w_tax'=>isset($cart['grand_total'])?$cart['grand_total']['amount']:0,
					    'status'=>$status,
						'weight'=>$res['weight_total'],
					    'delivery_charge'=>isset($cart['delivery_charges'])?$cart['delivery_charges']['amount']:0,
					    'delivery_date'=>isset($this->data['delivery_date'])?$this->data['delivery_date']:'',
					    'delivery_time'=>isset($this->data['delivery_time'])?$this->data['delivery_time']:'',
					    'delivery_asap'=>isset($this->data['delivery_asap'])?$this->data['delivery_asap']:'',
					    'delivery_instruction'=>isset($this->data['delivery_instruction'])?$this->data['delivery_instruction']:'',
					    'date_created'=>date('c'),
					    'ip_address'=>$_SERVER['REMOTE_ADDR'],
					    'order_change'=>isset($this->data['order_change'])?$this->data['order_change']:'',
					    'mobile_cart_details'=>isset($this->data['cart'])?$this->data['cart']:'',
						'planned_driver'=>$this->data['driver_id'],
					   );
					   
					   
					   if (isset($this->data['payment_provider_name'])){
					   	   $params['payment_provider_name']=$this->data['payment_provider_name'];
					   }
					   
		    		   
		    		   if(isset($cart['discount'])){
		    		   	  $params['discounted_amount']=$cart['discount']['amount'];
		    		   	  $params['discount_percentage']=$cart['discount']['discount'];
		    		   }
		    		   
					    					    
					    /*insert the order details*/				
					    $params['request_from']='mobile_app';  // tag the order to mobile app
						
											  
					    if (!$DbExt->insertData("{{order}}",$params)){
					    	$this->msg=AddonMobileApp::t("ERROR: Cannot insert records.");
					    	$this->output();
					    }
					    
					    $order_id=Yii::app()->db->getLastInsertID();	
						// var_dump($res['list_merchant']);
						
						$merchants_as_value = array();
						$tax_merchants = $res['tax_merchants'];
						foreach ($res['list_merchant'] as $merchant_id => $subtotal_merchant) {
                            $a=array(
					    	  'order_id'=>$order_id,
					    	  'merchant_id'=>$merchant_id,
							  'subtotal'=>$subtotal_merchant,
							  'part_status'=>'pending',
							  'tax'=>$tax_merchants[$merchant_id],
							  'earning'=>$subtotal_merchant+$tax_merchants[$merchant_id],
							  //earning merchant adalah penjumlahan dari subtotal ditambah tax
					    	);
					    	$DbExt->insertData("{{order_merchant}}",$a);
							$merchants_as_value[] = $merchant_id;
						}

					    /*saved food item details*/	
					    foreach ($cart['cart'] as $val_item) {
					    	//dump($val_item);		
					    	$item_details=Yii::app()->functions->getFoodItem($val_item['item_id']);
					    	$discounted_price=$val_item['price'];
					    	if($item_details['discount']>0){
					    		$discounted_price=$discounted_price-$item_details['discount'];
					    	}
					    	
                            $params_details=array(
					    	  'order_id'=>$order_id,
					    	  'client_id'=>$client_id,
					    	  'item_id'=>$val_item['item_id'],
							  'merchant_id'=>$val_item['merchant_id'],
					    	  'item_name'=>$val_item['item_name'],					    	  
					    	  'order_notes'=>isset($val_item['order_notes'])?$val_item['order_notes']:'',
					    	  'normal_price'=>$val_item['price'],
					    	  'discounted_price'=>$discounted_price,
					    	  'qty'=>isset($val_item['qty'])?$val_item['qty']:'',
					    	  'non_taxable'=>isset($val_item['non_taxable'])?$val_item['non_taxable']:1,
							  'tax'=>isset($val_item['tax'])?$val_item['tax']:'',
							  'weight'=>isset($val_item['weight'])?$val_item['weight']:'',
							  'photo'=>isset($val_item['photo'])?$val_item['photo']:'',
							  'uom'=>isset($val_item['uom'])?$val_item['uom']:'',
					    	);
							
					    	//dump($params_details);
					    	$DbExt->insertData("{{order_details}}",$params_details);
					    }

					    					   					   
					    /*save the customer delivery address*/
						
						$str_merchants = implode(',',$merchants_as_value);
						//get merchants
						$merchants = query("SELECT merchant_id,merchant_name,contact_name,contact_phone,street,city,state,post_code,latitude,longitude FROM {{view_merchant}} WHERE merchant_id IN(".$str_merchants.")");
						
						
						//mendapatkan latitude longitude customer
						$latitude = "";
						$longitude = "";
						if(!empty($this->data['latitude']) && !empty($this->data['longitude'])){
							$latitude = $this->data['latitude'];
							$longitude = $this->data['longitude'];
						} elseif(isset($res['ret_delivery']['client_lat'])) {
							$latitude = $res['ret_delivery']['client_lat'];
							$longitude = $res['ret_delivery']['client_lng'];
						}
						$params_address=array(
						  'order_id'=>$order_id,
						  'client_id'=>$client_id,
						  'street'=>isset($this->data['street'])?$this->data['street']:'',
						  'city'=>isset($this->data['city'])?$this->data['city']:'',
						  'state'=>isset($this->data['state'])?$this->data['state']:'',
						  'zipcode'=>isset($this->data['zipcode'])?$this->data['zipcode']:'',
						  'location_name'=>isset($this->data['location_name'])?$this->data['location_name']:'',
						  'country'=>Yii::app()->functions->adminCountry(),
						  'date_created'=>date('c'),
						  'ip_address'=>$_SERVER['REMOTE_ADDR'],
						  'contact_phone'=>isset($this->data['contact_phone'])?$this->data['contact_phone']:'',
						  'origin_merchant'=>json_encode($merchants),
						  'latitude'=>$latitude,
						  'longitude'=>$longitude,
						);
						
						//dump($params_address);
						$DbExt->insertData("{{order_delivery_address}}",$params_address);
					    
					    
						$cart = query("SELECT merchant_id, item_id, item_name,order_notes,normal_price,discounted_price,qty FROM {{order_details}} WHERE order_id=".$order_id." ");
						for($i=0;$i<count($cart);$i++) {
							$cart[$i]['normal_price_currency']=AddonMobileApp::prettyPrice($cart[$i]['normal_price']);
							$cart[$i]['discounted_price_currency']=AddonMobileApp::prettyPrice($cart[$i]['discounted_price']);
						}
						$data=query("SELECT * FROM {{order}} a WHERE a.order_id='".$order_id."' LIMIT 0,1");
						$data = $data[0];
						$data_raw['order_id']=$data['order_id'];
						$data_raw['subtotal']=$data['sub_total'];
						$data_raw['subtotal_currency']=AddonMobileApp::prettyPrice($data['sub_total']);
						$data_raw['taxable_total']=$data['taxable_total'];
						$data_raw['taxable_total_currency']=AddonMobileApp::prettyPrice($data['taxable_total']);
						$data_raw['delivery_charge']=$data['delivery_charge'];
						$data_raw['delivery_charge_currency']=AddonMobileApp::prettyPrice($data['delivery_charge']);
						$data_raw['total']=$data['total_w_tax'];
						$data_raw['total_currency']=AddonMobileApp::prettyPrice($data['total_w_tax']);
						// $data_raw['total']['tax_amt']=$data_raw['total']['tax_amt']."%";
						$pos = Yii::app()->functions->getOptionAdmin('admin_currency_position'); 
						$data_raw['currency_position']=$pos;
						$delivery_date=$data['delivery_date'];
						$data_raw['transaction_date']	= Yii::app()->functions->FormatDateTime($data['date_created']);						          $data_raw['delivery_date'] = Yii::app()->functions->FormatDateTime($delivery_date,false);
						$data_raw['delivery_time'] = $data['delivery_time'];
						$data_raw['status']=t($data['status']);
						$data_raw['status_raw']=strtolower($data['status']);
						$data_raw['trans_type']=t($data['trans_type']);
						$data_raw['trans_type_raw']=$data['trans_type'];
						$data_raw['payment_type']=strtoupper($data['payment_type']);						
						$data_raw['delivery_instruction']=$data['delivery_instruction'];
						$data_raw['cart']=$cart;
					
					
					    $this->code=1;
					    $this->details=$data_raw;
					    /*insert logs for food history*/
						$params_logs=array(
						  'order_id'=>$order_id,
						  'status'=> $status,
						  'date_created'=>date('c'),
						  'ip_address'=>$_SERVER['REMOTE_ADDR']
						);
						$DbExt->insertData("{{order_history}}",$params_logs);

						$ok_send_notification=true;

						//perbarui saldo setelah dikurangi total belanja
						
						if($this->data['payment_list'] == 'credit' && isset($saldo)) {
							$new_saldo = $saldo-$res['grand_total'];
							queryNoFetch("UPDATE {{client}} SET saldo=? WHERE client_id=?",array($new_saldo,$client_id));
						}
						
					    /*send email to client and merchant*/
					    AddonMobileApp::sendOrderEmail($cart,$params,$order_id,$this->data,$ok_send_notification);

				   } else $this->msg=$this->t("something went wrong.");
				} else $this->msg=$res['validation_msg'];
			} else $this->msg=$this->t("something went wrong");
		} else $this->msg=AddonMobileApp::parseValidatorError($Validator->getError());	
		
		$this->output();
	}
	//
	public function actionGetMerchantInfo()
	{		
		if (!isset($this->data['merchant_id'])){
			$this->msg=$this->t("Merchant Id is is missing");
			$this->output();
		}
		
		$mtid=$this->data['merchant_id'];
		
		if ( $data = AddonMobileApp::merchantInformation($this->data['merchant_id'])){							
						
			$this->details=array(
			  'merchant_info'=>$data,
			);
			if ($review=AddonMobileApp::previewMerchantReview($mtid)){
				$this->details['reviews']=Yii::app()->functions->translateDate($review);
			}
			
			$merchant_latitude=getOption($mtid,'merchant_latitude');
			$merchant_longitude=getOption($mtid,'merchant_longitude');
			if(!empty($merchant_latitude) && !empty($merchant_longitude)){
				$this->details['maps']=array(
				  'merchant_latitude'=>$merchant_latitude,
				  'merchant_longitude'=>$merchant_longitude
				);
			}
			
			
			$this->code=1;
			$this->msg="OK";
		} else $this->msg=AddonMobileApp::t("sorry but merchant information is not available");
		
		$this->output();
	}

	//
	public function actionMerchantReviews()
	{
	
		if (isset($this->data['merchant_id'])){
			if ( $res=Yii::app()->functions->getReviewsList($this->data['merchant_id'])){
				$data='';
				foreach ($res as $val) {
					$prety_date=PrettyDateTime::parse(new DateTime($val['date_created']));
					$data[]=array(
					  'client_name'=>empty($val['client_name'])?$this->t("not available"):$val['client_name'],
					  'review'=>$val['review'],
					  'rating'=>$val['rating'],
					  'date_created'=>Yii::app()->functions->translateDate($prety_date)
					);
				}
				$this->code=1;$this->msg="OK";
				$this->details=$data;
			} else $this->msg=$this->t("no current reviews");
		} else $this->msg=$this->t("Merchant id is missing");
		$this->output();	
	}
	//
	public function actionAddReview()
	{		
		$Validator=new Validator;
		$req=array(
		  'rating'=>$this->t("rating is required"),
		  'review'=>$this->t("review is required"),
		  'merchant_id'=>$this->t("Merchant id is missing")
		);
		$Validator->required($req,$this->data);

		if ( !$client=AddonMobileApp::getClientTokenInfo($this->data['client_token'])){
			$Validator->msg[]=$this->t("Sorry but you need to login to write a review.");
		} 
		$client_id=$client['client_id'];
		$mtid=$this->data['merchant_id'];
		
		if ( $Validator->validate()){
						
			$params=array(
	    	  'merchant_id'=>$mtid,
	    	  'client_id'=>$client_id,
	    	  'review'=>$this->data['review'],
	    	  'date_created'=>date('c'),
	    	  'rating'=>$this->data['rating']
	    	);		 
		    	
			/** check if user has bought from the merchant*/		    	
	    	if ( Yii::app()->functions->getOptionAdmin('website_reviews_actual_purchase')=="yes"){
	    		$functionk=new FunctionsK();
	    	    if (!$functionk->checkIfUserCanRateMerchant($client_id,$mtid)){
	    	    	$this->msg=$this->t("Reviews are only accepted from actual purchases!");
	    	    	$this->output();
	    	    }
	    	    		    	    	    	   
	    	    if (!$functionk->canReviewBasedOnOrder($client_id,$mtid)){
	    		   $this->msg=$this->t("Sorry but you can make one review per order");
	    	       $this->output();
	    	    }	  		   
	    	    
	    	    if ( $ref_orderid=$functionk->reviewByLastOrderRef($client_id,$this->data['merchant-id'])){
	    	    	$params['order_id']=$ref_orderid;
	    	    }
	    	}
	    	$DbExt=new DbExt;    	
	    	if ( $DbExt->insertData("{{review}}",$params)){
	    		$this->code=1;
	    		$this->msg=Yii::t("default","Your review has been published.");	    		    	
	    	} else $this->msg=Yii::t("default","ERROR: cannot insert records.");		
		} else $this->msg=AddonMobileApp::parseValidatorError($Validator->getError());	
		$this->output();	
	}
	//
	public function actionMerchantList()
	{
		$DbExt=new DbExt;  
		$DbExt->qry("SET SQL_BIG_SELECTS=1");		
		
		$start=0;
		$limit=200;
		
		$and='';
		if (isset($this->data['merchant_name'])){
			$and=" AND merchant_name LIKE '".$this->data['merchant_name']."%'";
		}	
		
		$stmt="SELECT SQL_CALC_FOUND_ROWS a.*,
    	(
    	select option_value
    	from 
    	{{option}}
    	WHERE
    	merchant_id=a.merchant_id
    	and
    	option_name='merchant_photo'
    	) as merchant_logo
    	        
    	 FROM
    	{{view_merchant}} a    	
    	WHERE is_ready ='2'
    	AND status in ('active')
    	$and
    	ORDER BY membership_expired,is_featured DESC
    	LIMIT $start,$limit    	
    	";    			
		
		if (isset($_GET['debug'])){
			dump($stmt);
		}

		if ($res=$DbExt->rst($stmt)){
			$data='';
			
			$total_records=0;
			$stmtc="SELECT FOUND_ROWS() as total_records";
	 		if ($resp=$DbExt->rst($stmtc)){			 			
	 			$total_records=$resp[0]['total_records'];
	 		}			 		
			 		
			foreach ($res as $val) {
								
				/*check if mechant is open*/
	 			$open=AddonMobileApp::isMerchantOpen($val['merchant_id']);
	 			
		        /*check if merchant is commission*/
		        $cod=AddonMobileApp::isCashAvailable($val['merchant_id']);
		        $online_payment='';
		         		
		        
		        $minimum_order=getOption($val['merchant_id'],'merchant_minimum_order');
	 			if(!empty($minimum_order)){
		 			$minimum_order=displayPrice(getCurrencyCode(),prettyFormat($minimum_order));		 			
	 			}
				        
				$data[]=array(
	 			  'merchant_id'=>$val['merchant_id'],
	 			  'merchant_name'=>$val['merchant_name'],
				  'contact_phone'=>$val['contact_phone'],
	 			  'address'=>$val['street'].", ".$val['city'].", ".$val['state'],
	 			  'ratings'=>Yii::app()->functions->getRatings($val['merchant_id']),
			 	  'minimum_order'=>$minimum_order,
	 			  'is_open'=>$tag,
	 			  'logo'=>AddonMobileApp::getMerchantLogo($val['merchant_id']),	 			  
	 			  'map_coordinates'=>array(
	 			    'latitude'=>!empty($val['latitude'])?$val['latitude']:'',
	 			    'longitude'=>!empty($val['longitude'])?$val['longitude']:'',
	 			  ),
	 			  
	 			);
			}
			$this->details=array(
	 		  'total'=>$total_records,
	 		  'data'=>$data
	 		);
	 		$this->code=1;$this->msg="Ok";
	 		$this->output();
		} else $this->msg=$this->t("No merchant found");
		$this->output();
	}
	//
	public function actiongetProfile()
	{	
		if ( $res=AddonMobileApp::getClientTokenInfo($this->data['client_token'])){			
			$this->code=1;
			$this->msg="OK";
			$avatar=AddonMobileApp::getAvatar( $res['client_id'] , $res );
			$res['avatar']=$avatar;
			$this->details=$res;
		} else $this->msg=$this->t("not login");
		$this->output();
	}
	//
	public function actionsaveProfile()
	{
		if ( $res=AddonMobileApp::getClientTokenInfo($this->data['client_token'])){
						
			/*check if mobile number is already exists*/
			if (isset($this->data['contact_phone'])){
			$functionsk=new FunctionsK();
				if ($functionsk->CheckCustomerMobile($this->data['contact_phone'],$res['client_id'])){
					$this->msg= $this->t("Sorry but your mobile number is already exist in our records");
					$this->output();
					Yii::app()->end();
				}		
			}			
			
			$params=array(
			  'first_name'=>$this->data['first_name'],
			  'last_name'=>$this->data['last_name'],
			  'contact_phone'=>isset($this->data['contact_phone'])?$this->data['contact_phone']:'',
			  'date_modified'=>date('c'),
			  'ip_address'=>$_SERVER['REMOTE_ADDR']
			);
			if (!empty($this->data['password'])){
				$params['password']=md5($this->data['password']);
			}					
			$DbExt=new DbExt;  
			if($DbExt->updateData("{{client}}",$params,'client_id',$res['client_id'])){
				$this->code=1;
				$this->msg=$this->t("your profile has been successfully updated");				
			} else $this->msg=$this->t("something went wrong during processing your request");
		} else $this->msg=$this->t("it seems that your token has expired. please re login again");
		$this->output();
	}
	//
	public function actionLogin()
	{
		
		/*check if email address is blocked by admin*/	    	
    	if ( FunctionsK::emailBlockedCheck($this->data['email_address'])){
    		$this->msg=t("Sorry but your email address is blocked by website admin");
    		$this->output();
    	}	    	
    	    
    	$Validator=new Validator;
		$req=array(
		  'email_address'=>$this->t("email address is required"),
		  'password'=>$this->t("password is required")		  
		);
		// var_dump($this->data);
		$Validator->required($req,$this->data);
    	
		if ( $Validator->validate()){
		   $stmt="SELECT * FROM
		   {{client}}
		    WHERE
	    	email_address=".Yii::app()->db->quoteValue($this->data['email_address'])."
	    	AND
	    	password=".Yii::app()->db->quoteValue(md5($this->data['password']))."
	    	AND
	    	status IN ('active')
	    	LIMIT 0,1
		   ";		   
		   $DbExt=new DbExt; 
		   
		   if ($res=$DbExt->rst($stmt)){
			   
		   	   $res=$res[0];
		   	   $client_id=$res['client_id'];
		   	   $token=AddonMobileApp::generateUniqueToken(15,$this->data['email_address']);
		   	   $params=array(
		   	     'token'=>$token,
		   	     'last_login'=>date('c'),
		   	     'ip_address'=>$_SERVER['REMOTE_ADDR']		   	     
		   	   );		  
				
		   	   if ($DbExt->updateData("{{client}}",$params,'client_id',$client_id)){
				  
		   	   	   $this->code=1;
		   	   	   $this->msg=$this->t("Login Okay");
		   	   	   
		   	   	   $avatar=''; $client_name='';
		   	   	   $avatar=AddonMobileApp::getAvatar( $client_id , $res );		   	   	   
		   	   	   
		   	   	   $this->details=array(
		   	   	     'token'=>$token,
		   	   	     'next_steps'=>isset($this->data['next_steps'])?$this->data['next_steps']:'',
		   	   	     'has_addressbook'=>AddonMobileApp::hasAddressBook($client_id)?2:1,
		   	   	     'avatar'=>$avatar,
		   	   	     'client_name_cookie'=>$res['first_name']
		   	   	   );
		   	   	    
		   	   	   //update device client id
		   	   	   if (isset($this->data['device_id'])){
		   	   	       AddonMobileApp::updateDeviceInfo($this->data['device_id'],$client_id);
		   	   	   }
		   	   	   
		   	   } else $this->msg=$this->t("something went wrong during processing your request");
		   } else $this->msg=$this->t("Login Failed. Either username or password is incorrect");
			// return true;
		} else $this->msg=AddonMobileApp::parseValidatorError($Validator->getError());	    	
    	$this->output();
	}
	//
	public function actionForgotPassword()
	{		
		$Validator=new Validator;
		$req=array(
		  'email_address'=>$this->t("email address is required")		  
		);
		$Validator->required($req,$this->data);
				
		if ( $Validator->validate()){
		   if ( $res=yii::app()->functions->isClientExist($this->data['email_address']) ){					
			$token=md5(date('c'));
			$params=array('lost_password_token'=>$token);					
			$DbExt=new DbExt;
			if ($DbExt->updateData("{{client}}",$params,'client_id',$res['client_id'])){
				$this->code=1;						
				$this->msg=Yii::t("default","We sent your forgot password link, Please follow that link. Thank You.");
				//send email					
				$tpl=EmailTPL::forgotPass($res,$token);			    
			    $sender='';
                $to=$res['email_address'];		                
                if (!sendEmail($to,$sender,Yii::t("default","Forgot Password"),$tpl)){		    			                	
                	$this->details="failed";
                } else $this->details="mail ok";
                				
			} else $this->msg=Yii::t("default","ERROR: Cannot update records");				
		} else $this->msg=Yii::t("default","Sorry but your Email address does not exist in our records.");
		} else $this->msg=AddonMobileApp::parseValidatorError($Validator->getError());	
		$this->output();
	}
	//
	public function actiongetOrderHistory()
	{		
		if ( $res=AddonMobileApp::getClientTokenInfo($this->data['client_token'])) {			
			if ( $order=query(" SELECT a.*,group_concat(d.merchant_name separator ',') title,
								group_concat(c.item_name separator ',') title2 
								FROM {{order}} a
								
								JOIN {{order_merchant}} b
								ON a.order_id = b.order_id
								JOIN  {{merchant}} d
								ON b.merchant_id=d.merchant_id
								
								JOIN {{order_details}} c 
								ON a.order_id = c.order_id
								WHERE a.client_id=? AND a.status NOT IN ('".initialStatus()."')
								GROUP BY a.order_id
								ORDER BY a.order_id DESC ",array($res['client_id']))){
				$this->code=1;
				$this->msg="Ok";
				$data='';
				
				foreach ($order as $val) {
					$val['title'] = implode(',',array_unique(explode(',',$val['title'])));
					$val['title2'] = implode(',',array_unique(explode(',',$val['title2'])));
					$data[]=array(
					  'order_id'=>$val['order_id'],
					  'title'=>(strlen($val['title']) > 938)? substr($val['title'],0,935).'...': $val['title'],
					  'title2'=>(strlen($val['title2']) > 938)? substr($val['title2'],0,935).'...': $val['title2'],
					  // 'create_date'=>Yii::app()->functions->translateDate(prettyDate($val['date_created'])),
					  'status'=>AddonMobileApp::t($val['status'])
					);
				}
				$this->details=$data;
			} else $this->msg =$this->t("you don't have any orders yet");
		} else {
			$this->msg=$this->t("sorry but your session has expired please login again");
			$this->code=3;
		}
		$this->output();
	}
	//
	public function actionOrdersDetails()
	{		
		$trans=getOptionA('enabled_multiple_translation'); 		
		if ( $res=AddonMobileApp::getClientTokenInfo($this->data['client_token'])) {			 
			 if ( $data=query("SELECT * FROM {{order}} a WHERE a.order_id=? LIMIT 0,1",array($this->data['order_id']))){
				  $data = $data[0];
			 	  // $data='';
			 	  // foreach ($res as $val) {
			 	  	 
			 	  	 // if ( $trans==2 && isset($_GET['lang_id'])){
			 	  	 	 // $lang_id=$_GET['lang_id'];
			 	  	 	 // $val['item_name']=AddonMobileApp::translateItem('item',$val['item_name'],
			 	  	 	 // $val['item_id'],'item_name_trans');
			 	  	 // }
			 	  	
			 	  	 // $data[]=array(
			 	  	   // 'item_name'=>$val['qty']."x ".$val['item_name']			 	  	   
			 	  	 // );
			 	  // }
				  
			 	  $history_data='';
			 	  if ($history=FunctionsK::orderHistory($this->data['order_id'])){
			 	  	 foreach ($history as $val) {
			 	  	 	$history_data[]=array(
			 	  	 	  'date_created'=>FormatDateTime($val['date_created'],true),
			 	  	 	  'status'=>AddonMobileApp::t($val['status']),
			 	  	 	  'remarks'=>$val['remarks']
			 	  	 	);
			 	  	 }
			 	  }
			 	  
			 	  // $stmt="SELECT 
			 	  // request_from,
			 	  // payment_type,
			 	  // trans_type
			 	   // FROM
			 	  // {{order}}
			 	  // WHERE 
			 	  // order_id=".AddonMobileApp::q($this->data['order_id'])."
			 	  // LIMIT 0,1
			 	  // ";
			 	  // $DbExt=new DbExt;
			 	  // $order_from='web';
			 	  // if ($resp=$DbExt->rst($stmt)){
			 	  	 // $order_from=$resp[0];
			 	  // } else {
			 	  	 // $order_from=array(
			 	  	   // 'request_from'=>'web'
			 	  	 // );
			 	  // }			 
			 	  

					$cart = query("SELECT merchant_id, item_id, item_name,order_notes,normal_price,discounted_price,qty,weight FROM {{order_details}} WHERE order_id=".$this->data['order_id']." ");
					$weight_total = 0;
					for($i=0;$i<count($cart);$i++) {
						$cart[$i]['normal_price_currency']=AddonMobileApp::prettyPrice($cart[$i]['normal_price']);
						$cart[$i]['discounted_price_currency']=AddonMobileApp::prettyPrice($cart[$i]['discounted_price']);
						
						$cart[$i]['weight'] = $cart[$i]['weight']?$cart[$i]['weight']:500;
						$weight_total += $cart[$i]['qty']* $cart[$i]['weight'];
					}



					// var_dump($cart);
					$data_raw = array();
					$data_raw['history_data'] = $history_data;
					$data_raw['cart'] = $cart;
					//dump($data);
					$data_raw['order_id']=$data['order_id'];
					$data_raw['subtotal']=$data['sub_total'];
					$data_raw['subtotal_currency']=AddonMobileApp::prettyPrice($data['sub_total']);
					$data_raw['taxable_total']=$data['taxable_total'];
					$data_raw['taxable_total_currency']=AddonMobileApp::prettyPrice($data['taxable_total']);
					$data_raw['delivery_charge']=$data['delivery_charge'];
					$data_raw['delivery_charge_currency']=AddonMobileApp::prettyPrice($data['delivery_charge']);
					$data_raw['total']=$data['total_w_tax'];
					$data_raw['total_currency']=AddonMobileApp::prettyPrice($data['total_w_tax']);
					$data_raw['weight_total']=($weight_total>=1000)?round($weight_total/1000)." kg":($weight_total)." gram";
					// $data_raw['total']['tax_amt']=$data_raw['total']['tax_amt']."%";
					$pos = Yii::app()->functions->getOptionAdmin('admin_currency_position'); 
					$data_raw['currency_position']=$pos;
					$delivery_date=$data['delivery_date'];
					$data_raw['transaction_date']	= Yii::app()->functions->FormatDateTime($data['date_created']);						          $data_raw['delivery_date'] = Yii::app()->functions->FormatDateTime($delivery_date,false);
					$data_raw['delivery_time'] = $data['delivery_time'];
					$data_raw['status']=t($data['status']);
					$data_raw['status_raw']=strtolower($data['status']);
					$data_raw['trans_type']=t($data['trans_type']);
					$data_raw['trans_type_raw']=$data['trans_type'];
					$data_raw['payment_type']=strtoupper($data['payment_type']);
					$data_raw['delivery_instruction']=$data['delivery_instruction'];

					$this->details=$data_raw;
			 	  // $this->details=array(
			 	    // 'order_id'=>$this->data['order_id'],
			 	    // 'order_from'=>$order_from,
			 	    // 'total'=>AddonMobileApp::prettyPrice($res[0]['total_w_tax']),
			 	    // 'item'=>$data,
			 	    // 'history_data'=>$history_data
			 	  // );
			 	  $this->code=1; $this->msg="OK";
			 } else $this->msg=$this->t("no item found");		
		} else $this->msg=$this->t("sorry but your session has expired please login again");
		$this->output();
	}
	
	public function actionGetLocationDriver() {
		if ( $res=AddonMobileApp::getClientTokenInfo($this->data['client_token'])) {
			
			 if ( $data=query("SELECT a.status,c.driver_id,c.location_lat latitude,c.location_lng longitude,c.last_online, concat(c.first_name,' ',c.last_name) name,c.phone,c.transport_type_id FROM {{order}} a 
							   LEFT JOIN {{driver_task}} b
							   ON a.order_id=b.order_id 
							   LEFT JOIN {{driver}} c
							   ON b.driver_id = c.driver_id
							   WHERE a.order_id=? AND a.client_id=? LIMIT 0,1",array($this->data['order_id'],$res['client_id']))){
				 $data = $data[0];
				 if($data['status'] == 'successful' || $data['status'] == 'failed') {
					 $this->msg=t('You cannot track your driver for order that has been failed or successful');
				 } else {
					// $time1 = new DateTime();
					// $time1->setTimestamp(strtotime("now")); 
					// $time2 = new DateTime();
					// $time2->setTimestamp($data['last_online']); 
					// $interval =  $time2->diff($time1);
					// echo $interval->format("%H hours %i minutes %s seconds");

					 if ($data['driver_id']) {
						$this->code=1;
						$data['last_online'] = date('d/m/Y H:i:s',$data['last_online']);
						$this->details=$data;
					 } else {
						$this->msg=t('Driver has not assigned for this order');
					 }
				 }
			 } else $this->msg=$this->t("order not found");
		} else $this->msg=$this->t("sorry but your session has expired please login again");
		$this->output();
	}
	
	//
	public function actiongetMyAddress()
	{
		if ( $res=AddonMobileApp::getClientTokenInfo($this->data['client_token'])) {			 
			if ( $resp= AddonMobileApp::getAddressBook($res['client_id'])){
				$this->code=1;
				$this->msg="OK";
				$this->details=$resp;
			} else $this->msg = $this->t("no results");
		} else {
			$this->msg=$this->t("sorry but your session has expired please login again");
			$this->code=3;
		}	
		$this->output();
	}
	//
	public function actionGetMyAddressDetails()
	{
		if ( $res=AddonMobileApp::getClientTokenInfo($this->data['client_token'])) {	
			 if ( $resp= Yii::app()->functions->getAddressBookByID($this->data['id'])){			 	 
			 	 $this->code=1; $this->msg="OK";
			 	 $this->details=$resp;
			 } else $this->msg=$this->t("address book details not available");
		} else $this->msg=$this->t("sorry but your session has expired please login again");
		$this->output();
	}
	//
	public function actionSaveMyAddress()
	{	
		$DbExt=new DbExt;
		if ( $res=AddonMobileApp::getClientTokenInfo($this->data['client_token'])) {	
			
			if (isset($this->data['as_default'])){
			   if ($this->data['as_default']==2){
			   	   $stmt="UPDATE 
			   	   {{address_book}}
			   	   SET as_default ='1'
			   	   ";
			   	   $DbExt->qry($stmt);
			   }			
			}					
			$params=array(
			  'client_id'=>$res['client_id'],
			  'street'=>isset($this->data['street'])?$this->data['street']:'',
			  'city'=>isset($this->data['city'])?$this->data['city']:'',
			  'state'=>isset($this->data['state'])?$this->data['state']:'',
			  'zipcode'=>isset($this->data['zipcode'])?$this->data['zipcode']:'',
			  'location_name'=>isset($this->data['location_name'])?$this->data['location_name']:'',
			  'as_default'=>isset($this->data['as_default'])?$this->data['as_default']:1,
			  'date_created'=>date('c'),
			  'ip_address'=>$_SERVER['REMOTE_ADDR'],
			  'country_code'=>Yii::app()->functions->adminSetCounryCode()
			);							
			if ( $this->data['action']=="add"){
				if ( $DbExt->insertData("{{address_book}}",$params)){
					$this->code=1;
					$this->msg="address book added";
					$this->details=$this->data['action'];
				} else $this->msg=$this->t("something went wrong during processing your request");	
			} else {
				unset($params['client_id']);
				unset($params['date_created']);
				if ( $DbExt->updateData("{{address_book}}",$params,'id',$this->data['id'])){
					$this->code=1;				
					$this->msg="successfully updated";
					$this->details=$this->data['action'];
				} else $this->msg=$this->t("something went wrong during processing your request");		
			}
		} else $this->msg=$this->t("sorry but your session has expired please login again");
		$this->output();
	}
	//
	public function actionDeleteMyAddress()
	{
		$DbExt=new DbExt;
		if ( $res=AddonMobileApp::getClientTokenInfo($this->data['client_token'])) {				
			if ( $resp=Yii::app()->functions->getAddressBookByID($this->data['id'])){
				if ( $res['client_id']==$resp['client_id']){
					$stmt="
					DELETE FROM {{address_book}}
					WHERE
					id=".self::q($this->data['id'])."
					";
					if ( $DbExt->qry($stmt)){
						$this->code=1;
						$this->msg="OK";
					} else $this->msg=$this->t("something went wrong during processing your request");		
				} else $this->msg=$this->t("sorry but you cannot delete this records");
			} else $this->msg=$this->t("address book id not found");
		} else $this->msg=$this->t("sorry but your session has expired please login again");
		$this->output();	
	}
	
//----------------------------//
	//
	public function actionregisterUsingFb()
	{
		$DbExt=new DbExt;
		
		if(!isset($this->data['email'])){
			$this->msg=$this->t("Email address is missing");
			$this->output();
		}	
				
		if (!empty($this->data['email']) && !empty($this->data['first_name'])){			
			if ( FunctionsK::emailBlockedCheck($this->data['email'])){
	    		$this->msg=$this->t("Sorry but your facebook account is blocked by website admin");
	    		$this->output();
	    	}	   
	    		   	    	 
	    	$token=AddonMobileApp::generateUniqueToken(15,$this->data['email']);
	    	
	    	//$name=explode(" ",$this->data['name']);	    	
	    	
	    	$params=array(
	    	  'social_strategy'=>'fb_mobile',
	    	  'email_address'=>$this->data['email'],
	    	  'first_name'=>isset($this->data['first_name'])?$this->data['first_name']:'' ,
	    	  'last_name'=>isset($this->data['last_name'])?$this->data['last_name']:'' ,
	    	  'token'=>$token,
	    	  'last_login'=>date('c')
	    	);
	    		    		    	
	    	if ( $res=AddonMobileApp::checkifEmailExists($this->data['email'])){
	    		// update
	    		unset($params['email_address']);
	    		$client_id=$res['client_id'];
	    		if (empty($res['password'])){
	    			$params['password']=md5($this->data['fbid']);
	    		}		    		
	    		if ($DbExt->updateData("{{client}}",$params,'client_id',$client_id)){
	    		   $this->code=1;
		   	   	    $this->msg=$this->t("Login Okay");
		   	   	    
		   	   	    $avatar=AddonMobileApp::getAvatar( $client_id , $res );
		   	   	    
		   	   	    $this->details=array(
		   	   	      'token'=>$token,
		   	   	      'next_steps'=>isset($this->data['next_steps'])?$this->data['next_steps']:'',
		   	   	      'has_addressbook'=>AddonMobileApp::hasAddressBook($client_id)?2:1,
		   	   	      'avatar'=>$avatar,
		   	   	      'client_name_cookie'=>$res['first_name']
		   	   	    );
		   	   	    
		   	   	    //update device client id
		   	   	   if (isset($this->data['device_id'])){
		   	   	       AddonMobileApp::updateDeviceInfo($this->data['device_id'],$client_id);
		   	   	   }
		   	   	    
	    		} else $this->msg=$this->t("something went wrong during processing your request");
	    	} else {
	    		// insert
	    		$params['date_created']=date('c');
	    		$params['password']=md5($this->data['fbid']);
	    		$params['ip_address']=$_SERVER['REMOTE_ADDR'];
	    		
	    		if ($DbExt->insertData("{{client}}",$params)){
	    			$client_id=Yii::app()->db->getLastInsertID();
	    			$this->code=1;
		   	   	    $this->msg=$this->t("Login Okay");
		   	   	    
		   	   	    $avatar=AddonMobileApp::getAvatar( $client_id , array() );
		   	   	    
		   	   	    $this->details=array(
		   	   	      'token'=>$token,
		   	   	      'next_steps'=>isset($this->data['next_steps'])?$this->data['next_steps']:'',
		   	   	      'has_addressbook'=>AddonMobileApp::hasAddressBook($client_id)?2:1,
		   	   	      'avatar'=>$avatar,
		   	   	      'client_name_cookie'=>$this->data['first_name']
		   	   	    );
		   	   	    
		   	   	   //update device client id
		   	   	   if (isset($this->data['device_id'])){
		   	   	       AddonMobileApp::updateDeviceInfo($this->data['device_id'],$client_id);
		   	   	   }
		   	   	    
	    		} else $this->msg=$this->t("something went wrong during processing your request");
	    	}		    	
		} else $this->msg=$this->t("failed. missing email and name");
		$this->output();	
	}
	
	public function actionregisterMobile()
	{	
		// file_put_contents('log6.txt','registerMobile');
		$DbExt=new DbExt;
		$params['device_id']=isset($this->data['registrationId'])?$this->data['registrationId']:'';
		$params['device_platform']=isset($this->data['device_platform'])?$this->data['device_platform']:'';
		
		if (isset($this->data['client_token'])){
			if ( $client=AddonMobileApp::getClientTokenInfo($this->data['client_token'])) {					
				$params['client_id']=$client['client_id'];
				var_dump($params['client_id']);
			} else {
				/*$this->msg="Client id is missing";
				$this->output();*/
			}		
		}

		if (!empty($this->data['registrationId'])){
			$params['date_created']=date('c');
			$params['ip_address']=$_SERVER['REMOTE_ADDR'];
			if ( $res=AddonMobileApp::getDeviceID($this->data['registrationId'])){
				 $DbExt->updateData("{{mobile_registered}}",$params,'id',$res['id']);
				 
				 /*update all old device id of client to inactive*/
				 if(isset($params['client_id'])){
				   if(!empty($params['client_id'])){
				   	  $sql="UPDATE
	         			{{mobile_registered}}
	         			SET status='inactive'
	         			WHERE
	         			client_id=".self::q($params['client_id'])."
	         			AND
	         			device_id<>".self::q($params['device_id'])."
	         			";
	         		    $DbExt->qry($sql);
				   }
				 }
				 
			} else {
				$DbExt->insertData("{{mobile_registered}}",$params);			
			}		
			$this->code=1; $this->msg="OK";
		} else $this->msg="Empty registration id";
		$this->output();	
	}
	
	public function ReverseGeoCoding($latitude,$longitude)
	{
		if (!empty($latitude) && !empty('longitude')){
			$latlng=$latitude.",".$longitude;
			$file="https://maps.googleapis.com/maps/api/geocode/json?latlng=$latlng&sensor=true";
			$key=Yii::app()->functions->getOptionAdmin('google_geo_api_key');		
			if(!empty($key)){
				$file.="&key=".urlencode($key);
			}
			if ($res=@file_get_contents($file)){
				 return $res;
			} else return false;
		} else return false;
	}
	public function actionReverseGeoCoding()
	{		
		gdump("actionReverseGeoCoding".$_SERVER['REQUEST_URI']."
");
	
		if (isset($this->data['latitude']) && !empty($this->data['longitude'])){
			$latlng=$this->data['latitude'].",".$this->data['longitude'];
			$file="https://maps.googleapis.com/maps/api/geocode/json?latlng=$latlng&sensor=true";
			$key=Yii::app()->functions->getOptionAdmin('google_geo_api_key');		
			if(!empty($key)){
				$file.="&key=".urlencode($key);
			}
			if ($res=@file_get_contents($file)){
				echo $res;
				// $res=json_decode($res,true);
				// if (AddonMobileApp::isArray($res)){
					// $this->code=1; $this->msg="OK";
					// $this->details=$res['results'][0]['formatted_address'];
				// } else  $this->msg=$this->t("not available");
			} else $this->msg=$this->t("not available");
		} else $this->msg=$this->t("missing coordinates");
		$this->output();
	}
	
	public function actionSaveSettings()
	{
		$DbExt=new DbExt;					
		if (!empty($this->data['device_id']) || $this->data['device_id']!="null"){
			$params=array(
			  'enabled_push'=>isset($this->data['enabled_push'])?1:2,
			  'date_modified'=>date('c'),
			  'ip_address'=>$_SERVER['REMOTE_ADDR'],
			  'country_code_set'=>isset($this->data['country_code_set'])?$this->data['country_code_set']:''
			);			
			
			$client_id='';
			if ( $client=AddonMobileApp::getClientTokenInfo($this->data['client_token'])) {				
				$client_id=$client['client_id'];
			}
			
			if ($client_id>0){
				$params['client_id']=$client_id;
			}			
			
			if ( $res=AddonMobileApp::getDeviceID($this->data['device_id'])){
				//update								
				if($DbExt->updateData("{{mobile_registered}}",$params,'device_id',$this->data['device_id'])){
					$this->code=1;
					$this->msg=$this->t("Setting saved");
				} else $this->msg=$this->t("something went wrong during processing your request");
			} else {
				//insert				
				$params['device_id']=$this->data['device_id'];
				$params['date_created']=date('c');
				if ($DbExt->insertData("{{mobile_registered}}",$params)){
					$this->code=1;
					$this->msg=$this->t("Setting saved");
				} else $this->msg=$this->t("something went wrong during processing your request");
			}		
		} else $this->msg=$this->t("missing device id");
		$this->output();
	}
	
	public function actionGetSettings()
	{				
		if (!empty($this->data['device_id']) || $this->data['device_id']!="null"){
			$device_id=$this->data['device_id'];			
			if ( $res=AddonMobileApp::getDeviceID($device_id)){				
				$this->code=1; $this->msg="OK";
				$this->details=$res;
			} else $this->msg=$this->t("settings not found");
		} else $this->msg=$this->t("missing device id");
		$this->output();
	}
	


	public function actionValidateCLient()
	{
		$db_ext=new DbExt;  
		
		switch ($this->data['validation_type']) {
			case "mobile_verification":
				if ( $res=AddonMobileApp::verifyMobileCode($this->data['code'],$this->data['client_id'])){
				    
					$params=array( 
					  'status'=>"active",
					  'mobile_verification_date'=>date('c'),
					  'last_login'=>date('c')
					);
					$db_ext->updateData("{{client}}",$params,'client_id',$res['client_id']);
					$this->code=1;
					$this->msg=$this->t("Validation successful");
					$this->details=array(
					  'token'=>$res['token'],
					  'is_checkout'=>$this->data['is_checkout']
					);
					
				} else $this->msg=$this->t("verification code is invalid");
				break;
		
			case "email_verification":	
			    if( $res=Yii::app()->functions->getClientInfo( $this->data['client_id'] )){	
			    	if ($res['email_verification_code']==trim($this->data['code'])){
			    		
			    		$params=array( 
						  'status'=>"active",
						  'last_login'=>date('c')
						);
						$db_ext->updateData("{{client}}",$params,'client_id',$res['client_id']);
			    		
			    	 	$this->code=1;
					    $this->msg=$this->t("Validation successful");
					    $this->details=array(
						  'token'=>$res['token'],
						  'is_checkout'=>$this->data['is_checkout']
						);
					    
			    	} else $this->msg=$this->t("verification code is invalid");
			    } else $this->msg=$this->t("verification code is invalid");
				break;
				
			default:
				$this->msg=$this->t("validation type unrecognize");
				break;
		}
		
		$this->output();
	}
	
	
} /*end class*/