<?php
$menu =  array(  		    		    
    'activeCssClass'=>'active', 
    'encodeLabel'=>false,
    'items'=>array(
        array('visible'=>true,'label'=>'<i class="fa fa-cog"></i>&nbsp; '.AddonMobileApp::t("General Settings"),
        'url'=>array('/mobileapp/index/settings'),'linkOptions'=>array()),
        
        array('visible'=>true,'label'=>'<i class="fa fa-user-plus"></i>&nbsp; '.AddonMobileApp::t('Registered Device'),
        'url'=>array('/mobileapp/index/registereddevice'),'linkOptions'=>array()),
        
        array('visible'=>true,'label'=>'<i class="fa fa-mobile"></i>&nbsp; '.AddonMobileApp::t("Push Notification Logs"),
        'url'=>array('/mobileapp/index/pushlogs'),'linkOptions'=>array()),
        
        array('visible'=>true,'label'=>'<i class="fa fa-info-circle"></i>&nbsp; '.AddonMobileApp::t("Push CronJobs"),
        'url'=>array('/mobileapp/index/pushhelp'),'linkOptions'=>array()),
        


     )   
);       
?>
<div class="menu">
<?php $this->widget('zii.widgets.CMenu', $menu);?>
</div>