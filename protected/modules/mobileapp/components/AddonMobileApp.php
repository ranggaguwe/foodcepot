<?php
class AddonMobileApp
{
	
	public static function moduleBaseUrl()
	{
		return Yii::app()->getBaseUrl(true)."/protected/modules/mobileapp";
	}

	public static function q($data)
	{
		return Yii::app()->db->quoteValue($data);
	}
	
    public static function t($message='')
	{
		return Yii::t("default",$message);
	}	
	
	public static function isMerchantOpen($merchant_id='')
	{
		$open = Yii::app()->functions->isMerchantOpen($merchant_id); 			 			
	    $preorder= Yii::app()->functions->getOption("merchant_preorder",$merchant_id);
			
		$now=date('Y-m-d');				 			
	    if ( $m_holiday=Yii::app()->functions->getMerchantHoliday($merchant_id)){  
      	   if (in_array($now,(array)$m_holiday)){
      	   	  $open=false;
      	   }
        }
        
        if (!$open){
        	if($preorder){
        		$open=true;
        	}
        }
               
        return $open;
	}
	
	public static function isCashAvailable($merchant_id='')
	{
		$cod=self::t("Cash on delivery available");
        if ( Yii::app()->functions->isMerchantCommission($merchant_id)){
        	$paymentgateway=getOptionA('paymentgateway');
        	$paymentgateway=!empty($paymentgateway)?json_decode($paymentgateway,true):false;
        	if($paymentgateway!=false){
        		if(!in_array('cod',(array)$paymentgateway)){
        			$cod='';
        		}
        	}
        } else {
        	if (getOption($merchant_id,'merchant_disabled_cod')!=""){
        		$cod='';
        	}
        }
        return $cod;
	}
	
	public static function getMerchantLogo($merchant_id='')
	{		
		if ( !$logo = getOption($merchant_id,'merchant_photo') ){			
			$logo = Yii::app()->functions->getOptionAdmin('mobile_default_image_not_available');
			if (empty($logo)){
			   $logo="mobile-default-logo.png";
			}
		}		
		$path_to_upload=Yii::getPathOfAlias('webroot')."/upload/";			
		if (file_exists($path_to_upload."/$logo")){
			return Yii::app()->getBaseUrl(true)."/upload/$logo";
		} 
		return self::moduleBaseUrl()."/assets/images/$logo";				
	}
	
	public static function getImage($image='')
	{		
		$default="mobile-default-logo.png";
		$path_to_upload=Yii::getPathOfAlias('webroot')."/upload/";				
		
		if (empty($image)){
			$image=Yii::app()->functions->getOptionAdmin('mobile_default_image_not_available');
		}	
		
		if (!empty($image)){			
			if (file_exists($path_to_upload."/$image")){							
				$default=$image;				
				$url = Yii::app()->getBaseUrl(true)."/upload/$default";
			} else $url=self::moduleBaseUrl()."/assets/images/$default";
		} else $url=self::moduleBaseUrl()."/assets/images/$default";
		return $url;
	}
	
	public static function merchantInformation($merchant_id='')
	{
		$data='';
		
		if ($merchantinfo=Yii::app()->functions->getMerchant($merchant_id)){
					
			$data['merchant_id']=$merchant_id;
			$data['merchant_name']=$merchantinfo['merchant_name'];
			$data['merchant_phone']=$merchantinfo['merchant_phone'];
			$data['contact_name']=$merchantinfo['contact_name'];
			$data['contact_phone']=$merchantinfo['contact_phone'];
			$data['address']=$merchantinfo['street']." ".$merchantinfo['city']." ".
			$merchantinfo['state']." ".$merchantinfo['post_code'];//." ".$data['country'];
						
			/*check if mechant is open*/			
			$data['merchant_close_store']=getOption($merchant_id,'merchant_close_store')=="yes"?true:false;
			
 			
 			$data['logo']=AddonMobileApp::getMerchantLogo($merchant_id);
 			
 			$delivery_fee=getOption($merchant_id,'merchant_delivery_charges');
 			$delivery_fee_raw=$delivery_fee;
 			if (!empty($delivery_fee)){
 				$delivery_fee=displayPrice(getCurrencyCode(),prettyFormat($delivery_fee));
 			}
			 			
 			$data['ratings']=Yii::app()->functions->getRatings($merchant_id);
 		
 			
 			return $data; 	
		}
		return false;
	}
	
	public static function prettyPrice($amount='')
	{
		if(!empty($amount)){
			return displayPrice(getCurrencyCode(),prettyFormat($amount));
		}
		return 0;
	}
	
	public static function isArray($data='')
	{
		if (is_array($data) && count($data)>=1){
			return true;
		}
		return false;
	}	
	
	public static function getDeliveryCharges($merchant_id='',$unit='',$distance='')
	{				
		$delivery_fee=0;
		
		$default_delivery_charges=getOption($merchant_id,'merchant_delivery_charges');
				
		if ($default_delivery_charges<0){
			return false;
		}

		$FunctionsK=new FunctionsK();
		$delivery_fee=$FunctionsK->getDeliveryChargesByDistance(
    	$merchant_id,
    	$distance,
    	$unit,
    	$default_delivery_charges); 			
    	return array(
    	  'delivery_fee'=>$delivery_fee,
    	  'unit'=>$unit,
    	  'use_distance'=>$distance
    	);	
		
		return false;		
	}
	
	public static function getDistance($merchant_id='',$customer_address='')
	{
		$merchant_distance_type=Yii::app()->functions->getOption("merchant_distance_type",$merchant_id);
		
		$DbExt=new DbExt; 
		$stmt="SELECT concat(street,' ',city,' ',state,' ',post_code,' ',country_code) as merchant_address
		FROM
		{{merchant}}
		WHERE
		merchant_id=".self::q($merchant_id)."
		LIMIT 0,1
		";
		$merchant_address='';
		if($res=$DbExt->rst($stmt)){
			$merchant_address=$res[0]['merchant_address'];
		}
		
		$miles=getDeliveryDistance2($customer_address,$merchant_address); 
		if (self::isArray($miles)){			
			if ( $merchant_distance_type=="km"){
				$unit="km";
				$use_distance=$miles['km'];
			} else {
				$unit='miles';
				$use_distance=$miles['mi'];
			}
			if (preg_match("/ft/i",$miles['mi'])) {
				$unit='ft';
			}
			
			$use_distance=str_replace(array(
			 'ft','mi','km',','
			),'',$use_distance);
			
			return array(
			  'unit'=>$unit,
			  'distance'=>$use_distance
			);
		}	
		return false;
	}
	
	public static function parseValidatorError($error='')
	{
		$error_string='';
		if (is_array($error) && count($error)>=1){
			foreach ($error as $val) {
				$error_string.="$val\n";
			}
		}
		return $error_string;		
	}		
		
	public static function generateUniqueToken($length,$unique_text=''){	
		$key = '';
	    $keys = array_merge(range(0, 9), range('a', 'z'));	
	    for ($i = 0; $i < $length; $i++) {
	        $key .= $keys[array_rand($keys)];
	    }	
	    return $key.md5($unique_text);
	}
			
	public static function computeCart($data='')
	{
		if ($data['transaction_type']=="null" || empty($data['transaction_type'])){
			$data['transaction_type']="express";
		}
		
		if ($data['delivery_date']=="null" || empty($data['delivery_date'])){
			$data['delivery_date']=date("Y-m-d");
		}
		
		
		$cart_content='';
		$subtotal=0;
		$taxable_total=0;
		$item_total=0;
		
		Yii::app()->functions->data="list";
		// $subcat_list=Yii::app()->functions->getSubcategory2($mtid);		
		
		$cart=json_decode($data['cart'],true);
		
		$list_merchant = array();
		$tax_merchants = array();

		$weight_total = 0;
		if (is_array($cart) && count($cart)>=1){
			
			foreach ($cart as $val) {
				
		    	$item_price=0;
		    	$item_size='';
				
				//price decision retail price or wholesal price
				$food=Yii::app()->functions->getFoodItem($val['item_id']);
				$item_price=$food['retail_price'];
				
				$wholesale_price = getWhere('wholesale_price','item_id',$val['item_id']);
				if($wholesale_price) {
					foreach($wholesale_price as $w) {
						if($val['qty'] >= $w['start'] && $val['qty'] <= $w['end']) {
							$item_price = $w['price'];
						}
					}
				}
						    	
		    	/*check if item qty is less than 1*/
		    	if($val['qty']<1){
		    		$val['qty']=1;
		    	}			    
		    	
		    	$discounted_price=0;
		    	if ($food['discount']>0){
		    		$discounted_price=$item_price-$food['discount'];
		    		$subtotal+=($val['qty']*$discounted_price);
		    	} else {
		    		$subtotal+=($val['qty']*$item_price);
		    	}
		    				    	
		    	//$subtotal+=($val['qty']*$item_price);
		    	// if ( $food['non_taxable']==1){
		    	   // $taxable_total=$subtotal;
		    	// }
		    	
		    	$item_total+=$val['qty'];

		    	$discount_amt=0;
		    	if (isset($val['discount'])){
		    		$discount_amt=$val['discount'];
		    	}
				$total_per_item = $val['qty']*($item_price-$discount_amt);
				
				//menghitung subtotal khusus masing2 merchant
				if(isset($list_merchant[$food['merchant_id']])) {
					$list_merchant[$food['merchant_id']] = $total_per_item + $list_merchant[$food['merchant_id']];
				} else {
					$list_merchant[$food['merchant_id']] = $total_per_item;
				}
				//get merchants list from product
				// $list_merchant[] = $food['merchant_id'];
				
				//kalkulasi tax untuk setiap merchant
				//sesuai produknya masing2
				if(isset($tax_merchants[$food['merchant_id']])) {
					//jika key merchant_id sudah diset
					$tax_merchants[$food['merchant_id']] += $food['tax'] * $total_per_item / 100;
				} else {
					//jika key merchant_id belum diset
					$tax_merchants[$food['merchant_id']] = $food['tax'] * $total_per_item / 100;
				}
				
				
				
		    	$cart_content[]=array(
					'item_id'=>$val['item_id'],
					'merchant_id'=>$food['merchant_id'],
					'item_name'=>$food['item_name'],
					'item_description'=>$food['item_description'],
					'qty'=>$val['qty'],
					'price'=>$item_price,
					'price_pretty'=>AddonMobileApp::prettyPrice($item_price),
					'total'=>$total_per_item,
					'total_pretty'=>AddonMobileApp::prettyPrice($total_per_item),
					'discount'=>isset($val['discount'])?$val['discount']:'',
					'discounted_price'=>$discounted_price,
					'discounted_price_pretty'=>AddonMobileApp::prettyPrice($discounted_price),	
					'order_notes'=>$val['order_notes'],
					'non_taxable'=>$food['non_taxable'],
					'tax'=>$food['tax'],
					'weight'=>$food['weight'],
					'photo'=>$food['photo'],
					'uom'=>$food['uom'],
		    	);
				$weight_total += $food['weight']?$food['weight']*$val['qty']:500*$val['qty'];
				// var_dump($food);
		    } /*end foreach*/
		    
            $ok_distance=2;
		    $delivery_charges=0;
		    $distance='';
		    
			if ( $data['transaction_type']=="express" || $data['transaction_type']=="economy"){
		    	/*CHANGE TO NEW DISTANCE CALCULATION*/
		    	$client_address=$data['street']." ";
		    	$client_address.=$data['city']." ";
			    $client_address.=$data['state']." ";
			    $client_address.=$data['zipcode']." ";
			    
				
				//the keys is merchant_id
				$list_merchant2 = array_keys($list_merchant);
				$ret_delivery = AddonMobileApp::calculateDeliveryCharges($list_merchant2,$data,$weight_total,$data['transaction_type']);
				$distance=array(
					'unit'=>$ret_delivery['distance_type'],
					'distance'=>$ret_delivery['distance'],
					'distance_is_valid'=>$ret_delivery['valid_distance'],
				);
				$delivery_charges=$ret_delivery['delivery_fee'];
				
		    	if(!$ret_delivery['valid_distance']){
					$ok_distance = 1;
				}
		    }
			
			$merchant_tax_percent=0;
	        $tax=0;
	        		        			    
			$cart_final_content=array(
			  'cart'=>$cart_content,
			  'sub_total'=>array(
			    'amount'=>$subtotal,
			    'amount_pretty'=>AddonMobileApp::prettyPrice($subtotal)
			  )			      
			);				

			if ($delivery_charges>0){
				$cart_final_content['delivery_charges']=array(
				  'amount'=>$delivery_charges,
				  'amount_pretty'=>AddonMobileApp::prettyPrice($delivery_charges)
				);
			}
			
			$tax=0;
			foreach($tax_merchants as $t) {
				$tax += $t;
			}
			if ($tax>0){
				$cart_final_content['tax']=array(
				  'tax'=>0,
				  'amount_raw'=>$tax,
				  'amount'=>AddonMobileApp::prettyPrice($tax),
				  'tax_pretty'=>self::t("Tax")
				);
			}
			
			$grand_total=$subtotal+$delivery_charges+$tax;
			$cart_final_content['grand_total']=array(
			  'amount'=>$grand_total,
			  'amount_pretty'=>AddonMobileApp::prettyPrice($grand_total)
			);
			
			/*validation*/																
			$validation_msg='';
			
			// $list_merchant = array_unique($list_merchant);
			return array(
			  'cart'=>$cart_final_content,
			  'validation_msg'=>$validation_msg,
			  'distance'=>$distance,
			  'list_merchant'=>$list_merchant,
			  'tax_merchants'=>$tax_merchants,
			  'ret_delivery'=>$ret_delivery,
			  'grand_total'=>$grand_total,
			  'weight_total'=>$weight_total,
			);
			
		} /*end is array*/
		
		return false;
	}
	
	public static function cartMobile2WebFormat($data='',$post_data='')
	{
		//dump($post_data);
		$json_data='';
		if (self::isArray($data['cart']['cart'])){
			foreach ($data['cart']['cart'] as $val) {				
				$sub_item=''; $addon_qty=''; $addon_ids='';
				
				$json_data[]=array(
				  'merchant_id'=>$val['merchant_id'],
				  'item_id'=>$val['item_id'],
				  'item_name'=>isset($val['item_name'])?$val['item_name']:'',
				  'discount'=>$val['discount'],
				  'price'=>$val['price'],
				  'qty'=>$val['qty'],
				  'notes'=>$val['order_notes'],
				  'order_notes'=>$val['order_notes'],				  
				  'non_taxable'=>isset($val['non_taxable'])?$val['non_taxable']:1,
				  'tax'=>isset($val['tax'])?$val['tax']:'',
				  'weight'=>isset($val['weight'])?$val['weight']:'',
				  'photo'=>isset($val['photo'])?$val['photo']:'',
				  'uom'=>isset($val['uom'])?$val['uom']:'',
				);
			}
			//dump($json_data);
			return $json_data;
		}
		return json_encode(array());
	}
	
	public static function getClientTokenInfo($token='')
	{
		if(empty($token)){
			return false;
		}
		$DbExt=new DbExt; 
		$stmt="
		SELECT * FROM
		{{client}}
		WHERE
		token=".self::q($token)."
		LIMIT 0,1
		";				
		if ($res=$DbExt->rst($stmt)){			
			return $res[0];
		}
		return false;
	}
	
	public static function getMerchantPaymentMethod($mtid='')
	{
		$merchant_payment_list='';
						
		$mobile_payment=array('cod','paypal','pyr','pyp','atz','stp');
			
		$payment_list=getOptionA('paymentgateway');
		$payment_list=!empty($payment_list)?json_decode($payment_list,true):false;		
		
		$pay_on_delivery_flag=false;
		$paypal_flag=false;
		
		$paypal_credentials='';
		
		$stripe_publish_key='';
		
		/*check master switch for offline payment*/
		if(is_array($payment_list) && count($payment_list)>=1){
		   $payment_list=array_flip($payment_list);
		   
		    $merchant_switch_master_cod=getOption($mtid,'merchant_switch_master_cod');
			if($merchant_switch_master_cod==2){
			   unset($payment_list['cod']);
			}
			$merchant_switch_master_pyr=getOption($mtid,'merchant_switch_master_pyr');
			if($merchant_switch_master_pyr==2){
			   unset($payment_list['pyr']);
			}
		}
		
		if(is_array($payment_list) && count($payment_list)>=1){
		   $payment_list=array_flip($payment_list);
		}		
		
		if (AddonMobileApp::isArray($payment_list)){			
			foreach ($mobile_payment as $val) {
				if(in_array($val,(array)$payment_list)){					
					switch ($val) {
						case "cod":			
						    if (Yii::app()->functions->isMerchantCommission($mtid)){
						    	$merchant_payment_list[]=array(
								  'icon'=>'fa-usd',
								  'value'=>$val,
								  'label'=>self::t("Cash On delivery")
								);
						    	continue;
						    }
							if ( getOption($mtid,'merchant_disabled_cod')!="yes"){
								$merchant_payment_list[]=array(
								  'icon'=>'fa-usd',
								  'value'=>$val,
								  'label'=>self::t("Cash On delivery")
								);
							}
							break;
						default:
							break;
					}					
				}			
			}
			
			if (AddonMobileApp::isArray($merchant_payment_list)){				
				return $merchant_payment_list;
			}	
		} 
		return false;
	}
	
	public static function getOperationalHours($merchant_id='')
	{
        $stores_open_day=Yii::app()->functions->getOption("stores_open_day",$merchant_id);
		$stores_open_starts=Yii::app()->functions->getOption("stores_open_starts",$merchant_id);
		$stores_open_ends=Yii::app()->functions->getOption("stores_open_ends",$merchant_id);
		$stores_open_custom_text=Yii::app()->functions->getOption("stores_open_custom_text",$merchant_id);
		
		$stores_open_day=!empty($stores_open_day)?(array)json_decode($stores_open_day):false;
		$stores_open_starts=!empty($stores_open_starts)?(array)json_decode($stores_open_starts):false;
		$stores_open_ends=!empty($stores_open_ends)?(array)json_decode($stores_open_ends):false;
		$stores_open_custom_text=!empty($stores_open_custom_text)?(array)json_decode($stores_open_custom_text):false;
		
		
		$stores_open_pm_start=Yii::app()->functions->getOption("stores_open_pm_start",$merchant_id);
		$stores_open_pm_start=!empty($stores_open_pm_start)?(array)json_decode($stores_open_pm_start):false;
		
		$stores_open_pm_ends=Yii::app()->functions->getOption("stores_open_pm_ends",$merchant_id);
		$stores_open_pm_ends=!empty($stores_open_pm_ends)?(array)json_decode($stores_open_pm_ends):false;		
						
		$tip='';						
		$open_starts='';
		$open_ends='';
		$open_text='';		
		if (is_array($stores_open_day) && count($stores_open_day)>=1){
			foreach ($stores_open_day as $val_open) {	
				if (array_key_exists($val_open,(array)$stores_open_starts)){
					$open_starts=timeFormat($stores_open_starts[$val_open],true);
				}							
				if (array_key_exists($val_open,(array)$stores_open_ends)){
					$open_ends=timeFormat($stores_open_ends[$val_open],true);
				}							
				if (array_key_exists($val_open,(array)$stores_open_custom_text)){
					$open_text=$stores_open_custom_text[$val_open];
				}					
				
				$pm_starts=''; $pm_ends=''; $pm_opens='';
				if (array_key_exists($val_open,(array)$stores_open_pm_start)){
					$pm_starts=timeFormat($stores_open_pm_start[$val_open],true);
				}											
				if (array_key_exists($val_open,(array)$stores_open_pm_ends)){
					$pm_ends=timeFormat($stores_open_pm_ends[$val_open],true);
				}								
							
				$full_time='';
				if (!empty($open_starts) && !empty($open_ends)){					
					$full_time=$open_starts." - ".$open_ends."&nbsp;&nbsp;";
				}			
				if (!empty($pm_starts) && !empty($pm_ends)){
					if ( !empty($full_time)){
						$full_time.=" / ";
					}				
					$full_time.="$pm_starts - $pm_ends";
				}
																				
				$tip.= ucwords(self::t($val_open))." ".$full_time." ".$open_text."<br/>";
				
				$open_starts='';
		        $open_ends='';
		        $open_text='';
			}
		} else $tip=self::t("Not available.");
		return $tip;
	}	
	
	public static function previewMerchantReview($mtid='')
	{
		$DbExt=new DbExt; 
		$stmt="
		SELECT 
		(
		  select count(*) as total_review
		  from {{review}}
		  where
		  merchant_id=".self::q($mtid)."
		) as total_review
		 FROM
		{{review}} a
		WHERE
		merchant_id=".self::q($mtid)."
		AND status IN ('publish','published')
		ORDER BY date_created DESC
		LIMIT 0,1
		";
		if ( $res=$DbExt->rst($stmt)){
			return $res[0];
		}
		return false;
	}
	
	public static function getOrderDetails($order_id='')
	{
		$DbExt=new DbExt; 
		$stmt="
		SELECT a.item_id, a.item_name,
		a.qty,
		(
		  select total_w_tax 
		  from {{order}}
		  where
		  order_id= a.order_id
		) as total_w_tax
		FROM
		{{order_details}} a
		WHERE
		order_id=".self::q($order_id)."
		ORDER BY id ASC
		";
		if ($res=$DbExt->rst($stmt)){
			return $res;
		}
		return false;
	}
	
    public static function getAddressBook($client_id='')
    {
    	$db_ext=new DbExt;    	
    	/*$stmt="SELECT  
    	       concat(street,' ',city,' ',state,' ',zipcode) as address,
    	       id,location_name,country_code,as_default,
    	       street,city,state,zipcode,location_name
    	       FROM
    	       {{address_book}}
    	       WHERE
    	       client_id =".self::q($client_id)."
    	       ORDER BY id DESC    	       
    	";*/    	    	
    	$stmt="SELECT  
    	       concat(street,' ',city,' ',state,' ',zipcode) as address,
    	       a.id,
    	       a.location_name,
    	       a.country_code,
    	       a.as_default,
    	       a.street,
    	       a.city,
    	       a.state,
    	       a.zipcode,
    	       a.location_name,
    	       (
    	       select contact_phone from {{client}} where client_id=a.client_id limit 0,1
    	       ) as contact_phone
    	       FROM
    	       {{address_book}} a
    	       WHERE
    	       client_id =".self::q($client_id)."
    	       ORDER BY id DESC    	       
    	";
    	if ($res=$db_ext->rst($stmt)){    		
    		return $res;
    	}
    	return false;
    } 	        	
    
    public static function hasAddressBook($client_id='')
    {
    	$db_ext=new DbExt;    	
    	$stmt="SELECT * FROM
    	{{address_book}}
    	WHERE client_id =".self::q($client_id)."
    	LIMIT 0,1
    	";    	    	
    	if ($res=$db_ext->rst($stmt)){    		
    		return $res;
    	}
    	return false;
    }
    
    public static function checkifEmailExists($email='')
    {
    	$db_ext=new DbExt;    	
    	$stmt="SELECT client_id,email_address,first_name,last_name,
    	password FROM
    	{{client}}
    	WHERE email_address =".self::q($email)."
    	LIMIT 0,1
    	";    	    	
    	if ($res=$db_ext->rst($stmt)){    		
    		return $res[0];
    	}
    	return false;
    }
    
    public static function getDeviceID($device_id='')
    {
    	$db_ext=new DbExt;    	
    	$stmt="SELECT * FROM
    	{{mobile_registered}}
    	WHERE
    	device_id=".self::q($device_id)."
    	LIMIT 0,1
    	";    	    	
    	if ($res=$db_ext->rst($stmt)){    		
    		return $res[0];
    	}
    	return false;
    }
    
   public static function getMerchantInfo($merchant_id='')
    {
    	$db_ext=new DbExt;    	
    	$stmt="SELECT * FROM
    	{{merchant}}
    	WHERE
    	merchant_id=".self::q($merchant_id)."
    	LIMIT 0,1
    	";    	    	
    	if ($res=$db_ext->rst($stmt)){    		
    		return $res[0];
    	}
    	return false;
    }    
    
    public static function updateDeviceInfo($device_id='',$client_id='')
    {    	    	
    	
    	$db_ext=new DbExt;   
         if (!empty($client_id)){
         	if (!empty($device_id)){
         		$params=array(
         		  'client_id'=>$client_id,
         		  'date_modified'=>date('c'),
         		  'ip_address'=>$_SERVER['REMOTE_ADDR'],
         		  'status'=>'active'
         		);      
         		if ( $res=self::getDeviceID($device_id)){         			
         			$db_ext->updateData("{{mobile_registered}}",$params,'device_id',$device_id);
         			
         			/*update all the registered all device to inactive*/
         			$sql="UPDATE
         			{{mobile_registered}}
         			SET status='inactive'
         			WHERE
         			client_id=".self::q($client_id)."
         			AND
         			device_id<>".self::q($device_id)."
         			";
         		    $db_ext->qry($sql);
         		
         		} else {
         			// do nothing
         		}
         	}
         }
    }
    
    public static function getDeviceByID($id='')
    {
    	$db_ext=new DbExt;    	
    	$stmt="SELECT * FROM
    	{{mobile_registered_view}}
    	WHERE
    	id=".self::q($id)."
    	LIMIT 0,1
    	";    	    	
    	if ($res=$db_ext->rst($stmt)){    		
    		return $res[0];
    	}
    	return false;
    }    
    
    public static function sendPush($platform='Android',$api_key='',$device_id='',$message='')
    {    	
    	if (empty($api_key)){
    		return array(
    		  'success'=>0,
    		  'results'=>array(
    		     array(
    		       'error'=>'missing api key'
    		     )
    		  )
    		);
    	}
    	if (empty($device_id)){
    		return array(
    		  'success'=>0,
    		  'results'=>array(
    		     array(
    		       'error'=>'missing device id'
    		     )
    		  )
    		);
    	}
    	    	
    	// $url = 'https://fcm.googleapis.com/fcm/send';//'https://android.googleapis.com/gcm/send';
		// $fields = array(
           // 'registration_ids' => array($device_id),
           // 'data' => $message,
        // );
        // //dump($fields);
        
        // $headers = array(
		  // 'Authorization: key=' . $api_key,
		  // 'Content-Type: application/json'
        // );
        // //dump($headers);
        
        // $ch = curl_init();
        // curl_setopt($ch, CURLOPT_URL, $url);
        // curl_setopt($ch, CURLOPT_POST, true);
        // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        // curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));		
		// $result = curl_exec($ch);
		// if ($result === FALSE) {
		    // //die('Curl failed: ' . curl_error($ch));
		   // return array(
    		  // 'success'=>0,
    		  // 'results'=>array(
    		     // array(
    		       // 'error'=>'Curl failed: '. curl_error($ch)
    		     // )
    		  // )
    		// );
		// }
		
        // curl_close($ch);
        // //echo $result; 
        // $result=!empty($result)?json_decode($result,true):false;
        // //dump($result);
        // if ($result==false){
        	// return array(
    		  // 'success'=>0,
    		  // 'results'=>array(
    		     // array(
    		       // 'error'=>'invalid response from push service'
    		     // )
    		  // )
    		// );
        // }
		
		
		
		//--------------FIREBASEE------------
		
define( 'API_ACCESS_KEY', $api_key );
$registrationIds = array( $device_id );
// prep the bundle
$msg = array
(
	'body' 	=> $message,
	'title'		=> $message,
	'vibrate'	=> 1,
	'sound'		=> 1,
);
$fields = array
(
	'registration_ids' 	=> $registrationIds,
	'notification'			=> $msg
);
 
$headers = array
(
	'Authorization: key=' . API_ACCESS_KEY,
	'Content-Type: application/json'
);
 
$ch = curl_init();
curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
curl_setopt( $ch,CURLOPT_POST, true );
curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
$result = curl_exec($ch );
curl_close( $ch );
echo "ok";
echo $result;
echo "ok";

		
		
		//-------------END FIREBASE-----------
		
		
		
		
        return $result;   
    }
    
    public static function savedOrderPushNotification($data='')
    {    	
    	if (!self::isArray($data)){
    		return ;
    	}
    	    
    	$push_title=getOptionA('mobile_push_order_title');
    	$push_message=getOptionA('mobile_push_order_message');    	
    	
    	if (empty($push_message)){
    		$push_message=$data['remarks'];
    	}
    	
    	if (empty($push_title)){
    		return ;
    	}
    	if (empty($push_message)){
    		return ;
    	}
    	
    	$push_title=Yii::app()->functions->smarty('order_id',$data['order_id'],$push_title);
    	$push_title=Yii::app()->functions->smarty('order_status',$data['status'],$push_title);
    	
    	$push_message=Yii::app()->functions->smarty('order_id',$data['order_id'],$push_message);
    	$push_message=Yii::app()->functions->smarty('order_status',$data['status'],$push_message);
    	
    	$stmt="
    	SELECT a.order_id,
    	a.merchant_id,
    	a.client_id,
    	b.client_name,
    	b.device_platform,
    	b.device_id,
    	b.enabled_push    	
    	FROM
    	{{order}} a
    	LEFT JOIN {{mobile_registered_view}} b
    	ON
    	a.client_id=b.client_id
    	WHERE
    	order_id=".q($data['order_id'])."
    	ORDER BY id DESC
    	LIMIT 0,1
    	";
    	$db_ext=new DbExt; 
    	if ( $res=$db_ext->rst($stmt)){
    		$val=$res[0];    		
    		if ($val['enabled_push']==1){
    			$params=array(
    			  'client_id'=>$val['client_id'],
    			  'client_name'=>$val['client_name'],
    			  'device_platform'=>$val['device_platform'],
    			  'device_id'=>$val['device_id'],
    			  'push_title'=>$push_title,
    			  'push_message'=>$push_message,
    			  'date_created'=>date('c'),
    			  'ip_address'=>$_SERVER['REMOTE_ADDR']
    			);    			
    			$db_ext->insertData("{{mobile_push_logs}}",$params);
    		}    	
    	}
    }
        
    public static function sendOrderEmail($data='',$info='',$order_id='',$full_data='',$ok_send_notification=true)
    {
    	
    	if (!self::isArray($data)){
    	    return false;
    	}
    	    	        	
    	$merchant_id=isset($info['merchant_id'])?$info['merchant_id']:'';
    	    	
    	if (!is_numeric($merchant_id)){
    		return false;
    	}    
    	    		      
        if($client_info=Yii::app()->functions->getClientInfo($info['client_id'])){
	        $client_fullname=$client_info['first_name']." ".$client_info['last_name'];
	        $client_email=$client_info['email_address'];
        } else {
        	$client_fullname='';
        	$client_email='';
        }    
                
        if($merchant_info=self::getMerchantInfo($merchant_id)){        
	        $merchant_address=$merchant_info['street']." ";
		    $merchant_address.=$merchant_info['city']." ";
		    $merchant_address.=$merchant_info['state']." ";
		    $merchant_address.=$merchant_info['post_code']." ";
        } else $merchant_address='';
	    
        if (isset($full_data['street']) && isset($full_data['city'])){
		    $customer_address=$full_data['street']." ";
			$customer_address.=$full_data['city']." ";
			$customer_address.=$full_data['state']." ";
			$customer_address.=$full_data['zipcode']." ";
        } else $customer_address='';
		    		        
        $print[]=array(
          'label'=>Yii::t("default","Customer Name"),
          'value'=>$client_fullname
        );
                
        $print[]=array(
	         'label'=>Yii::t("default","Merchant Name"),
	         'value'=>$merchant_info['merchant_name']
	       );
	       
	    if (!empty($merchant_info['abn'])){
		  $print[]=array(
	         'label'=>Yii::t("default","ABN"),
	         'value'=>$merchant_info['abn']
	       );		
	    }
		$print[]=array(
	         'label'=>Yii::t("default","Telephone"),
	         'value'=>$merchant_info['merchant_phone']
	       );		
		$print[]=array(
	         'label'=>Yii::t("default","Address"),
	         'value'=>$merchant_address
	       );		
		$print[]=array(
	         'label'=>Yii::t("default","TRN Type"),
	         'value'=>$info['trans_type']
	       );		
		$print[]=array(
	         'label'=>Yii::t("default","Payment Type"),
	         'value'=>strtoupper($info['payment_type'])
	       );				
	       	       
	    if ( isset($info['payment_provider_name'])){
	    	$print[]=array(
	         'label'=>Yii::t("default","Card#"),
	         'value'=>strtoupper($info['payment_provider_name'])
	       );
	    }   
	    	        
	    
	    $print[]=array(
          'label'=>Yii::t("default","Reference #"),
          'value'=>Yii::app()->functions->formatOrderNumber($order_id)
        );

        $trn_date=date('M d,Y G:i:s',strtotime($info['date_created']));
	    $trn_date= Yii::app()->functions->translateDate($trn_date);
	         
        $print[]=array(
         'label'=>Yii::t("default","TRN Date"),
         'value'=>$trn_date
        );       
        
        if ($info['trans_type']=="delivery"){
        	$print[]=array(
		         'label'=>Yii::t("default","Delivery Date"),
		         'value'=>$info['delivery_date']
		     );        
                
	        if (isset($info['delivery_time'])){
	        	if (!empty($info['delivery_time'])){
	        		$print[]=array(
			         'label'=>Yii::t("default","Delivery Time"),
			         'value'=>$info['delivery_time']
			       );
	        	}     
	        } 
	        		        
	        $print[]=array(
			  'label'=>Yii::t("default","Deliver to"),
			  'value'=>$customer_address
			);
					
			$print[]=array(
			  'label'=>Yii::t("default","Delivery Instruction"),
			  'value'=>$info['delivery_instruction']
			);
			
			$print[]=array(
			  'label'=>Yii::t("default","Location Name"),
			  'value'=>$full_data['location_name']
			);
			
			$print[]=array(
			  'label'=>Yii::t("default","Contact Number"),
			  'value'=>$full_data['contact_phone']
			);
			
			if (isset($full_data['order_change'])){
			   if ($full_data['order_change']>0){
			   	    $print[]=array(
					  'label'=>Yii::t("default","Change"),
					  'value'=>normalPrettyPrice($full_data['order_change'])
					);
			   }		
			}    		
			
        } else {
        	//pickup
        	
        	if (isset($full_data['contact_phone'])){
        	$print[]=array(
			  'label'=>Yii::t("default","Contact Number"),
			  'value'=>$full_data['contact_phone']
			);
        	}
			
			$print[]=array(
		         'label'=>Yii::t("default","Pickup Date"),
		         'value'=>$info['delivery_date']
		     );        
                
	        if (isset($info['delivery_time'])){
	        	if (!empty($info['delivery_time'])){
	        		$print[]=array(
			         'label'=>Yii::t("default","Pickup Time"),
			         'value'=>$info['delivery_time']
			       );
	        	}     
	        }   
	        
	        if (isset($full_data['order_change'])){
			   if ($full_data['order_change']>0){
			   	    $print[]=array(
					  'label'=>Yii::t("default","Change"),
					  'value'=>normalPrettyPrice($full_data['order_change'])
					);
			   }
			} 		
        	
        }    
        				                
        $item='';        
        if (self::isArray($data['cart'])){
        	foreach ($data['cart'] as $val) {
        		
      
        		
        		$item[]=array(
        		  'item_id'=>$val['item_id'],
        		  'item_name'=>$val['item_name'],
        		  'size_words'=>$val['size'],
        		  'qty'=>$val['qty'],
        		  'normal_price'=>$val['price'],
        		  'discounted_price'=>$val['discounted_price']>0?$val['discounted_price']:$val['price'],
        		  'order_notes'=>$val['order_notes'],
        		  'non_taxable'=>1,
        		);
        	}
        }
        
        $paypal_card_fee=isset($info['card_fee'])?$info['card_fee']:0;        
        
        $total=array(
          'mid'=>$merchant_id,
          'subtotal'=>$data['sub_total']['amount'],
          'delivery_charges'=>isset($data['delivery_charges']['amount'])?$data['delivery_charges']['amount']:'',
          'taxable_total'=>$data['tax']['amount_raw'],
          'tax_amt'=>str_replace(array("Tax","%"),'',$data['tax']['tax_pretty']),
          'total'=>$data['grand_total']['amount']+$paypal_card_fee,
          'curr'=>Yii::app()->functions->adminCurrencySymbol(),
          'packaging'=>isset($data['packaging']['amount'])?$data['packaging']['amount']:'',
          'card_fee'=>$paypal_card_fee
        );    
        
        /*dump($data);
        dump($total);*/
                                                     
    	$receipt=EmailTPL::salesReceipt($print,array(
    	  'item'=>$item,
    	  'total'=>$total
    	));    	    	
    	$tpl=Yii::app()->functions->getOption("receipt_content",$merchant_id);
		if (empty($tpl)){
			$tpl=EmailTPL::receiptTPL();
		}
		$tpl=Yii::app()->functions->smarty('receipt',$receipt,$tpl);
        $tpl=Yii::app()->functions->smarty('customer-name',$client_fullname,$tpl);
        $tpl=Yii::app()->functions->smarty('receipt-number',Yii::app()->functions->formatOrderNumber($order_id),$tpl);
    	    	
    	$receipt_sender=Yii::app()->functions->getOption("receipt_sender",$merchant_id);
		$receipt_subject=Yii::app()->functions->getOption("receipt_subject",$merchant_id);
		if (empty($receipt_subject)){	
			$receipt_subject=getOptionA('receipt_default_subject');
			if (empty($receipt_subject)){
			    $receipt_subject="We have receive your order";
			}
		}
		if (empty($receipt_sender)){
			$receipt_sender='no-reply@'.$_SERVER['HTTP_HOST'];
		}
				
		/*dump($tpl);
		die();*/
		
		$db_ext=new DbExt; 
				
		if ($ok_send_notification){
		   sendEmail($client_email,$receipt_sender,$receipt_subject,$tpl);
		} else {
			/// saved to database and send it once actually paid
			$params=array(
			  'order_id'=>$order_id,
			  'client_email'=>$client_email,
			  'receipt_sender'=>$receipt_sender,
			  'receipt_subject'=>$receipt_subject,
			  'tpl'=>$tpl			  
			);
			$db_ext->insertData("{{mobile_temp_email}}",$params);
		}   
		
		/*send email to merchant address*/ 
		$merchant_notify_email=Yii::app()->functions->getOption("merchant_notify_email",$merchant_id);    
        $enabled_alert_notification=Yii::app()->functions->getOption("enabled_alert_notification",$merchant_id);            
        
        if ( $enabled_alert_notification==""){ 
        	
        	$merchant_receipt_subject=Yii::app()->functions->getOption("merchant_receipt_subject",$merchant_id);
    	
    	    $merchant_receipt_subject=empty($merchant_receipt_subject)?t("New Order From").
    	    " ".$client_fullname:$merchant_receipt_subject;
    	
    	    $merchant_receipt_content=Yii::app()->functions->getMerchantReceiptTemplate($merchant_id);
    	    
    	    $final_tpl='';    	
	    	if (!empty($merchant_receipt_content)){
	    		$merchant_token=Yii::app()->functions->getMerchantActivationToken($merchant_id);
	    		$confirmation_link=Yii::app()->getBaseUrl(true)."/store/confirmorder/?id=".$order_id."&token=$merchant_token";
	    		$final_tpl=smarty('receipt-number',Yii::app()->functions->formatOrderNumber($order_id)
	    		,$merchant_receipt_content);    		
	    		$final_tpl=smarty('customer-name',$client_fullname,$final_tpl);
	    		$final_tpl=smarty('receipt',$receipt,$final_tpl); 
	    		$final_tpl=smarty('confirmation-link',$confirmation_link,$final_tpl); 
	    	} else $final_tpl=$tpl;
	    	    	
	    	$global_admin_sender_email=Yii::app()->functions->getOptionAdmin('global_admin_sender_email');
	    	if (empty($global_admin_sender_email)){
	    		$global_admin_sender_email=$receipt_sender;
	    	}     	
	    		    		    	
	    	// fixed if email is multiple
	    	$merchant_notify_email=explode(",",$merchant_notify_email);    	
	    	if (is_array($merchant_notify_email) && count($merchant_notify_email)>=1){
	    		foreach ($merchant_notify_email as $merchant_notify_email_val) {    			
	    			if(!empty($merchant_notify_email_val)){
	    				if ($ok_send_notification){
		    			    sendEmail(trim($merchant_notify_email_val),
		    			    $global_admin_sender_email,$merchant_receipt_subject,$final_tpl);
	    				} else {
	    					/// saved to database and send it once actually paid
	    					$params=array(
							  'order_id'=>$order_id,
							  'client_email'=>$merchant_notify_email_val,
							  'receipt_sender'=>$global_admin_sender_email,
							  'receipt_subject'=>$merchant_receipt_subject,
							  'tpl'=>$final_tpl,
							  'email_type'=>"merchant"
							);
							$db_ext->insertData("{{mobile_temp_email}}",$params);
	    				}	    			
	    			}
	    		}
	    	}    	    	
	    	
        }	
		
    }
    
    public function processPendingReceiptEmail($order_id='')
    {    	
    	$db_ext=new DbExt; 
    	$stmt="SELECT * FROM
    	{{mobile_temp_email}}
    	WHERE
    	order_id=".self::q($order_id)."
    	AND 
    	status='pending'    	    
    	";
    	if ($res=$db_ext->rst($stmt)){    		
    		foreach ($res as $val) {    			
    			$id=$val['id'];
    			    			    			
    			switch ($val['email_type']) {
    				
    				case "client":   
    				case "merchant": 					    					    			
	    			$send_stats=sendEmail($val['client_email'],$val['receipt_sender'],
	    			$val['receipt_subject'],$val['tpl']);
	    			if($send_stats){
	    				$send_stats='sent';
	    			} else $send_stats='sending failed';    		
	    			
	    			$params=array('status'=>$send_stats);      			
	    			$db_ext->updateData("{{mobile_temp_email}}",$params,'id',$id);
    			    break;

    				case "sms":
    					$params=array(
    					  'merchant_id'=>$val['merchant_id'],
    					  'broadcast_id'=>'999999999',
    					  'client_id'=>$val['client_id'],
    					  'client_name'=>$val['client_name'],
    					  'contact_phone'=>$val['client_email'],
    					  'sms_message'=>$val['tpl'],
    					  'date_created'=>date('c'),
    					  'ip_address'=>$_SERVER['REMOTE_ADDR'],
    					  'gateway'=>$val['gateway']
    					);    					
    					$db_ext->insertData("{{sms_broadcast_details}}",$params);
    					
    					$params=array('status'=>'saved');      			
	    			    $db_ext->updateData("{{mobile_temp_email}}",$params,'id',$id);
    					break ;	    						    
    			}
    		}
    	}
    	return false;  
    }
    
    public static function platFormList()
    {
    	return array(
	    	1=>"android",
	        2=>'ios',
	        3=>"all platform"
    	);
    }
    
    public static function getBroadcast($broadcast_id='')
    {
    	$stmt="
    	SELECT * FROM
    	{{mobile_broadcast}}
    	WHERE
    	broadcast_id=".self::q($broadcast_id)."
    	";
    	$db_ext=new DbExt; 
    	if ($res=$db_ext->rst($stmt)){
    		return $res;
    	}
    	return false;    
    }
    
    public static function availableLanguages()
    {
    	$lang['en']='English';
    	$stmt="
    	SELECT * FROM
    	{{languages}}
    	WHERE
    	status in ('publish','published')
    	";
    	$db_ext=new DbExt; 
    	if ($res=$db_ext->rst($stmt)){
    		foreach ($res as $val) {
    			$lang[$val['lang_id']]=$val['language_code'];
    		}    		
    	}
    	return $lang;
    }
    
    public function translateItem($type='category',$string='',$id='',$field1='category_name_trans')
    {    	
    	$lang_id=$_GET['lang_id'];
    	
    	$db_ext=new DbExt; 
    	    	
    	switch ($type) {
    		case "category":    			
    			$stmt="SELECT $field1 FROM
    			{{category}}
    			WHERE
    			cat_id=".self::q($id)."
    			LIMIT 0,1
    			";    		    			
    			break;
    			
    		case "item":	
    		   $stmt="SELECT $field1 FROM
    			{{item}}
    			WHERE
    			item_id=".self::q($id)."
    			LIMIT 0,1
    			";    	
    		    //dump($stmt);	    			
    			break;
    			
    		
    	
    		default:
    			break;
    	}
    	
    	if ($res=$db_ext->rst($stmt)){
    		$res=$res[0];
    		//dump($res);
    		$text=!empty($res[$field1])?json_decode($res[$field1],true):false;
    		if ($text!=false){
    			//dump($text);
    			if (array_key_exists($lang_id,(array)$text)){
    				if (!empty($text[$lang_id])){
    					return $text[$lang_id];
    				}    			
    			}    		
    		}    	
    	}
    	return $string;
    }
    
    
    
        
    public static function getDistanceNew($merchant_info='',$client_address='')
    {
    	if(!is_array($merchant_info)){
    		return false;
    	}
    	if(empty($client_address)){
    		return false;
    	}
    	$merchant_address=$merchant_info['address'];
    	$mtid=isset($merchant_info['merchant_id'])?$merchant_info['merchant_id']:'';
    	if(empty($mtid)){
    		return false;
    	}
    	$merchant_lat=getOption($mtid,'merchant_latitude');
    	$merchant_lng=getOption($mtid,'merchant_longitude');
    	
    	if(!is_numeric($merchant_lat)){
	    	if ($lat_res=Yii::app()->functions->geodecodeAddress($merchant_address)){
		        $merchant_lat=$lat_res['lat'];
				$merchant_lng=$lat_res['long'];
	    	} 
    	}
    	
    	$client_lat=0;
    	$client_lng=0;
    	
    	if ($client_position=Yii::app()->functions->geodecodeAddress($client_address)){
			
	        $client_lat=$client_position['lat'];
			$client_lng=$client_position['long'];
    	} else {
			// file_put_contents("log.txt",var_export($client_position,true));
			return false;}
    	
    	/*dump($client_lat);
    	dump($client_lng);*/
    	
    	/*get the distance from client address to merchant Address*/             
	    $distance_type=FunctionsV3::getMerchantDistanceType($mtid); 
	    $distance_type_orig=$distance_type;
	    
	    $distance=FunctionsV3::getDistanceBetweenPlot(
	        $client_lat,
	        $client_lng,
	        $merchant_lat,
	        $merchant_lng,
	        $distance_type
	    );    
	    if(!$distance){
	    	return false;
	    }
	    //dump($distance);
	   
	    $distance_type_raw = $distance_type=="M"?"miles":"kilometers";            		            
        $distance_type=$distance_type=="M"?t("miles"):t("kilometers");
        $distance_type_orig = $distance_type; 
        
        if(!empty(FunctionsV3::$distance_type_result)){
	       $distance_type_raw=FunctionsV3::$distance_type_result;
	       $distance_type=t(FunctionsV3::$distance_type_result);
	    }
	    
	    /*GET DELIVERY FEE*/
	    $delivery_fee=FunctionsV3::getMerchantDeliveryFee(
		                          $mtid,
		                          isset($merchant_info['delivery_fee_raw'])?$merchant_info['delivery_fee_raw']:0,
		                          $distance,
		                          $distance_type_raw);
		return array(
		  'distance_type'=>$distance_type,
		  'distance_type_raw'=>$distance_type_raw,
		  'distance'=>$distance,
		  'delivery_fee'=>$delivery_fee
		);                   
    }
	
	public static function calculateDeliveryCharges($list_merchant,$client,$weight,$order_type) {
		// var_dump($list_merchant);
		// var_dump($weight);
		// var_dump($order_type);
		if(!is_array($list_merchant)){
			// echo "false1";
    		return false;
    	}
    	if(empty($client)){
			// echo "false2";
    		return false;
    	}
		    	
    	$client_lat=0;
    	$client_lng=0;
    	if(!empty($client['latitude']) && !empty($client['longitude'])) {
	        $client_lat=$client['latitude'];
			$client_lng=$client['longitude'];
		} elseif (!empty($client['search_address']) || !empty($client['street'])){
			$param_search = empty($client['search_address'])? $client['street'] : $client['search_address'];
			if ($client_position=Yii::app()->functions->geodecodeAddress($param_search)){
				$client_lat=$client_position['lat'];
				$client_lng=$client_position['long'];
			} else {
				return false;
			}
    	} else {
			return false;
		}

    	/*dump($client_lat);
    	dump($client_lng);*/
    	
		$total_delivery_fee = 0;
		$total_distance = 0;
		
		//key nya adalah merchant id value nya adalah pendapatan untuk merchant
		//mendapatkan latitude longitude merchant
		
		$mtid_list = implode(",",array_unique($list_merchant));
		$merchants = query("SELECT merchant_id, latitude, longitude FROM {{view_merchant}} WHERE merchant_id IN($mtid_list)");
		//jadikan merchant_id sebagai key
		$merchants2 = array();
		$first = true;
		$merchants_track = array();
		// $index = 0;
		// $first_element = array();
		foreach($merchants as $m) {
			// if($first) {
				// //mendapatkan elemen pertama untuk dipakai nanti
				// $first_element = $m;
				// $first = false;
			// }
			$merchants2[$m['merchant_id']] = array(
				'latitude'=>$m['latitude'],
				'longitude'=>$m['longitude'],
				'merchant_id'=>$m['merchant_id'],
			);
		}
		
		//mencari rute terpendek 
		
		//tetapkan merchant pertama
		$first_element = array_slice($merchants2, 0, 1,true);
		$merchants2 = array_slice($merchants2, 1,100,true);
		
		$merchants_track = $first_element;
		$index = $first_element['merchant_id'];
		
		// var_dump($merchants_track);
		// var_dump("y");
		// var_dump($merchants2);
		// var_dump("y2");
		
		while(!empty($merchants2)) {
				
				//cari merchant berikutnya
				$shortest_merchant = 0;
				$distance = 9999999;
				foreach($merchants2 as $m) {
					if($m['merchant_id'] !== $merchants_track[$index]['merchant_id']) {
						//cari jarak nya
						
						$distance2=FunctionsV3::getDistanceBetweenPlot(
							$merchants_track[$index]['latitude'],
							$merchants_track[$index]['longitude'],
							$m['latitude'],
							$m['longitude'],
							'km');

						if($distance2<$distance) {
							// var_dump($distance);
							// var_dump($distance2);
							// var_dump($m['merchant_id']);
							//ditemukan lebih pendek
							$distance =$distance2;
							$shortest_merchant = $m['merchant_id'];
						}
					}
				}
				//setelah looping didapat merchant terpendek
				//ambil dari merchants 2 masukan ke merchants_track
					
				$merchants_track[$shortest_merchant] = $merchants2[$shortest_merchant];
				unset($merchants2[$shortest_merchant]);
				$index=$shortest_merchant;//tetapkan sebagai index merchant terakhir
				//lakukan terus sampai konten merchants2 kosong
				// var_dump($merchants_track);
		}
		
		//telah didapat urutan merchant terpendek
		//selanjutnya hitung jarak keseluruhan dari urutan merchant tsb
		$mt = array();
		
		
		$distance3 = array();
		 // var_dump($merchants_track);
		while(!empty($merchants_track)) {
			$mt = array_shift($merchants_track);
			reset($merchants_track);
			$current = current($merchants_track);
			
			//loop terakhir current tidak ada isinya, karena sudah diambil saat array_shift
			if(!empty($current)) {
				$distance3[$mt['merchant_id'].'-'.$current['merchant_id']]=FunctionsV3::getDistanceBetweenPlot(
								$mt['latitude'],
								$mt['longitude'],
								$current['latitude'],
								$current['longitude'],
								'km');
			}
			
		}
		//terakhir distance dengan customer
		$distance3[$mt['merchant_id'].'-customer']=FunctionsV3::getDistanceBetweenPlot(
						$mt['latitude'],
						$mt['longitude'],
						$client_lat,
						$client_lng,
						'km');
		
		// var_dump($distance3);
		$distance = 0;
		foreach ($distance3 as $d) {
			$distance += $d;
		}
		
		
		
		//setelah didapat distance barulah dihitung delivery charge
		$motor_max_weight = Yii::app()->functions->getOptionAdmin('motor_max_weight');
		$shuttle_motor_flat_range = Yii::app()->functions->getOptionAdmin('shuttle_motor_flat_range');
		$shuttle_motor_flat_cost = Yii::app()->functions->getOptionAdmin('shuttle_motor_flat_cost');
		$shuttle_motor_continue = Yii::app()->functions->getOptionAdmin('shuttle_motor_continue');
		$motor_flat_range = Yii::app()->functions->getOptionAdmin('motor_flat_range');
		$motor_flat_cost = Yii::app()->functions->getOptionAdmin('motor_flat_cost');
		$motor_continue = Yii::app()->functions->getOptionAdmin('motor_continue');
		$shuttle_car_flat_range = Yii::app()->functions->getOptionAdmin('shuttle_car_flat_range');
		$shuttle_car_flat_cost = Yii::app()->functions->getOptionAdmin('shuttle_car_flat_cost');
		$shuttle_car_continue = Yii::app()->functions->getOptionAdmin('shuttle_car_continue');
		$car_flat_range = Yii::app()->functions->getOptionAdmin('car_flat_range');
		$car_flat_cost = Yii::app()->functions->getOptionAdmin('car_flat_cost');
		$car_continue = Yii::app()->functions->getOptionAdmin('car_continue');
		// $distance = 12;
		// $weight= 40000;
		// var_dump("dis".$distance);
		$cost = 0;
		if($weight < ($motor_max_weight * 1000)) {
			//motor
			if($order_type =='express') {
				//tarif express
				if($distance > $motor_flat_range) {
					//jarak di atas 
					$cost = $motor_flat_cost + $motor_continue * ($distance - $motor_flat_range);
				} else {
					//jarak di bawah flat
					if($motor_flat_range) {
						//motor flat range diset
						$cost = $motor_flat_cost;
					} else {
						//motor flat range tidak diset atau nol
						$cost = $motor_continue * $distance;
					}
				}
			} elseif($order_type =='economy') {
				//tarif express
				if($distance > $shuttle_motor_flat_range) {
					//jarak di atas 
					$cost = $shuttle_motor_flat_cost + $shuttle_motor_continue * ($distance - $shuttle_motor_flat_range);
				} else {
					//jarak di bawah flat
					if($shuttle_motor_flat_range) {
						//motor flat range diset
						$cost = $shuttle_motor_flat_cost;
					} else {
						//motor flat range tidak diset atau nol
						$cost = $shuttle_motor_continue * $distance;
					}
				}
			}
		} else {
			//mobil
			//car
			if($order_type =='express') {
				//tarif express
				if($distance > $car_flat_range) {
					//jarak di atas 
					$cost = $car_flat_cost + $car_continue * ($distance - $car_flat_range);
				} else {
					//jarak di bawah flat
					if($car_flat_range) {
						//car flat range diset
						$cost = $car_flat_cost;
					} else {
						//car flat range tidak diset atau nol
						$cost = $car_continue * $distance;
					}
				}
			} elseif($order_type =='economy') {
				//tarif express
				if($distance > $shuttle_car_flat_range) {
					//jarak di atas 
					$cost = $shuttle_car_flat_cost + $shuttle_car_continue * ($distance - $shuttle_car_flat_range);
				} else {
					//jarak di bawah flat
					if($shuttle_car_flat_range) {
						//car flat range diset
						$cost = $shuttle_car_flat_cost;
					} else {
						//car flat range tidak diset atau nol
						$cost = $shuttle_car_continue * $distance;
					}
				}
			}
		}
		// var_dump($weight);
		// var_dump($cost);
		$merchants_max_distance = Yii::app()->functions->getOptionAdmin('merchants_max_distance');
		
		$valid_distance = true;
		if($distance > $merchants_max_distance) {
			$valid_distance = false;
		}
		
		return array(
			'delivery_fee' => $cost,
			'distance' => $distance,
			'valid_distance' => $valid_distance,
			'distance_type' =>  t("kilometers"),
			'client_lat'=>$client_lat,
			'client_lng'=>$client_lng,
		);
	}
	
    public static function getDistanceMerchantClient($list_merchant,$client_address='')
    {
    	if(!is_array($list_merchant)){
			// echo "false1";
    		return false;
    	}
    	if(empty($client_address)){
			// echo "false2";
    		return false;
    	}
		

    	
    	$client_lat=0;
    	$client_lng=0;
    	
    	if ($client_position=Yii::app()->functions->geodecodeAddress($client_address)){
	        $client_lat=$client_position['lat'];
			$client_lng=$client_position['long'];
    	} else {
			// echo "false3";
			// file_put_contents("log.txt",var_export($client_position,true));
			return false;}

    	/*dump($client_lat);
    	dump($client_lng);*/
    	
		$total_delivery_fee = 0;
		$total_distance = 0;
		
		//key nya adalah merchant id value nya adalah pendapatan untuk merchant
		foreach($list_merchant as $mtid => $revenue) {
				$merchant_info= AddonMobileApp::merchantInformation($mtid);		
				$merchant_address=$merchant_info['address'];
				// $mtid=isset($merchant_info['merchant_id'])?$merchant_info['merchant_id']:'';
				// if(empty($mtid)){
					// return false;
				// }
				$merchant_lat=getOption($mtid,'merchant_latitude');
				$merchant_lng=getOption($mtid,'merchant_longitude');
				
				if(!is_numeric($merchant_lat)){
					if ($lat_res=Yii::app()->functions->geodecodeAddress($merchant_address)){
						$merchant_lat=$lat_res['lat'];
						$merchant_lng=$lat_res['long'];
					} 
				}
				
				
				/*get the distance from client address to merchant Address*/             
				// $distance_type=FunctionsV3::getMerchantDistanceType($mtid); 
				// $distance_type_orig=$distance_type;
				// var_dump($distance_type);

				$distance_type_raw = "kilometers";
				$distance_type = t("kilometers");
				$distance_type_orig = $distance_type;
				
				$distance=FunctionsV3::getDistanceBetweenPlot(
					$client_lat,
					$client_lng,
					$merchant_lat,
					$merchant_lng,
					$distance_type
				);
				if(!$distance){
					// echo "false4";
					return false;
				}
				//dump($distance);
			   
				// $distance_type_raw = $distance_type=="M"?"miles":"kilometers";            		            
				// $distance_type=$distance_type=="M"?t("miles"):t("kilometers");
				

				
				if(!empty(FunctionsV3::$distance_type_result)){
				   $distance_type_raw=FunctionsV3::$distance_type_result;
				   $distance_type=t(FunctionsV3::$distance_type_result);
				}
				
				/*GET DELIVERY FEE*/
				$delivery_fee=FunctionsV3::getMerchantDeliveryFee(
										  $mtid,
										  1000,
										  $distance,
										  $distance_type_raw);
				//var_dump("delivery_fee",$delivery_fee);
				$total_delivery_fee += $delivery_fee;
				$total_distance += $distance;
		}
		
		return array(
		  'distance_type'=>$distance_type,
		  'distance_type_raw'=>$distance_type_raw,
		  'distance'=>$total_distance,
		  'delivery_fee'=>$total_delivery_fee
		);                   
    }
 
	public static function verifyMobileCode($code='',$client_id='')
	{	
		if( $res=Yii::app()->functions->getClientInfo($client_id)){
			if ( $code==$res['mobile_verification_code']){
				return $res;
			} 	
		} 
		return false;
	}
	
	public static function hasModuleAddon($modulename='')
	{
		if (Yii::app()->hasModule('pointsprogram')){
		   $path_to_upload=Yii::getPathOfAlias('webroot')."/protected/modules/$modulename";	
		   if(file_exists($path_to_upload)){
		   	   return true;
		   }
		}
		return false;
	}
	
	public static function qTranslate($text='',$key='',$data='')
	{		
		if (Yii::app()->functions->getOptionAdmin("enabled_multiple_translation")!=2){
			return stripslashes($text);
		}
		$key=$key."_trans";			
		$id=$_GET['lang_id'];		
		if ( $id>0){
			if (is_array($data) && count($data)>=1){
				if (isset($data[$key])){
					if (array_key_exists($id,(array)$data[$key])){
						if (!empty($data[$key][$id])){
						    return stripslashes($data[$key][$id]);
						}
					}
				}
			}
		}	
		return stripslashes($text);
	}
	
    public static function getAvatar($client_id='',$res='')
    {
    	//if ( $res = Yii::app()->functions->getClientInfo($client_id) ){
    	if($res){
    		$file=$res['avatar'];
    	} else $file='avatar.jpg';
    	
    	if (empty($file)){
    		$file='avatar.jpg';
    	}
    	    	    
    	$path=Yii::getPathOfAlias('webroot')."/upload/$file";    	
    	if ( file_exists($path) ){       		 		    	
    		return Yii::app()->getBaseUrl(true)."/upload/$file";
    	} else return Yii::app()->getBaseUrl(true)."/assets/images/avatar.jpg";    	
    }
		    
}/* end class*/